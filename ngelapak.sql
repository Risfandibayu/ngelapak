/*
 Navicat Premium Data Transfer

 Source Server         : Mysql
 Source Server Type    : MySQL
 Source Server Version : 100421
 Source Host           : localhost:3306
 Source Schema         : ngelapak

 Target Server Type    : MySQL
 Target Server Version : 100421
 File Encoding         : 65001

 Date: 24/06/2022 23:28:52
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for accounts
-- ----------------------------
DROP TABLE IF EXISTS `accounts`;
CREATE TABLE `accounts`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `account_title` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `opening_date` date NOT NULL,
  `account_number` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `account_currency` varchar(3) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `opening_balance` decimal(10, 2) NOT NULL,
  `note` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `company_id` bigint NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of accounts
-- ----------------------------

-- ----------------------------
-- Table structure for activity_logs
-- ----------------------------
DROP TABLE IF EXISTS `activity_logs`;
CREATE TABLE `activity_logs`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `related_to` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `related_id` bigint NULL DEFAULT NULL,
  `activity` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint NOT NULL,
  `company_id` bigint NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 150 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of activity_logs
-- ----------------------------
INSERT INTO `activity_logs` VALUES (1, 'projects', 1, 'Uploaded File', 6, 5, '2021-10-08 22:26:35', '2021-10-08 22:26:35');
INSERT INTO `activity_logs` VALUES (2, 'projects', 2, 'Uploaded File', 47, 46, '2021-10-14 20:42:54', '2021-10-14 20:42:54');
INSERT INTO `activity_logs` VALUES (3, 'projects', 2, 'File Removed', 47, 46, '2021-10-15 11:22:58', '2021-10-15 11:22:58');
INSERT INTO `activity_logs` VALUES (4, 'projects', 3, 'Uploaded File', 69, 68, '2021-10-21 15:32:51', '2021-10-21 15:32:51');
INSERT INTO `activity_logs` VALUES (5, 'projects', 3, 'Updated Project', 69, 68, '2021-10-21 16:23:09', '2021-10-21 16:23:09');
INSERT INTO `activity_logs` VALUES (6, 'projects', 3, 'File Removed', 69, 68, '2021-10-21 17:07:13', '2021-10-21 17:07:13');
INSERT INTO `activity_logs` VALUES (7, 'projects', 4, 'Uploaded File', 69, 68, '2021-10-21 17:56:05', '2021-10-21 17:56:05');
INSERT INTO `activity_logs` VALUES (8, 'projects', 4, 'File Removed', 69, 68, '2021-10-22 09:41:38', '2021-10-22 09:41:38');
INSERT INTO `activity_logs` VALUES (9, 'projects', 5, 'Uploaded File', 69, 68, '2021-10-22 11:24:51', '2021-10-22 11:24:51');
INSERT INTO `activity_logs` VALUES (10, 'projects', 5, 'Updated Project', 69, 68, '2021-10-22 11:25:05', '2021-10-22 11:25:05');
INSERT INTO `activity_logs` VALUES (11, 'projects', 5, 'File Removed', 69, 68, '2021-10-26 13:11:09', '2021-10-26 13:11:09');
INSERT INTO `activity_logs` VALUES (12, 'projects', 6, 'Uploaded File', 69, 68, '2021-10-26 13:29:07', '2021-10-26 13:29:07');
INSERT INTO `activity_logs` VALUES (13, 'projects', 7, 'Uploaded File', 121, 173, '2021-10-27 12:52:58', '2021-10-27 12:52:58');
INSERT INTO `activity_logs` VALUES (14, 'projects', 8, 'Uploaded File', 121, 173, '2021-10-27 13:12:59', '2021-10-27 13:12:59');
INSERT INTO `activity_logs` VALUES (15, 'projects', 9, 'Uploaded File', 121, 173, '2021-10-27 13:18:42', '2021-10-27 13:18:42');
INSERT INTO `activity_logs` VALUES (16, 'projects', 8, 'File Removed', 121, 173, '2021-10-27 13:19:48', '2021-10-27 13:19:48');
INSERT INTO `activity_logs` VALUES (17, 'projects', 7, 'File Removed', 121, 173, '2021-10-27 13:19:53', '2021-10-27 13:19:53');
INSERT INTO `activity_logs` VALUES (18, 'projects', 6, 'Updated Project', 69, 68, '2021-10-27 13:58:32', '2021-10-27 13:58:32');
INSERT INTO `activity_logs` VALUES (19, 'projects', 6, 'Updated Project', 69, 68, '2021-10-27 13:58:44', '2021-10-27 13:58:44');
INSERT INTO `activity_logs` VALUES (20, 'projects', 6, 'Updated Project', 69, 68, '2021-10-27 14:00:54', '2021-10-27 14:00:54');
INSERT INTO `activity_logs` VALUES (21, 'projects', 6, 'Updated Project', 69, 68, '2021-10-27 14:01:12', '2021-10-27 14:01:12');
INSERT INTO `activity_logs` VALUES (22, 'projects', 6, 'Updated Project', 69, 68, '2021-10-27 14:01:24', '2021-10-27 14:01:24');
INSERT INTO `activity_logs` VALUES (23, 'projects', 6, 'Updated Project', 69, 68, '2021-10-27 14:05:18', '2021-10-27 14:05:18');
INSERT INTO `activity_logs` VALUES (24, 'projects', 9, 'Updated Project', 121, 173, '2021-10-27 14:10:11', '2021-10-27 14:10:11');
INSERT INTO `activity_logs` VALUES (25, 'projects', 10, 'Uploaded File', 126, 178, '2021-10-28 12:46:20', '2021-10-28 12:46:20');
INSERT INTO `activity_logs` VALUES (26, 'projects', 11, 'Uploaded File', 133, 185, '2021-10-28 13:46:17', '2021-10-28 13:46:17');
INSERT INTO `activity_logs` VALUES (27, 'projects', 12, 'Uploaded File', 135, 190, '2021-10-28 20:23:32', '2021-10-28 20:23:32');
INSERT INTO `activity_logs` VALUES (28, 'projects', 10, 'File Removed', 126, 178, '2021-10-28 20:31:32', '2021-10-28 20:31:32');
INSERT INTO `activity_logs` VALUES (29, 'projects', 13, 'Uploaded File', 139, 194, '2021-10-31 21:32:26', '2021-10-31 21:32:26');
INSERT INTO `activity_logs` VALUES (30, 'projects', 14, 'Uploaded File', 139, 194, '2021-10-31 21:37:18', '2021-10-31 21:37:18');
INSERT INTO `activity_logs` VALUES (31, 'projects', 15, 'Uploaded File', 138, 193, '2021-11-04 10:55:34', '2021-11-04 10:55:34');
INSERT INTO `activity_logs` VALUES (32, 'projects', 16, 'Uploaded File', 138, 193, '2021-11-04 11:57:47', '2021-11-04 11:57:47');
INSERT INTO `activity_logs` VALUES (33, 'projects', 17, 'Uploaded File', 148, 203, '2021-11-05 13:56:40', '2021-11-05 13:56:40');
INSERT INTO `activity_logs` VALUES (34, 'projects', 17, 'Updated Project', 148, 203, '2021-11-05 14:14:32', '2021-11-05 14:14:32');
INSERT INTO `activity_logs` VALUES (35, 'projects', 17, 'Updated Project', 148, 203, '2021-11-05 14:27:31', '2021-11-05 14:27:31');
INSERT INTO `activity_logs` VALUES (36, 'projects', 17, 'Updated Project', 148, 203, '2021-11-08 13:50:43', '2021-11-08 13:50:43');
INSERT INTO `activity_logs` VALUES (37, 'projects', 17, 'Updated Project', 148, 203, '2021-11-08 13:55:43', '2021-11-08 13:55:43');
INSERT INTO `activity_logs` VALUES (38, 'projects', 17, 'Updated Project', 148, 203, '2021-11-08 13:55:54', '2021-11-08 13:55:54');
INSERT INTO `activity_logs` VALUES (39, 'projects', 17, 'Updated Project', 148, 203, '2021-11-08 13:56:25', '2021-11-08 13:56:25');
INSERT INTO `activity_logs` VALUES (40, 'projects', 17, 'Updated Project', 148, 203, '2021-11-08 13:57:15', '2021-11-08 13:57:15');
INSERT INTO `activity_logs` VALUES (41, 'projects', 17, 'Updated Project', 148, 203, '2021-11-08 13:57:39', '2021-11-08 13:57:39');
INSERT INTO `activity_logs` VALUES (42, 'projects', 17, 'Updated Project', 148, 203, '2021-11-08 13:57:55', '2021-11-08 13:57:55');
INSERT INTO `activity_logs` VALUES (43, 'projects', 17, 'Updated Project', 148, 203, '2021-11-08 13:59:47', '2021-11-08 13:59:47');
INSERT INTO `activity_logs` VALUES (44, 'projects', 17, 'Updated Project', 148, 203, '2021-11-08 14:00:54', '2021-11-08 14:00:54');
INSERT INTO `activity_logs` VALUES (45, 'projects', 17, 'Updated Project', 148, 203, '2021-11-08 14:04:12', '2021-11-08 14:04:12');
INSERT INTO `activity_logs` VALUES (46, 'projects', 17, 'Updated Project', 148, 203, '2021-11-08 14:10:45', '2021-11-08 14:10:45');
INSERT INTO `activity_logs` VALUES (47, 'projects', 17, 'Updated Project', 148, 203, '2021-11-09 10:34:35', '2021-11-09 10:34:35');
INSERT INTO `activity_logs` VALUES (48, 'projects', 17, 'File Removed', 148, 203, '2021-11-09 10:47:52', '2021-11-09 10:47:52');
INSERT INTO `activity_logs` VALUES (49, 'projects', 18, 'Uploaded File', 148, 203, '2021-11-09 10:49:38', '2021-11-09 10:49:38');
INSERT INTO `activity_logs` VALUES (50, 'projects', 18, 'File Removed', 148, 203, '2021-11-09 10:50:12', '2021-11-09 10:50:12');
INSERT INTO `activity_logs` VALUES (51, 'projects', 19, 'Uploaded File', 148, 203, '2021-11-09 10:54:50', '2021-11-09 10:54:50');
INSERT INTO `activity_logs` VALUES (52, 'projects', 19, 'File Removed', 148, 203, '2021-11-09 10:59:55', '2021-11-09 10:59:55');
INSERT INTO `activity_logs` VALUES (53, 'projects', 20, 'Uploaded File', 148, 203, '2021-11-09 11:00:43', '2021-11-09 11:00:43');
INSERT INTO `activity_logs` VALUES (54, 'projects', 20, 'File Removed', 148, 203, '2021-11-09 11:16:04', '2021-11-09 11:16:04');
INSERT INTO `activity_logs` VALUES (55, 'projects', 21, 'Uploaded File', 148, 203, '2021-11-09 11:16:22', '2021-11-09 11:16:22');
INSERT INTO `activity_logs` VALUES (56, 'projects', 21, 'Updated Project', 148, 203, '2021-11-10 09:49:25', '2021-11-10 09:49:25');
INSERT INTO `activity_logs` VALUES (57, 'projects', 21, 'File Removed', 148, 203, '2021-11-10 12:20:40', '2021-11-10 12:20:40');
INSERT INTO `activity_logs` VALUES (58, 'projects', 22, 'Uploaded File', 148, 203, '2021-11-10 12:20:53', '2021-11-10 12:20:53');
INSERT INTO `activity_logs` VALUES (59, 'projects', 22, 'Updated Project', 148, 203, '2021-11-10 12:21:48', '2021-11-10 12:21:48');
INSERT INTO `activity_logs` VALUES (60, 'projects', 22, 'Updated Project', 148, 203, '2021-11-10 13:18:27', '2021-11-10 13:18:27');
INSERT INTO `activity_logs` VALUES (61, 'projects', 22, 'Updated Project', 148, 203, '2021-11-10 13:27:57', '2021-11-10 13:27:57');
INSERT INTO `activity_logs` VALUES (62, 'projects', 22, 'Updated Project', 148, 203, '2021-11-10 13:35:22', '2021-11-10 13:35:22');
INSERT INTO `activity_logs` VALUES (63, 'projects', 22, 'Updated Project', 148, 203, '2021-11-10 13:36:43', '2021-11-10 13:36:43');
INSERT INTO `activity_logs` VALUES (64, 'projects', 22, 'Updated Project', 148, 203, '2021-11-10 13:38:52', '2021-11-10 13:38:52');
INSERT INTO `activity_logs` VALUES (65, 'projects', 22, 'Updated Project', 148, 203, '2021-11-10 13:39:38', '2021-11-10 13:39:38');
INSERT INTO `activity_logs` VALUES (66, 'projects', 22, 'Updated Project', 148, 203, '2021-11-10 13:42:20', '2021-11-10 13:42:20');
INSERT INTO `activity_logs` VALUES (67, 'projects', 22, 'Updated Project', 148, 203, '2021-11-10 13:43:09', '2021-11-10 13:43:09');
INSERT INTO `activity_logs` VALUES (70, 'projects', 22, 'Updated Project', 148, 203, '2021-11-12 11:33:56', '2021-11-12 11:33:56');
INSERT INTO `activity_logs` VALUES (71, 'projects', 22, 'Updated Project', 148, 203, '2021-11-15 09:10:09', '2021-11-15 09:10:09');
INSERT INTO `activity_logs` VALUES (72, 'projects', 22, 'Updated Project', 148, 203, '2021-11-15 14:11:40', '2021-11-15 14:11:40');
INSERT INTO `activity_logs` VALUES (73, 'projects', 22, 'Updated Project', 148, 203, '2021-11-15 14:14:03', '2021-11-15 14:14:03');
INSERT INTO `activity_logs` VALUES (74, 'projects', 22, 'Updated Project', 148, 203, '2021-11-15 14:14:34', '2021-11-15 14:14:34');
INSERT INTO `activity_logs` VALUES (75, 'projects', 22, 'Updated Project', 148, 203, '2021-11-15 14:15:02', '2021-11-15 14:15:02');
INSERT INTO `activity_logs` VALUES (76, 'projects', 22, 'Updated Project', 148, 203, '2021-11-15 14:15:34', '2021-11-15 14:15:34');
INSERT INTO `activity_logs` VALUES (77, 'projects', 22, 'Updated Project', 148, 203, '2021-11-15 14:15:50', '2021-11-15 14:15:50');
INSERT INTO `activity_logs` VALUES (78, 'projects', 22, 'Updated Project', 148, 203, '2021-11-15 14:16:17', '2021-11-15 14:16:17');
INSERT INTO `activity_logs` VALUES (79, 'projects', 22, 'Updated Project', 148, 203, '2021-11-15 14:16:34', '2021-11-15 14:16:34');
INSERT INTO `activity_logs` VALUES (80, 'projects', 22, 'Updated Project', 148, 203, '2021-11-15 14:17:21', '2021-11-15 14:17:21');
INSERT INTO `activity_logs` VALUES (81, 'projects', 22, 'Updated Project', 148, 203, '2021-11-15 14:18:01', '2021-11-15 14:18:01');
INSERT INTO `activity_logs` VALUES (82, 'projects', 22, 'Updated Project', 148, 203, '2021-11-15 14:18:33', '2021-11-15 14:18:33');
INSERT INTO `activity_logs` VALUES (83, 'projects', 22, 'Updated Project', 148, 203, '2021-11-15 14:20:19', '2021-11-15 14:20:19');
INSERT INTO `activity_logs` VALUES (84, 'projects', 22, 'Updated Project', 148, 203, '2021-11-15 14:21:09', '2021-11-15 14:21:09');
INSERT INTO `activity_logs` VALUES (85, 'projects', 22, 'Updated Project', 148, 203, '2021-11-15 14:27:37', '2021-11-15 14:27:37');
INSERT INTO `activity_logs` VALUES (86, 'projects', 22, 'Updated Project', 148, 203, '2021-11-15 14:32:53', '2021-11-15 14:32:53');
INSERT INTO `activity_logs` VALUES (87, 'projects', 22, 'Updated Project', 148, 203, '2021-11-15 14:33:20', '2021-11-15 14:33:20');
INSERT INTO `activity_logs` VALUES (88, 'projects', 22, 'Updated Project', 148, 203, '2021-11-15 14:34:56', '2021-11-15 14:34:56');
INSERT INTO `activity_logs` VALUES (89, 'projects', 22, 'Updated Project', 148, 203, '2021-11-15 14:36:48', '2021-11-15 14:36:48');
INSERT INTO `activity_logs` VALUES (90, 'projects', 22, 'Updated Project', 148, 203, '2021-11-15 14:37:17', '2021-11-15 14:37:17');
INSERT INTO `activity_logs` VALUES (91, 'projects', 22, 'Updated Project', 148, 203, '2021-11-15 14:38:59', '2021-11-15 14:38:59');
INSERT INTO `activity_logs` VALUES (92, 'projects', 22, 'Updated Project', 148, 203, '2021-11-15 14:41:37', '2021-11-15 14:41:37');
INSERT INTO `activity_logs` VALUES (93, 'projects', 22, 'Updated Project', 148, 203, '2021-11-15 14:41:51', '2021-11-15 14:41:51');
INSERT INTO `activity_logs` VALUES (94, 'projects', 22, 'Updated Project', 148, 203, '2021-11-15 14:43:18', '2021-11-15 14:43:18');
INSERT INTO `activity_logs` VALUES (95, 'projects', 22, 'Updated Project', 148, 203, '2021-11-15 14:44:30', '2021-11-15 14:44:30');
INSERT INTO `activity_logs` VALUES (96, 'projects', 22, 'Updated Project', 148, 203, '2021-11-15 14:46:30', '2021-11-15 14:46:30');
INSERT INTO `activity_logs` VALUES (97, 'projects', 22, 'Updated Project', 148, 203, '2021-11-15 14:57:12', '2021-11-15 14:57:12');
INSERT INTO `activity_logs` VALUES (98, 'projects', 22, 'Updated Project', 148, 203, '2021-11-15 14:57:57', '2021-11-15 14:57:57');
INSERT INTO `activity_logs` VALUES (99, 'projects', 22, 'Updated Project', 148, 203, '2021-11-15 14:58:03', '2021-11-15 14:58:03');
INSERT INTO `activity_logs` VALUES (100, 'projects', 22, 'Updated Project', 148, 203, '2021-11-15 14:58:12', '2021-11-15 14:58:12');
INSERT INTO `activity_logs` VALUES (101, 'projects', 22, 'Updated Project', 148, 203, '2021-11-15 14:59:36', '2021-11-15 14:59:36');
INSERT INTO `activity_logs` VALUES (102, 'projects', 22, 'Updated Project', 148, 203, '2021-11-15 15:01:09', '2021-11-15 15:01:09');
INSERT INTO `activity_logs` VALUES (103, 'projects', 22, 'Updated Project', 148, 203, '2021-11-15 15:01:15', '2021-11-15 15:01:15');
INSERT INTO `activity_logs` VALUES (104, 'projects', 22, 'Updated Project', 148, 203, '2021-11-15 15:02:03', '2021-11-15 15:02:03');
INSERT INTO `activity_logs` VALUES (105, 'projects', 22, 'Updated Project', 148, 203, '2021-11-15 15:03:26', '2021-11-15 15:03:26');
INSERT INTO `activity_logs` VALUES (106, 'projects', 22, 'Updated Project', 148, 203, '2021-11-15 15:31:27', '2021-11-15 15:31:27');
INSERT INTO `activity_logs` VALUES (107, 'projects', 22, 'Updated Project', 148, 203, '2021-11-15 15:32:11', '2021-11-15 15:32:11');
INSERT INTO `activity_logs` VALUES (108, 'projects', 22, 'Updated Project', 148, 203, '2021-11-15 15:38:34', '2021-11-15 15:38:34');
INSERT INTO `activity_logs` VALUES (109, 'projects', 22, 'Updated Project', 148, 203, '2021-11-15 15:39:49', '2021-11-15 15:39:49');
INSERT INTO `activity_logs` VALUES (110, 'projects', 22, 'Updated Project', 148, 203, '2021-11-15 15:40:37', '2021-11-15 15:40:37');
INSERT INTO `activity_logs` VALUES (111, 'projects', 22, 'Updated Project', 148, 203, '2021-11-15 15:42:04', '2021-11-15 15:42:04');
INSERT INTO `activity_logs` VALUES (112, 'projects', 22, 'Updated Project', 148, 203, '2021-11-15 15:54:21', '2021-11-15 15:54:21');
INSERT INTO `activity_logs` VALUES (113, 'projects', 22, 'Updated Project', 148, 203, '2021-11-15 15:56:50', '2021-11-15 15:56:50');
INSERT INTO `activity_logs` VALUES (114, 'projects', 22, 'Updated Project', 148, 203, '2021-11-15 15:58:50', '2021-11-15 15:58:50');
INSERT INTO `activity_logs` VALUES (115, 'projects', 22, 'Updated Project', 148, 203, '2021-11-15 16:00:16', '2021-11-15 16:00:16');
INSERT INTO `activity_logs` VALUES (116, 'projects', 22, 'Updated Project', 148, 203, '2021-11-15 16:00:56', '2021-11-15 16:00:56');
INSERT INTO `activity_logs` VALUES (117, 'projects', 22, 'Updated Project', 148, 203, '2021-11-15 16:46:54', '2021-11-15 16:46:54');
INSERT INTO `activity_logs` VALUES (118, 'projects', 22, 'Updated Project', 148, 203, '2021-11-16 09:49:15', '2021-11-16 09:49:15');
INSERT INTO `activity_logs` VALUES (119, 'projects', 22, 'Updated Project', 148, 203, '2021-11-16 09:51:06', '2021-11-16 09:51:06');
INSERT INTO `activity_logs` VALUES (120, 'projects', 22, 'Updated Project', 148, 203, '2021-11-16 10:35:02', '2021-11-16 10:35:02');
INSERT INTO `activity_logs` VALUES (121, 'projects', 22, 'Updated Project', 148, 203, '2021-11-16 11:16:48', '2021-11-16 11:16:48');
INSERT INTO `activity_logs` VALUES (122, 'projects', 23, 'Uploaded File', 126, 178, '2021-11-16 22:55:53', '2021-11-16 22:55:53');
INSERT INTO `activity_logs` VALUES (123, 'projects', 24, 'Uploaded File', 172, 234, '2021-11-18 10:21:32', '2021-11-18 10:21:32');
INSERT INTO `activity_logs` VALUES (124, 'projects', 24, 'File Removed', 172, 234, '2021-11-18 14:58:08', '2021-11-18 14:58:08');
INSERT INTO `activity_logs` VALUES (125, 'projects', 25, 'Uploaded File', 172, 234, '2021-11-18 15:08:40', '2021-11-18 15:08:40');
INSERT INTO `activity_logs` VALUES (126, 'projects', 25, 'Updated Project', 172, 234, '2021-11-18 15:10:34', '2021-11-18 15:10:34');
INSERT INTO `activity_logs` VALUES (127, 'projects', 25, 'Updated Project', 172, 234, '2021-11-19 10:07:50', '2021-11-19 10:07:50');
INSERT INTO `activity_logs` VALUES (128, 'projects', 25, 'Updated Project', 172, 234, '2021-11-25 12:49:48', '2021-11-25 12:49:48');
INSERT INTO `activity_logs` VALUES (129, 'projects', 25, 'File Removed', 172, 234, '2021-11-25 15:11:58', '2021-11-25 15:11:58');
INSERT INTO `activity_logs` VALUES (130, 'projects', 26, 'Uploaded File', 172, 234, '2021-11-25 17:09:55', '2021-11-25 17:09:55');
INSERT INTO `activity_logs` VALUES (131, 'projects', 26, 'Updated Project', 172, 234, '2021-11-25 17:13:18', '2021-11-25 17:13:18');
INSERT INTO `activity_logs` VALUES (132, 'projects', 26, 'File Removed', 172, 234, '2021-12-02 15:02:59', '2021-12-02 15:02:59');
INSERT INTO `activity_logs` VALUES (133, 'projects', 27, 'Uploaded File', 205, 268, '2021-12-30 10:07:21', '2021-12-30 10:07:21');
INSERT INTO `activity_logs` VALUES (134, 'projects', 28, 'Uploaded File', 172, 234, '2021-12-30 10:48:29', '2021-12-30 10:48:29');
INSERT INTO `activity_logs` VALUES (135, 'projects', 28, 'Updated Project', 172, 234, '2022-01-07 10:15:21', '2022-01-07 10:15:21');
INSERT INTO `activity_logs` VALUES (136, 'projects', 28, 'Updated Project', 172, 234, '2022-01-07 10:19:10', '2022-01-07 10:19:10');
INSERT INTO `activity_logs` VALUES (137, 'projects', 28, 'Updated Project', 172, 234, '2022-01-07 10:23:37', '2022-01-07 10:23:37');
INSERT INTO `activity_logs` VALUES (138, 'projects', 28, 'Updated Project', 172, 234, '2022-01-07 10:24:39', '2022-01-07 10:24:39');
INSERT INTO `activity_logs` VALUES (139, 'projects', 28, 'Updated Project', 172, 234, '2022-01-07 10:25:53', '2022-01-07 10:25:53');
INSERT INTO `activity_logs` VALUES (140, 'projects', 28, 'Updated Project', 172, 234, '2022-01-07 10:27:44', '2022-01-07 10:27:44');
INSERT INTO `activity_logs` VALUES (141, 'projects', 29, 'Uploaded File', 210, 306, '2022-01-07 13:28:03', '2022-01-07 13:28:03');
INSERT INTO `activity_logs` VALUES (142, 'projects', 29, 'Updated Project', 210, 306, '2022-01-24 11:12:10', '2022-01-24 11:12:10');
INSERT INTO `activity_logs` VALUES (143, 'projects', 30, 'Uploaded File', 211, 307, '2022-01-24 11:26:32', '2022-01-24 11:26:32');
INSERT INTO `activity_logs` VALUES (144, 'projects', 29, 'File Removed', 210, 306, '2022-01-24 12:46:39', '2022-01-24 12:46:39');
INSERT INTO `activity_logs` VALUES (145, 'projects', 31, 'Uploaded File', 210, 306, '2022-01-24 14:56:41', '2022-01-24 14:56:41');
INSERT INTO `activity_logs` VALUES (146, 'projects', 31, 'Updated Project', 210, 306, '2022-01-24 14:57:10', '2022-01-24 14:57:10');
INSERT INTO `activity_logs` VALUES (147, 'projects', 31, 'Updated Project', 210, 306, '2022-01-24 14:59:16', '2022-01-24 14:59:16');
INSERT INTO `activity_logs` VALUES (148, 'projects', 31, 'File Removed', 210, 306, '2022-02-04 12:44:38', '2022-02-04 12:44:38');
INSERT INTO `activity_logs` VALUES (149, 'projects', 32, 'Uploaded File', 210, 306, '2022-02-04 12:55:11', '2022-02-04 12:55:11');

-- ----------------------------
-- Table structure for alamat
-- ----------------------------
DROP TABLE IF EXISTS `alamat`;
CREATE TABLE `alamat`  (
  `id` int NOT NULL,
  `alamat` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `kota` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `prov` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `kode_pos` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of alamat
-- ----------------------------

-- ----------------------------
-- Table structure for chart_of_accounts
-- ----------------------------
DROP TABLE IF EXISTS `chart_of_accounts`;
CREATE TABLE `chart_of_accounts`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_id` bigint NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of chart_of_accounts
-- ----------------------------

-- ----------------------------
-- Table structure for chat_group_users
-- ----------------------------
DROP TABLE IF EXISTS `chat_group_users`;
CREATE TABLE `chat_group_users`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `group_id` bigint NOT NULL,
  `user_id` bigint NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of chat_group_users
-- ----------------------------

-- ----------------------------
-- Table structure for chat_groups
-- ----------------------------
DROP TABLE IF EXISTS `chat_groups`;
CREATE TABLE `chat_groups`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `company_id` bigint NOT NULL,
  `created_by` bigint NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of chat_groups
-- ----------------------------

-- ----------------------------
-- Table structure for chat_messages
-- ----------------------------
DROP TABLE IF EXISTS `chat_messages`;
CREATE TABLE `chat_messages`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `from` bigint NOT NULL,
  `to` bigint NOT NULL,
  `message` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `attachment` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `status` tinyint NOT NULL DEFAULT 0,
  `company_id` bigint NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of chat_messages
-- ----------------------------

-- ----------------------------
-- Table structure for cities
-- ----------------------------
DROP TABLE IF EXISTS `cities`;
CREATE TABLE `cities`  (
  `id` bigint NOT NULL,
  `province_id` bigint NOT NULL,
  `city_id` bigint NOT NULL,
  `title` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `postal_code` bigint NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cities
-- ----------------------------
INSERT INTO `cities` VALUES (536, 21, 1, 'Kabupaten Aceh Barat', 23681, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (537, 21, 2, 'Kabupaten Aceh Barat Daya', 23764, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (538, 21, 3, 'Kabupaten Aceh Besar', 23951, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (539, 21, 4, 'Kabupaten Aceh Jaya', 23654, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (540, 21, 5, 'Kabupaten Aceh Selatan', 23719, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (541, 21, 6, 'Kabupaten Aceh Singkil', 24785, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (542, 21, 7, 'Kabupaten Aceh Tamiang', 24476, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (543, 21, 8, 'Kabupaten Aceh Tengah', 24511, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (544, 21, 9, 'Kabupaten Aceh Tenggara', 24611, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (545, 21, 10, 'Kabupaten Aceh Timur', 24454, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (546, 21, 11, 'Kabupaten Aceh Utara', 24382, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (547, 32, 12, 'Kabupaten Agam', 26411, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (548, 23, 13, 'Kabupaten Alor', 85811, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (549, 19, 14, 'Kota Ambon', 97222, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (550, 34, 15, 'Kabupaten Asahan', 21214, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (551, 24, 16, 'Kabupaten Asmat', 99777, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (552, 1, 17, 'Kabupaten Badung', 80351, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (553, 13, 18, 'Kabupaten Balangan', 71611, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (554, 15, 19, 'Kota Balikpapan', 76111, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (555, 21, 20, 'Kota Banda Aceh', 23238, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (556, 18, 21, 'Kota Bandar Lampung', 35139, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (557, 9, 22, 'Kabupaten Bandung', 40311, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (558, 9, 23, 'Kota Bandung', 40111, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (559, 9, 24, 'Kabupaten Bandung Barat', 40721, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (560, 29, 25, 'Kabupaten Banggai', 94711, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (561, 29, 26, 'Kabupaten Banggai Kepulauan', 94881, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (562, 2, 27, 'Kabupaten Bangka', 33212, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (563, 2, 28, 'Kabupaten Bangka Barat', 33315, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (564, 2, 29, 'Kabupaten Bangka Selatan', 33719, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (565, 2, 30, 'Kabupaten Bangka Tengah', 33613, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (566, 11, 31, 'Kabupaten Bangkalan', 69118, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (567, 1, 32, 'Kabupaten Bangli', 80619, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (568, 13, 33, 'Kabupaten Banjar', 70619, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (569, 9, 34, 'Kota Banjar', 46311, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (570, 13, 35, 'Kota Banjarbaru', 70712, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (571, 13, 36, 'Kota Banjarmasin', 70117, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (572, 10, 37, 'Kabupaten Banjarnegara', 53419, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (573, 28, 38, 'Kabupaten Bantaeng', 92411, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (574, 5, 39, 'Kabupaten Bantul', 55715, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (575, 33, 40, 'Kabupaten Banyuasin', 30911, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (576, 10, 41, 'Kabupaten Banyumas', 53114, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (577, 11, 42, 'Kabupaten Banyuwangi', 68416, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (578, 13, 43, 'Kabupaten Barito Kuala', 70511, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (579, 14, 44, 'Kabupaten Barito Selatan', 73711, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (580, 14, 45, 'Kabupaten Barito Timur', 73671, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (581, 14, 46, 'Kabupaten Barito Utara', 73881, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (582, 28, 47, 'Kabupaten Barru', 90719, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (583, 17, 48, 'Kota Batam', 29413, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (584, 10, 49, 'Kabupaten Batang', 51211, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (585, 8, 50, 'Kabupaten Batang Hari', 36613, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (586, 11, 51, 'Kota Batu', 65311, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (587, 34, 52, 'Kabupaten Batu Bara', 21655, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (588, 30, 53, 'Kota Bau-Bau', 93719, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (589, 9, 54, 'Kabupaten Bekasi', 17837, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (590, 9, 55, 'Kota Bekasi', 17121, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (591, 2, 56, 'Kabupaten Belitung', 33419, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (592, 2, 57, 'Kabupaten Belitung Timur', 33519, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (593, 23, 58, 'Kabupaten Belu', 85711, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (594, 21, 59, 'Kabupaten Bener Meriah', 24581, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (595, 26, 60, 'Kabupaten Bengkalis', 28719, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (596, 12, 61, 'Kabupaten Bengkayang', 79213, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (597, 4, 62, 'Kota Bengkulu', 38229, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (598, 4, 63, 'Kabupaten Bengkulu Selatan', 38519, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (599, 4, 64, 'Kabupaten Bengkulu Tengah', 38319, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (600, 4, 65, 'Kabupaten Bengkulu Utara', 38619, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (601, 15, 66, 'Kabupaten Berau', 77311, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (602, 24, 67, 'Kabupaten Biak Numfor', 98119, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (603, 22, 68, 'Kabupaten Bima', 84171, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (604, 22, 69, 'Kota Bima', 84139, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (605, 34, 70, 'Kota Binjai', 20712, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (606, 17, 71, 'Kabupaten Bintan', 29135, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (607, 21, 72, 'Kabupaten Bireuen', 24219, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (608, 31, 73, 'Kota Bitung', 95512, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (609, 11, 74, 'Kabupaten Blitar', 66171, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (610, 11, 75, 'Kota Blitar', 66124, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (611, 10, 76, 'Kabupaten Blora', 58219, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (612, 7, 77, 'Kabupaten Boalemo', 96319, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (613, 9, 78, 'Kabupaten Bogor', 16911, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (614, 9, 79, 'Kota Bogor', 16119, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (615, 11, 80, 'Kabupaten Bojonegoro', 62119, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (616, 31, 81, 'Kabupaten Bolaang Mongondow (Bolmong)', 95755, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (617, 31, 82, 'Kabupaten Bolaang Mongondow Selatan', 95774, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (618, 31, 83, 'Kabupaten Bolaang Mongondow Timur', 95783, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (619, 31, 84, 'Kabupaten Bolaang Mongondow Utara', 95765, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (620, 30, 85, 'Kabupaten Bombana', 93771, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (621, 11, 86, 'Kabupaten Bondowoso', 68219, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (622, 28, 87, 'Kabupaten Bone', 92713, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (623, 7, 88, 'Kabupaten Bone Bolango', 96511, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (624, 15, 89, 'Kota Bontang', 75313, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (625, 24, 90, 'Kabupaten Boven Digoel', 99662, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (626, 10, 91, 'Kabupaten Boyolali', 57312, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (627, 10, 92, 'Kabupaten Brebes', 52212, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (628, 32, 93, 'Kota Bukittinggi', 26115, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (629, 1, 94, 'Kabupaten Buleleng', 81111, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (630, 28, 95, 'Kabupaten Bulukumba', 92511, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (631, 16, 96, 'Kabupaten Bulungan (Bulongan)', 77211, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (632, 8, 97, 'Kabupaten Bungo', 37216, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (633, 29, 98, 'Kabupaten Buol', 94564, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (634, 19, 99, 'Kabupaten Buru', 97371, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (635, 19, 100, 'Kabupaten Buru Selatan', 97351, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (636, 30, 101, 'Kabupaten Buton', 93754, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (637, 30, 102, 'Kabupaten Buton Utara', 93745, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (638, 9, 103, 'Kabupaten Ciamis', 46211, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (639, 9, 104, 'Kabupaten Cianjur', 43217, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (640, 10, 105, 'Kabupaten Cilacap', 53211, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (641, 3, 106, 'Kota Cilegon', 42417, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (642, 9, 107, 'Kota Cimahi', 40512, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (643, 9, 108, 'Kabupaten Cirebon', 45611, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (644, 9, 109, 'Kota Cirebon', 45116, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (645, 34, 110, 'Kabupaten Dairi', 22211, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (646, 24, 111, 'Kabupaten Deiyai (Deliyai)', 98784, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (647, 34, 112, 'Kabupaten Deli Serdang', 20511, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (648, 10, 113, 'Kabupaten Demak', 59519, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (649, 1, 114, 'Kota Denpasar', 80227, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (650, 9, 115, 'Kota Depok', 16416, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (651, 32, 116, 'Kabupaten Dharmasraya', 27612, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (652, 24, 117, 'Kabupaten Dogiyai', 98866, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (653, 22, 118, 'Kabupaten Dompu', 84217, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (654, 29, 119, 'Kabupaten Donggala', 94341, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (655, 26, 120, 'Kota Dumai', 28811, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (656, 33, 121, 'Kabupaten Empat Lawang', 31811, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (657, 23, 122, 'Kabupaten Ende', 86351, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (658, 28, 123, 'Kabupaten Enrekang', 91719, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (659, 25, 124, 'Kabupaten Fakfak', 98651, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (660, 23, 125, 'Kabupaten Flores Timur', 86213, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (661, 9, 126, 'Kabupaten Garut', 44126, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (662, 21, 127, 'Kabupaten Gayo Lues', 24653, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (663, 1, 128, 'Kabupaten Gianyar', 80519, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (664, 7, 129, 'Kabupaten Gorontalo', 96218, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (665, 7, 130, 'Kota Gorontalo', 96115, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (666, 7, 131, 'Kabupaten Gorontalo Utara', 96611, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (667, 28, 132, 'Kabupaten Gowa', 92111, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (668, 11, 133, 'Kabupaten Gresik', 61115, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (669, 10, 134, 'Kabupaten Grobogan', 58111, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (670, 5, 135, 'Kabupaten Gunung Kidul', 55812, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (671, 14, 136, 'Kabupaten Gunung Mas', 74511, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (672, 34, 137, 'Kota Gunungsitoli', 22813, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (673, 20, 138, 'Kabupaten Halmahera Barat', 97757, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (674, 20, 139, 'Kabupaten Halmahera Selatan', 97911, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (675, 20, 140, 'Kabupaten Halmahera Tengah', 97853, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (676, 20, 141, 'Kabupaten Halmahera Timur', 97862, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (677, 20, 142, 'Kabupaten Halmahera Utara', 97762, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (678, 13, 143, 'Kabupaten Hulu Sungai Selatan', 71212, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (679, 13, 144, 'Kabupaten Hulu Sungai Tengah', 71313, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (680, 13, 145, 'Kabupaten Hulu Sungai Utara', 71419, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (681, 34, 146, 'Kabupaten Humbang Hasundutan', 22457, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (682, 26, 147, 'Kabupaten Indragiri Hilir', 29212, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (683, 26, 148, 'Kabupaten Indragiri Hulu', 29319, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (684, 9, 149, 'Kabupaten Indramayu', 45214, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (685, 24, 150, 'Kabupaten Intan Jaya', 98771, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (686, 6, 151, 'Kota Jakarta Barat', 11220, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (687, 6, 152, 'Kota Jakarta Pusat', 10540, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (688, 6, 153, 'Kota Jakarta Selatan', 12230, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (689, 6, 154, 'Kota Jakarta Timur', 13330, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (690, 6, 155, 'Kota Jakarta Utara', 14140, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (691, 8, 156, 'Kota Jambi', 36111, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (692, 24, 157, 'Kabupaten Jayapura', 99352, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (693, 24, 158, 'Kota Jayapura', 99114, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (694, 24, 159, 'Kabupaten Jayawijaya', 99511, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (695, 11, 160, 'Kabupaten Jember', 68113, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (696, 1, 161, 'Kabupaten Jembrana', 82251, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (697, 28, 162, 'Kabupaten Jeneponto', 92319, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (698, 10, 163, 'Kabupaten Jepara', 59419, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (699, 11, 164, 'Kabupaten Jombang', 61415, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (700, 25, 165, 'Kabupaten Kaimana', 98671, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (701, 26, 166, 'Kabupaten Kampar', 28411, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (702, 14, 167, 'Kabupaten Kapuas', 73583, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (703, 12, 168, 'Kabupaten Kapuas Hulu', 78719, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (704, 10, 169, 'Kabupaten Karanganyar', 57718, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (705, 1, 170, 'Kabupaten Karangasem', 80819, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (706, 9, 171, 'Kabupaten Karawang', 41311, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (707, 17, 172, 'Kabupaten Karimun', 29611, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (708, 34, 173, 'Kabupaten Karo', 22119, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (709, 14, 174, 'Kabupaten Katingan', 74411, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (710, 4, 175, 'Kabupaten Kaur', 38911, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (711, 12, 176, 'Kabupaten Kayong Utara', 78852, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (712, 10, 177, 'Kabupaten Kebumen', 54319, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (713, 11, 178, 'Kabupaten Kediri', 64184, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (714, 11, 179, 'Kota Kediri', 64125, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (715, 24, 180, 'Kabupaten Keerom', 99461, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (716, 10, 181, 'Kabupaten Kendal', 51314, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (717, 30, 182, 'Kota Kendari', 93126, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (718, 4, 183, 'Kabupaten Kepahiang', 39319, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (719, 17, 184, 'Kabupaten Kepulauan Anambas', 29991, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (720, 19, 185, 'Kabupaten Kepulauan Aru', 97681, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (721, 32, 186, 'Kabupaten Kepulauan Mentawai', 25771, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (722, 26, 187, 'Kabupaten Kepulauan Meranti', 28791, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (723, 31, 188, 'Kabupaten Kepulauan Sangihe', 95819, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (724, 6, 189, 'Kabupaten Kepulauan Seribu', 14550, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (725, 31, 190, 'Kabupaten Kepulauan Siau Tagulandang Biaro (Sitaro)', 95862, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (726, 20, 191, 'Kabupaten Kepulauan Sula', 97995, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (727, 31, 192, 'Kabupaten Kepulauan Talaud', 95885, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (728, 24, 193, 'Kabupaten Kepulauan Yapen (Yapen Waropen)', 98211, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (729, 8, 194, 'Kabupaten Kerinci', 37167, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (730, 12, 195, 'Kabupaten Ketapang', 78874, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (731, 10, 196, 'Kabupaten Klaten', 57411, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (732, 1, 197, 'Kabupaten Klungkung', 80719, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (733, 30, 198, 'Kabupaten Kolaka', 93511, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (734, 30, 199, 'Kabupaten Kolaka Utara', 93911, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (735, 30, 200, 'Kabupaten Konawe', 93411, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (736, 30, 201, 'Kabupaten Konawe Selatan', 93811, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (737, 30, 202, 'Kabupaten Konawe Utara', 93311, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (738, 13, 203, 'Kabupaten Kotabaru', 72119, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (739, 31, 204, 'Kota Kotamobagu', 95711, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (740, 14, 205, 'Kabupaten Kotawaringin Barat', 74119, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (741, 14, 206, 'Kabupaten Kotawaringin Timur', 74364, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (742, 26, 207, 'Kabupaten Kuantan Singingi', 29519, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (743, 12, 208, 'Kabupaten Kubu Raya', 78311, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (744, 10, 209, 'Kabupaten Kudus', 59311, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (745, 5, 210, 'Kabupaten Kulon Progo', 55611, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (746, 9, 211, 'Kabupaten Kuningan', 45511, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (747, 23, 212, 'Kabupaten Kupang', 85362, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (748, 23, 213, 'Kota Kupang', 85119, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (749, 15, 214, 'Kabupaten Kutai Barat', 75711, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (750, 15, 215, 'Kabupaten Kutai Kartanegara', 75511, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (751, 15, 216, 'Kabupaten Kutai Timur', 75611, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (752, 34, 217, 'Kabupaten Labuhan Batu', 21412, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (753, 34, 218, 'Kabupaten Labuhan Batu Selatan', 21511, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (754, 34, 219, 'Kabupaten Labuhan Batu Utara', 21711, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (755, 33, 220, 'Kabupaten Lahat', 31419, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (756, 14, 221, 'Kabupaten Lamandau', 74611, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (757, 11, 222, 'Kabupaten Lamongan', 64125, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (758, 18, 223, 'Kabupaten Lampung Barat', 34814, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (759, 18, 224, 'Kabupaten Lampung Selatan', 35511, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (760, 18, 225, 'Kabupaten Lampung Tengah', 34212, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (761, 18, 226, 'Kabupaten Lampung Timur', 34319, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (762, 18, 227, 'Kabupaten Lampung Utara', 34516, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (763, 12, 228, 'Kabupaten Landak', 78319, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (764, 34, 229, 'Kabupaten Langkat', 20811, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (765, 21, 230, 'Kota Langsa', 24412, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (766, 24, 231, 'Kabupaten Lanny Jaya', 99531, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (767, 3, 232, 'Kabupaten Lebak', 42319, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (768, 4, 233, 'Kabupaten Lebong', 39264, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (769, 23, 234, 'Kabupaten Lembata', 86611, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (770, 21, 235, 'Kota Lhokseumawe', 24352, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (771, 32, 236, 'Kabupaten Lima Puluh Koto/Kota', 26671, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (772, 17, 237, 'Kabupaten Lingga', 29811, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (773, 22, 238, 'Kabupaten Lombok Barat', 83311, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (774, 22, 239, 'Kabupaten Lombok Tengah', 83511, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (775, 22, 240, 'Kabupaten Lombok Timur', 83612, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (776, 22, 241, 'Kabupaten Lombok Utara', 83711, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (777, 33, 242, 'Kota Lubuk Linggau', 31614, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (778, 11, 243, 'Kabupaten Lumajang', 67319, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (779, 28, 244, 'Kabupaten Luwu', 91994, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (780, 28, 245, 'Kabupaten Luwu Timur', 92981, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (781, 28, 246, 'Kabupaten Luwu Utara', 92911, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (782, 11, 247, 'Kabupaten Madiun', 63153, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (783, 11, 248, 'Kota Madiun', 63122, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (784, 10, 249, 'Kabupaten Magelang', 56519, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (785, 10, 250, 'Kota Magelang', 56133, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (786, 11, 251, 'Kabupaten Magetan', 63314, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (787, 9, 252, 'Kabupaten Majalengka', 45412, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (788, 27, 253, 'Kabupaten Majene', 91411, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (789, 28, 254, 'Kota Makassar', 90111, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (790, 11, 255, 'Kabupaten Malang', 65163, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (791, 11, 256, 'Kota Malang', 65112, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (792, 16, 257, 'Kabupaten Malinau', 77511, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (793, 19, 258, 'Kabupaten Maluku Barat Daya', 97451, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (794, 19, 259, 'Kabupaten Maluku Tengah', 97513, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (795, 19, 260, 'Kabupaten Maluku Tenggara', 97651, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (796, 19, 261, 'Kabupaten Maluku Tenggara Barat', 97465, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (797, 27, 262, 'Kabupaten Mamasa', 91362, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (798, 24, 263, 'Kabupaten Mamberamo Raya', 99381, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (799, 24, 264, 'Kabupaten Mamberamo Tengah', 99553, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (800, 27, 265, 'Kabupaten Mamuju', 91519, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (801, 27, 266, 'Kabupaten Mamuju Utara', 91571, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (802, 31, 267, 'Kota Manado', 95247, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (803, 34, 268, 'Kabupaten Mandailing Natal', 22916, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (804, 23, 269, 'Kabupaten Manggarai', 86551, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (805, 23, 270, 'Kabupaten Manggarai Barat', 86711, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (806, 23, 271, 'Kabupaten Manggarai Timur', 86811, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (807, 25, 272, 'Kabupaten Manokwari', 98311, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (808, 25, 273, 'Kabupaten Manokwari Selatan', 98355, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (809, 24, 274, 'Kabupaten Mappi', 99853, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (810, 28, 275, 'Kabupaten Maros', 90511, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (811, 22, 276, 'Kota Mataram', 83131, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (812, 25, 277, 'Kabupaten Maybrat', 98051, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (813, 34, 278, 'Kota Medan', 20228, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (814, 12, 279, 'Kabupaten Melawi', 78619, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (815, 8, 280, 'Kabupaten Merangin', 37319, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (816, 24, 281, 'Kabupaten Merauke', 99613, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (817, 18, 282, 'Kabupaten Mesuji', 34911, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (818, 18, 283, 'Kota Metro', 34111, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (819, 24, 284, 'Kabupaten Mimika', 99962, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (820, 31, 285, 'Kabupaten Minahasa', 95614, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (821, 31, 286, 'Kabupaten Minahasa Selatan', 95914, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (822, 31, 287, 'Kabupaten Minahasa Tenggara', 95995, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (823, 31, 288, 'Kabupaten Minahasa Utara', 95316, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (824, 11, 289, 'Kabupaten Mojokerto', 61382, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (825, 11, 290, 'Kota Mojokerto', 61316, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (826, 29, 291, 'Kabupaten Morowali', 94911, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (827, 33, 292, 'Kabupaten Muara Enim', 31315, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (828, 8, 293, 'Kabupaten Muaro Jambi', 36311, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (829, 4, 294, 'Kabupaten Muko Muko', 38715, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (830, 30, 295, 'Kabupaten Muna', 93611, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (831, 14, 296, 'Kabupaten Murung Raya', 73911, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (832, 33, 297, 'Kabupaten Musi Banyuasin', 30719, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (833, 33, 298, 'Kabupaten Musi Rawas', 31661, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (834, 24, 299, 'Kabupaten Nabire', 98816, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (835, 21, 300, 'Kabupaten Nagan Raya', 23674, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (836, 23, 301, 'Kabupaten Nagekeo', 86911, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (837, 17, 302, 'Kabupaten Natuna', 29711, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (838, 24, 303, 'Kabupaten Nduga', 99541, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (839, 23, 304, 'Kabupaten Ngada', 86413, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (840, 11, 305, 'Kabupaten Nganjuk', 64414, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (841, 11, 306, 'Kabupaten Ngawi', 63219, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (842, 34, 307, 'Kabupaten Nias', 22876, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (843, 34, 308, 'Kabupaten Nias Barat', 22895, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (844, 34, 309, 'Kabupaten Nias Selatan', 22865, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (845, 34, 310, 'Kabupaten Nias Utara', 22856, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (846, 16, 311, 'Kabupaten Nunukan', 77421, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (847, 33, 312, 'Kabupaten Ogan Ilir', 30811, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (848, 33, 313, 'Kabupaten Ogan Komering Ilir', 30618, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (849, 33, 314, 'Kabupaten Ogan Komering Ulu', 32112, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (850, 33, 315, 'Kabupaten Ogan Komering Ulu Selatan', 32211, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (851, 33, 316, 'Kabupaten Ogan Komering Ulu Timur', 32312, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (852, 11, 317, 'Kabupaten Pacitan', 63512, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (853, 32, 318, 'Kota Padang', 25112, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (854, 34, 319, 'Kabupaten Padang Lawas', 22763, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (855, 34, 320, 'Kabupaten Padang Lawas Utara', 22753, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (856, 32, 321, 'Kota Padang Panjang', 27122, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (857, 32, 322, 'Kabupaten Padang Pariaman', 25583, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (858, 34, 323, 'Kota Padang Sidempuan', 22727, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (859, 33, 324, 'Kota Pagar Alam', 31512, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (860, 34, 325, 'Kabupaten Pakpak Bharat', 22272, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (861, 14, 326, 'Kota Palangka Raya', 73112, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (862, 33, 327, 'Kota Palembang', 31512, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (863, 28, 328, 'Kota Palopo', 91911, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (864, 29, 329, 'Kota Palu', 94111, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (865, 11, 330, 'Kabupaten Pamekasan', 69319, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (866, 3, 331, 'Kabupaten Pandeglang', 42212, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (867, 9, 332, 'Kabupaten Pangandaran', 46511, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (868, 28, 333, 'Kabupaten Pangkajene Kepulauan', 90611, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (869, 2, 334, 'Kota Pangkal Pinang', 33115, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (870, 24, 335, 'Kabupaten Paniai', 98765, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (871, 28, 336, 'Kota Parepare', 91123, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (872, 32, 337, 'Kota Pariaman', 25511, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (873, 29, 338, 'Kabupaten Parigi Moutong', 94411, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (874, 32, 339, 'Kabupaten Pasaman', 26318, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (875, 32, 340, 'Kabupaten Pasaman Barat', 26511, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (876, 15, 341, 'Kabupaten Paser', 76211, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (877, 11, 342, 'Kabupaten Pasuruan', 67153, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (878, 11, 343, 'Kota Pasuruan', 67118, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (879, 10, 344, 'Kabupaten Pati', 59114, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (880, 32, 345, 'Kota Payakumbuh', 26213, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (881, 25, 346, 'Kabupaten Pegunungan Arfak', 98354, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (882, 24, 347, 'Kabupaten Pegunungan Bintang', 99573, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (883, 10, 348, 'Kabupaten Pekalongan', 51161, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (884, 10, 349, 'Kota Pekalongan', 51122, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (885, 26, 350, 'Kota Pekanbaru', 28112, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (886, 26, 351, 'Kabupaten Pelalawan', 28311, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (887, 10, 352, 'Kabupaten Pemalang', 52319, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (888, 34, 353, 'Kota Pematang Siantar', 21126, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (889, 15, 354, 'Kabupaten Penajam Paser Utara', 76311, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (890, 18, 355, 'Kabupaten Pesawaran', 35312, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (891, 18, 356, 'Kabupaten Pesisir Barat', 35974, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (892, 32, 357, 'Kabupaten Pesisir Selatan', 25611, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (893, 21, 358, 'Kabupaten Pidie', 24116, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (894, 21, 359, 'Kabupaten Pidie Jaya', 24186, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (895, 28, 360, 'Kabupaten Pinrang', 91251, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (896, 7, 361, 'Kabupaten Pohuwato', 96419, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (897, 27, 362, 'Kabupaten Polewali Mandar', 91311, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (898, 11, 363, 'Kabupaten Ponorogo', 63411, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (899, 12, 364, 'Kabupaten Pontianak', 78971, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (900, 12, 365, 'Kota Pontianak', 78112, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (901, 29, 366, 'Kabupaten Poso', 94615, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (902, 33, 367, 'Kota Prabumulih', 31121, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (903, 18, 368, 'Kabupaten Pringsewu', 35719, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (904, 11, 369, 'Kabupaten Probolinggo', 67282, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (905, 11, 370, 'Kota Probolinggo', 67215, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (906, 14, 371, 'Kabupaten Pulang Pisau', 74811, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (907, 20, 372, 'Kabupaten Pulau Morotai', 97771, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (908, 24, 373, 'Kabupaten Puncak', 98981, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (909, 24, 374, 'Kabupaten Puncak Jaya', 98979, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (910, 10, 375, 'Kabupaten Purbalingga', 53312, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (911, 9, 376, 'Kabupaten Purwakarta', 41119, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (912, 10, 377, 'Kabupaten Purworejo', 54111, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (913, 25, 378, 'Kabupaten Raja Ampat', 98489, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (914, 4, 379, 'Kabupaten Rejang Lebong', 39112, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (915, 10, 380, 'Kabupaten Rembang', 59219, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (916, 26, 381, 'Kabupaten Rokan Hilir', 28992, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (917, 26, 382, 'Kabupaten Rokan Hulu', 28511, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (918, 23, 383, 'Kabupaten Rote Ndao', 85982, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (919, 21, 384, 'Kota Sabang', 23512, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (920, 23, 385, 'Kabupaten Sabu Raijua', 85391, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (921, 10, 386, 'Kota Salatiga', 50711, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (922, 15, 387, 'Kota Samarinda', 75133, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (923, 12, 388, 'Kabupaten Sambas', 79453, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (924, 34, 389, 'Kabupaten Samosir', 22392, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (925, 11, 390, 'Kabupaten Sampang', 69219, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (926, 12, 391, 'Kabupaten Sanggau', 78557, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (927, 24, 392, 'Kabupaten Sarmi', 99373, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (928, 8, 393, 'Kabupaten Sarolangun', 37419, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (929, 32, 394, 'Kota Sawah Lunto', 27416, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (930, 12, 395, 'Kabupaten Sekadau', 79583, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (931, 28, 396, 'Kabupaten Selayar (Kepulauan Selayar)', 92812, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (932, 4, 397, 'Kabupaten Seluma', 38811, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (933, 10, 398, 'Kabupaten Semarang', 50511, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (934, 10, 399, 'Kota Semarang', 50135, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (935, 19, 400, 'Kabupaten Seram Bagian Barat', 97561, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (936, 19, 401, 'Kabupaten Seram Bagian Timur', 97581, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (937, 3, 402, 'Kabupaten Serang', 42182, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (938, 3, 403, 'Kota Serang', 42111, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (939, 34, 404, 'Kabupaten Serdang Bedagai', 20915, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (940, 14, 405, 'Kabupaten Seruyan', 74211, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (941, 26, 406, 'Kabupaten Siak', 28623, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (942, 34, 407, 'Kota Sibolga', 22522, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (943, 28, 408, 'Kabupaten Sidenreng Rappang/Rapang', 91613, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (944, 11, 409, 'Kabupaten Sidoarjo', 61219, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (945, 29, 410, 'Kabupaten Sigi', 94364, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (946, 32, 411, 'Kabupaten Sijunjung (Sawah Lunto Sijunjung)', 27511, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (947, 23, 412, 'Kabupaten Sikka', 86121, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (948, 34, 413, 'Kabupaten Simalungun', 21162, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (949, 21, 414, 'Kabupaten Simeulue', 23891, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (950, 12, 415, 'Kota Singkawang', 79117, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (951, 28, 416, 'Kabupaten Sinjai', 92615, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (952, 12, 417, 'Kabupaten Sintang', 78619, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (953, 11, 418, 'Kabupaten Situbondo', 68316, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (954, 5, 419, 'Kabupaten Sleman', 55513, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (955, 32, 420, 'Kabupaten Solok', 27365, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (956, 32, 421, 'Kota Solok', 27315, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (957, 32, 422, 'Kabupaten Solok Selatan', 27779, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (958, 28, 423, 'Kabupaten Soppeng', 90812, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (959, 25, 424, 'Kabupaten Sorong', 98431, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (960, 25, 425, 'Kota Sorong', 98411, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (961, 25, 426, 'Kabupaten Sorong Selatan', 98454, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (962, 10, 427, 'Kabupaten Sragen', 57211, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (963, 9, 428, 'Kabupaten Subang', 41215, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (964, 21, 429, 'Kota Subulussalam', 24882, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (965, 9, 430, 'Kabupaten Sukabumi', 43311, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (966, 9, 431, 'Kota Sukabumi', 43114, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (967, 14, 432, 'Kabupaten Sukamara', 74712, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (968, 10, 433, 'Kabupaten Sukoharjo', 57514, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (969, 23, 434, 'Kabupaten Sumba Barat', 87219, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (970, 23, 435, 'Kabupaten Sumba Barat Daya', 87453, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (971, 23, 436, 'Kabupaten Sumba Tengah', 87358, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (972, 23, 437, 'Kabupaten Sumba Timur', 87112, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (973, 22, 438, 'Kabupaten Sumbawa', 84315, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (974, 22, 439, 'Kabupaten Sumbawa Barat', 84419, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (975, 9, 440, 'Kabupaten Sumedang', 45326, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (976, 11, 441, 'Kabupaten Sumenep', 69413, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (977, 8, 442, 'Kota Sungaipenuh', 37113, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (978, 24, 443, 'Kabupaten Supiori', 98164, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (979, 11, 444, 'Kota Surabaya', 60119, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (980, 10, 445, 'Kota Surakarta (Solo)', 57113, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (981, 13, 446, 'Kabupaten Tabalong', 71513, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (982, 1, 447, 'Kabupaten Tabanan', 82119, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (983, 28, 448, 'Kabupaten Takalar', 92212, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (984, 25, 449, 'Kabupaten Tambrauw', 98475, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (985, 16, 450, 'Kabupaten Tana Tidung', 77611, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (986, 28, 451, 'Kabupaten Tana Toraja', 91819, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (987, 13, 452, 'Kabupaten Tanah Bumbu', 72211, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (988, 32, 453, 'Kabupaten Tanah Datar', 27211, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (989, 13, 454, 'Kabupaten Tanah Laut', 70811, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (990, 3, 455, 'Kabupaten Tangerang', 15914, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (991, 3, 456, 'Kota Tangerang', 15111, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (992, 3, 457, 'Kota Tangerang Selatan', 15332, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (993, 18, 458, 'Kabupaten Tanggamus', 35619, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (994, 34, 459, 'Kota Tanjung Balai', 21321, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (995, 8, 460, 'Kabupaten Tanjung Jabung Barat', 36513, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (996, 8, 461, 'Kabupaten Tanjung Jabung Timur', 36719, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (997, 17, 462, 'Kota Tanjung Pinang', 29111, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (998, 34, 463, 'Kabupaten Tapanuli Selatan', 22742, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (999, 34, 464, 'Kabupaten Tapanuli Tengah', 22611, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (1000, 34, 465, 'Kabupaten Tapanuli Utara', 22414, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (1001, 13, 466, 'Kabupaten Tapin', 71119, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (1002, 16, 467, 'Kota Tarakan', 77114, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (1003, 9, 468, 'Kabupaten Tasikmalaya', 46411, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (1004, 9, 469, 'Kota Tasikmalaya', 46116, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (1005, 34, 470, 'Kota Tebing Tinggi', 20632, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (1006, 8, 471, 'Kabupaten Tebo', 37519, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (1007, 10, 472, 'Kabupaten Tegal', 52419, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (1008, 10, 473, 'Kota Tegal', 52114, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (1009, 25, 474, 'Kabupaten Teluk Bintuni', 98551, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (1010, 25, 475, 'Kabupaten Teluk Wondama', 98591, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (1011, 10, 476, 'Kabupaten Temanggung', 56212, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (1012, 20, 477, 'Kota Ternate', 97714, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (1013, 20, 478, 'Kota Tidore Kepulauan', 97815, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (1014, 23, 479, 'Kabupaten Timor Tengah Selatan', 85562, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (1015, 23, 480, 'Kabupaten Timor Tengah Utara', 85612, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (1016, 34, 481, 'Kabupaten Toba Samosir', 22316, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (1017, 29, 482, 'Kabupaten Tojo Una-Una', 94683, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (1018, 29, 483, 'Kabupaten Toli-Toli', 94542, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (1019, 24, 484, 'Kabupaten Tolikara', 99411, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (1020, 31, 485, 'Kota Tomohon', 95416, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (1021, 28, 486, 'Kabupaten Toraja Utara', 91831, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (1022, 11, 487, 'Kabupaten Trenggalek', 66312, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (1023, 19, 488, 'Kota Tual', 97612, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (1024, 11, 489, 'Kabupaten Tuban', 62319, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (1025, 18, 490, 'Kabupaten Tulang Bawang', 34613, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (1026, 18, 491, 'Kabupaten Tulang Bawang Barat', 34419, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (1027, 11, 492, 'Kabupaten Tulungagung', 66212, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (1028, 28, 493, 'Kabupaten Wajo', 90911, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (1029, 30, 494, 'Kabupaten Wakatobi', 93791, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (1030, 24, 495, 'Kabupaten Waropen', 98269, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (1031, 18, 496, 'Kabupaten Way Kanan', 34711, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (1032, 10, 497, 'Kabupaten Wonogiri', 57619, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (1033, 10, 498, 'Kabupaten Wonosobo', 56311, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (1034, 24, 499, 'Kabupaten Yahukimo', 99041, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (1035, 24, 500, 'Kabupaten Yalimo', 99481, '2021-11-12 12:44:51', '2021-11-12 12:44:51');
INSERT INTO `cities` VALUES (1036, 5, 501, 'Kota Yogyakarta', 55222, '2021-11-12 12:44:51', '2021-11-12 12:44:51');

-- ----------------------------
-- Table structure for cm_email_subscribers
-- ----------------------------
DROP TABLE IF EXISTS `cm_email_subscribers`;
CREATE TABLE `cm_email_subscribers`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip_address` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cm_email_subscribers
-- ----------------------------

-- ----------------------------
-- Table structure for cm_faqs
-- ----------------------------
DROP TABLE IF EXISTS `cm_faqs`;
CREATE TABLE `cm_faqs`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `question` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `answer` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cm_faqs
-- ----------------------------
INSERT INTO `cm_faqs` VALUES (1, 'a:1:{s:9:\"Indonesia\";s:18:\"Apa itu Ngelapak ?\";}', 'a:1:{s:9:\"Indonesia\";s:340:\"<p>Perusahaan pengembang aplikasi seluler berbasis web maupun mobile. Spesialisasi dalam pembuatan dan perancangan WebbApp dengan menekankan pada pemasaran digital (digital marketing), dan profil perusahaan (company profile). Tujuan Ngelapak ingin membantu membangun skalabilitas produk UMKM untuk berkembang tanpa terbebani oleh biaya.</p>\";}', '2021-10-28 18:53:47', '2021-10-28 18:53:47');
INSERT INTO `cm_faqs` VALUES (2, 'a:1:{s:9:\"Indonesia\";s:44:\"Apa kelebihan membuat aplikasi di Ngelapak ?\";}', 'a:1:{s:9:\"Indonesia\";s:269:\"<p>Fitur unggulan dalam aplikasi, adanya fitur promosi &amp; penjualan otomatis (auto selling platform), dan toko online autopilot (independent e-commerce). Selain itu, pembuatan aplikasi Ngelapak bisa dipublikasi di Google Play Store (Android) dan App Store (iOS).</p>\";}', '2021-10-28 18:54:18', '2021-10-28 18:54:18');
INSERT INTO `cm_faqs` VALUES (3, 'a:1:{s:9:\"Indonesia\";s:45:\"Bagaimana cara membuat aplikasi di Ngelapak ?\";}', 'a:1:{s:9:\"Indonesia\";s:200:\"<p>Bisa mengikuti sesuai panduan dalam video&nbsp;<a id=\"headerVideoLink\" href=\"https://ngelapak.co.id/faq#headerPopup\" target=\"_blank\" rel=\"noopener\">Tutorial</a>&nbsp;di halaman website Ngelapak</p>\";}', '2021-10-28 18:55:02', '2021-10-28 18:55:02');
INSERT INTO `cm_faqs` VALUES (4, 'a:1:{s:9:\"Indonesia\";s:77:\"Apa itu akses mandiri pembuatan aplikasi (self service access builder apps) ?\";}', 'a:1:{s:9:\"Indonesia\";s:409:\"<div class=\"accordion-item\">\r\n<div id=\"collapseFour\" class=\"accordion-collapse collapse show\" aria-labelledby=\"headingFour\" data-bs-parent=\"#accordionExample\">\r\n<div class=\"accordion-body\">Akses untuk Anda, pelaku usaha yang ingin berkreatifitas melalui desain toko online sesuai keinginan.</div>\r\n</div>\r\n</div>\r\n<div class=\"accordion-item\">\r\n<h2 id=\"headingFive\" class=\"accordion-header\">&nbsp;</h2>\r\n</div>\";}', '2021-10-28 18:55:59', '2021-10-28 18:55:59');
INSERT INTO `cm_faqs` VALUES (5, 'a:1:{s:9:\"Indonesia\";s:35:\"Apa saja paket produk di Ngelapak ?\";}', 'a:1:{s:9:\"Indonesia\";s:122:\"<p>Paket Ngelapak Basic, Pro, dan Advanced. Semua pilihan paket tersebut mendapatkan gratis pemakaian 14 hari pertama.</p>\";}', '2021-10-28 18:56:22', '2021-10-28 18:56:22');

-- ----------------------------
-- Table structure for cm_features
-- ----------------------------
DROP TABLE IF EXISTS `cm_features`;
CREATE TABLE `cm_features`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `icon` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cm_features
-- ----------------------------
INSERT INTO `cm_features` VALUES (1, 'icon icon-Life-Safer', 'Website Builder', 'Manage website directly from your browser', NULL, NULL);
INSERT INTO `cm_features` VALUES (2, 'icon icon-Duplicate-Window', 'Emails reminder', 'Get remiders before your subscription ends', NULL, NULL);
INSERT INTO `cm_features` VALUES (3, 'icon icon-Fingerprint', 'Support', 'Real time Chat with staffs, customers and private groups', NULL, NULL);
INSERT INTO `cm_features` VALUES (4, 'icon icon-Pantone', 'Online Payments', 'Accept Online Payments from different providers', NULL, NULL);
INSERT INTO `cm_features` VALUES (5, 'icon icon-Life-Safer', 'Website Builder', 'Manage website directly from your browser', NULL, NULL);
INSERT INTO `cm_features` VALUES (6, 'icon icon-Duplicate-Window', 'Emails reminder', 'Get remiders before your subscription ends', NULL, NULL);
INSERT INTO `cm_features` VALUES (7, 'icon icon-Fingerprint', 'Support', 'Real time Chat with staffs, customers and private groups', NULL, NULL);
INSERT INTO `cm_features` VALUES (8, 'icon icon-Pantone', 'Online Payments', 'Accept Online Payments from different providers', NULL, NULL);

-- ----------------------------
-- Table structure for companies
-- ----------------------------
DROP TABLE IF EXISTS `companies`;
CREATE TABLE `companies`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `business_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `kategori` int NOT NULL,
  `status` int UNSIGNED NOT NULL,
  `package_id` int NULL DEFAULT NULL,
  `package_type` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `membership_type` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `valid_to` date NOT NULL,
  `last_email` date NULL DEFAULT NULL,
  `websites_limit` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `recurring_transaction` varchar(3) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `online_payment` varchar(3) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `alamat` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `kec` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `kota` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `prov` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `kode_pos` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `cabang` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `deskripsi` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `slogan` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `ig` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `fb` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `web` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `self_serv` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `ft_prd1` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `ft_prd2` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `ft_prd3` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `ft_prd4` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `ft_prd5` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `splash` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `bg_apk` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `judul_camp` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `sub_judul` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `key_brand` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi_camp` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `cover` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `thumbnail` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo_camp` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `gambar_camp` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `progress` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 315 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of companies
-- ----------------------------
INSERT INTO `companies` VALUES (1, 'tes1', 0, 1, 3, 'yearly', 'member', '2022-10-14', NULL, '1', 'Yes', 'Yes', '2021-09-30 05:30:51', '2021-10-14 20:56:55', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (2, 'tes2', 0, 1, 6, 'yearly', 'trial', '2021-10-08', NULL, 'Unlimited', 'Yes', 'Yes', '2021-10-01 03:37:49', '2021-10-01 03:37:49', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (3, 'Tes sign up', 0, 1, 1, 'monthly', 'trial', '2021-10-11', NULL, '3', 'No', 'No', '2021-10-04 12:33:24', '2021-10-04 12:33:24', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (4, 'tesssss', 0, 1, 2, 'monthly', 'trial', '2021-10-11', NULL, '10', 'Yes', 'No', '2021-10-04 13:08:00', '2021-10-04 13:08:00', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (5, 'Polinema', 0, 1, 6, 'yearly', 'member', '2021-10-17', NULL, 'Unlimited', 'Yes', 'Yes', '2021-10-04 13:12:15', '2021-10-14 16:04:31', 'tes', '', 'Kediri', 'Jawa timur', '64211', 'ya', NULL, NULL, NULL, NULL, NULL, '', 'logo_1634199669.jpg', 'ftprd1_1634200377.png', 'ftprd2_1634200377.png', 'ftprd3_1634200377.png', 'ftprd4_1634200377.jpg', '', 'splash_1634201301.jpg', 'bg_apk_1634201223.jpg', 'jdul camp', 'sub', 'key', 'des', 'cover_1634202271.jpg', 'tmnail_1634202271.jpg', 'logocamp_1634202271.jpg', 'gbrcamp_1634202271.jpg', '');
INSERT INTO `companies` VALUES (6, NULL, 0, 1, NULL, NULL, 'trial', '2021-10-12', NULL, NULL, NULL, NULL, '2021-10-05 10:38:47', '2021-10-05 10:38:47', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (7, NULL, 0, 1, NULL, NULL, 'trial', '2021-10-12', NULL, NULL, NULL, NULL, '2021-10-05 10:42:26', '2021-10-05 10:42:26', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (8, NULL, 0, 1, NULL, NULL, 'trial', '2021-10-12', NULL, NULL, NULL, NULL, '2021-10-05 10:46:27', '2021-10-05 10:46:27', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (9, NULL, 0, 1, NULL, NULL, 'trial', '2021-10-12', NULL, NULL, NULL, NULL, '2021-10-05 10:48:23', '2021-10-05 10:48:23', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (10, NULL, 0, 1, NULL, NULL, 'trial', '2021-10-12', NULL, NULL, NULL, NULL, '2021-10-05 10:50:57', '2021-10-05 10:50:57', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (11, NULL, 0, 1, NULL, NULL, 'trial', '2021-10-12', NULL, NULL, NULL, NULL, '2021-10-05 10:57:39', '2021-10-05 10:57:39', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (12, NULL, 0, 1, NULL, NULL, 'trial', '2021-10-12', NULL, NULL, NULL, NULL, '2021-10-05 11:02:50', '2021-10-05 11:02:50', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (13, NULL, 0, 1, NULL, NULL, 'trial', '2021-10-12', NULL, NULL, NULL, NULL, '2021-10-05 11:10:19', '2021-10-05 11:10:19', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (14, NULL, 0, 1, NULL, NULL, 'trial', '2021-10-12', NULL, NULL, NULL, NULL, '2021-10-05 11:38:48', '2021-10-05 11:38:48', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (15, NULL, 0, 1, NULL, NULL, 'trial', '2021-10-12', NULL, NULL, NULL, NULL, '2021-10-05 11:43:22', '2021-10-05 11:43:22', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (16, NULL, 0, 1, NULL, NULL, 'trial', '2021-10-12', NULL, NULL, NULL, NULL, '2021-10-05 11:45:57', '2021-10-05 11:45:57', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (17, NULL, 0, 1, NULL, NULL, 'trial', '2021-10-13', NULL, NULL, NULL, NULL, '2021-10-06 10:12:05', '2021-10-06 10:12:05', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (18, NULL, 0, 1, NULL, NULL, 'trial', '2021-10-13', NULL, NULL, NULL, NULL, '2021-10-06 10:50:05', '2021-10-06 10:50:05', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (19, 'Tescoba3', 0, 1, 6, 'yearly', 'member', '2021-10-13', NULL, 'Unlimited', 'Yes', 'Yes', '2021-10-06 11:08:44', '2021-10-06 11:08:44', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (20, NULL, 0, 1, NULL, NULL, 'trial', '2021-10-13', NULL, NULL, NULL, NULL, '2021-10-06 13:39:54', '2021-10-06 13:39:54', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (21, NULL, 0, 1, NULL, NULL, 'trial', '2021-10-13', NULL, NULL, NULL, NULL, '2021-10-06 15:00:02', '2021-10-06 15:00:02', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (22, NULL, 0, 1, NULL, NULL, 'trial', '2021-10-13', NULL, NULL, NULL, NULL, '2021-10-06 15:08:47', '2021-10-06 15:08:47', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (23, NULL, 0, 1, NULL, NULL, 'trial', '2021-10-14', NULL, NULL, NULL, NULL, '2021-10-07 09:22:13', '2021-10-07 09:22:13', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (24, NULL, 0, 1, NULL, NULL, 'trial', '2021-10-14', NULL, NULL, NULL, NULL, '2021-10-07 09:56:56', '2021-10-07 09:56:56', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (25, NULL, 0, 1, NULL, NULL, 'trial', '2021-10-14', NULL, NULL, NULL, NULL, '2021-10-07 10:12:25', '2021-10-07 10:12:25', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (26, 'tes edit perusahaan', 0, 1, 3, NULL, 'member', '2022-10-21', NULL, '1', 'Yes', 'Yes', '2021-10-07 11:46:27', '2021-10-21 11:00:33', 'jalan2', '', 'kota', 'prov', '12121', 'ya', 'r', 'cobaa', 'ig', 'fb', 'web', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (27, NULL, 0, 1, NULL, NULL, 'trial', '2021-10-15', NULL, NULL, NULL, NULL, '2021-10-08 22:47:43', '2021-10-08 22:47:43', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (28, NULL, 0, 1, NULL, NULL, 'trial', '2021-10-18', NULL, NULL, NULL, NULL, '2021-10-11 11:11:15', '2021-10-11 11:11:15', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (29, 'coba', 0, 1, NULL, NULL, 'trial', '2021-10-18', NULL, NULL, NULL, NULL, '2021-10-11 15:49:01', '2021-10-19 11:22:32', 'Desa Pelem, Dusun Tempuran', '', 'Kediri', 'Jawa Timur', '64211', 'ya', 'tes', NULL, NULL, NULL, NULL, '', 'logo_1634617351.png', 'ftprd1_1634617351.png', '', '', '', '', 'splash_1634617351.jpg', 'bg_apk_1634617352.jpg', 'rse', 'se', 'sese', 'sese', 'cover_1634617352.jpg', 'tmnail_1634617352.jpg', 'logocamp_1634617352.png', 'gbrcamp_1634617352.jpg', '');
INSERT INTO `companies` VALUES (30, NULL, 0, 1, NULL, NULL, 'trial', '2021-10-18', NULL, NULL, NULL, NULL, '2021-10-11 15:52:18', '2021-10-11 15:52:18', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (31, 'coba', 0, 1, 4, 'monthly', 'trial', '2021-10-18', NULL, '3', 'No', 'No', '2021-10-11 16:03:23', '2021-10-11 16:22:55', 'coba', '', 'sby', 'jawa timur', '643221', 'ya', 'coba', NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (32, NULL, 0, 1, 4, 'monthly', 'trial', '2021-10-14', NULL, '1', 'No', 'No', '2021-10-11 16:48:50', '2021-10-11 16:48:50', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (33, NULL, 0, 1, 4, 'monthly', 'trial', '2021-10-15', NULL, '1', 'No', 'No', '2021-10-12 09:40:19', '2021-10-12 09:40:19', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (34, NULL, 0, 1, 4, 'monthly', 'member', '2021-10-15', NULL, '1', 'No', 'No', '2021-10-12 10:25:01', '2021-10-12 10:25:01', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (35, NULL, 0, 1, 4, 'monthly', 'trial', '2021-10-15', NULL, '1', 'No', 'No', '2021-10-12 13:59:36', '2021-10-12 13:59:36', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (37, NULL, 0, 1, 1, 'yearly', 'trial', '2021-10-17', NULL, '1', 'Yes', 'Yes', '2021-10-14 10:01:57', '2021-10-14 10:01:57', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (38, NULL, 0, 1, 1, 'monthly', 'trial', '2021-10-17', NULL, '1', 'Yes', 'Yes', '2021-10-14 11:16:32', '2021-10-14 11:16:32', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (39, 'ngelapak', 0, 1, 1, 'monthly', 'trial', '2021-10-17', NULL, '1', 'Yes', 'Yes', '2021-10-14 11:39:09', '2021-10-14 12:08:15', 'royal', '', 'surabaya', 'jawa timur', '60225', 'tidak', 'platform', 'aplikasi', NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (40, 'ngelapak', 0, 1, 1, 'monthly', 'trial', '2021-10-17', NULL, '1', 'Yes', 'Yes', '2021-10-14 14:59:29', '2021-10-14 16:24:16', 'Ruko Soho Royal Residence BS-10 No. 07', '', 'Surabaya', 'jawa timur', '60227', 'tidak', 'platform pembuatan aplikasi', 'milyaran aplikasi', NULL, NULL, NULL, '', 'logo_1634203456.png', 'ftprd1_1634203456.png', '', '', '', '', 'splash_1634203456.png', 'bg_apk_1634203456.png', 'lalla', 'lallla', 'lallla', 'lallala', 'cover_1634203456.png', 'tmnail_1634203456.png', 'logocamp_1634203456.png', 'gbrcamp_1634203456.png', '');
INSERT INTO `companies` VALUES (41, NULL, 0, 1, 1, 'monthly', 'trial', '2021-11-13', NULL, '1', 'Yes', 'Yes', '2021-10-14 18:37:44', '2021-10-14 18:37:44', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (42, NULL, 0, 1, 1, 'monthly', 'trial', '2021-11-13', NULL, '1', 'Yes', 'Yes', '2021-10-14 18:53:50', '2021-10-14 18:53:50', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (45, NULL, 0, 1, 2, 'monthly', 'member', '2021-12-13', NULL, '1', 'Yes', 'Yes', '2021-10-14 19:21:47', '2021-10-14 19:33:27', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (46, NULL, 0, 1, 3, 'yearly', 'member', '2021-12-13', NULL, '1', 'Yes', 'Yes', '2021-10-14 19:50:35', '2021-10-14 19:52:55', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (48, NULL, 0, 1, 1, 'monthly', 'member', '2021-12-14', NULL, '1', 'Yes', 'Yes', '2021-10-15 12:26:56', '2021-10-15 12:30:12', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (49, NULL, 0, 1, 1, 'monthly', 'member', '2021-12-14', NULL, '1', 'Yes', 'Yes', '2021-10-15 19:29:29', '2021-10-15 19:29:29', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (50, NULL, 0, 1, 1, 'monthly', 'member', '2021-12-14', NULL, '1', 'Yes', 'Yes', '2021-10-15 19:29:30', '2021-10-15 19:29:30', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (51, NULL, 0, 1, 3, 'monthly', 'member', '2021-12-14', NULL, '1', 'Yes', 'Yes', '2021-10-15 19:29:32', '2021-10-15 19:29:32', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (52, NULL, 0, 1, 1, 'monthly', 'member', '2021-12-14', NULL, '1', 'Yes', 'Yes', '2021-10-15 19:30:13', '2021-10-15 19:30:13', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (53, NULL, 0, 1, 1, 'monthly', 'member', '2021-12-14', NULL, '1', 'Yes', 'Yes', '2021-10-15 19:30:16', '2021-10-15 19:39:25', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (54, 'YORRI Eatery', 3, 1, 3, 'monthly', 'member', '2021-12-14', NULL, '1', 'Yes', 'Yes', '2021-10-15 19:30:20', '2021-10-19 12:34:48', 'Pandugo Baru XIII P1A', '', 'Surabaya', 'Jawa Timur', '60297', 'ya', 'YORRI adalah penyedia makanan dan minuman Vegan dan Gluten Free yang sehat, bergizi dan lezat, serta berkontribusi baik terhadap sesama, lingkungan dan hewan.', 'Love Yourself, Eat Healthy Food', 'yorrieatery', 'YORRI Eatery', NULL, '', 'logo_1634621685.png', 'ftprd1_1634621685.jpg', 'ftprd2_1634621686.jpg', 'ftprd3_1634621686.jpg', 'ftprd4_1634621686.jpg', '', 'splash_1634621686.png', 'bg_apk_1634621687.png', 'Healthy Foods and Drinks', 'Vegan & Gluten Free', 'Vegan & Gluten Free', 'YORRI Eatery menyediakan makanan dan minuman Vegan dan Gluten Free tanpa 4P (Pemanis, Penguat rasa / Perisa, Pewarna, dan Pengawet) dengan menggunakan bahan baku lokal yang bermutu dari petani lokal Indonesia.', 'cover_1634621687.png', 'tmnail_1634621687.png', 'logocamp_1634621687.png', 'gbrcamp_1634621687.png', '');
INSERT INTO `companies` VALUES (55, NULL, 0, 1, 1, 'monthly', 'member', '2021-12-14', NULL, '1', 'Yes', 'Yes', '2021-10-15 19:30:22', '2021-10-15 19:33:29', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (56, 'CV. Smart Batik Indonesia', 2, 1, 3, 'monthly', 'member', '2021-12-14', NULL, '1', 'Yes', 'Yes', '2021-10-15 19:30:31', '2021-12-20 11:02:10', 'Ketanggungan, WB 2/429, RT 057, RW 012, Kelurahan Wirobrajan, Kecamatan Wirobrajan', '', 'Yogyakarta', 'Daerah Istimewa Yogyakarta', '55252', 'tidak', 'CV. Smart Batik Indonesia memproduksi dan memasarkan produk batik buatan tangan dengan motif unik, seperti motif sains, teknologi, olahraga, musik, lingkungan, perikanan, pertanian, pariwisata, dan budaya. Perusahaan juga menyediakan layanan desain kustom untuk pembuatan seragam instansi, organisasi, atau komunitas.', 'Inovatif, Edukatif, dan Kontributif', '@smart_batik', 'Smart Batik Indonesia', 'www.smartbatikindonesia.com', '', 'logo_1634303135.png', 'ftprd1_1634303137.jpg', 'ftprd2_1634303137.jpg', 'ftprd3_1634303137.jpg', 'ftprd4_1634303137.jpg', '', 'splash_1634303137.png', 'bg_apk_1634303138.jpg', 'Produsen Batik Tematik Kustom untuk Instansi Pemerintah, Organisasi, dan Asosiasi', 'Batik Tematik', 'Batik Unik Handmade', 'CV. Smart Batik Indonesia merupakan perusahaan batik yang telah dipercaya puluhan instansi pemerintah dan asosiasi untuk mendesain batik kustom sesuai karakter masing-masing organisasi. Perusahaan juga menjual produk batik dengan motif-motif unik dan anti', 'cover_1634303140.png', 'tmnail_1634303142.png', 'logocamp_1634303148.png', 'gbrcamp_1634303149.jpg', 'Done');
INSERT INTO `companies` VALUES (57, NULL, 0, 1, 2, 'monthly', 'member', '2021-12-14', NULL, '1', 'Yes', 'Yes', '2021-10-15 19:32:02', '2021-10-15 19:47:31', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (58, NULL, 0, 1, 1, 'monthly', 'member', '2021-12-14', NULL, '1', 'Yes', 'Yes', '2021-10-15 19:32:03', '2021-10-15 19:35:15', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (59, NULL, 0, 1, 3, 'monthly', 'member', '2021-12-14', NULL, '1', 'Yes', 'Yes', '2021-10-15 19:33:14', '2021-10-15 19:33:14', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (60, NULL, 0, 1, 3, 'monthly', 'member', '2021-12-14', NULL, '1', 'Yes', 'Yes', '2021-10-15 19:35:12', '2021-10-15 19:35:12', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (61, NULL, 0, 1, 1, 'monthly', 'member', '2021-12-14', NULL, '1', 'Yes', 'Yes', '2021-10-15 19:38:47', '2021-10-15 19:38:47', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (62, NULL, 0, 1, 1, 'monthly', 'member', '2021-12-14', NULL, '1', 'Yes', 'Yes', '2021-10-15 19:39:52', '2021-10-15 19:39:52', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (63, NULL, 0, 1, 2, 'monthly', 'member', '2021-12-14', NULL, '1', 'Yes', 'Yes', '2021-10-15 19:51:22', '2021-10-15 19:56:45', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (64, NULL, 0, 1, 3, 'monthly', 'member', '2021-12-14', NULL, '1', 'Yes', 'Yes', '2021-10-15 19:54:29', '2021-10-15 19:54:29', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (65, NULL, 0, 1, 3, 'monthly', 'member', '2021-12-14', NULL, '1', 'Yes', 'Yes', '2021-10-15 20:05:12', '2021-10-15 20:05:12', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (66, 'Polinema', 0, 1, 3, 'monthly', 'member', '2022-10-21', NULL, '1', 'Yes', 'Yes', '2021-10-19 11:25:36', '2021-10-21 11:33:22', 'Desa Pelem, Dusun Tempuran', '', 'Kediri', 'Pare', '64211', NULL, '2', NULL, NULL, NULL, NULL, '', 'logo_1634620001.png', 'ftprd1_1634620001.jpg', 'ftprd2_1634620001.jpg', 'ftprd3_1634620004.jpg', 'ftprd4_1634620005.jpg', '', 'splash_1634620005.jpg', 'bg_apk_1634620005.jpg', 'dsd', 'sd', 'asd', 'asd', 'cover_1634620005.jpg', 'tmnail_1634620006.jpg', 'logocamp_1634620006.png', 'gbrcamp_1634620006.jpg', '');
INSERT INTO `companies` VALUES (67, 'Learning Qur\'an International', 6, 1, 3, 'monthly', 'member', '2022-01-04', NULL, '1', 'Yes', 'Yes', '2021-10-19 22:22:13', '2021-12-09 09:54:56', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (70, NULL, 0, 1, 1, 'monthly', 'trial', '2021-11-08', NULL, '1', 'Yes', 'Yes', '2021-10-25 10:21:20', '2021-10-25 10:21:20', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (71, NULL, 0, 1, 3, 'monthly', 'trial', '2021-11-08', NULL, '1', 'Yes', 'Yes', '2021-10-25 10:29:31', '2021-10-25 10:29:31', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (74, NULL, 0, 1, 1, 'monthly', 'trial', '2021-11-08', NULL, '1', 'Yes', 'Yes', '2021-10-25 10:37:48', '2021-10-25 10:37:48', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (77, NULL, 0, 1, 2, 'monthly', 'trial', '2021-11-08', NULL, '1', 'Yes', 'Yes', '2021-10-25 10:41:39', '2021-10-25 10:41:39', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (80, NULL, 0, 1, 1, 'yearly', 'trial', '2021-11-08', NULL, '1', 'Yes', 'Yes', '2021-10-25 10:43:33', '2021-10-25 10:43:33', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (81, NULL, 0, 1, 1, 'monthly', 'trial', '2021-11-08', NULL, '1', 'Yes', 'Yes', '2021-10-25 10:44:17', '2021-10-25 10:44:17', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (87, NULL, 0, 1, 1, 'monthly', 'trial', '2021-11-08', NULL, '1', 'Yes', 'Yes', '2021-10-25 10:51:08', '2021-10-25 10:51:08', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (90, NULL, 0, 1, 1, 'monthly', 'trial', '2021-11-08', NULL, '1', 'Yes', 'Yes', '2021-10-25 10:53:39', '2021-10-25 10:53:39', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (91, NULL, 0, 1, 1, 'monthly', 'trial', '2021-11-08', NULL, '1', 'Yes', 'Yes', '2021-10-25 10:54:45', '2021-10-25 10:54:45', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (92, NULL, 0, 1, 1, 'monthly', 'trial', '2021-11-08', NULL, '1', 'Yes', 'Yes', '2021-10-25 10:55:28', '2021-10-25 10:55:28', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (95, NULL, 0, 1, 1, 'monthly', 'trial', '2021-11-08', NULL, '1', 'Yes', 'Yes', '2021-10-25 10:56:40', '2021-10-25 10:56:40', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (98, NULL, 0, 1, 1, 'monthly', 'trial', '2021-11-08', NULL, '1', 'Yes', 'Yes', '2021-10-25 10:57:27', '2021-10-25 10:57:27', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (103, NULL, 0, 1, 1, 'monthly', 'trial', '2021-11-08', NULL, '1', 'Yes', 'Yes', '2021-10-25 10:59:47', '2021-10-25 10:59:47', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (106, NULL, 0, 1, 1, 'monthly', 'trial', '2021-11-08', NULL, '1', 'Yes', 'Yes', '2021-10-25 11:02:18', '2021-10-25 11:02:18', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (107, NULL, 0, 1, 1, 'monthly', 'trial', '2021-11-08', NULL, '1', 'Yes', 'Yes', '2021-10-25 11:05:27', '2021-10-25 11:05:27', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (109, NULL, 0, 1, 1, 'monthly', 'trial', '2021-11-08', NULL, '1', 'Yes', 'Yes', '2021-10-25 11:06:07', '2021-10-25 11:06:07', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (112, NULL, 0, 1, 1, 'monthly', 'trial', '2021-11-08', NULL, '1', 'Yes', 'Yes', '2021-10-25 11:07:41', '2021-10-25 11:07:41', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (113, NULL, 0, 1, 1, 'monthly', 'trial', '2021-11-08', NULL, '1', 'Yes', 'Yes', '2021-10-25 11:07:52', '2021-10-25 11:07:52', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (119, NULL, 0, 1, 1, 'monthly', 'trial', '2021-11-08', NULL, '1', 'Yes', 'Yes', '2021-10-25 11:12:28', '2021-10-25 11:12:28', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (120, NULL, 0, 1, 1, 'monthly', 'trial', '2021-11-08', NULL, '1', 'Yes', 'Yes', '2021-10-25 11:15:12', '2021-10-25 11:15:12', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (121, NULL, 0, 1, 1, 'monthly', 'trial', '2021-11-08', NULL, '1', 'Yes', 'Yes', '2021-10-25 11:15:25', '2021-10-25 11:15:25', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (125, NULL, 0, 1, 1, 'monthly', 'trial', '2021-11-08', NULL, '1', 'Yes', 'Yes', '2021-10-25 11:18:06', '2021-10-25 11:18:06', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (130, NULL, 0, 1, 1, 'monthly', 'trial', '2021-11-08', NULL, '1', 'Yes', 'Yes', '2021-10-25 11:20:39', '2021-10-25 11:20:39', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (131, NULL, 0, 1, 1, 'monthly', 'trial', '2021-11-08', NULL, '1', 'Yes', 'Yes', '2021-10-25 11:22:53', '2021-10-25 11:22:53', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (139, NULL, 0, 1, 1, 'monthly', 'trial', '2021-11-08', NULL, '1', 'Yes', 'Yes', '2021-10-25 11:28:14', '2021-10-25 11:28:14', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (142, NULL, 0, 1, 1, 'monthly', 'trial', '2021-11-08', NULL, '1', 'Yes', 'Yes', '2021-10-25 12:31:21', '2021-10-25 12:31:21', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (143, NULL, 0, 1, 3, 'yearly', 'trial', '2021-11-08', NULL, '1', 'Yes', 'Yes', '2021-10-25 12:33:25', '2021-10-25 12:33:25', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (144, NULL, 0, 1, 1, 'yearly', 'trial', '2021-11-08', NULL, '1', 'Yes', 'Yes', '2021-10-25 12:41:21', '2021-10-25 12:41:21', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (145, NULL, 0, 1, 1, 'yearly', 'trial', '2021-11-09', NULL, '1', 'Yes', 'Yes', '2021-10-26 09:49:59', '2021-10-26 09:49:59', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (146, NULL, 0, 1, 1, 'monthly', 'trial', '2021-11-09', NULL, '1', 'Yes', 'Yes', '2021-10-26 04:47:07', '2021-10-26 04:47:07', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (147, 'tes', 0, 1, 3, 'yearly', 'trial', '2021-11-09', NULL, '1', 'Yes', 'Yes', '2021-10-26 04:48:59', '2021-10-26 04:55:09', 'Desa Pelem', '', 'Kediri', 'Jawa Timur', '64211', NULL, 'tes', NULL, NULL, NULL, NULL, '', 'logo_1635224108.png', 'ftprd1_1635224108.png', '', '', '', '', 'splash_1635224108.jpg', 'bg_apk_1635224108.jpg', 'tes', 'tes', 'tes', 'tes', 'cover_1635224108.jpg', 'tmnail_1635224109.jpg', 'logocamp_1635224109.png', 'gbrcamp_1635224109.png', '');
INSERT INTO `companies` VALUES (149, NULL, 0, 1, 3, 'monthly', 'trial', '2021-11-09', NULL, '1', 'Yes', 'Yes', '2021-10-26 05:08:03', '2021-10-26 05:08:03', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (150, NULL, 0, 1, 3, 'yearly', 'trial', '2021-11-09', NULL, '1', 'Yes', 'Yes', '2021-10-26 05:21:06', '2021-10-26 05:21:06', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (151, NULL, 0, 1, 3, 'yearly', 'trial', '2021-11-09', NULL, '1', 'Yes', 'Yes', '2021-10-26 05:23:09', '2021-10-26 05:23:09', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (153, NULL, 0, 1, 1, 'monthly', 'trial', '2021-11-09', NULL, '1', 'Yes', 'Yes', '2021-10-26 07:04:20', '2021-10-26 07:04:20', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (154, NULL, 0, 1, 2, 'monthly', 'trial', '2021-11-09', NULL, '1', 'Yes', 'Yes', '2021-10-26 07:21:05', '2021-10-26 07:21:05', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (159, NULL, 0, 1, 3, 'yearly', 'trial', '2021-11-25', NULL, '1', 'Yes', 'Yes', '2021-10-26 09:16:17', '2021-10-26 09:16:17', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (169, NULL, 0, 1, 3, 'yearly', 'trial', '2021-11-26', NULL, '1', 'Yes', 'Yes', '2021-10-27 00:23:28', '2021-10-27 00:23:28', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (171, NULL, 0, 1, 1, 'monthly', 'trial', '2021-11-10', NULL, '1', 'Yes', 'Yes', '2021-10-27 04:25:40', '2021-10-27 04:25:40', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (172, NULL, 0, 1, 1, 'monthly', 'trial', '2021-11-10', NULL, '1', 'Yes', 'Yes', '2021-10-27 04:35:56', '2021-10-27 04:35:56', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (173, 'LHM', 0, 1, 3, 'yearly', 'member', '2021-11-10', '2021-10-27', '1', 'Yes', 'Yes', '2021-10-27 05:28:47', '2021-10-28 01:34:19', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (174, NULL, 0, 1, 1, 'yearly', 'trial', '2021-11-10', NULL, '1', 'Yes', 'Yes', '2021-10-27 05:29:35', '2021-10-27 05:29:35', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (176, NULL, 0, 1, 1, 'monthly', 'member', '2021-10-28', '2021-10-27', '1', 'Yes', 'Yes', '2021-10-27 08:40:15', '2021-10-27 15:47:40', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (177, NULL, 0, 1, 1, 'monthly', 'member', '2021-10-28', '2021-10-27', '1', 'Yes', 'Yes', '2021-10-27 08:56:18', '2021-10-27 16:00:38', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (178, 'STEAM', 0, 1, 2, 'monthly', 'member', '2021-11-28', NULL, '1', 'Yes', 'Yes', '2021-10-27 09:16:06', '2021-10-28 07:15:15', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (182, 'Lautan', 0, 1, 2, 'monthly', 'member', '2021-11-27', NULL, '1', 'Yes', 'Yes', '2021-10-27 10:14:02', '2021-10-28 01:33:48', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (185, 'PT Mitra Semesta', 0, 1, 1, 'monthly', 'member', '2021-12-08', NULL, '1', 'Yes', 'Yes', '2021-10-28 06:04:10', '2021-11-08 12:46:31', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (191, 'abc', 0, 1, 1, 'monthly', 'trial', '2021-11-12', NULL, '1', 'Yes', 'Yes', '2021-10-29 02:38:26', '2021-10-29 02:41:47', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (192, 'Kreasitama', 0, 1, 3, 'monthly', 'trial', '2021-11-12', NULL, '1', 'Yes', 'Yes', '2021-10-29 04:18:44', '2021-10-29 04:20:55', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (194, 'Ngelapak', 0, 1, 1, 'monthly', 'member', '2021-12-01', NULL, '1', 'Yes', 'Yes', '2021-10-31 13:47:05', '2021-11-01 14:48:01', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (195, 'beras murah', 3, 1, 1, 'monthly', 'trial', '2021-11-17', NULL, '1', 'Yes', 'Yes', '2021-11-03 05:50:34', '2021-11-03 05:56:02', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (197, 'Nara salon', 2, 1, 3, 'monthly', 'trial', '2021-11-18', NULL, '1', 'Yes', 'Yes', '2021-11-04 03:38:34', '2021-11-10 11:02:36', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (202, NULL, 0, 1, 3, 'yearly', 'trial', '2021-11-18', NULL, '1', 'Yes', 'Yes', '2021-11-04 09:37:23', '2021-11-04 09:37:23', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (203, 'Buat Testing', 0, 1, 3, 'yearly', 'member', '2021-11-18', NULL, '1', 'Yes', 'Yes', '2021-11-04 09:38:29', '2021-11-24 10:43:46', 'tess', '3634', '256', '11', '1212', 'ya', 'dsf', 'sdf', 'sdf', 'sdf', 'sdf', 'ya', '', '', '', '', '', '', '', '', '', '', '', '', 'cover_1636706801.png', 'tmnail_1636706801.png', 'logocamp_1636706801.png', 'gbrcamp_1636706802.png', 'To Do');
INSERT INTO `companies` VALUES (206, 'TWINS BEST MART', 7, 1, 3, 'yearly', 'trial', '2021-11-22', NULL, '1', 'Yes', 'Yes', '2021-11-08 12:04:54', '2021-11-08 12:09:46', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (207, NULL, 0, 1, 1, 'monthly', 'trial', '2021-11-22', NULL, '1', 'Yes', 'Yes', '2021-11-08 13:21:52', '2021-11-08 13:21:52', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (211, 'Duri kaktus', 3, 1, 1, 'monthly', 'trial', '2021-11-23', NULL, '1', 'Yes', 'Yes', '2021-11-09 10:43:09', '2021-11-09 10:47:48', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (212, 'Pempek Baba Sultan', 3, 1, 1, 'monthly', 'member', '2021-11-23', NULL, '1', 'Yes', 'Yes', '2021-11-09 12:52:22', '2022-02-10 14:31:29', 'Pondok Maritim Indah AL-3 RT/RW 11/06 Balas Klumprik, Wiyung', '', 'Surabaya', 'Jawa Timur', '60222', 'tidak', 'Lemak Nian, Taraso Ikannyo', 'Lemak Nian Taraso Ikannyo', NULL, NULL, NULL, 'ya', '', '', '', '', '', '', '', '', 'Pempek Enak Harga Merakyat', 'Pempek Ikan Premium', 'Pempek, Ikan', 'Telah hadir, Pempek Baba Sultan. Rasa Premium, Harga Merakyat.', 'cover_1636437592.png', 'tmnail_1636437592.png', 'logocamp_1636437592.png', 'gbrcamp_1636437592.png', '');
INSERT INTO `companies` VALUES (213, 'Rona\'s Bakery', 3, 1, 1, 'monthly', 'trial', '2021-11-23', NULL, '1', 'Yes', 'Yes', '2021-11-09 13:36:09', '2021-11-09 13:39:49', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (214, 'PASAR ONLINENSEMARANG', 7, 1, 1, 'monthly', 'trial', '2021-11-23', NULL, '1', 'Yes', 'Yes', '2021-11-09 18:25:28', '2021-11-09 18:30:24', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (215, NULL, 0, 1, 1, 'monthly', 'trial', '2021-11-26', NULL, '1', 'Yes', 'Yes', '2021-11-12 07:10:07', '2021-11-12 07:10:07', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (216, NULL, 0, 1, 1, 'monthly', 'trial', '2021-11-26', NULL, '1', 'Yes', 'Yes', '2021-11-12 09:58:23', '2021-11-12 09:58:23', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (217, 'Pempek Baba Sultan', 0, 1, 2, 'monthly', 'trial', '2021-11-26', NULL, '1', 'Yes', 'Yes', '2021-11-12 10:39:45', '2021-11-15 14:59:29', 'Perumahan Pondok Maritim Indah AL-3 RT/RW 11/06', '6159', '444', '11', '60222', 'tidak', '-', 'Lemak nian.. taraso rasanyo', '-', '-', '-', 'ya', '', '', '', '', '', '', '', '', 'Pempek Ikan Premium', 'Pempek Murah Untuk Semua Kalangan', 'Pempek, Pempek ikan, Pempek sultan', 'Pempek Ikan premium untuk semua kalangan', 'cover_1636963168.png', 'tmnail_1636963168.png', 'logocamp_1636963169.png', 'gbrcamp_1636963169.jpg', '');
INSERT INTO `companies` VALUES (218, 'Hello Baby n Kids Shop', 2, 1, 1, 'monthly', 'member', '2021-12-01', NULL, '1', 'Yes', 'Yes', '2021-11-17 11:20:37', '2021-11-19 09:36:50', 'Jln Raya kediri blitar ds Susuhbango Rt01 Rw01', '2519', '178', '11', '64176', 'ya', 'Hello Baby and Kids Shop adalah online shop yang menyediakan perlengkapan bayi dan anak-anak yang berkualitas dengan harga terjangkau. Kami juga mempunyai offline shop yang dapat anda kunjungi dengan alamat yang sudah tertera.', 'Penyedia Perlengkapan Bayi', 'hellokittybutiqu', NULL, NULL, 'ya', '', '', '', '', '', '', '', '', 'Hello baby and kids shop premium', 'Smart choice for our baby n kids', 'Hello baby and kids shop', 'Tempat mewah harga terjangkau pelayanan prima', 'cover_1637124042.jpg', 'tmnail_1637124042.jpg', 'logocamp_1637124042.png', 'gbrcamp_1637124042.png', 'Done');
INSERT INTO `companies` VALUES (219, 'IT Lance', 4, 1, 1, 'monthly', 'trial', '2021-12-01', NULL, '1', 'Yes', 'Yes', '2021-11-17 11:51:17', '2021-11-24 11:44:21', 'Jln. Musyawarah', '3868', '275', '28', '90554', 'tidak', 'IT Lance dibangun untuk mempertemukan client dengan freelance punya kami, didirikan pada 2021 dan bergerak di bidang digital. IT Lance bertujuan membuat bisnis di bidang digital semakin banyak dan mudah di online kan. Kami menawarkan jasa apa saja yang berkaitan dengan pembuatan Website, WebApp, Content writing, Desain, dan Service komputer untuk wilayah sulsel', NULL, 'it_lancee', NULL, NULL, 'tidak', 'logo_1637728998.png', 'ftprd1_1637728998.png', 'ftprd2_1637728998.png', 'ftprd3_1637728999.png', 'ftprd4_1637728999.png', '', 'splash_1637728999.png', 'bg_apk_1637729000.png', '', '', '', '', '', '', '', '', 'Done');
INSERT INTO `companies` VALUES (223, NULL, 0, 1, 2, 'monthly', 'trial', '2021-12-01', NULL, '1', 'Yes', 'Yes', '2021-11-17 14:16:29', '2021-11-17 14:16:29', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (225, NULL, 0, 1, 1, 'monthly', 'trial', '2021-12-01', NULL, '1', 'Yes', 'Yes', '2021-11-17 14:22:46', '2021-11-17 14:22:46', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (226, NULL, 0, 1, 1, 'monthly', 'trial', '2021-12-01', NULL, '1', 'Yes', 'Yes', '2021-11-17 14:23:23', '2021-11-17 14:23:23', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (227, 'Sate Taichan Bangjeb', 3, 1, 1, 'monthly', 'trial', '2021-12-01', NULL, '1', 'Yes', 'Yes', '2021-11-17 14:25:06', '2021-11-24 15:22:06', 'Konp garuda Putra D 105', '4614', '327', '33', '30114', 'tidak', 'Sate Taichan Bang Jeb merupakan sate ayam biasa yang dibakar tanpa menggunakan bumbu yang beraneka ragam dan hanya menggunakan garam dan jeruk nipis saja sebagai bumbu. Sate Taichan disajikan dengan sambal dan disantap bisa dengan lontong.', 'NIKMATNYA BETAH DI LIDAH', 'satetaichanbangjeb', NULL, NULL, 'tidak', 'logo_1637741679.png', 'ftprd1_1637741680.jpeg', 'ftprd2_1637741680.jpeg', 'ftprd3_1637741680.jpeg', 'ftprd4_1637741680.jpeg', '', 'splash_1637741680.jpeg', 'bg_apk_1637741680.jpeg', '', '', '', '', '', '', '', '', 'Done');
INSERT INTO `companies` VALUES (229, NULL, 0, 1, 1, 'monthly', 'trial', '2021-12-01', NULL, '1', 'Yes', 'Yes', '2021-11-17 14:28:31', '2021-11-17 14:28:31', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (230, 'Frozen Fish Surabaya', 3, 1, 1, 'monthly', 'trial', '2021-12-01', NULL, '1', 'Yes', 'Yes', '2021-11-17 14:59:36', '2021-11-24 15:37:13', 'Pondok Benowo Indah', '6132', '444', '11', '60197', 'tidak', 'Frozen fish menjual berbagai macam jenis ikan laut segar yang dikemas dalam keadaan vakum dan beku. Ikan lebih tahan lama, praktis, dan kualitas terjaga. Ikan ada yang dikemas utuh dan juga fillet. Tidak perlu khawatir juga, pembersihan ikan sudah dibuang mulai dari sirip hingga insang.', 'Ayo Makan Ikan', NULL, NULL, NULL, 'tidak', 'logo_1637742713.png', 'ftprd1_1637742713.png', 'ftprd2_1637742713.png', 'ftprd3_1637742713.png', 'ftprd4_1637742713.png', '', 'splash_1637742713.png', 'bg_apk_1637742714.png', '', '', '', '', '', '', '', '', 'Done');
INSERT INTO `companies` VALUES (231, 'Fight Art', 4, 1, 1, 'monthly', 'trial', '2021-12-01', NULL, '1', 'Yes', 'Yes', '2021-11-17 15:04:15', '2021-11-29 10:15:37', 'Perum. Airpor Village F-16', '5640', '409', '11', '61253', 'tidak', 'FIGHT ART Training Club merupakan program pelatihan olahraga beladiri secara private. Kami akan hadir di kota-kota besar di seluruh Indonesia.', 'Healthy, Positivity, Perfect Body', 'fightart.club', NULL, NULL, 'tidak', 'logo_1637744123.png', 'ftprd1_1637744123.png', 'ftprd2_1637744123.png', 'ftprd3_1637744124.png', 'ftprd4_1637744124.png', '', 'splash_1637744124.png', 'bg_apk_1637744125.png', '', '', '', '', '', '', '', '', 'Done');
INSERT INTO `companies` VALUES (232, 'Yok Belanja Yok', 2, 1, 1, 'monthly', 'trial', '2021-12-01', NULL, '1', 'Yes', 'Yes', '2021-11-17 16:06:59', '2021-11-29 10:15:56', 'Jl. Trunojoyo', '3636', '256', '11', '65111', 'tidak', 'YOK BELANJA YOK, adalah online store yang khusus menjual Luxury Brand items. Kami menyediakan kondisi baru dan bekas. Dan kami juga membuka pre-order service.', 'Punya barang original nggak harus mahal', 'yokbelanjayok', NULL, NULL, 'tidak', 'logo_1637810531.png', 'ftprd1_1637810531.jpg', 'ftprd2_1637810532.jpg', 'ftprd3_1637810532.jpg', 'ftprd4_1637810532.jpg', '', 'splash_1637810532.jpg', 'bg_apk_1637810532.jpg', '', '', '', '', '', '', '', '', 'Done');
INSERT INTO `companies` VALUES (233, 'Mastoy Catering', 3, 1, 1, 'monthly', 'trial', '2021-12-01', NULL, '1', 'Yes', 'Yes', '2021-11-17 16:12:08', '2021-11-29 10:14:43', 'Jl. Peta Selatan', '2089', '151', '6', '11840', 'tidak', 'Mastoy Catering, kami menjual seafood matang dengan konsep bancakan untuk berbagai kebutuhan acara, tanpa repot antri atau keluar rumah dengan rasa yang enak dan harga terjangkau.', 'Good Food for Everyone', 'mastoycatering', NULL, NULL, 'tidak', 'logo_1637811989.png', 'ftprd1_1637811989.png', 'ftprd2_1637811989.png', 'ftprd3_1637811990.png', 'ftprd4_1637811990.png', '', 'splash_1637811990.png', 'bg_apk_1637811996.png', '', '', '', '', '', '', '', '', 'Done');
INSERT INTO `companies` VALUES (234, 'Perusahaan Sobat Ngelapak', 4, 1, 4, 'yearly', 'member', '2023-01-05', NULL, '1', 'Yes', 'Yes', '2021-11-18 09:48:52', '2022-01-05 14:25:07', 'Jalan ke perusahaan Sobat Ngelapak', '2102', '152', '6', '123', 'tidak', 'Perusahaan sobat ngelapak', 'Usaha Sobat Ngelapak', '@sobatngelapak', 'Sobat Ngelapak', 'sobatngelapak.co.id', 'ya', '', '', '', '', '', '', '', '', 'Campaign Toko Sobat Ngelapak', 'Sub Judul Toko Sobat Ngelapak', 'Toko Sobat Ngelapak', 'Deskripsi Campaign', 'cover_1637204945.png', 'tmnail_1637204945.jpg', 'logocamp_1637204945.png', 'gbrcamp_1637204945.png', 'To Do');
INSERT INTO `companies` VALUES (235, 'Syena Cheesecake', 3, 1, 1, 'monthly', 'trial', '2021-12-02', NULL, '1', 'Yes', 'Yes', '2021-11-18 12:22:08', '2021-11-29 10:14:11', 'jalan menteng atas selatan 3', '2111', '153', '6', '12960', 'tidak', 'Syena Cheesecake & Sweets adalah UMKM yang bergerak di bidang makanan, yaitu cheesecake.', 'Cheesecake In Jar to fulfill your cravings', 'syenacheese', NULL, NULL, 'tidak', 'logo_1637817438.png', 'ftprd1_1637817442.jpg', 'ftprd2_1637817442.jpg', 'ftprd3_1637817442.jpg', 'ftprd4_1637817442.jpg', '', 'splash_1637817442.png', 'bg_apk_1637817447.png', '', '', '', '', '', '', '', '', 'Done');
INSERT INTO `companies` VALUES (236, 'Nata Komunikasi', 4, 1, 1, 'monthly', 'trial', '2021-12-02', NULL, '1', 'Yes', 'Yes', '2021-11-18 12:31:29', '2021-11-29 10:13:17', 'Sumber Lor RT 001/028, Kalitirto', '5779', '419', '5', '55573', 'tidak', 'Nata Komunikasi Indonesia adalah sebuah start up di bidang pengembangan sumber daya manusia yang berfokus pada pengembangan soft skill khususnya keterampilan berkomunikasi. Nata Komunikasi Indonesia memiliki program pengembangan SDM bagi institusi maupun individu dalam meningkatkan kemampuan berkomunikasi dan pengembangan diri. Tidak hanya mengajarkan peserta untuk berani bicara, kami juga memperbaiki mindset dari peserta, mengajarkan cara bicara yang efektif, dan cara berpenampilan yang profesional.', 'We Make Training as a Life Style', 'natakomunikasi', NULL, NULL, 'tidak', 'logo_1637822860.png', 'ftprd1_1637822860.png', 'ftprd2_1637822860.png', 'ftprd3_1637822861.jpg', 'ftprd4_1637822861.jpg', '', 'splash_1637822861.jpg', 'bg_apk_1637822862.jpg', '', '', '', '', '', '', '', '', 'Done');
INSERT INTO `companies` VALUES (237, 'Fazeelahafidz', 6, 1, 1, 'monthly', 'trial', '2021-12-03', NULL, '1', 'Yes', 'Yes', '2021-11-19 12:58:20', '2021-11-29 10:12:51', 'Tulang Bawang', '6806', '490', '18', '34555', 'tidak', 'Kami menjual beberapa macam speaker Al-Qur\'an lengkap 30 juz, flashdisk islami serta busana Muslimah', 'Fazeelahafidz is The Best !', 'fazeelahafidz', NULL, NULL, 'tidak', 'logo_1637823470.png', 'ftprd1_1637823470.jpg', 'ftprd2_1637823470.jpg', 'ftprd3_1637823470.jpg', 'ftprd4_1637823470.jpg', '', 'splash_1637823470.png', 'bg_apk_1637823471.png', '', '', '', '', '', '', '', '', 'Done');
INSERT INTO `companies` VALUES (238, 'Bakehouse PLM', 3, 1, 1, 'monthly', 'trial', '2021-12-03', NULL, '1', 'Yes', 'Yes', '2021-11-19 13:20:30', '2021-11-29 10:11:33', 'Perumahan awani residence cluster vimala blok L-09', '378', '24', '9', '40552', 'tidak', 'Bake house palembang, premium quality home made bakery and pastry, menggunakan bahan-bahan berkualitas terbaik, resep yang digunakan di hotel bintang 5, dengan racikan Chef Pastry berpengalaman internasional.', 'Coz there is always room for dessert', NULL, NULL, NULL, 'tidak', 'logo_1637824365.png', 'ftprd1_1637824365.jpeg', 'ftprd2_1637824365.jpeg', 'ftprd3_1637824365.jpeg', 'ftprd4_1637824365.jpeg', '', 'splash_1637824365.png', 'bg_apk_1637824367.png', '', '', '', '', '', '', '', '', 'Done');
INSERT INTO `companies` VALUES (239, 'Gelang By Shera', 2, 1, 1, 'monthly', 'trial', '2021-12-03', NULL, '1', 'Yes', 'Yes', '2021-11-19 15:10:12', '2021-11-29 10:11:08', 'Apartment Green Pramuka, Tower Chrysant, Jl. Jend. A. Yani Kav. 49', '2095', '152', '6', '10570', 'tidak', 'Gelang by Shera, membuat berbagai macam gelang baik dewasa maupun anak-anak. Semua gelang dibuat secara manual tidak menggunakan mesin. Gelang yang dibuat menggunakan bahan dari tali sintetis, kawat, senar karet, mutiara air tawar, batu alam, gudo (kaca olahan) ataupun manik-manik plastik.', 'Handmade, Simple, Etnik, Gaul', NULL, NULL, NULL, 'tidak', 'logo_1637900456.png', 'ftprd1_1637900456.jpeg', 'ftprd2_1637900456.jpeg', 'ftprd3_1637900456.jpeg', 'ftprd4_1637900457.jpeg', '', 'splash_1637900457.png', 'bg_apk_1637900458.png', '', '', '', '', '', '', '', '', 'Done');
INSERT INTO `companies` VALUES (240, 'Mamam Ssedapp', 3, 1, 1, 'monthly', 'trial', '2021-12-03', NULL, '1', 'Yes', 'Yes', '2021-11-19 15:19:33', '2021-11-29 10:10:20', 'Jalan Pemancingan I No. 22, Srengseng', '2091', '151', '6', '11630', 'tidak', 'Mamam Ssedapp merupakan usaha yang menyediakan kuliner khas Indonesia dalam kondisi matang (ready to eat) dan beku (frozen). Produk unggulan kami adalah Pempek Ikan Gabus khas Palembang dan Cilok Daging Sapi khas Jakarta.  Memiliki Halal MUI NO. 22.03.00747.12.18 dan izin PIRT NO. 2021671011519-21', 'MENYEDIAKAN BERANEKA RAGAM KULINER KHAS INDONESIA YANG SSEDAPP DAN BERKUALITAS', 'mamamssedapp', NULL, NULL, 'tidak', 'logo_1637901383.png', 'ftprd1_1637901383.jpeg', 'ftprd2_1637901383.jpeg', 'ftprd3_1637901383.jpeg', 'ftprd4_1637901384.jpeg', '', 'splash_1637901384.png', 'bg_apk_1637901385.png', '', '', '', '', '', '', '', '', 'Done');
INSERT INTO `companies` VALUES (241, 'Plato Betta Shop', 2, 1, 1, 'monthly', 'trial', '2021-12-03', NULL, '1', 'Yes', 'Yes', '2021-11-19 15:29:58', '2021-11-29 10:03:53', 'JL RAWA GEDE WETAN NO 48', '759', '55', '9', '16350', 'tidak', 'Menjual berbagai ikan hias (Cupang, Koi, Gupy, Louhan dll), akuarium & melayani pemesanan pembuatan aquascape', 'Plato Betta Shop', 'platobetta_brebes', NULL, NULL, 'tidak', 'logo_1637902998.png', 'ftprd1_1637902998.jpg', 'ftprd2_1637902998.jpg', 'ftprd3_1637902998.jpg', 'ftprd4_1637902999.jpg', '', 'splash_1637902999.png', 'bg_apk_1637903000.png', '', '', '', '', '', '', '', '', 'Done');
INSERT INTO `companies` VALUES (242, 'Bumi Arasy', 4, 1, 1, 'monthly', 'trial', '2021-12-03', NULL, '1', 'Yes', 'Yes', '2021-11-19 15:53:41', '2021-11-29 10:02:10', 'Jl. M. Kahfi 1 gang Aselih Grand Matoa Blok AA No. 9', '2104', '153', '6', '12530', 'tidak', 'PT. BUMI ARASY KONSULTANINDO adalah perusahaan swasta nasional dengan reputasi internasional yang bergerak dalam bidang Human Capital & Management Consultant. Selama lebih dari 28 tahun kami berkarya di Indonesia dan mempunyai lebih dari 200 klien baik dari, Perusahaan Multinasional, Perusahaan Swasta Nasional, BUMN, BUMD, Lembaga Pemerintahan, dan Organisasi Profit & Non Profit. Bumi Arasy Konsultanindo memberikan pelayanan Jasa Utama yaitu : Business Coaching, Management Consulting dan Training.', 'The Solution for Happiness & Success', 'bumiarasy_official', NULL, NULL, 'tidak', 'logo_1637903532.png', 'ftprd1_1637903532.png', 'ftprd2_1637903532.png', 'ftprd3_1637903532.png', '', '', 'splash_1637903532.png', 'bg_apk_1637903533.png', '', '', '', '', '', '', '', '', 'Done');
INSERT INTO `companies` VALUES (243, 'Kedai Kezia', 3, 1, 1, 'monthly', 'trial', '2021-12-06', NULL, '1', 'Yes', 'Yes', '2021-11-22 10:42:23', '2021-11-29 10:01:33', 'Jalan haji tarin no 56 rt.02 rw.09 kp.cikumpa', '1586', '115', '9', '16412', 'tidak', 'Kedai kezia merupakan usaha ikan kering yang bermacam-macam jenisnya.proses ikannya semua tanpa bahan pengawet. pengawet ikannya hanya dari garam saja. untuk itu kita menjaga kualitas ikannya agar tetap fresh dan harga ikan pun kita jual dibawah harga pasar.', 'Berdoa dan berusaha', NULL, NULL, NULL, 'tidak', 'logo_1637904035.png', 'ftprd1_1637904035.jpg', '', '', '', '', 'splash_1637904035.jpg', 'bg_apk_1637904036.jpg', '', '', '', '', '', '', '', '', 'Done');
INSERT INTO `companies` VALUES (244, 'Semangat Tani Sweet', 4, 1, 1, 'monthly', 'trial', '2021-12-06', NULL, '1', 'Yes', 'Yes', '2021-11-22 10:49:27', '2021-11-29 10:00:47', 'Jalan Sisingamangaraja no.169', '6415', '465', '34', '22474', 'tidak', 'Adapun usaha ini dibuat dan dirancang adalah untuk membantu para petani dalam proses pemasaran,pendistribusian,dan menekan perbuatan tengkulak yang selama ini semena mena membuat harga pada petani yang sangat merugikan petani.', 'Semangat tani sweet merupakan sebuah usaha berdasar dari  kelompok tani yang bergerak dibidang komoditi hasil pertanian.', NULL, NULL, NULL, 'tidak', 'logo_1637906535.png', 'ftprd1_1637906535.png', '', '', '', '', 'splash_1637906535.png', 'bg_apk_1637906536.png', '', '', '', '', '', '', '', '', 'Done');
INSERT INTO `companies` VALUES (245, 'Manifestudio', 2, 1, 1, 'monthly', 'trial', '2021-12-06', NULL, '1', 'Yes', 'Yes', '2021-11-22 11:05:46', '2021-11-29 10:00:15', 'Jl Delman Elok VII no 17 Tanah Kusir', '2106', '153', '6', '12240', 'tidak', 'MANIFESTUDIO is a design studio that creates a wide range of lifestyle products with its whimsical and colourful hand-painted illustrations. We are focusing on product development and innovation to make such a little memento become valuable - because of the stories in it. Our goal is to paint a colourful world and bring aesthetic pleasure through our creations.', '“Capture memories around the world through souvenirs, gifts, home décor, and illustrations”', 'manifestudio', NULL, NULL, 'tidak', 'logo_1637908009.png', 'ftprd1_1637908009.jpg', 'ftprd2_1637908014.jpg', 'ftprd3_1637908014.jpg', 'ftprd4_1637908014.jpg', '', 'splash_1637908014.png', 'bg_apk_1637908016.png', '', '', '', '', '', '', '', '', 'Done');
INSERT INTO `companies` VALUES (246, 'BF Selling Supply', 4, 1, 1, 'monthly', 'trial', '2021-12-06', NULL, '1', 'Yes', 'Yes', '2021-11-22 11:20:39', '2021-11-29 09:59:48', 'Dukuh pakis 5 no 34B, rt4 rw3', '6135', '444', '11', '60224', 'tidak', 'Brainfreeze adalah perusahaan yang bergerak di bidang pembangunan sipil dan penyedia segala kebutuhan Event. Telah dipercaya oleh puluhan pengguna sejak Tahun 2017', 'SEWA, CUSTOM RAK DAN PORTABLE BOOTH', 'bfsellingsupply', NULL, NULL, 'ya', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Done');
INSERT INTO `companies` VALUES (247, 'Neeture', 2, 1, 1, 'monthly', 'trial', '2021-12-06', NULL, '1', 'Yes', 'Yes', '2021-11-22 11:38:57', '2021-11-29 09:59:24', 'Perum. Istana Mentari blok E1 no.31', '5641', '409', '11', '61234', 'tidak', 'Menyediakan produk yang bermanfaat khususnya produk kesehatan & kecantikan yang berkualitas serta memberikan nilai lebih yaitu setiap pembelian ada nilai sedekah untuk anak yatim.', 'Go 1% penduduk Indonesia hidup lebih sehat & sejahtera', 'kkneeture', NULL, NULL, 'tidak', 'logo_1637910497.png', 'ftprd1_1637910497.jpeg', 'ftprd2_1637910498.jpeg', 'ftprd3_1637910498.jpeg', 'ftprd4_1637910498.jpeg', '', 'splash_1637910498.png', 'bg_apk_1637910499.png', '', '', '', '', '', '', '', '', 'Done');
INSERT INTO `companies` VALUES (248, 'TheKetin', 4, 1, 1, 'monthly', 'trial', '2021-12-06', NULL, '1', 'Yes', 'Yes', '2021-11-22 11:48:16', '2021-11-29 09:59:00', 'Sono Indah, Buduran Gg Tpq, Sidokerto', '5632', '409', '11', '61252', 'tidak', 'The Ketin merupakan pelayanan pengiriman atau transportasi penumpang dengan pilihan kurirnya sendiri, serta konsumen mengetahui sampai mana kurir secara real time. Konsumen membawahi atau mempunyai kurir langganan secara berkelanjutan, karena target pasar kami merupakan kurir tetap untuk lebih dekat dengan konsumen.', 'Pilihan Kurir Pribadimu', 'theketin', NULL, NULL, 'ya', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Done');
INSERT INTO `companies` VALUES (249, 'PT Ardhinawa', 5, 1, 1, 'monthly', 'trial', '2021-12-06', NULL, '1', 'Yes', 'Yes', '2021-11-22 12:03:31', '2021-11-29 09:58:37', 'Jemursari Timur V blok ji no 13', '6160', '444', '11', '60237', 'tidak', 'PT. ARDHINAWA  Is contractor and supplier that is engaged in the following service fields focus :  Office Building  Warehouse/Factory Building  Powerplant     Infrastructur      and Equipment Construction  Mechanical  and  Electrical   Work and Equipment Supply  Road Pavement, Railway and Runway  Water Pipe  Wasted Water Pipe', 'SUSTAINABLE CONSTRUCTION - GREEN CONSTRUCTION - HARMONY', NULL, NULL, NULL, 'ya', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Done');
INSERT INTO `companies` VALUES (250, 'Baros Food', 3, 1, 1, 'monthly', 'trial', '2021-12-06', NULL, '1', 'Yes', 'Yes', '2021-11-22 12:17:40', '2021-11-29 09:58:09', 'Jl. Baros no 69', '5965', '431', '9', '43161', 'tidak', 'BAROSFOOD, Merupakan perusahaan dagang kuliner yang ada di Baros kota sukabumi, membantu UMKM kecil untuk berevolusi dari manual ke digital.', 'UKM Go Digital!', 'creative_clothing11', NULL, NULL, 'tidak', 'logo_1637916300.png', 'ftprd1_1637916300.jpeg', 'ftprd2_1637916300.jpeg', 'ftprd3_1637916300.jpeg', 'ftprd4_1637916300.jpeg', '', 'splash_1637916300.png', 'bg_apk_1637916301.png', '', '', '', '', '', '', '', '', 'Done');
INSERT INTO `companies` VALUES (251, 'AL Sportwear', 2, 1, 1, 'monthly', 'trial', '2021-12-06', NULL, '1', 'Yes', 'Yes', '2021-11-22 13:13:21', '2021-11-29 09:57:38', 'Jl. Mentani I', '1830', '133', '11', '61152', 'tidak', 'AL Sportwear merupakan nama dari bisnis di bidang penyedia perlengkapan olahraga yang kami jalani, saat ini produk dari AL Sportwear sudah dipasarkan menggunakan teknologi digital melalui berbagai market place', 'proud of local product', 'alsportwear', NULL, NULL, 'tidak', 'logo_1637916561.png', '', 'ftprd2_1637916561.jpg', 'ftprd3_1637916562.jpg', 'ftprd4_1637916562.jpg', '', 'splash_1637916563.png', 'bg_apk_1637916564.png', '', '', '', '', '', '', '', '', 'Done');
INSERT INTO `companies` VALUES (252, 'Papa Bangunan', 5, 1, 1, 'monthly', 'trial', '2021-12-06', NULL, '1', 'Yes', 'Yes', '2021-11-22 13:19:51', '2021-11-29 09:57:16', 'Komplek Nata Endah 2 Sadang, Jalan Teratai Blok K No 48', '327', '22', '9', '40225', 'ya', 'PAPA BANGUNAN, menjual alat alat Teknik dan bahan bangunan non pasir dan batu bata.', '#PapaBangunan #Konstruksi #AlatTeknik', NULL, NULL, NULL, 'ya', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Done');
INSERT INTO `companies` VALUES (253, 'Comely Furniture', 5, 1, 1, 'monthly', 'trial', '2021-12-06', NULL, '1', 'Yes', 'Yes', '2021-11-22 13:25:00', '2021-11-29 09:56:56', 'Kembul sari, Suwawal Timur', '2251', '163', '10', '59456', 'tidak', 'Comely Furniture adalah UMKM yang bergerak di bidang furniture kayu.  Visi kami adalah maju dengan industri furniture lokal.  Misi kami adalah Siap memasok furniture ke mana pun.', 'Find your future funiture here', NULL, NULL, NULL, 'ya', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Done');
INSERT INTO `companies` VALUES (254, 'Cumi Hitam Racun', 3, 1, 1, 'monthly', 'trial', '2021-12-07', NULL, '1', 'Yes', 'Yes', '2021-11-23 11:06:21', '2021-11-29 09:56:32', 'Jalan bogen 49a', '6155', '444', '11', '60133', 'tidak', 'Sesuai Tagline kami “keracunan pengen nambah terus” makanan olahan cumi membawa resep yang original dan beda dari yang lain nya', 'Keracunan Pengen Nambah Terus', 'cumihitamracun', NULL, NULL, 'tidak', 'logo_1637920941.png', 'ftprd1_1637920942.jpeg', 'ftprd2_1637920942.jpeg', 'ftprd3_1637920942.jpeg', 'ftprd4_1637920942.jpeg', '', 'splash_1637920942.png', 'bg_apk_1637920943.png', '', '', '', '', '', '', '', '', 'Done');
INSERT INTO `companies` VALUES (255, 'Datum Carport Gallery', 4, 1, 1, 'monthly', 'trial', '2021-12-07', NULL, '1', 'Yes', 'Yes', '2021-11-23 11:13:24', '2021-11-29 09:56:11', 'Jl.Sapta marga Blok M No 8', '2943', '211', '9', '46264', 'tidak', 'Datum Carport Gallery adalah sebuah usaha yang bergerak di bidang otomotif', 'Bukan sekedar yang anda cari', 'swayzadatum', NULL, NULL, 'tidak', 'logo_1637921527.png', 'ftprd1_1637921527.jpeg', 'ftprd2_1637921527.jpeg', 'ftprd3_1637921527.jpeg', 'ftprd4_1637921527.jpeg', '', 'splash_1637921527.png', 'bg_apk_1637921528.png', '', '', '', '', '', '', '', '', 'Done');
INSERT INTO `companies` VALUES (256, 'Neila Hijab Official', 2, 1, 1, 'monthly', 'trial', '2021-12-07', NULL, '1', 'Yes', 'Yes', '2021-11-23 11:32:56', '2021-11-29 09:55:18', 'Jl Arwana 1 Blok B8 No 4 Perumahan Pondok Gede Permai', '755', '55', '9', '17424', 'tidak', 'Niela Official adalah brand fashion muslimah yang saat ini menyediakan beberapa ragam hijab harian dengan warna warna pastel yang mengedepankan kualitas baik dengan hangka terjangkau.', 'Your Daily Hijab Companion', 'nielaofficial', NULL, NULL, 'tidak', 'logo_1637922036.png', 'ftprd1_1637922036.jpg', 'ftprd2_1637922036.jpg', 'ftprd3_1637922036.jpg', 'ftprd4_1637922036.jpg', '', 'splash_1637922036.png', 'bg_apk_1637922037.png', '', '', '', '', '', '', '', '', 'Done');
INSERT INTO `companies` VALUES (257, 'VACANA INDONESIA', 4, 1, 1, 'monthly', 'trial', '2021-12-07', NULL, '1', 'Yes', 'Yes', '2021-11-23 11:36:39', '2021-11-29 09:54:58', 'Jl. Taman Buana Permai Blk. C', '1573', '114', '1', '80118', 'tidak', 'Vacana Indonesia merupakan group company Travel & Transportation mempunyai jaringan dan pelayanan seluruh Indonesia.', 'Unforgettable Dream Vacation', 'vacanatour', NULL, NULL, 'tidak', 'logo_1637923481.png', 'ftprd1_1637923481.JPG', 'ftprd2_1637923481.JPG', 'ftprd3_1637923481.JPG', 'ftprd4_1637923481.png', '', 'splash_1637923482.png', 'bg_apk_1637923483.png', '', '', '', '', '', '', '', '', 'Done');
INSERT INTO `companies` VALUES (258, 'Lezatta.id', 3, 1, 1, 'monthly', 'trial', '2021-12-07', NULL, '1', 'Yes', 'Yes', '2021-11-23 13:00:09', '2021-11-29 09:54:41', 'Jl. Jeruk VI/37, Pondok Chandra, Tambak Rejo', '5647', '409', '11', '61256', 'tidak', 'Lezatta di dirikan pada tahun 2019 oleh 2 orang yaitu Putiari Alaina Rizki dan Mella Ramadhani. Lezatta merupakan usaha di bidang kuliner dengan menyediakan menu utamanya yaitu chicken rice box dan memiliki 3 menu utama seperti Chic Blackpepper, Chic Barbeque, dan Chic Mentai. Di tahun 2021 ini, Lezatta membuat menu baru yaitu Mega Mentai dengan 2 menu seperti Birthday Mentai Rice Cake dan Kani Mentai Rice Box.', 'Lezatta.. Pas kenyangnya, Ramah Harganya!', 'lezatta.id', NULL, NULL, 'tidak', 'logo_1637923766.png', 'ftprd1_1637923766.jpeg', 'ftprd2_1637923766.jpeg', '', 'ftprd4_1637923766.jpeg', '', 'splash_1637923766.jpeg', 'bg_apk_1637923767.jpeg', '', '', '', '', '', '', '', '', 'Done');
INSERT INTO `companies` VALUES (259, 'UNO Laundry', 4, 1, 1, 'monthly', 'trial', '2021-12-07', NULL, '1', 'Yes', 'Yes', '2021-11-23 13:05:57', '2021-11-29 09:54:26', 'Jl. K. Zainal abidin no.38a, Tambak sumur', '5647', '409', '11', '61256', 'tidak', 'Usaha di bidang binatu dengan pelayanan Ekspres, melayani laundry satuan maupun kiloan dengan macam paket pilihan.', 'Suci bersih wangi', 'unolaundrywaru', NULL, NULL, 'tidak', 'logo_1637925578.png', 'ftprd1_1637925578.png', 'ftprd2_1637925578.png', 'ftprd3_1637925578.png', 'ftprd4_1637925579.png', '', 'splash_1637925579.png', 'bg_apk_1637925580.png', '', '', '', '', '', '', '', '', 'Done');
INSERT INTO `companies` VALUES (260, 'D\'Pecel EA', 3, 1, 1, 'monthly', 'trial', '2021-12-07', NULL, '1', 'Yes', 'Yes', '2021-11-23 14:00:08', '2021-11-29 09:53:48', 'Jl. Palem no 9', '1585', '115', '9', '16516', 'tidak', 'Dpecel EA (Enak Asli) berdiri sejak tahun 2015, salah satu makanan favorit kami adalah Bumbu Pecel EA (Enak Asli), bumbu pecel yang dibuat dengan resep eyang, diproduksi dengan bahan-bahan premium berkualitas sehingga menciptakan rasa legit dan lezat membuat Bumbu Pecel EA berbeda dari yang lain.', 'Pecel Favorit Keluarga !', NULL, NULL, NULL, 'tidak', 'logo_1638153089.png', 'ftprd1_1638153089.jpg', 'ftprd2_1638153089.jpg', 'ftprd3_1638153089.jpg', 'ftprd4_1638153090.jpg', '', 'splash_1638153090.jpg', 'bg_apk_1638153090.jpg', '', '', '', '', '', '', '', '', 'Done');
INSERT INTO `companies` VALUES (261, 'Balirent.id', 4, 1, 1, 'monthly', 'trial', '2021-12-07', NULL, '1', 'Yes', 'Yes', '2021-11-23 14:03:57', '2021-11-29 09:53:31', 'Jl. Bukit Permai Lot B4 No. 8', '260', '17', '1', '80361', 'tidak', 'Balirent.id adalah aplikasi tour & travel yang berdomisili di daerah Bali.', 'Design Your Day', 'balirent.id', NULL, NULL, 'ya', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Done');
INSERT INTO `companies` VALUES (262, 'Nazarets Welding', 5, 1, 1, 'monthly', 'trial', '2021-12-07', NULL, '1', 'Yes', 'Yes', '2021-11-23 14:13:15', '2021-11-29 09:53:16', 'Jl. Banda Aceh - Calang Km. 117', '47', '4', '21', '23656', 'tidak', 'Fabrication and Welding Services', 'Pioneer MIG Welding Aceh', NULL, NULL, NULL, 'tidak', 'logo_1638154298.png', 'ftprd1_1638154298.jpg', 'ftprd2_1638154298.jpg', 'ftprd3_1638154298.jpg', 'ftprd4_1638154298.jpg', '', 'splash_1638154298.png', 'bg_apk_1638154299.png', '', '', '', '', '', '', '', '', 'Done');
INSERT INTO `companies` VALUES (264, NULL, 0, 1, 1, 'monthly', 'trial', '2021-12-09', NULL, '1', 'Yes', 'Yes', '2021-11-25 16:03:50', '2021-11-25 16:03:50', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (305, 'lusilicous', 0, 1, 2, 'monthly', 'member', '2022-02-07', NULL, '1', 'Yes', 'Yes', '2022-01-07 10:27:17', '2022-01-07 10:31:47', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (306, 'Karya Cipta Sejahtera', 5, 1, 1, 'monthly', 'member', '2022-02-07', NULL, '1', 'Yes', 'Yes', '2022-01-07 13:11:44', '2022-01-24 14:59:16', 'Jl Raya Wiyung no. 454', '6159', '444', '11', '60222', 'tidak', 'Menjual sparepart air conditioning', NULL, NULL, NULL, NULL, 'ya', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'To Do');
INSERT INTO `companies` VALUES (307, NULL, 0, 1, 3, 'monthly', 'member', '2022-01-28', NULL, '1', 'Yes', 'Yes', '2022-01-14 10:22:11', '2022-01-14 10:22:11', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (308, 'Madura United FC', 0, 1, 3, 'monthly', 'member', '2022-01-31', NULL, '1', 'Yes', 'Yes', '2022-01-17 11:11:39', '2022-01-17 11:16:25', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (309, NULL, 0, 1, NULL, NULL, 'trial', '2022-02-09', NULL, '1', 'Yes', 'Yes', '2022-01-26 15:25:19', '2022-01-26 15:25:19', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (310, 'Satrya', 0, 1, 2, 'monthly', 'trial', '2022-02-22', NULL, '1', 'Yes', 'Yes', '2022-02-08 16:57:25', '2022-02-08 16:59:08', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (311, 'Seragam', 0, 1, 1, 'monthly', 'trial', '2022-02-28', NULL, '1', 'Yes', 'Yes', '2022-02-14 21:32:59', '2022-02-14 21:40:04', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (312, NULL, 0, 1, 1, 'monthly', 'trial', '2022-03-01', NULL, '1', 'Yes', 'Yes', '2022-02-15 08:30:02', '2022-02-15 08:30:02', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (313, NULL, 0, 1, 3, 'yearly', 'trial', '2022-03-01', NULL, '1', 'Yes', 'Yes', '2022-02-15 08:36:47', '2022-02-15 08:36:47', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `companies` VALUES (314, NULL, 0, 1, 1, 'monthly', 'trial', '2022-03-06', NULL, '1', 'Yes', 'Yes', '2022-02-20 20:40:22', '2022-02-20 20:40:22', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');

-- ----------------------------
-- Table structure for company_email_template
-- ----------------------------
DROP TABLE IF EXISTS `company_email_template`;
CREATE TABLE `company_email_template`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `related_to` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `company_id` bigint NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of company_email_template
-- ----------------------------

-- ----------------------------
-- Table structure for company_settings
-- ----------------------------
DROP TABLE IF EXISTS `company_settings`;
CREATE TABLE `company_settings`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_id` bigint NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of company_settings
-- ----------------------------

-- ----------------------------
-- Table structure for contact_groups
-- ----------------------------
DROP TABLE IF EXISTS `contact_groups`;
CREATE TABLE `contact_groups`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `note` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `company_id` bigint NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of contact_groups
-- ----------------------------

-- ----------------------------
-- Table structure for contacts
-- ----------------------------
DROP TABLE IF EXISTS `contacts`;
CREATE TABLE `contacts`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `profile_type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `contact_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_email` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `vat_id` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `reg_no` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `contact_phone` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `country` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `currency` varchar(3) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `state` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `zip` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `address` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `facebook` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `twitter` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `linkedin` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `remarks` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `contact_image` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `group_id` bigint NULL DEFAULT NULL,
  `user_id` bigint NULL DEFAULT NULL,
  `company_id` bigint NOT NULL,
  `custom_fields` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of contacts
-- ----------------------------

-- ----------------------------
-- Table structure for currency_rates
-- ----------------------------
DROP TABLE IF EXISTS `currency_rates`;
CREATE TABLE `currency_rates`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `currency` varchar(3) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `rate` decimal(10, 6) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 169 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of currency_rates
-- ----------------------------
INSERT INTO `currency_rates` VALUES (1, 'AED', 4.101083, NULL, NULL);
INSERT INTO `currency_rates` VALUES (2, 'AFN', 85.378309, NULL, NULL);
INSERT INTO `currency_rates` VALUES (3, 'ALL', 123.510844, NULL, NULL);
INSERT INTO `currency_rates` VALUES (4, 'AMD', 548.849773, NULL, NULL);
INSERT INTO `currency_rates` VALUES (5, 'ANG', 2.008050, NULL, NULL);
INSERT INTO `currency_rates` VALUES (6, 'AOA', 556.155120, NULL, NULL);
INSERT INTO `currency_rates` VALUES (7, 'ARS', 70.205746, NULL, NULL);
INSERT INTO `currency_rates` VALUES (8, 'AUD', 1.809050, NULL, NULL);
INSERT INTO `currency_rates` VALUES (9, 'AWG', 2.009782, NULL, NULL);
INSERT INTO `currency_rates` VALUES (10, 'AZN', 1.833159, NULL, NULL);
INSERT INTO `currency_rates` VALUES (11, 'BAM', 1.966840, NULL, NULL);
INSERT INTO `currency_rates` VALUES (12, 'BBD', 2.245460, NULL, NULL);
INSERT INTO `currency_rates` VALUES (13, 'BDT', 95.162306, NULL, NULL);
INSERT INTO `currency_rates` VALUES (14, 'BGN', 1.952383, NULL, NULL);
INSERT INTO `currency_rates` VALUES (15, 'BHD', 0.421787, NULL, NULL);
INSERT INTO `currency_rates` VALUES (16, 'BIF', 2117.865003, NULL, NULL);
INSERT INTO `currency_rates` VALUES (17, 'BMD', 1.116545, NULL, NULL);
INSERT INTO `currency_rates` VALUES (18, 'BND', 1.583270, NULL, NULL);
INSERT INTO `currency_rates` VALUES (19, 'BOB', 7.718004, NULL, NULL);
INSERT INTO `currency_rates` VALUES (20, 'BRL', 5.425949, NULL, NULL);
INSERT INTO `currency_rates` VALUES (21, 'BSD', 1.121775, NULL, NULL);
INSERT INTO `currency_rates` VALUES (22, 'BTC', 0.000244, NULL, NULL);
INSERT INTO `currency_rates` VALUES (23, 'BTN', 82.818317, NULL, NULL);
INSERT INTO `currency_rates` VALUES (24, 'BWP', 12.683055, NULL, NULL);
INSERT INTO `currency_rates` VALUES (25, 'BYN', 2.621037, NULL, NULL);
INSERT INTO `currency_rates` VALUES (26, 'BYR', 9999.999999, NULL, NULL);
INSERT INTO `currency_rates` VALUES (27, 'BZD', 2.261248, NULL, NULL);
INSERT INTO `currency_rates` VALUES (28, 'CAD', 1.552879, NULL, NULL);
INSERT INTO `currency_rates` VALUES (29, 'CDF', 1898.127343, NULL, NULL);
INSERT INTO `currency_rates` VALUES (30, 'CHF', 1.056023, NULL, NULL);
INSERT INTO `currency_rates` VALUES (31, 'CLF', 0.033950, NULL, NULL);
INSERT INTO `currency_rates` VALUES (32, 'CLP', 936.781769, NULL, NULL);
INSERT INTO `currency_rates` VALUES (33, 'CNY', 7.827878, NULL, NULL);
INSERT INTO `currency_rates` VALUES (34, 'COP', 4491.872864, NULL, NULL);
INSERT INTO `currency_rates` VALUES (35, 'CRC', 635.520417, NULL, NULL);
INSERT INTO `currency_rates` VALUES (36, 'CUC', 1.116545, NULL, NULL);
INSERT INTO `currency_rates` VALUES (37, 'CUP', 29.588450, NULL, NULL);
INSERT INTO `currency_rates` VALUES (38, 'CVE', 110.887227, NULL, NULL);
INSERT INTO `currency_rates` VALUES (39, 'CZK', 26.906059, NULL, NULL);
INSERT INTO `currency_rates` VALUES (40, 'DJF', 198.432393, NULL, NULL);
INSERT INTO `currency_rates` VALUES (41, 'DKK', 7.472892, NULL, NULL);
INSERT INTO `currency_rates` VALUES (42, 'DOP', 60.196240, NULL, NULL);
INSERT INTO `currency_rates` VALUES (43, 'DZD', 134.499489, NULL, NULL);
INSERT INTO `currency_rates` VALUES (44, 'EGP', 17.585483, NULL, NULL);
INSERT INTO `currency_rates` VALUES (45, 'ERN', 16.748349, NULL, NULL);
INSERT INTO `currency_rates` VALUES (46, 'ETB', 36.696587, NULL, NULL);
INSERT INTO `currency_rates` VALUES (47, 'EUR', 1.000000, NULL, NULL);
INSERT INTO `currency_rates` VALUES (48, 'FJD', 2.549240, NULL, NULL);
INSERT INTO `currency_rates` VALUES (49, 'FKP', 0.908257, NULL, NULL);
INSERT INTO `currency_rates` VALUES (50, 'GBP', 0.907964, NULL, NULL);
INSERT INTO `currency_rates` VALUES (51, 'GEL', 3.115301, NULL, NULL);
INSERT INTO `currency_rates` VALUES (52, 'GGP', 0.908257, NULL, NULL);
INSERT INTO `currency_rates` VALUES (53, 'GHS', 6.220337, NULL, NULL);
INSERT INTO `currency_rates` VALUES (54, 'GIP', 0.908257, NULL, NULL);
INSERT INTO `currency_rates` VALUES (55, 'GMD', 56.605069, NULL, NULL);
INSERT INTO `currency_rates` VALUES (56, 'GNF', 9999.999999, NULL, NULL);
INSERT INTO `currency_rates` VALUES (57, 'GTQ', 8.576324, NULL, NULL);
INSERT INTO `currency_rates` VALUES (58, 'GYD', 234.489495, NULL, NULL);
INSERT INTO `currency_rates` VALUES (59, 'HKD', 8.674753, NULL, NULL);
INSERT INTO `currency_rates` VALUES (60, 'HNL', 27.678062, NULL, NULL);
INSERT INTO `currency_rates` VALUES (61, 'HRK', 7.590196, NULL, NULL);
INSERT INTO `currency_rates` VALUES (62, 'HTG', 106.356510, NULL, NULL);
INSERT INTO `currency_rates` VALUES (63, 'HUF', 341.150311, NULL, NULL);
INSERT INTO `currency_rates` VALUES (64, 'IDR', 9999.999999, NULL, NULL);
INSERT INTO `currency_rates` VALUES (65, 'ILS', 4.159226, NULL, NULL);
INSERT INTO `currency_rates` VALUES (66, 'IMP', 0.908257, NULL, NULL);
INSERT INTO `currency_rates` VALUES (67, 'INR', 82.763894, NULL, NULL);
INSERT INTO `currency_rates` VALUES (68, 'IQD', 1339.198712, NULL, NULL);
INSERT INTO `currency_rates` VALUES (69, 'IRR', 9999.999999, NULL, NULL);
INSERT INTO `currency_rates` VALUES (70, 'ISK', 151.202539, NULL, NULL);
INSERT INTO `currency_rates` VALUES (71, 'JEP', 0.908257, NULL, NULL);
INSERT INTO `currency_rates` VALUES (72, 'JMD', 151.606351, NULL, NULL);
INSERT INTO `currency_rates` VALUES (73, 'JOD', 0.791685, NULL, NULL);
INSERT INTO `currency_rates` VALUES (74, 'JPY', 118.278988, NULL, NULL);
INSERT INTO `currency_rates` VALUES (75, 'KES', 115.283224, NULL, NULL);
INSERT INTO `currency_rates` VALUES (76, 'KGS', 81.395812, NULL, NULL);
INSERT INTO `currency_rates` VALUES (77, 'KHR', 4603.144194, NULL, NULL);
INSERT INTO `currency_rates` VALUES (78, 'KMF', 495.355724, NULL, NULL);
INSERT INTO `currency_rates` VALUES (79, 'KPW', 1004.922902, NULL, NULL);
INSERT INTO `currency_rates` VALUES (80, 'KRW', 1372.190164, NULL, NULL);
INSERT INTO `currency_rates` VALUES (81, 'KWD', 0.344879, NULL, NULL);
INSERT INTO `currency_rates` VALUES (82, 'KYD', 0.934921, NULL, NULL);
INSERT INTO `currency_rates` VALUES (83, 'KZT', 456.318281, NULL, NULL);
INSERT INTO `currency_rates` VALUES (84, 'LAK', 9978.233671, NULL, NULL);
INSERT INTO `currency_rates` VALUES (85, 'LBP', 1696.373291, NULL, NULL);
INSERT INTO `currency_rates` VALUES (86, 'LKR', 206.967335, NULL, NULL);
INSERT INTO `currency_rates` VALUES (87, 'LRD', 221.076044, NULL, NULL);
INSERT INTO `currency_rates` VALUES (88, 'LSL', 18.121543, NULL, NULL);
INSERT INTO `currency_rates` VALUES (89, 'LTL', 3.296868, NULL, NULL);
INSERT INTO `currency_rates` VALUES (90, 'LVL', 0.675387, NULL, NULL);
INSERT INTO `currency_rates` VALUES (91, 'LYD', 1.557311, NULL, NULL);
INSERT INTO `currency_rates` VALUES (92, 'MAD', 10.730569, NULL, NULL);
INSERT INTO `currency_rates` VALUES (93, 'MDL', 19.734707, NULL, NULL);
INSERT INTO `currency_rates` VALUES (94, 'MGA', 4165.265277, NULL, NULL);
INSERT INTO `currency_rates` VALUES (95, 'MKD', 61.516342, NULL, NULL);
INSERT INTO `currency_rates` VALUES (96, 'MMK', 1566.586511, NULL, NULL);
INSERT INTO `currency_rates` VALUES (97, 'MNT', 3088.650418, NULL, NULL);
INSERT INTO `currency_rates` VALUES (98, 'MOP', 8.975925, NULL, NULL);
INSERT INTO `currency_rates` VALUES (99, 'MRO', 398.607011, NULL, NULL);
INSERT INTO `currency_rates` VALUES (100, 'MUR', 43.205754, NULL, NULL);
INSERT INTO `currency_rates` VALUES (101, 'MVR', 17.250725, NULL, NULL);
INSERT INTO `currency_rates` VALUES (102, 'MWK', 825.239292, NULL, NULL);
INSERT INTO `currency_rates` VALUES (103, 'MXN', 24.963329, NULL, NULL);
INSERT INTO `currency_rates` VALUES (104, 'MYR', 4.810633, NULL, NULL);
INSERT INTO `currency_rates` VALUES (105, 'MZN', 73.591410, NULL, NULL);
INSERT INTO `currency_rates` VALUES (106, 'NAD', 18.121621, NULL, NULL);
INSERT INTO `currency_rates` VALUES (107, 'NGN', 408.099790, NULL, NULL);
INSERT INTO `currency_rates` VALUES (108, 'NIO', 37.844015, NULL, NULL);
INSERT INTO `currency_rates` VALUES (109, 'NOK', 11.405599, NULL, NULL);
INSERT INTO `currency_rates` VALUES (110, 'NPR', 132.508354, NULL, NULL);
INSERT INTO `currency_rates` VALUES (111, 'NZD', 1.847363, NULL, NULL);
INSERT INTO `currency_rates` VALUES (112, 'OMR', 0.429801, NULL, NULL);
INSERT INTO `currency_rates` VALUES (113, 'PAB', 1.121880, NULL, NULL);
INSERT INTO `currency_rates` VALUES (114, 'PEN', 3.958258, NULL, NULL);
INSERT INTO `currency_rates` VALUES (115, 'PGK', 3.838505, NULL, NULL);
INSERT INTO `currency_rates` VALUES (116, 'PHP', 57.698037, NULL, NULL);
INSERT INTO `currency_rates` VALUES (117, 'PKR', 176.121721, NULL, NULL);
INSERT INTO `currency_rates` VALUES (118, 'PLN', 4.386058, NULL, NULL);
INSERT INTO `currency_rates` VALUES (119, 'PYG', 7386.917924, NULL, NULL);
INSERT INTO `currency_rates` VALUES (120, 'QAR', 4.065302, NULL, NULL);
INSERT INTO `currency_rates` VALUES (121, 'RON', 4.826717, NULL, NULL);
INSERT INTO `currency_rates` VALUES (122, 'RSD', 117.627735, NULL, NULL);
INSERT INTO `currency_rates` VALUES (123, 'RUB', 83.568390, NULL, NULL);
INSERT INTO `currency_rates` VALUES (124, 'RWF', 1067.822267, NULL, NULL);
INSERT INTO `currency_rates` VALUES (125, 'SAR', 4.190432, NULL, NULL);
INSERT INTO `currency_rates` VALUES (126, 'SBD', 9.235251, NULL, NULL);
INSERT INTO `currency_rates` VALUES (127, 'SCR', 14.529548, NULL, NULL);
INSERT INTO `currency_rates` VALUES (128, 'SDG', 61.772847, NULL, NULL);
INSERT INTO `currency_rates` VALUES (129, 'SEK', 10.785247, NULL, NULL);
INSERT INTO `currency_rates` VALUES (130, 'SGD', 1.587844, NULL, NULL);
INSERT INTO `currency_rates` VALUES (131, 'SHP', 0.908257, NULL, NULL);
INSERT INTO `currency_rates` VALUES (132, 'SLL', 9999.999999, NULL, NULL);
INSERT INTO `currency_rates` VALUES (133, 'SOS', 653.732410, NULL, NULL);
INSERT INTO `currency_rates` VALUES (134, 'SRD', 8.327212, NULL, NULL);
INSERT INTO `currency_rates` VALUES (135, 'STD', 9999.999999, NULL, NULL);
INSERT INTO `currency_rates` VALUES (136, 'SVC', 9.816821, NULL, NULL);
INSERT INTO `currency_rates` VALUES (137, 'SYP', 575.019506, NULL, NULL);
INSERT INTO `currency_rates` VALUES (138, 'SZL', 18.038821, NULL, NULL);
INSERT INTO `currency_rates` VALUES (139, 'THB', 35.884679, NULL, NULL);
INSERT INTO `currency_rates` VALUES (140, 'TJS', 10.875343, NULL, NULL);
INSERT INTO `currency_rates` VALUES (141, 'TMT', 3.907909, NULL, NULL);
INSERT INTO `currency_rates` VALUES (142, 'TND', 3.186636, NULL, NULL);
INSERT INTO `currency_rates` VALUES (143, 'TOP', 2.635661, NULL, NULL);
INSERT INTO `currency_rates` VALUES (144, 'TRY', 7.131927, NULL, NULL);
INSERT INTO `currency_rates` VALUES (145, 'TTD', 7.585158, NULL, NULL);
INSERT INTO `currency_rates` VALUES (146, 'TWD', 33.739208, NULL, NULL);
INSERT INTO `currency_rates` VALUES (147, 'TZS', 2582.397529, NULL, NULL);
INSERT INTO `currency_rates` VALUES (148, 'UAH', 29.335146, NULL, NULL);
INSERT INTO `currency_rates` VALUES (149, 'UGX', 4169.685347, NULL, NULL);
INSERT INTO `currency_rates` VALUES (150, 'USD', 1.116545, NULL, NULL);
INSERT INTO `currency_rates` VALUES (151, 'UYU', 48.718630, NULL, NULL);
INSERT INTO `currency_rates` VALUES (152, 'UZS', 9999.999999, NULL, NULL);
INSERT INTO `currency_rates` VALUES (153, 'VEF', 11.151499, NULL, NULL);
INSERT INTO `currency_rates` VALUES (154, 'VND', 9999.999999, NULL, NULL);
INSERT INTO `currency_rates` VALUES (155, 'VUV', 133.944917, NULL, NULL);
INSERT INTO `currency_rates` VALUES (156, 'WST', 3.074259, NULL, NULL);
INSERT INTO `currency_rates` VALUES (157, 'XAF', 659.652615, NULL, NULL);
INSERT INTO `currency_rates` VALUES (158, 'XAG', 0.088073, NULL, NULL);
INSERT INTO `currency_rates` VALUES (159, 'XAU', 0.000756, NULL, NULL);
INSERT INTO `currency_rates` VALUES (160, 'XCD', 3.017519, NULL, NULL);
INSERT INTO `currency_rates` VALUES (161, 'XDR', 0.809234, NULL, NULL);
INSERT INTO `currency_rates` VALUES (162, 'XOF', 659.646672, NULL, NULL);
INSERT INTO `currency_rates` VALUES (163, 'XPF', 119.931356, NULL, NULL);
INSERT INTO `currency_rates` VALUES (164, 'YER', 279.475009, NULL, NULL);
INSERT INTO `currency_rates` VALUES (165, 'ZAR', 18.603040, NULL, NULL);
INSERT INTO `currency_rates` VALUES (166, 'ZMK', 9999.999999, NULL, NULL);
INSERT INTO `currency_rates` VALUES (167, 'ZMW', 17.892580, NULL, NULL);
INSERT INTO `currency_rates` VALUES (168, 'ZWL', 359.527584, NULL, NULL);

-- ----------------------------
-- Table structure for current_stocks
-- ----------------------------
DROP TABLE IF EXISTS `current_stocks`;
CREATE TABLE `current_stocks`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `product_id` bigint NOT NULL,
  `quantity` decimal(8, 2) NOT NULL,
  `company_id` bigint NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of current_stocks
-- ----------------------------

-- ----------------------------
-- Table structure for email_templates
-- ----------------------------
DROP TABLE IF EXISTS `email_templates`;
CREATE TABLE `email_templates`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of email_templates
-- ----------------------------
INSERT INTO `email_templates` VALUES (1, 'registration', 'Registrasi Berhasil', '<div style=\"padding: 15px 30px;\">\r\n<h2 style=\"color: #555555;\">Pendaftaran Akun Berhasil</h2>\r\n<p style=\"color: #555555;\">Hai, {name}!,<br /><span style=\"color: #555555;\">Selamat! Akun anda berhasil diperbarui. Nikmati kemudahan layanan akses mandiri pembuatan aplikasi (self service access builder apps) di Ngelapak. Mulai sekarang kamu bisa buat toko online impian berbasis website dan aplikasi mobile tanpa ribet coding.<br />Jika ada pertanyaan, hubungi kami di contact@ngelapak.co.id. <br /><br /></span>Salam,</p>\r\n<p style=\"color: #555555;\">Tim Ngelapak</p>\r\n<p>&nbsp;</p>\r\n<p style=\"color: #555555;\"><span style=\"color: #555555;\">&nbsp;</span></p>\r\n</div>', NULL, '2021-10-27 05:20:42');
INSERT INTO `email_templates` VALUES (2, 'premium_membership', 'Langganan Premium', '<div style=\"padding: 15px 30px;\">\r\n<h2 style=\"color: #555555;\">Langganan Premium Ngelapak</h2>\r\n<p style=\"color: #555555;\">Hai {name},<br /><span style=\"color: #555555;\">Selamat pembayaran Anda berhasil dilakukan. Langganan Anda saat ini berlaku sampai <strong>{valid_to}</strong></span><span style=\"color: #555555;\"><strong>.</strong>&nbsp;</span></p>\r\n<p><br style=\"color: #555555;\" /><span style=\"color: #555555;\">Terima Kasih</span><br style=\"color: #555555;\" /><span style=\"color: #555555;\">Tim Ngelapak</span></p>\r\n</div>', NULL, '2021-10-27 05:22:48');
INSERT INTO `email_templates` VALUES (3, 'alert_notification', 'Pembaharuan Ngelapak', '<div style=\"padding: 15px 30px;\">\r\n<h2 style=\"color: #555555;\">Pemberitahuan Perpanjangan Akun</h2>\r\n<p style=\"color: #555555;\">Hai {name},<br />Paket Anda akan kedaluwarsa pada {valid_to} sehingga Anda harus memperbaruinya saat itu agar akun Anda tetap aktif.</p>\r\n<p><br style=\"color: #555555;\" /><span style=\"color: #555555;\">Salam,</span></p>\r\n<p><br style=\"color: #555555;\" /><span style=\"color: #555555;\">Tim Ngelapak</span></p>\r\n</div>', NULL, '2021-10-27 05:24:44');

-- ----------------------------
-- Table structure for file_manager
-- ----------------------------
DROP TABLE IF EXISTS `file_manager`;
CREATE TABLE `file_manager`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `mime_type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `is_dir` varchar(3) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'no',
  `file` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `parent_id` bigint NULL DEFAULT NULL,
  `company_id` bigint NOT NULL,
  `created_by` bigint NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of file_manager
-- ----------------------------

-- ----------------------------
-- Table structure for files
-- ----------------------------
DROP TABLE IF EXISTS `files`;
CREATE TABLE `files`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `related_to` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `related_id` bigint NULL DEFAULT NULL,
  `file` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint NOT NULL,
  `company_id` bigint NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 33 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of files
-- ----------------------------
INSERT INTO `files` VALUES (1, 'projects', 1, '/home/tesngela/public_html/public/uploads/project_files/1_project.supra', 6, 5, '2021-10-08 22:26:35', '2021-10-08 22:26:35');
INSERT INTO `files` VALUES (2, 'projects', 2, '/home/ngelapak/dev2.ngelapak.co.id/public/uploads/project_files/2_project.supra', 47, 46, '2021-10-14 20:42:54', '2021-10-14 20:42:54');
INSERT INTO `files` VALUES (3, 'projects', 3, '/home/ngelapak/public_html/public/uploads/project_files/3_project.supra', 69, 68, '2021-10-21 15:32:51', '2021-10-21 15:32:51');
INSERT INTO `files` VALUES (4, 'projects', 4, '/home/ngelapak/public_html/public/uploads/project_files/4_project.supra', 69, 68, '2021-10-21 17:56:05', '2021-10-21 17:56:05');
INSERT INTO `files` VALUES (5, 'projects', 5, '/home/ngelapak/public_html/public/uploads/project_files/5_project.supra', 69, 68, '2021-10-22 11:24:51', '2021-10-22 11:24:51');
INSERT INTO `files` VALUES (6, 'projects', 6, '/home/ngelapak/public_html/public/uploads/project_files/6_project.supra', 69, 68, '2021-10-26 13:29:07', '2021-10-26 13:29:07');
INSERT INTO `files` VALUES (7, 'projects', 7, '/home/ngelapak/public_html/public/uploads/project_files/7_project.supra', 121, 173, '2021-10-27 12:52:58', '2021-10-27 12:52:58');
INSERT INTO `files` VALUES (8, 'projects', 8, '/home/ngelapak/public_html/public/uploads/project_files/8_project.supra', 121, 173, '2021-10-27 13:12:59', '2021-10-27 13:12:59');
INSERT INTO `files` VALUES (9, 'projects', 9, '/home/ngelapak/public_html/public/uploads/project_files/9_project.supra', 121, 173, '2021-10-27 13:18:42', '2021-10-27 13:18:42');
INSERT INTO `files` VALUES (10, 'projects', 10, '/home/ngelapak/public_html/public/uploads/project_files/10_project.supra', 126, 178, '2021-10-28 12:46:20', '2021-10-28 12:46:20');
INSERT INTO `files` VALUES (11, 'projects', 11, '/home/ngelapak/public_html/public/uploads/project_files/11_project.supra', 133, 185, '2021-10-28 13:46:17', '2021-10-28 13:46:17');
INSERT INTO `files` VALUES (12, 'projects', 12, '/home/ngelapak/public_html/public/uploads/project_files/12_project.supra', 135, 190, '2021-10-28 20:23:32', '2021-10-28 20:23:32');
INSERT INTO `files` VALUES (13, 'projects', 13, '/home/ngelapak/public_html/public/uploads/project_files/13_project.supra', 139, 194, '2021-10-31 21:32:26', '2021-10-31 21:32:26');
INSERT INTO `files` VALUES (14, 'projects', 14, '/home/ngelapak/public_html/public/uploads/project_files/14_project.supra', 139, 194, '2021-10-31 21:37:18', '2021-10-31 21:37:18');
INSERT INTO `files` VALUES (15, 'projects', 15, '/home/ngelapak/public_html/public/uploads/project_files/15_project.supra', 138, 193, '2021-11-04 10:55:34', '2021-11-04 10:55:34');
INSERT INTO `files` VALUES (16, 'projects', 16, '/home/ngelapak/public_html/public/uploads/project_files/16_project.supra', 138, 193, '2021-11-04 11:57:47', '2021-11-04 11:57:47');
INSERT INTO `files` VALUES (17, 'projects', 17, '/home/ngelapak/public_html/public/uploads/project_files/17_project.supra', 148, 203, '2021-11-05 13:56:40', '2021-11-05 13:56:40');
INSERT INTO `files` VALUES (18, 'projects', 18, '/home/ngelapak/public_html/public/uploads/project_files/18_project.supra', 148, 203, '2021-11-09 10:49:38', '2021-11-09 10:49:38');
INSERT INTO `files` VALUES (19, 'projects', 19, '/home/ngelapak/public_html/public/uploads/project_files/19_project.supra', 148, 203, '2021-11-09 10:54:50', '2021-11-09 10:54:50');
INSERT INTO `files` VALUES (20, 'projects', 20, '/home/ngelapak/public_html/public/uploads/project_files/20_project.supra', 148, 203, '2021-11-09 11:00:43', '2021-11-09 11:00:43');
INSERT INTO `files` VALUES (21, 'projects', 21, '/home/ngelapak/public_html/public/uploads/project_files/21_project.supra', 148, 203, '2021-11-09 11:16:22', '2021-11-09 11:16:22');
INSERT INTO `files` VALUES (22, 'projects', 22, '/home/ngelapak/public_html/public/uploads/project_files/22_project.supra', 148, 203, '2021-11-10 12:20:53', '2021-11-10 12:20:53');
INSERT INTO `files` VALUES (23, 'projects', 23, '/home/ngelapak/public_html/public/uploads/project_files/23_project.supra', 126, 178, '2021-11-16 22:55:53', '2021-11-16 22:55:53');
INSERT INTO `files` VALUES (24, 'projects', 24, '/home/ngelapak/public_html/public/uploads/project_files/24_project.supra', 172, 234, '2021-11-18 10:21:32', '2021-11-18 10:21:32');
INSERT INTO `files` VALUES (25, 'projects', 25, '/home/ngelapak/public_html/public/uploads/project_files/25_project.supra', 172, 234, '2021-11-18 15:08:40', '2021-11-18 15:08:40');
INSERT INTO `files` VALUES (26, 'projects', 26, '/home/ngelapak/public_html/public/uploads/project_files/26_project.supra', 172, 234, '2021-11-25 17:09:55', '2021-11-25 17:09:55');
INSERT INTO `files` VALUES (27, 'projects', 27, '/home/ngelapak/public_html/public/uploads/project_files/27_project.supra', 205, 268, '2021-12-30 10:07:21', '2021-12-30 10:07:21');
INSERT INTO `files` VALUES (28, 'projects', 28, '/home/ngelapak/public_html/public/uploads/project_files/28_project.supra', 172, 234, '2021-12-30 10:48:29', '2021-12-30 10:48:29');
INSERT INTO `files` VALUES (29, 'projects', 29, '/home/ngelapak/public_html/public/uploads/project_files/29_project.supra', 210, 306, '2022-01-07 13:28:03', '2022-01-07 13:28:03');
INSERT INTO `files` VALUES (30, 'projects', 30, '/home/ngelapak/public_html/public/uploads/project_files/30_project.supra', 211, 307, '2022-01-24 11:26:32', '2022-01-24 11:26:32');
INSERT INTO `files` VALUES (31, 'projects', 31, '/home/ngelapak/public_html/public/uploads/project_files/31_project.supra', 210, 306, '2022-01-24 14:56:41', '2022-01-24 14:56:41');
INSERT INTO `files` VALUES (32, 'projects', 32, '/home/ngelapak/public_html/public/uploads/project_files/32_project.supra', 210, 306, '2022-02-04 12:55:11', '2022-02-04 12:55:11');

-- ----------------------------
-- Table structure for group_chat_message_status
-- ----------------------------
DROP TABLE IF EXISTS `group_chat_message_status`;
CREATE TABLE `group_chat_message_status`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `message_id` bigint NOT NULL,
  `group_id` bigint NOT NULL,
  `user_id` bigint NOT NULL,
  `company_id` bigint NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of group_chat_message_status
-- ----------------------------

-- ----------------------------
-- Table structure for group_chat_messages
-- ----------------------------
DROP TABLE IF EXISTS `group_chat_messages`;
CREATE TABLE `group_chat_messages`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `group_id` bigint NOT NULL,
  `message` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `attachment` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `sender_id` bigint NOT NULL,
  `company_id` bigint NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of group_chat_messages
-- ----------------------------

-- ----------------------------
-- Table structure for invoice_items
-- ----------------------------
DROP TABLE IF EXISTS `invoice_items`;
CREATE TABLE `invoice_items`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `invoice_id` bigint NOT NULL,
  `item_id` bigint NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `quantity` decimal(10, 2) NOT NULL,
  `unit_cost` decimal(10, 2) NOT NULL,
  `discount` decimal(10, 2) NOT NULL,
  `tax_method` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `tax_id` bigint NULL DEFAULT NULL,
  `tax_amount` decimal(10, 2) NULL DEFAULT NULL,
  `sub_total` decimal(10, 2) NOT NULL,
  `company_id` bigint NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of invoice_items
-- ----------------------------

-- ----------------------------
-- Table structure for invoice_templates
-- ----------------------------
DROP TABLE IF EXISTS `invoice_templates`;
CREATE TABLE `invoice_templates`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `editor` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `custom_css` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `company_id` bigint NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of invoice_templates
-- ----------------------------

-- ----------------------------
-- Table structure for invoices
-- ----------------------------
DROP TABLE IF EXISTS `invoices`;
CREATE TABLE `invoices`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `invoice_number` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `invoice_date` date NOT NULL,
  `due_date` date NOT NULL,
  `grand_total` decimal(10, 2) NOT NULL,
  `tax_total` decimal(10, 2) NOT NULL,
  `paid` decimal(10, 2) NULL DEFAULT NULL,
  `converted_total` decimal(10, 2) NULL DEFAULT NULL,
  `status` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `template` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `note` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `related_to` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `related_id` bigint NULL DEFAULT NULL,
  `client_id` bigint NOT NULL,
  `company_id` bigint NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of invoices
-- ----------------------------

-- ----------------------------
-- Table structure for items
-- ----------------------------
DROP TABLE IF EXISTS `items`;
CREATE TABLE `items`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `item_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `item_type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_id` bigint NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of items
-- ----------------------------

-- ----------------------------
-- Table structure for kategori
-- ----------------------------
DROP TABLE IF EXISTS `kategori`;
CREATE TABLE `kategori`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `kategori` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of kategori
-- ----------------------------
INSERT INTO `kategori` VALUES (2, 'Mode dan Gaya Hidup');
INSERT INTO `kategori` VALUES (3, 'Makanan dan Minuman');
INSERT INTO `kategori` VALUES (4, 'Jasa');
INSERT INTO `kategori` VALUES (5, 'Manufaktur');
INSERT INTO `kategori` VALUES (6, 'Edukasi');
INSERT INTO `kategori` VALUES (7, 'Lainnya');

-- ----------------------------
-- Table structure for lead_sources
-- ----------------------------
DROP TABLE IF EXISTS `lead_sources`;
CREATE TABLE `lead_sources`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int NULL DEFAULT NULL,
  `company_id` bigint NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of lead_sources
-- ----------------------------

-- ----------------------------
-- Table structure for lead_statuses
-- ----------------------------
DROP TABLE IF EXISTS `lead_statuses`;
CREATE TABLE `lead_statuses`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `color` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int NULL DEFAULT NULL,
  `company_id` bigint NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of lead_statuses
-- ----------------------------

-- ----------------------------
-- Table structure for leads
-- ----------------------------
DROP TABLE IF EXISTS `leads`;
CREATE TABLE `leads`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `converted_lead` int NULL DEFAULT NULL,
  `lead_status_id` bigint NOT NULL,
  `lead_source_id` bigint NOT NULL,
  `assigned_user_id` bigint NOT NULL,
  `created_user_id` bigint NOT NULL,
  `contact_date` date NOT NULL,
  `phone` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `website` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `country` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `currency` varchar(3) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `vat_id` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `reg_no` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `city` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `state` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `zip` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `address` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `custom_fields` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `company_id` bigint NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of leads
-- ----------------------------

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 63 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES (1, '2014_10_12_000000_create_users_table', 1);
INSERT INTO `migrations` VALUES (2, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO `migrations` VALUES (3, '2018_06_01_080940_create_settings_table', 1);
INSERT INTO `migrations` VALUES (4, '2018_08_29_084110_create_permissions_table', 1);
INSERT INTO `migrations` VALUES (5, '2018_10_28_101819_create_contact_groups_table', 1);
INSERT INTO `migrations` VALUES (6, '2018_10_28_104344_create_contacts_table', 1);
INSERT INTO `migrations` VALUES (7, '2018_10_28_151911_create_taxs_table', 1);
INSERT INTO `migrations` VALUES (8, '2018_10_29_095644_create_items_table', 1);
INSERT INTO `migrations` VALUES (9, '2018_10_29_100449_create_products_table', 1);
INSERT INTO `migrations` VALUES (10, '2018_10_29_101301_create_services_table', 1);
INSERT INTO `migrations` VALUES (11, '2018_10_29_101756_create_suppliers_table', 1);
INSERT INTO `migrations` VALUES (12, '2018_11_12_152015_create_email_templates_table', 1);
INSERT INTO `migrations` VALUES (13, '2018_11_13_063551_create_accounts_table', 1);
INSERT INTO `migrations` VALUES (14, '2018_11_13_082226_create_chart_of_accounts_table', 1);
INSERT INTO `migrations` VALUES (15, '2018_11_13_082512_create_payment_methods_table', 1);
INSERT INTO `migrations` VALUES (16, '2018_11_13_141249_create_transactions_table', 1);
INSERT INTO `migrations` VALUES (17, '2018_11_14_134254_create_repeating_transactions_table', 1);
INSERT INTO `migrations` VALUES (18, '2018_11_17_142037_create_payment_histories_table', 1);
INSERT INTO `migrations` VALUES (19, '2019_03_07_084028_create_purchase_orders_table', 1);
INSERT INTO `migrations` VALUES (20, '2019_03_07_085537_create_purchase_order_items_table', 1);
INSERT INTO `migrations` VALUES (21, '2019_03_19_070903_create_current_stocks_table', 1);
INSERT INTO `migrations` VALUES (22, '2019_03_19_123527_create_company_settings_table', 1);
INSERT INTO `migrations` VALUES (23, '2019_03_19_133922_create_product_units_table', 1);
INSERT INTO `migrations` VALUES (24, '2019_03_20_113605_create_invoices_table', 1);
INSERT INTO `migrations` VALUES (25, '2019_03_20_113618_create_invoice_items_table', 1);
INSERT INTO `migrations` VALUES (26, '2019_05_11_080519_create_purchase_return_table', 1);
INSERT INTO `migrations` VALUES (27, '2019_05_11_080546_create_purchase_return_items_table', 1);
INSERT INTO `migrations` VALUES (28, '2019_05_27_153656_create_quotations_table', 1);
INSERT INTO `migrations` VALUES (29, '2019_05_27_153712_create_quotation_items_table', 1);
INSERT INTO `migrations` VALUES (30, '2019_06_22_062221_create_sales_return_table', 1);
INSERT INTO `migrations` VALUES (31, '2019_06_22_062233_create_sales_return_items_table', 1);
INSERT INTO `migrations` VALUES (32, '2019_06_23_055645_create_company_email_template_table', 1);
INSERT INTO `migrations` VALUES (33, '2019_10_31_172912_create_social_google_accounts_table', 1);
INSERT INTO `migrations` VALUES (34, '2019_11_04_133151_create_chat_messages_table', 1);
INSERT INTO `migrations` VALUES (35, '2019_11_07_105822_create_chat_groups_table', 1);
INSERT INTO `migrations` VALUES (36, '2019_11_08_063856_create_chat_group_users', 1);
INSERT INTO `migrations` VALUES (37, '2019_11_08_143329_create_group_chat_messages_table', 1);
INSERT INTO `migrations` VALUES (38, '2019_11_08_143607_create_group_chat_message_status_table', 1);
INSERT INTO `migrations` VALUES (39, '2019_11_11_170656_create_file_manager_table', 1);
INSERT INTO `migrations` VALUES (40, '2020_03_15_154649_create_currency_rates_table', 1);
INSERT INTO `migrations` VALUES (41, '2020_03_21_052934_create_companies_table', 1);
INSERT INTO `migrations` VALUES (42, '2020_03_21_070022_create_packages_table', 1);
INSERT INTO `migrations` VALUES (43, '2020_04_02_155956_create_cm_features_table', 1);
INSERT INTO `migrations` VALUES (44, '2020_04_02_160209_create_cm_faqs_table', 1);
INSERT INTO `migrations` VALUES (45, '2020_04_02_160249_create_cm_email_subscribers_table', 1);
INSERT INTO `migrations` VALUES (46, '2020_05_18_104400_create_invoice_templates_table', 1);
INSERT INTO `migrations` VALUES (47, '2020_05_24_152947_create_lead_statuses_table', 1);
INSERT INTO `migrations` VALUES (48, '2020_05_24_153000_create_lead_sources_table', 1);
INSERT INTO `migrations` VALUES (49, '2020_05_24_153224_create_leads_table', 1);
INSERT INTO `migrations` VALUES (50, '2020_06_03_112519_create_files_table', 1);
INSERT INTO `migrations` VALUES (51, '2020_06_03_112538_create_notes_table', 1);
INSERT INTO `migrations` VALUES (52, '2020_06_03_112553_create_activity_logs_table', 1);
INSERT INTO `migrations` VALUES (53, '2020_06_22_083001_create_projects_table', 1);
INSERT INTO `migrations` VALUES (54, '2020_06_22_095143_create_project_members_table', 1);
INSERT INTO `migrations` VALUES (55, '2020_06_23_083455_create_project_milestones_table', 1);
INSERT INTO `migrations` VALUES (56, '2020_06_23_112159_create_task_statuses_table', 1);
INSERT INTO `migrations` VALUES (57, '2020_06_23_144512_create_tasks_table', 1);
INSERT INTO `migrations` VALUES (58, '2020_06_25_065937_create_timesheets_table', 1);
INSERT INTO `migrations` VALUES (59, '2020_06_27_152210_create_notifications_table', 1);
INSERT INTO `migrations` VALUES (60, '2020_08_21_063443_add_related_to_company_email_template', 1);
INSERT INTO `migrations` VALUES (61, '2020_10_19_082621_create_staff_roles_table', 1);
INSERT INTO `migrations` VALUES (62, '2020_10_20_080849_add_description_to_invoice_items', 1);

-- ----------------------------
-- Table structure for notes
-- ----------------------------
DROP TABLE IF EXISTS `notes`;
CREATE TABLE `notes`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `related_to` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `related_id` bigint NULL DEFAULT NULL,
  `note` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint NOT NULL,
  `company_id` bigint NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of notes
-- ----------------------------

-- ----------------------------
-- Table structure for notifications
-- ----------------------------
DROP TABLE IF EXISTS `notifications`;
CREATE TABLE `notifications`  (
  `id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_id` bigint UNSIGNED NOT NULL,
  `data` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint NOT NULL,
  `read_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `notifications_notifiable_type_notifiable_id_index`(`notifiable_type`, `notifiable_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of notifications
-- ----------------------------

-- ----------------------------
-- Table structure for oauth_access_tokens
-- ----------------------------
DROP TABLE IF EXISTS `oauth_access_tokens`;
CREATE TABLE `oauth_access_tokens`  (
  `id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint NULL DEFAULT NULL,
  `client_id` int UNSIGNED NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `scopes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `oauth_access_tokens_user_id_index`(`user_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of oauth_access_tokens
-- ----------------------------
INSERT INTO `oauth_access_tokens` VALUES ('083194b2f913d489473d2f9c88496a09d90106687c1a8f01ec8fdaa7e37e8b890655f5be0e44dc9b', 207, 1, 'authToken', '[]', 0, '2022-01-06 13:03:22', '2022-01-06 13:03:22', '2023-01-06 13:03:22');
INSERT INTO `oauth_access_tokens` VALUES ('0930783a95e4ab3430ebc73d2eea5f68fd20a1c527600696ee41aa3145ccb72cbe6a161239b3c257', 205, 1, 'authToken', '[]', 0, '2021-11-30 10:33:21', '2021-11-30 10:33:21', '2022-11-30 10:33:21');
INSERT INTO `oauth_access_tokens` VALUES ('1805baae3099f279db9b95dd04685965b1a651258585fa056a9881c6ff4faaff9236a06c8d7331f3', 217, 1, 'authToken', '[]', 0, '2021-11-30 11:26:19', '2021-11-30 11:26:19', '2022-11-30 11:26:19');
INSERT INTO `oauth_access_tokens` VALUES ('20b13b1f3a63443010c74cd248dc9308bbecf8478f0b0672150d67d66981afe3d6557ab000e99c51', 204, 1, 'authToken', '[]', 0, '2021-11-30 13:26:06', '2021-11-30 13:26:06', '2022-11-30 13:26:06');
INSERT INTO `oauth_access_tokens` VALUES ('216cf4ff7018b18b57af67c5d33807c419dd47587520ca122009e4b39e718e2cbd29c50ef05ebfe1', 172, 1, 'authToken', '[]', 0, '2021-11-30 13:21:58', '2021-11-30 13:21:58', '2022-11-30 13:21:58');
INSERT INTO `oauth_access_tokens` VALUES ('26535e4937919e8cddb9d193ca816ad7e3849edd34f05a018912c4375b3362b02c58b43e8aecd5b0', 204, 1, 'authToken', '[]', 0, '2021-11-30 10:32:06', '2021-11-30 10:32:06', '2022-11-30 10:32:06');
INSERT INTO `oauth_access_tokens` VALUES ('28168ebb3066db92df4e8616ddfa5abd8906e4168c1c8cc570c074ca1d214cf8bf88881ca2d2ae26', 172, 1, NULL, '[]', 0, '2021-11-29 13:53:49', '2021-11-29 13:53:49', '2022-11-29 13:53:49');
INSERT INTO `oauth_access_tokens` VALUES ('40df0b4588533525f99d5261e9bf3a83c1a32bd1938cd4186770c48b6792d18b67541e52c0726f3c', 206, 1, 'authToken', '[]', 0, '2021-11-30 10:34:11', '2021-11-30 10:34:11', '2022-11-30 10:34:11');
INSERT INTO `oauth_access_tokens` VALUES ('43aba0a0bf1a1f4a63df7e874df530ed72ffdce69c39fb415b4ed7b0296dc5c87c7ba06a85fd26e4', 216, 1, 'authToken', '[]', 0, '2021-11-30 11:21:14', '2021-11-30 11:21:14', '2022-11-30 11:21:14');
INSERT INTO `oauth_access_tokens` VALUES ('46d4bcd74459ea01f7e7a5a8a1a37ec397eae2889f9b01b2c89a6eaf7f078b25bbd25cdedd9e2957', 148, 1, 'authToken', '[]', 0, '2021-11-30 10:06:11', '2021-11-30 10:06:11', '2022-11-30 10:06:11');
INSERT INTO `oauth_access_tokens` VALUES ('46e80c0d55563927772333774d69c5f7753434012a95da34efc285b09511e3c9ef73e4e4ea9be5d8', 218, 1, 'authToken', '[]', 0, '2021-11-30 11:51:38', '2021-11-30 11:51:38', '2022-11-30 11:51:38');
INSERT INTO `oauth_access_tokens` VALUES ('5a1e530c5c42b6f19cc66bc0aed55d76d771588078c47962188c44fadef994c3349e607de2b0478a', 172, 1, 'authToken', '[]', 0, '2021-11-30 10:01:45', '2021-11-30 10:01:45', '2022-11-30 10:01:45');
INSERT INTO `oauth_access_tokens` VALUES ('5d30858c23dde1ce9db62945921b9371862c7dba0f3c1bd7b69e5b15ac3f7f321ce709799436f84c', 172, 1, 'authToken', '[]', 0, '2021-11-30 10:05:50', '2021-11-30 10:05:50', '2022-11-30 10:05:50');
INSERT INTO `oauth_access_tokens` VALUES ('8df5d3250eb322bc1eb300a4f4384d4acd140c1cc88b81fceb216a6ba020ebf062addd41aefe915f', 172, 1, NULL, '[]', 0, '2021-11-30 09:57:52', '2021-11-30 09:57:52', '2022-11-30 09:57:52');
INSERT INTO `oauth_access_tokens` VALUES ('91d9ecfc4462d2d7cf3f491d18f884bce20132a0e5a629a29015f1f26905812ccc8577459a047cb7', 206, 1, 'authToken', '[]', 0, '2021-11-30 10:37:20', '2021-11-30 10:37:20', '2022-11-30 10:37:20');
INSERT INTO `oauth_access_tokens` VALUES ('926bf78fc1cf5e4d335a4f2c935ae5f6a83cf06b82dfb8c8655e41cdec92550cca5f37d6e4a0e7e5', 213, 1, 'authToken', '[]', 0, '2022-01-26 15:25:19', '2022-01-26 15:25:19', '2023-01-26 15:25:19');
INSERT INTO `oauth_access_tokens` VALUES ('93cdfbfda9f1b4d95c399ed9ae4e97b0b08c66af57917f1f0f2f112490f941cf6f8d788b6a0b6447', 172, 1, 'authToken', '[]', 0, '2021-11-30 10:06:33', '2021-11-30 10:06:33', '2022-11-30 10:06:33');
INSERT INTO `oauth_access_tokens` VALUES ('979eb7c17a26d63afbc8886a013a0949e594c2b886130acae157263d74f006bc536280d6ac092278', 214, 1, 'authToken', '[]', 0, '2021-11-30 11:20:50', '2021-11-30 11:20:50', '2022-11-30 11:20:50');
INSERT INTO `oauth_access_tokens` VALUES ('b77c00133fd19446f78d205be69c19fc6abe6eff6307cdeb19d4a27c02d495406daa0fef90437540', 172, 1, NULL, '[]', 0, '2021-11-29 13:54:41', '2021-11-29 13:54:41', '2022-11-29 13:54:41');
INSERT INTO `oauth_access_tokens` VALUES ('bf3e043ed6120cbfdb19653241829a740800fa6bbcd8560be184ee62966dc6f293ff85189ce0feeb', 172, 1, NULL, '[]', 0, '2021-11-29 13:54:00', '2021-11-29 13:54:00', '2022-11-29 13:54:00');
INSERT INTO `oauth_access_tokens` VALUES ('cf54ef66acad0b518d7bb95283baf8be31f1bc42261571fa499177ad8453f8b2f0067eebb933d5a2', 203, 1, 'authToken', '[]', 0, '2021-11-30 13:23:56', '2021-11-30 13:23:56', '2022-11-30 13:23:56');
INSERT INTO `oauth_access_tokens` VALUES ('e8dc47dcfd3dd467cda2c01ef76f3018faee3ebe751b400de232968488e17a952e026e8c411d0317', 172, 1, NULL, '[]', 0, '2021-11-29 13:55:11', '2021-11-29 13:55:11', '2022-11-29 13:55:11');

-- ----------------------------
-- Table structure for oauth_auth_codes
-- ----------------------------
DROP TABLE IF EXISTS `oauth_auth_codes`;
CREATE TABLE `oauth_auth_codes`  (
  `id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint NOT NULL,
  `client_id` int UNSIGNED NOT NULL,
  `scopes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of oauth_auth_codes
-- ----------------------------

-- ----------------------------
-- Table structure for oauth_clients
-- ----------------------------
DROP TABLE IF EXISTS `oauth_clients`;
CREATE TABLE `oauth_clients`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint NULL DEFAULT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `oauth_clients_user_id_index`(`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of oauth_clients
-- ----------------------------
INSERT INTO `oauth_clients` VALUES (1, NULL, 'Ngelapak Personal Access Client', 'PYnO95uZGxF44VNXG38vRkw5gDsAc6WQwcwiS7TO', 'http://localhost', 1, 0, 0, '2021-11-29 11:21:10', '2021-11-29 11:21:10');
INSERT INTO `oauth_clients` VALUES (2, NULL, 'Ngelapak Password Grant Client', 'ZJgiuf1P5Ff9xXysxXFJLeJnZ2TqykRfCxDc59PK', 'http://localhost', 0, 1, 0, '2021-11-29 11:21:10', '2021-11-29 11:21:10');

-- ----------------------------
-- Table structure for oauth_personal_access_clients
-- ----------------------------
DROP TABLE IF EXISTS `oauth_personal_access_clients`;
CREATE TABLE `oauth_personal_access_clients`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `client_id` int UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `oauth_personal_access_clients_client_id_index`(`client_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of oauth_personal_access_clients
-- ----------------------------
INSERT INTO `oauth_personal_access_clients` VALUES (1, 1, '2021-11-29 11:21:10', '2021-11-29 11:21:10');

-- ----------------------------
-- Table structure for oauth_refresh_tokens
-- ----------------------------
DROP TABLE IF EXISTS `oauth_refresh_tokens`;
CREATE TABLE `oauth_refresh_tokens`  (
  `id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `oauth_refresh_tokens_access_token_id_index`(`access_token_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of oauth_refresh_tokens
-- ----------------------------

-- ----------------------------
-- Table structure for packages
-- ----------------------------
DROP TABLE IF EXISTS `packages`;
CREATE TABLE `packages`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `package_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `cost_per_month` decimal(10, 2) NOT NULL,
  `cost_per_year` decimal(10, 2) NOT NULL,
  `websites_limit` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `recurring_transaction` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `online_payment` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_featured` tinyint NOT NULL DEFAULT 0,
  `others` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of packages
-- ----------------------------
INSERT INTO `packages` VALUES (1, 'Ngelapak Basic', 99000.00, 1100000.00, 'a:2:{s:7:\"monthly\";s:1:\"1\";s:6:\"yearly\";s:1:\"1\";}', 'a:2:{s:7:\"monthly\";s:3:\"Yes\";s:6:\"yearly\";s:3:\"Yes\";}', 'a:2:{s:7:\"monthly\";s:3:\"Yes\";s:6:\"yearly\";s:3:\"Yes\";}', 1, NULL, NULL, '2021-10-11 16:34:35');
INSERT INTO `packages` VALUES (2, 'Ngelapak Pro', 449000.00, 5300000.00, 'a:2:{s:7:\"monthly\";s:1:\"1\";s:6:\"yearly\";s:1:\"1\";}', 'a:2:{s:7:\"monthly\";s:3:\"Yes\";s:6:\"yearly\";s:3:\"Yes\";}', 'a:2:{s:7:\"monthly\";s:3:\"Yes\";s:6:\"yearly\";s:3:\"Yes\";}', 1, NULL, NULL, '2021-10-11 16:33:03');
INSERT INTO `packages` VALUES (3, 'Ngelapak Advanced', 499000.00, 5900000.00, 'a:2:{s:7:\"monthly\";s:1:\"1\";s:6:\"yearly\";s:1:\"1\";}', 'a:2:{s:7:\"monthly\";s:3:\"Yes\";s:6:\"yearly\";s:3:\"Yes\";}', 'a:2:{s:7:\"monthly\";s:3:\"Yes\";s:6:\"yearly\";s:3:\"Yes\";}', 1, NULL, NULL, '2021-10-11 16:34:19');
INSERT INTO `packages` VALUES (4, 'Trial', 10000.00, 10000.00, 'a:2:{s:7:\"monthly\";s:1:\"1\";s:6:\"yearly\";s:1:\"1\";}', 'a:2:{s:7:\"monthly\";s:3:\"Yes\";s:6:\"yearly\";s:3:\"Yes\";}', 'a:2:{s:7:\"monthly\";s:3:\"Yes\";s:6:\"yearly\";s:3:\"Yes\";}', 1, NULL, '2021-10-11 16:29:11', '2021-10-27 11:14:32');

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets`  (
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  INDEX `password_resets_email_index`(`email`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of password_resets
-- ----------------------------
INSERT INTO `password_resets` VALUES ('testing@gmail.com', '$2y$10$ndX1yCdceIMLfFF1JHVYSOmxVaSj/b9rvOvGlw.Ssu.akfecTjPvO', '2021-10-26 04:32:55');
INSERT INTO `password_resets` VALUES ('pygmy199@gmail.com', '$2y$10$r.GDMoiIrRIyVw6nocYgiuSmoxusTh9foxvafjdjPN9i25Vo0cU2G', '2021-10-26 04:33:19');
INSERT INTO `password_resets` VALUES ('neeturelife@gmail.com', '$2y$10$UucoTZM9Izi7pH4WJBhAG.809E69o9RseCpI41rZpw4ptB.h2dbha', '2021-12-24 14:58:52');
INSERT INTO `password_resets` VALUES ('sobat@ngelapak.co.id', '$2y$10$Y3ZN98o0l6.G8zmYbDFFCu5g4iZsHZL6twmh7VHgE15ePKaodj.uG', '2022-01-25 10:40:45');

-- ----------------------------
-- Table structure for payment_histories
-- ----------------------------
DROP TABLE IF EXISTS `payment_histories`;
CREATE TABLE `payment_histories`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `company_id` bigint NOT NULL,
  `title` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `method` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `currency` varchar(3) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `amount` decimal(10, 2) NOT NULL,
  `package_id` int NOT NULL,
  `package_type` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `sessionID` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `trx_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 187 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of payment_histories
-- ----------------------------
INSERT INTO `payment_histories` VALUES (77, 68, 'Buy Ngelapak Basic Package', 'Ipaymu', 'IDR', 99000.00, 1, 'monthly', 'paid', '2021-10-21 11:56:16', '2021-10-21 11:59:57', '580DD342-C1F4-4EDD-A0BF-02B7B43EA2EB', 'https://my.ipaymu.com/payment/580DD342-C1F4-4EDD-A0BF-02B7B43EA2EB', '5529006');
INSERT INTO `payment_histories` VALUES (122, 178, 'Buy Trial Package', 'Ipaymu', 'IDR', 10000.00, 4, 'monthly', 'paid', '2021-10-27 16:17:26', '2021-10-27 16:21:39', 'A8926232-6F2B-4211-857C-E95D952D62CB', 'https://my.ipaymu.com/payment/A8926232-6F2B-4211-857C-E95D952D62CB', '5591047');
INSERT INTO `payment_histories` VALUES (126, 68, 'Buy Trial Package', 'Ipaymu', 'IDR', 10000.00, 4, 'yearly', 'paid', '2021-10-27 16:56:13', '2021-10-27 16:57:07', '5DEE6CB8-5B3A-408D-BD51-63C6CE0E9FA9', 'https://my.ipaymu.com/payment/5DEE6CB8-5B3A-408D-BD51-63C6CE0E9FA9', '5591465');
INSERT INTO `payment_histories` VALUES (127, 185, 'Buy Trial Package', 'Ipaymu', 'IDR', 10000.00, 4, 'monthly', 'paid', '2021-10-28 13:11:25', '2021-10-28 13:14:06', '8F48752B-51C3-4668-8ABE-CDA8B8F696D8', 'https://my.ipaymu.com/payment/8F48752B-51C3-4668-8ABE-CDA8B8F696D8', '5599161');
INSERT INTO `payment_histories` VALUES (129, 178, 'Buy Ngelapak Pro Package', 'Offline', 'IDR', 449000.00, 2, 'monthly', 'paid', '2021-10-28 18:52:20', '2021-10-28 18:52:20', '', '', '');
INSERT INTO `payment_histories` VALUES (132, 194, 'Buy Ngelapak Pro Package', 'Offline', 'IDR', 449000.00, 2, 'monthly', 'paid', '2021-10-31 20:50:32', '2021-10-31 20:50:32', '', '', '');
INSERT INTO `payment_histories` VALUES (134, 194, 'Buy Ngelapak Basic Package', 'Ipaymu', 'IDR', 99000.00, 1, 'monthly', 'paid', '2021-10-31 21:43:05', '2021-10-31 21:44:15', '47D8D583-D7F4-4FB8-91BC-C0C3692CA963', 'https://my.ipaymu.com/payment/47D8D583-D7F4-4FB8-91BC-C0C3692CA963', '5639137');
INSERT INTO `payment_histories` VALUES (152, 234, 'Buy Ngelapak Advanced Package', 'Ipaymu', 'IDR', 5900000.00, 3, 'yearly', 'paid', '2021-12-10 09:35:00', '2021-12-10 09:35:37', '333BC53F-B104-4223-BD74-4ECF3F6E7913', 'https://sandbox.ipaymu.com/payment/333BC53F-B104-4223-BD74-4ECF3F6E7913', '50622');
INSERT INTO `payment_histories` VALUES (155, 234, 'Buy Ngelapak Pro Package', 'Ipaymu', 'IDR', 449000.00, 2, 'monthly', 'paid', '2021-12-14 13:43:22', '2021-12-14 14:28:06', '00394104-B047-4FD4-B599-09F74E23A3AE', 'https://sandbox.ipaymu.com/payment/00394104-B047-4FD4-B599-09F74E23A3AE', '50957');
INSERT INTO `payment_histories` VALUES (156, 234, 'Buy Ngelapak Advanced Package', 'Ipaymu', 'IDR', 499000.00, 3, 'monthly', 'paid', '2021-12-14 14:33:58', '2021-12-14 14:34:38', '59054A0B-002D-477E-8652-833D691B147A', 'https://sandbox.ipaymu.com/payment/59054A0B-002D-477E-8652-833D691B147A', '50964');
INSERT INTO `payment_histories` VALUES (157, 234, 'Buy Ngelapak Advanced Package', 'Ipaymu', 'IDR', 499000.00, 3, 'monthly', 'paid', '2021-12-14 14:35:32', '2021-12-14 14:35:54', '529AE5E0-9030-4995-93E1-FD91600EDF37', 'https://sandbox.ipaymu.com/payment/529AE5E0-9030-4995-93E1-FD91600EDF37', '50965');
INSERT INTO `payment_histories` VALUES (158, 234, 'Buy Ngelapak Pro Package', 'Ipaymu', 'IDR', 449000.00, 2, 'monthly', 'paid', '2021-12-14 14:39:48', '2021-12-14 14:40:11', '0486788A-CF8E-43A7-A899-9501AF68870A', 'https://sandbox.ipaymu.com/payment/0486788A-CF8E-43A7-A899-9501AF68870A', '50966');
INSERT INTO `payment_histories` VALUES (159, 234, 'Buy Ngelapak Pro Package', 'Ipaymu', 'IDR', 5300000.00, 2, 'yearly', 'paid', '2021-12-14 15:25:55', '2021-12-14 15:26:15', '8BF7FD5B-2F8D-4D7A-AFF9-5C34274D23AD', 'https://sandbox.ipaymu.com/payment/8BF7FD5B-2F8D-4D7A-AFF9-5C34274D23AD', '50970');
INSERT INTO `payment_histories` VALUES (163, 234, 'Buy Ngelapak Pro Package', 'Ipaymu', 'IDR', 5300000.00, 2, 'yearly', 'paid', '2021-12-14 16:48:54', '2021-12-14 16:49:19', 'EB503E9B-76F8-4B1B-8354-01D9AB2B2AE5', 'https://sandbox.ipaymu.com/payment/EB503E9B-76F8-4B1B-8354-01D9AB2B2AE5', '50985');
INSERT INTO `payment_histories` VALUES (164, 234, 'Buy Ngelapak Pro Package', 'Ipaymu', 'IDR', 5300000.00, 2, 'yearly', 'paid', '2021-12-14 16:52:28', '2021-12-14 16:52:47', '764C7E02-B02C-43C9-B536-B0EA6BC47906', 'https://sandbox.ipaymu.com/payment/764C7E02-B02C-43C9-B536-B0EA6BC47906', '50986');
INSERT INTO `payment_histories` VALUES (176, 234, 'Buy Trial Package', 'Ipaymu', 'IDR', 10000.00, 4, 'yearly', 'paid', '2022-01-03 11:10:33', '2022-01-03 11:13:31', '2672EED6-C5E6-4F59-8EA9-70CA2F3D9643', 'https://my.ipaymu.com/payment/2672EED6-C5E6-4F59-8EA9-70CA2F3D9643', '6670000');
INSERT INTO `payment_histories` VALUES (177, 234, 'Buy Trial Package', '', 'IDR', 10000.00, 4, 'yearly', 'pending', '2022-01-05 15:11:12', '2022-01-05 15:11:13', 'B1656219-35AF-4A24-B284-0B3C5EE9200D', 'https://my.ipaymu.com/payment/B1656219-35AF-4A24-B284-0B3C5EE9200D', '');
INSERT INTO `payment_histories` VALUES (178, 234, 'Buy Trial Package', '', 'IDR', 10000.00, 4, 'yearly', 'pending', '2022-01-05 15:11:39', '2022-01-05 15:11:40', 'D2347F45-8601-4E33-A94F-8445F5224D27', 'https://my.ipaymu.com/payment/D2347F45-8601-4E33-A94F-8445F5224D27', '');
INSERT INTO `payment_histories` VALUES (179, 234, 'Buy Trial Package', '', 'IDR', 10000.00, 4, 'yearly', 'pending', '2022-01-05 15:13:32', '2022-01-05 15:13:32', 'AE82F470-7447-43D7-ABA1-674692F1506A', 'https://my.ipaymu.com/payment/AE82F470-7447-43D7-ABA1-674692F1506A', '');
INSERT INTO `payment_histories` VALUES (180, 234, 'Buy Trial Package', '', 'IDR', 10000.00, 4, 'yearly', 'pending', '2022-01-05 15:56:51', '2022-01-05 15:56:51', '', '', '');
INSERT INTO `payment_histories` VALUES (181, 234, 'Buy Trial Package', 'Ipaymu', 'IDR', 10000.00, 4, 'yearly', 'paid', '2022-01-05 16:07:15', '2022-01-05 16:11:35', '525A4400-0C57-4B3C-851F-F815C86C1E13', 'https://my.ipaymu.com/payment/525A4400-0C57-4B3C-851F-F815C86C1E13', '6694834');
INSERT INTO `payment_histories` VALUES (182, 234, 'Buy Trial Package', '', 'IDR', 10000.00, 4, 'yearly', 'pending', '2022-01-05 16:19:51', '2022-01-05 16:19:51', '03362A83-E78E-4F86-BF51-596279D73285', 'https://my.ipaymu.com/payment/03362A83-E78E-4F86-BF51-596279D73285', '');
INSERT INTO `payment_histories` VALUES (183, 306, 'Buy Ngelapak Basic Package', 'Ipaymu', 'IDR', 99000.00, 1, 'monthly', 'paid', '2022-01-07 13:22:15', '2022-01-07 13:24:01', 'F4906DBF-9107-4242-8627-BB634C92B720', 'https://my.ipaymu.com/payment/F4906DBF-9107-4242-8627-BB634C92B720', '6714127');
INSERT INTO `payment_histories` VALUES (184, 310, 'Buy Ngelapak Pro Package', '', 'IDR', 449000.00, 2, 'monthly', 'pending', '2022-02-08 16:59:47', '2022-02-08 16:59:47', '42247081-1F29-4E4C-B409-34B767F9AA29', 'https://my.ipaymu.com/payment/42247081-1F29-4E4C-B409-34B767F9AA29', '');
INSERT INTO `payment_histories` VALUES (185, 212, 'Buy Ngelapak Basic Package', '', 'IDR', 99000.00, 1, 'monthly', 'pending', '2022-02-10 14:32:53', '2022-02-10 14:32:54', 'C4EF1D12-6E0D-4179-8978-646332B2A0AD', 'https://my.ipaymu.com/payment/C4EF1D12-6E0D-4179-8978-646332B2A0AD', '');
INSERT INTO `payment_histories` VALUES (186, 311, 'Buy Trial Package', '', 'IDR', 10000.00, 4, 'monthly', 'pending', '2022-02-14 21:41:07', '2022-02-14 21:41:08', '9EF24DD7-936B-4C4C-8DA8-68ECAE80F6FB', 'https://my.ipaymu.com/payment/9EF24DD7-936B-4C4C-8DA8-68ECAE80F6FB', '');

-- ----------------------------
-- Table structure for payment_methods
-- ----------------------------
DROP TABLE IF EXISTS `payment_methods`;
CREATE TABLE `payment_methods`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_id` bigint NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of payment_methods
-- ----------------------------

-- ----------------------------
-- Table structure for permissions
-- ----------------------------
DROP TABLE IF EXISTS `permissions`;
CREATE TABLE `permissions`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `role_id` bigint NOT NULL,
  `permission` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of permissions
-- ----------------------------
INSERT INTO `permissions` VALUES (1, 1, 'websites.create', '2021-12-30 09:50:59', '2021-12-30 09:50:59');

-- ----------------------------
-- Table structure for product_units
-- ----------------------------
DROP TABLE IF EXISTS `product_units`;
CREATE TABLE `product_units`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `unit_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_id` bigint NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of product_units
-- ----------------------------

-- ----------------------------
-- Table structure for products
-- ----------------------------
DROP TABLE IF EXISTS `products`;
CREATE TABLE `products`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `item_id` bigint NOT NULL,
  `supplier_id` bigint NULL DEFAULT NULL,
  `product_cost` decimal(10, 2) NOT NULL,
  `product_price` decimal(10, 2) NOT NULL,
  `product_unit` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `tax_method` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `tax_id` bigint NULL DEFAULT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of products
-- ----------------------------

-- ----------------------------
-- Table structure for project_members
-- ----------------------------
DROP TABLE IF EXISTS `project_members`;
CREATE TABLE `project_members`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `project_id` bigint NOT NULL,
  `user_id` bigint NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of project_members
-- ----------------------------

-- ----------------------------
-- Table structure for project_milestones
-- ----------------------------
DROP TABLE IF EXISTS `project_milestones`;
CREATE TABLE `project_milestones`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `due_date` date NOT NULL,
  `status` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `cost` decimal(10, 2) NULL DEFAULT NULL,
  `project_id` bigint NOT NULL,
  `user_id` bigint NOT NULL,
  `company_id` bigint NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of project_milestones
-- ----------------------------

-- ----------------------------
-- Table structure for projects
-- ----------------------------
DROP TABLE IF EXISTS `projects`;
CREATE TABLE `projects`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `client_id` bigint NOT NULL,
  `progress` int NULL DEFAULT NULL,
  `billing_type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `fixed_rate` decimal(10, 2) NULL DEFAULT NULL,
  `hourly_rate` decimal(10, 2) NULL DEFAULT NULL,
  `start_date` date NOT NULL,
  `end_date` date NULL DEFAULT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `custom_fields` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `user_id` bigint NOT NULL,
  `company_id` bigint NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `ket` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `file` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 33 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of projects
-- ----------------------------
INSERT INTO `projects` VALUES (1, 'Project_2021-10-08_22:26:35', 0, NULL, '', 'lara', NULL, NULL, '0000-00-00', NULL, NULL, NULL, 6, 5, '2021-10-08 22:26:35', '2021-10-08 22:26:35', '', '');
INSERT INTO `projects` VALUES (6, 'Project_2021-10-26_13:29:07', 0, NULL, '', 'lara', NULL, NULL, '0000-00-00', NULL, '<p>sdf</p>', 'testing', 69, 68, '2021-10-26 13:29:07', '2021-10-27 14:05:18', '', '');
INSERT INTO `projects` VALUES (9, 'Lautan.io Landing Page', 0, NULL, '', 'lara', NULL, NULL, '0000-00-00', NULL, '<p>Lautan.io Landing Page</p>', 'lautan', 121, 173, '2021-10-27 13:18:42', '2021-10-27 14:10:11', '', '');
INSERT INTO `projects` VALUES (11, 'Project_2021-10-28_13:46:17', 0, NULL, '', 'lara', NULL, NULL, '0000-00-00', NULL, NULL, NULL, 133, 185, '2021-10-28 13:46:17', '2021-10-28 13:46:17', '', '');
INSERT INTO `projects` VALUES (12, 'Project_2021-10-28_20:23:32', 0, NULL, '', 'lara', NULL, NULL, '0000-00-00', NULL, NULL, NULL, 135, 190, '2021-10-28 20:23:32', '2021-10-28 20:23:32', '', '');
INSERT INTO `projects` VALUES (13, 'Project_2021-10-31_21:32:26', 0, NULL, '', 'lara', NULL, NULL, '0000-00-00', NULL, NULL, NULL, 139, 194, '2021-10-31 21:32:26', '2021-10-31 21:32:26', '', '');
INSERT INTO `projects` VALUES (14, 'Project_2021-10-31_21:37:18', 0, NULL, '', 'lara', NULL, NULL, '0000-00-00', NULL, NULL, NULL, 139, 194, '2021-10-31 21:37:18', '2021-10-31 21:37:18', '', '');
INSERT INTO `projects` VALUES (15, 'Project_2021-11-04_10:55:34', 0, NULL, '', 'lara', NULL, NULL, '0000-00-00', NULL, NULL, NULL, 138, 193, '2021-11-04 10:55:34', '2021-11-04 10:55:34', '', '');
INSERT INTO `projects` VALUES (16, 'Project_2021-11-04_11:57:47', 0, NULL, '', 'lara', NULL, NULL, '0000-00-00', NULL, NULL, NULL, 138, 193, '2021-11-04 11:57:47', '2021-11-04 11:57:47', '', '');
INSERT INTO `projects` VALUES (22, 'tes', 0, NULL, '', 'lara', NULL, NULL, '0000-00-00', NULL, '<p>asdfghjkl</p>', 'tes', 148, 203, '2021-11-10 12:20:53', '2021-11-16 11:16:48', 'Selesai', '61933089e9945_website.zip');
INSERT INTO `projects` VALUES (23, 'Project_2021-11-16_22:55:53', 0, NULL, '', 'lara', NULL, NULL, '0000-00-00', NULL, NULL, NULL, 126, 178, '2021-11-16 22:55:53', '2021-11-16 22:55:53', '', '');
INSERT INTO `projects` VALUES (27, 'Project_2021-12-30_10:07:21', 0, NULL, '', 'lara', NULL, NULL, '0000-00-00', NULL, NULL, NULL, 205, 268, '2021-12-30 10:07:21', '2021-12-30 10:07:21', '', '');
INSERT INTO `projects` VALUES (28, 'Project_2021-12-30_10:48:29', 0, NULL, '', 'lara', NULL, NULL, '0000-00-00', NULL, '<p>we</p>', 'tes', 172, 234, '2021-12-30 10:48:29', '2022-01-07 10:27:44', 'Selesai', 'ChromeSetup.zip');
INSERT INTO `projects` VALUES (30, 'Project_2022-01-24_11:26:32', 0, NULL, '', 'lara', NULL, NULL, '0000-00-00', NULL, NULL, NULL, 211, 307, '2022-01-24 11:26:32', '2022-01-24 11:26:32', '', '');
INSERT INTO `projects` VALUES (32, 'Project_2022-02-04_12:55:11', 0, NULL, '', 'lara', NULL, NULL, '0000-00-00', NULL, NULL, NULL, 210, 306, '2022-02-04 12:55:11', '2022-02-04 12:55:11', '', '');

-- ----------------------------
-- Table structure for provinces
-- ----------------------------
DROP TABLE IF EXISTS `provinces`;
CREATE TABLE `provinces`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `province_id` bigint UNSIGNED NOT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 40 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of provinces
-- ----------------------------
INSERT INTO `provinces` VALUES (6, 1, 'Bali', '2020-03-22 14:06:12', '2020-03-22 14:06:12');
INSERT INTO `provinces` VALUES (7, 2, 'Bangka Belitung', '2020-03-22 14:06:13', '2020-03-22 14:06:13');
INSERT INTO `provinces` VALUES (8, 3, 'Banten', '2020-03-22 14:06:14', '2020-03-22 14:06:14');
INSERT INTO `provinces` VALUES (9, 4, 'Bengkulu', '2020-03-22 14:06:15', '2020-03-22 14:06:15');
INSERT INTO `provinces` VALUES (10, 5, 'DI Yogyakarta', '2020-03-22 14:06:17', '2020-03-22 14:06:17');
INSERT INTO `provinces` VALUES (11, 6, 'DKI Jakarta', '2020-03-22 14:06:19', '2020-03-22 14:06:19');
INSERT INTO `provinces` VALUES (12, 7, 'Gorontalo', '2020-03-22 14:06:23', '2020-03-22 14:06:23');
INSERT INTO `provinces` VALUES (13, 8, 'Jambi', '2020-03-22 14:06:24', '2020-03-22 14:06:24');
INSERT INTO `provinces` VALUES (14, 9, 'Jawa Barat', '2020-03-22 14:06:26', '2020-03-22 14:06:26');
INSERT INTO `provinces` VALUES (15, 10, 'Jawa Tengah', '2020-03-22 14:06:29', '2020-03-22 14:06:29');
INSERT INTO `provinces` VALUES (16, 11, 'Jawa Timur', '2020-03-22 14:06:33', '2020-03-22 14:06:33');
INSERT INTO `provinces` VALUES (17, 12, 'Kalimantan Barat', '2020-03-22 14:06:37', '2020-03-22 14:06:37');
INSERT INTO `provinces` VALUES (18, 13, 'Kalimantan Selatan', '2020-03-22 14:06:38', '2020-03-22 14:06:38');
INSERT INTO `provinces` VALUES (19, 14, 'Kalimantan Tengah', '2020-03-22 14:06:40', '2020-03-22 14:06:40');
INSERT INTO `provinces` VALUES (20, 15, 'Kalimantan Timur', '2020-03-22 14:06:42', '2020-03-22 14:06:42');
INSERT INTO `provinces` VALUES (21, 16, 'Kalimantan Utara', '2020-03-22 14:06:44', '2020-03-22 14:06:44');
INSERT INTO `provinces` VALUES (22, 17, 'Kepulauan Riau', '2020-03-22 14:06:45', '2020-03-22 14:06:45');
INSERT INTO `provinces` VALUES (23, 18, 'Lampung', '2020-03-22 14:07:15', '2020-03-22 14:07:15');
INSERT INTO `provinces` VALUES (24, 19, 'Maluku', '2020-03-22 14:07:17', '2020-03-22 14:07:17');
INSERT INTO `provinces` VALUES (25, 20, 'Maluku Utara', '2020-03-22 14:07:19', '2020-03-22 14:07:19');
INSERT INTO `provinces` VALUES (26, 21, 'Nanggroe Aceh Darussalam (NAD)', '2020-03-22 14:07:21', '2020-03-22 14:07:21');
INSERT INTO `provinces` VALUES (27, 22, 'Nusa Tenggara Barat (NTB)', '2020-03-22 14:07:23', '2020-03-22 14:07:23');
INSERT INTO `provinces` VALUES (28, 23, 'Nusa Tenggara Timur (NTT)', '2020-03-22 14:07:26', '2020-03-22 14:07:26');
INSERT INTO `provinces` VALUES (29, 24, 'Papua', '2020-03-22 14:07:30', '2020-03-22 14:07:30');
INSERT INTO `provinces` VALUES (30, 25, 'Papua Barat', '2020-03-22 14:07:33', '2020-03-22 14:07:33');
INSERT INTO `provinces` VALUES (31, 26, 'Riau', '2020-03-22 14:07:34', '2020-03-22 14:07:34');
INSERT INTO `provinces` VALUES (32, 27, 'Sulawesi Barat', '2020-03-22 14:07:37', '2020-03-22 14:07:37');
INSERT INTO `provinces` VALUES (33, 28, 'Sulawesi Selatan', '2020-03-22 14:07:40', '2020-03-22 14:07:40');
INSERT INTO `provinces` VALUES (34, 29, 'Sulawesi Tengah', '2020-03-22 14:07:43', '2020-03-22 14:07:43');
INSERT INTO `provinces` VALUES (35, 30, 'Sulawesi Tenggara', '2020-03-22 14:07:45', '2020-03-22 14:07:45');
INSERT INTO `provinces` VALUES (36, 31, 'Sulawesi Utara', '2020-03-22 14:07:46', '2020-03-22 14:07:46');
INSERT INTO `provinces` VALUES (37, 32, 'Sumatera Barat', '2020-03-22 14:07:48', '2020-03-22 14:07:48');
INSERT INTO `provinces` VALUES (38, 33, 'Sumatera Selatan', '2020-03-22 14:07:51', '2020-03-22 14:07:51');
INSERT INTO `provinces` VALUES (39, 34, 'Sumatera Utara', '2020-03-22 14:07:53', '2020-03-22 14:07:53');

-- ----------------------------
-- Table structure for purchase_order_items
-- ----------------------------
DROP TABLE IF EXISTS `purchase_order_items`;
CREATE TABLE `purchase_order_items`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `purchase_order_id` bigint NOT NULL,
  `product_id` bigint NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `quantity` decimal(8, 2) NOT NULL,
  `unit_cost` decimal(10, 2) NOT NULL,
  `discount` decimal(10, 2) NOT NULL,
  `tax_method` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `tax_id` bigint NULL DEFAULT NULL,
  `tax_amount` decimal(10, 2) NULL DEFAULT NULL,
  `sub_total` decimal(10, 2) NOT NULL,
  `company_id` bigint NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of purchase_order_items
-- ----------------------------

-- ----------------------------
-- Table structure for purchase_orders
-- ----------------------------
DROP TABLE IF EXISTS `purchase_orders`;
CREATE TABLE `purchase_orders`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `order_date` date NOT NULL,
  `supplier_id` bigint NOT NULL,
  `order_status` tinyint NOT NULL,
  `order_tax_id` bigint NULL DEFAULT NULL,
  `order_tax` decimal(10, 2) NULL DEFAULT NULL,
  `order_discount` decimal(10, 2) NOT NULL,
  `shipping_cost` decimal(10, 2) NOT NULL,
  `product_total` decimal(10, 2) NOT NULL,
  `grand_total` decimal(10, 2) NOT NULL,
  `paid` decimal(10, 2) NOT NULL,
  `payment_status` tinyint NOT NULL,
  `attachemnt` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `note` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `company_id` bigint NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of purchase_orders
-- ----------------------------

-- ----------------------------
-- Table structure for purchase_return
-- ----------------------------
DROP TABLE IF EXISTS `purchase_return`;
CREATE TABLE `purchase_return`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `return_date` date NOT NULL,
  `supplier_id` bigint NULL DEFAULT NULL,
  `account_id` bigint NOT NULL,
  `chart_id` bigint NOT NULL,
  `payment_method_id` bigint NOT NULL,
  `tax_id` bigint NULL DEFAULT NULL,
  `tax_amount` decimal(10, 2) NULL DEFAULT NULL,
  `product_total` decimal(10, 2) NOT NULL,
  `grand_total` decimal(10, 2) NOT NULL,
  `attachemnt` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `note` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `company_id` bigint NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of purchase_return
-- ----------------------------

-- ----------------------------
-- Table structure for purchase_return_items
-- ----------------------------
DROP TABLE IF EXISTS `purchase_return_items`;
CREATE TABLE `purchase_return_items`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `purchase_return_id` int NOT NULL,
  `product_id` bigint NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `quantity` decimal(10, 2) NOT NULL,
  `unit_cost` decimal(10, 2) NOT NULL,
  `discount` decimal(10, 2) NOT NULL,
  `tax_method` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `tax_id` bigint NULL DEFAULT NULL,
  `tax_amount` decimal(10, 2) NULL DEFAULT NULL,
  `sub_total` decimal(10, 2) NOT NULL,
  `company_id` bigint NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of purchase_return_items
-- ----------------------------

-- ----------------------------
-- Table structure for quotation_items
-- ----------------------------
DROP TABLE IF EXISTS `quotation_items`;
CREATE TABLE `quotation_items`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `quotation_id` bigint NOT NULL,
  `item_id` bigint NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `quantity` decimal(10, 2) NOT NULL,
  `unit_cost` decimal(10, 2) NOT NULL,
  `discount` decimal(10, 2) NOT NULL,
  `tax_method` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `tax_id` bigint NULL DEFAULT NULL,
  `tax_amount` decimal(10, 2) NULL DEFAULT NULL,
  `sub_total` decimal(10, 2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of quotation_items
-- ----------------------------

-- ----------------------------
-- Table structure for quotations
-- ----------------------------
DROP TABLE IF EXISTS `quotations`;
CREATE TABLE `quotations`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `quotation_number` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `quotation_date` date NOT NULL,
  `template` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `grand_total` decimal(10, 2) NOT NULL,
  `converted_total` decimal(10, 2) NULL DEFAULT NULL,
  `tax_total` decimal(10, 2) NOT NULL,
  `note` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `related_to` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `related_id` bigint NULL DEFAULT NULL,
  `company_id` bigint NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of quotations
-- ----------------------------

-- ----------------------------
-- Table structure for repeating_transactions
-- ----------------------------
DROP TABLE IF EXISTS `repeating_transactions`;
CREATE TABLE `repeating_transactions`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `trans_date` date NOT NULL,
  `account_id` bigint NOT NULL,
  `chart_id` bigint NOT NULL,
  `type` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `dr_cr` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` decimal(10, 2) NOT NULL,
  `base_amount` decimal(10, 2) NULL DEFAULT NULL,
  `payer_payee_id` bigint NULL DEFAULT NULL,
  `payment_method_id` bigint NOT NULL,
  `reference` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `note` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `company_id` bigint NOT NULL,
  `status` tinyint NULL DEFAULT 0,
  `trans_id` bigint NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of repeating_transactions
-- ----------------------------

-- ----------------------------
-- Table structure for sales_return
-- ----------------------------
DROP TABLE IF EXISTS `sales_return`;
CREATE TABLE `sales_return`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `return_date` date NOT NULL,
  `customer_id` bigint NOT NULL,
  `tax_id` bigint NULL DEFAULT NULL,
  `tax_amount` decimal(10, 2) NULL DEFAULT NULL,
  `product_total` decimal(10, 2) NOT NULL,
  `grand_total` decimal(10, 2) NOT NULL,
  `converted_total` decimal(10, 2) NULL DEFAULT NULL,
  `attachemnt` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `note` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `company_id` bigint NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sales_return
-- ----------------------------

-- ----------------------------
-- Table structure for sales_return_items
-- ----------------------------
DROP TABLE IF EXISTS `sales_return_items`;
CREATE TABLE `sales_return_items`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `sales_return_id` bigint NOT NULL,
  `product_id` bigint NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `quantity` decimal(8, 2) NOT NULL,
  `unit_cost` decimal(10, 2) NOT NULL,
  `discount` decimal(10, 2) NOT NULL,
  `tax_method` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `tax_id` bigint NULL DEFAULT NULL,
  `tax_amount` decimal(10, 2) NULL DEFAULT NULL,
  `sub_total` decimal(10, 2) NOT NULL,
  `company_id` bigint NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sales_return_items
-- ----------------------------

-- ----------------------------
-- Table structure for services
-- ----------------------------
DROP TABLE IF EXISTS `services`;
CREATE TABLE `services`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `item_id` bigint NOT NULL,
  `cost` decimal(10, 2) NOT NULL,
  `tax_method` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `tax_id` bigint NULL DEFAULT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of services
-- ----------------------------

-- ----------------------------
-- Table structure for settings
-- ----------------------------
DROP TABLE IF EXISTS `settings`;
CREATE TABLE `settings`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 79 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of settings
-- ----------------------------
INSERT INTO `settings` VALUES (1, 'mail_type', 'smtp', NULL, '2021-10-26 17:21:05');
INSERT INTO `settings` VALUES (2, 'backend_direction', 'ltr', NULL, '2021-10-27 12:59:31');
INSERT INTO `settings` VALUES (3, 'membership_system', 'enabled', NULL, '2021-10-27 10:22:12');
INSERT INTO `settings` VALUES (4, 'trial_period', '14', NULL, '2021-10-27 10:22:12');
INSERT INTO `settings` VALUES (5, 'allow_singup', 'yes', NULL, '2021-10-27 10:22:12');
INSERT INTO `settings` VALUES (6, 'email_verification', 'enabled', NULL, '2021-10-27 10:22:12');
INSERT INTO `settings` VALUES (7, 'hero_title', 'Start Your Business With LaraBuilder', NULL, NULL);
INSERT INTO `settings` VALUES (8, 'hero_sub_title', 'A +400 professionally designed blocks allowing you to start building sites and pages instantly with Easy to use drag & drop builder!', NULL, NULL);
INSERT INTO `settings` VALUES (9, 'meta_keywords', 'drag and drop, laravel drag and drop, laravel sitebuilder, site builder, sitebuilder, website builder', NULL, NULL);
INSERT INTO `settings` VALUES (10, 'meta_description', 'A +400 professionally designed blocks allowing you to start building sites and pages instantly with Easy to use drag & drop builder!', NULL, NULL);
INSERT INTO `settings` VALUES (11, 'language', 'Indonesia', NULL, '2021-11-05 10:20:16');
INSERT INTO `settings` VALUES (12, 'company_name', 'Ngelapak Bersama Kita', '2021-09-27 22:49:52', '2021-11-05 10:20:16');
INSERT INTO `settings` VALUES (13, 'site_title', 'Ngelapak - Milyaran Aplikasi Untuk Indonesia', '2021-09-27 22:49:52', '2021-11-05 10:20:16');
INSERT INTO `settings` VALUES (14, 'phone', '081333222111', '2021-09-27 22:49:52', '2021-11-05 10:20:16');
INSERT INTO `settings` VALUES (15, 'email', 'admin@ngelapak.co.id', '2021-09-27 22:49:52', '2021-11-05 10:20:16');
INSERT INTO `settings` VALUES (16, 'timezone', 'Asia/Jakarta', '2021-09-27 22:49:52', '2021-11-05 10:20:16');
INSERT INTO `settings` VALUES (17, 'mail_type', 'smtp', NULL, '2021-10-26 17:21:05');
INSERT INTO `settings` VALUES (18, 'backend_direction', 'ltr', NULL, '2021-10-27 12:59:31');
INSERT INTO `settings` VALUES (19, 'membership_system', 'enabled', NULL, '2021-10-27 10:22:12');
INSERT INTO `settings` VALUES (20, 'trial_period', '14', NULL, '2021-10-27 10:22:12');
INSERT INTO `settings` VALUES (21, 'allow_singup', 'yes', NULL, '2021-10-27 10:22:12');
INSERT INTO `settings` VALUES (22, 'email_verification', 'enabled', NULL, '2021-10-27 10:22:12');
INSERT INTO `settings` VALUES (23, 'hero_title', 'Start Your Business With LaraBuilder', NULL, NULL);
INSERT INTO `settings` VALUES (24, 'hero_sub_title', 'A +400 professionally designed blocks allowing you to start building sites and pages instantly with Easy to use drag & drop builder!', NULL, NULL);
INSERT INTO `settings` VALUES (25, 'meta_keywords', 'drag and drop, laravel drag and drop, laravel sitebuilder, site builder, sitebuilder, website builder', NULL, NULL);
INSERT INTO `settings` VALUES (26, 'meta_description', 'A +400 professionally designed blocks allowing you to start building sites and pages instantly with Easy to use drag & drop builder!', NULL, NULL);
INSERT INTO `settings` VALUES (27, 'language', 'Indonesia', NULL, '2021-11-05 10:20:16');
INSERT INTO `settings` VALUES (28, 'currency', 'IDR', '2021-10-05 11:37:42', '2021-10-27 10:22:12');
INSERT INTO `settings` VALUES (29, 'currency_position', 'left', '2021-10-05 11:37:42', '2021-10-27 10:22:12');
INSERT INTO `settings` VALUES (30, 'from_email', 'customer@ngelapak.co.id', '2021-10-05 11:41:25', '2021-10-26 17:21:05');
INSERT INTO `settings` VALUES (31, 'from_name', 'Ngelapak', '2021-10-05 11:41:25', '2021-10-26 17:21:05');
INSERT INTO `settings` VALUES (32, 'smtp_host', 'ngelapak.co.id', '2021-10-05 11:41:25', '2021-10-26 17:21:05');
INSERT INTO `settings` VALUES (33, 'smtp_port', '587', '2021-10-05 11:41:25', '2021-10-26 17:21:05');
INSERT INTO `settings` VALUES (34, 'smtp_username', 'customer@ngelapak.co.id', '2021-10-05 11:41:25', '2021-10-26 17:21:05');
INSERT INTO `settings` VALUES (35, 'smtp_password', 'Ngelapak2021!', '2021-10-05 11:41:25', '2021-10-26 17:21:05');
INSERT INTO `settings` VALUES (36, 'smtp_encryption', 'tls', '2021-10-05 11:41:25', '2021-10-26 17:21:05');
INSERT INTO `settings` VALUES (37, 'address', '', '2021-10-05 12:43:29', '2021-11-05 10:20:16');
INSERT INTO `settings` VALUES (38, 'paypal_active', 'No', '2021-10-05 13:01:40', '2021-10-27 13:38:30');
INSERT INTO `settings` VALUES (39, 'paypal_email', 'ngelapak2021@gmail.com', '2021-10-05 13:01:40', '2021-10-27 13:38:30');
INSERT INTO `settings` VALUES (40, 'paypal_currency', 'USD', '2021-10-05 13:01:40', '2021-10-27 13:38:30');
INSERT INTO `settings` VALUES (41, 'stripe_active', 'No', '2021-10-05 13:01:40', '2021-10-27 13:38:30');
INSERT INTO `settings` VALUES (42, 'stripe_secret_key', '23424', '2021-10-05 13:01:40', '2021-10-27 13:38:30');
INSERT INTO `settings` VALUES (43, 'stripe_publishable_key', '234', '2021-10-05 13:01:40', '2021-10-27 13:38:30');
INSERT INTO `settings` VALUES (44, 'stripe_currency', 'USD', '2021-10-05 13:01:40', '2021-10-27 13:38:30');
INSERT INTO `settings` VALUES (45, 'razorpay_active', 'No', '2021-10-05 13:01:40', '2021-10-27 13:38:30');
INSERT INTO `settings` VALUES (46, 'razorpay_key_id', '2324', '2021-10-05 13:01:40', '2021-10-27 13:38:30');
INSERT INTO `settings` VALUES (47, 'razorpay_secret_key', '23424', '2021-10-05 13:01:40', '2021-10-27 13:38:30');
INSERT INTO `settings` VALUES (48, 'razorpay_currency', 'INR', '2021-10-05 13:01:40', '2021-10-27 13:38:30');
INSERT INTO `settings` VALUES (49, 'paystack_active', 'No', '2021-10-05 13:01:40', '2021-10-27 13:38:30');
INSERT INTO `settings` VALUES (50, 'paystack_public_key', '123', '2021-10-05 13:01:40', '2021-10-27 13:38:30');
INSERT INTO `settings` VALUES (51, 'paystack_secret_key', '123', '2021-10-05 13:01:40', '2021-10-27 13:38:30');
INSERT INTO `settings` VALUES (52, 'paystack_currency', 'NGN', '2021-10-05 13:01:40', '2021-10-27 13:38:30');
INSERT INTO `settings` VALUES (53, 'mail_type', 'smtp', NULL, '2021-10-26 17:21:05');
INSERT INTO `settings` VALUES (54, 'backend_direction', 'ltr', NULL, '2021-10-27 12:59:31');
INSERT INTO `settings` VALUES (55, 'membership_system', 'enabled', NULL, '2021-10-27 10:22:12');
INSERT INTO `settings` VALUES (56, 'trial_period', '14', NULL, '2021-10-27 10:22:12');
INSERT INTO `settings` VALUES (57, 'allow_singup', 'yes', NULL, '2021-10-27 10:22:12');
INSERT INTO `settings` VALUES (58, 'email_verification', 'enabled', NULL, '2021-10-27 10:22:12');
INSERT INTO `settings` VALUES (59, 'hero_title', 'Start Your Business With LaraBuilder', NULL, NULL);
INSERT INTO `settings` VALUES (60, 'hero_sub_title', 'A +400 professionally designed blocks allowing you to start building sites and pages instantly with Easy to use drag & drop builder!', NULL, NULL);
INSERT INTO `settings` VALUES (61, 'meta_keywords', 'drag and drop, laravel drag and drop, laravel sitebuilder, site builder, sitebuilder, website builder', NULL, NULL);
INSERT INTO `settings` VALUES (62, 'meta_description', 'A +400 professionally designed blocks allowing you to start building sites and pages instantly with Easy to use drag & drop builder!', NULL, NULL);
INSERT INTO `settings` VALUES (63, 'language', 'Indonesia', NULL, '2021-11-05 10:20:16');
INSERT INTO `settings` VALUES (64, 'website_enable', 'yes', '2021-10-26 13:21:05', '2021-10-27 12:59:31');
INSERT INTO `settings` VALUES (65, 'default_builder', 'both', '2021-10-26 13:21:05', '2021-10-27 12:59:31');
INSERT INTO `settings` VALUES (66, 'website_language_dropdown', 'yes', '2021-10-26 13:21:05', '2021-10-27 12:59:31');
INSERT INTO `settings` VALUES (67, 'currency_converter', 'manual', '2021-10-26 13:21:05', '2021-10-27 12:59:31');
INSERT INTO `settings` VALUES (68, 'fixer_api_key', '', '2021-10-26 13:21:05', '2021-10-27 12:59:31');
INSERT INTO `settings` VALUES (69, 'google_map_key', 'AIzaSyAcp1Ne9Mh0PEReSU155zIqxke7R-nOgSg', '2021-10-26 13:21:05', '2021-10-27 12:59:31');
INSERT INTO `settings` VALUES (70, 'date_format', 'Y-m-d', '2021-10-26 13:21:05', '2021-10-27 12:59:31');
INSERT INTO `settings` VALUES (71, 'time_format', '24', '2021-10-26 13:21:05', '2021-10-27 12:59:31');
INSERT INTO `settings` VALUES (72, 'file_manager_file_type_supported', 'png,jpg,jpeg', '2021-10-26 13:21:05', '2021-10-27 12:59:31');
INSERT INTO `settings` VALUES (73, 'file_manager_max_upload_size', '2', '2021-10-26 13:21:05', '2021-10-27 12:59:31');
INSERT INTO `settings` VALUES (74, 'favicon', 'file_1635229632.png', '2021-10-26 13:27:12', '2021-10-26 13:27:12');
INSERT INTO `settings` VALUES (75, 'logo', 'logo.png', '2021-10-26 13:27:42', '2021-10-26 13:27:42');
INSERT INTO `settings` VALUES (76, 'google_login', 'disabled', '2021-10-26 16:16:02', '2021-10-26 16:16:43');
INSERT INTO `settings` VALUES (77, 'GOOGLE_CLIENT_ID', '', '2021-10-26 16:16:02', '2021-10-26 16:16:43');
INSERT INTO `settings` VALUES (78, 'GOOGLE_CLIENT_SECRET', '', '2021-10-26 16:16:02', '2021-10-26 16:16:43');

-- ----------------------------
-- Table structure for social_google_accounts
-- ----------------------------
DROP TABLE IF EXISTS `social_google_accounts`;
CREATE TABLE `social_google_accounts`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint NOT NULL,
  `provider_user_id` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `provider` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of social_google_accounts
-- ----------------------------

-- ----------------------------
-- Table structure for staff_roles
-- ----------------------------
DROP TABLE IF EXISTS `staff_roles`;
CREATE TABLE `staff_roles`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `company_id` bigint NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of staff_roles
-- ----------------------------
INSERT INTO `staff_roles` VALUES (1, 'manager', 'For manging the websites', 268, '2021-12-30 09:50:59', '2021-12-30 09:50:59');

-- ----------------------------
-- Table structure for subdistricts
-- ----------------------------
DROP TABLE IF EXISTS `subdistricts`;
CREATE TABLE `subdistricts`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `subdistrict_id` int NULL DEFAULT NULL,
  `city_id` int NULL DEFAULT NULL,
  `subdistrict_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6995 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of subdistricts
-- ----------------------------
INSERT INTO `subdistricts` VALUES (1, 1, 1, 'Arongan Lambalek');
INSERT INTO `subdistricts` VALUES (2, 2, 1, 'Bubon');
INSERT INTO `subdistricts` VALUES (3, 3, 1, 'Johan Pahlawan');
INSERT INTO `subdistricts` VALUES (4, 4, 1, 'Kaway XVI');
INSERT INTO `subdistricts` VALUES (5, 5, 1, 'Meureubo');
INSERT INTO `subdistricts` VALUES (6, 6, 1, 'Pante Ceureumen (Pantai Ceuremen)');
INSERT INTO `subdistricts` VALUES (7, 7, 1, 'Panton Reu');
INSERT INTO `subdistricts` VALUES (8, 8, 1, 'Samatiga');
INSERT INTO `subdistricts` VALUES (9, 9, 1, 'Sungai Mas');
INSERT INTO `subdistricts` VALUES (10, 10, 1, 'Woyla');
INSERT INTO `subdistricts` VALUES (11, 11, 1, 'Woyla Barat');
INSERT INTO `subdistricts` VALUES (12, 12, 1, 'Woyla Timur');
INSERT INTO `subdistricts` VALUES (13, 13, 2, 'Babah Rot');
INSERT INTO `subdistricts` VALUES (14, 14, 2, 'Blang Pidie');
INSERT INTO `subdistricts` VALUES (15, 15, 2, 'Jeumpa');
INSERT INTO `subdistricts` VALUES (16, 16, 2, 'Kuala Batee');
INSERT INTO `subdistricts` VALUES (17, 17, 2, 'Lembah Sabil');
INSERT INTO `subdistricts` VALUES (18, 18, 2, 'Manggeng');
INSERT INTO `subdistricts` VALUES (19, 19, 2, 'Setia');
INSERT INTO `subdistricts` VALUES (20, 20, 2, 'Susoh');
INSERT INTO `subdistricts` VALUES (21, 21, 2, 'Tangan-Tangan');
INSERT INTO `subdistricts` VALUES (22, 22, 3, 'Baitussalam');
INSERT INTO `subdistricts` VALUES (23, 23, 3, 'Blank Bintang');
INSERT INTO `subdistricts` VALUES (24, 24, 3, 'Darul Imarah');
INSERT INTO `subdistricts` VALUES (25, 25, 3, 'Darul Kamal');
INSERT INTO `subdistricts` VALUES (26, 26, 3, 'Darussalam');
INSERT INTO `subdistricts` VALUES (27, 27, 3, 'Indrapuri');
INSERT INTO `subdistricts` VALUES (28, 28, 3, 'Ingin Jaya');
INSERT INTO `subdistricts` VALUES (29, 29, 3, 'Kota Cot Glie (Kuta Cot Glie)');
INSERT INTO `subdistricts` VALUES (30, 30, 3, 'Kota Jantho');
INSERT INTO `subdistricts` VALUES (31, 31, 3, 'Kota Malaka (Kuta Malaka)');
INSERT INTO `subdistricts` VALUES (32, 32, 3, 'Krueng Barona Jaya');
INSERT INTO `subdistricts` VALUES (33, 33, 3, 'Kuta Baro');
INSERT INTO `subdistricts` VALUES (34, 34, 3, 'Lembah Seulawah');
INSERT INTO `subdistricts` VALUES (35, 35, 3, 'Leupung');
INSERT INTO `subdistricts` VALUES (36, 36, 3, 'Lhoknga (Lho\'nga)');
INSERT INTO `subdistricts` VALUES (37, 37, 3, 'Lhoong');
INSERT INTO `subdistricts` VALUES (38, 38, 3, 'Mantasiek (Montasik)');
INSERT INTO `subdistricts` VALUES (39, 39, 3, 'Mesjid Raya');
INSERT INTO `subdistricts` VALUES (40, 40, 3, 'Peukan Bada');
INSERT INTO `subdistricts` VALUES (41, 41, 3, 'Pulo Aceh');
INSERT INTO `subdistricts` VALUES (42, 42, 3, 'Seulimeum');
INSERT INTO `subdistricts` VALUES (43, 43, 3, 'Simpang Tiga');
INSERT INTO `subdistricts` VALUES (44, 44, 3, 'Suka Makmur');
INSERT INTO `subdistricts` VALUES (45, 45, 4, 'Darul Hikmah');
INSERT INTO `subdistricts` VALUES (46, 46, 4, 'Indra Jaya');
INSERT INTO `subdistricts` VALUES (47, 47, 4, 'Jaya');
INSERT INTO `subdistricts` VALUES (48, 48, 4, 'Keude Panga');
INSERT INTO `subdistricts` VALUES (49, 49, 4, 'Krueng Sabee');
INSERT INTO `subdistricts` VALUES (50, 50, 4, 'Pasie Raya');
INSERT INTO `subdistricts` VALUES (51, 51, 4, 'Sampoiniet');
INSERT INTO `subdistricts` VALUES (52, 52, 4, 'Setia Bakti');
INSERT INTO `subdistricts` VALUES (53, 53, 4, 'Teunom');
INSERT INTO `subdistricts` VALUES (54, 54, 5, 'Bakongan');
INSERT INTO `subdistricts` VALUES (55, 55, 5, 'Bakongan Timur');
INSERT INTO `subdistricts` VALUES (56, 56, 5, 'Kluet Selatan');
INSERT INTO `subdistricts` VALUES (57, 57, 5, 'Kluet Tengah');
INSERT INTO `subdistricts` VALUES (58, 58, 5, 'Kluet Timur');
INSERT INTO `subdistricts` VALUES (59, 59, 5, 'Kluet Utara');
INSERT INTO `subdistricts` VALUES (60, 60, 5, 'Kota Bahagia');
INSERT INTO `subdistricts` VALUES (61, 61, 5, 'Labuhan Haji');
INSERT INTO `subdistricts` VALUES (62, 62, 5, 'Labuhan Haji Barat');
INSERT INTO `subdistricts` VALUES (63, 63, 5, 'Labuhan Haji Timur');
INSERT INTO `subdistricts` VALUES (64, 64, 5, 'Meukek');
INSERT INTO `subdistricts` VALUES (65, 65, 5, 'Pasie Raja');
INSERT INTO `subdistricts` VALUES (66, 66, 5, 'Sama Dua');
INSERT INTO `subdistricts` VALUES (67, 67, 5, 'Sawang');
INSERT INTO `subdistricts` VALUES (68, 68, 5, 'Tapak Tuan');
INSERT INTO `subdistricts` VALUES (69, 69, 5, 'Trumon');
INSERT INTO `subdistricts` VALUES (70, 70, 5, 'Trumon Tengah');
INSERT INTO `subdistricts` VALUES (71, 71, 5, 'Trumon Timur');
INSERT INTO `subdistricts` VALUES (72, 72, 6, 'Danau Paris');
INSERT INTO `subdistricts` VALUES (73, 73, 6, 'Gunung Meriah (Mariah)');
INSERT INTO `subdistricts` VALUES (74, 74, 6, 'Kota Baharu');
INSERT INTO `subdistricts` VALUES (75, 75, 6, 'Kuala Baru');
INSERT INTO `subdistricts` VALUES (76, 76, 6, 'Pulau Banyak');
INSERT INTO `subdistricts` VALUES (77, 77, 6, 'Pulau Banyak Barat');
INSERT INTO `subdistricts` VALUES (78, 78, 6, 'Simpang Kanan');
INSERT INTO `subdistricts` VALUES (79, 79, 6, 'Singkil');
INSERT INTO `subdistricts` VALUES (80, 80, 6, 'Singkil Utara');
INSERT INTO `subdistricts` VALUES (81, 81, 6, 'Singkohor');
INSERT INTO `subdistricts` VALUES (82, 82, 6, 'Suro Makmur');
INSERT INTO `subdistricts` VALUES (83, 83, 7, 'Banda Mulia');
INSERT INTO `subdistricts` VALUES (84, 84, 7, 'Bandar Pusaka');
INSERT INTO `subdistricts` VALUES (85, 85, 7, 'Bendahara');
INSERT INTO `subdistricts` VALUES (86, 86, 7, 'Karang Baru');
INSERT INTO `subdistricts` VALUES (87, 87, 7, 'Kejuruan Muda');
INSERT INTO `subdistricts` VALUES (88, 88, 7, 'Kota Kuala Simpang');
INSERT INTO `subdistricts` VALUES (89, 89, 7, 'Manyak Payed');
INSERT INTO `subdistricts` VALUES (90, 90, 7, 'Rantau');
INSERT INTO `subdistricts` VALUES (91, 91, 7, 'Sekerak');
INSERT INTO `subdistricts` VALUES (92, 92, 7, 'Seruway');
INSERT INTO `subdistricts` VALUES (93, 93, 7, 'Tamiang Hulu');
INSERT INTO `subdistricts` VALUES (94, 94, 7, 'Tenggulun');
INSERT INTO `subdistricts` VALUES (95, 95, 8, 'Atu Lintang');
INSERT INTO `subdistricts` VALUES (96, 96, 8, 'Bebesen');
INSERT INTO `subdistricts` VALUES (97, 97, 8, 'Bies');
INSERT INTO `subdistricts` VALUES (98, 98, 8, 'Bintang');
INSERT INTO `subdistricts` VALUES (99, 99, 8, 'Celala');
INSERT INTO `subdistricts` VALUES (100, 100, 8, 'Jagong Jeget');
INSERT INTO `subdistricts` VALUES (101, 101, 8, 'Kebayakan');
INSERT INTO `subdistricts` VALUES (102, 102, 8, 'Ketol');
INSERT INTO `subdistricts` VALUES (103, 103, 8, 'Kute Panang');
INSERT INTO `subdistricts` VALUES (104, 104, 8, 'Linge');
INSERT INTO `subdistricts` VALUES (105, 105, 8, 'Lut Tawar');
INSERT INTO `subdistricts` VALUES (106, 106, 8, 'Pegasing');
INSERT INTO `subdistricts` VALUES (107, 107, 8, 'Rusip Antara');
INSERT INTO `subdistricts` VALUES (108, 108, 8, 'Silih Nara');
INSERT INTO `subdistricts` VALUES (109, 109, 9, 'Babul Makmur');
INSERT INTO `subdistricts` VALUES (110, 110, 9, 'Babul Rahmah');
INSERT INTO `subdistricts` VALUES (111, 111, 9, 'Babussalam');
INSERT INTO `subdistricts` VALUES (112, 112, 9, 'Badar');
INSERT INTO `subdistricts` VALUES (113, 113, 9, 'Bambel');
INSERT INTO `subdistricts` VALUES (114, 114, 9, 'Bukit Tusam');
INSERT INTO `subdistricts` VALUES (115, 115, 9, 'Darul Hasanah');
INSERT INTO `subdistricts` VALUES (116, 116, 9, 'Deleng Pokhisen');
INSERT INTO `subdistricts` VALUES (117, 117, 9, 'Ketambe');
INSERT INTO `subdistricts` VALUES (118, 118, 9, 'Lawe Alas');
INSERT INTO `subdistricts` VALUES (119, 119, 9, 'Lawe Bulan');
INSERT INTO `subdistricts` VALUES (120, 120, 9, 'Lawe Sigala-Gala');
INSERT INTO `subdistricts` VALUES (121, 121, 9, 'Lawe Sumur');
INSERT INTO `subdistricts` VALUES (122, 122, 9, 'Leuser');
INSERT INTO `subdistricts` VALUES (123, 123, 9, 'Semadam');
INSERT INTO `subdistricts` VALUES (124, 124, 9, 'Tanah Alas');
INSERT INTO `subdistricts` VALUES (125, 125, 10, 'Banda Alam');
INSERT INTO `subdistricts` VALUES (126, 126, 10, 'Birem Bayeun');
INSERT INTO `subdistricts` VALUES (127, 127, 10, 'Darul Aman');
INSERT INTO `subdistricts` VALUES (128, 128, 10, 'Darul Falah');
INSERT INTO `subdistricts` VALUES (129, 129, 10, 'Darul Iksan (Ihsan)');
INSERT INTO `subdistricts` VALUES (130, 130, 10, 'Idi Rayeuk');
INSERT INTO `subdistricts` VALUES (131, 131, 10, 'Idi Timur');
INSERT INTO `subdistricts` VALUES (132, 132, 10, 'Idi Tunong');
INSERT INTO `subdistricts` VALUES (133, 133, 10, 'Indra Makmur');
INSERT INTO `subdistricts` VALUES (134, 134, 10, 'Julok');
INSERT INTO `subdistricts` VALUES (135, 135, 10, 'Madat');
INSERT INTO `subdistricts` VALUES (136, 136, 10, 'Nurussalam');
INSERT INTO `subdistricts` VALUES (137, 137, 10, 'Pante Bidari (Beudari)');
INSERT INTO `subdistricts` VALUES (138, 138, 10, 'Peudawa');
INSERT INTO `subdistricts` VALUES (139, 139, 10, 'Peunaron');
INSERT INTO `subdistricts` VALUES (140, 140, 10, 'Peureulak');
INSERT INTO `subdistricts` VALUES (141, 141, 10, 'Peureulak Barat');
INSERT INTO `subdistricts` VALUES (142, 142, 10, 'Peureulak Timur');
INSERT INTO `subdistricts` VALUES (143, 143, 10, 'Rantau Selamat');
INSERT INTO `subdistricts` VALUES (144, 144, 10, 'Ranto Peureulak');
INSERT INTO `subdistricts` VALUES (145, 145, 10, 'Serba Jadi');
INSERT INTO `subdistricts` VALUES (146, 146, 10, 'Simpang Jernih');
INSERT INTO `subdistricts` VALUES (147, 147, 10, 'Simpang Ulim');
INSERT INTO `subdistricts` VALUES (148, 148, 10, 'Sungai Raya');
INSERT INTO `subdistricts` VALUES (149, 149, 11, 'Baktiya');
INSERT INTO `subdistricts` VALUES (150, 150, 11, 'Baktiya Barat');
INSERT INTO `subdistricts` VALUES (151, 151, 11, 'Banda Baro');
INSERT INTO `subdistricts` VALUES (152, 152, 11, 'Cot Girek');
INSERT INTO `subdistricts` VALUES (153, 153, 11, 'Dewantara');
INSERT INTO `subdistricts` VALUES (154, 154, 11, 'Geuredong Pase');
INSERT INTO `subdistricts` VALUES (155, 155, 11, 'Kuta Makmur');
INSERT INTO `subdistricts` VALUES (156, 156, 11, 'Langkahan');
INSERT INTO `subdistricts` VALUES (157, 157, 11, 'Lapang');
INSERT INTO `subdistricts` VALUES (158, 158, 11, 'Lhoksukon');
INSERT INTO `subdistricts` VALUES (159, 159, 11, 'Matangkuli');
INSERT INTO `subdistricts` VALUES (160, 160, 11, 'Meurah Mulia');
INSERT INTO `subdistricts` VALUES (161, 161, 11, 'Muara Batu');
INSERT INTO `subdistricts` VALUES (162, 162, 11, 'Nibong');
INSERT INTO `subdistricts` VALUES (163, 163, 11, 'Nisam');
INSERT INTO `subdistricts` VALUES (164, 164, 11, 'Nisam Antara');
INSERT INTO `subdistricts` VALUES (165, 165, 11, 'Paya Bakong');
INSERT INTO `subdistricts` VALUES (166, 166, 11, 'Pirak Timur');
INSERT INTO `subdistricts` VALUES (167, 167, 11, 'Samudera');
INSERT INTO `subdistricts` VALUES (168, 168, 11, 'Sawang');
INSERT INTO `subdistricts` VALUES (169, 169, 11, 'Seunuddon (Seunudon)');
INSERT INTO `subdistricts` VALUES (170, 170, 11, 'Simpang Kramat (Keramat)');
INSERT INTO `subdistricts` VALUES (171, 171, 11, 'Syamtalira Aron');
INSERT INTO `subdistricts` VALUES (172, 172, 11, 'Syamtalira Bayu');
INSERT INTO `subdistricts` VALUES (173, 173, 11, 'Tanah Jambo Aye');
INSERT INTO `subdistricts` VALUES (174, 174, 11, 'Tanah Luas');
INSERT INTO `subdistricts` VALUES (175, 175, 11, 'Tanah Pasir');
INSERT INTO `subdistricts` VALUES (176, 176, 12, 'Ampek Nagari (IV Nagari )');
INSERT INTO `subdistricts` VALUES (177, 177, 12, 'Banuhampu');
INSERT INTO `subdistricts` VALUES (178, 178, 12, 'Baso');
INSERT INTO `subdistricts` VALUES (179, 179, 12, 'Candung');
INSERT INTO `subdistricts` VALUES (180, 180, 12, 'IV Angkat Candung (Ampek Angkek)');
INSERT INTO `subdistricts` VALUES (181, 181, 12, 'IV Koto (Ampek Koto)');
INSERT INTO `subdistricts` VALUES (182, 182, 12, 'Kamang Magek');
INSERT INTO `subdistricts` VALUES (183, 183, 12, 'Lubuk Basung');
INSERT INTO `subdistricts` VALUES (184, 184, 12, 'Malakak');
INSERT INTO `subdistricts` VALUES (185, 185, 12, 'Matur');
INSERT INTO `subdistricts` VALUES (186, 186, 12, 'Palembayan');
INSERT INTO `subdistricts` VALUES (187, 187, 12, 'Palupuh');
INSERT INTO `subdistricts` VALUES (188, 188, 12, 'Sungai Pua (Puar)');
INSERT INTO `subdistricts` VALUES (189, 189, 12, 'Tanjung Mutiara');
INSERT INTO `subdistricts` VALUES (190, 190, 12, 'Tanjung Raya');
INSERT INTO `subdistricts` VALUES (191, 191, 12, 'Tilatang Kamang');
INSERT INTO `subdistricts` VALUES (192, 192, 13, 'Alor Barat Daya');
INSERT INTO `subdistricts` VALUES (193, 193, 13, 'Alor Barat Laut');
INSERT INTO `subdistricts` VALUES (194, 194, 13, 'Alor Selatan');
INSERT INTO `subdistricts` VALUES (195, 195, 13, 'Alor Tengah Utara');
INSERT INTO `subdistricts` VALUES (196, 196, 13, 'Alor Timur');
INSERT INTO `subdistricts` VALUES (197, 197, 13, 'Alor Timur Laut');
INSERT INTO `subdistricts` VALUES (198, 198, 13, 'Kabola');
INSERT INTO `subdistricts` VALUES (199, 199, 13, 'Lembur');
INSERT INTO `subdistricts` VALUES (200, 200, 13, 'Mataru');
INSERT INTO `subdistricts` VALUES (201, 201, 13, 'Pantar');
INSERT INTO `subdistricts` VALUES (202, 202, 13, 'Pantar Barat');
INSERT INTO `subdistricts` VALUES (203, 203, 13, 'Pantar Barat Laut');
INSERT INTO `subdistricts` VALUES (204, 204, 13, 'Pantar Tengah');
INSERT INTO `subdistricts` VALUES (205, 205, 13, 'Pantar Timur');
INSERT INTO `subdistricts` VALUES (206, 206, 13, 'Pulau Pura');
INSERT INTO `subdistricts` VALUES (207, 207, 13, 'Pureman');
INSERT INTO `subdistricts` VALUES (208, 208, 13, 'Teluk Mutiara');
INSERT INTO `subdistricts` VALUES (209, 209, 14, 'Baguala');
INSERT INTO `subdistricts` VALUES (210, 210, 14, 'Leitimur Selatan');
INSERT INTO `subdistricts` VALUES (211, 211, 14, 'Nusaniwe (Nusanive)');
INSERT INTO `subdistricts` VALUES (212, 212, 14, 'Sirimau');
INSERT INTO `subdistricts` VALUES (213, 213, 14, 'Teluk Ambon');
INSERT INTO `subdistricts` VALUES (214, 214, 15, 'Aek Kuasan');
INSERT INTO `subdistricts` VALUES (215, 215, 15, 'Aek Ledong');
INSERT INTO `subdistricts` VALUES (216, 216, 15, 'Aek Songsongan');
INSERT INTO `subdistricts` VALUES (217, 217, 15, 'Air Batu');
INSERT INTO `subdistricts` VALUES (218, 218, 15, 'Air Joman');
INSERT INTO `subdistricts` VALUES (219, 219, 15, 'Bandar Pasir Mandoge');
INSERT INTO `subdistricts` VALUES (220, 220, 15, 'Bandar Pulau');
INSERT INTO `subdistricts` VALUES (221, 221, 15, 'Buntu Pane');
INSERT INTO `subdistricts` VALUES (222, 222, 15, 'Kisaran Barat Kota');
INSERT INTO `subdistricts` VALUES (223, 223, 15, 'Kisaran Timur Kota');
INSERT INTO `subdistricts` VALUES (224, 224, 15, 'Meranti');
INSERT INTO `subdistricts` VALUES (225, 225, 15, 'Pulau Rakyat');
INSERT INTO `subdistricts` VALUES (226, 226, 15, 'Pulo Bandring');
INSERT INTO `subdistricts` VALUES (227, 227, 15, 'Rahuning');
INSERT INTO `subdistricts` VALUES (228, 228, 15, 'Rawang Panca Arga');
INSERT INTO `subdistricts` VALUES (229, 229, 15, 'Sei Dadap');
INSERT INTO `subdistricts` VALUES (230, 230, 15, 'Sei Kepayang');
INSERT INTO `subdistricts` VALUES (231, 231, 15, 'Sei Kepayang Barat');
INSERT INTO `subdistricts` VALUES (232, 232, 15, 'Sei Kepayang Timur');
INSERT INTO `subdistricts` VALUES (233, 233, 15, 'Setia Janji');
INSERT INTO `subdistricts` VALUES (234, 234, 15, 'Silau Laut');
INSERT INTO `subdistricts` VALUES (235, 235, 15, 'Simpang Empat');
INSERT INTO `subdistricts` VALUES (236, 236, 15, 'Tanjung Balai');
INSERT INTO `subdistricts` VALUES (237, 237, 15, 'Teluk Dalam');
INSERT INTO `subdistricts` VALUES (238, 238, 15, 'Tinggi Raja');
INSERT INTO `subdistricts` VALUES (239, 239, 16, 'Agats');
INSERT INTO `subdistricts` VALUES (240, 240, 16, 'Akat');
INSERT INTO `subdistricts` VALUES (241, 241, 16, 'Atsy / Atsj');
INSERT INTO `subdistricts` VALUES (242, 242, 16, 'Ayip');
INSERT INTO `subdistricts` VALUES (243, 243, 16, 'Betcbamu');
INSERT INTO `subdistricts` VALUES (244, 244, 16, 'Der Koumur');
INSERT INTO `subdistricts` VALUES (245, 245, 16, 'Fayit');
INSERT INTO `subdistricts` VALUES (246, 246, 16, 'Jetsy');
INSERT INTO `subdistricts` VALUES (247, 247, 16, 'Joerat');
INSERT INTO `subdistricts` VALUES (248, 248, 16, 'Kolf Braza');
INSERT INTO `subdistricts` VALUES (249, 249, 16, 'Kopay');
INSERT INTO `subdistricts` VALUES (250, 250, 16, 'Pantai Kasuari');
INSERT INTO `subdistricts` VALUES (251, 251, 16, 'Pulau Tiga');
INSERT INTO `subdistricts` VALUES (252, 252, 16, 'Safan');
INSERT INTO `subdistricts` VALUES (253, 253, 16, 'Sawa Erma');
INSERT INTO `subdistricts` VALUES (254, 254, 16, 'Sirets');
INSERT INTO `subdistricts` VALUES (255, 255, 16, 'Suator');
INSERT INTO `subdistricts` VALUES (256, 256, 16, 'Suru-suru');
INSERT INTO `subdistricts` VALUES (257, 257, 16, 'Unir Sirau');
INSERT INTO `subdistricts` VALUES (258, 258, 17, 'Abiansemal');
INSERT INTO `subdistricts` VALUES (259, 259, 17, 'Kuta');
INSERT INTO `subdistricts` VALUES (260, 260, 17, 'Kuta Selatan');
INSERT INTO `subdistricts` VALUES (261, 261, 17, 'Kuta Utara');
INSERT INTO `subdistricts` VALUES (262, 262, 17, 'Mengwi');
INSERT INTO `subdistricts` VALUES (263, 263, 17, 'Petang');
INSERT INTO `subdistricts` VALUES (264, 264, 18, 'Awayan');
INSERT INTO `subdistricts` VALUES (265, 265, 18, 'Batu Mandi');
INSERT INTO `subdistricts` VALUES (266, 266, 18, 'Halong');
INSERT INTO `subdistricts` VALUES (267, 267, 18, 'Juai');
INSERT INTO `subdistricts` VALUES (268, 268, 18, 'Lampihong');
INSERT INTO `subdistricts` VALUES (269, 269, 18, 'Paringin');
INSERT INTO `subdistricts` VALUES (270, 270, 18, 'Paringin Selatan');
INSERT INTO `subdistricts` VALUES (271, 271, 18, 'Tebing Tinggi');
INSERT INTO `subdistricts` VALUES (272, 272, 19, 'Balikpapan Barat');
INSERT INTO `subdistricts` VALUES (273, 273, 19, 'Balikpapan Kota');
INSERT INTO `subdistricts` VALUES (274, 274, 19, 'Balikpapan Selatan');
INSERT INTO `subdistricts` VALUES (275, 275, 19, 'Balikpapan Tengah');
INSERT INTO `subdistricts` VALUES (276, 276, 19, 'Balikpapan Timur');
INSERT INTO `subdistricts` VALUES (277, 277, 19, 'Balikpapan Utara');
INSERT INTO `subdistricts` VALUES (278, 278, 20, 'Baiturrahman');
INSERT INTO `subdistricts` VALUES (279, 279, 20, 'Banda Raya');
INSERT INTO `subdistricts` VALUES (280, 280, 20, 'Jaya Baru');
INSERT INTO `subdistricts` VALUES (281, 281, 20, 'Kuta Alam');
INSERT INTO `subdistricts` VALUES (282, 282, 20, 'Kuta Raja');
INSERT INTO `subdistricts` VALUES (283, 283, 20, 'Lueng Bata');
INSERT INTO `subdistricts` VALUES (284, 284, 20, 'Meuraxa');
INSERT INTO `subdistricts` VALUES (285, 285, 20, 'Syiah Kuala');
INSERT INTO `subdistricts` VALUES (286, 286, 20, 'Ulee Kareng');
INSERT INTO `subdistricts` VALUES (287, 287, 21, 'Bumi Waras');
INSERT INTO `subdistricts` VALUES (288, 288, 21, 'Enggal');
INSERT INTO `subdistricts` VALUES (289, 289, 21, 'Kedamaian');
INSERT INTO `subdistricts` VALUES (290, 290, 21, 'Kedaton');
INSERT INTO `subdistricts` VALUES (291, 291, 21, 'Kemiling');
INSERT INTO `subdistricts` VALUES (292, 292, 21, 'Labuhan Ratu');
INSERT INTO `subdistricts` VALUES (293, 293, 21, 'Langkapura');
INSERT INTO `subdistricts` VALUES (294, 294, 21, 'Panjang');
INSERT INTO `subdistricts` VALUES (295, 295, 21, 'Rajabasa');
INSERT INTO `subdistricts` VALUES (296, 296, 21, 'Sukabumi');
INSERT INTO `subdistricts` VALUES (297, 297, 21, 'Sukarame');
INSERT INTO `subdistricts` VALUES (298, 298, 21, 'Tanjung Karang Barat');
INSERT INTO `subdistricts` VALUES (299, 299, 21, 'Tanjung Karang Pusat');
INSERT INTO `subdistricts` VALUES (300, 300, 21, 'Tanjung Karang Timur');
INSERT INTO `subdistricts` VALUES (301, 301, 21, 'Tanjung Senang');
INSERT INTO `subdistricts` VALUES (302, 302, 21, 'Telukbetung Barat');
INSERT INTO `subdistricts` VALUES (303, 303, 21, 'Telukbetung Selatan');
INSERT INTO `subdistricts` VALUES (304, 304, 21, 'Telukbetung Timur');
INSERT INTO `subdistricts` VALUES (305, 305, 21, 'Telukbetung Utara');
INSERT INTO `subdistricts` VALUES (306, 306, 21, 'Way Halim');
INSERT INTO `subdistricts` VALUES (307, 307, 22, 'Arjasari');
INSERT INTO `subdistricts` VALUES (308, 308, 22, 'Baleendah');
INSERT INTO `subdistricts` VALUES (309, 309, 22, 'Banjaran');
INSERT INTO `subdistricts` VALUES (310, 310, 22, 'Bojongsoang');
INSERT INTO `subdistricts` VALUES (311, 311, 22, 'Cangkuang');
INSERT INTO `subdistricts` VALUES (312, 312, 22, 'Cicalengka');
INSERT INTO `subdistricts` VALUES (313, 313, 22, 'Cikancung');
INSERT INTO `subdistricts` VALUES (314, 314, 22, 'Cilengkrang');
INSERT INTO `subdistricts` VALUES (315, 315, 22, 'Cileunyi');
INSERT INTO `subdistricts` VALUES (316, 316, 22, 'Cimaung');
INSERT INTO `subdistricts` VALUES (317, 317, 22, 'Cimeunyan');
INSERT INTO `subdistricts` VALUES (318, 318, 22, 'Ciparay');
INSERT INTO `subdistricts` VALUES (319, 319, 22, 'Ciwidey');
INSERT INTO `subdistricts` VALUES (320, 320, 22, 'Dayeuhkolot');
INSERT INTO `subdistricts` VALUES (321, 321, 22, 'Ibun');
INSERT INTO `subdistricts` VALUES (322, 322, 22, 'Katapang');
INSERT INTO `subdistricts` VALUES (323, 323, 22, 'Kertasari');
INSERT INTO `subdistricts` VALUES (324, 324, 22, 'Kutawaringin');
INSERT INTO `subdistricts` VALUES (325, 325, 22, 'Majalaya');
INSERT INTO `subdistricts` VALUES (326, 326, 22, 'Margaasih');
INSERT INTO `subdistricts` VALUES (327, 327, 22, 'Margahayu');
INSERT INTO `subdistricts` VALUES (328, 328, 22, 'Nagreg');
INSERT INTO `subdistricts` VALUES (329, 329, 22, 'Pacet');
INSERT INTO `subdistricts` VALUES (330, 330, 22, 'Pameungpeuk');
INSERT INTO `subdistricts` VALUES (331, 331, 22, 'Pangalengan');
INSERT INTO `subdistricts` VALUES (332, 332, 22, 'Paseh');
INSERT INTO `subdistricts` VALUES (333, 333, 22, 'Pasirjambu');
INSERT INTO `subdistricts` VALUES (334, 334, 22, 'Ranca Bali');
INSERT INTO `subdistricts` VALUES (335, 335, 22, 'Rancaekek');
INSERT INTO `subdistricts` VALUES (336, 336, 22, 'Solokan Jeruk');
INSERT INTO `subdistricts` VALUES (337, 337, 22, 'Soreang');
INSERT INTO `subdistricts` VALUES (338, 338, 23, 'Andir');
INSERT INTO `subdistricts` VALUES (339, 339, 23, 'Antapani (Cicadas)');
INSERT INTO `subdistricts` VALUES (340, 340, 23, 'Arcamanik');
INSERT INTO `subdistricts` VALUES (341, 341, 23, 'Astana Anyar');
INSERT INTO `subdistricts` VALUES (342, 342, 23, 'Babakan Ciparay');
INSERT INTO `subdistricts` VALUES (343, 343, 23, 'Bandung Kidul');
INSERT INTO `subdistricts` VALUES (344, 344, 23, 'Bandung Kulon');
INSERT INTO `subdistricts` VALUES (345, 345, 23, 'Bandung Wetan');
INSERT INTO `subdistricts` VALUES (346, 346, 23, 'Batununggal');
INSERT INTO `subdistricts` VALUES (347, 347, 23, 'Bojongloa Kaler');
INSERT INTO `subdistricts` VALUES (348, 348, 23, 'Bojongloa Kidul');
INSERT INTO `subdistricts` VALUES (349, 349, 23, 'Buahbatu (Margacinta)');
INSERT INTO `subdistricts` VALUES (350, 350, 23, 'Cibeunying Kaler');
INSERT INTO `subdistricts` VALUES (351, 351, 23, 'Cibeunying Kidul');
INSERT INTO `subdistricts` VALUES (352, 352, 23, 'Cibiru');
INSERT INTO `subdistricts` VALUES (353, 353, 23, 'Cicendo');
INSERT INTO `subdistricts` VALUES (354, 354, 23, 'Cidadap');
INSERT INTO `subdistricts` VALUES (355, 355, 23, 'Cinambo');
INSERT INTO `subdistricts` VALUES (356, 356, 23, 'Coblong');
INSERT INTO `subdistricts` VALUES (357, 357, 23, 'Gedebage');
INSERT INTO `subdistricts` VALUES (358, 358, 23, 'Kiaracondong');
INSERT INTO `subdistricts` VALUES (359, 359, 23, 'Lengkong');
INSERT INTO `subdistricts` VALUES (360, 360, 23, 'Mandalajati');
INSERT INTO `subdistricts` VALUES (361, 361, 23, 'Panyileukan');
INSERT INTO `subdistricts` VALUES (362, 362, 23, 'Rancasari');
INSERT INTO `subdistricts` VALUES (363, 363, 23, 'Regol');
INSERT INTO `subdistricts` VALUES (364, 364, 23, 'Sukajadi');
INSERT INTO `subdistricts` VALUES (365, 365, 23, 'Sukasari');
INSERT INTO `subdistricts` VALUES (366, 366, 23, 'Sumur Bandung');
INSERT INTO `subdistricts` VALUES (367, 367, 23, 'Ujung Berung');
INSERT INTO `subdistricts` VALUES (368, 368, 24, 'Batujajar');
INSERT INTO `subdistricts` VALUES (369, 369, 24, 'Cihampelas');
INSERT INTO `subdistricts` VALUES (370, 370, 24, 'Cikalong Wetan');
INSERT INTO `subdistricts` VALUES (371, 371, 24, 'Cililin');
INSERT INTO `subdistricts` VALUES (372, 372, 24, 'Cipatat');
INSERT INTO `subdistricts` VALUES (373, 373, 24, 'Cipeundeuy');
INSERT INTO `subdistricts` VALUES (374, 374, 24, 'Cipongkor');
INSERT INTO `subdistricts` VALUES (375, 375, 24, 'Cisarua');
INSERT INTO `subdistricts` VALUES (376, 376, 24, 'Gununghalu');
INSERT INTO `subdistricts` VALUES (377, 377, 24, 'Lembang');
INSERT INTO `subdistricts` VALUES (378, 378, 24, 'Ngamprah');
INSERT INTO `subdistricts` VALUES (379, 379, 24, 'Padalarang');
INSERT INTO `subdistricts` VALUES (380, 380, 24, 'Parongpong');
INSERT INTO `subdistricts` VALUES (381, 381, 24, 'Rongga');
INSERT INTO `subdistricts` VALUES (382, 382, 24, 'Saguling');
INSERT INTO `subdistricts` VALUES (383, 383, 24, 'Sindangkerta');
INSERT INTO `subdistricts` VALUES (384, 384, 25, 'Balantak');
INSERT INTO `subdistricts` VALUES (385, 385, 25, 'Balantak Selatan');
INSERT INTO `subdistricts` VALUES (386, 386, 25, 'Balantak Utara');
INSERT INTO `subdistricts` VALUES (387, 387, 25, 'Batui');
INSERT INTO `subdistricts` VALUES (388, 388, 25, 'Batui Selatan');
INSERT INTO `subdistricts` VALUES (389, 389, 25, 'Bualemo (Boalemo)');
INSERT INTO `subdistricts` VALUES (390, 390, 25, 'Bunta');
INSERT INTO `subdistricts` VALUES (391, 391, 25, 'Kintom');
INSERT INTO `subdistricts` VALUES (392, 392, 25, 'Lamala');
INSERT INTO `subdistricts` VALUES (393, 393, 25, 'Lobu');
INSERT INTO `subdistricts` VALUES (394, 394, 25, 'Luwuk');
INSERT INTO `subdistricts` VALUES (395, 395, 25, 'Luwuk Selatan');
INSERT INTO `subdistricts` VALUES (396, 396, 25, 'Luwuk Timur');
INSERT INTO `subdistricts` VALUES (397, 397, 25, 'Luwuk Utara');
INSERT INTO `subdistricts` VALUES (398, 398, 25, 'Mantoh');
INSERT INTO `subdistricts` VALUES (399, 399, 25, 'Masama');
INSERT INTO `subdistricts` VALUES (400, 400, 25, 'Moilong');
INSERT INTO `subdistricts` VALUES (401, 401, 25, 'Nambo');
INSERT INTO `subdistricts` VALUES (402, 402, 25, 'Nuhon');
INSERT INTO `subdistricts` VALUES (403, 403, 25, 'Pagimana');
INSERT INTO `subdistricts` VALUES (404, 404, 25, 'Simpang Raya');
INSERT INTO `subdistricts` VALUES (405, 405, 25, 'Toili');
INSERT INTO `subdistricts` VALUES (406, 406, 25, 'Toili Barat');
INSERT INTO `subdistricts` VALUES (407, 407, 26, 'Banggai');
INSERT INTO `subdistricts` VALUES (408, 408, 26, 'Banggai Selatan');
INSERT INTO `subdistricts` VALUES (409, 409, 26, 'Banggai Tengah');
INSERT INTO `subdistricts` VALUES (410, 410, 26, 'Banggai Utara');
INSERT INTO `subdistricts` VALUES (411, 411, 26, 'Bangkurung');
INSERT INTO `subdistricts` VALUES (412, 412, 26, 'Bokan Kepulauan');
INSERT INTO `subdistricts` VALUES (413, 413, 26, 'Buko');
INSERT INTO `subdistricts` VALUES (414, 414, 26, 'Buko Selatan');
INSERT INTO `subdistricts` VALUES (415, 415, 26, 'Bulagi');
INSERT INTO `subdistricts` VALUES (416, 416, 26, 'Bulagi Selatan');
INSERT INTO `subdistricts` VALUES (417, 417, 26, 'Bulagi Utara');
INSERT INTO `subdistricts` VALUES (418, 418, 26, 'Labobo (Lobangkurung)');
INSERT INTO `subdistricts` VALUES (419, 419, 26, 'Liang');
INSERT INTO `subdistricts` VALUES (420, 420, 26, 'Peling Tengah');
INSERT INTO `subdistricts` VALUES (421, 421, 26, 'Tinangkung');
INSERT INTO `subdistricts` VALUES (422, 422, 26, 'Tinangkung Selatan');
INSERT INTO `subdistricts` VALUES (423, 423, 26, 'Tinangkung Utara');
INSERT INTO `subdistricts` VALUES (424, 424, 26, 'Totikum (Totikung)');
INSERT INTO `subdistricts` VALUES (425, 425, 26, 'Totikum Selatan');
INSERT INTO `subdistricts` VALUES (426, 426, 27, 'Bakam');
INSERT INTO `subdistricts` VALUES (427, 427, 27, 'Belinyu');
INSERT INTO `subdistricts` VALUES (428, 428, 27, 'Mendo Barat');
INSERT INTO `subdistricts` VALUES (429, 429, 27, 'Merawang');
INSERT INTO `subdistricts` VALUES (430, 430, 27, 'Pemali');
INSERT INTO `subdistricts` VALUES (431, 431, 27, 'Puding Besar');
INSERT INTO `subdistricts` VALUES (432, 432, 27, 'Riau Silip');
INSERT INTO `subdistricts` VALUES (433, 433, 27, 'Sungai Liat');
INSERT INTO `subdistricts` VALUES (434, 434, 28, 'Jebus');
INSERT INTO `subdistricts` VALUES (435, 435, 28, 'Kelapa');
INSERT INTO `subdistricts` VALUES (436, 436, 28, 'Mentok (Muntok)');
INSERT INTO `subdistricts` VALUES (437, 437, 28, 'Parittiga');
INSERT INTO `subdistricts` VALUES (438, 438, 28, 'Simpang Teritip');
INSERT INTO `subdistricts` VALUES (439, 439, 28, 'Tempilang');
INSERT INTO `subdistricts` VALUES (440, 440, 29, 'Air Gegas');
INSERT INTO `subdistricts` VALUES (441, 441, 29, 'Kepulauan Pongok');
INSERT INTO `subdistricts` VALUES (442, 442, 29, 'Lepar Pongok');
INSERT INTO `subdistricts` VALUES (443, 443, 29, 'Payung');
INSERT INTO `subdistricts` VALUES (444, 444, 29, 'Pulau Besar');
INSERT INTO `subdistricts` VALUES (445, 445, 29, 'Simpang Rimba');
INSERT INTO `subdistricts` VALUES (446, 446, 29, 'Toboali');
INSERT INTO `subdistricts` VALUES (447, 447, 29, 'Tukak Sadai');
INSERT INTO `subdistricts` VALUES (448, 448, 30, 'Koba');
INSERT INTO `subdistricts` VALUES (449, 449, 30, 'Lubuk Besar');
INSERT INTO `subdistricts` VALUES (450, 450, 30, 'Namang');
INSERT INTO `subdistricts` VALUES (451, 451, 30, 'Pangkalan Baru');
INSERT INTO `subdistricts` VALUES (452, 452, 30, 'Simpang Katis');
INSERT INTO `subdistricts` VALUES (453, 453, 30, 'Sungai Selan');
INSERT INTO `subdistricts` VALUES (454, 454, 31, 'Arosbaya');
INSERT INTO `subdistricts` VALUES (455, 455, 31, 'Bangkalan');
INSERT INTO `subdistricts` VALUES (456, 456, 31, 'Blega');
INSERT INTO `subdistricts` VALUES (457, 457, 31, 'Burneh');
INSERT INTO `subdistricts` VALUES (458, 458, 31, 'Galis');
INSERT INTO `subdistricts` VALUES (459, 459, 31, 'Geger');
INSERT INTO `subdistricts` VALUES (460, 460, 31, 'Kamal');
INSERT INTO `subdistricts` VALUES (461, 461, 31, 'Klampis');
INSERT INTO `subdistricts` VALUES (462, 462, 31, 'Kokop');
INSERT INTO `subdistricts` VALUES (463, 463, 31, 'Konang');
INSERT INTO `subdistricts` VALUES (464, 464, 31, 'Kwanyar');
INSERT INTO `subdistricts` VALUES (465, 465, 31, 'Labang');
INSERT INTO `subdistricts` VALUES (466, 466, 31, 'Modung');
INSERT INTO `subdistricts` VALUES (467, 467, 31, 'Sepulu');
INSERT INTO `subdistricts` VALUES (468, 468, 31, 'Socah');
INSERT INTO `subdistricts` VALUES (469, 469, 31, 'Tanah Merah');
INSERT INTO `subdistricts` VALUES (470, 470, 31, 'Tanjungbumi');
INSERT INTO `subdistricts` VALUES (471, 471, 31, 'Tragah');
INSERT INTO `subdistricts` VALUES (472, 472, 32, 'Bangli');
INSERT INTO `subdistricts` VALUES (473, 473, 32, 'Kintamani');
INSERT INTO `subdistricts` VALUES (474, 474, 32, 'Susut');
INSERT INTO `subdistricts` VALUES (475, 475, 32, 'Tembuku');
INSERT INTO `subdistricts` VALUES (476, 476, 33, 'Aluh-Aluh');
INSERT INTO `subdistricts` VALUES (477, 477, 33, 'Aranio');
INSERT INTO `subdistricts` VALUES (478, 478, 33, 'Astambul');
INSERT INTO `subdistricts` VALUES (479, 479, 33, 'Beruntung Baru');
INSERT INTO `subdistricts` VALUES (480, 480, 33, 'Gambut');
INSERT INTO `subdistricts` VALUES (481, 481, 33, 'Karang Intan');
INSERT INTO `subdistricts` VALUES (482, 482, 33, 'Kertak Hanyar');
INSERT INTO `subdistricts` VALUES (483, 483, 33, 'Martapura Barat');
INSERT INTO `subdistricts` VALUES (484, 484, 33, 'Martapura Kota');
INSERT INTO `subdistricts` VALUES (485, 485, 33, 'Martapura Timur');
INSERT INTO `subdistricts` VALUES (486, 486, 33, 'Mataraman');
INSERT INTO `subdistricts` VALUES (487, 487, 33, 'Pengaron');
INSERT INTO `subdistricts` VALUES (488, 488, 33, 'Peramasan');
INSERT INTO `subdistricts` VALUES (489, 489, 33, 'Sambung Makmur');
INSERT INTO `subdistricts` VALUES (490, 490, 33, 'Sei/Sungai Pinang');
INSERT INTO `subdistricts` VALUES (491, 491, 33, 'Sei/Sungai Tabuk');
INSERT INTO `subdistricts` VALUES (492, 492, 33, 'Simpang Empat');
INSERT INTO `subdistricts` VALUES (493, 493, 33, 'Tatah Makmur');
INSERT INTO `subdistricts` VALUES (494, 494, 33, 'Telaga Bauntung');
INSERT INTO `subdistricts` VALUES (495, 495, 34, 'Banjar');
INSERT INTO `subdistricts` VALUES (496, 496, 34, 'Langensari');
INSERT INTO `subdistricts` VALUES (497, 497, 34, 'Pataruman');
INSERT INTO `subdistricts` VALUES (498, 498, 34, 'Purwaharja');
INSERT INTO `subdistricts` VALUES (499, 499, 35, 'Banjar Baru Selatan');
INSERT INTO `subdistricts` VALUES (500, 500, 35, 'Banjar Baru Utara');
INSERT INTO `subdistricts` VALUES (501, 501, 35, 'Cempaka');
INSERT INTO `subdistricts` VALUES (502, 502, 35, 'Landasan Ulin');
INSERT INTO `subdistricts` VALUES (503, 503, 35, 'Liang Anggang');
INSERT INTO `subdistricts` VALUES (504, 504, 36, 'Banjarmasin Barat');
INSERT INTO `subdistricts` VALUES (505, 505, 36, 'Banjarmasin Selatan');
INSERT INTO `subdistricts` VALUES (506, 506, 36, 'Banjarmasin Tengah');
INSERT INTO `subdistricts` VALUES (507, 507, 36, 'Banjarmasin Timur');
INSERT INTO `subdistricts` VALUES (508, 508, 36, 'Banjarmasin Utara');
INSERT INTO `subdistricts` VALUES (509, 509, 37, 'Banjarmangu');
INSERT INTO `subdistricts` VALUES (510, 510, 37, 'Banjarnegara');
INSERT INTO `subdistricts` VALUES (511, 511, 37, 'Batur');
INSERT INTO `subdistricts` VALUES (512, 512, 37, 'Bawang');
INSERT INTO `subdistricts` VALUES (513, 513, 37, 'Kalibening');
INSERT INTO `subdistricts` VALUES (514, 514, 37, 'Karangkobar');
INSERT INTO `subdistricts` VALUES (515, 515, 37, 'Madukara');
INSERT INTO `subdistricts` VALUES (516, 516, 37, 'Mandiraja');
INSERT INTO `subdistricts` VALUES (517, 517, 37, 'Pagedongan');
INSERT INTO `subdistricts` VALUES (518, 518, 37, 'Pagentan');
INSERT INTO `subdistricts` VALUES (519, 519, 37, 'Pandanarum');
INSERT INTO `subdistricts` VALUES (520, 520, 37, 'Pejawaran');
INSERT INTO `subdistricts` VALUES (521, 521, 37, 'Punggelan');
INSERT INTO `subdistricts` VALUES (522, 522, 37, 'Purwonegoro');
INSERT INTO `subdistricts` VALUES (523, 523, 37, 'Purworejo Klampok');
INSERT INTO `subdistricts` VALUES (524, 524, 37, 'Rakit');
INSERT INTO `subdistricts` VALUES (525, 525, 37, 'Sigaluh');
INSERT INTO `subdistricts` VALUES (526, 526, 37, 'Susukan');
INSERT INTO `subdistricts` VALUES (527, 527, 37, 'Wanadadi (Wonodadi)');
INSERT INTO `subdistricts` VALUES (528, 528, 37, 'Wanayasa');
INSERT INTO `subdistricts` VALUES (529, 529, 38, 'Bantaeng');
INSERT INTO `subdistricts` VALUES (530, 530, 38, 'Bissappu');
INSERT INTO `subdistricts` VALUES (531, 531, 38, 'Eremerasa');
INSERT INTO `subdistricts` VALUES (532, 532, 38, 'Gantarang Keke (Gantareng Keke)');
INSERT INTO `subdistricts` VALUES (533, 533, 38, 'Pajukukang');
INSERT INTO `subdistricts` VALUES (534, 534, 38, 'Sinoa');
INSERT INTO `subdistricts` VALUES (535, 535, 38, 'Tompobulu');
INSERT INTO `subdistricts` VALUES (536, 536, 38, 'Uluere');
INSERT INTO `subdistricts` VALUES (537, 537, 39, 'Bambang Lipuro');
INSERT INTO `subdistricts` VALUES (538, 538, 39, 'Banguntapan');
INSERT INTO `subdistricts` VALUES (539, 539, 39, 'Bantul');
INSERT INTO `subdistricts` VALUES (540, 540, 39, 'Dlingo');
INSERT INTO `subdistricts` VALUES (541, 541, 39, 'Imogiri');
INSERT INTO `subdistricts` VALUES (542, 542, 39, 'Jetis');
INSERT INTO `subdistricts` VALUES (543, 543, 39, 'Kasihan');
INSERT INTO `subdistricts` VALUES (544, 544, 39, 'Kretek');
INSERT INTO `subdistricts` VALUES (545, 545, 39, 'Pajangan');
INSERT INTO `subdistricts` VALUES (546, 546, 39, 'Pandak');
INSERT INTO `subdistricts` VALUES (547, 547, 39, 'Piyungan');
INSERT INTO `subdistricts` VALUES (548, 548, 39, 'Pleret');
INSERT INTO `subdistricts` VALUES (549, 549, 39, 'Pundong');
INSERT INTO `subdistricts` VALUES (550, 550, 39, 'Sanden');
INSERT INTO `subdistricts` VALUES (551, 551, 39, 'Sedayu');
INSERT INTO `subdistricts` VALUES (552, 552, 39, 'Sewon');
INSERT INTO `subdistricts` VALUES (553, 553, 39, 'Srandakan');
INSERT INTO `subdistricts` VALUES (554, 554, 40, 'Air Kumbang');
INSERT INTO `subdistricts` VALUES (555, 555, 40, 'Air Salek');
INSERT INTO `subdistricts` VALUES (556, 556, 40, 'Banyuasin I');
INSERT INTO `subdistricts` VALUES (557, 557, 40, 'Banyuasin II');
INSERT INTO `subdistricts` VALUES (558, 558, 40, 'Banyuasin III');
INSERT INTO `subdistricts` VALUES (559, 559, 40, 'Betung');
INSERT INTO `subdistricts` VALUES (560, 560, 40, 'Makarti Jaya');
INSERT INTO `subdistricts` VALUES (561, 561, 40, 'Muara Padang');
INSERT INTO `subdistricts` VALUES (562, 562, 40, 'Muara Sugihan');
INSERT INTO `subdistricts` VALUES (563, 563, 40, 'Muara Telang');
INSERT INTO `subdistricts` VALUES (564, 564, 40, 'Pulau Rimau');
INSERT INTO `subdistricts` VALUES (565, 565, 40, 'Rambutan');
INSERT INTO `subdistricts` VALUES (566, 566, 40, 'Rantau Bayur');
INSERT INTO `subdistricts` VALUES (567, 567, 40, 'Sembawa');
INSERT INTO `subdistricts` VALUES (568, 568, 40, 'Suak Tapeh');
INSERT INTO `subdistricts` VALUES (569, 569, 40, 'Sumber Marga Telang');
INSERT INTO `subdistricts` VALUES (570, 570, 40, 'Talang Kelapa');
INSERT INTO `subdistricts` VALUES (571, 571, 40, 'Tanjung Lago');
INSERT INTO `subdistricts` VALUES (572, 572, 40, 'Tungkal Ilir');
INSERT INTO `subdistricts` VALUES (573, 573, 41, 'Ajibarang');
INSERT INTO `subdistricts` VALUES (574, 574, 41, 'Banyumas');
INSERT INTO `subdistricts` VALUES (575, 575, 41, 'Baturaden');
INSERT INTO `subdistricts` VALUES (576, 576, 41, 'Cilongok');
INSERT INTO `subdistricts` VALUES (577, 577, 41, 'Gumelar');
INSERT INTO `subdistricts` VALUES (578, 578, 41, 'Jatilawang');
INSERT INTO `subdistricts` VALUES (579, 579, 41, 'Kalibagor');
INSERT INTO `subdistricts` VALUES (580, 580, 41, 'Karanglewas');
INSERT INTO `subdistricts` VALUES (581, 581, 41, 'Kebasen');
INSERT INTO `subdistricts` VALUES (582, 582, 41, 'Kedung Banteng');
INSERT INTO `subdistricts` VALUES (583, 583, 41, 'Kembaran');
INSERT INTO `subdistricts` VALUES (584, 584, 41, 'Kemranjen');
INSERT INTO `subdistricts` VALUES (585, 585, 41, 'Lumbir');
INSERT INTO `subdistricts` VALUES (586, 586, 41, 'Patikraja');
INSERT INTO `subdistricts` VALUES (587, 587, 41, 'Pekuncen');
INSERT INTO `subdistricts` VALUES (588, 588, 41, 'Purwojati');
INSERT INTO `subdistricts` VALUES (589, 589, 41, 'Purwokerto Barat');
INSERT INTO `subdistricts` VALUES (590, 590, 41, 'Purwokerto Selatan');
INSERT INTO `subdistricts` VALUES (591, 591, 41, 'Purwokerto Timur');
INSERT INTO `subdistricts` VALUES (592, 592, 41, 'Purwokerto Utara');
INSERT INTO `subdistricts` VALUES (593, 593, 41, 'Rawalo');
INSERT INTO `subdistricts` VALUES (594, 594, 41, 'Sokaraja');
INSERT INTO `subdistricts` VALUES (595, 595, 41, 'Somagede');
INSERT INTO `subdistricts` VALUES (596, 596, 41, 'Sumbang');
INSERT INTO `subdistricts` VALUES (597, 597, 41, 'Sumpiuh');
INSERT INTO `subdistricts` VALUES (598, 598, 41, 'Tambak');
INSERT INTO `subdistricts` VALUES (599, 599, 41, 'Wangon');
INSERT INTO `subdistricts` VALUES (600, 600, 42, 'Bangorejo');
INSERT INTO `subdistricts` VALUES (601, 601, 42, 'Banyuwangi');
INSERT INTO `subdistricts` VALUES (602, 602, 42, 'Cluring');
INSERT INTO `subdistricts` VALUES (603, 603, 42, 'Gambiran');
INSERT INTO `subdistricts` VALUES (604, 604, 42, 'Genteng');
INSERT INTO `subdistricts` VALUES (605, 605, 42, 'Giri');
INSERT INTO `subdistricts` VALUES (606, 606, 42, 'Glagah');
INSERT INTO `subdistricts` VALUES (607, 607, 42, 'Glenmore');
INSERT INTO `subdistricts` VALUES (608, 608, 42, 'Kabat');
INSERT INTO `subdistricts` VALUES (609, 609, 42, 'Kalibaru');
INSERT INTO `subdistricts` VALUES (610, 610, 42, 'Kalipuro');
INSERT INTO `subdistricts` VALUES (611, 611, 42, 'Licin');
INSERT INTO `subdistricts` VALUES (612, 612, 42, 'Muncar');
INSERT INTO `subdistricts` VALUES (613, 613, 42, 'Pesanggaran');
INSERT INTO `subdistricts` VALUES (614, 614, 42, 'Purwoharjo');
INSERT INTO `subdistricts` VALUES (615, 615, 42, 'Rogojampi');
INSERT INTO `subdistricts` VALUES (616, 616, 42, 'Sempu');
INSERT INTO `subdistricts` VALUES (617, 617, 42, 'Siliragung');
INSERT INTO `subdistricts` VALUES (618, 618, 42, 'Singojuruh');
INSERT INTO `subdistricts` VALUES (619, 619, 42, 'Songgon');
INSERT INTO `subdistricts` VALUES (620, 620, 42, 'Srono');
INSERT INTO `subdistricts` VALUES (621, 621, 42, 'Tegaldlimo');
INSERT INTO `subdistricts` VALUES (622, 622, 42, 'Tegalsari');
INSERT INTO `subdistricts` VALUES (623, 623, 42, 'Wongsorejo');
INSERT INTO `subdistricts` VALUES (624, 624, 43, 'Alalak');
INSERT INTO `subdistricts` VALUES (625, 625, 43, 'Anjir Muara');
INSERT INTO `subdistricts` VALUES (626, 626, 43, 'Anjir Pasar');
INSERT INTO `subdistricts` VALUES (627, 627, 43, 'Bakumpai');
INSERT INTO `subdistricts` VALUES (628, 628, 43, 'Barambai');
INSERT INTO `subdistricts` VALUES (629, 629, 43, 'Belawang');
INSERT INTO `subdistricts` VALUES (630, 630, 43, 'Cerbon');
INSERT INTO `subdistricts` VALUES (631, 631, 43, 'Jejangkit');
INSERT INTO `subdistricts` VALUES (632, 632, 43, 'Kuripan');
INSERT INTO `subdistricts` VALUES (633, 633, 43, 'Mandastana');
INSERT INTO `subdistricts` VALUES (634, 634, 43, 'Marabahan');
INSERT INTO `subdistricts` VALUES (635, 635, 43, 'Mekar Sari');
INSERT INTO `subdistricts` VALUES (636, 636, 43, 'Rantau Badauh');
INSERT INTO `subdistricts` VALUES (637, 637, 43, 'Tabukan');
INSERT INTO `subdistricts` VALUES (638, 638, 43, 'Tabunganen');
INSERT INTO `subdistricts` VALUES (639, 639, 43, 'Tamban');
INSERT INTO `subdistricts` VALUES (640, 640, 43, 'Wanaraya');
INSERT INTO `subdistricts` VALUES (641, 641, 44, 'Dusun Hilir');
INSERT INTO `subdistricts` VALUES (642, 642, 44, 'Dusun Selatan');
INSERT INTO `subdistricts` VALUES (643, 643, 44, 'Dusun Utara');
INSERT INTO `subdistricts` VALUES (644, 644, 44, 'Gunung Bintang Awai');
INSERT INTO `subdistricts` VALUES (645, 645, 44, 'Jenamas');
INSERT INTO `subdistricts` VALUES (646, 646, 44, 'Karau Kuala');
INSERT INTO `subdistricts` VALUES (647, 647, 45, 'Awang');
INSERT INTO `subdistricts` VALUES (648, 648, 45, 'Benua Lima');
INSERT INTO `subdistricts` VALUES (649, 649, 45, 'Dusun Tengah');
INSERT INTO `subdistricts` VALUES (650, 650, 45, 'Dusun Timur');
INSERT INTO `subdistricts` VALUES (651, 651, 45, 'Karusen Janang');
INSERT INTO `subdistricts` VALUES (652, 652, 45, 'Paju Epat');
INSERT INTO `subdistricts` VALUES (653, 653, 45, 'Paku');
INSERT INTO `subdistricts` VALUES (654, 654, 45, 'Patangkep Tutui');
INSERT INTO `subdistricts` VALUES (655, 655, 45, 'Pematang Karau');
INSERT INTO `subdistricts` VALUES (656, 656, 45, 'Raren Batuah');
INSERT INTO `subdistricts` VALUES (657, 657, 46, 'Gunung Purei');
INSERT INTO `subdistricts` VALUES (658, 658, 46, 'Gunung Timang');
INSERT INTO `subdistricts` VALUES (659, 659, 46, 'Lahei');
INSERT INTO `subdistricts` VALUES (660, 660, 46, 'Lahei Barat');
INSERT INTO `subdistricts` VALUES (661, 661, 46, 'Montallat (Montalat)');
INSERT INTO `subdistricts` VALUES (662, 662, 46, 'Teweh Baru');
INSERT INTO `subdistricts` VALUES (663, 663, 46, 'Teweh Selatan');
INSERT INTO `subdistricts` VALUES (664, 664, 46, 'Teweh Tengah');
INSERT INTO `subdistricts` VALUES (665, 665, 46, 'Teweh Timur');
INSERT INTO `subdistricts` VALUES (666, 666, 47, 'Balusu');
INSERT INTO `subdistricts` VALUES (667, 667, 47, 'Barru');
INSERT INTO `subdistricts` VALUES (668, 668, 47, 'Mallusetasi');
INSERT INTO `subdistricts` VALUES (669, 669, 47, 'Pujananting');
INSERT INTO `subdistricts` VALUES (670, 670, 47, 'Soppeng Riaja');
INSERT INTO `subdistricts` VALUES (671, 671, 47, 'Tanete Riaja');
INSERT INTO `subdistricts` VALUES (672, 672, 47, 'Tanete Rilau');
INSERT INTO `subdistricts` VALUES (673, 673, 48, 'Batam Kota');
INSERT INTO `subdistricts` VALUES (674, 674, 48, 'Batu Aji');
INSERT INTO `subdistricts` VALUES (675, 675, 48, 'Batu Ampar');
INSERT INTO `subdistricts` VALUES (676, 676, 48, 'Belakang Padang');
INSERT INTO `subdistricts` VALUES (677, 677, 48, 'Bengkong');
INSERT INTO `subdistricts` VALUES (678, 678, 48, 'Bulang');
INSERT INTO `subdistricts` VALUES (679, 679, 48, 'Galang');
INSERT INTO `subdistricts` VALUES (680, 680, 48, 'Lubuk Baja');
INSERT INTO `subdistricts` VALUES (681, 681, 48, 'Nongsa');
INSERT INTO `subdistricts` VALUES (682, 682, 48, 'Sagulung');
INSERT INTO `subdistricts` VALUES (683, 683, 48, 'Sei/Sungai Beduk');
INSERT INTO `subdistricts` VALUES (684, 684, 48, 'Sekupang');
INSERT INTO `subdistricts` VALUES (685, 685, 49, 'Bandar');
INSERT INTO `subdistricts` VALUES (686, 686, 49, 'Banyuputih');
INSERT INTO `subdistricts` VALUES (687, 687, 49, 'Batang');
INSERT INTO `subdistricts` VALUES (688, 688, 49, 'Bawang');
INSERT INTO `subdistricts` VALUES (689, 689, 49, 'Blado');
INSERT INTO `subdistricts` VALUES (690, 690, 49, 'Gringsing');
INSERT INTO `subdistricts` VALUES (691, 691, 49, 'Kandeman');
INSERT INTO `subdistricts` VALUES (692, 692, 49, 'Limpung');
INSERT INTO `subdistricts` VALUES (693, 693, 49, 'Pecalungan');
INSERT INTO `subdistricts` VALUES (694, 694, 49, 'Reban');
INSERT INTO `subdistricts` VALUES (695, 695, 49, 'Subah');
INSERT INTO `subdistricts` VALUES (696, 696, 49, 'Tersono');
INSERT INTO `subdistricts` VALUES (697, 697, 49, 'Tulis');
INSERT INTO `subdistricts` VALUES (698, 698, 49, 'Warungasem');
INSERT INTO `subdistricts` VALUES (699, 699, 49, 'Wonotunggal');
INSERT INTO `subdistricts` VALUES (700, 700, 50, 'Bajubang');
INSERT INTO `subdistricts` VALUES (701, 701, 50, 'Batin XXIV');
INSERT INTO `subdistricts` VALUES (702, 702, 50, 'Maro Sebo Ilir');
INSERT INTO `subdistricts` VALUES (703, 703, 50, 'Maro Sebo Ulu');
INSERT INTO `subdistricts` VALUES (704, 704, 50, 'Mersam');
INSERT INTO `subdistricts` VALUES (705, 705, 50, 'Muara Bulian');
INSERT INTO `subdistricts` VALUES (706, 706, 50, 'Muara Tembesi');
INSERT INTO `subdistricts` VALUES (707, 707, 50, 'Pemayung');
INSERT INTO `subdistricts` VALUES (708, 708, 51, 'Batu');
INSERT INTO `subdistricts` VALUES (709, 709, 51, 'Bumiaji');
INSERT INTO `subdistricts` VALUES (710, 710, 51, 'Junrejo');
INSERT INTO `subdistricts` VALUES (711, 711, 52, 'Air Putih');
INSERT INTO `subdistricts` VALUES (712, 712, 52, 'Limapuluh');
INSERT INTO `subdistricts` VALUES (713, 713, 52, 'Medang Deras');
INSERT INTO `subdistricts` VALUES (714, 714, 52, 'Sei Balai');
INSERT INTO `subdistricts` VALUES (715, 715, 52, 'Sei Suka');
INSERT INTO `subdistricts` VALUES (716, 716, 52, 'Talawi');
INSERT INTO `subdistricts` VALUES (717, 717, 52, 'Tanjung Tiram');
INSERT INTO `subdistricts` VALUES (718, 718, 53, 'Batupoaro');
INSERT INTO `subdistricts` VALUES (719, 719, 53, 'Betoambari');
INSERT INTO `subdistricts` VALUES (720, 720, 53, 'Bungi');
INSERT INTO `subdistricts` VALUES (721, 721, 53, 'Kokalukuna');
INSERT INTO `subdistricts` VALUES (722, 722, 53, 'Lea-Lea');
INSERT INTO `subdistricts` VALUES (723, 723, 53, 'Murhum');
INSERT INTO `subdistricts` VALUES (724, 724, 53, 'Sora Walio (Sorowalio)');
INSERT INTO `subdistricts` VALUES (725, 725, 53, 'Wolio');
INSERT INTO `subdistricts` VALUES (726, 726, 54, 'Babelan');
INSERT INTO `subdistricts` VALUES (727, 727, 54, 'Bojongmangu');
INSERT INTO `subdistricts` VALUES (728, 728, 54, 'Cabangbungin');
INSERT INTO `subdistricts` VALUES (729, 729, 54, 'Cibarusah');
INSERT INTO `subdistricts` VALUES (730, 730, 54, 'Cibitung');
INSERT INTO `subdistricts` VALUES (731, 731, 54, 'Cikarang Barat');
INSERT INTO `subdistricts` VALUES (732, 732, 54, 'Cikarang Pusat');
INSERT INTO `subdistricts` VALUES (733, 733, 54, 'Cikarang Selatan');
INSERT INTO `subdistricts` VALUES (734, 734, 54, 'Cikarang Timur');
INSERT INTO `subdistricts` VALUES (735, 735, 54, 'Cikarang Utara');
INSERT INTO `subdistricts` VALUES (736, 736, 54, 'Karangbahagia');
INSERT INTO `subdistricts` VALUES (737, 737, 54, 'Kedung Waringin');
INSERT INTO `subdistricts` VALUES (738, 738, 54, 'Muara Gembong');
INSERT INTO `subdistricts` VALUES (739, 739, 54, 'Pebayuran');
INSERT INTO `subdistricts` VALUES (740, 740, 54, 'Serang Baru');
INSERT INTO `subdistricts` VALUES (741, 741, 54, 'Setu');
INSERT INTO `subdistricts` VALUES (742, 742, 54, 'Sukakarya');
INSERT INTO `subdistricts` VALUES (743, 743, 54, 'Sukatani');
INSERT INTO `subdistricts` VALUES (744, 744, 54, 'Sukawangi');
INSERT INTO `subdistricts` VALUES (745, 745, 54, 'Tambelang');
INSERT INTO `subdistricts` VALUES (746, 746, 54, 'Tambun Selatan');
INSERT INTO `subdistricts` VALUES (747, 747, 54, 'Tambun Utara');
INSERT INTO `subdistricts` VALUES (748, 748, 54, 'Tarumajaya');
INSERT INTO `subdistricts` VALUES (749, 749, 55, 'Bantar Gebang');
INSERT INTO `subdistricts` VALUES (750, 750, 55, 'Bekasi Barat');
INSERT INTO `subdistricts` VALUES (751, 751, 55, 'Bekasi Selatan');
INSERT INTO `subdistricts` VALUES (752, 752, 55, 'Bekasi Timur');
INSERT INTO `subdistricts` VALUES (753, 753, 55, 'Bekasi Utara');
INSERT INTO `subdistricts` VALUES (754, 754, 55, 'Jati Sampurna');
INSERT INTO `subdistricts` VALUES (755, 755, 55, 'Jatiasih');
INSERT INTO `subdistricts` VALUES (756, 756, 55, 'Medan Satria');
INSERT INTO `subdistricts` VALUES (757, 757, 55, 'Mustika Jaya');
INSERT INTO `subdistricts` VALUES (758, 758, 55, 'Pondok Gede');
INSERT INTO `subdistricts` VALUES (759, 759, 55, 'Pondok Melati');
INSERT INTO `subdistricts` VALUES (760, 760, 55, 'Rawalumbu');
INSERT INTO `subdistricts` VALUES (761, 761, 56, 'Badau');
INSERT INTO `subdistricts` VALUES (762, 762, 56, 'Membalong');
INSERT INTO `subdistricts` VALUES (763, 763, 56, 'Selat Nasik');
INSERT INTO `subdistricts` VALUES (764, 764, 56, 'Sijuk');
INSERT INTO `subdistricts` VALUES (765, 765, 56, 'Tanjung Pandan');
INSERT INTO `subdistricts` VALUES (766, 766, 57, 'Damar');
INSERT INTO `subdistricts` VALUES (767, 767, 57, 'Dendang');
INSERT INTO `subdistricts` VALUES (768, 768, 57, 'Gantung');
INSERT INTO `subdistricts` VALUES (769, 769, 57, 'Kelapa Kampit');
INSERT INTO `subdistricts` VALUES (770, 770, 57, 'Manggar');
INSERT INTO `subdistricts` VALUES (771, 771, 57, 'Simpang Pesak');
INSERT INTO `subdistricts` VALUES (772, 772, 57, 'Simpang Renggiang');
INSERT INTO `subdistricts` VALUES (773, 773, 58, 'Atambua Barat');
INSERT INTO `subdistricts` VALUES (774, 774, 58, 'Atambua Kota');
INSERT INTO `subdistricts` VALUES (775, 775, 58, 'Atambua Selatan');
INSERT INTO `subdistricts` VALUES (776, 776, 58, 'Botin Leo Bele');
INSERT INTO `subdistricts` VALUES (777, 777, 58, 'Io Kufeu');
INSERT INTO `subdistricts` VALUES (778, 778, 58, 'Kakuluk Mesak');
INSERT INTO `subdistricts` VALUES (779, 779, 58, 'Kobalima');
INSERT INTO `subdistricts` VALUES (780, 780, 58, 'Kobalima Timur');
INSERT INTO `subdistricts` VALUES (781, 781, 58, 'Laen Manen');
INSERT INTO `subdistricts` VALUES (782, 782, 58, 'Lamaknen');
INSERT INTO `subdistricts` VALUES (783, 783, 58, 'Lamaknen Selatan');
INSERT INTO `subdistricts` VALUES (784, 784, 58, 'Lasiolat');
INSERT INTO `subdistricts` VALUES (785, 785, 58, 'Malaka Barat');
INSERT INTO `subdistricts` VALUES (786, 786, 58, 'Malaka Tengah');
INSERT INTO `subdistricts` VALUES (787, 787, 58, 'Malaka Timur');
INSERT INTO `subdistricts` VALUES (788, 788, 58, 'Nanaet Duabesi');
INSERT INTO `subdistricts` VALUES (789, 789, 58, 'Raihat');
INSERT INTO `subdistricts` VALUES (790, 790, 58, 'Raimanuk');
INSERT INTO `subdistricts` VALUES (791, 791, 58, 'Rinhat');
INSERT INTO `subdistricts` VALUES (792, 792, 58, 'Sasitamean');
INSERT INTO `subdistricts` VALUES (793, 793, 58, 'Tasifeto Barat');
INSERT INTO `subdistricts` VALUES (794, 794, 58, 'Tasifeto Timur');
INSERT INTO `subdistricts` VALUES (795, 795, 58, 'Weliman');
INSERT INTO `subdistricts` VALUES (796, 796, 58, 'Wewiku');
INSERT INTO `subdistricts` VALUES (797, 797, 59, 'Bandar');
INSERT INTO `subdistricts` VALUES (798, 798, 59, 'Bener Kelipah');
INSERT INTO `subdistricts` VALUES (799, 799, 59, 'Bukit');
INSERT INTO `subdistricts` VALUES (800, 800, 59, 'Gajah Putih');
INSERT INTO `subdistricts` VALUES (801, 801, 59, 'Mesidah');
INSERT INTO `subdistricts` VALUES (802, 802, 59, 'Permata');
INSERT INTO `subdistricts` VALUES (803, 803, 59, 'Pintu Rime Gayo');
INSERT INTO `subdistricts` VALUES (804, 804, 59, 'Syiah Utama');
INSERT INTO `subdistricts` VALUES (805, 805, 59, 'Timang Gajah');
INSERT INTO `subdistricts` VALUES (806, 806, 59, 'Wih Pesam');
INSERT INTO `subdistricts` VALUES (807, 807, 60, 'Bantan');
INSERT INTO `subdistricts` VALUES (808, 808, 60, 'Bengkalis');
INSERT INTO `subdistricts` VALUES (809, 809, 60, 'Bukit Batu');
INSERT INTO `subdistricts` VALUES (810, 810, 60, 'Mandau');
INSERT INTO `subdistricts` VALUES (811, 811, 60, 'Pinggir');
INSERT INTO `subdistricts` VALUES (812, 812, 60, 'Rupat');
INSERT INTO `subdistricts` VALUES (813, 813, 60, 'Rupat Utara');
INSERT INTO `subdistricts` VALUES (814, 814, 60, 'Siak Kecil');
INSERT INTO `subdistricts` VALUES (815, 815, 61, 'Bengkayang');
INSERT INTO `subdistricts` VALUES (816, 816, 61, 'Capkala');
INSERT INTO `subdistricts` VALUES (817, 817, 61, 'Jagoi Babang');
INSERT INTO `subdistricts` VALUES (818, 818, 61, 'Ledo');
INSERT INTO `subdistricts` VALUES (819, 819, 61, 'Lembah Bawang');
INSERT INTO `subdistricts` VALUES (820, 820, 61, 'Lumar');
INSERT INTO `subdistricts` VALUES (821, 821, 61, 'Monterado');
INSERT INTO `subdistricts` VALUES (822, 822, 61, 'Samalantan');
INSERT INTO `subdistricts` VALUES (823, 823, 61, 'Sanggau Ledo');
INSERT INTO `subdistricts` VALUES (824, 824, 61, 'Seluas');
INSERT INTO `subdistricts` VALUES (825, 825, 61, 'Siding');
INSERT INTO `subdistricts` VALUES (826, 826, 61, 'Sungai Betung');
INSERT INTO `subdistricts` VALUES (827, 827, 61, 'Sungai Raya');
INSERT INTO `subdistricts` VALUES (828, 828, 61, 'Sungai Raya Kepulauan');
INSERT INTO `subdistricts` VALUES (829, 829, 61, 'Suti Semarang');
INSERT INTO `subdistricts` VALUES (830, 830, 61, 'Teriak');
INSERT INTO `subdistricts` VALUES (831, 831, 61, 'Tujuh Belas');
INSERT INTO `subdistricts` VALUES (832, 832, 62, 'Gading Cempaka');
INSERT INTO `subdistricts` VALUES (833, 833, 62, 'Kampung Melayu');
INSERT INTO `subdistricts` VALUES (834, 834, 62, 'Muara Bangka Hulu');
INSERT INTO `subdistricts` VALUES (835, 835, 62, 'Ratu Agung');
INSERT INTO `subdistricts` VALUES (836, 836, 62, 'Ratu Samban');
INSERT INTO `subdistricts` VALUES (837, 837, 62, 'Selebar');
INSERT INTO `subdistricts` VALUES (838, 838, 62, 'Singaran Pati');
INSERT INTO `subdistricts` VALUES (839, 839, 62, 'Sungai Serut');
INSERT INTO `subdistricts` VALUES (840, 840, 62, 'Teluk Segara');
INSERT INTO `subdistricts` VALUES (841, 841, 63, 'Air Nipis');
INSERT INTO `subdistricts` VALUES (842, 842, 63, 'Bunga Mas');
INSERT INTO `subdistricts` VALUES (843, 843, 63, 'Kedurang');
INSERT INTO `subdistricts` VALUES (844, 844, 63, 'Kedurang Ilir');
INSERT INTO `subdistricts` VALUES (845, 845, 63, 'Kota Manna');
INSERT INTO `subdistricts` VALUES (846, 846, 63, 'Manna');
INSERT INTO `subdistricts` VALUES (847, 847, 63, 'Pasar Manna');
INSERT INTO `subdistricts` VALUES (848, 848, 63, 'Pino');
INSERT INTO `subdistricts` VALUES (849, 849, 63, 'Pinoraya');
INSERT INTO `subdistricts` VALUES (850, 850, 63, 'Seginim');
INSERT INTO `subdistricts` VALUES (851, 851, 63, 'Ulu Manna');
INSERT INTO `subdistricts` VALUES (852, 852, 64, 'Bang Haji');
INSERT INTO `subdistricts` VALUES (853, 853, 64, 'Karang Tinggi');
INSERT INTO `subdistricts` VALUES (854, 854, 64, 'Merigi Kelindang');
INSERT INTO `subdistricts` VALUES (855, 855, 64, 'Merigi Sakti');
INSERT INTO `subdistricts` VALUES (856, 856, 64, 'Pagar Jati');
INSERT INTO `subdistricts` VALUES (857, 857, 64, 'Pematang Tiga');
INSERT INTO `subdistricts` VALUES (858, 858, 64, 'Pondok Kelapa');
INSERT INTO `subdistricts` VALUES (859, 859, 64, 'Pondok Kubang');
INSERT INTO `subdistricts` VALUES (860, 860, 64, 'Taba Penanjung');
INSERT INTO `subdistricts` VALUES (861, 861, 64, 'Talang Empat');
INSERT INTO `subdistricts` VALUES (862, 862, 65, 'Air Besi');
INSERT INTO `subdistricts` VALUES (863, 863, 65, 'Air Napal');
INSERT INTO `subdistricts` VALUES (864, 864, 65, 'Air Padang');
INSERT INTO `subdistricts` VALUES (865, 865, 65, 'Arga Makmur');
INSERT INTO `subdistricts` VALUES (866, 866, 65, 'Arma Jaya');
INSERT INTO `subdistricts` VALUES (867, 867, 65, 'Batik Nau');
INSERT INTO `subdistricts` VALUES (868, 868, 65, 'Enggano');
INSERT INTO `subdistricts` VALUES (869, 869, 65, 'Giri Mulia');
INSERT INTO `subdistricts` VALUES (870, 870, 65, 'Hulu Palik');
INSERT INTO `subdistricts` VALUES (871, 871, 65, 'Kerkap');
INSERT INTO `subdistricts` VALUES (872, 872, 65, 'Ketahun');
INSERT INTO `subdistricts` VALUES (873, 873, 65, 'Lais');
INSERT INTO `subdistricts` VALUES (874, 874, 65, 'Napal Putih');
INSERT INTO `subdistricts` VALUES (875, 875, 65, 'Padang Jaya');
INSERT INTO `subdistricts` VALUES (876, 876, 65, 'Putri Hijau');
INSERT INTO `subdistricts` VALUES (877, 877, 65, 'Tanjung Agung Palik');
INSERT INTO `subdistricts` VALUES (878, 878, 65, 'Ulok Kupai');
INSERT INTO `subdistricts` VALUES (879, 879, 66, 'Batu Putih');
INSERT INTO `subdistricts` VALUES (880, 880, 66, 'Biatan');
INSERT INTO `subdistricts` VALUES (881, 881, 66, 'Biduk-Biduk');
INSERT INTO `subdistricts` VALUES (882, 882, 66, 'Derawan (Pulau Derawan)');
INSERT INTO `subdistricts` VALUES (883, 883, 66, 'Gunung Tabur');
INSERT INTO `subdistricts` VALUES (884, 884, 66, 'Kelay');
INSERT INTO `subdistricts` VALUES (885, 885, 66, 'Maratua');
INSERT INTO `subdistricts` VALUES (886, 886, 66, 'Sambaliung');
INSERT INTO `subdistricts` VALUES (887, 887, 66, 'Segah');
INSERT INTO `subdistricts` VALUES (888, 888, 66, 'Tabalar');
INSERT INTO `subdistricts` VALUES (889, 889, 66, 'Talisayan');
INSERT INTO `subdistricts` VALUES (890, 890, 66, 'Tanjung Redeb');
INSERT INTO `subdistricts` VALUES (891, 891, 66, 'Teluk Bayur');
INSERT INTO `subdistricts` VALUES (892, 892, 67, 'Aimando Padaido');
INSERT INTO `subdistricts` VALUES (893, 893, 67, 'Andey (Andei)');
INSERT INTO `subdistricts` VALUES (894, 894, 67, 'Biak Barat');
INSERT INTO `subdistricts` VALUES (895, 895, 67, 'Biak Kota');
INSERT INTO `subdistricts` VALUES (896, 896, 67, 'Biak Timur');
INSERT INTO `subdistricts` VALUES (897, 897, 67, 'Biak Utara');
INSERT INTO `subdistricts` VALUES (898, 898, 67, 'Bondifuar');
INSERT INTO `subdistricts` VALUES (899, 899, 67, 'Bruyadori');
INSERT INTO `subdistricts` VALUES (900, 900, 67, 'Numfor Barat');
INSERT INTO `subdistricts` VALUES (901, 901, 67, 'Numfor Timur');
INSERT INTO `subdistricts` VALUES (902, 902, 67, 'Oridek');
INSERT INTO `subdistricts` VALUES (903, 903, 67, 'Orkeri');
INSERT INTO `subdistricts` VALUES (904, 904, 67, 'Padaido');
INSERT INTO `subdistricts` VALUES (905, 905, 67, 'Poiru');
INSERT INTO `subdistricts` VALUES (906, 906, 67, 'Samofa');
INSERT INTO `subdistricts` VALUES (907, 907, 67, 'Swandiwe');
INSERT INTO `subdistricts` VALUES (908, 908, 67, 'Warsa');
INSERT INTO `subdistricts` VALUES (909, 909, 67, 'Yawosi');
INSERT INTO `subdistricts` VALUES (910, 910, 67, 'Yendidori');
INSERT INTO `subdistricts` VALUES (911, 911, 68, 'Ambalawi');
INSERT INTO `subdistricts` VALUES (912, 912, 68, 'Belo');
INSERT INTO `subdistricts` VALUES (913, 913, 68, 'Bolo');
INSERT INTO `subdistricts` VALUES (914, 914, 68, 'Donggo');
INSERT INTO `subdistricts` VALUES (915, 915, 68, 'Lambitu');
INSERT INTO `subdistricts` VALUES (916, 916, 68, 'Lambu');
INSERT INTO `subdistricts` VALUES (917, 917, 68, 'Langgudu');
INSERT INTO `subdistricts` VALUES (918, 918, 68, 'Madapangga');
INSERT INTO `subdistricts` VALUES (919, 919, 68, 'Monta');
INSERT INTO `subdistricts` VALUES (920, 920, 68, 'Palibelo');
INSERT INTO `subdistricts` VALUES (921, 921, 68, 'Parado');
INSERT INTO `subdistricts` VALUES (922, 922, 68, 'Sanggar');
INSERT INTO `subdistricts` VALUES (923, 923, 68, 'Sape');
INSERT INTO `subdistricts` VALUES (924, 924, 68, 'Soromandi');
INSERT INTO `subdistricts` VALUES (925, 925, 68, 'Tambora');
INSERT INTO `subdistricts` VALUES (926, 926, 68, 'Wawo');
INSERT INTO `subdistricts` VALUES (927, 927, 68, 'Wera');
INSERT INTO `subdistricts` VALUES (928, 928, 68, 'Woha');
INSERT INTO `subdistricts` VALUES (929, 929, 69, 'Asakota');
INSERT INTO `subdistricts` VALUES (930, 930, 69, 'Mpunda');
INSERT INTO `subdistricts` VALUES (931, 931, 69, 'Raba');
INSERT INTO `subdistricts` VALUES (932, 932, 69, 'Rasanae Barat');
INSERT INTO `subdistricts` VALUES (933, 933, 69, 'Rasanae Timur');
INSERT INTO `subdistricts` VALUES (934, 934, 70, 'Binjai Barat');
INSERT INTO `subdistricts` VALUES (935, 935, 70, 'Binjai Kota');
INSERT INTO `subdistricts` VALUES (936, 936, 70, 'Binjai Selatan');
INSERT INTO `subdistricts` VALUES (937, 937, 70, 'Binjai Timur');
INSERT INTO `subdistricts` VALUES (938, 938, 70, 'Binjai Utara');
INSERT INTO `subdistricts` VALUES (939, 939, 71, 'Bintan Pesisir');
INSERT INTO `subdistricts` VALUES (940, 940, 71, 'Bintan Timur');
INSERT INTO `subdistricts` VALUES (941, 941, 71, 'Bintan Utara');
INSERT INTO `subdistricts` VALUES (942, 942, 71, 'Gunung Kijang');
INSERT INTO `subdistricts` VALUES (943, 943, 71, 'Mantang');
INSERT INTO `subdistricts` VALUES (944, 944, 71, 'Seri/Sri Kuala Lobam');
INSERT INTO `subdistricts` VALUES (945, 945, 71, 'Tambelan');
INSERT INTO `subdistricts` VALUES (946, 946, 71, 'Teluk Bintan');
INSERT INTO `subdistricts` VALUES (947, 947, 71, 'Teluk Sebong');
INSERT INTO `subdistricts` VALUES (948, 948, 71, 'Toapaya');
INSERT INTO `subdistricts` VALUES (949, 949, 72, 'Ganda Pura');
INSERT INTO `subdistricts` VALUES (950, 950, 72, 'Jangka');
INSERT INTO `subdistricts` VALUES (951, 951, 72, 'Jeumpa');
INSERT INTO `subdistricts` VALUES (952, 952, 72, 'Jeunieb');
INSERT INTO `subdistricts` VALUES (953, 953, 72, 'Juli');
INSERT INTO `subdistricts` VALUES (954, 954, 72, 'Kota Juang');
INSERT INTO `subdistricts` VALUES (955, 955, 72, 'Kuala');
INSERT INTO `subdistricts` VALUES (956, 956, 72, 'Kuta Blang');
INSERT INTO `subdistricts` VALUES (957, 957, 72, 'Makmur');
INSERT INTO `subdistricts` VALUES (958, 958, 72, 'Pandrah');
INSERT INTO `subdistricts` VALUES (959, 959, 72, 'Peudada');
INSERT INTO `subdistricts` VALUES (960, 960, 72, 'Peulimbang (Plimbang)');
INSERT INTO `subdistricts` VALUES (961, 961, 72, 'Peusangan');
INSERT INTO `subdistricts` VALUES (962, 962, 72, 'Peusangan Selatan');
INSERT INTO `subdistricts` VALUES (963, 963, 72, 'Peusangan Siblah Krueng');
INSERT INTO `subdistricts` VALUES (964, 964, 72, 'Samalanga');
INSERT INTO `subdistricts` VALUES (965, 965, 72, 'Simpang Mamplam');
INSERT INTO `subdistricts` VALUES (966, 966, 73, 'Aertembaga (Bitung Timur)');
INSERT INTO `subdistricts` VALUES (967, 967, 73, 'Girian');
INSERT INTO `subdistricts` VALUES (968, 968, 73, 'Lembeh Selatan (Bitung Selatan)');
INSERT INTO `subdistricts` VALUES (969, 969, 73, 'Lembeh Utara');
INSERT INTO `subdistricts` VALUES (970, 970, 73, 'Madidir (Bitung Tengah)');
INSERT INTO `subdistricts` VALUES (971, 971, 73, 'Maesa');
INSERT INTO `subdistricts` VALUES (972, 972, 73, 'Matuari (Bitung Barat)');
INSERT INTO `subdistricts` VALUES (973, 973, 73, 'Ranowulu (Bitung Utara)');
INSERT INTO `subdistricts` VALUES (974, 974, 74, 'Bakung');
INSERT INTO `subdistricts` VALUES (975, 975, 74, 'Binangun');
INSERT INTO `subdistricts` VALUES (976, 976, 74, 'Doko');
INSERT INTO `subdistricts` VALUES (977, 977, 74, 'Gandusari');
INSERT INTO `subdistricts` VALUES (978, 978, 74, 'Garum');
INSERT INTO `subdistricts` VALUES (979, 979, 74, 'Kademangan');
INSERT INTO `subdistricts` VALUES (980, 980, 74, 'Kanigoro');
INSERT INTO `subdistricts` VALUES (981, 981, 74, 'Kesamben');
INSERT INTO `subdistricts` VALUES (982, 982, 74, 'Nglegok');
INSERT INTO `subdistricts` VALUES (983, 983, 74, 'Panggungrejo');
INSERT INTO `subdistricts` VALUES (984, 984, 74, 'Ponggok');
INSERT INTO `subdistricts` VALUES (985, 985, 74, 'Sanan Kulon');
INSERT INTO `subdistricts` VALUES (986, 986, 74, 'Selopuro');
INSERT INTO `subdistricts` VALUES (987, 987, 74, 'Selorejo');
INSERT INTO `subdistricts` VALUES (988, 988, 74, 'Srengat');
INSERT INTO `subdistricts` VALUES (989, 989, 74, 'Sutojayan');
INSERT INTO `subdistricts` VALUES (990, 990, 74, 'Talun');
INSERT INTO `subdistricts` VALUES (991, 991, 74, 'Udanawu');
INSERT INTO `subdistricts` VALUES (992, 992, 74, 'Wates');
INSERT INTO `subdistricts` VALUES (993, 993, 74, 'Wlingi');
INSERT INTO `subdistricts` VALUES (994, 994, 74, 'Wonodadi');
INSERT INTO `subdistricts` VALUES (995, 995, 74, 'Wonotirto');
INSERT INTO `subdistricts` VALUES (996, 996, 75, 'Kepanjen Kidul');
INSERT INTO `subdistricts` VALUES (997, 997, 75, 'Sanan Wetan');
INSERT INTO `subdistricts` VALUES (998, 998, 75, 'Sukorejo');
INSERT INTO `subdistricts` VALUES (999, 999, 76, 'Banjarejo');
INSERT INTO `subdistricts` VALUES (1000, 1000, 76, 'Blora kota');
INSERT INTO `subdistricts` VALUES (1001, 1001, 76, 'Bogorejo');
INSERT INTO `subdistricts` VALUES (1002, 1002, 76, 'Cepu');
INSERT INTO `subdistricts` VALUES (1003, 1003, 76, 'Japah');
INSERT INTO `subdistricts` VALUES (1004, 1004, 76, 'Jati');
INSERT INTO `subdistricts` VALUES (1005, 1005, 76, 'Jepon');
INSERT INTO `subdistricts` VALUES (1006, 1006, 76, 'Jiken');
INSERT INTO `subdistricts` VALUES (1007, 1007, 76, 'Kedungtuban');
INSERT INTO `subdistricts` VALUES (1008, 1008, 76, 'Kradenan');
INSERT INTO `subdistricts` VALUES (1009, 1009, 76, 'Kunduran');
INSERT INTO `subdistricts` VALUES (1010, 1010, 76, 'Ngawen');
INSERT INTO `subdistricts` VALUES (1011, 1011, 76, 'Randublatung');
INSERT INTO `subdistricts` VALUES (1012, 1012, 76, 'Sambong');
INSERT INTO `subdistricts` VALUES (1013, 1013, 76, 'Todanan');
INSERT INTO `subdistricts` VALUES (1014, 1014, 76, 'Tunjungan');
INSERT INTO `subdistricts` VALUES (1015, 1015, 77, 'Botumoita (Botumoito)');
INSERT INTO `subdistricts` VALUES (1016, 1016, 77, 'Dulupi');
INSERT INTO `subdistricts` VALUES (1017, 1017, 77, 'Mananggu');
INSERT INTO `subdistricts` VALUES (1018, 1018, 77, 'Paguyaman');
INSERT INTO `subdistricts` VALUES (1019, 1019, 77, 'Paguyaman Pantai');
INSERT INTO `subdistricts` VALUES (1020, 1020, 77, 'Tilamuta');
INSERT INTO `subdistricts` VALUES (1021, 1021, 77, 'Wonosari');
INSERT INTO `subdistricts` VALUES (1022, 1022, 78, 'Babakan Madang');
INSERT INTO `subdistricts` VALUES (1023, 1023, 78, 'Bojonggede');
INSERT INTO `subdistricts` VALUES (1024, 1024, 78, 'Caringin');
INSERT INTO `subdistricts` VALUES (1025, 1025, 78, 'Cariu');
INSERT INTO `subdistricts` VALUES (1026, 1026, 78, 'Ciampea');
INSERT INTO `subdistricts` VALUES (1027, 1027, 78, 'Ciawi');
INSERT INTO `subdistricts` VALUES (1028, 1028, 78, 'Cibinong');
INSERT INTO `subdistricts` VALUES (1029, 1029, 78, 'Cibungbulang');
INSERT INTO `subdistricts` VALUES (1030, 1030, 78, 'Cigombong');
INSERT INTO `subdistricts` VALUES (1031, 1031, 78, 'Cigudeg');
INSERT INTO `subdistricts` VALUES (1032, 1032, 78, 'Cijeruk');
INSERT INTO `subdistricts` VALUES (1033, 1033, 78, 'Cileungsi');
INSERT INTO `subdistricts` VALUES (1034, 1034, 78, 'Ciomas');
INSERT INTO `subdistricts` VALUES (1035, 1035, 78, 'Cisarua');
INSERT INTO `subdistricts` VALUES (1036, 1036, 78, 'Ciseeng');
INSERT INTO `subdistricts` VALUES (1037, 1037, 78, 'Citeureup');
INSERT INTO `subdistricts` VALUES (1038, 1038, 78, 'Dramaga');
INSERT INTO `subdistricts` VALUES (1039, 1039, 78, 'Gunung Putri');
INSERT INTO `subdistricts` VALUES (1040, 1040, 78, 'Gunung Sindur');
INSERT INTO `subdistricts` VALUES (1041, 1041, 78, 'Jasinga');
INSERT INTO `subdistricts` VALUES (1042, 1042, 78, 'Jonggol');
INSERT INTO `subdistricts` VALUES (1043, 1043, 78, 'Kemang');
INSERT INTO `subdistricts` VALUES (1044, 1044, 78, 'Klapa Nunggal (Kelapa Nunggal)');
INSERT INTO `subdistricts` VALUES (1045, 1045, 78, 'Leuwiliang');
INSERT INTO `subdistricts` VALUES (1046, 1046, 78, 'Leuwisadeng');
INSERT INTO `subdistricts` VALUES (1047, 1047, 78, 'Megamendung');
INSERT INTO `subdistricts` VALUES (1048, 1048, 78, 'Nanggung');
INSERT INTO `subdistricts` VALUES (1049, 1049, 78, 'Pamijahan');
INSERT INTO `subdistricts` VALUES (1050, 1050, 78, 'Parung');
INSERT INTO `subdistricts` VALUES (1051, 1051, 78, 'Parung Panjang');
INSERT INTO `subdistricts` VALUES (1052, 1052, 78, 'Ranca Bungur');
INSERT INTO `subdistricts` VALUES (1053, 1053, 78, 'Rumpin');
INSERT INTO `subdistricts` VALUES (1054, 1054, 78, 'Sukajaya');
INSERT INTO `subdistricts` VALUES (1055, 1055, 78, 'Sukamakmur');
INSERT INTO `subdistricts` VALUES (1056, 1056, 78, 'Sukaraja');
INSERT INTO `subdistricts` VALUES (1057, 1057, 78, 'Tajurhalang');
INSERT INTO `subdistricts` VALUES (1058, 1058, 78, 'Tamansari');
INSERT INTO `subdistricts` VALUES (1059, 1059, 78, 'Tanjungsari');
INSERT INTO `subdistricts` VALUES (1060, 1060, 78, 'Tenjo');
INSERT INTO `subdistricts` VALUES (1061, 1061, 78, 'Tenjolaya');
INSERT INTO `subdistricts` VALUES (1062, 1062, 79, 'Bogor Barat - Kota');
INSERT INTO `subdistricts` VALUES (1063, 1063, 79, 'Bogor Selatan - Kota');
INSERT INTO `subdistricts` VALUES (1064, 1064, 79, 'Bogor Tengah - Kota');
INSERT INTO `subdistricts` VALUES (1065, 1065, 79, 'Bogor Timur - Kota');
INSERT INTO `subdistricts` VALUES (1066, 1066, 79, 'Bogor Utara - Kota');
INSERT INTO `subdistricts` VALUES (1067, 1067, 79, 'Tanah Sereal');
INSERT INTO `subdistricts` VALUES (1068, 1068, 80, 'Balen');
INSERT INTO `subdistricts` VALUES (1069, 1069, 80, 'Baureno');
INSERT INTO `subdistricts` VALUES (1070, 1070, 80, 'Bojonegoro');
INSERT INTO `subdistricts` VALUES (1071, 1071, 80, 'Bubulan');
INSERT INTO `subdistricts` VALUES (1072, 1072, 80, 'Dander');
INSERT INTO `subdistricts` VALUES (1073, 1073, 80, 'Gayam');
INSERT INTO `subdistricts` VALUES (1074, 1074, 80, 'Gondang');
INSERT INTO `subdistricts` VALUES (1075, 1075, 80, 'Kalitidu');
INSERT INTO `subdistricts` VALUES (1076, 1076, 80, 'Kanor');
INSERT INTO `subdistricts` VALUES (1077, 1077, 80, 'Kapas');
INSERT INTO `subdistricts` VALUES (1078, 1078, 80, 'Kasiman');
INSERT INTO `subdistricts` VALUES (1079, 1079, 80, 'Kedewan');
INSERT INTO `subdistricts` VALUES (1080, 1080, 80, 'Kedungadem');
INSERT INTO `subdistricts` VALUES (1081, 1081, 80, 'Kepoh Baru');
INSERT INTO `subdistricts` VALUES (1082, 1082, 80, 'Malo');
INSERT INTO `subdistricts` VALUES (1083, 1083, 80, 'Margomulyo');
INSERT INTO `subdistricts` VALUES (1084, 1084, 80, 'Ngambon');
INSERT INTO `subdistricts` VALUES (1085, 1085, 80, 'Ngasem');
INSERT INTO `subdistricts` VALUES (1086, 1086, 80, 'Ngraho');
INSERT INTO `subdistricts` VALUES (1087, 1087, 80, 'Padangan');
INSERT INTO `subdistricts` VALUES (1088, 1088, 80, 'Purwosari');
INSERT INTO `subdistricts` VALUES (1089, 1089, 80, 'Sekar');
INSERT INTO `subdistricts` VALUES (1090, 1090, 80, 'Sugihwaras');
INSERT INTO `subdistricts` VALUES (1091, 1091, 80, 'Sukosewu');
INSERT INTO `subdistricts` VALUES (1092, 1092, 80, 'Sumberrejo');
INSERT INTO `subdistricts` VALUES (1093, 1093, 80, 'Tambakrejo');
INSERT INTO `subdistricts` VALUES (1094, 1094, 80, 'Temayang');
INSERT INTO `subdistricts` VALUES (1095, 1095, 80, 'Trucuk');
INSERT INTO `subdistricts` VALUES (1096, 1096, 81, 'Bilalang');
INSERT INTO `subdistricts` VALUES (1097, 1097, 81, 'Bolaang');
INSERT INTO `subdistricts` VALUES (1098, 1098, 81, 'Bolaang Timur');
INSERT INTO `subdistricts` VALUES (1099, 1099, 81, 'Dumoga');
INSERT INTO `subdistricts` VALUES (1100, 1100, 81, 'Dumoga Barat');
INSERT INTO `subdistricts` VALUES (1101, 1101, 81, 'Dumoga Tengah');
INSERT INTO `subdistricts` VALUES (1102, 1102, 81, 'Dumoga Tenggara');
INSERT INTO `subdistricts` VALUES (1103, 1103, 81, 'Dumoga Timur');
INSERT INTO `subdistricts` VALUES (1104, 1104, 81, 'Dumoga Utara');
INSERT INTO `subdistricts` VALUES (1105, 1105, 81, 'Lolak');
INSERT INTO `subdistricts` VALUES (1106, 1106, 81, 'Lolayan');
INSERT INTO `subdistricts` VALUES (1107, 1107, 81, 'Passi Barat');
INSERT INTO `subdistricts` VALUES (1108, 1108, 81, 'Passi Timur');
INSERT INTO `subdistricts` VALUES (1109, 1109, 81, 'Poigar');
INSERT INTO `subdistricts` VALUES (1110, 1110, 81, 'Sangtombolang');
INSERT INTO `subdistricts` VALUES (1111, 1111, 82, 'Bolaang Uki');
INSERT INTO `subdistricts` VALUES (1112, 1112, 82, 'Pinolosian');
INSERT INTO `subdistricts` VALUES (1113, 1113, 82, 'Pinolosian Tengah');
INSERT INTO `subdistricts` VALUES (1114, 1114, 82, 'Pinolosian Timur');
INSERT INTO `subdistricts` VALUES (1115, 1115, 82, 'Posigadan');
INSERT INTO `subdistricts` VALUES (1116, 1116, 83, 'Kotabunan');
INSERT INTO `subdistricts` VALUES (1117, 1117, 83, 'Modayag');
INSERT INTO `subdistricts` VALUES (1118, 1118, 83, 'Modayag Barat');
INSERT INTO `subdistricts` VALUES (1119, 1119, 83, 'Nuangan');
INSERT INTO `subdistricts` VALUES (1120, 1120, 83, 'Tutuyan');
INSERT INTO `subdistricts` VALUES (1121, 1121, 84, 'Bintauna');
INSERT INTO `subdistricts` VALUES (1122, 1122, 84, 'Bolang Itang Barat');
INSERT INTO `subdistricts` VALUES (1123, 1123, 84, 'Bolang Itang Timur');
INSERT INTO `subdistricts` VALUES (1124, 1124, 84, 'Kaidipang');
INSERT INTO `subdistricts` VALUES (1125, 1125, 84, 'Pinogaluman');
INSERT INTO `subdistricts` VALUES (1126, 1126, 84, 'Sangkub');
INSERT INTO `subdistricts` VALUES (1127, 1127, 85, 'Kabaena');
INSERT INTO `subdistricts` VALUES (1128, 1128, 85, 'Kabaena Barat');
INSERT INTO `subdistricts` VALUES (1129, 1129, 85, 'Kabaena Selatan');
INSERT INTO `subdistricts` VALUES (1130, 1130, 85, 'Kabaena Tengah');
INSERT INTO `subdistricts` VALUES (1131, 1131, 85, 'Kabaena Timur');
INSERT INTO `subdistricts` VALUES (1132, 1132, 85, 'Kabaena Utara');
INSERT INTO `subdistricts` VALUES (1133, 1133, 85, 'Kepulauan Masaloka Raya');
INSERT INTO `subdistricts` VALUES (1134, 1134, 85, 'Lentarai Jaya S. (Lantari Jaya)');
INSERT INTO `subdistricts` VALUES (1135, 1135, 85, 'Mata Oleo');
INSERT INTO `subdistricts` VALUES (1136, 1136, 85, 'Mata Usu');
INSERT INTO `subdistricts` VALUES (1137, 1137, 85, 'Poleang');
INSERT INTO `subdistricts` VALUES (1138, 1138, 85, 'Poleang Barat');
INSERT INTO `subdistricts` VALUES (1139, 1139, 85, 'Poleang Selatan');
INSERT INTO `subdistricts` VALUES (1140, 1140, 85, 'Poleang Tengah');
INSERT INTO `subdistricts` VALUES (1141, 1141, 85, 'Poleang Tenggara');
INSERT INTO `subdistricts` VALUES (1142, 1142, 85, 'Poleang Timur');
INSERT INTO `subdistricts` VALUES (1143, 1143, 85, 'Poleang Utara');
INSERT INTO `subdistricts` VALUES (1144, 1144, 85, 'Rarowatu');
INSERT INTO `subdistricts` VALUES (1145, 1145, 85, 'Rarowatu Utara');
INSERT INTO `subdistricts` VALUES (1146, 1146, 85, 'Rumbia');
INSERT INTO `subdistricts` VALUES (1147, 1147, 85, 'Rumbia Tengah');
INSERT INTO `subdistricts` VALUES (1148, 1148, 85, 'Tontonunu (Tontonuwu)');
INSERT INTO `subdistricts` VALUES (1149, 1149, 86, 'Binakal');
INSERT INTO `subdistricts` VALUES (1150, 1150, 86, 'Bondowoso');
INSERT INTO `subdistricts` VALUES (1151, 1151, 86, 'Botolinggo');
INSERT INTO `subdistricts` VALUES (1152, 1152, 86, 'Cermee');
INSERT INTO `subdistricts` VALUES (1153, 1153, 86, 'Curahdami');
INSERT INTO `subdistricts` VALUES (1154, 1154, 86, 'Grujugan');
INSERT INTO `subdistricts` VALUES (1155, 1155, 86, 'Jambe Sari Darus Sholah');
INSERT INTO `subdistricts` VALUES (1156, 1156, 86, 'Klabang');
INSERT INTO `subdistricts` VALUES (1157, 1157, 86, 'Maesan');
INSERT INTO `subdistricts` VALUES (1158, 1158, 86, 'Pakem');
INSERT INTO `subdistricts` VALUES (1159, 1159, 86, 'Prajekan');
INSERT INTO `subdistricts` VALUES (1160, 1160, 86, 'Pujer');
INSERT INTO `subdistricts` VALUES (1161, 1161, 86, 'Sempol');
INSERT INTO `subdistricts` VALUES (1162, 1162, 86, 'Sukosari');
INSERT INTO `subdistricts` VALUES (1163, 1163, 86, 'Sumber Wringin');
INSERT INTO `subdistricts` VALUES (1164, 1164, 86, 'Taman Krocok');
INSERT INTO `subdistricts` VALUES (1165, 1165, 86, 'Tamanan');
INSERT INTO `subdistricts` VALUES (1166, 1166, 86, 'Tapen');
INSERT INTO `subdistricts` VALUES (1167, 1167, 86, 'Tegalampel');
INSERT INTO `subdistricts` VALUES (1168, 1168, 86, 'Tenggarang');
INSERT INTO `subdistricts` VALUES (1169, 1169, 86, 'Tlogosari');
INSERT INTO `subdistricts` VALUES (1170, 1170, 86, 'Wonosari');
INSERT INTO `subdistricts` VALUES (1171, 1171, 86, 'Wringin');
INSERT INTO `subdistricts` VALUES (1172, 1172, 87, 'Ajangale');
INSERT INTO `subdistricts` VALUES (1173, 1173, 87, 'Amali');
INSERT INTO `subdistricts` VALUES (1174, 1174, 87, 'Awangpone');
INSERT INTO `subdistricts` VALUES (1175, 1175, 87, 'Barebbo');
INSERT INTO `subdistricts` VALUES (1176, 1176, 87, 'Bengo');
INSERT INTO `subdistricts` VALUES (1177, 1177, 87, 'Bontocani');
INSERT INTO `subdistricts` VALUES (1178, 1178, 87, 'Cenrana');
INSERT INTO `subdistricts` VALUES (1179, 1179, 87, 'Cina');
INSERT INTO `subdistricts` VALUES (1180, 1180, 87, 'Dua Boccoe');
INSERT INTO `subdistricts` VALUES (1181, 1181, 87, 'Kahu');
INSERT INTO `subdistricts` VALUES (1182, 1182, 87, 'Kajuara');
INSERT INTO `subdistricts` VALUES (1183, 1183, 87, 'Lamuru');
INSERT INTO `subdistricts` VALUES (1184, 1184, 87, 'Lappariaja');
INSERT INTO `subdistricts` VALUES (1185, 1185, 87, 'Libureng');
INSERT INTO `subdistricts` VALUES (1186, 1186, 87, 'Mare');
INSERT INTO `subdistricts` VALUES (1187, 1187, 87, 'Palakka');
INSERT INTO `subdistricts` VALUES (1188, 1188, 87, 'Patimpeng');
INSERT INTO `subdistricts` VALUES (1189, 1189, 87, 'Ponre');
INSERT INTO `subdistricts` VALUES (1190, 1190, 87, 'Salomekko');
INSERT INTO `subdistricts` VALUES (1191, 1191, 87, 'Sibulue');
INSERT INTO `subdistricts` VALUES (1192, 1192, 87, 'Tanete Riattang');
INSERT INTO `subdistricts` VALUES (1193, 1193, 87, 'Tanete Riattang Barat');
INSERT INTO `subdistricts` VALUES (1194, 1194, 87, 'Tanete Riattang Timur');
INSERT INTO `subdistricts` VALUES (1195, 1195, 87, 'Tellu Limpoe');
INSERT INTO `subdistricts` VALUES (1196, 1196, 87, 'Tellu Siattinge');
INSERT INTO `subdistricts` VALUES (1197, 1197, 87, 'Tonra');
INSERT INTO `subdistricts` VALUES (1198, 1198, 87, 'Ulaweng');
INSERT INTO `subdistricts` VALUES (1199, 1199, 88, 'Bone');
INSERT INTO `subdistricts` VALUES (1200, 1200, 88, 'Bone Raya');
INSERT INTO `subdistricts` VALUES (1201, 1201, 88, 'Bonepantai');
INSERT INTO `subdistricts` VALUES (1202, 1202, 88, 'Botu Pingge');
INSERT INTO `subdistricts` VALUES (1203, 1203, 88, 'Bulango Selatan');
INSERT INTO `subdistricts` VALUES (1204, 1204, 88, 'Bulango Timur');
INSERT INTO `subdistricts` VALUES (1205, 1205, 88, 'Bulango Ulu');
INSERT INTO `subdistricts` VALUES (1206, 1206, 88, 'Bulango Utara');
INSERT INTO `subdistricts` VALUES (1207, 1207, 88, 'Bulawa');
INSERT INTO `subdistricts` VALUES (1208, 1208, 88, 'Kabila');
INSERT INTO `subdistricts` VALUES (1209, 1209, 88, 'Kabila Bone');
INSERT INTO `subdistricts` VALUES (1210, 1210, 88, 'Pinogu');
INSERT INTO `subdistricts` VALUES (1211, 1211, 88, 'Suwawa');
INSERT INTO `subdistricts` VALUES (1212, 1212, 88, 'Suwawa Selatan');
INSERT INTO `subdistricts` VALUES (1213, 1213, 88, 'Suwawa Tengah');
INSERT INTO `subdistricts` VALUES (1214, 1214, 88, 'Suwawa Timur');
INSERT INTO `subdistricts` VALUES (1215, 1215, 88, 'Tapa');
INSERT INTO `subdistricts` VALUES (1216, 1216, 88, 'Tilongkabila');
INSERT INTO `subdistricts` VALUES (1217, 1217, 89, 'Bontang Barat');
INSERT INTO `subdistricts` VALUES (1218, 1218, 89, 'Bontang Selatan');
INSERT INTO `subdistricts` VALUES (1219, 1219, 89, 'Bontang Utara');
INSERT INTO `subdistricts` VALUES (1220, 1220, 90, 'Ambatkwi (Ambatkui)');
INSERT INTO `subdistricts` VALUES (1221, 1221, 90, 'Arimop');
INSERT INTO `subdistricts` VALUES (1222, 1222, 90, 'Bomakia');
INSERT INTO `subdistricts` VALUES (1223, 1223, 90, 'Firiwage');
INSERT INTO `subdistricts` VALUES (1224, 1224, 90, 'Fofi');
INSERT INTO `subdistricts` VALUES (1225, 1225, 90, 'Iniyandit');
INSERT INTO `subdistricts` VALUES (1226, 1226, 90, 'Jair');
INSERT INTO `subdistricts` VALUES (1227, 1227, 90, 'Kawagit');
INSERT INTO `subdistricts` VALUES (1228, 1228, 90, 'Ki');
INSERT INTO `subdistricts` VALUES (1229, 1229, 90, 'Kombay');
INSERT INTO `subdistricts` VALUES (1230, 1230, 90, 'Kombut');
INSERT INTO `subdistricts` VALUES (1231, 1231, 90, 'Kouh');
INSERT INTO `subdistricts` VALUES (1232, 1232, 90, 'Mandobo');
INSERT INTO `subdistricts` VALUES (1233, 1233, 90, 'Manggelum');
INSERT INTO `subdistricts` VALUES (1234, 1234, 90, 'Mindiptana');
INSERT INTO `subdistricts` VALUES (1235, 1235, 90, 'Ninati');
INSERT INTO `subdistricts` VALUES (1236, 1236, 90, 'Sesnuk');
INSERT INTO `subdistricts` VALUES (1237, 1237, 90, 'Subur');
INSERT INTO `subdistricts` VALUES (1238, 1238, 90, 'Waropko');
INSERT INTO `subdistricts` VALUES (1239, 1239, 90, 'Yaniruma');
INSERT INTO `subdistricts` VALUES (1240, 1240, 91, 'Ampel');
INSERT INTO `subdistricts` VALUES (1241, 1241, 91, 'Andong');
INSERT INTO `subdistricts` VALUES (1242, 1242, 91, 'Banyudono');
INSERT INTO `subdistricts` VALUES (1243, 1243, 91, 'Boyolali');
INSERT INTO `subdistricts` VALUES (1244, 1244, 91, 'Cepogo');
INSERT INTO `subdistricts` VALUES (1245, 1245, 91, 'Juwangi');
INSERT INTO `subdistricts` VALUES (1246, 1246, 91, 'Karanggede');
INSERT INTO `subdistricts` VALUES (1247, 1247, 91, 'Kemusu');
INSERT INTO `subdistricts` VALUES (1248, 1248, 91, 'Klego');
INSERT INTO `subdistricts` VALUES (1249, 1249, 91, 'Mojosongo');
INSERT INTO `subdistricts` VALUES (1250, 1250, 91, 'Musuk');
INSERT INTO `subdistricts` VALUES (1251, 1251, 91, 'Ngemplak');
INSERT INTO `subdistricts` VALUES (1252, 1252, 91, 'Nogosari');
INSERT INTO `subdistricts` VALUES (1253, 1253, 91, 'Sambi');
INSERT INTO `subdistricts` VALUES (1254, 1254, 91, 'Sawit');
INSERT INTO `subdistricts` VALUES (1255, 1255, 91, 'Selo');
INSERT INTO `subdistricts` VALUES (1256, 1256, 91, 'Simo');
INSERT INTO `subdistricts` VALUES (1257, 1257, 91, 'Teras');
INSERT INTO `subdistricts` VALUES (1258, 1258, 91, 'Wonosegoro');
INSERT INTO `subdistricts` VALUES (1259, 1259, 92, 'Banjarharjo');
INSERT INTO `subdistricts` VALUES (1260, 1260, 92, 'Bantarkawung');
INSERT INTO `subdistricts` VALUES (1261, 1261, 92, 'Brebes');
INSERT INTO `subdistricts` VALUES (1262, 1262, 92, 'Bulakamba');
INSERT INTO `subdistricts` VALUES (1263, 1263, 92, 'Bumiayu');
INSERT INTO `subdistricts` VALUES (1264, 1264, 92, 'Jatibarang');
INSERT INTO `subdistricts` VALUES (1265, 1265, 92, 'Kersana');
INSERT INTO `subdistricts` VALUES (1266, 1266, 92, 'Ketanggungan');
INSERT INTO `subdistricts` VALUES (1267, 1267, 92, 'Larangan');
INSERT INTO `subdistricts` VALUES (1268, 1268, 92, 'Losari');
INSERT INTO `subdistricts` VALUES (1269, 1269, 92, 'Paguyangan');
INSERT INTO `subdistricts` VALUES (1270, 1270, 92, 'Salem');
INSERT INTO `subdistricts` VALUES (1271, 1271, 92, 'Sirampog');
INSERT INTO `subdistricts` VALUES (1272, 1272, 92, 'Songgom');
INSERT INTO `subdistricts` VALUES (1273, 1273, 92, 'Tanjung');
INSERT INTO `subdistricts` VALUES (1274, 1274, 92, 'Tonjong');
INSERT INTO `subdistricts` VALUES (1275, 1275, 92, 'Wanasari');
INSERT INTO `subdistricts` VALUES (1276, 1276, 93, 'Aur Birugo Tigo Baleh');
INSERT INTO `subdistricts` VALUES (1277, 1277, 93, 'Guguk Panjang (Guguak Panjang)');
INSERT INTO `subdistricts` VALUES (1278, 1278, 93, 'Mandiangin Koto Selayan');
INSERT INTO `subdistricts` VALUES (1279, 1279, 94, 'Banjar');
INSERT INTO `subdistricts` VALUES (1280, 1280, 94, 'Buleleng');
INSERT INTO `subdistricts` VALUES (1281, 1281, 94, 'Busungbiu');
INSERT INTO `subdistricts` VALUES (1282, 1282, 94, 'Gerokgak');
INSERT INTO `subdistricts` VALUES (1283, 1283, 94, 'Kubutambahan');
INSERT INTO `subdistricts` VALUES (1284, 1284, 94, 'Sawan');
INSERT INTO `subdistricts` VALUES (1285, 1285, 94, 'Seririt');
INSERT INTO `subdistricts` VALUES (1286, 1286, 94, 'Sukasada');
INSERT INTO `subdistricts` VALUES (1287, 1287, 94, 'Tejakula');
INSERT INTO `subdistricts` VALUES (1288, 1288, 95, 'Bonto Bahari');
INSERT INTO `subdistricts` VALUES (1289, 1289, 95, 'Bontotiro');
INSERT INTO `subdistricts` VALUES (1290, 1290, 95, 'Bulukumba (Bulukumpa)');
INSERT INTO `subdistricts` VALUES (1291, 1291, 95, 'Gantorang/Gantarang (Gangking)');
INSERT INTO `subdistricts` VALUES (1292, 1292, 95, 'Hero Lange-Lange (Herlang)');
INSERT INTO `subdistricts` VALUES (1293, 1293, 95, 'Kajang');
INSERT INTO `subdistricts` VALUES (1294, 1294, 95, 'Kindang');
INSERT INTO `subdistricts` VALUES (1295, 1295, 95, 'Rilau Ale');
INSERT INTO `subdistricts` VALUES (1296, 1296, 95, 'Ujung Bulu');
INSERT INTO `subdistricts` VALUES (1297, 1297, 95, 'Ujung Loe');
INSERT INTO `subdistricts` VALUES (1298, 1298, 96, 'Peso');
INSERT INTO `subdistricts` VALUES (1299, 1299, 96, 'Peso Hilir/Ilir');
INSERT INTO `subdistricts` VALUES (1300, 1300, 96, 'Pulau Bunyu');
INSERT INTO `subdistricts` VALUES (1301, 1301, 96, 'Sekatak');
INSERT INTO `subdistricts` VALUES (1302, 1302, 96, 'Tanjung Palas');
INSERT INTO `subdistricts` VALUES (1303, 1303, 96, 'Tanjung Palas Barat');
INSERT INTO `subdistricts` VALUES (1304, 1304, 96, 'Tanjung Palas Tengah');
INSERT INTO `subdistricts` VALUES (1305, 1305, 96, 'Tanjung Palas Timur');
INSERT INTO `subdistricts` VALUES (1306, 1306, 96, 'Tanjung Palas Utara');
INSERT INTO `subdistricts` VALUES (1307, 1307, 96, 'Tanjung Selor');
INSERT INTO `subdistricts` VALUES (1308, 1308, 97, 'Bathin II Babeko');
INSERT INTO `subdistricts` VALUES (1309, 1309, 97, 'Bathin II Pelayang');
INSERT INTO `subdistricts` VALUES (1310, 1310, 97, 'Bathin III');
INSERT INTO `subdistricts` VALUES (1311, 1311, 97, 'Bathin III Ulu');
INSERT INTO `subdistricts` VALUES (1312, 1312, 97, 'Bungo Dani');
INSERT INTO `subdistricts` VALUES (1313, 1313, 97, 'Jujuhan');
INSERT INTO `subdistricts` VALUES (1314, 1314, 97, 'Jujuhan Ilir');
INSERT INTO `subdistricts` VALUES (1315, 1315, 97, 'Limbur Lubuk Mengkuang');
INSERT INTO `subdistricts` VALUES (1316, 1316, 97, 'Muko-Muko Batin VII');
INSERT INTO `subdistricts` VALUES (1317, 1317, 97, 'Pasar Muara Bungo');
INSERT INTO `subdistricts` VALUES (1318, 1318, 97, 'Pelepat');
INSERT INTO `subdistricts` VALUES (1319, 1319, 97, 'Pelepat Ilir');
INSERT INTO `subdistricts` VALUES (1320, 1320, 97, 'Rantau Pandan');
INSERT INTO `subdistricts` VALUES (1321, 1321, 97, 'Rimbo Tengah');
INSERT INTO `subdistricts` VALUES (1322, 1322, 97, 'Tanah Sepenggal');
INSERT INTO `subdistricts` VALUES (1323, 1323, 97, 'Tanah Sepenggal Lintas');
INSERT INTO `subdistricts` VALUES (1324, 1324, 97, 'Tanah Tumbuh');
INSERT INTO `subdistricts` VALUES (1325, 1325, 98, 'Biau');
INSERT INTO `subdistricts` VALUES (1326, 1326, 98, 'Bokat');
INSERT INTO `subdistricts` VALUES (1327, 1327, 98, 'Bukal');
INSERT INTO `subdistricts` VALUES (1328, 1328, 98, 'Bunobogu');
INSERT INTO `subdistricts` VALUES (1329, 1329, 98, 'Gadung');
INSERT INTO `subdistricts` VALUES (1330, 1330, 98, 'Karamat');
INSERT INTO `subdistricts` VALUES (1331, 1331, 98, 'Lakea (Lipunoto)');
INSERT INTO `subdistricts` VALUES (1332, 1332, 98, 'Momunu');
INSERT INTO `subdistricts` VALUES (1333, 1333, 98, 'Paleleh');
INSERT INTO `subdistricts` VALUES (1334, 1334, 98, 'Paleleh Barat');
INSERT INTO `subdistricts` VALUES (1335, 1335, 98, 'Tiloan');
INSERT INTO `subdistricts` VALUES (1336, 1336, 99, 'Airbuaya');
INSERT INTO `subdistricts` VALUES (1337, 1337, 99, 'Batabual');
INSERT INTO `subdistricts` VALUES (1338, 1338, 99, 'Fena Leisela');
INSERT INTO `subdistricts` VALUES (1339, 1339, 99, 'Lilialy');
INSERT INTO `subdistricts` VALUES (1340, 1340, 99, 'Lolong Guba');
INSERT INTO `subdistricts` VALUES (1341, 1341, 99, 'Namlea');
INSERT INTO `subdistricts` VALUES (1342, 1342, 99, 'Teluk Kaiely');
INSERT INTO `subdistricts` VALUES (1343, 1343, 99, 'Waeapo');
INSERT INTO `subdistricts` VALUES (1344, 1344, 99, 'Waelata');
INSERT INTO `subdistricts` VALUES (1345, 1345, 99, 'Waplau');
INSERT INTO `subdistricts` VALUES (1346, 1346, 100, 'Ambalau');
INSERT INTO `subdistricts` VALUES (1347, 1347, 100, 'Fena Fafan');
INSERT INTO `subdistricts` VALUES (1348, 1348, 100, 'Kepala Madan');
INSERT INTO `subdistricts` VALUES (1349, 1349, 100, 'Leksula');
INSERT INTO `subdistricts` VALUES (1350, 1350, 100, 'Namrole');
INSERT INTO `subdistricts` VALUES (1351, 1351, 100, 'Waesama');
INSERT INTO `subdistricts` VALUES (1352, 1352, 101, 'Batauga');
INSERT INTO `subdistricts` VALUES (1353, 1353, 101, 'Batu Atas');
INSERT INTO `subdistricts` VALUES (1354, 1354, 101, 'Gu');
INSERT INTO `subdistricts` VALUES (1355, 1355, 101, 'Kadatua');
INSERT INTO `subdistricts` VALUES (1356, 1356, 101, 'Kapontori');
INSERT INTO `subdistricts` VALUES (1357, 1357, 101, 'Lakudo');
INSERT INTO `subdistricts` VALUES (1358, 1358, 101, 'Lapandewa');
INSERT INTO `subdistricts` VALUES (1359, 1359, 101, 'Lasalimu');
INSERT INTO `subdistricts` VALUES (1360, 1360, 101, 'Lasalimu Selatan');
INSERT INTO `subdistricts` VALUES (1361, 1361, 101, 'Mawasangka');
INSERT INTO `subdistricts` VALUES (1362, 1362, 101, 'Mawasangka Tengah');
INSERT INTO `subdistricts` VALUES (1363, 1363, 101, 'Mawasangka Timur');
INSERT INTO `subdistricts` VALUES (1364, 1364, 101, 'Pasar Wajo');
INSERT INTO `subdistricts` VALUES (1365, 1365, 101, 'Sampolawa');
INSERT INTO `subdistricts` VALUES (1366, 1366, 101, 'Sangia Mambulu');
INSERT INTO `subdistricts` VALUES (1367, 1367, 101, 'Siompu');
INSERT INTO `subdistricts` VALUES (1368, 1368, 101, 'Siompu Barat');
INSERT INTO `subdistricts` VALUES (1369, 1369, 101, 'Siontapia (Siontapina)');
INSERT INTO `subdistricts` VALUES (1370, 1370, 101, 'Talaga Raya (Telaga Raya)');
INSERT INTO `subdistricts` VALUES (1371, 1371, 101, 'Wabula');
INSERT INTO `subdistricts` VALUES (1372, 1372, 101, 'Wolowa');
INSERT INTO `subdistricts` VALUES (1373, 1373, 102, 'Bonegunu');
INSERT INTO `subdistricts` VALUES (1374, 1374, 102, 'Kambowa');
INSERT INTO `subdistricts` VALUES (1375, 1375, 102, 'Kulisusu (Kalingsusu/Kalisusu)');
INSERT INTO `subdistricts` VALUES (1376, 1376, 102, 'Kulisusu Barat');
INSERT INTO `subdistricts` VALUES (1377, 1377, 102, 'Kulisusu Utara');
INSERT INTO `subdistricts` VALUES (1378, 1378, 102, 'Wakorumba Utara');
INSERT INTO `subdistricts` VALUES (1379, 1379, 103, 'Banjarsari');
INSERT INTO `subdistricts` VALUES (1380, 1380, 103, 'Baregbeg');
INSERT INTO `subdistricts` VALUES (1381, 1381, 103, 'Ciamis');
INSERT INTO `subdistricts` VALUES (1382, 1382, 103, 'Cidolog');
INSERT INTO `subdistricts` VALUES (1383, 1383, 103, 'Cihaurbeuti');
INSERT INTO `subdistricts` VALUES (1384, 1384, 103, 'Cijeungjing');
INSERT INTO `subdistricts` VALUES (1385, 1385, 103, 'Cikoneng');
INSERT INTO `subdistricts` VALUES (1386, 1386, 103, 'Cimaragas');
INSERT INTO `subdistricts` VALUES (1387, 1387, 103, 'Cipaku');
INSERT INTO `subdistricts` VALUES (1388, 1388, 103, 'Cisaga');
INSERT INTO `subdistricts` VALUES (1389, 1389, 103, 'Jatinagara');
INSERT INTO `subdistricts` VALUES (1390, 1390, 103, 'Kawali');
INSERT INTO `subdistricts` VALUES (1391, 1391, 103, 'Lakbok');
INSERT INTO `subdistricts` VALUES (1392, 1392, 103, 'Lumbung');
INSERT INTO `subdistricts` VALUES (1393, 1393, 103, 'Pamarican');
INSERT INTO `subdistricts` VALUES (1394, 1394, 103, 'Panawangan');
INSERT INTO `subdistricts` VALUES (1395, 1395, 103, 'Panjalu');
INSERT INTO `subdistricts` VALUES (1396, 1396, 103, 'Panumbangan');
INSERT INTO `subdistricts` VALUES (1397, 1397, 103, 'Purwadadi');
INSERT INTO `subdistricts` VALUES (1398, 1398, 103, 'Rajadesa');
INSERT INTO `subdistricts` VALUES (1399, 1399, 103, 'Rancah');
INSERT INTO `subdistricts` VALUES (1400, 1400, 103, 'Sadananya');
INSERT INTO `subdistricts` VALUES (1401, 1401, 103, 'Sindangkasih');
INSERT INTO `subdistricts` VALUES (1402, 1402, 103, 'Sukadana');
INSERT INTO `subdistricts` VALUES (1403, 1403, 103, 'Sukamantri');
INSERT INTO `subdistricts` VALUES (1404, 1404, 103, 'Tambaksari');
INSERT INTO `subdistricts` VALUES (1405, 1405, 104, 'Agrabinta');
INSERT INTO `subdistricts` VALUES (1406, 1406, 104, 'Bojongpicung');
INSERT INTO `subdistricts` VALUES (1407, 1407, 104, 'Campaka');
INSERT INTO `subdistricts` VALUES (1408, 1408, 104, 'Campaka Mulya');
INSERT INTO `subdistricts` VALUES (1409, 1409, 104, 'Cianjur');
INSERT INTO `subdistricts` VALUES (1410, 1410, 104, 'Cibeber');
INSERT INTO `subdistricts` VALUES (1411, 1411, 104, 'Cibinong');
INSERT INTO `subdistricts` VALUES (1412, 1412, 104, 'Cidaun');
INSERT INTO `subdistricts` VALUES (1413, 1413, 104, 'Cijati');
INSERT INTO `subdistricts` VALUES (1414, 1414, 104, 'Cikadu');
INSERT INTO `subdistricts` VALUES (1415, 1415, 104, 'Cikalongkulon');
INSERT INTO `subdistricts` VALUES (1416, 1416, 104, 'Cilaku');
INSERT INTO `subdistricts` VALUES (1417, 1417, 104, 'Cipanas');
INSERT INTO `subdistricts` VALUES (1418, 1418, 104, 'Ciranjang');
INSERT INTO `subdistricts` VALUES (1419, 1419, 104, 'Cugenang');
INSERT INTO `subdistricts` VALUES (1420, 1420, 104, 'Gekbrong');
INSERT INTO `subdistricts` VALUES (1421, 1421, 104, 'Haurwangi');
INSERT INTO `subdistricts` VALUES (1422, 1422, 104, 'Kadupandak');
INSERT INTO `subdistricts` VALUES (1423, 1423, 104, 'Karangtengah');
INSERT INTO `subdistricts` VALUES (1424, 1424, 104, 'Leles');
INSERT INTO `subdistricts` VALUES (1425, 1425, 104, 'Mande');
INSERT INTO `subdistricts` VALUES (1426, 1426, 104, 'Naringgul');
INSERT INTO `subdistricts` VALUES (1427, 1427, 104, 'Pacet');
INSERT INTO `subdistricts` VALUES (1428, 1428, 104, 'Pagelaran');
INSERT INTO `subdistricts` VALUES (1429, 1429, 104, 'Pasirkuda');
INSERT INTO `subdistricts` VALUES (1430, 1430, 104, 'Sindangbarang');
INSERT INTO `subdistricts` VALUES (1431, 1431, 104, 'Sukaluyu');
INSERT INTO `subdistricts` VALUES (1432, 1432, 104, 'Sukanagara');
INSERT INTO `subdistricts` VALUES (1433, 1433, 104, 'Sukaresmi');
INSERT INTO `subdistricts` VALUES (1434, 1434, 104, 'Takokak');
INSERT INTO `subdistricts` VALUES (1435, 1435, 104, 'Tanggeung');
INSERT INTO `subdistricts` VALUES (1436, 1436, 104, 'Warungkondang');
INSERT INTO `subdistricts` VALUES (1437, 1437, 105, 'Adipala');
INSERT INTO `subdistricts` VALUES (1438, 1438, 105, 'Bantarsari');
INSERT INTO `subdistricts` VALUES (1439, 1439, 105, 'Binangun');
INSERT INTO `subdistricts` VALUES (1440, 1440, 105, 'Cilacap Selatan');
INSERT INTO `subdistricts` VALUES (1441, 1441, 105, 'Cilacap Tengah');
INSERT INTO `subdistricts` VALUES (1442, 1442, 105, 'Cilacap Utara');
INSERT INTO `subdistricts` VALUES (1443, 1443, 105, 'Cimanggu');
INSERT INTO `subdistricts` VALUES (1444, 1444, 105, 'Cipari');
INSERT INTO `subdistricts` VALUES (1445, 1445, 105, 'Dayeuhluhur');
INSERT INTO `subdistricts` VALUES (1446, 1446, 105, 'Gandrungmangu');
INSERT INTO `subdistricts` VALUES (1447, 1447, 105, 'Jeruklegi');
INSERT INTO `subdistricts` VALUES (1448, 1448, 105, 'Kampung Laut');
INSERT INTO `subdistricts` VALUES (1449, 1449, 105, 'Karangpucung');
INSERT INTO `subdistricts` VALUES (1450, 1450, 105, 'Kawunganten');
INSERT INTO `subdistricts` VALUES (1451, 1451, 105, 'Kedungreja');
INSERT INTO `subdistricts` VALUES (1452, 1452, 105, 'Kesugihan');
INSERT INTO `subdistricts` VALUES (1453, 1453, 105, 'Kroya');
INSERT INTO `subdistricts` VALUES (1454, 1454, 105, 'Majenang');
INSERT INTO `subdistricts` VALUES (1455, 1455, 105, 'Maos');
INSERT INTO `subdistricts` VALUES (1456, 1456, 105, 'Nusawungu');
INSERT INTO `subdistricts` VALUES (1457, 1457, 105, 'Patimuan');
INSERT INTO `subdistricts` VALUES (1458, 1458, 105, 'Sampang');
INSERT INTO `subdistricts` VALUES (1459, 1459, 105, 'Sidareja');
INSERT INTO `subdistricts` VALUES (1460, 1460, 105, 'Wanareja');
INSERT INTO `subdistricts` VALUES (1461, 1461, 106, 'Cibeber');
INSERT INTO `subdistricts` VALUES (1462, 1462, 106, 'Cilegon');
INSERT INTO `subdistricts` VALUES (1463, 1463, 106, 'Citangkil');
INSERT INTO `subdistricts` VALUES (1464, 1464, 106, 'Ciwandan');
INSERT INTO `subdistricts` VALUES (1465, 1465, 106, 'Gerogol');
INSERT INTO `subdistricts` VALUES (1466, 1466, 106, 'Jombang');
INSERT INTO `subdistricts` VALUES (1467, 1467, 106, 'Pulomerak');
INSERT INTO `subdistricts` VALUES (1468, 1468, 106, 'Purwakarta');
INSERT INTO `subdistricts` VALUES (1469, 1469, 107, 'Cimahi Selatan');
INSERT INTO `subdistricts` VALUES (1470, 1470, 107, 'Cimahi Tengah');
INSERT INTO `subdistricts` VALUES (1471, 1471, 107, 'Cimahi Utara');
INSERT INTO `subdistricts` VALUES (1472, 1472, 108, 'Arjawinangun');
INSERT INTO `subdistricts` VALUES (1473, 1473, 108, 'Astanajapura');
INSERT INTO `subdistricts` VALUES (1474, 1474, 108, 'Babakan');
INSERT INTO `subdistricts` VALUES (1475, 1475, 108, 'Beber');
INSERT INTO `subdistricts` VALUES (1476, 1476, 108, 'Ciledug');
INSERT INTO `subdistricts` VALUES (1477, 1477, 108, 'Ciwaringin');
INSERT INTO `subdistricts` VALUES (1478, 1478, 108, 'Depok');
INSERT INTO `subdistricts` VALUES (1479, 1479, 108, 'Dukupuntang');
INSERT INTO `subdistricts` VALUES (1480, 1480, 108, 'Gebang');
INSERT INTO `subdistricts` VALUES (1481, 1481, 108, 'Gegesik');
INSERT INTO `subdistricts` VALUES (1482, 1482, 108, 'Gempol');
INSERT INTO `subdistricts` VALUES (1483, 1483, 108, 'Greged (Greget)');
INSERT INTO `subdistricts` VALUES (1484, 1484, 108, 'Gunung Jati (Cirebon Utara)');
INSERT INTO `subdistricts` VALUES (1485, 1485, 108, 'Jamblang');
INSERT INTO `subdistricts` VALUES (1486, 1486, 108, 'Kaliwedi');
INSERT INTO `subdistricts` VALUES (1487, 1487, 108, 'Kapetakan');
INSERT INTO `subdistricts` VALUES (1488, 1488, 108, 'Karangsembung');
INSERT INTO `subdistricts` VALUES (1489, 1489, 108, 'Karangwareng');
INSERT INTO `subdistricts` VALUES (1490, 1490, 108, 'Kedawung');
INSERT INTO `subdistricts` VALUES (1491, 1491, 108, 'Klangenan');
INSERT INTO `subdistricts` VALUES (1492, 1492, 108, 'Lemahabang');
INSERT INTO `subdistricts` VALUES (1493, 1493, 108, 'Losari');
INSERT INTO `subdistricts` VALUES (1494, 1494, 108, 'Mundu');
INSERT INTO `subdistricts` VALUES (1495, 1495, 108, 'Pabedilan');
INSERT INTO `subdistricts` VALUES (1496, 1496, 108, 'Pabuaran');
INSERT INTO `subdistricts` VALUES (1497, 1497, 108, 'Palimanan');
INSERT INTO `subdistricts` VALUES (1498, 1498, 108, 'Pangenan');
INSERT INTO `subdistricts` VALUES (1499, 1499, 108, 'Panguragan');
INSERT INTO `subdistricts` VALUES (1500, 1500, 108, 'Pasaleman');
INSERT INTO `subdistricts` VALUES (1501, 1501, 108, 'Plered');
INSERT INTO `subdistricts` VALUES (1502, 1502, 108, 'Plumbon');
INSERT INTO `subdistricts` VALUES (1503, 1503, 108, 'Sedong');
INSERT INTO `subdistricts` VALUES (1504, 1504, 108, 'Sumber');
INSERT INTO `subdistricts` VALUES (1505, 1505, 108, 'Suranenggala');
INSERT INTO `subdistricts` VALUES (1506, 1506, 108, 'Susukan');
INSERT INTO `subdistricts` VALUES (1507, 1507, 108, 'Susukan Lebak');
INSERT INTO `subdistricts` VALUES (1508, 1508, 108, 'Talun (Cirebon Selatan)');
INSERT INTO `subdistricts` VALUES (1509, 1509, 108, 'Tengah Tani');
INSERT INTO `subdistricts` VALUES (1510, 1510, 108, 'Waled');
INSERT INTO `subdistricts` VALUES (1511, 1511, 108, 'Weru');
INSERT INTO `subdistricts` VALUES (1512, 1512, 109, 'Harjamukti');
INSERT INTO `subdistricts` VALUES (1513, 1513, 109, 'Kejaksan');
INSERT INTO `subdistricts` VALUES (1514, 1514, 109, 'Kesambi');
INSERT INTO `subdistricts` VALUES (1515, 1515, 109, 'Lemahwungkuk');
INSERT INTO `subdistricts` VALUES (1516, 1516, 109, 'Pekalipan');
INSERT INTO `subdistricts` VALUES (1517, 1517, 110, 'Berampu (Brampu)');
INSERT INTO `subdistricts` VALUES (1518, 1518, 110, 'Gunung Sitember');
INSERT INTO `subdistricts` VALUES (1519, 1519, 110, 'Lae Parira');
INSERT INTO `subdistricts` VALUES (1520, 1520, 110, 'Parbuluan');
INSERT INTO `subdistricts` VALUES (1521, 1521, 110, 'Pegagan Hilir');
INSERT INTO `subdistricts` VALUES (1522, 1522, 110, 'Sidikalang');
INSERT INTO `subdistricts` VALUES (1523, 1523, 110, 'Siempat Nempu');
INSERT INTO `subdistricts` VALUES (1524, 1524, 110, 'Siempat Nempu Hilir');
INSERT INTO `subdistricts` VALUES (1525, 1525, 110, 'Siempat Nempu Hulu');
INSERT INTO `subdistricts` VALUES (1526, 1526, 110, 'Silahi Sabungan');
INSERT INTO `subdistricts` VALUES (1527, 1527, 110, 'Silima Pungga-Pungga');
INSERT INTO `subdistricts` VALUES (1528, 1528, 110, 'Sitinjo');
INSERT INTO `subdistricts` VALUES (1529, 1529, 110, 'Sumbul');
INSERT INTO `subdistricts` VALUES (1530, 1530, 110, 'Tanah Pinem');
INSERT INTO `subdistricts` VALUES (1531, 1531, 110, 'Tiga Lingga');
INSERT INTO `subdistricts` VALUES (1532, 1532, 111, 'Bowobado');
INSERT INTO `subdistricts` VALUES (1533, 1533, 111, 'Kapiraya');
INSERT INTO `subdistricts` VALUES (1534, 1534, 111, 'Tigi');
INSERT INTO `subdistricts` VALUES (1535, 1535, 111, 'Tigi Barat');
INSERT INTO `subdistricts` VALUES (1536, 1536, 111, 'Tigi Timur');
INSERT INTO `subdistricts` VALUES (1537, 1537, 112, 'Bangun Purba');
INSERT INTO `subdistricts` VALUES (1538, 1538, 112, 'Batang Kuis');
INSERT INTO `subdistricts` VALUES (1539, 1539, 112, 'Beringin');
INSERT INTO `subdistricts` VALUES (1540, 1540, 112, 'Biru-Biru');
INSERT INTO `subdistricts` VALUES (1541, 1541, 112, 'Deli Tua');
INSERT INTO `subdistricts` VALUES (1542, 1542, 112, 'Galang');
INSERT INTO `subdistricts` VALUES (1543, 1543, 112, 'Gunung Meriah');
INSERT INTO `subdistricts` VALUES (1544, 1544, 112, 'Hamparan Perak');
INSERT INTO `subdistricts` VALUES (1545, 1545, 112, 'Kutalimbaru');
INSERT INTO `subdistricts` VALUES (1546, 1546, 112, 'Labuhan Deli');
INSERT INTO `subdistricts` VALUES (1547, 1547, 112, 'Lubuk Pakam');
INSERT INTO `subdistricts` VALUES (1548, 1548, 112, 'Namo Rambe');
INSERT INTO `subdistricts` VALUES (1549, 1549, 112, 'Pagar Merbau');
INSERT INTO `subdistricts` VALUES (1550, 1550, 112, 'Pancur Batu');
INSERT INTO `subdistricts` VALUES (1551, 1551, 112, 'Pantai Labu');
INSERT INTO `subdistricts` VALUES (1552, 1552, 112, 'Patumbak');
INSERT INTO `subdistricts` VALUES (1553, 1553, 112, 'Percut Sei Tuan');
INSERT INTO `subdistricts` VALUES (1554, 1554, 112, 'Sibolangit');
INSERT INTO `subdistricts` VALUES (1555, 1555, 112, 'Sinembah Tanjung Muda Hilir');
INSERT INTO `subdistricts` VALUES (1556, 1556, 112, 'Sinembah Tanjung Muda Hulu');
INSERT INTO `subdistricts` VALUES (1557, 1557, 112, 'Sunggal');
INSERT INTO `subdistricts` VALUES (1558, 1558, 112, 'Tanjung Morawa');
INSERT INTO `subdistricts` VALUES (1559, 1559, 113, 'Bonang');
INSERT INTO `subdistricts` VALUES (1560, 1560, 113, 'Demak');
INSERT INTO `subdistricts` VALUES (1561, 1561, 113, 'Dempet');
INSERT INTO `subdistricts` VALUES (1562, 1562, 113, 'Gajah');
INSERT INTO `subdistricts` VALUES (1563, 1563, 113, 'Guntur');
INSERT INTO `subdistricts` VALUES (1564, 1564, 113, 'Karang Tengah');
INSERT INTO `subdistricts` VALUES (1565, 1565, 113, 'Karanganyar');
INSERT INTO `subdistricts` VALUES (1566, 1566, 113, 'Karangawen');
INSERT INTO `subdistricts` VALUES (1567, 1567, 113, 'Kebonagung');
INSERT INTO `subdistricts` VALUES (1568, 1568, 113, 'Mijen');
INSERT INTO `subdistricts` VALUES (1569, 1569, 113, 'Mranggen');
INSERT INTO `subdistricts` VALUES (1570, 1570, 113, 'Sayung');
INSERT INTO `subdistricts` VALUES (1571, 1571, 113, 'Wedung');
INSERT INTO `subdistricts` VALUES (1572, 1572, 113, 'Wonosalam');
INSERT INTO `subdistricts` VALUES (1573, 1573, 114, 'Denpasar Barat');
INSERT INTO `subdistricts` VALUES (1574, 1574, 114, 'Denpasar Selatan');
INSERT INTO `subdistricts` VALUES (1575, 1575, 114, 'Denpasar Timur');
INSERT INTO `subdistricts` VALUES (1576, 1576, 114, 'Denpasar Utara');
INSERT INTO `subdistricts` VALUES (1577, 1577, 115, 'Beji');
INSERT INTO `subdistricts` VALUES (1578, 1578, 115, 'Bojongsari');
INSERT INTO `subdistricts` VALUES (1579, 1579, 115, 'Cilodong');
INSERT INTO `subdistricts` VALUES (1580, 1580, 115, 'Cimanggis');
INSERT INTO `subdistricts` VALUES (1581, 1581, 115, 'Cinere');
INSERT INTO `subdistricts` VALUES (1582, 1582, 115, 'Cipayung');
INSERT INTO `subdistricts` VALUES (1583, 1583, 115, 'Limo');
INSERT INTO `subdistricts` VALUES (1584, 1584, 115, 'Pancoran Mas');
INSERT INTO `subdistricts` VALUES (1585, 1585, 115, 'Sawangan');
INSERT INTO `subdistricts` VALUES (1586, 1586, 115, 'Sukmajaya');
INSERT INTO `subdistricts` VALUES (1587, 1587, 115, 'Tapos');
INSERT INTO `subdistricts` VALUES (1588, 1588, 116, 'Asam Jujuhan');
INSERT INTO `subdistricts` VALUES (1589, 1589, 116, 'Koto Baru');
INSERT INTO `subdistricts` VALUES (1590, 1590, 116, 'Koto Besar');
INSERT INTO `subdistricts` VALUES (1591, 1591, 116, 'Koto Salak');
INSERT INTO `subdistricts` VALUES (1592, 1592, 116, 'Padang Laweh');
INSERT INTO `subdistricts` VALUES (1593, 1593, 116, 'Pulau Punjung');
INSERT INTO `subdistricts` VALUES (1594, 1594, 116, 'Sembilan Koto (IX Koto)');
INSERT INTO `subdistricts` VALUES (1595, 1595, 116, 'Sitiung');
INSERT INTO `subdistricts` VALUES (1596, 1596, 116, 'Sungai Rumbai');
INSERT INTO `subdistricts` VALUES (1597, 1597, 116, 'Timpeh');
INSERT INTO `subdistricts` VALUES (1598, 1598, 116, 'Tiumang');
INSERT INTO `subdistricts` VALUES (1599, 1599, 117, 'Dogiyai');
INSERT INTO `subdistricts` VALUES (1600, 1600, 117, 'Kamu');
INSERT INTO `subdistricts` VALUES (1601, 1601, 117, 'Kamu Selatan');
INSERT INTO `subdistricts` VALUES (1602, 1602, 117, 'Kamu Timur');
INSERT INTO `subdistricts` VALUES (1603, 1603, 117, 'Kamu Utara (Ikrar/Ikrat)');
INSERT INTO `subdistricts` VALUES (1604, 1604, 117, 'Mapia');
INSERT INTO `subdistricts` VALUES (1605, 1605, 117, 'Mapia Barat');
INSERT INTO `subdistricts` VALUES (1606, 1606, 117, 'Mapia Tengah');
INSERT INTO `subdistricts` VALUES (1607, 1607, 117, 'Piyaiye (Sukikai)');
INSERT INTO `subdistricts` VALUES (1608, 1608, 117, 'Sukikai Selatan');
INSERT INTO `subdistricts` VALUES (1609, 1609, 118, 'Dompu');
INSERT INTO `subdistricts` VALUES (1610, 1610, 118, 'Hu\'u');
INSERT INTO `subdistricts` VALUES (1611, 1611, 118, 'Kempo');
INSERT INTO `subdistricts` VALUES (1612, 1612, 118, 'Kilo');
INSERT INTO `subdistricts` VALUES (1613, 1613, 118, 'Menggelewa (Manggelewa)');
INSERT INTO `subdistricts` VALUES (1614, 1614, 118, 'Pajo');
INSERT INTO `subdistricts` VALUES (1615, 1615, 118, 'Pekat');
INSERT INTO `subdistricts` VALUES (1616, 1616, 118, 'Woja');
INSERT INTO `subdistricts` VALUES (1617, 1617, 119, 'Balaesang');
INSERT INTO `subdistricts` VALUES (1618, 1618, 119, 'Balaesang Tanjung');
INSERT INTO `subdistricts` VALUES (1619, 1619, 119, 'Banawa');
INSERT INTO `subdistricts` VALUES (1620, 1620, 119, 'Banawa Selatan');
INSERT INTO `subdistricts` VALUES (1621, 1621, 119, 'Banawa Tengah');
INSERT INTO `subdistricts` VALUES (1622, 1622, 119, 'Damsol (Dampelas Sojol)');
INSERT INTO `subdistricts` VALUES (1623, 1623, 119, 'Labuan');
INSERT INTO `subdistricts` VALUES (1624, 1624, 119, 'Pinembani');
INSERT INTO `subdistricts` VALUES (1625, 1625, 119, 'Rio Pakava (Riopakawa)');
INSERT INTO `subdistricts` VALUES (1626, 1626, 119, 'Sindue');
INSERT INTO `subdistricts` VALUES (1627, 1627, 119, 'Sindue Tobata');
INSERT INTO `subdistricts` VALUES (1628, 1628, 119, 'Sindue Tombusabora');
INSERT INTO `subdistricts` VALUES (1629, 1629, 119, 'Sirenja');
INSERT INTO `subdistricts` VALUES (1630, 1630, 119, 'Sojol');
INSERT INTO `subdistricts` VALUES (1631, 1631, 119, 'Sojol Utara');
INSERT INTO `subdistricts` VALUES (1632, 1632, 119, 'Tanantovea');
INSERT INTO `subdistricts` VALUES (1633, 1633, 120, 'Bukit Kapur');
INSERT INTO `subdistricts` VALUES (1634, 1634, 120, 'Dumai Barat');
INSERT INTO `subdistricts` VALUES (1635, 1635, 120, 'Dumai Kota');
INSERT INTO `subdistricts` VALUES (1636, 1636, 120, 'Dumai Selatan');
INSERT INTO `subdistricts` VALUES (1637, 1637, 120, 'Dumai Timur');
INSERT INTO `subdistricts` VALUES (1638, 1638, 120, 'Medang Kampai');
INSERT INTO `subdistricts` VALUES (1639, 1639, 120, 'Sungai Sembilan');
INSERT INTO `subdistricts` VALUES (1640, 1640, 121, 'Lintang Kanan');
INSERT INTO `subdistricts` VALUES (1641, 1641, 121, 'Muara Pinang');
INSERT INTO `subdistricts` VALUES (1642, 1642, 121, 'Pasemah Air Keruh');
INSERT INTO `subdistricts` VALUES (1643, 1643, 121, 'Pendopo');
INSERT INTO `subdistricts` VALUES (1644, 1644, 121, 'Pendopo Barat');
INSERT INTO `subdistricts` VALUES (1645, 1645, 121, 'Saling');
INSERT INTO `subdistricts` VALUES (1646, 1646, 121, 'Sikap Dalam');
INSERT INTO `subdistricts` VALUES (1647, 1647, 121, 'Talang Padang');
INSERT INTO `subdistricts` VALUES (1648, 1648, 121, 'Tebing Tinggi');
INSERT INTO `subdistricts` VALUES (1649, 1649, 121, 'Ulu Musi');
INSERT INTO `subdistricts` VALUES (1650, 1650, 122, 'Detukeli');
INSERT INTO `subdistricts` VALUES (1651, 1651, 122, 'Detusoko');
INSERT INTO `subdistricts` VALUES (1652, 1652, 122, 'Ende');
INSERT INTO `subdistricts` VALUES (1653, 1653, 122, 'Ende Selatan');
INSERT INTO `subdistricts` VALUES (1654, 1654, 122, 'Ende Tengah');
INSERT INTO `subdistricts` VALUES (1655, 1655, 122, 'Ende Timur');
INSERT INTO `subdistricts` VALUES (1656, 1656, 122, 'Ende Utara');
INSERT INTO `subdistricts` VALUES (1657, 1657, 122, 'Kelimutu');
INSERT INTO `subdistricts` VALUES (1658, 1658, 122, 'Kotabaru');
INSERT INTO `subdistricts` VALUES (1659, 1659, 122, 'Lepembusu Kelisoke');
INSERT INTO `subdistricts` VALUES (1660, 1660, 122, 'Lio Timur');
INSERT INTO `subdistricts` VALUES (1661, 1661, 122, 'Maukaro');
INSERT INTO `subdistricts` VALUES (1662, 1662, 122, 'Maurole');
INSERT INTO `subdistricts` VALUES (1663, 1663, 122, 'Nangapanda');
INSERT INTO `subdistricts` VALUES (1664, 1664, 122, 'Ndona');
INSERT INTO `subdistricts` VALUES (1665, 1665, 122, 'Ndona Timur');
INSERT INTO `subdistricts` VALUES (1666, 1666, 122, 'Ndori');
INSERT INTO `subdistricts` VALUES (1667, 1667, 122, 'Pulau Ende');
INSERT INTO `subdistricts` VALUES (1668, 1668, 122, 'Wewaria');
INSERT INTO `subdistricts` VALUES (1669, 1669, 122, 'Wolojita');
INSERT INTO `subdistricts` VALUES (1670, 1670, 122, 'Wolowaru');
INSERT INTO `subdistricts` VALUES (1671, 1671, 123, 'Alla');
INSERT INTO `subdistricts` VALUES (1672, 1672, 123, 'Anggeraja');
INSERT INTO `subdistricts` VALUES (1673, 1673, 123, 'Baraka');
INSERT INTO `subdistricts` VALUES (1674, 1674, 123, 'Baroko');
INSERT INTO `subdistricts` VALUES (1675, 1675, 123, 'Bungin');
INSERT INTO `subdistricts` VALUES (1676, 1676, 123, 'Buntu Batu');
INSERT INTO `subdistricts` VALUES (1677, 1677, 123, 'Cendana');
INSERT INTO `subdistricts` VALUES (1678, 1678, 123, 'Curio');
INSERT INTO `subdistricts` VALUES (1679, 1679, 123, 'Enrekang');
INSERT INTO `subdistricts` VALUES (1680, 1680, 123, 'Maiwa');
INSERT INTO `subdistricts` VALUES (1681, 1681, 123, 'Malua');
INSERT INTO `subdistricts` VALUES (1682, 1682, 123, 'Masalle');
INSERT INTO `subdistricts` VALUES (1683, 1683, 124, 'Bombarai (Bomberay)');
INSERT INTO `subdistricts` VALUES (1684, 1684, 124, 'Fakfak');
INSERT INTO `subdistricts` VALUES (1685, 1685, 124, 'Fakfak Barat');
INSERT INTO `subdistricts` VALUES (1686, 1686, 124, 'Fakfak Tengah');
INSERT INTO `subdistricts` VALUES (1687, 1687, 124, 'Fakfak Timur');
INSERT INTO `subdistricts` VALUES (1688, 1688, 124, 'Karas');
INSERT INTO `subdistricts` VALUES (1689, 1689, 124, 'Kokas');
INSERT INTO `subdistricts` VALUES (1690, 1690, 124, 'Kramongmongga (Kramamongga)');
INSERT INTO `subdistricts` VALUES (1691, 1691, 124, 'Teluk Patipi');
INSERT INTO `subdistricts` VALUES (1692, 1692, 125, 'Adonara');
INSERT INTO `subdistricts` VALUES (1693, 1693, 125, 'Adonara Barat');
INSERT INTO `subdistricts` VALUES (1694, 1694, 125, 'Adonara Tengah');
INSERT INTO `subdistricts` VALUES (1695, 1695, 125, 'Adonara Timur');
INSERT INTO `subdistricts` VALUES (1696, 1696, 125, 'Demon Pagong');
INSERT INTO `subdistricts` VALUES (1697, 1697, 125, 'Ile Boleng');
INSERT INTO `subdistricts` VALUES (1698, 1698, 125, 'Ile Bura');
INSERT INTO `subdistricts` VALUES (1699, 1699, 125, 'Ile Mandiri');
INSERT INTO `subdistricts` VALUES (1700, 1700, 125, 'Kelubagolit (Klubagolit)');
INSERT INTO `subdistricts` VALUES (1701, 1701, 125, 'Larantuka');
INSERT INTO `subdistricts` VALUES (1702, 1702, 125, 'Lewolema');
INSERT INTO `subdistricts` VALUES (1703, 1703, 125, 'Solor Barat');
INSERT INTO `subdistricts` VALUES (1704, 1704, 125, 'Solor Selatan');
INSERT INTO `subdistricts` VALUES (1705, 1705, 125, 'Solor Timur');
INSERT INTO `subdistricts` VALUES (1706, 1706, 125, 'Tanjung Bunga');
INSERT INTO `subdistricts` VALUES (1707, 1707, 125, 'Titehena');
INSERT INTO `subdistricts` VALUES (1708, 1708, 125, 'Witihama (Watihama)');
INSERT INTO `subdistricts` VALUES (1709, 1709, 125, 'Wotan Ulumado');
INSERT INTO `subdistricts` VALUES (1710, 1710, 125, 'Wulanggitang');
INSERT INTO `subdistricts` VALUES (1711, 1711, 126, 'Banjarwangi');
INSERT INTO `subdistricts` VALUES (1712, 1712, 126, 'Banyuresmi');
INSERT INTO `subdistricts` VALUES (1713, 1713, 126, 'Bayongbong');
INSERT INTO `subdistricts` VALUES (1714, 1714, 126, 'Blubur Limbangan');
INSERT INTO `subdistricts` VALUES (1715, 1715, 126, 'Bungbulang');
INSERT INTO `subdistricts` VALUES (1716, 1716, 126, 'Caringin');
INSERT INTO `subdistricts` VALUES (1717, 1717, 126, 'Cibalong');
INSERT INTO `subdistricts` VALUES (1718, 1718, 126, 'Cibatu');
INSERT INTO `subdistricts` VALUES (1719, 1719, 126, 'Cibiuk');
INSERT INTO `subdistricts` VALUES (1720, 1720, 126, 'Cigedug');
INSERT INTO `subdistricts` VALUES (1721, 1721, 126, 'Cihurip');
INSERT INTO `subdistricts` VALUES (1722, 1722, 126, 'Cikajang');
INSERT INTO `subdistricts` VALUES (1723, 1723, 126, 'Cikelet');
INSERT INTO `subdistricts` VALUES (1724, 1724, 126, 'Cilawu');
INSERT INTO `subdistricts` VALUES (1725, 1725, 126, 'Cisewu');
INSERT INTO `subdistricts` VALUES (1726, 1726, 126, 'Cisompet');
INSERT INTO `subdistricts` VALUES (1727, 1727, 126, 'Cisurupan');
INSERT INTO `subdistricts` VALUES (1728, 1728, 126, 'Garut Kota');
INSERT INTO `subdistricts` VALUES (1729, 1729, 126, 'Kadungora');
INSERT INTO `subdistricts` VALUES (1730, 1730, 126, 'Karangpawitan');
INSERT INTO `subdistricts` VALUES (1731, 1731, 126, 'Karangtengah');
INSERT INTO `subdistricts` VALUES (1732, 1732, 126, 'Kersamanah');
INSERT INTO `subdistricts` VALUES (1733, 1733, 126, 'Leles');
INSERT INTO `subdistricts` VALUES (1734, 1734, 126, 'Leuwigoong');
INSERT INTO `subdistricts` VALUES (1735, 1735, 126, 'Malangbong');
INSERT INTO `subdistricts` VALUES (1736, 1736, 126, 'Mekarmukti');
INSERT INTO `subdistricts` VALUES (1737, 1737, 126, 'Pakenjeng');
INSERT INTO `subdistricts` VALUES (1738, 1738, 126, 'Pameungpeuk');
INSERT INTO `subdistricts` VALUES (1739, 1739, 126, 'Pamulihan');
INSERT INTO `subdistricts` VALUES (1740, 1740, 126, 'Pangatikan');
INSERT INTO `subdistricts` VALUES (1741, 1741, 126, 'Pasirwangi');
INSERT INTO `subdistricts` VALUES (1742, 1742, 126, 'Peundeuy');
INSERT INTO `subdistricts` VALUES (1743, 1743, 126, 'Samarang');
INSERT INTO `subdistricts` VALUES (1744, 1744, 126, 'Selaawi');
INSERT INTO `subdistricts` VALUES (1745, 1745, 126, 'Singajaya');
INSERT INTO `subdistricts` VALUES (1746, 1746, 126, 'Sucinaraja');
INSERT INTO `subdistricts` VALUES (1747, 1747, 126, 'Sukaresmi');
INSERT INTO `subdistricts` VALUES (1748, 1748, 126, 'Sukawening');
INSERT INTO `subdistricts` VALUES (1749, 1749, 126, 'Talegong');
INSERT INTO `subdistricts` VALUES (1750, 1750, 126, 'Tarogong Kaler');
INSERT INTO `subdistricts` VALUES (1751, 1751, 126, 'Tarogong Kidul');
INSERT INTO `subdistricts` VALUES (1752, 1752, 126, 'Wanaraja');
INSERT INTO `subdistricts` VALUES (1753, 1753, 127, 'Blang Jerango');
INSERT INTO `subdistricts` VALUES (1754, 1754, 127, 'Blang Kejeren');
INSERT INTO `subdistricts` VALUES (1755, 1755, 127, 'Blang Pegayon');
INSERT INTO `subdistricts` VALUES (1756, 1756, 127, 'Dabun Gelang (Debun Gelang)');
INSERT INTO `subdistricts` VALUES (1757, 1757, 127, 'Kuta Panjang');
INSERT INTO `subdistricts` VALUES (1758, 1758, 127, 'Pantan Cuaca');
INSERT INTO `subdistricts` VALUES (1759, 1759, 127, 'Pining (Pinding)');
INSERT INTO `subdistricts` VALUES (1760, 1760, 127, 'Putri Betung');
INSERT INTO `subdistricts` VALUES (1761, 1761, 127, 'Rikit Gaib');
INSERT INTO `subdistricts` VALUES (1762, 1762, 127, 'Terangun (Terangon)');
INSERT INTO `subdistricts` VALUES (1763, 1763, 127, 'Teripe/Tripe Jaya');
INSERT INTO `subdistricts` VALUES (1764, 1764, 128, 'Belah Batuh (Blahbatuh)');
INSERT INTO `subdistricts` VALUES (1765, 1765, 128, 'Gianyar');
INSERT INTO `subdistricts` VALUES (1766, 1766, 128, 'Payangan');
INSERT INTO `subdistricts` VALUES (1767, 1767, 128, 'Sukawati');
INSERT INTO `subdistricts` VALUES (1768, 1768, 128, 'Tampak Siring');
INSERT INTO `subdistricts` VALUES (1769, 1769, 128, 'Tegallalang');
INSERT INTO `subdistricts` VALUES (1770, 1770, 128, 'Ubud');
INSERT INTO `subdistricts` VALUES (1771, 1771, 129, 'Asparaga');
INSERT INTO `subdistricts` VALUES (1772, 1772, 129, 'Batudaa');
INSERT INTO `subdistricts` VALUES (1773, 1773, 129, 'Batudaa Pantai');
INSERT INTO `subdistricts` VALUES (1774, 1774, 129, 'Bilato');
INSERT INTO `subdistricts` VALUES (1775, 1775, 129, 'Biluhu');
INSERT INTO `subdistricts` VALUES (1776, 1776, 129, 'Boliohuto (Boliyohuto)');
INSERT INTO `subdistricts` VALUES (1777, 1777, 129, 'Bongomeme');
INSERT INTO `subdistricts` VALUES (1778, 1778, 129, 'Dungaliyo');
INSERT INTO `subdistricts` VALUES (1779, 1779, 129, 'Limboto');
INSERT INTO `subdistricts` VALUES (1780, 1780, 129, 'Limboto Barat');
INSERT INTO `subdistricts` VALUES (1781, 1781, 129, 'Mootilango');
INSERT INTO `subdistricts` VALUES (1782, 1782, 129, 'Pulubala');
INSERT INTO `subdistricts` VALUES (1783, 1783, 129, 'Tabongo');
INSERT INTO `subdistricts` VALUES (1784, 1784, 129, 'Telaga');
INSERT INTO `subdistricts` VALUES (1785, 1785, 129, 'Telaga Biru');
INSERT INTO `subdistricts` VALUES (1786, 1786, 129, 'Telaga Jaya');
INSERT INTO `subdistricts` VALUES (1787, 1787, 129, 'Tibawa');
INSERT INTO `subdistricts` VALUES (1788, 1788, 129, 'Tilango');
INSERT INTO `subdistricts` VALUES (1789, 1789, 129, 'Tolangohula');
INSERT INTO `subdistricts` VALUES (1790, 1790, 130, 'Dumbo Raya');
INSERT INTO `subdistricts` VALUES (1791, 1791, 130, 'Dungingi');
INSERT INTO `subdistricts` VALUES (1792, 1792, 130, 'Hulonthalangi');
INSERT INTO `subdistricts` VALUES (1793, 1793, 130, 'Kota Barat');
INSERT INTO `subdistricts` VALUES (1794, 1794, 130, 'Kota Selatan');
INSERT INTO `subdistricts` VALUES (1795, 1795, 130, 'Kota Tengah');
INSERT INTO `subdistricts` VALUES (1796, 1796, 130, 'Kota Timur');
INSERT INTO `subdistricts` VALUES (1797, 1797, 130, 'Kota Utara');
INSERT INTO `subdistricts` VALUES (1798, 1798, 130, 'Sipatana');
INSERT INTO `subdistricts` VALUES (1799, 1799, 131, 'Anggrek');
INSERT INTO `subdistricts` VALUES (1800, 1800, 131, 'Atinggola');
INSERT INTO `subdistricts` VALUES (1801, 1801, 131, 'Biau');
INSERT INTO `subdistricts` VALUES (1802, 1802, 131, 'Gentuma Raya');
INSERT INTO `subdistricts` VALUES (1803, 1803, 131, 'Kwandang');
INSERT INTO `subdistricts` VALUES (1804, 1804, 131, 'Monano');
INSERT INTO `subdistricts` VALUES (1805, 1805, 131, 'Ponelo Kepulauan');
INSERT INTO `subdistricts` VALUES (1806, 1806, 131, 'Sumalata');
INSERT INTO `subdistricts` VALUES (1807, 1807, 131, 'Sumalata Timur');
INSERT INTO `subdistricts` VALUES (1808, 1808, 131, 'Tolinggula');
INSERT INTO `subdistricts` VALUES (1809, 1809, 131, 'Tomolito');
INSERT INTO `subdistricts` VALUES (1810, 1810, 132, 'Bajeng');
INSERT INTO `subdistricts` VALUES (1811, 1811, 132, 'Bajeng Barat');
INSERT INTO `subdistricts` VALUES (1812, 1812, 132, 'Barombong');
INSERT INTO `subdistricts` VALUES (1813, 1813, 132, 'Biringbulu');
INSERT INTO `subdistricts` VALUES (1814, 1814, 132, 'Bontolempangang');
INSERT INTO `subdistricts` VALUES (1815, 1815, 132, 'Bontomarannu');
INSERT INTO `subdistricts` VALUES (1816, 1816, 132, 'Bontonompo');
INSERT INTO `subdistricts` VALUES (1817, 1817, 132, 'Bontonompo Selatan');
INSERT INTO `subdistricts` VALUES (1818, 1818, 132, 'Bungaya');
INSERT INTO `subdistricts` VALUES (1819, 1819, 132, 'Manuju');
INSERT INTO `subdistricts` VALUES (1820, 1820, 132, 'Pallangga');
INSERT INTO `subdistricts` VALUES (1821, 1821, 132, 'Parangloe');
INSERT INTO `subdistricts` VALUES (1822, 1822, 132, 'Parigi');
INSERT INTO `subdistricts` VALUES (1823, 1823, 132, 'Pattallassang');
INSERT INTO `subdistricts` VALUES (1824, 1824, 132, 'Somba Opu (Upu)');
INSERT INTO `subdistricts` VALUES (1825, 1825, 132, 'Tinggimoncong');
INSERT INTO `subdistricts` VALUES (1826, 1826, 132, 'Tombolo Pao');
INSERT INTO `subdistricts` VALUES (1827, 1827, 132, 'Tompobulu');
INSERT INTO `subdistricts` VALUES (1828, 1828, 133, 'Balong Panggang');
INSERT INTO `subdistricts` VALUES (1829, 1829, 133, 'Benjeng');
INSERT INTO `subdistricts` VALUES (1830, 1830, 133, 'Bungah');
INSERT INTO `subdistricts` VALUES (1831, 1831, 133, 'Cerme');
INSERT INTO `subdistricts` VALUES (1832, 1832, 133, 'Driyorejo');
INSERT INTO `subdistricts` VALUES (1833, 1833, 133, 'Duduk Sampeyan');
INSERT INTO `subdistricts` VALUES (1834, 1834, 133, 'Dukun');
INSERT INTO `subdistricts` VALUES (1835, 1835, 133, 'Gresik');
INSERT INTO `subdistricts` VALUES (1836, 1836, 133, 'Kebomas');
INSERT INTO `subdistricts` VALUES (1837, 1837, 133, 'Kedamean');
INSERT INTO `subdistricts` VALUES (1838, 1838, 133, 'Manyar');
INSERT INTO `subdistricts` VALUES (1839, 1839, 133, 'Menganti');
INSERT INTO `subdistricts` VALUES (1840, 1840, 133, 'Panceng');
INSERT INTO `subdistricts` VALUES (1841, 1841, 133, 'Sangkapura');
INSERT INTO `subdistricts` VALUES (1842, 1842, 133, 'Sidayu');
INSERT INTO `subdistricts` VALUES (1843, 1843, 133, 'Tambak');
INSERT INTO `subdistricts` VALUES (1844, 1844, 133, 'Ujung Pangkah');
INSERT INTO `subdistricts` VALUES (1845, 1845, 133, 'Wringin Anom');
INSERT INTO `subdistricts` VALUES (1846, 1846, 134, 'Brati');
INSERT INTO `subdistricts` VALUES (1847, 1847, 134, 'Gabus');
INSERT INTO `subdistricts` VALUES (1848, 1848, 134, 'Geyer');
INSERT INTO `subdistricts` VALUES (1849, 1849, 134, 'Godong');
INSERT INTO `subdistricts` VALUES (1850, 1850, 134, 'Grobogan');
INSERT INTO `subdistricts` VALUES (1851, 1851, 134, 'Gubug');
INSERT INTO `subdistricts` VALUES (1852, 1852, 134, 'Karangrayung');
INSERT INTO `subdistricts` VALUES (1853, 1853, 134, 'Kedungjati');
INSERT INTO `subdistricts` VALUES (1854, 1854, 134, 'Klambu');
INSERT INTO `subdistricts` VALUES (1855, 1855, 134, 'Kradenan');
INSERT INTO `subdistricts` VALUES (1856, 1856, 134, 'Ngaringan');
INSERT INTO `subdistricts` VALUES (1857, 1857, 134, 'Penawangan');
INSERT INTO `subdistricts` VALUES (1858, 1858, 134, 'Pulokulon');
INSERT INTO `subdistricts` VALUES (1859, 1859, 134, 'Purwodadi');
INSERT INTO `subdistricts` VALUES (1860, 1860, 134, 'Tanggungharjo');
INSERT INTO `subdistricts` VALUES (1861, 1861, 134, 'Tawangharjo');
INSERT INTO `subdistricts` VALUES (1862, 1862, 134, 'Tegowanu');
INSERT INTO `subdistricts` VALUES (1863, 1863, 134, 'Toroh');
INSERT INTO `subdistricts` VALUES (1864, 1864, 134, 'Wirosari');
INSERT INTO `subdistricts` VALUES (1865, 1865, 135, 'Gedang Sari');
INSERT INTO `subdistricts` VALUES (1866, 1866, 135, 'Girisubo');
INSERT INTO `subdistricts` VALUES (1867, 1867, 135, 'Karangmojo');
INSERT INTO `subdistricts` VALUES (1868, 1868, 135, 'Ngawen');
INSERT INTO `subdistricts` VALUES (1869, 1869, 135, 'Nglipar');
INSERT INTO `subdistricts` VALUES (1870, 1870, 135, 'Paliyan');
INSERT INTO `subdistricts` VALUES (1871, 1871, 135, 'Panggang');
INSERT INTO `subdistricts` VALUES (1872, 1872, 135, 'Patuk');
INSERT INTO `subdistricts` VALUES (1873, 1873, 135, 'Playen');
INSERT INTO `subdistricts` VALUES (1874, 1874, 135, 'Ponjong');
INSERT INTO `subdistricts` VALUES (1875, 1875, 135, 'Purwosari');
INSERT INTO `subdistricts` VALUES (1876, 1876, 135, 'Rongkop');
INSERT INTO `subdistricts` VALUES (1877, 1877, 135, 'Sapto Sari');
INSERT INTO `subdistricts` VALUES (1878, 1878, 135, 'Semanu');
INSERT INTO `subdistricts` VALUES (1879, 1879, 135, 'Semin');
INSERT INTO `subdistricts` VALUES (1880, 1880, 135, 'Tanjungsari');
INSERT INTO `subdistricts` VALUES (1881, 1881, 135, 'Tepus');
INSERT INTO `subdistricts` VALUES (1882, 1882, 135, 'Wonosari');
INSERT INTO `subdistricts` VALUES (1883, 1883, 136, 'Damang Batu');
INSERT INTO `subdistricts` VALUES (1884, 1884, 136, 'Kahayan Hulu Utara');
INSERT INTO `subdistricts` VALUES (1885, 1885, 136, 'Kurun');
INSERT INTO `subdistricts` VALUES (1886, 1886, 136, 'Manuhing');
INSERT INTO `subdistricts` VALUES (1887, 1887, 136, 'Manuhing Raya');
INSERT INTO `subdistricts` VALUES (1888, 1888, 136, 'Mihing Raya');
INSERT INTO `subdistricts` VALUES (1889, 1889, 136, 'Miri Manasa');
INSERT INTO `subdistricts` VALUES (1890, 1890, 136, 'Rungan');
INSERT INTO `subdistricts` VALUES (1891, 1891, 136, 'Rungan Barat');
INSERT INTO `subdistricts` VALUES (1892, 1892, 136, 'Rungan Hulu');
INSERT INTO `subdistricts` VALUES (1893, 1893, 136, 'Sepang (Sepang Simin)');
INSERT INTO `subdistricts` VALUES (1894, 1894, 136, 'Tewah');
INSERT INTO `subdistricts` VALUES (1895, 1895, 137, 'Gunungsitoli');
INSERT INTO `subdistricts` VALUES (1896, 1896, 137, 'Gunungsitoli Alo\'oa');
INSERT INTO `subdistricts` VALUES (1897, 1897, 137, 'Gunungsitoli Barat');
INSERT INTO `subdistricts` VALUES (1898, 1898, 137, 'Gunungsitoli Idanoi');
INSERT INTO `subdistricts` VALUES (1899, 1899, 137, 'Gunungsitoli Selatan');
INSERT INTO `subdistricts` VALUES (1900, 1900, 137, 'Gunungsitoli Utara');
INSERT INTO `subdistricts` VALUES (1901, 1901, 138, 'Ibu');
INSERT INTO `subdistricts` VALUES (1902, 1902, 138, 'Ibu Selatan');
INSERT INTO `subdistricts` VALUES (1903, 1903, 138, 'Ibu Utara');
INSERT INTO `subdistricts` VALUES (1904, 1904, 138, 'Jailolo');
INSERT INTO `subdistricts` VALUES (1905, 1905, 138, 'Jailolo Selatan');
INSERT INTO `subdistricts` VALUES (1906, 1906, 138, 'Loloda');
INSERT INTO `subdistricts` VALUES (1907, 1907, 138, 'Sahu');
INSERT INTO `subdistricts` VALUES (1908, 1908, 138, 'Sahu Timur');
INSERT INTO `subdistricts` VALUES (1909, 1909, 139, 'Bacan');
INSERT INTO `subdistricts` VALUES (1910, 1910, 139, 'Bacan Barat');
INSERT INTO `subdistricts` VALUES (1911, 1911, 139, 'Bacan Barat Utara');
INSERT INTO `subdistricts` VALUES (1912, 1912, 139, 'Bacan Selatan');
INSERT INTO `subdistricts` VALUES (1913, 1913, 139, 'Bacan Timur');
INSERT INTO `subdistricts` VALUES (1914, 1914, 139, 'Bacan Timur Selatan');
INSERT INTO `subdistricts` VALUES (1915, 1915, 139, 'Bacan Timur Tengah');
INSERT INTO `subdistricts` VALUES (1916, 1916, 139, 'Gane Barat');
INSERT INTO `subdistricts` VALUES (1917, 1917, 139, 'Gane Barat Selatan');
INSERT INTO `subdistricts` VALUES (1918, 1918, 139, 'Gane Barat Utara');
INSERT INTO `subdistricts` VALUES (1919, 1919, 139, 'Gane Timur');
INSERT INTO `subdistricts` VALUES (1920, 1920, 139, 'Gane Timur Selatan');
INSERT INTO `subdistricts` VALUES (1921, 1921, 139, 'Gane Timur Tengah');
INSERT INTO `subdistricts` VALUES (1922, 1922, 139, 'Kasiruta Barat');
INSERT INTO `subdistricts` VALUES (1923, 1923, 139, 'Kasiruta Timur');
INSERT INTO `subdistricts` VALUES (1924, 1924, 139, 'Kayoa');
INSERT INTO `subdistricts` VALUES (1925, 1925, 139, 'Kayoa Barat');
INSERT INTO `subdistricts` VALUES (1926, 1926, 139, 'Kayoa Selatan');
INSERT INTO `subdistricts` VALUES (1927, 1927, 139, 'Kayoa Utara');
INSERT INTO `subdistricts` VALUES (1928, 1928, 139, 'Kepulauan Botanglomang');
INSERT INTO `subdistricts` VALUES (1929, 1929, 139, 'Kepulauan Joronga');
INSERT INTO `subdistricts` VALUES (1930, 1930, 139, 'Makian (Pulau Makian)');
INSERT INTO `subdistricts` VALUES (1931, 1931, 139, 'Makian Barat (Pulau Makian)');
INSERT INTO `subdistricts` VALUES (1932, 1932, 139, 'Mandioli Selatan');
INSERT INTO `subdistricts` VALUES (1933, 1933, 139, 'Mandioli Utara');
INSERT INTO `subdistricts` VALUES (1934, 1934, 139, 'Obi');
INSERT INTO `subdistricts` VALUES (1935, 1935, 139, 'Obi Barat');
INSERT INTO `subdistricts` VALUES (1936, 1936, 139, 'Obi Selatan');
INSERT INTO `subdistricts` VALUES (1937, 1937, 139, 'Obi Timur');
INSERT INTO `subdistricts` VALUES (1938, 1938, 139, 'Obi Utara');
INSERT INTO `subdistricts` VALUES (1939, 1939, 140, 'Patani');
INSERT INTO `subdistricts` VALUES (1940, 1940, 140, 'Patani Barat');
INSERT INTO `subdistricts` VALUES (1941, 1941, 140, 'Patani Utara');
INSERT INTO `subdistricts` VALUES (1942, 1942, 140, 'Pulau Gebe');
INSERT INTO `subdistricts` VALUES (1943, 1943, 140, 'Weda');
INSERT INTO `subdistricts` VALUES (1944, 1944, 140, 'Weda Selatan');
INSERT INTO `subdistricts` VALUES (1945, 1945, 140, 'Weda Tengah');
INSERT INTO `subdistricts` VALUES (1946, 1946, 140, 'Weda Utara');
INSERT INTO `subdistricts` VALUES (1947, 1947, 141, 'Kota Maba');
INSERT INTO `subdistricts` VALUES (1948, 1948, 141, 'Maba');
INSERT INTO `subdistricts` VALUES (1949, 1949, 141, 'Maba Selatan');
INSERT INTO `subdistricts` VALUES (1950, 1950, 141, 'Maba Tengah');
INSERT INTO `subdistricts` VALUES (1951, 1951, 141, 'Maba Utara');
INSERT INTO `subdistricts` VALUES (1952, 1952, 141, 'Wasile');
INSERT INTO `subdistricts` VALUES (1953, 1953, 141, 'Wasile Selatan');
INSERT INTO `subdistricts` VALUES (1954, 1954, 141, 'Wasile Tengah');
INSERT INTO `subdistricts` VALUES (1955, 1955, 141, 'Wasile Timur');
INSERT INTO `subdistricts` VALUES (1956, 1956, 141, 'Wasile Utara');
INSERT INTO `subdistricts` VALUES (1957, 1957, 142, 'Galela');
INSERT INTO `subdistricts` VALUES (1958, 1958, 142, 'Galela Barat');
INSERT INTO `subdistricts` VALUES (1959, 1959, 142, 'Galela Selatan');
INSERT INTO `subdistricts` VALUES (1960, 1960, 142, 'Galela Utara');
INSERT INTO `subdistricts` VALUES (1961, 1961, 142, 'Kao');
INSERT INTO `subdistricts` VALUES (1962, 1962, 142, 'Kao Barat');
INSERT INTO `subdistricts` VALUES (1963, 1963, 142, 'Kao Teluk');
INSERT INTO `subdistricts` VALUES (1964, 1964, 142, 'Kao Utara');
INSERT INTO `subdistricts` VALUES (1965, 1965, 142, 'Loloda Kepulauan');
INSERT INTO `subdistricts` VALUES (1966, 1966, 142, 'Loloda Utara');
INSERT INTO `subdistricts` VALUES (1967, 1967, 142, 'Malifut');
INSERT INTO `subdistricts` VALUES (1968, 1968, 142, 'Tobelo');
INSERT INTO `subdistricts` VALUES (1969, 1969, 142, 'Tobelo Barat');
INSERT INTO `subdistricts` VALUES (1970, 1970, 142, 'Tobelo Selatan');
INSERT INTO `subdistricts` VALUES (1971, 1971, 142, 'Tobelo Tengah');
INSERT INTO `subdistricts` VALUES (1972, 1972, 142, 'Tobelo Timur');
INSERT INTO `subdistricts` VALUES (1973, 1973, 142, 'Tobelo Utara');
INSERT INTO `subdistricts` VALUES (1974, 1974, 143, 'Angkinang');
INSERT INTO `subdistricts` VALUES (1975, 1975, 143, 'Daha Barat');
INSERT INTO `subdistricts` VALUES (1976, 1976, 143, 'Daha Selatan');
INSERT INTO `subdistricts` VALUES (1977, 1977, 143, 'Daha Utara');
INSERT INTO `subdistricts` VALUES (1978, 1978, 143, 'Kalumpang (Kelumpang)');
INSERT INTO `subdistricts` VALUES (1979, 1979, 143, 'Kandangan');
INSERT INTO `subdistricts` VALUES (1980, 1980, 143, 'Loksado');
INSERT INTO `subdistricts` VALUES (1981, 1981, 143, 'Padang Batung');
INSERT INTO `subdistricts` VALUES (1982, 1982, 143, 'Simpur');
INSERT INTO `subdistricts` VALUES (1983, 1983, 143, 'Sungai Raya');
INSERT INTO `subdistricts` VALUES (1984, 1984, 143, 'Telaga Langsat');
INSERT INTO `subdistricts` VALUES (1985, 1985, 144, 'Barabai');
INSERT INTO `subdistricts` VALUES (1986, 1986, 144, 'Batang Alai Selatan');
INSERT INTO `subdistricts` VALUES (1987, 1987, 144, 'Batang Alai Timur');
INSERT INTO `subdistricts` VALUES (1988, 1988, 144, 'Batang Alai Utara');
INSERT INTO `subdistricts` VALUES (1989, 1989, 144, 'Batu Benawa');
INSERT INTO `subdistricts` VALUES (1990, 1990, 144, 'Hantakan');
INSERT INTO `subdistricts` VALUES (1991, 1991, 144, 'Haruyan');
INSERT INTO `subdistricts` VALUES (1992, 1992, 144, 'Labuan Amas Selatan');
INSERT INTO `subdistricts` VALUES (1993, 1993, 144, 'Labuan Amas Utara');
INSERT INTO `subdistricts` VALUES (1994, 1994, 144, 'Limpasu');
INSERT INTO `subdistricts` VALUES (1995, 1995, 144, 'Pandawan');
INSERT INTO `subdistricts` VALUES (1996, 1996, 145, 'Amuntai Selatan');
INSERT INTO `subdistricts` VALUES (1997, 1997, 145, 'Amuntai Tengah');
INSERT INTO `subdistricts` VALUES (1998, 1998, 145, 'Amuntai Utara');
INSERT INTO `subdistricts` VALUES (1999, 1999, 145, 'Babirik');
INSERT INTO `subdistricts` VALUES (2000, 2000, 145, 'Banjang');
INSERT INTO `subdistricts` VALUES (2001, 2001, 145, 'Danau Panggang');
INSERT INTO `subdistricts` VALUES (2002, 2002, 145, 'Haur Gading');
INSERT INTO `subdistricts` VALUES (2003, 2003, 145, 'Paminggir');
INSERT INTO `subdistricts` VALUES (2004, 2004, 145, 'Sungai Pandan');
INSERT INTO `subdistricts` VALUES (2005, 2005, 145, 'Sungai Tabukan');
INSERT INTO `subdistricts` VALUES (2006, 2006, 146, 'Bakti Raja');
INSERT INTO `subdistricts` VALUES (2007, 2007, 146, 'Dolok Sanggul');
INSERT INTO `subdistricts` VALUES (2008, 2008, 146, 'Lintong Nihuta');
INSERT INTO `subdistricts` VALUES (2009, 2009, 146, 'Onan Ganjang');
INSERT INTO `subdistricts` VALUES (2010, 2010, 146, 'Pakkat');
INSERT INTO `subdistricts` VALUES (2011, 2011, 146, 'Paranginan');
INSERT INTO `subdistricts` VALUES (2012, 2012, 146, 'Parlilitan');
INSERT INTO `subdistricts` VALUES (2013, 2013, 146, 'Pollung');
INSERT INTO `subdistricts` VALUES (2014, 2014, 146, 'Sijama Polang');
INSERT INTO `subdistricts` VALUES (2015, 2015, 146, 'Tara Bintang');
INSERT INTO `subdistricts` VALUES (2016, 2016, 147, 'Batang Tuaka');
INSERT INTO `subdistricts` VALUES (2017, 2017, 147, 'Concong');
INSERT INTO `subdistricts` VALUES (2018, 2018, 147, 'Enok');
INSERT INTO `subdistricts` VALUES (2019, 2019, 147, 'Gaung');
INSERT INTO `subdistricts` VALUES (2020, 2020, 147, 'Gaung Anak Serka');
INSERT INTO `subdistricts` VALUES (2021, 2021, 147, 'Kateman');
INSERT INTO `subdistricts` VALUES (2022, 2022, 147, 'Kempas');
INSERT INTO `subdistricts` VALUES (2023, 2023, 147, 'Kemuning');
INSERT INTO `subdistricts` VALUES (2024, 2024, 147, 'Keritang');
INSERT INTO `subdistricts` VALUES (2025, 2025, 147, 'Kuala Indragiri');
INSERT INTO `subdistricts` VALUES (2026, 2026, 147, 'Mandah');
INSERT INTO `subdistricts` VALUES (2027, 2027, 147, 'Pelangiran');
INSERT INTO `subdistricts` VALUES (2028, 2028, 147, 'Pulau Burung');
INSERT INTO `subdistricts` VALUES (2029, 2029, 147, 'Reteh');
INSERT INTO `subdistricts` VALUES (2030, 2030, 147, 'Sungai Batang');
INSERT INTO `subdistricts` VALUES (2031, 2031, 147, 'Tanah Merah');
INSERT INTO `subdistricts` VALUES (2032, 2032, 147, 'Teluk Belengkong');
INSERT INTO `subdistricts` VALUES (2033, 2033, 147, 'Tembilahan');
INSERT INTO `subdistricts` VALUES (2034, 2034, 147, 'Tembilahan Hulu');
INSERT INTO `subdistricts` VALUES (2035, 2035, 147, 'Tempuling');
INSERT INTO `subdistricts` VALUES (2036, 2036, 148, 'Batang Cenaku');
INSERT INTO `subdistricts` VALUES (2037, 2037, 148, 'Batang Gansal');
INSERT INTO `subdistricts` VALUES (2038, 2038, 148, 'Batang Peranap');
INSERT INTO `subdistricts` VALUES (2039, 2039, 148, 'Kelayang');
INSERT INTO `subdistricts` VALUES (2040, 2040, 148, 'Kuala Cenaku');
INSERT INTO `subdistricts` VALUES (2041, 2041, 148, 'Lirik');
INSERT INTO `subdistricts` VALUES (2042, 2042, 148, 'Lubuk Batu Jaya');
INSERT INTO `subdistricts` VALUES (2043, 2043, 148, 'Pasir Penyu');
INSERT INTO `subdistricts` VALUES (2044, 2044, 148, 'Peranap');
INSERT INTO `subdistricts` VALUES (2045, 2045, 148, 'Rakit Kulim');
INSERT INTO `subdistricts` VALUES (2046, 2046, 148, 'Rengat');
INSERT INTO `subdistricts` VALUES (2047, 2047, 148, 'Rengat Barat');
INSERT INTO `subdistricts` VALUES (2048, 2048, 148, 'Seberida');
INSERT INTO `subdistricts` VALUES (2049, 2049, 148, 'Sungai Lala');
INSERT INTO `subdistricts` VALUES (2050, 2050, 149, 'Anjatan');
INSERT INTO `subdistricts` VALUES (2051, 2051, 149, 'Arahan');
INSERT INTO `subdistricts` VALUES (2052, 2052, 149, 'Balongan');
INSERT INTO `subdistricts` VALUES (2053, 2053, 149, 'Bangodua');
INSERT INTO `subdistricts` VALUES (2054, 2054, 149, 'Bongas');
INSERT INTO `subdistricts` VALUES (2055, 2055, 149, 'Cantigi');
INSERT INTO `subdistricts` VALUES (2056, 2056, 149, 'Cikedung');
INSERT INTO `subdistricts` VALUES (2057, 2057, 149, 'Gabuswetan');
INSERT INTO `subdistricts` VALUES (2058, 2058, 149, 'Gantar');
INSERT INTO `subdistricts` VALUES (2059, 2059, 149, 'Haurgeulis');
INSERT INTO `subdistricts` VALUES (2060, 2060, 149, 'Indramayu');
INSERT INTO `subdistricts` VALUES (2061, 2061, 149, 'Jatibarang');
INSERT INTO `subdistricts` VALUES (2062, 2062, 149, 'Juntinyuat');
INSERT INTO `subdistricts` VALUES (2063, 2063, 149, 'Kandanghaur');
INSERT INTO `subdistricts` VALUES (2064, 2064, 149, 'Karangampel');
INSERT INTO `subdistricts` VALUES (2065, 2065, 149, 'Kedokan Bunder');
INSERT INTO `subdistricts` VALUES (2066, 2066, 149, 'Kertasemaya');
INSERT INTO `subdistricts` VALUES (2067, 2067, 149, 'Krangkeng');
INSERT INTO `subdistricts` VALUES (2068, 2068, 149, 'Kroya');
INSERT INTO `subdistricts` VALUES (2069, 2069, 149, 'Lelea');
INSERT INTO `subdistricts` VALUES (2070, 2070, 149, 'Lohbener');
INSERT INTO `subdistricts` VALUES (2071, 2071, 149, 'Losarang');
INSERT INTO `subdistricts` VALUES (2072, 2072, 149, 'Pasekan');
INSERT INTO `subdistricts` VALUES (2073, 2073, 149, 'Patrol');
INSERT INTO `subdistricts` VALUES (2074, 2074, 149, 'Sindang');
INSERT INTO `subdistricts` VALUES (2075, 2075, 149, 'Sliyeg');
INSERT INTO `subdistricts` VALUES (2076, 2076, 149, 'Sukagumiwang');
INSERT INTO `subdistricts` VALUES (2077, 2077, 149, 'Sukra');
INSERT INTO `subdistricts` VALUES (2078, 2078, 149, 'Trisi/Terisi');
INSERT INTO `subdistricts` VALUES (2079, 2079, 149, 'Tukdana');
INSERT INTO `subdistricts` VALUES (2080, 2080, 149, 'Widasari');
INSERT INTO `subdistricts` VALUES (2081, 2081, 150, 'Agisiga');
INSERT INTO `subdistricts` VALUES (2082, 2082, 150, 'Biandoga');
INSERT INTO `subdistricts` VALUES (2083, 2083, 150, 'Hitadipa');
INSERT INTO `subdistricts` VALUES (2084, 2084, 150, 'Homeo (Homeyo)');
INSERT INTO `subdistricts` VALUES (2085, 2085, 150, 'Sugapa');
INSERT INTO `subdistricts` VALUES (2086, 2086, 150, 'Wandai');
INSERT INTO `subdistricts` VALUES (2087, 2087, 151, 'Cengkareng');
INSERT INTO `subdistricts` VALUES (2088, 2088, 151, 'Grogol Petamburan');
INSERT INTO `subdistricts` VALUES (2089, 2089, 151, 'Kalideres');
INSERT INTO `subdistricts` VALUES (2090, 2090, 151, 'Kebon Jeruk');
INSERT INTO `subdistricts` VALUES (2091, 2091, 151, 'Kembangan');
INSERT INTO `subdistricts` VALUES (2092, 2092, 151, 'Palmerah');
INSERT INTO `subdistricts` VALUES (2093, 2093, 151, 'Taman Sari');
INSERT INTO `subdistricts` VALUES (2094, 2094, 151, 'Tambora');
INSERT INTO `subdistricts` VALUES (2095, 2095, 152, 'Cempaka Putih');
INSERT INTO `subdistricts` VALUES (2096, 2096, 152, 'Gambir');
INSERT INTO `subdistricts` VALUES (2097, 2097, 152, 'Johar Baru');
INSERT INTO `subdistricts` VALUES (2098, 2098, 152, 'Kemayoran');
INSERT INTO `subdistricts` VALUES (2099, 2099, 152, 'Menteng');
INSERT INTO `subdistricts` VALUES (2100, 2100, 152, 'Sawah Besar');
INSERT INTO `subdistricts` VALUES (2101, 2101, 152, 'Senen');
INSERT INTO `subdistricts` VALUES (2102, 2102, 152, 'Tanah Abang');
INSERT INTO `subdistricts` VALUES (2103, 2103, 153, 'Cilandak');
INSERT INTO `subdistricts` VALUES (2104, 2104, 153, 'Jagakarsa');
INSERT INTO `subdistricts` VALUES (2105, 2105, 153, 'Kebayoran Baru');
INSERT INTO `subdistricts` VALUES (2106, 2106, 153, 'Kebayoran Lama');
INSERT INTO `subdistricts` VALUES (2107, 2107, 153, 'Mampang Prapatan');
INSERT INTO `subdistricts` VALUES (2108, 2108, 153, 'Pancoran');
INSERT INTO `subdistricts` VALUES (2109, 2109, 153, 'Pasar Minggu');
INSERT INTO `subdistricts` VALUES (2110, 2110, 153, 'Pesanggrahan');
INSERT INTO `subdistricts` VALUES (2111, 2111, 153, 'Setia Budi');
INSERT INTO `subdistricts` VALUES (2112, 2112, 153, 'Tebet');
INSERT INTO `subdistricts` VALUES (2113, 2113, 154, 'Cakung');
INSERT INTO `subdistricts` VALUES (2114, 2114, 154, 'Cipayung');
INSERT INTO `subdistricts` VALUES (2115, 2115, 154, 'Ciracas');
INSERT INTO `subdistricts` VALUES (2116, 2116, 154, 'Duren Sawit');
INSERT INTO `subdistricts` VALUES (2117, 2117, 154, 'Jatinegara');
INSERT INTO `subdistricts` VALUES (2118, 2118, 154, 'Kramat Jati');
INSERT INTO `subdistricts` VALUES (2119, 2119, 154, 'Makasar');
INSERT INTO `subdistricts` VALUES (2120, 2120, 154, 'Matraman');
INSERT INTO `subdistricts` VALUES (2121, 2121, 154, 'Pasar Rebo');
INSERT INTO `subdistricts` VALUES (2122, 2122, 154, 'Pulo Gadung');
INSERT INTO `subdistricts` VALUES (2123, 2123, 155, 'Cilincing');
INSERT INTO `subdistricts` VALUES (2124, 2124, 155, 'Kelapa Gading');
INSERT INTO `subdistricts` VALUES (2125, 2125, 155, 'Koja');
INSERT INTO `subdistricts` VALUES (2126, 2126, 155, 'Pademangan');
INSERT INTO `subdistricts` VALUES (2127, 2127, 155, 'Penjaringan');
INSERT INTO `subdistricts` VALUES (2128, 2128, 155, 'Tanjung Priok');
INSERT INTO `subdistricts` VALUES (2129, 2129, 156, 'Danau Teluk');
INSERT INTO `subdistricts` VALUES (2130, 2130, 156, 'Jambi Selatan');
INSERT INTO `subdistricts` VALUES (2131, 2131, 156, 'Jambi Timur');
INSERT INTO `subdistricts` VALUES (2132, 2132, 156, 'Jelutung');
INSERT INTO `subdistricts` VALUES (2133, 2133, 156, 'Kota Baru');
INSERT INTO `subdistricts` VALUES (2134, 2134, 156, 'Pasar Jambi');
INSERT INTO `subdistricts` VALUES (2135, 2135, 156, 'Pelayangan');
INSERT INTO `subdistricts` VALUES (2136, 2136, 156, 'Telanaipura');
INSERT INTO `subdistricts` VALUES (2137, 2137, 157, 'Airu');
INSERT INTO `subdistricts` VALUES (2138, 2138, 157, 'Demta');
INSERT INTO `subdistricts` VALUES (2139, 2139, 157, 'Depapre');
INSERT INTO `subdistricts` VALUES (2140, 2140, 157, 'Ebungfau (Ebungfa)');
INSERT INTO `subdistricts` VALUES (2141, 2141, 157, 'Gresi Selatan');
INSERT INTO `subdistricts` VALUES (2142, 2142, 157, 'Kaureh');
INSERT INTO `subdistricts` VALUES (2143, 2143, 157, 'Kemtuk');
INSERT INTO `subdistricts` VALUES (2144, 2144, 157, 'Kemtuk Gresi');
INSERT INTO `subdistricts` VALUES (2145, 2145, 157, 'Nambluong');
INSERT INTO `subdistricts` VALUES (2146, 2146, 157, 'Nimbokrang');
INSERT INTO `subdistricts` VALUES (2147, 2147, 157, 'Nimboran');
INSERT INTO `subdistricts` VALUES (2148, 2148, 157, 'Ravenirara');
INSERT INTO `subdistricts` VALUES (2149, 2149, 157, 'Sentani');
INSERT INTO `subdistricts` VALUES (2150, 2150, 157, 'Sentani Barat');
INSERT INTO `subdistricts` VALUES (2151, 2151, 157, 'Sentani Timur');
INSERT INTO `subdistricts` VALUES (2152, 2152, 157, 'Unurum Guay');
INSERT INTO `subdistricts` VALUES (2153, 2153, 157, 'Waibu');
INSERT INTO `subdistricts` VALUES (2154, 2154, 157, 'Yapsi');
INSERT INTO `subdistricts` VALUES (2155, 2155, 157, 'Yokari');
INSERT INTO `subdistricts` VALUES (2156, 2156, 158, 'Abepura');
INSERT INTO `subdistricts` VALUES (2157, 2157, 158, 'Heram');
INSERT INTO `subdistricts` VALUES (2158, 2158, 158, 'Jayapura Selatan');
INSERT INTO `subdistricts` VALUES (2159, 2159, 158, 'Jayapura Utara');
INSERT INTO `subdistricts` VALUES (2160, 2160, 158, 'Muara Tami');
INSERT INTO `subdistricts` VALUES (2161, 2161, 159, 'Asologaima (Asalogaima)');
INSERT INTO `subdistricts` VALUES (2162, 2162, 159, 'Asolokobal');
INSERT INTO `subdistricts` VALUES (2163, 2163, 159, 'Asotipo');
INSERT INTO `subdistricts` VALUES (2164, 2164, 159, 'Bolakme');
INSERT INTO `subdistricts` VALUES (2165, 2165, 159, 'Bpiri');
INSERT INTO `subdistricts` VALUES (2166, 2166, 159, 'Bugi');
INSERT INTO `subdistricts` VALUES (2167, 2167, 159, 'Hubikiak');
INSERT INTO `subdistricts` VALUES (2168, 2168, 159, 'Hubikosi (Hobikosi)');
INSERT INTO `subdistricts` VALUES (2169, 2169, 159, 'Ibele');
INSERT INTO `subdistricts` VALUES (2170, 2170, 159, 'Itlay Hisage');
INSERT INTO `subdistricts` VALUES (2171, 2171, 159, 'Koragi');
INSERT INTO `subdistricts` VALUES (2172, 2172, 159, 'Kurulu');
INSERT INTO `subdistricts` VALUES (2173, 2173, 159, 'Libarek');
INSERT INTO `subdistricts` VALUES (2174, 2174, 159, 'Maima');
INSERT INTO `subdistricts` VALUES (2175, 2175, 159, 'Molagalome');
INSERT INTO `subdistricts` VALUES (2176, 2176, 159, 'Muliama');
INSERT INTO `subdistricts` VALUES (2177, 2177, 159, 'Musatfak');
INSERT INTO `subdistricts` VALUES (2178, 2178, 159, 'Napua');
INSERT INTO `subdistricts` VALUES (2179, 2179, 159, 'Pelebaga');
INSERT INTO `subdistricts` VALUES (2180, 2180, 159, 'Piramid');
INSERT INTO `subdistricts` VALUES (2181, 2181, 159, 'Pisugi');
INSERT INTO `subdistricts` VALUES (2182, 2182, 159, 'Popugoba');
INSERT INTO `subdistricts` VALUES (2183, 2183, 159, 'Siepkosi');
INSERT INTO `subdistricts` VALUES (2184, 2184, 159, 'Silo Karno Doga');
INSERT INTO `subdistricts` VALUES (2185, 2185, 159, 'Taelarek');
INSERT INTO `subdistricts` VALUES (2186, 2186, 159, 'Tagime');
INSERT INTO `subdistricts` VALUES (2187, 2187, 159, 'Tagineri');
INSERT INTO `subdistricts` VALUES (2188, 2188, 159, 'Trikora');
INSERT INTO `subdistricts` VALUES (2189, 2189, 159, 'Usilimo');
INSERT INTO `subdistricts` VALUES (2190, 2190, 159, 'Wadangku');
INSERT INTO `subdistricts` VALUES (2191, 2191, 159, 'Walaik');
INSERT INTO `subdistricts` VALUES (2192, 2192, 159, 'Walelagama');
INSERT INTO `subdistricts` VALUES (2193, 2193, 159, 'Wame');
INSERT INTO `subdistricts` VALUES (2194, 2194, 159, 'Wamena');
INSERT INTO `subdistricts` VALUES (2195, 2195, 159, 'Welesi');
INSERT INTO `subdistricts` VALUES (2196, 2196, 159, 'Wesaput');
INSERT INTO `subdistricts` VALUES (2197, 2197, 159, 'Wita Waya');
INSERT INTO `subdistricts` VALUES (2198, 2198, 159, 'Wollo (Wolo)');
INSERT INTO `subdistricts` VALUES (2199, 2199, 159, 'Wouma');
INSERT INTO `subdistricts` VALUES (2200, 2200, 159, 'Yalengga');
INSERT INTO `subdistricts` VALUES (2201, 2201, 160, 'Ajung');
INSERT INTO `subdistricts` VALUES (2202, 2202, 160, 'Ambulu');
INSERT INTO `subdistricts` VALUES (2203, 2203, 160, 'Arjasa');
INSERT INTO `subdistricts` VALUES (2204, 2204, 160, 'Balung');
INSERT INTO `subdistricts` VALUES (2205, 2205, 160, 'Bangsalsari');
INSERT INTO `subdistricts` VALUES (2206, 2206, 160, 'Gumuk Mas');
INSERT INTO `subdistricts` VALUES (2207, 2207, 160, 'Jelbuk');
INSERT INTO `subdistricts` VALUES (2208, 2208, 160, 'Jenggawah');
INSERT INTO `subdistricts` VALUES (2209, 2209, 160, 'Jombang');
INSERT INTO `subdistricts` VALUES (2210, 2210, 160, 'Kalisat');
INSERT INTO `subdistricts` VALUES (2211, 2211, 160, 'Kaliwates');
INSERT INTO `subdistricts` VALUES (2212, 2212, 160, 'Kencong');
INSERT INTO `subdistricts` VALUES (2213, 2213, 160, 'Ledokombo');
INSERT INTO `subdistricts` VALUES (2214, 2214, 160, 'Mayang');
INSERT INTO `subdistricts` VALUES (2215, 2215, 160, 'Mumbulsari');
INSERT INTO `subdistricts` VALUES (2216, 2216, 160, 'Pakusari');
INSERT INTO `subdistricts` VALUES (2217, 2217, 160, 'Panti');
INSERT INTO `subdistricts` VALUES (2218, 2218, 160, 'Patrang');
INSERT INTO `subdistricts` VALUES (2219, 2219, 160, 'Puger');
INSERT INTO `subdistricts` VALUES (2220, 2220, 160, 'Rambipuji');
INSERT INTO `subdistricts` VALUES (2221, 2221, 160, 'Semboro');
INSERT INTO `subdistricts` VALUES (2222, 2222, 160, 'Silo');
INSERT INTO `subdistricts` VALUES (2223, 2223, 160, 'Sukorambi');
INSERT INTO `subdistricts` VALUES (2224, 2224, 160, 'Sukowono');
INSERT INTO `subdistricts` VALUES (2225, 2225, 160, 'Sumber Baru');
INSERT INTO `subdistricts` VALUES (2226, 2226, 160, 'Sumber Jambe');
INSERT INTO `subdistricts` VALUES (2227, 2227, 160, 'Sumber Sari');
INSERT INTO `subdistricts` VALUES (2228, 2228, 160, 'Tanggul');
INSERT INTO `subdistricts` VALUES (2229, 2229, 160, 'Tempurejo');
INSERT INTO `subdistricts` VALUES (2230, 2230, 160, 'Umbulsari');
INSERT INTO `subdistricts` VALUES (2231, 2231, 160, 'Wuluhan');
INSERT INTO `subdistricts` VALUES (2232, 2232, 161, 'Jembrana');
INSERT INTO `subdistricts` VALUES (2233, 2233, 161, 'Melaya');
INSERT INTO `subdistricts` VALUES (2234, 2234, 161, 'Mendoyo');
INSERT INTO `subdistricts` VALUES (2235, 2235, 161, 'Negara');
INSERT INTO `subdistricts` VALUES (2236, 2236, 161, 'Pekutatan');
INSERT INTO `subdistricts` VALUES (2237, 2237, 162, 'Arungkeke');
INSERT INTO `subdistricts` VALUES (2238, 2238, 162, 'Bangkala');
INSERT INTO `subdistricts` VALUES (2239, 2239, 162, 'Bangkala Barat');
INSERT INTO `subdistricts` VALUES (2240, 2240, 162, 'Batang');
INSERT INTO `subdistricts` VALUES (2241, 2241, 162, 'Binamu');
INSERT INTO `subdistricts` VALUES (2242, 2242, 162, 'Bontoramba');
INSERT INTO `subdistricts` VALUES (2243, 2243, 162, 'Kelara');
INSERT INTO `subdistricts` VALUES (2244, 2244, 162, 'Rumbia');
INSERT INTO `subdistricts` VALUES (2245, 2245, 162, 'Tamalatea');
INSERT INTO `subdistricts` VALUES (2246, 2246, 162, 'Tarowang');
INSERT INTO `subdistricts` VALUES (2247, 2247, 162, 'Turatea');
INSERT INTO `subdistricts` VALUES (2248, 2248, 163, 'Bangsri');
INSERT INTO `subdistricts` VALUES (2249, 2249, 163, 'Batealit');
INSERT INTO `subdistricts` VALUES (2250, 2250, 163, 'Donorojo');
INSERT INTO `subdistricts` VALUES (2251, 2251, 163, 'Jepara');
INSERT INTO `subdistricts` VALUES (2252, 2252, 163, 'Kalinyamatan');
INSERT INTO `subdistricts` VALUES (2253, 2253, 163, 'Karimunjawa');
INSERT INTO `subdistricts` VALUES (2254, 2254, 163, 'Kedung');
INSERT INTO `subdistricts` VALUES (2255, 2255, 163, 'Keling');
INSERT INTO `subdistricts` VALUES (2256, 2256, 163, 'Kembang');
INSERT INTO `subdistricts` VALUES (2257, 2257, 163, 'Mayong');
INSERT INTO `subdistricts` VALUES (2258, 2258, 163, 'Mlonggo');
INSERT INTO `subdistricts` VALUES (2259, 2259, 163, 'Nalumsari');
INSERT INTO `subdistricts` VALUES (2260, 2260, 163, 'Pakis Aji');
INSERT INTO `subdistricts` VALUES (2261, 2261, 163, 'Pecangaan');
INSERT INTO `subdistricts` VALUES (2262, 2262, 163, 'Tahunan');
INSERT INTO `subdistricts` VALUES (2263, 2263, 163, 'Welahan');
INSERT INTO `subdistricts` VALUES (2264, 2264, 164, 'Bandar Kedung Mulyo');
INSERT INTO `subdistricts` VALUES (2265, 2265, 164, 'Bareng');
INSERT INTO `subdistricts` VALUES (2266, 2266, 164, 'Diwek');
INSERT INTO `subdistricts` VALUES (2267, 2267, 164, 'Gudo');
INSERT INTO `subdistricts` VALUES (2268, 2268, 164, 'Jogoroto');
INSERT INTO `subdistricts` VALUES (2269, 2269, 164, 'Jombang');
INSERT INTO `subdistricts` VALUES (2270, 2270, 164, 'Kabuh');
INSERT INTO `subdistricts` VALUES (2271, 2271, 164, 'Kesamben');
INSERT INTO `subdistricts` VALUES (2272, 2272, 164, 'Kudu');
INSERT INTO `subdistricts` VALUES (2273, 2273, 164, 'Megaluh');
INSERT INTO `subdistricts` VALUES (2274, 2274, 164, 'Mojoagung');
INSERT INTO `subdistricts` VALUES (2275, 2275, 164, 'Mojowarno');
INSERT INTO `subdistricts` VALUES (2276, 2276, 164, 'Ngoro');
INSERT INTO `subdistricts` VALUES (2277, 2277, 164, 'Ngusikan');
INSERT INTO `subdistricts` VALUES (2278, 2278, 164, 'Perak');
INSERT INTO `subdistricts` VALUES (2279, 2279, 164, 'Peterongan');
INSERT INTO `subdistricts` VALUES (2280, 2280, 164, 'Plandaan');
INSERT INTO `subdistricts` VALUES (2281, 2281, 164, 'Ploso');
INSERT INTO `subdistricts` VALUES (2282, 2282, 164, 'Sumobito');
INSERT INTO `subdistricts` VALUES (2283, 2283, 164, 'Tembelang');
INSERT INTO `subdistricts` VALUES (2284, 2284, 164, 'Wonosalam');
INSERT INTO `subdistricts` VALUES (2285, 2285, 165, 'Buruway');
INSERT INTO `subdistricts` VALUES (2286, 2286, 165, 'Kaimana');
INSERT INTO `subdistricts` VALUES (2287, 2287, 165, 'Kambraw (Kamberau)');
INSERT INTO `subdistricts` VALUES (2288, 2288, 165, 'Teluk Arguni Atas');
INSERT INTO `subdistricts` VALUES (2289, 2289, 165, 'Teluk Arguni Bawah (Yerusi)');
INSERT INTO `subdistricts` VALUES (2290, 2290, 165, 'Teluk Etna');
INSERT INTO `subdistricts` VALUES (2291, 2291, 165, 'Yamor');
INSERT INTO `subdistricts` VALUES (2292, 2292, 166, 'Bangkinang');
INSERT INTO `subdistricts` VALUES (2293, 2293, 166, 'Bangkinang Seberang');
INSERT INTO `subdistricts` VALUES (2294, 2294, 166, 'Gunung Sahilan');
INSERT INTO `subdistricts` VALUES (2295, 2295, 166, 'Kampar');
INSERT INTO `subdistricts` VALUES (2296, 2296, 166, 'Kampar Kiri');
INSERT INTO `subdistricts` VALUES (2297, 2297, 166, 'Kampar Kiri Hilir');
INSERT INTO `subdistricts` VALUES (2298, 2298, 166, 'Kampar Kiri Hulu');
INSERT INTO `subdistricts` VALUES (2299, 2299, 166, 'Kampar Kiri Tengah');
INSERT INTO `subdistricts` VALUES (2300, 2300, 166, 'Kampar Timur');
INSERT INTO `subdistricts` VALUES (2301, 2301, 166, 'Kampar Utara');
INSERT INTO `subdistricts` VALUES (2302, 2302, 166, 'Koto Kampar Hulu');
INSERT INTO `subdistricts` VALUES (2303, 2303, 166, 'Kuok (Bangkinang Barat)');
INSERT INTO `subdistricts` VALUES (2304, 2304, 166, 'Perhentian Raja');
INSERT INTO `subdistricts` VALUES (2305, 2305, 166, 'Rumbio Jaya');
INSERT INTO `subdistricts` VALUES (2306, 2306, 166, 'Salo');
INSERT INTO `subdistricts` VALUES (2307, 2307, 166, 'Siak Hulu');
INSERT INTO `subdistricts` VALUES (2308, 2308, 166, 'Tambang');
INSERT INTO `subdistricts` VALUES (2309, 2309, 166, 'Tapung');
INSERT INTO `subdistricts` VALUES (2310, 2310, 166, 'Tapung Hilir');
INSERT INTO `subdistricts` VALUES (2311, 2311, 166, 'Tapung Hulu');
INSERT INTO `subdistricts` VALUES (2312, 2312, 166, 'XIII Koto Kampar');
INSERT INTO `subdistricts` VALUES (2313, 2313, 167, 'Basarang');
INSERT INTO `subdistricts` VALUES (2314, 2314, 167, 'Bataguh');
INSERT INTO `subdistricts` VALUES (2315, 2315, 167, 'Dadahup');
INSERT INTO `subdistricts` VALUES (2316, 2316, 167, 'Kapuas Barat');
INSERT INTO `subdistricts` VALUES (2317, 2317, 167, 'Kapuas Hilir');
INSERT INTO `subdistricts` VALUES (2318, 2318, 167, 'Kapuas Hulu');
INSERT INTO `subdistricts` VALUES (2319, 2319, 167, 'Kapuas Kuala');
INSERT INTO `subdistricts` VALUES (2320, 2320, 167, 'Kapuas Murung');
INSERT INTO `subdistricts` VALUES (2321, 2321, 167, 'Kapuas Tengah');
INSERT INTO `subdistricts` VALUES (2322, 2322, 167, 'Kapuas Timur');
INSERT INTO `subdistricts` VALUES (2323, 2323, 167, 'Mandau Talawang');
INSERT INTO `subdistricts` VALUES (2324, 2324, 167, 'Mantangai');
INSERT INTO `subdistricts` VALUES (2325, 2325, 167, 'Pasak Talawang');
INSERT INTO `subdistricts` VALUES (2326, 2326, 167, 'Pulau Petak');
INSERT INTO `subdistricts` VALUES (2327, 2327, 167, 'Selat');
INSERT INTO `subdistricts` VALUES (2328, 2328, 167, 'Tamban Catur');
INSERT INTO `subdistricts` VALUES (2329, 2329, 167, 'Timpah');
INSERT INTO `subdistricts` VALUES (2330, 2330, 168, 'Badau');
INSERT INTO `subdistricts` VALUES (2331, 2331, 168, 'Batang Lupar');
INSERT INTO `subdistricts` VALUES (2332, 2332, 168, 'Bika');
INSERT INTO `subdistricts` VALUES (2333, 2333, 168, 'Boyan Tanjung');
INSERT INTO `subdistricts` VALUES (2334, 2334, 168, 'Bunut Hilir');
INSERT INTO `subdistricts` VALUES (2335, 2335, 168, 'Bunut Hulu');
INSERT INTO `subdistricts` VALUES (2336, 2336, 168, 'Embaloh Hilir');
INSERT INTO `subdistricts` VALUES (2337, 2337, 168, 'Embaloh Hulu');
INSERT INTO `subdistricts` VALUES (2338, 2338, 168, 'Empanang');
INSERT INTO `subdistricts` VALUES (2339, 2339, 168, 'Hulu Gurung');
INSERT INTO `subdistricts` VALUES (2340, 2340, 168, 'Jongkong (Jengkong)');
INSERT INTO `subdistricts` VALUES (2341, 2341, 168, 'Kalis');
INSERT INTO `subdistricts` VALUES (2342, 2342, 168, 'Mentebah');
INSERT INTO `subdistricts` VALUES (2343, 2343, 168, 'Pengkadan (Batu Datu)');
INSERT INTO `subdistricts` VALUES (2344, 2344, 168, 'Puring Kencana');
INSERT INTO `subdistricts` VALUES (2345, 2345, 168, 'Putussibau Selatan');
INSERT INTO `subdistricts` VALUES (2346, 2346, 168, 'Putussibau Utara');
INSERT INTO `subdistricts` VALUES (2347, 2347, 168, 'Seberuang');
INSERT INTO `subdistricts` VALUES (2348, 2348, 168, 'Selimbau');
INSERT INTO `subdistricts` VALUES (2349, 2349, 168, 'Semitau');
INSERT INTO `subdistricts` VALUES (2350, 2350, 168, 'Silat Hilir');
INSERT INTO `subdistricts` VALUES (2351, 2351, 168, 'Silat Hulu');
INSERT INTO `subdistricts` VALUES (2352, 2352, 168, 'Suhaid');
INSERT INTO `subdistricts` VALUES (2353, 2353, 169, 'Colomadu');
INSERT INTO `subdistricts` VALUES (2354, 2354, 169, 'Gondangrejo');
INSERT INTO `subdistricts` VALUES (2355, 2355, 169, 'Jaten');
INSERT INTO `subdistricts` VALUES (2356, 2356, 169, 'Jatipuro');
INSERT INTO `subdistricts` VALUES (2357, 2357, 169, 'Jatiyoso');
INSERT INTO `subdistricts` VALUES (2358, 2358, 169, 'Jenawi');
INSERT INTO `subdistricts` VALUES (2359, 2359, 169, 'Jumantono');
INSERT INTO `subdistricts` VALUES (2360, 2360, 169, 'Jumapolo');
INSERT INTO `subdistricts` VALUES (2361, 2361, 169, 'Karanganyar');
INSERT INTO `subdistricts` VALUES (2362, 2362, 169, 'Karangpandan');
INSERT INTO `subdistricts` VALUES (2363, 2363, 169, 'Kebakkramat');
INSERT INTO `subdistricts` VALUES (2364, 2364, 169, 'Kerjo');
INSERT INTO `subdistricts` VALUES (2365, 2365, 169, 'Matesih');
INSERT INTO `subdistricts` VALUES (2366, 2366, 169, 'Mojogedang');
INSERT INTO `subdistricts` VALUES (2367, 2367, 169, 'Ngargoyoso');
INSERT INTO `subdistricts` VALUES (2368, 2368, 169, 'Tasikmadu');
INSERT INTO `subdistricts` VALUES (2369, 2369, 169, 'Tawangmangu');
INSERT INTO `subdistricts` VALUES (2370, 2370, 170, 'Abang');
INSERT INTO `subdistricts` VALUES (2371, 2371, 170, 'Bebandem');
INSERT INTO `subdistricts` VALUES (2372, 2372, 170, 'Karang Asem');
INSERT INTO `subdistricts` VALUES (2373, 2373, 170, 'Kubu');
INSERT INTO `subdistricts` VALUES (2374, 2374, 170, 'Manggis');
INSERT INTO `subdistricts` VALUES (2375, 2375, 170, 'Rendang');
INSERT INTO `subdistricts` VALUES (2376, 2376, 170, 'Selat');
INSERT INTO `subdistricts` VALUES (2377, 2377, 170, 'Sidemen');
INSERT INTO `subdistricts` VALUES (2378, 2378, 171, 'Banyusari');
INSERT INTO `subdistricts` VALUES (2379, 2379, 171, 'Batujaya');
INSERT INTO `subdistricts` VALUES (2380, 2380, 171, 'Ciampel');
INSERT INTO `subdistricts` VALUES (2381, 2381, 171, 'Cibuaya');
INSERT INTO `subdistricts` VALUES (2382, 2382, 171, 'Cikampek');
INSERT INTO `subdistricts` VALUES (2383, 2383, 171, 'Cilamaya Kulon');
INSERT INTO `subdistricts` VALUES (2384, 2384, 171, 'Cilamaya Wetan');
INSERT INTO `subdistricts` VALUES (2385, 2385, 171, 'Cilebar');
INSERT INTO `subdistricts` VALUES (2386, 2386, 171, 'Jatisari');
INSERT INTO `subdistricts` VALUES (2387, 2387, 171, 'Jayakerta');
INSERT INTO `subdistricts` VALUES (2388, 2388, 171, 'Karawang Barat');
INSERT INTO `subdistricts` VALUES (2389, 2389, 171, 'Karawang Timur');
INSERT INTO `subdistricts` VALUES (2390, 2390, 171, 'Klari');
INSERT INTO `subdistricts` VALUES (2391, 2391, 171, 'Kotabaru');
INSERT INTO `subdistricts` VALUES (2392, 2392, 171, 'Kutawaluya');
INSERT INTO `subdistricts` VALUES (2393, 2393, 171, 'Lemahabang');
INSERT INTO `subdistricts` VALUES (2394, 2394, 171, 'Majalaya');
INSERT INTO `subdistricts` VALUES (2395, 2395, 171, 'Pakisjaya');
INSERT INTO `subdistricts` VALUES (2396, 2396, 171, 'Pangkalan');
INSERT INTO `subdistricts` VALUES (2397, 2397, 171, 'Pedes');
INSERT INTO `subdistricts` VALUES (2398, 2398, 171, 'Purwasari');
INSERT INTO `subdistricts` VALUES (2399, 2399, 171, 'Rawamerta');
INSERT INTO `subdistricts` VALUES (2400, 2400, 171, 'Rengasdengklok');
INSERT INTO `subdistricts` VALUES (2401, 2401, 171, 'Talagasari');
INSERT INTO `subdistricts` VALUES (2402, 2402, 171, 'Tegalwaru');
INSERT INTO `subdistricts` VALUES (2403, 2403, 171, 'Telukjambe Barat');
INSERT INTO `subdistricts` VALUES (2404, 2404, 171, 'Telukjambe Timur');
INSERT INTO `subdistricts` VALUES (2405, 2405, 171, 'Tempuran');
INSERT INTO `subdistricts` VALUES (2406, 2406, 171, 'Tirtajaya');
INSERT INTO `subdistricts` VALUES (2407, 2407, 171, 'Tirtamulya');
INSERT INTO `subdistricts` VALUES (2408, 2408, 172, 'Belat');
INSERT INTO `subdistricts` VALUES (2409, 2409, 172, 'Buru');
INSERT INTO `subdistricts` VALUES (2410, 2410, 172, 'Durai');
INSERT INTO `subdistricts` VALUES (2411, 2411, 172, 'Karimun');
INSERT INTO `subdistricts` VALUES (2412, 2412, 172, 'Kundur');
INSERT INTO `subdistricts` VALUES (2413, 2413, 172, 'Kundur Barat');
INSERT INTO `subdistricts` VALUES (2414, 2414, 172, 'Kundur Utara');
INSERT INTO `subdistricts` VALUES (2415, 2415, 172, 'Meral');
INSERT INTO `subdistricts` VALUES (2416, 2416, 172, 'Meral Barat');
INSERT INTO `subdistricts` VALUES (2417, 2417, 172, 'Moro');
INSERT INTO `subdistricts` VALUES (2418, 2418, 172, 'Tebing');
INSERT INTO `subdistricts` VALUES (2419, 2419, 172, 'Ungar');
INSERT INTO `subdistricts` VALUES (2420, 2420, 173, 'Barus Jahe');
INSERT INTO `subdistricts` VALUES (2421, 2421, 173, 'Brastagi (Berastagi)');
INSERT INTO `subdistricts` VALUES (2422, 2422, 173, 'Dolat Rayat');
INSERT INTO `subdistricts` VALUES (2423, 2423, 173, 'Juhar');
INSERT INTO `subdistricts` VALUES (2424, 2424, 173, 'Kabanjahe');
INSERT INTO `subdistricts` VALUES (2425, 2425, 173, 'Kuta Buluh');
INSERT INTO `subdistricts` VALUES (2426, 2426, 173, 'Laubaleng');
INSERT INTO `subdistricts` VALUES (2427, 2427, 173, 'Mardinding');
INSERT INTO `subdistricts` VALUES (2428, 2428, 173, 'Merdeka');
INSERT INTO `subdistricts` VALUES (2429, 2429, 173, 'Merek');
INSERT INTO `subdistricts` VALUES (2430, 2430, 173, 'Munte');
INSERT INTO `subdistricts` VALUES (2431, 2431, 173, 'Nama Teran');
INSERT INTO `subdistricts` VALUES (2432, 2432, 173, 'Payung');
INSERT INTO `subdistricts` VALUES (2433, 2433, 173, 'Simpang Empat');
INSERT INTO `subdistricts` VALUES (2434, 2434, 173, 'Tiga Binanga');
INSERT INTO `subdistricts` VALUES (2435, 2435, 173, 'Tiga Panah');
INSERT INTO `subdistricts` VALUES (2436, 2436, 173, 'Tiganderket');
INSERT INTO `subdistricts` VALUES (2437, 2437, 174, 'Bukit Raya');
INSERT INTO `subdistricts` VALUES (2438, 2438, 174, 'Kamipang');
INSERT INTO `subdistricts` VALUES (2439, 2439, 174, 'Katingan Hilir');
INSERT INTO `subdistricts` VALUES (2440, 2440, 174, 'Katingan Hulu');
INSERT INTO `subdistricts` VALUES (2441, 2441, 174, 'Katingan Kuala');
INSERT INTO `subdistricts` VALUES (2442, 2442, 174, 'Katingan Tengah');
INSERT INTO `subdistricts` VALUES (2443, 2443, 174, 'Marikit');
INSERT INTO `subdistricts` VALUES (2444, 2444, 174, 'Mendawai');
INSERT INTO `subdistricts` VALUES (2445, 2445, 174, 'Petak Malai');
INSERT INTO `subdistricts` VALUES (2446, 2446, 174, 'Pulau Malan');
INSERT INTO `subdistricts` VALUES (2447, 2447, 174, 'Sanaman Mantikei (Senamang Mantikei)');
INSERT INTO `subdistricts` VALUES (2448, 2448, 174, 'Tasik Payawan');
INSERT INTO `subdistricts` VALUES (2449, 2449, 174, 'Tewang Sanggalang Garing (Sangalang)');
INSERT INTO `subdistricts` VALUES (2450, 2450, 175, 'Kaur Selatan');
INSERT INTO `subdistricts` VALUES (2451, 2451, 175, 'Kaur Tengah');
INSERT INTO `subdistricts` VALUES (2452, 2452, 175, 'Kaur Utara');
INSERT INTO `subdistricts` VALUES (2453, 2453, 175, 'Kelam Tengah');
INSERT INTO `subdistricts` VALUES (2454, 2454, 175, 'Kinal');
INSERT INTO `subdistricts` VALUES (2455, 2455, 175, 'Luas');
INSERT INTO `subdistricts` VALUES (2456, 2456, 175, 'Lungkang Kule');
INSERT INTO `subdistricts` VALUES (2457, 2457, 175, 'Maje');
INSERT INTO `subdistricts` VALUES (2458, 2458, 175, 'Muara Sahung');
INSERT INTO `subdistricts` VALUES (2459, 2459, 175, 'Nasal');
INSERT INTO `subdistricts` VALUES (2460, 2460, 175, 'Padang Guci Hilir');
INSERT INTO `subdistricts` VALUES (2461, 2461, 175, 'Padang Guci Hulu');
INSERT INTO `subdistricts` VALUES (2462, 2462, 175, 'Semidang Gumai (Gumay)');
INSERT INTO `subdistricts` VALUES (2463, 2463, 175, 'Tanjung Kemuning');
INSERT INTO `subdistricts` VALUES (2464, 2464, 175, 'Tetap (Muara Tetap)');
INSERT INTO `subdistricts` VALUES (2465, 2465, 176, 'Kepulauan Karimata');
INSERT INTO `subdistricts` VALUES (2466, 2466, 176, 'Pulau Maya (Pulau Maya Karimata)');
INSERT INTO `subdistricts` VALUES (2467, 2467, 176, 'Seponti');
INSERT INTO `subdistricts` VALUES (2468, 2468, 176, 'Simpang Hilir');
INSERT INTO `subdistricts` VALUES (2469, 2469, 176, 'Sukadana');
INSERT INTO `subdistricts` VALUES (2470, 2470, 176, 'Teluk Batang');
INSERT INTO `subdistricts` VALUES (2471, 2471, 177, 'Adimulyo');
INSERT INTO `subdistricts` VALUES (2472, 2472, 177, 'Alian/Aliyan');
INSERT INTO `subdistricts` VALUES (2473, 2473, 177, 'Ambal');
INSERT INTO `subdistricts` VALUES (2474, 2474, 177, 'Ayah');
INSERT INTO `subdistricts` VALUES (2475, 2475, 177, 'Bonorowo');
INSERT INTO `subdistricts` VALUES (2476, 2476, 177, 'Buayan');
INSERT INTO `subdistricts` VALUES (2477, 2477, 177, 'Buluspesantren');
INSERT INTO `subdistricts` VALUES (2478, 2478, 177, 'Gombong');
INSERT INTO `subdistricts` VALUES (2479, 2479, 177, 'Karanganyar');
INSERT INTO `subdistricts` VALUES (2480, 2480, 177, 'Karanggayam');
INSERT INTO `subdistricts` VALUES (2481, 2481, 177, 'Karangsambung');
INSERT INTO `subdistricts` VALUES (2482, 2482, 177, 'Kebumen');
INSERT INTO `subdistricts` VALUES (2483, 2483, 177, 'Klirong');
INSERT INTO `subdistricts` VALUES (2484, 2484, 177, 'Kutowinangun');
INSERT INTO `subdistricts` VALUES (2485, 2485, 177, 'Kuwarasan');
INSERT INTO `subdistricts` VALUES (2486, 2486, 177, 'Mirit');
INSERT INTO `subdistricts` VALUES (2487, 2487, 177, 'Padureso');
INSERT INTO `subdistricts` VALUES (2488, 2488, 177, 'Pejagoan');
INSERT INTO `subdistricts` VALUES (2489, 2489, 177, 'Petanahan');
INSERT INTO `subdistricts` VALUES (2490, 2490, 177, 'Poncowarno');
INSERT INTO `subdistricts` VALUES (2491, 2491, 177, 'Prembun');
INSERT INTO `subdistricts` VALUES (2492, 2492, 177, 'Puring');
INSERT INTO `subdistricts` VALUES (2493, 2493, 177, 'Rowokele');
INSERT INTO `subdistricts` VALUES (2494, 2494, 177, 'Sadang');
INSERT INTO `subdistricts` VALUES (2495, 2495, 177, 'Sempor');
INSERT INTO `subdistricts` VALUES (2496, 2496, 177, 'Sruweng');
INSERT INTO `subdistricts` VALUES (2497, 2497, 178, 'Badas');
INSERT INTO `subdistricts` VALUES (2498, 2498, 178, 'Banyakan');
INSERT INTO `subdistricts` VALUES (2499, 2499, 178, 'Gampengrejo');
INSERT INTO `subdistricts` VALUES (2500, 2500, 178, 'Grogol');
INSERT INTO `subdistricts` VALUES (2501, 2501, 178, 'Gurah');
INSERT INTO `subdistricts` VALUES (2502, 2502, 178, 'Kandangan');
INSERT INTO `subdistricts` VALUES (2503, 2503, 178, 'Kandat');
INSERT INTO `subdistricts` VALUES (2504, 2504, 178, 'Kayen Kidul');
INSERT INTO `subdistricts` VALUES (2505, 2505, 178, 'Kepung');
INSERT INTO `subdistricts` VALUES (2506, 2506, 178, 'Kras');
INSERT INTO `subdistricts` VALUES (2507, 2507, 178, 'Kunjang');
INSERT INTO `subdistricts` VALUES (2508, 2508, 178, 'Mojo');
INSERT INTO `subdistricts` VALUES (2509, 2509, 178, 'Ngadiluwih');
INSERT INTO `subdistricts` VALUES (2510, 2510, 178, 'Ngancar');
INSERT INTO `subdistricts` VALUES (2511, 2511, 178, 'Ngasem');
INSERT INTO `subdistricts` VALUES (2512, 2512, 178, 'Pagu');
INSERT INTO `subdistricts` VALUES (2513, 2513, 178, 'Papar');
INSERT INTO `subdistricts` VALUES (2514, 2514, 178, 'Pare');
INSERT INTO `subdistricts` VALUES (2515, 2515, 178, 'Plemahan');
INSERT INTO `subdistricts` VALUES (2516, 2516, 178, 'Plosoklaten');
INSERT INTO `subdistricts` VALUES (2517, 2517, 178, 'Puncu');
INSERT INTO `subdistricts` VALUES (2518, 2518, 178, 'Purwoasri');
INSERT INTO `subdistricts` VALUES (2519, 2519, 178, 'Ringinrejo');
INSERT INTO `subdistricts` VALUES (2520, 2520, 178, 'Semen');
INSERT INTO `subdistricts` VALUES (2521, 2521, 178, 'Tarokan');
INSERT INTO `subdistricts` VALUES (2522, 2522, 178, 'Wates');
INSERT INTO `subdistricts` VALUES (2523, 2523, 179, 'Kediri Kota');
INSERT INTO `subdistricts` VALUES (2524, 2524, 179, 'Mojoroto');
INSERT INTO `subdistricts` VALUES (2525, 2525, 179, 'Pesantren');
INSERT INTO `subdistricts` VALUES (2526, 2526, 180, 'Arso');
INSERT INTO `subdistricts` VALUES (2527, 2527, 180, 'Arso Timur');
INSERT INTO `subdistricts` VALUES (2528, 2528, 180, 'Senggi');
INSERT INTO `subdistricts` VALUES (2529, 2529, 180, 'Skamto (Skanto)');
INSERT INTO `subdistricts` VALUES (2530, 2530, 180, 'Towe');
INSERT INTO `subdistricts` VALUES (2531, 2531, 180, 'Waris');
INSERT INTO `subdistricts` VALUES (2532, 2532, 180, 'Web');
INSERT INTO `subdistricts` VALUES (2533, 2533, 181, 'Boja');
INSERT INTO `subdistricts` VALUES (2534, 2534, 181, 'Brangsong');
INSERT INTO `subdistricts` VALUES (2535, 2535, 181, 'Cepiring');
INSERT INTO `subdistricts` VALUES (2536, 2536, 181, 'Gemuh');
INSERT INTO `subdistricts` VALUES (2537, 2537, 181, 'Kaliwungu');
INSERT INTO `subdistricts` VALUES (2538, 2538, 181, 'Kaliwungu Selatan');
INSERT INTO `subdistricts` VALUES (2539, 2539, 181, 'Kangkung');
INSERT INTO `subdistricts` VALUES (2540, 2540, 181, 'Kendal');
INSERT INTO `subdistricts` VALUES (2541, 2541, 181, 'Limbangan');
INSERT INTO `subdistricts` VALUES (2542, 2542, 181, 'Ngampel');
INSERT INTO `subdistricts` VALUES (2543, 2543, 181, 'Pagerruyung');
INSERT INTO `subdistricts` VALUES (2544, 2544, 181, 'Patean');
INSERT INTO `subdistricts` VALUES (2545, 2545, 181, 'Patebon');
INSERT INTO `subdistricts` VALUES (2546, 2546, 181, 'Pegandon');
INSERT INTO `subdistricts` VALUES (2547, 2547, 181, 'Plantungan');
INSERT INTO `subdistricts` VALUES (2548, 2548, 181, 'Ringinarum');
INSERT INTO `subdistricts` VALUES (2549, 2549, 181, 'Rowosari');
INSERT INTO `subdistricts` VALUES (2550, 2550, 181, 'Singorojo');
INSERT INTO `subdistricts` VALUES (2551, 2551, 181, 'Sukorejo');
INSERT INTO `subdistricts` VALUES (2552, 2552, 181, 'Weleri');
INSERT INTO `subdistricts` VALUES (2553, 2553, 182, 'Abeli');
INSERT INTO `subdistricts` VALUES (2554, 2554, 182, 'Baruga');
INSERT INTO `subdistricts` VALUES (2555, 2555, 182, 'Kadia');
INSERT INTO `subdistricts` VALUES (2556, 2556, 182, 'Kambu');
INSERT INTO `subdistricts` VALUES (2557, 2557, 182, 'Kendari');
INSERT INTO `subdistricts` VALUES (2558, 2558, 182, 'Kendari Barat');
INSERT INTO `subdistricts` VALUES (2559, 2559, 182, 'Mandonga');
INSERT INTO `subdistricts` VALUES (2560, 2560, 182, 'Poasia');
INSERT INTO `subdistricts` VALUES (2561, 2561, 182, 'Puuwatu');
INSERT INTO `subdistricts` VALUES (2562, 2562, 182, 'Wua-Wua');
INSERT INTO `subdistricts` VALUES (2563, 2563, 183, 'Bermani Ilir');
INSERT INTO `subdistricts` VALUES (2564, 2564, 183, 'Kebawetan (Kabawetan)');
INSERT INTO `subdistricts` VALUES (2565, 2565, 183, 'Kepahiang');
INSERT INTO `subdistricts` VALUES (2566, 2566, 183, 'Merigi');
INSERT INTO `subdistricts` VALUES (2567, 2567, 183, 'Muara Kemumu');
INSERT INTO `subdistricts` VALUES (2568, 2568, 183, 'Seberang Musi');
INSERT INTO `subdistricts` VALUES (2569, 2569, 183, 'Tebat Karai');
INSERT INTO `subdistricts` VALUES (2570, 2570, 183, 'Ujan Mas');
INSERT INTO `subdistricts` VALUES (2571, 2571, 184, 'Jemaja');
INSERT INTO `subdistricts` VALUES (2572, 2572, 184, 'Jemaja Timur');
INSERT INTO `subdistricts` VALUES (2573, 2573, 184, 'Palmatak');
INSERT INTO `subdistricts` VALUES (2574, 2574, 184, 'Siantan');
INSERT INTO `subdistricts` VALUES (2575, 2575, 184, 'Siantan Selatan');
INSERT INTO `subdistricts` VALUES (2576, 2576, 184, 'Siantan Tengah');
INSERT INTO `subdistricts` VALUES (2577, 2577, 184, 'Siantan Timur');
INSERT INTO `subdistricts` VALUES (2578, 2578, 185, 'Aru Selatan');
INSERT INTO `subdistricts` VALUES (2579, 2579, 185, 'Aru Selatan Timur');
INSERT INTO `subdistricts` VALUES (2580, 2580, 185, 'Aru Selatan Utara');
INSERT INTO `subdistricts` VALUES (2581, 2581, 185, 'Aru Tengah');
INSERT INTO `subdistricts` VALUES (2582, 2582, 185, 'Aru Tengah Selatan');
INSERT INTO `subdistricts` VALUES (2583, 2583, 185, 'Aru Tengah Timur');
INSERT INTO `subdistricts` VALUES (2584, 2584, 185, 'Aru Utara');
INSERT INTO `subdistricts` VALUES (2585, 2585, 185, 'Aru Utara Timur Batuley');
INSERT INTO `subdistricts` VALUES (2586, 2586, 185, 'Pulau-Pulau Aru');
INSERT INTO `subdistricts` VALUES (2587, 2587, 185, 'Sir-Sir');
INSERT INTO `subdistricts` VALUES (2588, 2588, 186, 'Pagai Selatan');
INSERT INTO `subdistricts` VALUES (2589, 2589, 186, 'Pagai Utara');
INSERT INTO `subdistricts` VALUES (2590, 2590, 186, 'Siberut Barat');
INSERT INTO `subdistricts` VALUES (2591, 2591, 186, 'Siberut Barat Daya');
INSERT INTO `subdistricts` VALUES (2592, 2592, 186, 'Siberut Selatan');
INSERT INTO `subdistricts` VALUES (2593, 2593, 186, 'Siberut Tengah');
INSERT INTO `subdistricts` VALUES (2594, 2594, 186, 'Siberut Utara');
INSERT INTO `subdistricts` VALUES (2595, 2595, 186, 'Sikakap');
INSERT INTO `subdistricts` VALUES (2596, 2596, 186, 'Sipora Selatan');
INSERT INTO `subdistricts` VALUES (2597, 2597, 186, 'Sipora Utara');
INSERT INTO `subdistricts` VALUES (2598, 2598, 187, 'Merbau');
INSERT INTO `subdistricts` VALUES (2599, 2599, 187, 'Pulaumerbau');
INSERT INTO `subdistricts` VALUES (2600, 2600, 187, 'Rangsang');
INSERT INTO `subdistricts` VALUES (2601, 2601, 187, 'Rangsang Barat');
INSERT INTO `subdistricts` VALUES (2602, 2602, 187, 'Rangsang Pesisir');
INSERT INTO `subdistricts` VALUES (2603, 2603, 187, 'Tasik Putri Puyu');
INSERT INTO `subdistricts` VALUES (2604, 2604, 187, 'Tebing Tinggi');
INSERT INTO `subdistricts` VALUES (2605, 2605, 187, 'Tebing Tinggi Barat');
INSERT INTO `subdistricts` VALUES (2606, 2606, 187, 'Tebing Tinggi Timur');
INSERT INTO `subdistricts` VALUES (2607, 2607, 188, 'Kendahe');
INSERT INTO `subdistricts` VALUES (2608, 2608, 188, 'Kepulauan Marore');
INSERT INTO `subdistricts` VALUES (2609, 2609, 188, 'Manganitu');
INSERT INTO `subdistricts` VALUES (2610, 2610, 188, 'Manganitu Selatan');
INSERT INTO `subdistricts` VALUES (2611, 2611, 188, 'Nusa Tabukan');
INSERT INTO `subdistricts` VALUES (2612, 2612, 188, 'Tabukan Selatan');
INSERT INTO `subdistricts` VALUES (2613, 2613, 188, 'Tabukan Selatan Tengah');
INSERT INTO `subdistricts` VALUES (2614, 2614, 188, 'Tabukan Selatan Tenggara');
INSERT INTO `subdistricts` VALUES (2615, 2615, 188, 'Tabukan Tengah');
INSERT INTO `subdistricts` VALUES (2616, 2616, 188, 'Tabukan Utara');
INSERT INTO `subdistricts` VALUES (2617, 2617, 188, 'Tahuna');
INSERT INTO `subdistricts` VALUES (2618, 2618, 188, 'Tahuna Barat');
INSERT INTO `subdistricts` VALUES (2619, 2619, 188, 'Tahuna Timur');
INSERT INTO `subdistricts` VALUES (2620, 2620, 188, 'Tamako');
INSERT INTO `subdistricts` VALUES (2621, 2621, 188, 'Tatoareng');
INSERT INTO `subdistricts` VALUES (2622, 2622, 189, 'Kepulauan Seribu Selatan');
INSERT INTO `subdistricts` VALUES (2623, 2623, 189, 'Kepulauan Seribu Utara');
INSERT INTO `subdistricts` VALUES (2624, 2624, 190, 'Biaro');
INSERT INTO `subdistricts` VALUES (2625, 2625, 190, 'Siau Barat');
INSERT INTO `subdistricts` VALUES (2626, 2626, 190, 'Siau Barat Selatan');
INSERT INTO `subdistricts` VALUES (2627, 2627, 190, 'Siau Barat Utara');
INSERT INTO `subdistricts` VALUES (2628, 2628, 190, 'Siau Tengah');
INSERT INTO `subdistricts` VALUES (2629, 2629, 190, 'Siau Timur');
INSERT INTO `subdistricts` VALUES (2630, 2630, 190, 'Siau Timur Selatan');
INSERT INTO `subdistricts` VALUES (2631, 2631, 190, 'Tagulandang');
INSERT INTO `subdistricts` VALUES (2632, 2632, 190, 'Tagulandang Selatan');
INSERT INTO `subdistricts` VALUES (2633, 2633, 190, 'Tagulandang Utara');
INSERT INTO `subdistricts` VALUES (2634, 2634, 191, 'Lede');
INSERT INTO `subdistricts` VALUES (2635, 2635, 191, 'Mangoli Barat');
INSERT INTO `subdistricts` VALUES (2636, 2636, 191, 'Mangoli Selatan');
INSERT INTO `subdistricts` VALUES (2637, 2637, 191, 'Mangoli Tengah');
INSERT INTO `subdistricts` VALUES (2638, 2638, 191, 'Mangoli Timur');
INSERT INTO `subdistricts` VALUES (2639, 2639, 191, 'Mangoli Utara');
INSERT INTO `subdistricts` VALUES (2640, 2640, 191, 'Mangoli Utara Timur');
INSERT INTO `subdistricts` VALUES (2641, 2641, 191, 'Sanana');
INSERT INTO `subdistricts` VALUES (2642, 2642, 191, 'Sanana Utara');
INSERT INTO `subdistricts` VALUES (2643, 2643, 191, 'Sulabesi Barat');
INSERT INTO `subdistricts` VALUES (2644, 2644, 191, 'Sulabesi Selatan');
INSERT INTO `subdistricts` VALUES (2645, 2645, 191, 'Sulabesi Tengah');
INSERT INTO `subdistricts` VALUES (2646, 2646, 191, 'Sulabesi Timur');
INSERT INTO `subdistricts` VALUES (2647, 2647, 191, 'Taliabu Barat');
INSERT INTO `subdistricts` VALUES (2648, 2648, 191, 'Taliabu Barat Laut');
INSERT INTO `subdistricts` VALUES (2649, 2649, 191, 'Taliabu Selatan');
INSERT INTO `subdistricts` VALUES (2650, 2650, 191, 'Taliabu Timur');
INSERT INTO `subdistricts` VALUES (2651, 2651, 191, 'Taliabu Timur Selatan');
INSERT INTO `subdistricts` VALUES (2652, 2652, 191, 'Taliabu Utara');
INSERT INTO `subdistricts` VALUES (2653, 2653, 192, 'Beo');
INSERT INTO `subdistricts` VALUES (2654, 2654, 192, 'Beo Selatan');
INSERT INTO `subdistricts` VALUES (2655, 2655, 192, 'Beo Utara');
INSERT INTO `subdistricts` VALUES (2656, 2656, 192, 'Damao (Damau)');
INSERT INTO `subdistricts` VALUES (2657, 2657, 192, 'Essang');
INSERT INTO `subdistricts` VALUES (2658, 2658, 192, 'Essang Selatan');
INSERT INTO `subdistricts` VALUES (2659, 2659, 192, 'Gemeh');
INSERT INTO `subdistricts` VALUES (2660, 2660, 192, 'Kabaruan');
INSERT INTO `subdistricts` VALUES (2661, 2661, 192, 'Kalongan');
INSERT INTO `subdistricts` VALUES (2662, 2662, 192, 'Lirung');
INSERT INTO `subdistricts` VALUES (2663, 2663, 192, 'Melonguane');
INSERT INTO `subdistricts` VALUES (2664, 2664, 192, 'Melonguane Timur');
INSERT INTO `subdistricts` VALUES (2665, 2665, 192, 'Miangas');
INSERT INTO `subdistricts` VALUES (2666, 2666, 192, 'Moronge');
INSERT INTO `subdistricts` VALUES (2667, 2667, 192, 'Nanusa');
INSERT INTO `subdistricts` VALUES (2668, 2668, 192, 'Pulutan');
INSERT INTO `subdistricts` VALUES (2669, 2669, 192, 'Rainis');
INSERT INTO `subdistricts` VALUES (2670, 2670, 192, 'Salibabu');
INSERT INTO `subdistricts` VALUES (2671, 2671, 192, 'Tampan Amma');
INSERT INTO `subdistricts` VALUES (2672, 2672, 193, 'Angkaisera');
INSERT INTO `subdistricts` VALUES (2673, 2673, 193, 'Kepulauan Ambai');
INSERT INTO `subdistricts` VALUES (2674, 2674, 193, 'Kosiwo');
INSERT INTO `subdistricts` VALUES (2675, 2675, 193, 'Poom');
INSERT INTO `subdistricts` VALUES (2676, 2676, 193, 'Pulau Kurudu');
INSERT INTO `subdistricts` VALUES (2677, 2677, 193, 'Pulau Yerui');
INSERT INTO `subdistricts` VALUES (2678, 2678, 193, 'Raimbawi');
INSERT INTO `subdistricts` VALUES (2679, 2679, 193, 'Teluk Ampimoi');
INSERT INTO `subdistricts` VALUES (2680, 2680, 193, 'Windesi');
INSERT INTO `subdistricts` VALUES (2681, 2681, 193, 'Wonawa');
INSERT INTO `subdistricts` VALUES (2682, 2682, 193, 'Yapen Barat');
INSERT INTO `subdistricts` VALUES (2683, 2683, 193, 'Yapen Selatan');
INSERT INTO `subdistricts` VALUES (2684, 2684, 193, 'Yapen Timur');
INSERT INTO `subdistricts` VALUES (2685, 2685, 193, 'Yapen Utara');
INSERT INTO `subdistricts` VALUES (2686, 2686, 194, 'Air Hangat');
INSERT INTO `subdistricts` VALUES (2687, 2687, 194, 'Air Hangat Barat');
INSERT INTO `subdistricts` VALUES (2688, 2688, 194, 'Air Hangat Timur');
INSERT INTO `subdistricts` VALUES (2689, 2689, 194, 'Batang Merangin');
INSERT INTO `subdistricts` VALUES (2690, 2690, 194, 'Bukitkerman');
INSERT INTO `subdistricts` VALUES (2691, 2691, 194, 'Danau Kerinci');
INSERT INTO `subdistricts` VALUES (2692, 2692, 194, 'Depati Tujuh');
INSERT INTO `subdistricts` VALUES (2693, 2693, 194, 'Gunung Kerinci');
INSERT INTO `subdistricts` VALUES (2694, 2694, 194, 'Gunung Raya');
INSERT INTO `subdistricts` VALUES (2695, 2695, 194, 'Gunung Tujuh');
INSERT INTO `subdistricts` VALUES (2696, 2696, 194, 'Kayu Aro');
INSERT INTO `subdistricts` VALUES (2697, 2697, 194, 'Kayu Aro Barat');
INSERT INTO `subdistricts` VALUES (2698, 2698, 194, 'Keliling Danau');
INSERT INTO `subdistricts` VALUES (2699, 2699, 194, 'Sitinjau Laut');
INSERT INTO `subdistricts` VALUES (2700, 2700, 194, 'Siulak');
INSERT INTO `subdistricts` VALUES (2701, 2701, 194, 'Siulak Mukai');
INSERT INTO `subdistricts` VALUES (2702, 2702, 195, 'Air Upas');
INSERT INTO `subdistricts` VALUES (2703, 2703, 195, 'Benua Kayong');
INSERT INTO `subdistricts` VALUES (2704, 2704, 195, 'Delta Pawan');
INSERT INTO `subdistricts` VALUES (2705, 2705, 195, 'Hulu Sungai');
INSERT INTO `subdistricts` VALUES (2706, 2706, 195, 'Jelai Hulu');
INSERT INTO `subdistricts` VALUES (2707, 2707, 195, 'Kendawangan');
INSERT INTO `subdistricts` VALUES (2708, 2708, 195, 'Manis Mata');
INSERT INTO `subdistricts` VALUES (2709, 2709, 195, 'Marau');
INSERT INTO `subdistricts` VALUES (2710, 2710, 195, 'Matan Hilir Selatan');
INSERT INTO `subdistricts` VALUES (2711, 2711, 195, 'Matan Hilir Utara');
INSERT INTO `subdistricts` VALUES (2712, 2712, 195, 'Muara Pawan');
INSERT INTO `subdistricts` VALUES (2713, 2713, 195, 'Nanga Tayap');
INSERT INTO `subdistricts` VALUES (2714, 2714, 195, 'Pemahan');
INSERT INTO `subdistricts` VALUES (2715, 2715, 195, 'Sandai');
INSERT INTO `subdistricts` VALUES (2716, 2716, 195, 'Simpang Dua');
INSERT INTO `subdistricts` VALUES (2717, 2717, 195, 'Simpang Hulu');
INSERT INTO `subdistricts` VALUES (2718, 2718, 195, 'Singkup');
INSERT INTO `subdistricts` VALUES (2719, 2719, 195, 'Sungai Laur');
INSERT INTO `subdistricts` VALUES (2720, 2720, 195, 'Sungai Melayu Rayak');
INSERT INTO `subdistricts` VALUES (2721, 2721, 195, 'Tumbang Titi');
INSERT INTO `subdistricts` VALUES (2722, 2722, 196, 'Bayat');
INSERT INTO `subdistricts` VALUES (2723, 2723, 196, 'Cawas');
INSERT INTO `subdistricts` VALUES (2724, 2724, 196, 'Ceper');
INSERT INTO `subdistricts` VALUES (2725, 2725, 196, 'Delanggu');
INSERT INTO `subdistricts` VALUES (2726, 2726, 196, 'Gantiwarno');
INSERT INTO `subdistricts` VALUES (2727, 2727, 196, 'Jatinom');
INSERT INTO `subdistricts` VALUES (2728, 2728, 196, 'Jogonalan');
INSERT INTO `subdistricts` VALUES (2729, 2729, 196, 'Juwiring');
INSERT INTO `subdistricts` VALUES (2730, 2730, 196, 'Kalikotes');
INSERT INTO `subdistricts` VALUES (2731, 2731, 196, 'Karanganom');
INSERT INTO `subdistricts` VALUES (2732, 2732, 196, 'Karangdowo');
INSERT INTO `subdistricts` VALUES (2733, 2733, 196, 'Karangnongko');
INSERT INTO `subdistricts` VALUES (2734, 2734, 196, 'Kebonarum');
INSERT INTO `subdistricts` VALUES (2735, 2735, 196, 'Kemalang');
INSERT INTO `subdistricts` VALUES (2736, 2736, 196, 'Klaten Selatan');
INSERT INTO `subdistricts` VALUES (2737, 2737, 196, 'Klaten Tengah');
INSERT INTO `subdistricts` VALUES (2738, 2738, 196, 'Klaten Utara');
INSERT INTO `subdistricts` VALUES (2739, 2739, 196, 'Manisrenggo');
INSERT INTO `subdistricts` VALUES (2740, 2740, 196, 'Ngawen');
INSERT INTO `subdistricts` VALUES (2741, 2741, 196, 'Pedan');
INSERT INTO `subdistricts` VALUES (2742, 2742, 196, 'Polanharjo');
INSERT INTO `subdistricts` VALUES (2743, 2743, 196, 'Prambanan');
INSERT INTO `subdistricts` VALUES (2744, 2744, 196, 'Trucuk');
INSERT INTO `subdistricts` VALUES (2745, 2745, 196, 'Tulung');
INSERT INTO `subdistricts` VALUES (2746, 2746, 196, 'Wedi');
INSERT INTO `subdistricts` VALUES (2747, 2747, 196, 'Wonosari');
INSERT INTO `subdistricts` VALUES (2748, 2748, 197, 'Banjarangkan');
INSERT INTO `subdistricts` VALUES (2749, 2749, 197, 'Dawan');
INSERT INTO `subdistricts` VALUES (2750, 2750, 197, 'Klungkung');
INSERT INTO `subdistricts` VALUES (2751, 2751, 197, 'Nusapenida');
INSERT INTO `subdistricts` VALUES (2752, 2752, 198, 'Baula');
INSERT INTO `subdistricts` VALUES (2753, 2753, 198, 'Kolaka');
INSERT INTO `subdistricts` VALUES (2754, 2754, 198, 'Ladongi');
INSERT INTO `subdistricts` VALUES (2755, 2755, 198, 'Lalolae');
INSERT INTO `subdistricts` VALUES (2756, 2756, 198, 'Lambandia (Lambadia)');
INSERT INTO `subdistricts` VALUES (2757, 2757, 198, 'Latambaga');
INSERT INTO `subdistricts` VALUES (2758, 2758, 198, 'Loea');
INSERT INTO `subdistricts` VALUES (2759, 2759, 198, 'Mowewe');
INSERT INTO `subdistricts` VALUES (2760, 2760, 198, 'Poli Polia');
INSERT INTO `subdistricts` VALUES (2761, 2761, 198, 'Polinggona');
INSERT INTO `subdistricts` VALUES (2762, 2762, 198, 'Pomalaa');
INSERT INTO `subdistricts` VALUES (2763, 2763, 198, 'Samaturu');
INSERT INTO `subdistricts` VALUES (2764, 2764, 198, 'Tanggetada');
INSERT INTO `subdistricts` VALUES (2765, 2765, 198, 'Tinondo');
INSERT INTO `subdistricts` VALUES (2766, 2766, 198, 'Tirawuta');
INSERT INTO `subdistricts` VALUES (2767, 2767, 198, 'Toari');
INSERT INTO `subdistricts` VALUES (2768, 2768, 198, 'Uluiwoi');
INSERT INTO `subdistricts` VALUES (2769, 2769, 198, 'Watumbangga (Watubanggo)');
INSERT INTO `subdistricts` VALUES (2770, 2770, 198, 'Wolo');
INSERT INTO `subdistricts` VALUES (2771, 2771, 198, 'Wundulako');
INSERT INTO `subdistricts` VALUES (2772, 2772, 199, 'Batu Putih');
INSERT INTO `subdistricts` VALUES (2773, 2773, 199, 'Katoi');
INSERT INTO `subdistricts` VALUES (2774, 2774, 199, 'Kodeoha');
INSERT INTO `subdistricts` VALUES (2775, 2775, 199, 'Lasusua');
INSERT INTO `subdistricts` VALUES (2776, 2776, 199, 'Lombai (Lambai)');
INSERT INTO `subdistricts` VALUES (2777, 2777, 199, 'Ngapa');
INSERT INTO `subdistricts` VALUES (2778, 2778, 199, 'Pakue');
INSERT INTO `subdistricts` VALUES (2779, 2779, 199, 'Pakue Tengah');
INSERT INTO `subdistricts` VALUES (2780, 2780, 199, 'Pakue Utara');
INSERT INTO `subdistricts` VALUES (2781, 2781, 199, 'Porehu');
INSERT INTO `subdistricts` VALUES (2782, 2782, 199, 'Ranteangin');
INSERT INTO `subdistricts` VALUES (2783, 2783, 199, 'Tiwu');
INSERT INTO `subdistricts` VALUES (2784, 2784, 199, 'Tolala');
INSERT INTO `subdistricts` VALUES (2785, 2785, 199, 'Watunohu');
INSERT INTO `subdistricts` VALUES (2786, 2786, 199, 'Wawo');
INSERT INTO `subdistricts` VALUES (2787, 2787, 200, 'Abuki');
INSERT INTO `subdistricts` VALUES (2788, 2788, 200, 'Amonggedo');
INSERT INTO `subdistricts` VALUES (2789, 2789, 200, 'Anggaberi');
INSERT INTO `subdistricts` VALUES (2790, 2790, 200, 'Asinua');
INSERT INTO `subdistricts` VALUES (2791, 2791, 200, 'Besulutu');
INSERT INTO `subdistricts` VALUES (2792, 2792, 200, 'Bondoala');
INSERT INTO `subdistricts` VALUES (2793, 2793, 200, 'Kapoiala (Kapoyala)');
INSERT INTO `subdistricts` VALUES (2794, 2794, 200, 'Konawe');
INSERT INTO `subdistricts` VALUES (2795, 2795, 200, 'Lalonggasumeeto');
INSERT INTO `subdistricts` VALUES (2796, 2796, 200, 'Lambuya');
INSERT INTO `subdistricts` VALUES (2797, 2797, 200, 'Latoma');
INSERT INTO `subdistricts` VALUES (2798, 2798, 200, 'Meluhu');
INSERT INTO `subdistricts` VALUES (2799, 2799, 200, 'Onembute');
INSERT INTO `subdistricts` VALUES (2800, 2800, 200, 'Pondidaha');
INSERT INTO `subdistricts` VALUES (2801, 2801, 200, 'Puriala');
INSERT INTO `subdistricts` VALUES (2802, 2802, 200, 'Routa');
INSERT INTO `subdistricts` VALUES (2803, 2803, 200, 'Sampara');
INSERT INTO `subdistricts` VALUES (2804, 2804, 200, 'Soropia');
INSERT INTO `subdistricts` VALUES (2805, 2805, 200, 'Tongauna');
INSERT INTO `subdistricts` VALUES (2806, 2806, 200, 'Uepai (Uwepai)');
INSERT INTO `subdistricts` VALUES (2807, 2807, 200, 'Unaaha');
INSERT INTO `subdistricts` VALUES (2808, 2808, 200, 'Wawonii Barat');
INSERT INTO `subdistricts` VALUES (2809, 2809, 200, 'Wawonii Selatan');
INSERT INTO `subdistricts` VALUES (2810, 2810, 200, 'Wawonii Tengah');
INSERT INTO `subdistricts` VALUES (2811, 2811, 200, 'Wawonii Tenggara');
INSERT INTO `subdistricts` VALUES (2812, 2812, 200, 'Wawonii Timur');
INSERT INTO `subdistricts` VALUES (2813, 2813, 200, 'Wawonii Timur Laut');
INSERT INTO `subdistricts` VALUES (2814, 2814, 200, 'Wawonii Utara');
INSERT INTO `subdistricts` VALUES (2815, 2815, 200, 'Wawotobi');
INSERT INTO `subdistricts` VALUES (2816, 2816, 200, 'Wonggeduku');
INSERT INTO `subdistricts` VALUES (2817, 2817, 201, 'Andoolo');
INSERT INTO `subdistricts` VALUES (2818, 2818, 201, 'Angata');
INSERT INTO `subdistricts` VALUES (2819, 2819, 201, 'Baito');
INSERT INTO `subdistricts` VALUES (2820, 2820, 201, 'Basala');
INSERT INTO `subdistricts` VALUES (2821, 2821, 201, 'Benua');
INSERT INTO `subdistricts` VALUES (2822, 2822, 201, 'Buke');
INSERT INTO `subdistricts` VALUES (2823, 2823, 201, 'Kolono');
INSERT INTO `subdistricts` VALUES (2824, 2824, 201, 'Konda');
INSERT INTO `subdistricts` VALUES (2825, 2825, 201, 'Laeya');
INSERT INTO `subdistricts` VALUES (2826, 2826, 201, 'Lainea');
INSERT INTO `subdistricts` VALUES (2827, 2827, 201, 'Lalembuu / Lalumbuu');
INSERT INTO `subdistricts` VALUES (2828, 2828, 201, 'Landono');
INSERT INTO `subdistricts` VALUES (2829, 2829, 201, 'Laonti');
INSERT INTO `subdistricts` VALUES (2830, 2830, 201, 'Moramo');
INSERT INTO `subdistricts` VALUES (2831, 2831, 201, 'Moramo Utara');
INSERT INTO `subdistricts` VALUES (2832, 2832, 201, 'Mowila');
INSERT INTO `subdistricts` VALUES (2833, 2833, 201, 'Palangga');
INSERT INTO `subdistricts` VALUES (2834, 2834, 201, 'Palangga Selatan');
INSERT INTO `subdistricts` VALUES (2835, 2835, 201, 'Ranomeeto');
INSERT INTO `subdistricts` VALUES (2836, 2836, 201, 'Ranomeeto Barat');
INSERT INTO `subdistricts` VALUES (2837, 2837, 201, 'Tinanggea');
INSERT INTO `subdistricts` VALUES (2838, 2838, 201, 'Wolasi');
INSERT INTO `subdistricts` VALUES (2839, 2839, 202, 'Andowia');
INSERT INTO `subdistricts` VALUES (2840, 2840, 202, 'Asera');
INSERT INTO `subdistricts` VALUES (2841, 2841, 202, 'Langgikima');
INSERT INTO `subdistricts` VALUES (2842, 2842, 202, 'Lasolo');
INSERT INTO `subdistricts` VALUES (2843, 2843, 202, 'Lembo');
INSERT INTO `subdistricts` VALUES (2844, 2844, 202, 'Molawe');
INSERT INTO `subdistricts` VALUES (2845, 2845, 202, 'Motui');
INSERT INTO `subdistricts` VALUES (2846, 2846, 202, 'Oheo');
INSERT INTO `subdistricts` VALUES (2847, 2847, 202, 'Sawa');
INSERT INTO `subdistricts` VALUES (2848, 2848, 202, 'Wiwirano');
INSERT INTO `subdistricts` VALUES (2849, 2849, 203, 'Hampang');
INSERT INTO `subdistricts` VALUES (2850, 2850, 203, 'Kelumpang Barat');
INSERT INTO `subdistricts` VALUES (2851, 2851, 203, 'Kelumpang Hilir');
INSERT INTO `subdistricts` VALUES (2852, 2852, 203, 'Kelumpang Hulu');
INSERT INTO `subdistricts` VALUES (2853, 2853, 203, 'Kelumpang Selatan');
INSERT INTO `subdistricts` VALUES (2854, 2854, 203, 'Kelumpang Tengah');
INSERT INTO `subdistricts` VALUES (2855, 2855, 203, 'Kelumpang Utara');
INSERT INTO `subdistricts` VALUES (2856, 2856, 203, 'Pamukan Barat');
INSERT INTO `subdistricts` VALUES (2857, 2857, 203, 'Pamukan Selatan');
INSERT INTO `subdistricts` VALUES (2858, 2858, 203, 'Pamukan Utara');
INSERT INTO `subdistricts` VALUES (2859, 2859, 203, 'Pulau Laut Barat');
INSERT INTO `subdistricts` VALUES (2860, 2860, 203, 'Pulau Laut Kepulauan');
INSERT INTO `subdistricts` VALUES (2861, 2861, 203, 'Pulau Laut Selatan');
INSERT INTO `subdistricts` VALUES (2862, 2862, 203, 'Pulau Laut Tanjung Selayar');
INSERT INTO `subdistricts` VALUES (2863, 2863, 203, 'Pulau Laut Tengah');
INSERT INTO `subdistricts` VALUES (2864, 2864, 203, 'Pulau Laut Timur');
INSERT INTO `subdistricts` VALUES (2865, 2865, 203, 'Pulau Laut Utara');
INSERT INTO `subdistricts` VALUES (2866, 2866, 203, 'Pulau Sebuku');
INSERT INTO `subdistricts` VALUES (2867, 2867, 203, 'Pulau Sembilan');
INSERT INTO `subdistricts` VALUES (2868, 2868, 203, 'Sampanahan');
INSERT INTO `subdistricts` VALUES (2869, 2869, 203, 'Sungai Durian');
INSERT INTO `subdistricts` VALUES (2870, 2870, 204, 'Kotamobagu Barat');
INSERT INTO `subdistricts` VALUES (2871, 2871, 204, 'Kotamobagu Selatan');
INSERT INTO `subdistricts` VALUES (2872, 2872, 204, 'Kotamobagu Timur');
INSERT INTO `subdistricts` VALUES (2873, 2873, 204, 'Kotamobagu Utara');
INSERT INTO `subdistricts` VALUES (2874, 2874, 205, 'Arut Selatan');
INSERT INTO `subdistricts` VALUES (2875, 2875, 205, 'Arut Utara');
INSERT INTO `subdistricts` VALUES (2876, 2876, 205, 'Kotawaringin Lama');
INSERT INTO `subdistricts` VALUES (2877, 2877, 205, 'Kumai');
INSERT INTO `subdistricts` VALUES (2878, 2878, 205, 'Pangkalan Banteng');
INSERT INTO `subdistricts` VALUES (2879, 2879, 205, 'Pangkalan Lada');
INSERT INTO `subdistricts` VALUES (2880, 2880, 206, 'Antang Kalang');
INSERT INTO `subdistricts` VALUES (2881, 2881, 206, 'Baamang');
INSERT INTO `subdistricts` VALUES (2882, 2882, 206, 'Bukit Santuei');
INSERT INTO `subdistricts` VALUES (2883, 2883, 206, 'Cempaga');
INSERT INTO `subdistricts` VALUES (2884, 2884, 206, 'Cempaga Hulu');
INSERT INTO `subdistricts` VALUES (2885, 2885, 206, 'Kota Besi');
INSERT INTO `subdistricts` VALUES (2886, 2886, 206, 'Mentawa Baru (Ketapang)');
INSERT INTO `subdistricts` VALUES (2887, 2887, 206, 'Mentaya Hilir Selatan');
INSERT INTO `subdistricts` VALUES (2888, 2888, 206, 'Mentaya Hilir Utara');
INSERT INTO `subdistricts` VALUES (2889, 2889, 206, 'Mentaya Hulu');
INSERT INTO `subdistricts` VALUES (2890, 2890, 206, 'Parenggean');
INSERT INTO `subdistricts` VALUES (2891, 2891, 206, 'Pulau Hanaut');
INSERT INTO `subdistricts` VALUES (2892, 2892, 206, 'Seranau');
INSERT INTO `subdistricts` VALUES (2893, 2893, 206, 'Telaga Antang');
INSERT INTO `subdistricts` VALUES (2894, 2894, 206, 'Telawang');
INSERT INTO `subdistricts` VALUES (2895, 2895, 206, 'Teluk Sampit');
INSERT INTO `subdistricts` VALUES (2896, 2896, 206, 'Tualan Hulu');
INSERT INTO `subdistricts` VALUES (2897, 2897, 207, 'Benai');
INSERT INTO `subdistricts` VALUES (2898, 2898, 207, 'Cerenti');
INSERT INTO `subdistricts` VALUES (2899, 2899, 207, 'Gunung Toar');
INSERT INTO `subdistricts` VALUES (2900, 2900, 207, 'Hulu Kuantan');
INSERT INTO `subdistricts` VALUES (2901, 2901, 207, 'Inuman');
INSERT INTO `subdistricts` VALUES (2902, 2902, 207, 'Kuantan Hilir');
INSERT INTO `subdistricts` VALUES (2903, 2903, 207, 'Kuantan Hilir Seberang');
INSERT INTO `subdistricts` VALUES (2904, 2904, 207, 'Kuantan Mudik');
INSERT INTO `subdistricts` VALUES (2905, 2905, 207, 'Kuantan Tengah');
INSERT INTO `subdistricts` VALUES (2906, 2906, 207, 'Logas Tanah Darat');
INSERT INTO `subdistricts` VALUES (2907, 2907, 207, 'Pangean');
INSERT INTO `subdistricts` VALUES (2908, 2908, 207, 'Pucuk Rantau');
INSERT INTO `subdistricts` VALUES (2909, 2909, 207, 'Sentajo Raya');
INSERT INTO `subdistricts` VALUES (2910, 2910, 207, 'Singingi');
INSERT INTO `subdistricts` VALUES (2911, 2911, 207, 'Singingi Hilir');
INSERT INTO `subdistricts` VALUES (2912, 2912, 208, 'Batu Ampar');
INSERT INTO `subdistricts` VALUES (2913, 2913, 208, 'Kuala Mandor-B');
INSERT INTO `subdistricts` VALUES (2914, 2914, 208, 'Kubu');
INSERT INTO `subdistricts` VALUES (2915, 2915, 208, 'Rasau Jaya');
INSERT INTO `subdistricts` VALUES (2916, 2916, 208, 'Sei/Sungai Ambawang');
INSERT INTO `subdistricts` VALUES (2917, 2917, 208, 'Sei/Sungai Kakap');
INSERT INTO `subdistricts` VALUES (2918, 2918, 208, 'Sei/Sungai Raya');
INSERT INTO `subdistricts` VALUES (2919, 2919, 208, 'Teluk/Telok Pakedai');
INSERT INTO `subdistricts` VALUES (2920, 2920, 208, 'Terentang');
INSERT INTO `subdistricts` VALUES (2921, 2921, 209, 'Bae');
INSERT INTO `subdistricts` VALUES (2922, 2922, 209, 'Dawe');
INSERT INTO `subdistricts` VALUES (2923, 2923, 209, 'Gebog');
INSERT INTO `subdistricts` VALUES (2924, 2924, 209, 'Jati');
INSERT INTO `subdistricts` VALUES (2925, 2925, 209, 'Jekulo');
INSERT INTO `subdistricts` VALUES (2926, 2926, 209, 'Kaliwungu');
INSERT INTO `subdistricts` VALUES (2927, 2927, 209, 'Kudus Kota');
INSERT INTO `subdistricts` VALUES (2928, 2928, 209, 'Mejobo');
INSERT INTO `subdistricts` VALUES (2929, 2929, 209, 'Undaan');
INSERT INTO `subdistricts` VALUES (2930, 2930, 210, 'Galur');
INSERT INTO `subdistricts` VALUES (2931, 2931, 210, 'Girimulyo');
INSERT INTO `subdistricts` VALUES (2932, 2932, 210, 'Kalibawang');
INSERT INTO `subdistricts` VALUES (2933, 2933, 210, 'Kokap');
INSERT INTO `subdistricts` VALUES (2934, 2934, 210, 'Lendah');
INSERT INTO `subdistricts` VALUES (2935, 2935, 210, 'Nanggulan');
INSERT INTO `subdistricts` VALUES (2936, 2936, 210, 'Panjatan');
INSERT INTO `subdistricts` VALUES (2937, 2937, 210, 'Pengasih');
INSERT INTO `subdistricts` VALUES (2938, 2938, 210, 'Samigaluh');
INSERT INTO `subdistricts` VALUES (2939, 2939, 210, 'Sentolo');
INSERT INTO `subdistricts` VALUES (2940, 2940, 210, 'Temon');
INSERT INTO `subdistricts` VALUES (2941, 2941, 210, 'Wates');
INSERT INTO `subdistricts` VALUES (2942, 2942, 211, 'Ciawigebang');
INSERT INTO `subdistricts` VALUES (2943, 2943, 211, 'Cibeureum');
INSERT INTO `subdistricts` VALUES (2944, 2944, 211, 'Cibingbin');
INSERT INTO `subdistricts` VALUES (2945, 2945, 211, 'Cidahu');
INSERT INTO `subdistricts` VALUES (2946, 2946, 211, 'Cigandamekar');
INSERT INTO `subdistricts` VALUES (2947, 2947, 211, 'Cigugur');
INSERT INTO `subdistricts` VALUES (2948, 2948, 211, 'Cilebak');
INSERT INTO `subdistricts` VALUES (2949, 2949, 211, 'Cilimus');
INSERT INTO `subdistricts` VALUES (2950, 2950, 211, 'Cimahi');
INSERT INTO `subdistricts` VALUES (2951, 2951, 211, 'Ciniru');
INSERT INTO `subdistricts` VALUES (2952, 2952, 211, 'Cipicung');
INSERT INTO `subdistricts` VALUES (2953, 2953, 211, 'Ciwaru');
INSERT INTO `subdistricts` VALUES (2954, 2954, 211, 'Darma');
INSERT INTO `subdistricts` VALUES (2955, 2955, 211, 'Garawangi');
INSERT INTO `subdistricts` VALUES (2956, 2956, 211, 'Hantara');
INSERT INTO `subdistricts` VALUES (2957, 2957, 211, 'Jalaksana');
INSERT INTO `subdistricts` VALUES (2958, 2958, 211, 'Japara');
INSERT INTO `subdistricts` VALUES (2959, 2959, 211, 'Kadugede');
INSERT INTO `subdistricts` VALUES (2960, 2960, 211, 'Kalimanggis');
INSERT INTO `subdistricts` VALUES (2961, 2961, 211, 'Karangkancana');
INSERT INTO `subdistricts` VALUES (2962, 2962, 211, 'Kramat Mulya');
INSERT INTO `subdistricts` VALUES (2963, 2963, 211, 'Kuningan');
INSERT INTO `subdistricts` VALUES (2964, 2964, 211, 'Lebakwangi');
INSERT INTO `subdistricts` VALUES (2965, 2965, 211, 'Luragung');
INSERT INTO `subdistricts` VALUES (2966, 2966, 211, 'Maleber');
INSERT INTO `subdistricts` VALUES (2967, 2967, 211, 'Mandirancan');
INSERT INTO `subdistricts` VALUES (2968, 2968, 211, 'Nusaherang');
INSERT INTO `subdistricts` VALUES (2969, 2969, 211, 'Pancalang');
INSERT INTO `subdistricts` VALUES (2970, 2970, 211, 'Pasawahan');
INSERT INTO `subdistricts` VALUES (2971, 2971, 211, 'Selajambe');
INSERT INTO `subdistricts` VALUES (2972, 2972, 211, 'Sindangagung');
INSERT INTO `subdistricts` VALUES (2973, 2973, 211, 'Subang');
INSERT INTO `subdistricts` VALUES (2974, 2974, 212, 'Amabi Oefeto');
INSERT INTO `subdistricts` VALUES (2975, 2975, 212, 'Amabi Oefeto Timur');
INSERT INTO `subdistricts` VALUES (2976, 2976, 212, 'Amarasi');
INSERT INTO `subdistricts` VALUES (2977, 2977, 212, 'Amarasi Barat');
INSERT INTO `subdistricts` VALUES (2978, 2978, 212, 'Amarasi Selatan');
INSERT INTO `subdistricts` VALUES (2979, 2979, 212, 'Amarasi Timur');
INSERT INTO `subdistricts` VALUES (2980, 2980, 212, 'Amfoang Barat Daya');
INSERT INTO `subdistricts` VALUES (2981, 2981, 212, 'Amfoang Barat Laut');
INSERT INTO `subdistricts` VALUES (2982, 2982, 212, 'Amfoang Selatan');
INSERT INTO `subdistricts` VALUES (2983, 2983, 212, 'Amfoang Tengah');
INSERT INTO `subdistricts` VALUES (2984, 2984, 212, 'Amfoang Timur');
INSERT INTO `subdistricts` VALUES (2985, 2985, 212, 'Amfoang Utara');
INSERT INTO `subdistricts` VALUES (2986, 2986, 212, 'Fatuleu');
INSERT INTO `subdistricts` VALUES (2987, 2987, 212, 'Fatuleu Barat');
INSERT INTO `subdistricts` VALUES (2988, 2988, 212, 'Fatuleu Tengah');
INSERT INTO `subdistricts` VALUES (2989, 2989, 212, 'Kupang Barat');
INSERT INTO `subdistricts` VALUES (2990, 2990, 212, 'Kupang Tengah');
INSERT INTO `subdistricts` VALUES (2991, 2991, 212, 'Kupang Timur');
INSERT INTO `subdistricts` VALUES (2992, 2992, 212, 'Nekamese');
INSERT INTO `subdistricts` VALUES (2993, 2993, 212, 'Semau');
INSERT INTO `subdistricts` VALUES (2994, 2994, 212, 'Semau Selatan');
INSERT INTO `subdistricts` VALUES (2995, 2995, 212, 'Sulamu');
INSERT INTO `subdistricts` VALUES (2996, 2996, 212, 'Taebenu');
INSERT INTO `subdistricts` VALUES (2997, 2997, 212, 'Takari');
INSERT INTO `subdistricts` VALUES (2998, 2998, 213, 'Alak');
INSERT INTO `subdistricts` VALUES (2999, 2999, 213, 'Kelapa Lima');
INSERT INTO `subdistricts` VALUES (3000, 3000, 213, 'Kota Lama');
INSERT INTO `subdistricts` VALUES (3001, 3001, 213, 'Kota Raja');
INSERT INTO `subdistricts` VALUES (3002, 3002, 213, 'Maulafa');
INSERT INTO `subdistricts` VALUES (3003, 3003, 213, 'Oebobo');
INSERT INTO `subdistricts` VALUES (3004, 3004, 214, 'Barong Tongkok');
INSERT INTO `subdistricts` VALUES (3005, 3005, 214, 'Bentian Besar');
INSERT INTO `subdistricts` VALUES (3006, 3006, 214, 'Bongan');
INSERT INTO `subdistricts` VALUES (3007, 3007, 214, 'Damai');
INSERT INTO `subdistricts` VALUES (3008, 3008, 214, 'Jempang');
INSERT INTO `subdistricts` VALUES (3009, 3009, 214, 'Laham');
INSERT INTO `subdistricts` VALUES (3010, 3010, 214, 'Linggang Bigung');
INSERT INTO `subdistricts` VALUES (3011, 3011, 214, 'Long Apari');
INSERT INTO `subdistricts` VALUES (3012, 3012, 214, 'Long Bagun');
INSERT INTO `subdistricts` VALUES (3013, 3013, 214, 'Long Hubung');
INSERT INTO `subdistricts` VALUES (3014, 3014, 214, 'Long Iram');
INSERT INTO `subdistricts` VALUES (3015, 3015, 214, 'Long Pahangai');
INSERT INTO `subdistricts` VALUES (3016, 3016, 214, 'Manor Bulatin (Mook Manaar Bulatn)');
INSERT INTO `subdistricts` VALUES (3017, 3017, 214, 'Melak');
INSERT INTO `subdistricts` VALUES (3018, 3018, 214, 'Muara Lawa');
INSERT INTO `subdistricts` VALUES (3019, 3019, 214, 'Muara Pahu');
INSERT INTO `subdistricts` VALUES (3020, 3020, 214, 'Nyuatan');
INSERT INTO `subdistricts` VALUES (3021, 3021, 214, 'Penyinggahan');
INSERT INTO `subdistricts` VALUES (3022, 3022, 214, 'Sekolaq Darat');
INSERT INTO `subdistricts` VALUES (3023, 3023, 214, 'Siluq Ngurai');
INSERT INTO `subdistricts` VALUES (3024, 3024, 214, 'Tering');
INSERT INTO `subdistricts` VALUES (3025, 3025, 215, 'Anggana');
INSERT INTO `subdistricts` VALUES (3026, 3026, 215, 'Kembang Janggut');
INSERT INTO `subdistricts` VALUES (3027, 3027, 215, 'Kenohan');
INSERT INTO `subdistricts` VALUES (3028, 3028, 215, 'Kota Bangun');
INSERT INTO `subdistricts` VALUES (3029, 3029, 215, 'Loa Janan');
INSERT INTO `subdistricts` VALUES (3030, 3030, 215, 'Loa Kulu');
INSERT INTO `subdistricts` VALUES (3031, 3031, 215, 'Marang Kayu');
INSERT INTO `subdistricts` VALUES (3032, 3032, 215, 'Muara Badak');
INSERT INTO `subdistricts` VALUES (3033, 3033, 215, 'Muara Jawa');
INSERT INTO `subdistricts` VALUES (3034, 3034, 215, 'Muara Kaman');
INSERT INTO `subdistricts` VALUES (3035, 3035, 215, 'Muara Muntai');
INSERT INTO `subdistricts` VALUES (3036, 3036, 215, 'Muara Wis');
INSERT INTO `subdistricts` VALUES (3037, 3037, 215, 'Samboja (Semboja)');
INSERT INTO `subdistricts` VALUES (3038, 3038, 215, 'Sanga-Sanga');
INSERT INTO `subdistricts` VALUES (3039, 3039, 215, 'Sebulu');
INSERT INTO `subdistricts` VALUES (3040, 3040, 215, 'Tabang');
INSERT INTO `subdistricts` VALUES (3041, 3041, 215, 'Tenggarong');
INSERT INTO `subdistricts` VALUES (3042, 3042, 215, 'Tenggarong Seberang');
INSERT INTO `subdistricts` VALUES (3043, 3043, 216, 'Batu Ampar');
INSERT INTO `subdistricts` VALUES (3044, 3044, 216, 'Bengalon');
INSERT INTO `subdistricts` VALUES (3045, 3045, 216, 'Busang');
INSERT INTO `subdistricts` VALUES (3046, 3046, 216, 'Kaliorang');
INSERT INTO `subdistricts` VALUES (3047, 3047, 216, 'Karangan');
INSERT INTO `subdistricts` VALUES (3048, 3048, 216, 'Kaubun');
INSERT INTO `subdistricts` VALUES (3049, 3049, 216, 'Kongbeng');
INSERT INTO `subdistricts` VALUES (3050, 3050, 216, 'Long Mesangat (Mesengat)');
INSERT INTO `subdistricts` VALUES (3051, 3051, 216, 'Muara Ancalong');
INSERT INTO `subdistricts` VALUES (3052, 3052, 216, 'Muara Bengkal');
INSERT INTO `subdistricts` VALUES (3053, 3053, 216, 'Muara Wahau');
INSERT INTO `subdistricts` VALUES (3054, 3054, 216, 'Rantau Pulung');
INSERT INTO `subdistricts` VALUES (3055, 3055, 216, 'Sandaran');
INSERT INTO `subdistricts` VALUES (3056, 3056, 216, 'Sangatta Selatan');
INSERT INTO `subdistricts` VALUES (3057, 3057, 216, 'Sangatta Utara');
INSERT INTO `subdistricts` VALUES (3058, 3058, 216, 'Sangkulirang');
INSERT INTO `subdistricts` VALUES (3059, 3059, 216, 'Telen');
INSERT INTO `subdistricts` VALUES (3060, 3060, 216, 'Teluk Pandan');
INSERT INTO `subdistricts` VALUES (3061, 3061, 217, 'Bilah Barat');
INSERT INTO `subdistricts` VALUES (3062, 3062, 217, 'Bilah Hilir');
INSERT INTO `subdistricts` VALUES (3063, 3063, 217, 'Bilah Hulu');
INSERT INTO `subdistricts` VALUES (3064, 3064, 217, 'Panai Hilir');
INSERT INTO `subdistricts` VALUES (3065, 3065, 217, 'Panai Hulu');
INSERT INTO `subdistricts` VALUES (3066, 3066, 217, 'Panai Tengah');
INSERT INTO `subdistricts` VALUES (3067, 3067, 217, 'Pangkatan');
INSERT INTO `subdistricts` VALUES (3068, 3068, 217, 'Rantau Selatan');
INSERT INTO `subdistricts` VALUES (3069, 3069, 217, 'Rantau Utara');
INSERT INTO `subdistricts` VALUES (3070, 3070, 218, 'Kampung Rakyat');
INSERT INTO `subdistricts` VALUES (3071, 3071, 218, 'Kota Pinang');
INSERT INTO `subdistricts` VALUES (3072, 3072, 218, 'Sei/Sungai Kanan');
INSERT INTO `subdistricts` VALUES (3073, 3073, 218, 'Silangkitang');
INSERT INTO `subdistricts` VALUES (3074, 3074, 218, 'Torgamba');
INSERT INTO `subdistricts` VALUES (3075, 3075, 219, 'Aek Kuo');
INSERT INTO `subdistricts` VALUES (3076, 3076, 219, 'Aek Natas');
INSERT INTO `subdistricts` VALUES (3077, 3077, 219, 'Kuala Ledong (Kualuh Leidong)');
INSERT INTO `subdistricts` VALUES (3078, 3078, 219, 'Kualuh Hilir');
INSERT INTO `subdistricts` VALUES (3079, 3079, 219, 'Kualuh Hulu');
INSERT INTO `subdistricts` VALUES (3080, 3080, 219, 'Kualuh Selatan');
INSERT INTO `subdistricts` VALUES (3081, 3081, 219, 'Marbau');
INSERT INTO `subdistricts` VALUES (3082, 3082, 219, 'Na IX-X');
INSERT INTO `subdistricts` VALUES (3083, 3083, 220, 'Gumay Talang');
INSERT INTO `subdistricts` VALUES (3084, 3084, 220, 'Gumay Ulu');
INSERT INTO `subdistricts` VALUES (3085, 3085, 220, 'Jarai');
INSERT INTO `subdistricts` VALUES (3086, 3086, 220, 'Kikim Barat');
INSERT INTO `subdistricts` VALUES (3087, 3087, 220, 'Kikim Selatan');
INSERT INTO `subdistricts` VALUES (3088, 3088, 220, 'Kikim Tengah');
INSERT INTO `subdistricts` VALUES (3089, 3089, 220, 'Kikim Timur');
INSERT INTO `subdistricts` VALUES (3090, 3090, 220, 'Kota Agung');
INSERT INTO `subdistricts` VALUES (3091, 3091, 220, 'Lahat');
INSERT INTO `subdistricts` VALUES (3092, 3092, 220, 'Merapi Barat');
INSERT INTO `subdistricts` VALUES (3093, 3093, 220, 'Merapi Selatan');
INSERT INTO `subdistricts` VALUES (3094, 3094, 220, 'Merapi Timur');
INSERT INTO `subdistricts` VALUES (3095, 3095, 220, 'Muarapayang');
INSERT INTO `subdistricts` VALUES (3096, 3096, 220, 'Mulak Ulu');
INSERT INTO `subdistricts` VALUES (3097, 3097, 220, 'Pagar Gunung');
INSERT INTO `subdistricts` VALUES (3098, 3098, 220, 'Pajar Bulan');
INSERT INTO `subdistricts` VALUES (3099, 3099, 220, 'Pseksu');
INSERT INTO `subdistricts` VALUES (3100, 3100, 220, 'Pulau Pinang');
INSERT INTO `subdistricts` VALUES (3101, 3101, 220, 'Sukamerindu');
INSERT INTO `subdistricts` VALUES (3102, 3102, 220, 'Tanjung Sakti Pumi');
INSERT INTO `subdistricts` VALUES (3103, 3103, 220, 'Tanjung Sakti Pumu');
INSERT INTO `subdistricts` VALUES (3104, 3104, 220, 'Tanjung Tebat');
INSERT INTO `subdistricts` VALUES (3105, 3105, 221, 'Batangkawa');
INSERT INTO `subdistricts` VALUES (3106, 3106, 221, 'Belantikan Raya');
INSERT INTO `subdistricts` VALUES (3107, 3107, 221, 'Bulik');
INSERT INTO `subdistricts` VALUES (3108, 3108, 221, 'Bulik Timur');
INSERT INTO `subdistricts` VALUES (3109, 3109, 221, 'Delang');
INSERT INTO `subdistricts` VALUES (3110, 3110, 221, 'Lamandau');
INSERT INTO `subdistricts` VALUES (3111, 3111, 221, 'Menthobi Raya');
INSERT INTO `subdistricts` VALUES (3112, 3112, 221, 'Sematu Jaya');
INSERT INTO `subdistricts` VALUES (3113, 3113, 222, 'Babat');
INSERT INTO `subdistricts` VALUES (3114, 3114, 222, 'Bluluk');
INSERT INTO `subdistricts` VALUES (3115, 3115, 222, 'Brondong');
INSERT INTO `subdistricts` VALUES (3116, 3116, 222, 'Deket');
INSERT INTO `subdistricts` VALUES (3117, 3117, 222, 'Glagah');
INSERT INTO `subdistricts` VALUES (3118, 3118, 222, 'Kalitengah');
INSERT INTO `subdistricts` VALUES (3119, 3119, 222, 'Karang Geneng');
INSERT INTO `subdistricts` VALUES (3120, 3120, 222, 'Karangbinangun');
INSERT INTO `subdistricts` VALUES (3121, 3121, 222, 'Kedungpring');
INSERT INTO `subdistricts` VALUES (3122, 3122, 222, 'Kembangbahu');
INSERT INTO `subdistricts` VALUES (3123, 3123, 222, 'Lamongan');
INSERT INTO `subdistricts` VALUES (3124, 3124, 222, 'Laren');
INSERT INTO `subdistricts` VALUES (3125, 3125, 222, 'Maduran');
INSERT INTO `subdistricts` VALUES (3126, 3126, 222, 'Mantup');
INSERT INTO `subdistricts` VALUES (3127, 3127, 222, 'Modo');
INSERT INTO `subdistricts` VALUES (3128, 3128, 222, 'Ngimbang');
INSERT INTO `subdistricts` VALUES (3129, 3129, 222, 'Paciran');
INSERT INTO `subdistricts` VALUES (3130, 3130, 222, 'Pucuk');
INSERT INTO `subdistricts` VALUES (3131, 3131, 222, 'Sambeng');
INSERT INTO `subdistricts` VALUES (3132, 3132, 222, 'Sarirejo');
INSERT INTO `subdistricts` VALUES (3133, 3133, 222, 'Sekaran');
INSERT INTO `subdistricts` VALUES (3134, 3134, 222, 'Solokuro');
INSERT INTO `subdistricts` VALUES (3135, 3135, 222, 'Sugio');
INSERT INTO `subdistricts` VALUES (3136, 3136, 222, 'Sukodadi');
INSERT INTO `subdistricts` VALUES (3137, 3137, 222, 'Sukorame');
INSERT INTO `subdistricts` VALUES (3138, 3138, 222, 'Tikung');
INSERT INTO `subdistricts` VALUES (3139, 3139, 222, 'Turi');
INSERT INTO `subdistricts` VALUES (3140, 3140, 223, 'Air Hitam');
INSERT INTO `subdistricts` VALUES (3141, 3141, 223, 'Balik Bukit');
INSERT INTO `subdistricts` VALUES (3142, 3142, 223, 'Bandar Negeri Suoh');
INSERT INTO `subdistricts` VALUES (3143, 3143, 223, 'Batu Brak');
INSERT INTO `subdistricts` VALUES (3144, 3144, 223, 'Batu Ketulis');
INSERT INTO `subdistricts` VALUES (3145, 3145, 223, 'Belalau');
INSERT INTO `subdistricts` VALUES (3146, 3146, 223, 'Gedung Surian');
INSERT INTO `subdistricts` VALUES (3147, 3147, 223, 'Kebun Tebu');
INSERT INTO `subdistricts` VALUES (3148, 3148, 223, 'Lumbok Seminung');
INSERT INTO `subdistricts` VALUES (3149, 3149, 223, 'Pagar Dewa');
INSERT INTO `subdistricts` VALUES (3150, 3150, 223, 'Sekincau');
INSERT INTO `subdistricts` VALUES (3151, 3151, 223, 'Sukau');
INSERT INTO `subdistricts` VALUES (3152, 3152, 223, 'Sumber Jaya');
INSERT INTO `subdistricts` VALUES (3153, 3153, 223, 'Suoh');
INSERT INTO `subdistricts` VALUES (3154, 3154, 223, 'Way Tenong');
INSERT INTO `subdistricts` VALUES (3155, 3155, 224, 'Bakauheni');
INSERT INTO `subdistricts` VALUES (3156, 3156, 224, 'Candipuro');
INSERT INTO `subdistricts` VALUES (3157, 3157, 224, 'Jati Agung');
INSERT INTO `subdistricts` VALUES (3158, 3158, 224, 'Kalianda');
INSERT INTO `subdistricts` VALUES (3159, 3159, 224, 'Katibung');
INSERT INTO `subdistricts` VALUES (3160, 3160, 224, 'Ketapang');
INSERT INTO `subdistricts` VALUES (3161, 3161, 224, 'Merbau Mataram');
INSERT INTO `subdistricts` VALUES (3162, 3162, 224, 'Natar');
INSERT INTO `subdistricts` VALUES (3163, 3163, 224, 'Palas');
INSERT INTO `subdistricts` VALUES (3164, 3164, 224, 'Penengahan');
INSERT INTO `subdistricts` VALUES (3165, 3165, 224, 'Rajabasa');
INSERT INTO `subdistricts` VALUES (3166, 3166, 224, 'Sidomulyo');
INSERT INTO `subdistricts` VALUES (3167, 3167, 224, 'Sragi');
INSERT INTO `subdistricts` VALUES (3168, 3168, 224, 'Tanjung Bintang');
INSERT INTO `subdistricts` VALUES (3169, 3169, 224, 'Tanjung Sari');
INSERT INTO `subdistricts` VALUES (3170, 3170, 224, 'Way Panji');
INSERT INTO `subdistricts` VALUES (3171, 3171, 224, 'Way Sulan');
INSERT INTO `subdistricts` VALUES (3172, 3172, 225, 'Anak Ratu Aji');
INSERT INTO `subdistricts` VALUES (3173, 3173, 225, 'Anak Tuha');
INSERT INTO `subdistricts` VALUES (3174, 3174, 225, 'Bandar Mataram');
INSERT INTO `subdistricts` VALUES (3175, 3175, 225, 'Bandar Surabaya');
INSERT INTO `subdistricts` VALUES (3176, 3176, 225, 'Bangunrejo');
INSERT INTO `subdistricts` VALUES (3177, 3177, 225, 'Bekri');
INSERT INTO `subdistricts` VALUES (3178, 3178, 225, 'Bumi Nabung');
INSERT INTO `subdistricts` VALUES (3179, 3179, 225, 'Bumi Ratu Nuban');
INSERT INTO `subdistricts` VALUES (3180, 3180, 225, 'Gunung Sugih');
INSERT INTO `subdistricts` VALUES (3181, 3181, 225, 'Kalirejo');
INSERT INTO `subdistricts` VALUES (3182, 3182, 225, 'Kota Gajah');
INSERT INTO `subdistricts` VALUES (3183, 3183, 225, 'Padang Ratu');
INSERT INTO `subdistricts` VALUES (3184, 3184, 225, 'Pubian');
INSERT INTO `subdistricts` VALUES (3185, 3185, 225, 'Punggur');
INSERT INTO `subdistricts` VALUES (3186, 3186, 225, 'Putra Rumbia');
INSERT INTO `subdistricts` VALUES (3187, 3187, 225, 'Rumbia');
INSERT INTO `subdistricts` VALUES (3188, 3188, 225, 'Selagai Lingga');
INSERT INTO `subdistricts` VALUES (3189, 3189, 225, 'Sendang Agung');
INSERT INTO `subdistricts` VALUES (3190, 3190, 225, 'Seputih Agung');
INSERT INTO `subdistricts` VALUES (3191, 3191, 225, 'Seputih Banyak');
INSERT INTO `subdistricts` VALUES (3192, 3192, 225, 'Seputih Mataram');
INSERT INTO `subdistricts` VALUES (3193, 3193, 225, 'Seputih Raman');
INSERT INTO `subdistricts` VALUES (3194, 3194, 225, 'Seputih Surabaya');
INSERT INTO `subdistricts` VALUES (3195, 3195, 225, 'Terbanggi Besar');
INSERT INTO `subdistricts` VALUES (3196, 3196, 225, 'Terusan Nunyai');
INSERT INTO `subdistricts` VALUES (3197, 3197, 225, 'Trimurjo');
INSERT INTO `subdistricts` VALUES (3198, 3198, 225, 'Way Pangubuan (Pengubuan)');
INSERT INTO `subdistricts` VALUES (3199, 3199, 225, 'Way Seputih');
INSERT INTO `subdistricts` VALUES (3200, 3200, 226, 'Bandar Sribawono');
INSERT INTO `subdistricts` VALUES (3201, 3201, 226, 'Batanghari');
INSERT INTO `subdistricts` VALUES (3202, 3202, 226, 'Batanghari Nuban');
INSERT INTO `subdistricts` VALUES (3203, 3203, 226, 'Braja Slebah');
INSERT INTO `subdistricts` VALUES (3204, 3204, 226, 'Bumi Agung');
INSERT INTO `subdistricts` VALUES (3205, 3205, 226, 'Gunung Pelindung');
INSERT INTO `subdistricts` VALUES (3206, 3206, 226, 'Jabung');
INSERT INTO `subdistricts` VALUES (3207, 3207, 226, 'Labuhan Maringgai');
INSERT INTO `subdistricts` VALUES (3208, 3208, 226, 'Labuhan Ratu');
INSERT INTO `subdistricts` VALUES (3209, 3209, 226, 'Marga Sekampung');
INSERT INTO `subdistricts` VALUES (3210, 3210, 226, 'Margatiga');
INSERT INTO `subdistricts` VALUES (3211, 3211, 226, 'Mataram Baru');
INSERT INTO `subdistricts` VALUES (3212, 3212, 226, 'Melinting');
INSERT INTO `subdistricts` VALUES (3213, 3213, 226, 'Metro Kibang');
INSERT INTO `subdistricts` VALUES (3214, 3214, 226, 'Pasir Sakti');
INSERT INTO `subdistricts` VALUES (3215, 3215, 226, 'Pekalongan');
INSERT INTO `subdistricts` VALUES (3216, 3216, 226, 'Purbolinggo');
INSERT INTO `subdistricts` VALUES (3217, 3217, 226, 'Raman Utara');
INSERT INTO `subdistricts` VALUES (3218, 3218, 226, 'Sekampung');
INSERT INTO `subdistricts` VALUES (3219, 3219, 226, 'Sekampung Udik');
INSERT INTO `subdistricts` VALUES (3220, 3220, 226, 'Sukadana');
INSERT INTO `subdistricts` VALUES (3221, 3221, 226, 'Waway Karya');
INSERT INTO `subdistricts` VALUES (3222, 3222, 226, 'Way Bungur (Purbolinggo Utara)');
INSERT INTO `subdistricts` VALUES (3223, 3223, 226, 'Way Jepara');
INSERT INTO `subdistricts` VALUES (3224, 3224, 227, 'Abung Barat');
INSERT INTO `subdistricts` VALUES (3225, 3225, 227, 'Abung Kunang');
INSERT INTO `subdistricts` VALUES (3226, 3226, 227, 'Abung Pekurun');
INSERT INTO `subdistricts` VALUES (3227, 3227, 227, 'Abung Selatan');
INSERT INTO `subdistricts` VALUES (3228, 3228, 227, 'Abung Semuli');
INSERT INTO `subdistricts` VALUES (3229, 3229, 227, 'Abung Surakarta');
INSERT INTO `subdistricts` VALUES (3230, 3230, 227, 'Abung Tengah');
INSERT INTO `subdistricts` VALUES (3231, 3231, 227, 'Abung Timur');
INSERT INTO `subdistricts` VALUES (3232, 3232, 227, 'Abung Tinggi');
INSERT INTO `subdistricts` VALUES (3233, 3233, 227, 'Blambangan Pagar');
INSERT INTO `subdistricts` VALUES (3234, 3234, 227, 'Bukit Kemuning');
INSERT INTO `subdistricts` VALUES (3235, 3235, 227, 'Bunga Mayang');
INSERT INTO `subdistricts` VALUES (3236, 3236, 227, 'Hulu Sungkai');
INSERT INTO `subdistricts` VALUES (3237, 3237, 227, 'Kotabumi');
INSERT INTO `subdistricts` VALUES (3238, 3238, 227, 'Kotabumi Selatan');
INSERT INTO `subdistricts` VALUES (3239, 3239, 227, 'Kotabumi Utara');
INSERT INTO `subdistricts` VALUES (3240, 3240, 227, 'Muara Sungkai');
INSERT INTO `subdistricts` VALUES (3241, 3241, 227, 'Sungkai Barat');
INSERT INTO `subdistricts` VALUES (3242, 3242, 227, 'Sungkai Jaya');
INSERT INTO `subdistricts` VALUES (3243, 3243, 227, 'Sungkai Selatan');
INSERT INTO `subdistricts` VALUES (3244, 3244, 227, 'Sungkai Tengah');
INSERT INTO `subdistricts` VALUES (3245, 3245, 227, 'Sungkai Utara');
INSERT INTO `subdistricts` VALUES (3246, 3246, 227, 'Tanjung Raja');
INSERT INTO `subdistricts` VALUES (3247, 3247, 228, 'Air Besar');
INSERT INTO `subdistricts` VALUES (3248, 3248, 228, 'Banyuke Hulu');
INSERT INTO `subdistricts` VALUES (3249, 3249, 228, 'Jelimpo');
INSERT INTO `subdistricts` VALUES (3250, 3250, 228, 'Kuala Behe');
INSERT INTO `subdistricts` VALUES (3251, 3251, 228, 'Mandor');
INSERT INTO `subdistricts` VALUES (3252, 3252, 228, 'Mempawah Hulu');
INSERT INTO `subdistricts` VALUES (3253, 3253, 228, 'Menjalin');
INSERT INTO `subdistricts` VALUES (3254, 3254, 228, 'Menyuke');
INSERT INTO `subdistricts` VALUES (3255, 3255, 228, 'Meranti');
INSERT INTO `subdistricts` VALUES (3256, 3256, 228, 'Ngabang');
INSERT INTO `subdistricts` VALUES (3257, 3257, 228, 'Sebangki');
INSERT INTO `subdistricts` VALUES (3258, 3258, 228, 'Sengah Temila');
INSERT INTO `subdistricts` VALUES (3259, 3259, 228, 'Sompak');
INSERT INTO `subdistricts` VALUES (3260, 3260, 229, 'Babalan');
INSERT INTO `subdistricts` VALUES (3261, 3261, 229, 'Bahorok');
INSERT INTO `subdistricts` VALUES (3262, 3262, 229, 'Batang Serangan');
INSERT INTO `subdistricts` VALUES (3263, 3263, 229, 'Besitang');
INSERT INTO `subdistricts` VALUES (3264, 3264, 229, 'Binjai');
INSERT INTO `subdistricts` VALUES (3265, 3265, 229, 'Brandan Barat');
INSERT INTO `subdistricts` VALUES (3266, 3266, 229, 'Gebang');
INSERT INTO `subdistricts` VALUES (3267, 3267, 229, 'Hinai');
INSERT INTO `subdistricts` VALUES (3268, 3268, 229, 'Kuala');
INSERT INTO `subdistricts` VALUES (3269, 3269, 229, 'Kutambaru');
INSERT INTO `subdistricts` VALUES (3270, 3270, 229, 'Padang Tualang');
INSERT INTO `subdistricts` VALUES (3271, 3271, 229, 'Pangkalan Susu');
INSERT INTO `subdistricts` VALUES (3272, 3272, 229, 'Pematang Jaya');
INSERT INTO `subdistricts` VALUES (3273, 3273, 229, 'Salapian');
INSERT INTO `subdistricts` VALUES (3274, 3274, 229, 'Sawit Seberang');
INSERT INTO `subdistricts` VALUES (3275, 3275, 229, 'Secanggang');
INSERT INTO `subdistricts` VALUES (3276, 3276, 229, 'Sei Binge (Bingai)');
INSERT INTO `subdistricts` VALUES (3277, 3277, 229, 'Sei Lepan');
INSERT INTO `subdistricts` VALUES (3278, 3278, 229, 'Selesai');
INSERT INTO `subdistricts` VALUES (3279, 3279, 229, 'Sirapit (Serapit)');
INSERT INTO `subdistricts` VALUES (3280, 3280, 229, 'Stabat');
INSERT INTO `subdistricts` VALUES (3281, 3281, 229, 'Tanjungpura');
INSERT INTO `subdistricts` VALUES (3282, 3282, 229, 'Wampu');
INSERT INTO `subdistricts` VALUES (3283, 3283, 230, 'Langsa Barat');
INSERT INTO `subdistricts` VALUES (3284, 3284, 230, 'Langsa Baro');
INSERT INTO `subdistricts` VALUES (3285, 3285, 230, 'Langsa Kota');
INSERT INTO `subdistricts` VALUES (3286, 3286, 230, 'Langsa Lama');
INSERT INTO `subdistricts` VALUES (3287, 3287, 230, 'Langsa Timur');
INSERT INTO `subdistricts` VALUES (3288, 3288, 231, 'Balingga');
INSERT INTO `subdistricts` VALUES (3289, 3289, 231, 'Dimba');
INSERT INTO `subdistricts` VALUES (3290, 3290, 231, 'Gamelia');
INSERT INTO `subdistricts` VALUES (3291, 3291, 231, 'Kuyawage');
INSERT INTO `subdistricts` VALUES (3292, 3292, 231, 'Makki (Maki)');
INSERT INTO `subdistricts` VALUES (3293, 3293, 231, 'Malagaineri (Malagineri)');
INSERT INTO `subdistricts` VALUES (3294, 3294, 231, 'Pirime');
INSERT INTO `subdistricts` VALUES (3295, 3295, 231, 'Poga');
INSERT INTO `subdistricts` VALUES (3296, 3296, 231, 'Tiom');
INSERT INTO `subdistricts` VALUES (3297, 3297, 231, 'Tiomneri');
INSERT INTO `subdistricts` VALUES (3298, 3298, 232, 'Banjarsari');
INSERT INTO `subdistricts` VALUES (3299, 3299, 232, 'Bayah');
INSERT INTO `subdistricts` VALUES (3300, 3300, 232, 'Bojongmanik');
INSERT INTO `subdistricts` VALUES (3301, 3301, 232, 'Cibadak');
INSERT INTO `subdistricts` VALUES (3302, 3302, 232, 'Cibeber');
INSERT INTO `subdistricts` VALUES (3303, 3303, 232, 'Cigemblong');
INSERT INTO `subdistricts` VALUES (3304, 3304, 232, 'Cihara');
INSERT INTO `subdistricts` VALUES (3305, 3305, 232, 'Cijaku');
INSERT INTO `subdistricts` VALUES (3306, 3306, 232, 'Cikulur');
INSERT INTO `subdistricts` VALUES (3307, 3307, 232, 'Cileles');
INSERT INTO `subdistricts` VALUES (3308, 3308, 232, 'Cilograng');
INSERT INTO `subdistricts` VALUES (3309, 3309, 232, 'Cimarga');
INSERT INTO `subdistricts` VALUES (3310, 3310, 232, 'Cipanas');
INSERT INTO `subdistricts` VALUES (3311, 3311, 232, 'Cirinten');
INSERT INTO `subdistricts` VALUES (3312, 3312, 232, 'Curugbitung');
INSERT INTO `subdistricts` VALUES (3313, 3313, 232, 'Gunung Kencana');
INSERT INTO `subdistricts` VALUES (3314, 3314, 232, 'Kalanganyar');
INSERT INTO `subdistricts` VALUES (3315, 3315, 232, 'Lebakgedong');
INSERT INTO `subdistricts` VALUES (3316, 3316, 232, 'Leuwidamar');
INSERT INTO `subdistricts` VALUES (3317, 3317, 232, 'Maja');
INSERT INTO `subdistricts` VALUES (3318, 3318, 232, 'Malingping');
INSERT INTO `subdistricts` VALUES (3319, 3319, 232, 'Muncang');
INSERT INTO `subdistricts` VALUES (3320, 3320, 232, 'Panggarangan');
INSERT INTO `subdistricts` VALUES (3321, 3321, 232, 'Rangkasbitung');
INSERT INTO `subdistricts` VALUES (3322, 3322, 232, 'Sajira');
INSERT INTO `subdistricts` VALUES (3323, 3323, 232, 'Sobang');
INSERT INTO `subdistricts` VALUES (3324, 3324, 232, 'Wanasalam');
INSERT INTO `subdistricts` VALUES (3325, 3325, 232, 'Warunggunung');
INSERT INTO `subdistricts` VALUES (3326, 3326, 233, 'Amen');
INSERT INTO `subdistricts` VALUES (3327, 3327, 233, 'Bingin Kuning');
INSERT INTO `subdistricts` VALUES (3328, 3328, 233, 'Lebong Atas');
INSERT INTO `subdistricts` VALUES (3329, 3329, 233, 'Lebong Sakti');
INSERT INTO `subdistricts` VALUES (3330, 3330, 233, 'Lebong Selatan');
INSERT INTO `subdistricts` VALUES (3331, 3331, 233, 'Lebong Tengah');
INSERT INTO `subdistricts` VALUES (3332, 3332, 233, 'Lebong Utara');
INSERT INTO `subdistricts` VALUES (3333, 3333, 233, 'Pelabai');
INSERT INTO `subdistricts` VALUES (3334, 3334, 233, 'Pinang Belapis');
INSERT INTO `subdistricts` VALUES (3335, 3335, 233, 'Rimbo Pengadang');
INSERT INTO `subdistricts` VALUES (3336, 3336, 233, 'Topos');
INSERT INTO `subdistricts` VALUES (3337, 3337, 233, 'Uram Jaya');
INSERT INTO `subdistricts` VALUES (3338, 3338, 234, 'Atadei');
INSERT INTO `subdistricts` VALUES (3339, 3339, 234, 'Buyasuri (Buyasari)');
INSERT INTO `subdistricts` VALUES (3340, 3340, 234, 'Ile Ape');
INSERT INTO `subdistricts` VALUES (3341, 3341, 234, 'Ile Ape Timur');
INSERT INTO `subdistricts` VALUES (3342, 3342, 234, 'Lebatukan');
INSERT INTO `subdistricts` VALUES (3343, 3343, 234, 'Nagawutung');
INSERT INTO `subdistricts` VALUES (3344, 3344, 234, 'Nubatukan');
INSERT INTO `subdistricts` VALUES (3345, 3345, 234, 'Omesuri');
INSERT INTO `subdistricts` VALUES (3346, 3346, 234, 'Wulandoni (Wulandioni)');
INSERT INTO `subdistricts` VALUES (3347, 3347, 235, 'Banda Sakti');
INSERT INTO `subdistricts` VALUES (3348, 3348, 235, 'Blang Mangat');
INSERT INTO `subdistricts` VALUES (3349, 3349, 235, 'Muara Dua');
INSERT INTO `subdistricts` VALUES (3350, 3350, 235, 'Muara Satu');
INSERT INTO `subdistricts` VALUES (3351, 3351, 236, 'Akabiluru');
INSERT INTO `subdistricts` VALUES (3352, 3352, 236, 'Bukik Barisan');
INSERT INTO `subdistricts` VALUES (3353, 3353, 236, 'Guguak (Gugu)');
INSERT INTO `subdistricts` VALUES (3354, 3354, 236, 'Gunuang Omeh (Gunung Mas)');
INSERT INTO `subdistricts` VALUES (3355, 3355, 236, 'Harau');
INSERT INTO `subdistricts` VALUES (3356, 3356, 236, 'Kapur IX/Sembilan');
INSERT INTO `subdistricts` VALUES (3357, 3357, 236, 'Lareh Sago Halaban');
INSERT INTO `subdistricts` VALUES (3358, 3358, 236, 'Luak (Luhak)');
INSERT INTO `subdistricts` VALUES (3359, 3359, 236, 'Mungka');
INSERT INTO `subdistricts` VALUES (3360, 3360, 236, 'Pangkalan Koto Baru');
INSERT INTO `subdistricts` VALUES (3361, 3361, 236, 'Payakumbuh');
INSERT INTO `subdistricts` VALUES (3362, 3362, 236, 'Situjuah Limo/Lima Nagari');
INSERT INTO `subdistricts` VALUES (3363, 3363, 236, 'Suliki');
INSERT INTO `subdistricts` VALUES (3364, 3364, 237, 'Lingga');
INSERT INTO `subdistricts` VALUES (3365, 3365, 237, 'Lingga Timur');
INSERT INTO `subdistricts` VALUES (3366, 3366, 237, 'Lingga Utara');
INSERT INTO `subdistricts` VALUES (3367, 3367, 237, 'Selayar');
INSERT INTO `subdistricts` VALUES (3368, 3368, 237, 'Senayang');
INSERT INTO `subdistricts` VALUES (3369, 3369, 237, 'Singkep');
INSERT INTO `subdistricts` VALUES (3370, 3370, 237, 'Singkep Barat');
INSERT INTO `subdistricts` VALUES (3371, 3371, 237, 'Singkep Pesisir');
INSERT INTO `subdistricts` VALUES (3372, 3372, 238, 'Batu Layar');
INSERT INTO `subdistricts` VALUES (3373, 3373, 238, 'Gerung');
INSERT INTO `subdistricts` VALUES (3374, 3374, 238, 'Gunungsari');
INSERT INTO `subdistricts` VALUES (3375, 3375, 238, 'Kediri');
INSERT INTO `subdistricts` VALUES (3376, 3376, 238, 'Kuripan');
INSERT INTO `subdistricts` VALUES (3377, 3377, 238, 'Labuapi');
INSERT INTO `subdistricts` VALUES (3378, 3378, 238, 'Lembar');
INSERT INTO `subdistricts` VALUES (3379, 3379, 238, 'Lingsar');
INSERT INTO `subdistricts` VALUES (3380, 3380, 238, 'Narmada');
INSERT INTO `subdistricts` VALUES (3381, 3381, 238, 'Sekotong');
INSERT INTO `subdistricts` VALUES (3382, 3382, 239, 'Batukliang');
INSERT INTO `subdistricts` VALUES (3383, 3383, 239, 'Batukliang Utara');
INSERT INTO `subdistricts` VALUES (3384, 3384, 239, 'Janapria');
INSERT INTO `subdistricts` VALUES (3385, 3385, 239, 'Jonggat');
INSERT INTO `subdistricts` VALUES (3386, 3386, 239, 'Kopang');
INSERT INTO `subdistricts` VALUES (3387, 3387, 239, 'Praya');
INSERT INTO `subdistricts` VALUES (3388, 3388, 239, 'Praya Barat');
INSERT INTO `subdistricts` VALUES (3389, 3389, 239, 'Praya Barat Daya');
INSERT INTO `subdistricts` VALUES (3390, 3390, 239, 'Praya Tengah');
INSERT INTO `subdistricts` VALUES (3391, 3391, 239, 'Praya Timur');
INSERT INTO `subdistricts` VALUES (3392, 3392, 239, 'Pringgarata');
INSERT INTO `subdistricts` VALUES (3393, 3393, 239, 'Pujut');
INSERT INTO `subdistricts` VALUES (3394, 3394, 240, 'Aikmel');
INSERT INTO `subdistricts` VALUES (3395, 3395, 240, 'Jerowaru');
INSERT INTO `subdistricts` VALUES (3396, 3396, 240, 'Keruak');
INSERT INTO `subdistricts` VALUES (3397, 3397, 240, 'Labuhan Haji');
INSERT INTO `subdistricts` VALUES (3398, 3398, 240, 'Masbagik');
INSERT INTO `subdistricts` VALUES (3399, 3399, 240, 'Montong Gading');
INSERT INTO `subdistricts` VALUES (3400, 3400, 240, 'Pringgabaya');
INSERT INTO `subdistricts` VALUES (3401, 3401, 240, 'Pringgasela');
INSERT INTO `subdistricts` VALUES (3402, 3402, 240, 'Sakra');
INSERT INTO `subdistricts` VALUES (3403, 3403, 240, 'Sakra Barat');
INSERT INTO `subdistricts` VALUES (3404, 3404, 240, 'Sakra Timur');
INSERT INTO `subdistricts` VALUES (3405, 3405, 240, 'Sambalia (Sambelia)');
INSERT INTO `subdistricts` VALUES (3406, 3406, 240, 'Selong');
INSERT INTO `subdistricts` VALUES (3407, 3407, 240, 'Sembalun');
INSERT INTO `subdistricts` VALUES (3408, 3408, 240, 'Sikur');
INSERT INTO `subdistricts` VALUES (3409, 3409, 240, 'Suela (Suwela)');
INSERT INTO `subdistricts` VALUES (3410, 3410, 240, 'Sukamulia');
INSERT INTO `subdistricts` VALUES (3411, 3411, 240, 'Suralaga');
INSERT INTO `subdistricts` VALUES (3412, 3412, 240, 'Terara');
INSERT INTO `subdistricts` VALUES (3413, 3413, 240, 'Wanasaba');
INSERT INTO `subdistricts` VALUES (3414, 3414, 241, 'Bayan');
INSERT INTO `subdistricts` VALUES (3415, 3415, 241, 'Gangga');
INSERT INTO `subdistricts` VALUES (3416, 3416, 241, 'Kayangan');
INSERT INTO `subdistricts` VALUES (3417, 3417, 241, 'Pemenang');
INSERT INTO `subdistricts` VALUES (3418, 3418, 241, 'Tanjung');
INSERT INTO `subdistricts` VALUES (3419, 3419, 242, 'Lubuk Linggau Barat Dua (II)');
INSERT INTO `subdistricts` VALUES (3420, 3420, 242, 'Lubuk Linggau Barat Satu (I)');
INSERT INTO `subdistricts` VALUES (3421, 3421, 242, 'Lubuk Linggau Selatan Dua (II)');
INSERT INTO `subdistricts` VALUES (3422, 3422, 242, 'Lubuk Linggau Selatan Satu (I)');
INSERT INTO `subdistricts` VALUES (3423, 3423, 242, 'Lubuk Linggau Timur Dua (II)');
INSERT INTO `subdistricts` VALUES (3424, 3424, 242, 'Lubuk Linggau Timur Satu (I)');
INSERT INTO `subdistricts` VALUES (3425, 3425, 242, 'Lubuk Linggau Utara Dua (II)');
INSERT INTO `subdistricts` VALUES (3426, 3426, 242, 'Lubuk Linggau Utara Satu (I)');
INSERT INTO `subdistricts` VALUES (3427, 3427, 243, 'Candipuro');
INSERT INTO `subdistricts` VALUES (3428, 3428, 243, 'Gucialit');
INSERT INTO `subdistricts` VALUES (3429, 3429, 243, 'Jatiroto');
INSERT INTO `subdistricts` VALUES (3430, 3430, 243, 'Kedungjajang');
INSERT INTO `subdistricts` VALUES (3431, 3431, 243, 'Klakah');
INSERT INTO `subdistricts` VALUES (3432, 3432, 243, 'Kunir');
INSERT INTO `subdistricts` VALUES (3433, 3433, 243, 'Lumajang');
INSERT INTO `subdistricts` VALUES (3434, 3434, 243, 'Padang');
INSERT INTO `subdistricts` VALUES (3435, 3435, 243, 'Pasirian');
INSERT INTO `subdistricts` VALUES (3436, 3436, 243, 'Pasrujambe/Pasujambe');
INSERT INTO `subdistricts` VALUES (3437, 3437, 243, 'Pronojiwo');
INSERT INTO `subdistricts` VALUES (3438, 3438, 243, 'Randuagung');
INSERT INTO `subdistricts` VALUES (3439, 3439, 243, 'Ranuyoso');
INSERT INTO `subdistricts` VALUES (3440, 3440, 243, 'Rowokangkung');
INSERT INTO `subdistricts` VALUES (3441, 3441, 243, 'Senduro');
INSERT INTO `subdistricts` VALUES (3442, 3442, 243, 'Sukodono');
INSERT INTO `subdistricts` VALUES (3443, 3443, 243, 'Sumbersuko');
INSERT INTO `subdistricts` VALUES (3444, 3444, 243, 'Tekung');
INSERT INTO `subdistricts` VALUES (3445, 3445, 243, 'Tempeh');
INSERT INTO `subdistricts` VALUES (3446, 3446, 243, 'Tempursari');
INSERT INTO `subdistricts` VALUES (3447, 3447, 243, 'Yosowilangun');
INSERT INTO `subdistricts` VALUES (3448, 3448, 244, 'Bajo');
INSERT INTO `subdistricts` VALUES (3449, 3449, 244, 'Bajo Barat');
INSERT INTO `subdistricts` VALUES (3450, 3450, 244, 'Basse Sangtempe Utara');
INSERT INTO `subdistricts` VALUES (3451, 3451, 244, 'Bassesang Tempe (Bastem)');
INSERT INTO `subdistricts` VALUES (3452, 3452, 244, 'Belopa');
INSERT INTO `subdistricts` VALUES (3453, 3453, 244, 'Belopa Utara');
INSERT INTO `subdistricts` VALUES (3454, 3454, 244, 'Bua');
INSERT INTO `subdistricts` VALUES (3455, 3455, 244, 'Bua Ponrang (Bupon)');
INSERT INTO `subdistricts` VALUES (3456, 3456, 244, 'Kamanre');
INSERT INTO `subdistricts` VALUES (3457, 3457, 244, 'Lamasi');
INSERT INTO `subdistricts` VALUES (3458, 3458, 244, 'Lamasi Timur');
INSERT INTO `subdistricts` VALUES (3459, 3459, 244, 'Larompong');
INSERT INTO `subdistricts` VALUES (3460, 3460, 244, 'Larompong Selatan');
INSERT INTO `subdistricts` VALUES (3461, 3461, 244, 'Latimojong');
INSERT INTO `subdistricts` VALUES (3462, 3462, 244, 'Ponrang');
INSERT INTO `subdistricts` VALUES (3463, 3463, 244, 'Ponrang Selatan');
INSERT INTO `subdistricts` VALUES (3464, 3464, 244, 'Suli');
INSERT INTO `subdistricts` VALUES (3465, 3465, 244, 'Suli Barat');
INSERT INTO `subdistricts` VALUES (3466, 3466, 244, 'Walenrang');
INSERT INTO `subdistricts` VALUES (3467, 3467, 244, 'Walenrang Barat');
INSERT INTO `subdistricts` VALUES (3468, 3468, 244, 'Walenrang Timur');
INSERT INTO `subdistricts` VALUES (3469, 3469, 244, 'Walenrang Utara');
INSERT INTO `subdistricts` VALUES (3470, 3470, 245, 'Angkona');
INSERT INTO `subdistricts` VALUES (3471, 3471, 245, 'Burau');
INSERT INTO `subdistricts` VALUES (3472, 3472, 245, 'Kalaena');
INSERT INTO `subdistricts` VALUES (3473, 3473, 245, 'Malili');
INSERT INTO `subdistricts` VALUES (3474, 3474, 245, 'Mangkutana');
INSERT INTO `subdistricts` VALUES (3475, 3475, 245, 'Nuha');
INSERT INTO `subdistricts` VALUES (3476, 3476, 245, 'Tomoni');
INSERT INTO `subdistricts` VALUES (3477, 3477, 245, 'Tomoni Timur');
INSERT INTO `subdistricts` VALUES (3478, 3478, 245, 'Towuti');
INSERT INTO `subdistricts` VALUES (3479, 3479, 245, 'Wasuponda');
INSERT INTO `subdistricts` VALUES (3480, 3480, 245, 'Wotu');
INSERT INTO `subdistricts` VALUES (3481, 3481, 246, 'Baebunta');
INSERT INTO `subdistricts` VALUES (3482, 3482, 246, 'Bone-Bone');
INSERT INTO `subdistricts` VALUES (3483, 3483, 246, 'Limbong');
INSERT INTO `subdistricts` VALUES (3484, 3484, 246, 'Malangke');
INSERT INTO `subdistricts` VALUES (3485, 3485, 246, 'Malangke Barat');
INSERT INTO `subdistricts` VALUES (3486, 3486, 246, 'Mappedeceng');
INSERT INTO `subdistricts` VALUES (3487, 3487, 246, 'Masamba');
INSERT INTO `subdistricts` VALUES (3488, 3488, 246, 'Rampi');
INSERT INTO `subdistricts` VALUES (3489, 3489, 246, 'Sabbang');
INSERT INTO `subdistricts` VALUES (3490, 3490, 246, 'Seko');
INSERT INTO `subdistricts` VALUES (3491, 3491, 246, 'Sukamaju');
INSERT INTO `subdistricts` VALUES (3492, 3492, 246, 'Tana Lili');
INSERT INTO `subdistricts` VALUES (3493, 3493, 247, 'Balerejo');
INSERT INTO `subdistricts` VALUES (3494, 3494, 247, 'Dagangan');
INSERT INTO `subdistricts` VALUES (3495, 3495, 247, 'Dolopo');
INSERT INTO `subdistricts` VALUES (3496, 3496, 247, 'Geger');
INSERT INTO `subdistricts` VALUES (3497, 3497, 247, 'Gemarang');
INSERT INTO `subdistricts` VALUES (3498, 3498, 247, 'Jiwan');
INSERT INTO `subdistricts` VALUES (3499, 3499, 247, 'Kare');
INSERT INTO `subdistricts` VALUES (3500, 3500, 247, 'Kebonsari');
INSERT INTO `subdistricts` VALUES (3501, 3501, 247, 'Madiun');
INSERT INTO `subdistricts` VALUES (3502, 3502, 247, 'Mejayan');
INSERT INTO `subdistricts` VALUES (3503, 3503, 247, 'Pilangkenceng');
INSERT INTO `subdistricts` VALUES (3504, 3504, 247, 'Saradan');
INSERT INTO `subdistricts` VALUES (3505, 3505, 247, 'Sawahan');
INSERT INTO `subdistricts` VALUES (3506, 3506, 247, 'Wonoasri');
INSERT INTO `subdistricts` VALUES (3507, 3507, 247, 'Wungu');
INSERT INTO `subdistricts` VALUES (3508, 3508, 248, 'Kartoharjo');
INSERT INTO `subdistricts` VALUES (3509, 3509, 248, 'Manguharjo');
INSERT INTO `subdistricts` VALUES (3510, 3510, 248, 'Taman');
INSERT INTO `subdistricts` VALUES (3511, 3511, 249, 'Bandongan');
INSERT INTO `subdistricts` VALUES (3512, 3512, 249, 'Borobudur');
INSERT INTO `subdistricts` VALUES (3513, 3513, 249, 'Candimulyo');
INSERT INTO `subdistricts` VALUES (3514, 3514, 249, 'Dukun');
INSERT INTO `subdistricts` VALUES (3515, 3515, 249, 'Grabag');
INSERT INTO `subdistricts` VALUES (3516, 3516, 249, 'Kajoran');
INSERT INTO `subdistricts` VALUES (3517, 3517, 249, 'Kaliangkrik');
INSERT INTO `subdistricts` VALUES (3518, 3518, 249, 'Mertoyudan');
INSERT INTO `subdistricts` VALUES (3519, 3519, 249, 'Mungkid');
INSERT INTO `subdistricts` VALUES (3520, 3520, 249, 'Muntilan');
INSERT INTO `subdistricts` VALUES (3521, 3521, 249, 'Ngablak');
INSERT INTO `subdistricts` VALUES (3522, 3522, 249, 'Ngluwar');
INSERT INTO `subdistricts` VALUES (3523, 3523, 249, 'Pakis');
INSERT INTO `subdistricts` VALUES (3524, 3524, 249, 'Salam');
INSERT INTO `subdistricts` VALUES (3525, 3525, 249, 'Salaman');
INSERT INTO `subdistricts` VALUES (3526, 3526, 249, 'Sawangan');
INSERT INTO `subdistricts` VALUES (3527, 3527, 249, 'Secang');
INSERT INTO `subdistricts` VALUES (3528, 3528, 249, 'Srumbung');
INSERT INTO `subdistricts` VALUES (3529, 3529, 249, 'Tegalrejo');
INSERT INTO `subdistricts` VALUES (3530, 3530, 249, 'Tempuran');
INSERT INTO `subdistricts` VALUES (3531, 3531, 249, 'Windusari');
INSERT INTO `subdistricts` VALUES (3532, 3532, 250, 'Magelang Selatan');
INSERT INTO `subdistricts` VALUES (3533, 3533, 250, 'Magelang Tengah');
INSERT INTO `subdistricts` VALUES (3534, 3534, 250, 'Magelang Utara');
INSERT INTO `subdistricts` VALUES (3535, 3535, 251, 'Barat');
INSERT INTO `subdistricts` VALUES (3536, 3536, 251, 'Bendo');
INSERT INTO `subdistricts` VALUES (3537, 3537, 251, 'Karangrejo');
INSERT INTO `subdistricts` VALUES (3538, 3538, 251, 'Karas');
INSERT INTO `subdistricts` VALUES (3539, 3539, 251, 'Kartoharjo (Kertoharjo)');
INSERT INTO `subdistricts` VALUES (3540, 3540, 251, 'Kawedanan');
INSERT INTO `subdistricts` VALUES (3541, 3541, 251, 'Lembeyan');
INSERT INTO `subdistricts` VALUES (3542, 3542, 251, 'Magetan');
INSERT INTO `subdistricts` VALUES (3543, 3543, 251, 'Maospati');
INSERT INTO `subdistricts` VALUES (3544, 3544, 251, 'Ngariboyo');
INSERT INTO `subdistricts` VALUES (3545, 3545, 251, 'Nguntoronadi');
INSERT INTO `subdistricts` VALUES (3546, 3546, 251, 'Panekan');
INSERT INTO `subdistricts` VALUES (3547, 3547, 251, 'Parang');
INSERT INTO `subdistricts` VALUES (3548, 3548, 251, 'Plaosan');
INSERT INTO `subdistricts` VALUES (3549, 3549, 251, 'Poncol');
INSERT INTO `subdistricts` VALUES (3550, 3550, 251, 'Sidorejo');
INSERT INTO `subdistricts` VALUES (3551, 3551, 251, 'Sukomoro');
INSERT INTO `subdistricts` VALUES (3552, 3552, 251, 'Takeran');
INSERT INTO `subdistricts` VALUES (3553, 3553, 252, 'Argapura');
INSERT INTO `subdistricts` VALUES (3554, 3554, 252, 'Banjaran');
INSERT INTO `subdistricts` VALUES (3555, 3555, 252, 'Bantarujeg');
INSERT INTO `subdistricts` VALUES (3556, 3556, 252, 'Cigasong');
INSERT INTO `subdistricts` VALUES (3557, 3557, 252, 'Cikijing');
INSERT INTO `subdistricts` VALUES (3558, 3558, 252, 'Cingambul');
INSERT INTO `subdistricts` VALUES (3559, 3559, 252, 'Dawuan');
INSERT INTO `subdistricts` VALUES (3560, 3560, 252, 'Jatitujuh');
INSERT INTO `subdistricts` VALUES (3561, 3561, 252, 'Jatiwangi');
INSERT INTO `subdistricts` VALUES (3562, 3562, 252, 'Kadipaten');
INSERT INTO `subdistricts` VALUES (3563, 3563, 252, 'Kasokandel');
INSERT INTO `subdistricts` VALUES (3564, 3564, 252, 'Kertajati');
INSERT INTO `subdistricts` VALUES (3565, 3565, 252, 'Lemahsugih');
INSERT INTO `subdistricts` VALUES (3566, 3566, 252, 'Leuwimunding');
INSERT INTO `subdistricts` VALUES (3567, 3567, 252, 'Ligung');
INSERT INTO `subdistricts` VALUES (3568, 3568, 252, 'Maja');
INSERT INTO `subdistricts` VALUES (3569, 3569, 252, 'Majalengka');
INSERT INTO `subdistricts` VALUES (3570, 3570, 252, 'Malausma');
INSERT INTO `subdistricts` VALUES (3571, 3571, 252, 'Palasah');
INSERT INTO `subdistricts` VALUES (3572, 3572, 252, 'Panyingkiran');
INSERT INTO `subdistricts` VALUES (3573, 3573, 252, 'Rajagaluh');
INSERT INTO `subdistricts` VALUES (3574, 3574, 252, 'Sindang');
INSERT INTO `subdistricts` VALUES (3575, 3575, 252, 'Sindangwangi');
INSERT INTO `subdistricts` VALUES (3576, 3576, 252, 'Sukahaji');
INSERT INTO `subdistricts` VALUES (3577, 3577, 252, 'Sumberjaya');
INSERT INTO `subdistricts` VALUES (3578, 3578, 252, 'Talaga');
INSERT INTO `subdistricts` VALUES (3579, 3579, 253, 'Banggae');
INSERT INTO `subdistricts` VALUES (3580, 3580, 253, 'Banggae Timur');
INSERT INTO `subdistricts` VALUES (3581, 3581, 253, 'Malunda');
INSERT INTO `subdistricts` VALUES (3582, 3582, 253, 'Pamboang');
INSERT INTO `subdistricts` VALUES (3583, 3583, 253, 'Sendana');
INSERT INTO `subdistricts` VALUES (3584, 3584, 253, 'Tammeredo Sendana');
INSERT INTO `subdistricts` VALUES (3585, 3585, 253, 'Tubo (Tubo Sendana)');
INSERT INTO `subdistricts` VALUES (3586, 3586, 253, 'Ulumunda');
INSERT INTO `subdistricts` VALUES (3587, 3587, 254, 'Biring Kanaya');
INSERT INTO `subdistricts` VALUES (3588, 3588, 254, 'Bontoala');
INSERT INTO `subdistricts` VALUES (3589, 3589, 254, 'Makassar');
INSERT INTO `subdistricts` VALUES (3590, 3590, 254, 'Mamajang');
INSERT INTO `subdistricts` VALUES (3591, 3591, 254, 'Manggala');
INSERT INTO `subdistricts` VALUES (3592, 3592, 254, 'Mariso');
INSERT INTO `subdistricts` VALUES (3593, 3593, 254, 'Panakkukang');
INSERT INTO `subdistricts` VALUES (3594, 3594, 254, 'Rappocini');
INSERT INTO `subdistricts` VALUES (3595, 3595, 254, 'Tallo');
INSERT INTO `subdistricts` VALUES (3596, 3596, 254, 'Tamalanrea');
INSERT INTO `subdistricts` VALUES (3597, 3597, 254, 'Tamalate');
INSERT INTO `subdistricts` VALUES (3598, 3598, 254, 'Ujung Pandang');
INSERT INTO `subdistricts` VALUES (3599, 3599, 254, 'Ujung Tanah');
INSERT INTO `subdistricts` VALUES (3600, 3600, 254, 'Wajo');
INSERT INTO `subdistricts` VALUES (3601, 3601, 255, 'Ampelgading');
INSERT INTO `subdistricts` VALUES (3602, 3602, 255, 'Bantur');
INSERT INTO `subdistricts` VALUES (3603, 3603, 255, 'Bululawang');
INSERT INTO `subdistricts` VALUES (3604, 3604, 255, 'Dampit');
INSERT INTO `subdistricts` VALUES (3605, 3605, 255, 'Dau');
INSERT INTO `subdistricts` VALUES (3606, 3606, 255, 'Donomulyo');
INSERT INTO `subdistricts` VALUES (3607, 3607, 255, 'Gedangan');
INSERT INTO `subdistricts` VALUES (3608, 3608, 255, 'Gondanglegi');
INSERT INTO `subdistricts` VALUES (3609, 3609, 255, 'Jabung');
INSERT INTO `subdistricts` VALUES (3610, 3610, 255, 'Kalipare');
INSERT INTO `subdistricts` VALUES (3611, 3611, 255, 'Karangploso');
INSERT INTO `subdistricts` VALUES (3612, 3612, 255, 'Kasembon');
INSERT INTO `subdistricts` VALUES (3613, 3613, 255, 'Kepanjen');
INSERT INTO `subdistricts` VALUES (3614, 3614, 255, 'Kromengan');
INSERT INTO `subdistricts` VALUES (3615, 3615, 255, 'Lawang');
INSERT INTO `subdistricts` VALUES (3616, 3616, 255, 'Ngajung (Ngajum)');
INSERT INTO `subdistricts` VALUES (3617, 3617, 255, 'Ngantang');
INSERT INTO `subdistricts` VALUES (3618, 3618, 255, 'Pagak');
INSERT INTO `subdistricts` VALUES (3619, 3619, 255, 'Pagelaran');
INSERT INTO `subdistricts` VALUES (3620, 3620, 255, 'Pakis');
INSERT INTO `subdistricts` VALUES (3621, 3621, 255, 'Pakisaji');
INSERT INTO `subdistricts` VALUES (3622, 3622, 255, 'Poncokusumo');
INSERT INTO `subdistricts` VALUES (3623, 3623, 255, 'Pujon');
INSERT INTO `subdistricts` VALUES (3624, 3624, 255, 'Singosari');
INSERT INTO `subdistricts` VALUES (3625, 3625, 255, 'Sumbermanjing Wetan');
INSERT INTO `subdistricts` VALUES (3626, 3626, 255, 'Sumberpucung');
INSERT INTO `subdistricts` VALUES (3627, 3627, 255, 'Tajinan');
INSERT INTO `subdistricts` VALUES (3628, 3628, 255, 'Tirtoyudo');
INSERT INTO `subdistricts` VALUES (3629, 3629, 255, 'Tumpang');
INSERT INTO `subdistricts` VALUES (3630, 3630, 255, 'Turen');
INSERT INTO `subdistricts` VALUES (3631, 3631, 255, 'Wagir');
INSERT INTO `subdistricts` VALUES (3632, 3632, 255, 'Wajak');
INSERT INTO `subdistricts` VALUES (3633, 3633, 255, 'Wonosari');
INSERT INTO `subdistricts` VALUES (3634, 3634, 256, 'Blimbing');
INSERT INTO `subdistricts` VALUES (3635, 3635, 256, 'Kedungkandang');
INSERT INTO `subdistricts` VALUES (3636, 3636, 256, 'Klojen');
INSERT INTO `subdistricts` VALUES (3637, 3637, 256, 'Lowokwaru');
INSERT INTO `subdistricts` VALUES (3638, 3638, 256, 'Sukun');
INSERT INTO `subdistricts` VALUES (3639, 3639, 257, 'Bahau Hulu');
INSERT INTO `subdistricts` VALUES (3640, 3640, 257, 'Kayan Hilir');
INSERT INTO `subdistricts` VALUES (3641, 3641, 257, 'Kayan Hulu');
INSERT INTO `subdistricts` VALUES (3642, 3642, 257, 'Kayan Selatan');
INSERT INTO `subdistricts` VALUES (3643, 3643, 257, 'Malinau Barat');
INSERT INTO `subdistricts` VALUES (3644, 3644, 257, 'Malinau Kota');
INSERT INTO `subdistricts` VALUES (3645, 3645, 257, 'Malinau Selatan');
INSERT INTO `subdistricts` VALUES (3646, 3646, 257, 'Malinau Selatan Hilir');
INSERT INTO `subdistricts` VALUES (3647, 3647, 257, 'Malinau Selatan Hulu');
INSERT INTO `subdistricts` VALUES (3648, 3648, 257, 'Malinau Utara');
INSERT INTO `subdistricts` VALUES (3649, 3649, 257, 'Mentarang');
INSERT INTO `subdistricts` VALUES (3650, 3650, 257, 'Mentarang Hulu');
INSERT INTO `subdistricts` VALUES (3651, 3651, 257, 'Pujungan');
INSERT INTO `subdistricts` VALUES (3652, 3652, 257, 'Sungai Boh');
INSERT INTO `subdistricts` VALUES (3653, 3653, 257, 'Sungai Tubu');
INSERT INTO `subdistricts` VALUES (3654, 3654, 258, 'Damer');
INSERT INTO `subdistricts` VALUES (3655, 3655, 258, 'Dawelor Dawera');
INSERT INTO `subdistricts` VALUES (3656, 3656, 258, 'Kepulauan Romang');
INSERT INTO `subdistricts` VALUES (3657, 3657, 258, 'Kisar Utara');
INSERT INTO `subdistricts` VALUES (3658, 3658, 258, 'Mdona Hyera/Hiera');
INSERT INTO `subdistricts` VALUES (3659, 3659, 258, 'Moa Lakor');
INSERT INTO `subdistricts` VALUES (3660, 3660, 258, 'Pulau Lakor');
INSERT INTO `subdistricts` VALUES (3661, 3661, 258, 'Pulau Letti (Leti Moa Lakor)');
INSERT INTO `subdistricts` VALUES (3662, 3662, 258, 'Pulau Masela');
INSERT INTO `subdistricts` VALUES (3663, 3663, 258, 'Pulau Pulau Babar');
INSERT INTO `subdistricts` VALUES (3664, 3664, 258, 'Pulau Pulau Terselatan');
INSERT INTO `subdistricts` VALUES (3665, 3665, 258, 'Pulau Wetang');
INSERT INTO `subdistricts` VALUES (3666, 3666, 258, 'Pulau-Pulau Babar Timur');
INSERT INTO `subdistricts` VALUES (3667, 3667, 258, 'Wetar');
INSERT INTO `subdistricts` VALUES (3668, 3668, 258, 'Wetar Barat');
INSERT INTO `subdistricts` VALUES (3669, 3669, 258, 'Wetar Timur');
INSERT INTO `subdistricts` VALUES (3670, 3670, 258, 'Wetar Utara');
INSERT INTO `subdistricts` VALUES (3671, 3671, 259, 'Amahai');
INSERT INTO `subdistricts` VALUES (3672, 3672, 259, 'Banda');
INSERT INTO `subdistricts` VALUES (3673, 3673, 259, 'Leihitu');
INSERT INTO `subdistricts` VALUES (3674, 3674, 259, 'Leihitu Barat');
INSERT INTO `subdistricts` VALUES (3675, 3675, 259, 'Masohi Kota');
INSERT INTO `subdistricts` VALUES (3676, 3676, 259, 'Nusalaut');
INSERT INTO `subdistricts` VALUES (3677, 3677, 259, 'Pulau Haruku');
INSERT INTO `subdistricts` VALUES (3678, 3678, 259, 'Salahutu');
INSERT INTO `subdistricts` VALUES (3679, 3679, 259, 'Saparua');
INSERT INTO `subdistricts` VALUES (3680, 3680, 259, 'Saparua Timur');
INSERT INTO `subdistricts` VALUES (3681, 3681, 259, 'Seram Utara');
INSERT INTO `subdistricts` VALUES (3682, 3682, 259, 'Seram Utara Barat');
INSERT INTO `subdistricts` VALUES (3683, 3683, 259, 'Seram Utara Timur Kobi');
INSERT INTO `subdistricts` VALUES (3684, 3684, 259, 'Seram Utara Timur Seti');
INSERT INTO `subdistricts` VALUES (3685, 3685, 259, 'Tehoru');
INSERT INTO `subdistricts` VALUES (3686, 3686, 259, 'Teluk Elpaputih');
INSERT INTO `subdistricts` VALUES (3687, 3687, 259, 'Telutih');
INSERT INTO `subdistricts` VALUES (3688, 3688, 259, 'Teon Nila Serua');
INSERT INTO `subdistricts` VALUES (3689, 3689, 260, 'Hoat Sorbay');
INSERT INTO `subdistricts` VALUES (3690, 3690, 260, 'Kei Besar');
INSERT INTO `subdistricts` VALUES (3691, 3691, 260, 'Kei Besar Selatan');
INSERT INTO `subdistricts` VALUES (3692, 3692, 260, 'Kei Besar Selatan Barat');
INSERT INTO `subdistricts` VALUES (3693, 3693, 260, 'Kei Besar Utara Barat');
INSERT INTO `subdistricts` VALUES (3694, 3694, 260, 'Kei Besar Utara Timur');
INSERT INTO `subdistricts` VALUES (3695, 3695, 260, 'Kei Kecil');
INSERT INTO `subdistricts` VALUES (3696, 3696, 260, 'Kei Kecil Barat');
INSERT INTO `subdistricts` VALUES (3697, 3697, 260, 'Kei Kecil Timur');
INSERT INTO `subdistricts` VALUES (3698, 3698, 260, 'Kei Kecil Timur Selatan');
INSERT INTO `subdistricts` VALUES (3699, 3699, 260, 'Manyeuw');
INSERT INTO `subdistricts` VALUES (3700, 3700, 261, 'Kormomolin');
INSERT INTO `subdistricts` VALUES (3701, 3701, 261, 'Molu Maru');
INSERT INTO `subdistricts` VALUES (3702, 3702, 261, 'Nirunmas');
INSERT INTO `subdistricts` VALUES (3703, 3703, 261, 'Selaru');
INSERT INTO `subdistricts` VALUES (3704, 3704, 261, 'Tanimbar Selatan');
INSERT INTO `subdistricts` VALUES (3705, 3705, 261, 'Tanimbar Utara');
INSERT INTO `subdistricts` VALUES (3706, 3706, 261, 'Wermakatian (Wer Maktian)');
INSERT INTO `subdistricts` VALUES (3707, 3707, 261, 'Wertamrian');
INSERT INTO `subdistricts` VALUES (3708, 3708, 261, 'Wuarlabobar');
INSERT INTO `subdistricts` VALUES (3709, 3709, 261, 'Yaru');
INSERT INTO `subdistricts` VALUES (3710, 3710, 262, 'Aralle (Arrale)');
INSERT INTO `subdistricts` VALUES (3711, 3711, 262, 'Balla');
INSERT INTO `subdistricts` VALUES (3712, 3712, 262, 'Bambang');
INSERT INTO `subdistricts` VALUES (3713, 3713, 262, 'Buntumalangka');
INSERT INTO `subdistricts` VALUES (3714, 3714, 262, 'Mamasa');
INSERT INTO `subdistricts` VALUES (3715, 3715, 262, 'Mambi');
INSERT INTO `subdistricts` VALUES (3716, 3716, 262, 'Mehalaan');
INSERT INTO `subdistricts` VALUES (3717, 3717, 262, 'Messawa');
INSERT INTO `subdistricts` VALUES (3718, 3718, 262, 'Nosu');
INSERT INTO `subdistricts` VALUES (3719, 3719, 262, 'Pana');
INSERT INTO `subdistricts` VALUES (3720, 3720, 262, 'Rantebulahan Timur');
INSERT INTO `subdistricts` VALUES (3721, 3721, 262, 'Sesena Padang');
INSERT INTO `subdistricts` VALUES (3722, 3722, 262, 'Sumarorong');
INSERT INTO `subdistricts` VALUES (3723, 3723, 262, 'Tabang');
INSERT INTO `subdistricts` VALUES (3724, 3724, 262, 'Tabulahan');
INSERT INTO `subdistricts` VALUES (3725, 3725, 262, 'Tanduk Kalua');
INSERT INTO `subdistricts` VALUES (3726, 3726, 262, 'Tawalian');
INSERT INTO `subdistricts` VALUES (3727, 3727, 263, 'Benuki');
INSERT INTO `subdistricts` VALUES (3728, 3728, 263, 'Mamberamo Hilir/Ilir');
INSERT INTO `subdistricts` VALUES (3729, 3729, 263, 'Mamberamo Hulu/Ulu');
INSERT INTO `subdistricts` VALUES (3730, 3730, 263, 'Mamberamo Tengah');
INSERT INTO `subdistricts` VALUES (3731, 3731, 263, 'Mamberamo Tengah Timur');
INSERT INTO `subdistricts` VALUES (3732, 3732, 263, 'Rofaer (Rufaer)');
INSERT INTO `subdistricts` VALUES (3733, 3733, 263, 'Sawai');
INSERT INTO `subdistricts` VALUES (3734, 3734, 263, 'Waropen Atas');
INSERT INTO `subdistricts` VALUES (3735, 3735, 264, 'Eragayam');
INSERT INTO `subdistricts` VALUES (3736, 3736, 264, 'Ilugwa');
INSERT INTO `subdistricts` VALUES (3737, 3737, 264, 'Kelila');
INSERT INTO `subdistricts` VALUES (3738, 3738, 264, 'Kobakma');
INSERT INTO `subdistricts` VALUES (3739, 3739, 264, 'Megabilis (Megambilis)');
INSERT INTO `subdistricts` VALUES (3740, 3740, 265, 'Bonehau');
INSERT INTO `subdistricts` VALUES (3741, 3741, 265, 'Budong-Budong');
INSERT INTO `subdistricts` VALUES (3742, 3742, 265, 'Kalukku');
INSERT INTO `subdistricts` VALUES (3743, 3743, 265, 'Kalumpang');
INSERT INTO `subdistricts` VALUES (3744, 3744, 265, 'Karossa');
INSERT INTO `subdistricts` VALUES (3745, 3745, 265, 'Kep. Bala Balakang');
INSERT INTO `subdistricts` VALUES (3746, 3746, 265, 'Mamuju');
INSERT INTO `subdistricts` VALUES (3747, 3747, 265, 'Pangale');
INSERT INTO `subdistricts` VALUES (3748, 3748, 265, 'Papalang');
INSERT INTO `subdistricts` VALUES (3749, 3749, 265, 'Sampaga');
INSERT INTO `subdistricts` VALUES (3750, 3750, 265, 'Simboro dan Kepulauan');
INSERT INTO `subdistricts` VALUES (3751, 3751, 265, 'Tapalang');
INSERT INTO `subdistricts` VALUES (3752, 3752, 265, 'Tapalang Barat');
INSERT INTO `subdistricts` VALUES (3753, 3753, 265, 'Tobadak');
INSERT INTO `subdistricts` VALUES (3754, 3754, 265, 'Tommo');
INSERT INTO `subdistricts` VALUES (3755, 3755, 265, 'Topoyo');
INSERT INTO `subdistricts` VALUES (3756, 3756, 266, 'Bambaira');
INSERT INTO `subdistricts` VALUES (3757, 3757, 266, 'Bambalamotu');
INSERT INTO `subdistricts` VALUES (3758, 3758, 266, 'Baras');
INSERT INTO `subdistricts` VALUES (3759, 3759, 266, 'Bulu Taba');
INSERT INTO `subdistricts` VALUES (3760, 3760, 266, 'Dapurang');
INSERT INTO `subdistricts` VALUES (3761, 3761, 266, 'Duripoku');
INSERT INTO `subdistricts` VALUES (3762, 3762, 266, 'Lariang');
INSERT INTO `subdistricts` VALUES (3763, 3763, 266, 'Pasangkayu');
INSERT INTO `subdistricts` VALUES (3764, 3764, 266, 'Pedongga');
INSERT INTO `subdistricts` VALUES (3765, 3765, 266, 'Sarjo');
INSERT INTO `subdistricts` VALUES (3766, 3766, 266, 'Sarudu');
INSERT INTO `subdistricts` VALUES (3767, 3767, 266, 'Tikke Raya');
INSERT INTO `subdistricts` VALUES (3768, 3768, 267, 'Bunaken');
INSERT INTO `subdistricts` VALUES (3769, 3769, 267, 'Bunaken Kepulauan');
INSERT INTO `subdistricts` VALUES (3770, 3770, 267, 'Malalayang');
INSERT INTO `subdistricts` VALUES (3771, 3771, 267, 'Mapanget');
INSERT INTO `subdistricts` VALUES (3772, 3772, 267, 'Paal Dua');
INSERT INTO `subdistricts` VALUES (3773, 3773, 267, 'Sario');
INSERT INTO `subdistricts` VALUES (3774, 3774, 267, 'Singkil');
INSERT INTO `subdistricts` VALUES (3775, 3775, 267, 'Tikala');
INSERT INTO `subdistricts` VALUES (3776, 3776, 267, 'Tuminiting');
INSERT INTO `subdistricts` VALUES (3777, 3777, 267, 'Wanea');
INSERT INTO `subdistricts` VALUES (3778, 3778, 267, 'Wenang');
INSERT INTO `subdistricts` VALUES (3779, 3779, 268, 'Batahan');
INSERT INTO `subdistricts` VALUES (3780, 3780, 268, 'Batang Natal');
INSERT INTO `subdistricts` VALUES (3781, 3781, 268, 'Bukit Malintang');
INSERT INTO `subdistricts` VALUES (3782, 3782, 268, 'Huta Bargot');
INSERT INTO `subdistricts` VALUES (3783, 3783, 268, 'Kotanopan');
INSERT INTO `subdistricts` VALUES (3784, 3784, 268, 'Langga Bayu (Lingga Bayu)');
INSERT INTO `subdistricts` VALUES (3785, 3785, 268, 'Lembah Sorik Merapi');
INSERT INTO `subdistricts` VALUES (3786, 3786, 268, 'Muara Batang Gadis');
INSERT INTO `subdistricts` VALUES (3787, 3787, 268, 'Muara Sipongi');
INSERT INTO `subdistricts` VALUES (3788, 3788, 268, 'Naga Juang');
INSERT INTO `subdistricts` VALUES (3789, 3789, 268, 'Natal');
INSERT INTO `subdistricts` VALUES (3790, 3790, 268, 'Pakantan');
INSERT INTO `subdistricts` VALUES (3791, 3791, 268, 'Panyabungan Barat');
INSERT INTO `subdistricts` VALUES (3792, 3792, 268, 'Panyabungan Kota');
INSERT INTO `subdistricts` VALUES (3793, 3793, 268, 'Panyabungan Selatan');
INSERT INTO `subdistricts` VALUES (3794, 3794, 268, 'Panyabungan Timur');
INSERT INTO `subdistricts` VALUES (3795, 3795, 268, 'Panyabungan Utara');
INSERT INTO `subdistricts` VALUES (3796, 3796, 268, 'Puncak Sorik Marapi/Merapi');
INSERT INTO `subdistricts` VALUES (3797, 3797, 268, 'Ranto Baek/Baik');
INSERT INTO `subdistricts` VALUES (3798, 3798, 268, 'Siabu');
INSERT INTO `subdistricts` VALUES (3799, 3799, 268, 'Sinunukan');
INSERT INTO `subdistricts` VALUES (3800, 3800, 268, 'Tambangan');
INSERT INTO `subdistricts` VALUES (3801, 3801, 268, 'Ulu Pungkut');
INSERT INTO `subdistricts` VALUES (3802, 3802, 269, 'Cibal');
INSERT INTO `subdistricts` VALUES (3803, 3803, 269, 'Cibal Barat');
INSERT INTO `subdistricts` VALUES (3804, 3804, 269, 'Langke Rembong');
INSERT INTO `subdistricts` VALUES (3805, 3805, 269, 'Lelak');
INSERT INTO `subdistricts` VALUES (3806, 3806, 269, 'Rahong Utara');
INSERT INTO `subdistricts` VALUES (3807, 3807, 269, 'Reok');
INSERT INTO `subdistricts` VALUES (3808, 3808, 269, 'Reok Barat');
INSERT INTO `subdistricts` VALUES (3809, 3809, 269, 'Ruteng');
INSERT INTO `subdistricts` VALUES (3810, 3810, 269, 'Satar Mese');
INSERT INTO `subdistricts` VALUES (3811, 3811, 269, 'Satar Mese Barat');
INSERT INTO `subdistricts` VALUES (3812, 3812, 269, 'Wae Rii');
INSERT INTO `subdistricts` VALUES (3813, 3813, 270, 'Boleng');
INSERT INTO `subdistricts` VALUES (3814, 3814, 270, 'Komodo');
INSERT INTO `subdistricts` VALUES (3815, 3815, 270, 'Kuwus');
INSERT INTO `subdistricts` VALUES (3816, 3816, 270, 'Lembor');
INSERT INTO `subdistricts` VALUES (3817, 3817, 270, 'Lembor Selatan');
INSERT INTO `subdistricts` VALUES (3818, 3818, 270, 'Macang Pacar');
INSERT INTO `subdistricts` VALUES (3819, 3819, 270, 'Mbeliling');
INSERT INTO `subdistricts` VALUES (3820, 3820, 270, 'Ndoso');
INSERT INTO `subdistricts` VALUES (3821, 3821, 270, 'Sano Nggoang');
INSERT INTO `subdistricts` VALUES (3822, 3822, 270, 'Welak');
INSERT INTO `subdistricts` VALUES (3823, 3823, 271, 'Borong');
INSERT INTO `subdistricts` VALUES (3824, 3824, 271, 'Elar');
INSERT INTO `subdistricts` VALUES (3825, 3825, 271, 'Elar Selatan');
INSERT INTO `subdistricts` VALUES (3826, 3826, 271, 'Kota Komba');
INSERT INTO `subdistricts` VALUES (3827, 3827, 271, 'Lamba Leda');
INSERT INTO `subdistricts` VALUES (3828, 3828, 271, 'Poco Ranaka');
INSERT INTO `subdistricts` VALUES (3829, 3829, 271, 'Poco Ranaka Timur');
INSERT INTO `subdistricts` VALUES (3830, 3830, 271, 'Rana Mese');
INSERT INTO `subdistricts` VALUES (3831, 3831, 271, 'Sambi Rampas');
INSERT INTO `subdistricts` VALUES (3832, 3832, 272, 'Manokwari Barat');
INSERT INTO `subdistricts` VALUES (3833, 3833, 272, 'Manokwari Selatan');
INSERT INTO `subdistricts` VALUES (3834, 3834, 272, 'Manokwari Timur');
INSERT INTO `subdistricts` VALUES (3835, 3835, 272, 'Manokwari Utara');
INSERT INTO `subdistricts` VALUES (3836, 3836, 272, 'Masni');
INSERT INTO `subdistricts` VALUES (3837, 3837, 272, 'Prafi');
INSERT INTO `subdistricts` VALUES (3838, 3838, 272, 'Sidey');
INSERT INTO `subdistricts` VALUES (3839, 3839, 272, 'Tanah Rubuh');
INSERT INTO `subdistricts` VALUES (3840, 3840, 272, 'Warmare');
INSERT INTO `subdistricts` VALUES (3841, 3841, 273, 'Dataran Isim');
INSERT INTO `subdistricts` VALUES (3842, 3842, 273, 'Momi Waren');
INSERT INTO `subdistricts` VALUES (3843, 3843, 273, 'Neney (Nenei)');
INSERT INTO `subdistricts` VALUES (3844, 3844, 273, 'Oransbari');
INSERT INTO `subdistricts` VALUES (3845, 3845, 273, 'Ransiki');
INSERT INTO `subdistricts` VALUES (3846, 3846, 273, 'Tahota (Tohota)');
INSERT INTO `subdistricts` VALUES (3847, 3847, 274, 'Assue');
INSERT INTO `subdistricts` VALUES (3848, 3848, 274, 'Bamgi');
INSERT INTO `subdistricts` VALUES (3849, 3849, 274, 'Citakmitak');
INSERT INTO `subdistricts` VALUES (3850, 3850, 274, 'Edera');
INSERT INTO `subdistricts` VALUES (3851, 3851, 274, 'Haju');
INSERT INTO `subdistricts` VALUES (3852, 3852, 274, 'Kaibar');
INSERT INTO `subdistricts` VALUES (3853, 3853, 274, 'Minyamur');
INSERT INTO `subdistricts` VALUES (3854, 3854, 274, 'Nambioman Bapai (Mambioman)');
INSERT INTO `subdistricts` VALUES (3855, 3855, 274, 'Obaa');
INSERT INTO `subdistricts` VALUES (3856, 3856, 274, 'Passue');
INSERT INTO `subdistricts` VALUES (3857, 3857, 274, 'Passue Bawah');
INSERT INTO `subdistricts` VALUES (3858, 3858, 274, 'Syahcame');
INSERT INTO `subdistricts` VALUES (3859, 3859, 274, 'Ti Zain');
INSERT INTO `subdistricts` VALUES (3860, 3860, 274, 'Venaha');
INSERT INTO `subdistricts` VALUES (3861, 3861, 274, 'Yakomi');
INSERT INTO `subdistricts` VALUES (3862, 3862, 275, 'Bantimurung');
INSERT INTO `subdistricts` VALUES (3863, 3863, 275, 'Bontoa (Maros Utara)');
INSERT INTO `subdistricts` VALUES (3864, 3864, 275, 'Camba');
INSERT INTO `subdistricts` VALUES (3865, 3865, 275, 'Cenrana');
INSERT INTO `subdistricts` VALUES (3866, 3866, 275, 'Lau');
INSERT INTO `subdistricts` VALUES (3867, 3867, 275, 'Mallawa');
INSERT INTO `subdistricts` VALUES (3868, 3868, 275, 'Mandai');
INSERT INTO `subdistricts` VALUES (3869, 3869, 275, 'Maros Baru');
INSERT INTO `subdistricts` VALUES (3870, 3870, 275, 'Marusu');
INSERT INTO `subdistricts` VALUES (3871, 3871, 275, 'Moncongloe');
INSERT INTO `subdistricts` VALUES (3872, 3872, 275, 'Simbang');
INSERT INTO `subdistricts` VALUES (3873, 3873, 275, 'Tanralili');
INSERT INTO `subdistricts` VALUES (3874, 3874, 275, 'Tompu Bulu');
INSERT INTO `subdistricts` VALUES (3875, 3875, 275, 'Turikale');
INSERT INTO `subdistricts` VALUES (3876, 3876, 276, 'Ampenan');
INSERT INTO `subdistricts` VALUES (3877, 3877, 276, 'Cakranegara');
INSERT INTO `subdistricts` VALUES (3878, 3878, 276, 'Mataram');
INSERT INTO `subdistricts` VALUES (3879, 3879, 276, 'Sandubaya (Sandujaya)');
INSERT INTO `subdistricts` VALUES (3880, 3880, 276, 'Sekarbela');
INSERT INTO `subdistricts` VALUES (3881, 3881, 276, 'Selaparang (Selaprang)');
INSERT INTO `subdistricts` VALUES (3882, 3882, 277, 'Aifat');
INSERT INTO `subdistricts` VALUES (3883, 3883, 277, 'Aifat Selatan');
INSERT INTO `subdistricts` VALUES (3884, 3884, 277, 'Aifat Timur');
INSERT INTO `subdistricts` VALUES (3885, 3885, 277, 'Aifat Timur Jauh');
INSERT INTO `subdistricts` VALUES (3886, 3886, 277, 'Aifat Timur Selatan');
INSERT INTO `subdistricts` VALUES (3887, 3887, 277, 'Aifat Timur Tengah');
INSERT INTO `subdistricts` VALUES (3888, 3888, 277, 'Aifat Utara');
INSERT INTO `subdistricts` VALUES (3889, 3889, 277, 'Aitinyo');
INSERT INTO `subdistricts` VALUES (3890, 3890, 277, 'Aitinyo Barat');
INSERT INTO `subdistricts` VALUES (3891, 3891, 277, 'Aitinyo Raya');
INSERT INTO `subdistricts` VALUES (3892, 3892, 277, 'Aitinyo Tengah');
INSERT INTO `subdistricts` VALUES (3893, 3893, 277, 'Aitinyo Utara');
INSERT INTO `subdistricts` VALUES (3894, 3894, 277, 'Ayamaru');
INSERT INTO `subdistricts` VALUES (3895, 3895, 277, 'Ayamaru Barat');
INSERT INTO `subdistricts` VALUES (3896, 3896, 277, 'Ayamaru Jaya');
INSERT INTO `subdistricts` VALUES (3897, 3897, 277, 'Ayamaru Selatan');
INSERT INTO `subdistricts` VALUES (3898, 3898, 277, 'Ayamaru Selatan Jaya');
INSERT INTO `subdistricts` VALUES (3899, 3899, 277, 'Ayamaru Tengah');
INSERT INTO `subdistricts` VALUES (3900, 3900, 277, 'Ayamaru Timur');
INSERT INTO `subdistricts` VALUES (3901, 3901, 277, 'Ayamaru Timur Selatan');
INSERT INTO `subdistricts` VALUES (3902, 3902, 277, 'Ayamaru Utara');
INSERT INTO `subdistricts` VALUES (3903, 3903, 277, 'Ayamaru Utara Timur');
INSERT INTO `subdistricts` VALUES (3904, 3904, 277, 'Mare');
INSERT INTO `subdistricts` VALUES (3905, 3905, 277, 'Mare Selatan');
INSERT INTO `subdistricts` VALUES (3906, 3906, 278, 'Medan Amplas');
INSERT INTO `subdistricts` VALUES (3907, 3907, 278, 'Medan Area');
INSERT INTO `subdistricts` VALUES (3908, 3908, 278, 'Medan Barat');
INSERT INTO `subdistricts` VALUES (3909, 3909, 278, 'Medan Baru');
INSERT INTO `subdistricts` VALUES (3910, 3910, 278, 'Medan Belawan Kota');
INSERT INTO `subdistricts` VALUES (3911, 3911, 278, 'Medan Deli');
INSERT INTO `subdistricts` VALUES (3912, 3912, 278, 'Medan Denai');
INSERT INTO `subdistricts` VALUES (3913, 3913, 278, 'Medan Helvetia');
INSERT INTO `subdistricts` VALUES (3914, 3914, 278, 'Medan Johor');
INSERT INTO `subdistricts` VALUES (3915, 3915, 278, 'Medan Kota');
INSERT INTO `subdistricts` VALUES (3916, 3916, 278, 'Medan Labuhan');
INSERT INTO `subdistricts` VALUES (3917, 3917, 278, 'Medan Maimun');
INSERT INTO `subdistricts` VALUES (3918, 3918, 278, 'Medan Marelan');
INSERT INTO `subdistricts` VALUES (3919, 3919, 278, 'Medan Perjuangan');
INSERT INTO `subdistricts` VALUES (3920, 3920, 278, 'Medan Petisah');
INSERT INTO `subdistricts` VALUES (3921, 3921, 278, 'Medan Polonia');
INSERT INTO `subdistricts` VALUES (3922, 3922, 278, 'Medan Selayang');
INSERT INTO `subdistricts` VALUES (3923, 3923, 278, 'Medan Sunggal');
INSERT INTO `subdistricts` VALUES (3924, 3924, 278, 'Medan Tembung');
INSERT INTO `subdistricts` VALUES (3925, 3925, 278, 'Medan Timur');
INSERT INTO `subdistricts` VALUES (3926, 3926, 278, 'Medan Tuntungan');
INSERT INTO `subdistricts` VALUES (3927, 3927, 279, 'Belimbing');
INSERT INTO `subdistricts` VALUES (3928, 3928, 279, 'Belimbing Hulu');
INSERT INTO `subdistricts` VALUES (3929, 3929, 279, 'Ella Hilir');
INSERT INTO `subdistricts` VALUES (3930, 3930, 279, 'Menukung');
INSERT INTO `subdistricts` VALUES (3931, 3931, 279, 'Nanga Pinoh');
INSERT INTO `subdistricts` VALUES (3932, 3932, 279, 'Pinoh Selatan');
INSERT INTO `subdistricts` VALUES (3933, 3933, 279, 'Pinoh Utara');
INSERT INTO `subdistricts` VALUES (3934, 3934, 279, 'Sayan');
INSERT INTO `subdistricts` VALUES (3935, 3935, 279, 'Sokan');
INSERT INTO `subdistricts` VALUES (3936, 3936, 279, 'Tanah Pinoh');
INSERT INTO `subdistricts` VALUES (3937, 3937, 279, 'Tanah Pinoh Barat');
INSERT INTO `subdistricts` VALUES (3938, 3938, 280, 'Bangko');
INSERT INTO `subdistricts` VALUES (3939, 3939, 280, 'Bangko Barat');
INSERT INTO `subdistricts` VALUES (3940, 3940, 280, 'Batang Masumai');
INSERT INTO `subdistricts` VALUES (3941, 3941, 280, 'Jangkat');
INSERT INTO `subdistricts` VALUES (3942, 3942, 280, 'Lembah Masurai');
INSERT INTO `subdistricts` VALUES (3943, 3943, 280, 'Margo Tabir');
INSERT INTO `subdistricts` VALUES (3944, 3944, 280, 'Muara Siau');
INSERT INTO `subdistricts` VALUES (3945, 3945, 280, 'Nalo Tantan');
INSERT INTO `subdistricts` VALUES (3946, 3946, 280, 'Pamenang');
INSERT INTO `subdistricts` VALUES (3947, 3947, 280, 'Pamenang Barat');
INSERT INTO `subdistricts` VALUES (3948, 3948, 280, 'Pamenang Selatan');
INSERT INTO `subdistricts` VALUES (3949, 3949, 280, 'Pangkalan Jambu');
INSERT INTO `subdistricts` VALUES (3950, 3950, 280, 'Renah Pembarap');
INSERT INTO `subdistricts` VALUES (3951, 3951, 280, 'Renah Pemenang');
INSERT INTO `subdistricts` VALUES (3952, 3952, 280, 'Sungai Manau');
INSERT INTO `subdistricts` VALUES (3953, 3953, 280, 'Sungai Tenang');
INSERT INTO `subdistricts` VALUES (3954, 3954, 280, 'Tabir');
INSERT INTO `subdistricts` VALUES (3955, 3955, 280, 'Tabir Barat');
INSERT INTO `subdistricts` VALUES (3956, 3956, 280, 'Tabir Ilir');
INSERT INTO `subdistricts` VALUES (3957, 3957, 280, 'Tabir Lintas');
INSERT INTO `subdistricts` VALUES (3958, 3958, 280, 'Tabir Selatan');
INSERT INTO `subdistricts` VALUES (3959, 3959, 280, 'Tabir Timur');
INSERT INTO `subdistricts` VALUES (3960, 3960, 280, 'Tabir Ulu');
INSERT INTO `subdistricts` VALUES (3961, 3961, 280, 'Tiang Pumpung');
INSERT INTO `subdistricts` VALUES (3962, 3962, 281, 'Animha');
INSERT INTO `subdistricts` VALUES (3963, 3963, 281, 'Eligobel');
INSERT INTO `subdistricts` VALUES (3964, 3964, 281, 'Ilyawab');
INSERT INTO `subdistricts` VALUES (3965, 3965, 281, 'Jagebob');
INSERT INTO `subdistricts` VALUES (3966, 3966, 281, 'Kaptel');
INSERT INTO `subdistricts` VALUES (3967, 3967, 281, 'Kimaam');
INSERT INTO `subdistricts` VALUES (3968, 3968, 281, 'Kurik');
INSERT INTO `subdistricts` VALUES (3969, 3969, 281, 'Malind');
INSERT INTO `subdistricts` VALUES (3970, 3970, 281, 'Merauke');
INSERT INTO `subdistricts` VALUES (3971, 3971, 281, 'Muting');
INSERT INTO `subdistricts` VALUES (3972, 3972, 281, 'Naukenjerai');
INSERT INTO `subdistricts` VALUES (3973, 3973, 281, 'Ngguti (Nggunti)');
INSERT INTO `subdistricts` VALUES (3974, 3974, 281, 'Okaba');
INSERT INTO `subdistricts` VALUES (3975, 3975, 281, 'Semangga');
INSERT INTO `subdistricts` VALUES (3976, 3976, 281, 'Sota');
INSERT INTO `subdistricts` VALUES (3977, 3977, 281, 'Tabonji');
INSERT INTO `subdistricts` VALUES (3978, 3978, 281, 'Tanah Miring');
INSERT INTO `subdistricts` VALUES (3979, 3979, 281, 'Tubang');
INSERT INTO `subdistricts` VALUES (3980, 3980, 281, 'Ulilin');
INSERT INTO `subdistricts` VALUES (3981, 3981, 281, 'Waan');
INSERT INTO `subdistricts` VALUES (3982, 3982, 282, 'Mesuji');
INSERT INTO `subdistricts` VALUES (3983, 3983, 282, 'Mesuji Timur');
INSERT INTO `subdistricts` VALUES (3984, 3984, 282, 'Panca Jaya');
INSERT INTO `subdistricts` VALUES (3985, 3985, 282, 'Rawa Jitu Utara');
INSERT INTO `subdistricts` VALUES (3986, 3986, 282, 'Simpang Pematang');
INSERT INTO `subdistricts` VALUES (3987, 3987, 282, 'Tanjung Raya');
INSERT INTO `subdistricts` VALUES (3988, 3988, 282, 'Way Serdang');
INSERT INTO `subdistricts` VALUES (3989, 3989, 283, 'Metro Barat');
INSERT INTO `subdistricts` VALUES (3990, 3990, 283, 'Metro Pusat');
INSERT INTO `subdistricts` VALUES (3991, 3991, 283, 'Metro Selatan');
INSERT INTO `subdistricts` VALUES (3992, 3992, 283, 'Metro Timur');
INSERT INTO `subdistricts` VALUES (3993, 3993, 283, 'Metro Utara');
INSERT INTO `subdistricts` VALUES (3994, 3994, 284, 'Agimuga');
INSERT INTO `subdistricts` VALUES (3995, 3995, 284, 'Jila');
INSERT INTO `subdistricts` VALUES (3996, 3996, 284, 'Jita');
INSERT INTO `subdistricts` VALUES (3997, 3997, 284, 'Kuala Kencana');
INSERT INTO `subdistricts` VALUES (3998, 3998, 284, 'Mimika Barat (Mibar)');
INSERT INTO `subdistricts` VALUES (3999, 3999, 284, 'Mimika Barat Jauh');
INSERT INTO `subdistricts` VALUES (4000, 4000, 284, 'Mimika Barat Tengah');
INSERT INTO `subdistricts` VALUES (4001, 4001, 284, 'Mimika Baru');
INSERT INTO `subdistricts` VALUES (4002, 4002, 284, 'Mimika Timur');
INSERT INTO `subdistricts` VALUES (4003, 4003, 284, 'Mimika Timur Jauh');
INSERT INTO `subdistricts` VALUES (4004, 4004, 284, 'Mimika Timur Tengah');
INSERT INTO `subdistricts` VALUES (4005, 4005, 284, 'Tembagapura');
INSERT INTO `subdistricts` VALUES (4006, 4006, 285, 'Eris');
INSERT INTO `subdistricts` VALUES (4007, 4007, 285, 'Kakas');
INSERT INTO `subdistricts` VALUES (4008, 4008, 285, 'Kakas Barat');
INSERT INTO `subdistricts` VALUES (4009, 4009, 285, 'Kawangkoan');
INSERT INTO `subdistricts` VALUES (4010, 4010, 285, 'Kawangkoan Barat');
INSERT INTO `subdistricts` VALUES (4011, 4011, 285, 'Kawangkoan Utara');
INSERT INTO `subdistricts` VALUES (4012, 4012, 285, 'Kombi');
INSERT INTO `subdistricts` VALUES (4013, 4013, 285, 'Langowan Barat');
INSERT INTO `subdistricts` VALUES (4014, 4014, 285, 'Langowan Selatan');
INSERT INTO `subdistricts` VALUES (4015, 4015, 285, 'Langowan Timur');
INSERT INTO `subdistricts` VALUES (4016, 4016, 285, 'Langowan Utara');
INSERT INTO `subdistricts` VALUES (4017, 4017, 285, 'Lembean Timur');
INSERT INTO `subdistricts` VALUES (4018, 4018, 285, 'Mandolang');
INSERT INTO `subdistricts` VALUES (4019, 4019, 285, 'Pineleng');
INSERT INTO `subdistricts` VALUES (4020, 4020, 285, 'Remboken');
INSERT INTO `subdistricts` VALUES (4021, 4021, 285, 'Sonder');
INSERT INTO `subdistricts` VALUES (4022, 4022, 285, 'Tombariri');
INSERT INTO `subdistricts` VALUES (4023, 4023, 285, 'Tombariri Timur');
INSERT INTO `subdistricts` VALUES (4024, 4024, 285, 'Tombulu');
INSERT INTO `subdistricts` VALUES (4025, 4025, 285, 'Tompaso');
INSERT INTO `subdistricts` VALUES (4026, 4026, 285, 'Tompaso Barat');
INSERT INTO `subdistricts` VALUES (4027, 4027, 285, 'Tondano Barat');
INSERT INTO `subdistricts` VALUES (4028, 4028, 285, 'Tondano Selatan');
INSERT INTO `subdistricts` VALUES (4029, 4029, 285, 'Tondano Timur');
INSERT INTO `subdistricts` VALUES (4030, 4030, 285, 'Tondano Utara');
INSERT INTO `subdistricts` VALUES (4031, 4031, 286, 'Amurang');
INSERT INTO `subdistricts` VALUES (4032, 4032, 286, 'Amurang Barat');
INSERT INTO `subdistricts` VALUES (4033, 4033, 286, 'Amurang Timur');
INSERT INTO `subdistricts` VALUES (4034, 4034, 286, 'Kumelembuai');
INSERT INTO `subdistricts` VALUES (4035, 4035, 286, 'Maesaan');
INSERT INTO `subdistricts` VALUES (4036, 4036, 286, 'Modoinding');
INSERT INTO `subdistricts` VALUES (4037, 4037, 286, 'Motoling');
INSERT INTO `subdistricts` VALUES (4038, 4038, 286, 'Motoling Barat');
INSERT INTO `subdistricts` VALUES (4039, 4039, 286, 'Motoling Timur');
INSERT INTO `subdistricts` VALUES (4040, 4040, 286, 'Ranoyapo');
INSERT INTO `subdistricts` VALUES (4041, 4041, 286, 'Sinonsayang');
INSERT INTO `subdistricts` VALUES (4042, 4042, 286, 'Suluun Tareran');
INSERT INTO `subdistricts` VALUES (4043, 4043, 286, 'Tareran');
INSERT INTO `subdistricts` VALUES (4044, 4044, 286, 'Tatapaan');
INSERT INTO `subdistricts` VALUES (4045, 4045, 286, 'Tenga');
INSERT INTO `subdistricts` VALUES (4046, 4046, 286, 'Tompaso Baru');
INSERT INTO `subdistricts` VALUES (4047, 4047, 286, 'Tumpaan');
INSERT INTO `subdistricts` VALUES (4048, 4048, 287, 'Belang');
INSERT INTO `subdistricts` VALUES (4049, 4049, 287, 'Pasan');
INSERT INTO `subdistricts` VALUES (4050, 4050, 287, 'Pusomaen');
INSERT INTO `subdistricts` VALUES (4051, 4051, 287, 'Ratahan');
INSERT INTO `subdistricts` VALUES (4052, 4052, 287, 'Ratahan Timur');
INSERT INTO `subdistricts` VALUES (4053, 4053, 287, 'Ratatotok');
INSERT INTO `subdistricts` VALUES (4054, 4054, 287, 'Silian Raya');
INSERT INTO `subdistricts` VALUES (4055, 4055, 287, 'Tombatu');
INSERT INTO `subdistricts` VALUES (4056, 4056, 287, 'Tombatu Timur');
INSERT INTO `subdistricts` VALUES (4057, 4057, 287, 'Tombatu Utara');
INSERT INTO `subdistricts` VALUES (4058, 4058, 287, 'Touluaan');
INSERT INTO `subdistricts` VALUES (4059, 4059, 287, 'Touluaan Selatan');
INSERT INTO `subdistricts` VALUES (4060, 4060, 288, 'Airmadidi');
INSERT INTO `subdistricts` VALUES (4061, 4061, 288, 'Dimembe');
INSERT INTO `subdistricts` VALUES (4062, 4062, 288, 'Kalawat');
INSERT INTO `subdistricts` VALUES (4063, 4063, 288, 'Kauditan');
INSERT INTO `subdistricts` VALUES (4064, 4064, 288, 'Kema');
INSERT INTO `subdistricts` VALUES (4065, 4065, 288, 'Likupang Barat');
INSERT INTO `subdistricts` VALUES (4066, 4066, 288, 'Likupang Selatan');
INSERT INTO `subdistricts` VALUES (4067, 4067, 288, 'Likupang Timur');
INSERT INTO `subdistricts` VALUES (4068, 4068, 288, 'Talawaan');
INSERT INTO `subdistricts` VALUES (4069, 4069, 288, 'Wori');
INSERT INTO `subdistricts` VALUES (4070, 4070, 289, 'Bangsal');
INSERT INTO `subdistricts` VALUES (4071, 4071, 289, 'Dawar Blandong');
INSERT INTO `subdistricts` VALUES (4072, 4072, 289, 'Dlanggu');
INSERT INTO `subdistricts` VALUES (4073, 4073, 289, 'Gedeg');
INSERT INTO `subdistricts` VALUES (4074, 4074, 289, 'Gondang');
INSERT INTO `subdistricts` VALUES (4075, 4075, 289, 'Jatirejo');
INSERT INTO `subdistricts` VALUES (4076, 4076, 289, 'Jetis');
INSERT INTO `subdistricts` VALUES (4077, 4077, 289, 'Kemlagi');
INSERT INTO `subdistricts` VALUES (4078, 4078, 289, 'Kutorejo');
INSERT INTO `subdistricts` VALUES (4079, 4079, 289, 'Mojoanyar');
INSERT INTO `subdistricts` VALUES (4080, 4080, 289, 'Mojosari');
INSERT INTO `subdistricts` VALUES (4081, 4081, 289, 'Ngoro');
INSERT INTO `subdistricts` VALUES (4082, 4082, 289, 'Pacet');
INSERT INTO `subdistricts` VALUES (4083, 4083, 289, 'Pungging');
INSERT INTO `subdistricts` VALUES (4084, 4084, 289, 'Puri');
INSERT INTO `subdistricts` VALUES (4085, 4085, 289, 'Sooko');
INSERT INTO `subdistricts` VALUES (4086, 4086, 289, 'Trawas');
INSERT INTO `subdistricts` VALUES (4087, 4087, 289, 'Trowulan');
INSERT INTO `subdistricts` VALUES (4088, 4088, 290, 'Magersari');
INSERT INTO `subdistricts` VALUES (4089, 4089, 290, 'Prajurit Kulon');
INSERT INTO `subdistricts` VALUES (4090, 4090, 291, 'Bahodopi');
INSERT INTO `subdistricts` VALUES (4091, 4091, 291, 'Bumi Raya');
INSERT INTO `subdistricts` VALUES (4092, 4092, 291, 'Bungku Barat');
INSERT INTO `subdistricts` VALUES (4093, 4093, 291, 'Bungku Pesisir');
INSERT INTO `subdistricts` VALUES (4094, 4094, 291, 'Bungku Selatan');
INSERT INTO `subdistricts` VALUES (4095, 4095, 291, 'Bungku Tengah');
INSERT INTO `subdistricts` VALUES (4096, 4096, 291, 'Bungku Timur');
INSERT INTO `subdistricts` VALUES (4097, 4097, 291, 'Bungku Utara');
INSERT INTO `subdistricts` VALUES (4098, 4098, 291, 'Lembo');
INSERT INTO `subdistricts` VALUES (4099, 4099, 291, 'Lembo Raya');
INSERT INTO `subdistricts` VALUES (4100, 4100, 291, 'Mamosalato');
INSERT INTO `subdistricts` VALUES (4101, 4101, 291, 'Menui Kepulauan');
INSERT INTO `subdistricts` VALUES (4102, 4102, 291, 'Mori Atas');
INSERT INTO `subdistricts` VALUES (4103, 4103, 291, 'Mori Utara');
INSERT INTO `subdistricts` VALUES (4104, 4104, 291, 'Petasia');
INSERT INTO `subdistricts` VALUES (4105, 4105, 291, 'Petasia Barat');
INSERT INTO `subdistricts` VALUES (4106, 4106, 291, 'Petasia Timur');
INSERT INTO `subdistricts` VALUES (4107, 4107, 291, 'Soyo Jaya');
INSERT INTO `subdistricts` VALUES (4108, 4108, 291, 'Wita Ponda');
INSERT INTO `subdistricts` VALUES (4109, 4109, 292, 'Abab');
INSERT INTO `subdistricts` VALUES (4110, 4110, 292, 'Benakat');
INSERT INTO `subdistricts` VALUES (4111, 4111, 292, 'Gelumbang');
INSERT INTO `subdistricts` VALUES (4112, 4112, 292, 'Gunung Megang');
INSERT INTO `subdistricts` VALUES (4113, 4113, 292, 'Kelekar');
INSERT INTO `subdistricts` VALUES (4114, 4114, 292, 'Lawang Kidul');
INSERT INTO `subdistricts` VALUES (4115, 4115, 292, 'Lembak');
INSERT INTO `subdistricts` VALUES (4116, 4116, 292, 'Lubai');
INSERT INTO `subdistricts` VALUES (4117, 4117, 292, 'Muara Belida');
INSERT INTO `subdistricts` VALUES (4118, 4118, 292, 'Muara Enim');
INSERT INTO `subdistricts` VALUES (4119, 4119, 292, 'Penukal (Penukal Abab)');
INSERT INTO `subdistricts` VALUES (4120, 4120, 292, 'Penukal Utara');
INSERT INTO `subdistricts` VALUES (4121, 4121, 292, 'Rambang');
INSERT INTO `subdistricts` VALUES (4122, 4122, 292, 'Rambang Dangku');
INSERT INTO `subdistricts` VALUES (4123, 4123, 292, 'Semendo Darat Laut');
INSERT INTO `subdistricts` VALUES (4124, 4124, 292, 'Semendo Darat Tengah');
INSERT INTO `subdistricts` VALUES (4125, 4125, 292, 'Semendo Darat Ulu');
INSERT INTO `subdistricts` VALUES (4126, 4126, 292, 'Sungai Rotan');
INSERT INTO `subdistricts` VALUES (4127, 4127, 292, 'Talang Ubi');
INSERT INTO `subdistricts` VALUES (4128, 4128, 292, 'Tanah Abang');
INSERT INTO `subdistricts` VALUES (4129, 4129, 292, 'Tanjung Agung');
INSERT INTO `subdistricts` VALUES (4130, 4130, 292, 'Ujan Mas');
INSERT INTO `subdistricts` VALUES (4131, 4131, 293, 'Bahar Selatan');
INSERT INTO `subdistricts` VALUES (4132, 4132, 293, 'Bahar Utara');
INSERT INTO `subdistricts` VALUES (4133, 4133, 293, 'Jambi Luar Kota');
INSERT INTO `subdistricts` VALUES (4134, 4134, 293, 'Kumpeh');
INSERT INTO `subdistricts` VALUES (4135, 4135, 293, 'Kumpeh Ulu');
INSERT INTO `subdistricts` VALUES (4136, 4136, 293, 'Maro Sebo');
INSERT INTO `subdistricts` VALUES (4137, 4137, 293, 'Mestong');
INSERT INTO `subdistricts` VALUES (4138, 4138, 293, 'Sekernan');
INSERT INTO `subdistricts` VALUES (4139, 4139, 293, 'Sungai Bahar');
INSERT INTO `subdistricts` VALUES (4140, 4140, 293, 'Sungai Gelam');
INSERT INTO `subdistricts` VALUES (4141, 4141, 293, 'Taman Rajo / Rejo');
INSERT INTO `subdistricts` VALUES (4142, 4142, 294, 'Air Dikit');
INSERT INTO `subdistricts` VALUES (4143, 4143, 294, 'Air Majunto');
INSERT INTO `subdistricts` VALUES (4144, 4144, 294, 'Air Rami');
INSERT INTO `subdistricts` VALUES (4145, 4145, 294, 'Ipuh (Muko-Muko Selatan)');
INSERT INTO `subdistricts` VALUES (4146, 4146, 294, 'Kota Mukomuko (Mukomuko Utara)');
INSERT INTO `subdistricts` VALUES (4147, 4147, 294, 'Lubuk Pinang');
INSERT INTO `subdistricts` VALUES (4148, 4148, 294, 'Malin Deman');
INSERT INTO `subdistricts` VALUES (4149, 4149, 294, 'Penarik');
INSERT INTO `subdistricts` VALUES (4150, 4150, 294, 'Pondok Suguh');
INSERT INTO `subdistricts` VALUES (4151, 4151, 294, 'Selagan Raya');
INSERT INTO `subdistricts` VALUES (4152, 4152, 294, 'Sungai Rumbai');
INSERT INTO `subdistricts` VALUES (4153, 4153, 294, 'Teramang Jaya');
INSERT INTO `subdistricts` VALUES (4154, 4154, 294, 'Teras Terunjam');
INSERT INTO `subdistricts` VALUES (4155, 4155, 294, 'V Koto');
INSERT INTO `subdistricts` VALUES (4156, 4156, 294, 'XIV Koto');
INSERT INTO `subdistricts` VALUES (4157, 4157, 295, 'Barangka');
INSERT INTO `subdistricts` VALUES (4158, 4158, 295, 'Batalaiwaru (Batalaiworu)');
INSERT INTO `subdistricts` VALUES (4159, 4159, 295, 'Batukara');
INSERT INTO `subdistricts` VALUES (4160, 4160, 295, 'Bone (Bone Tondo)');
INSERT INTO `subdistricts` VALUES (4161, 4161, 295, 'Duruka');
INSERT INTO `subdistricts` VALUES (4162, 4162, 295, 'Kabangka');
INSERT INTO `subdistricts` VALUES (4163, 4163, 295, 'Kabawo');
INSERT INTO `subdistricts` VALUES (4164, 4164, 295, 'Katobu');
INSERT INTO `subdistricts` VALUES (4165, 4165, 295, 'Kontu Kowuna');
INSERT INTO `subdistricts` VALUES (4166, 4166, 295, 'Kontunaga');
INSERT INTO `subdistricts` VALUES (4167, 4167, 295, 'Kusambi');
INSERT INTO `subdistricts` VALUES (4168, 4168, 295, 'Lasalepa');
INSERT INTO `subdistricts` VALUES (4169, 4169, 295, 'Lawa');
INSERT INTO `subdistricts` VALUES (4170, 4170, 295, 'Lohia');
INSERT INTO `subdistricts` VALUES (4171, 4171, 295, 'Maginti');
INSERT INTO `subdistricts` VALUES (4172, 4172, 295, 'Maligano');
INSERT INTO `subdistricts` VALUES (4173, 4173, 295, 'Marobo');
INSERT INTO `subdistricts` VALUES (4174, 4174, 295, 'Napabalano');
INSERT INTO `subdistricts` VALUES (4175, 4175, 295, 'Napano Kusambi');
INSERT INTO `subdistricts` VALUES (4176, 4176, 295, 'Parigi');
INSERT INTO `subdistricts` VALUES (4177, 4177, 295, 'Pasi Kolaga');
INSERT INTO `subdistricts` VALUES (4178, 4178, 295, 'Pasir Putih');
INSERT INTO `subdistricts` VALUES (4179, 4179, 295, 'Sawerigadi (Sawerigading/Sewergadi)');
INSERT INTO `subdistricts` VALUES (4180, 4180, 295, 'Tiworo Kepulauan');
INSERT INTO `subdistricts` VALUES (4181, 4181, 295, 'Tiworo Selatan');
INSERT INTO `subdistricts` VALUES (4182, 4182, 295, 'Tiworo Tengah');
INSERT INTO `subdistricts` VALUES (4183, 4183, 295, 'Tiworo Utara');
INSERT INTO `subdistricts` VALUES (4184, 4184, 295, 'Tongkuno');
INSERT INTO `subdistricts` VALUES (4185, 4185, 295, 'Tongkuno Selatan');
INSERT INTO `subdistricts` VALUES (4186, 4186, 295, 'Towea');
INSERT INTO `subdistricts` VALUES (4187, 4187, 295, 'Wa Daga');
INSERT INTO `subdistricts` VALUES (4188, 4188, 295, 'Wakorumba Selatan');
INSERT INTO `subdistricts` VALUES (4189, 4189, 295, 'Watopute');
INSERT INTO `subdistricts` VALUES (4190, 4190, 296, 'Barito Tuhup Raya');
INSERT INTO `subdistricts` VALUES (4191, 4191, 296, 'Laung Tuhup');
INSERT INTO `subdistricts` VALUES (4192, 4192, 296, 'Murung');
INSERT INTO `subdistricts` VALUES (4193, 4193, 296, 'Permata Intan');
INSERT INTO `subdistricts` VALUES (4194, 4194, 296, 'Seribu Riam');
INSERT INTO `subdistricts` VALUES (4195, 4195, 296, 'Sumber Barito');
INSERT INTO `subdistricts` VALUES (4196, 4196, 296, 'Sungai Babuat');
INSERT INTO `subdistricts` VALUES (4197, 4197, 296, 'Tanah Siang');
INSERT INTO `subdistricts` VALUES (4198, 4198, 296, 'Tanah Siang Selatan');
INSERT INTO `subdistricts` VALUES (4199, 4199, 296, 'Uut Murung');
INSERT INTO `subdistricts` VALUES (4200, 4200, 297, 'Babat Supat');
INSERT INTO `subdistricts` VALUES (4201, 4201, 297, 'Babat Toman');
INSERT INTO `subdistricts` VALUES (4202, 4202, 297, 'Batanghari Leko');
INSERT INTO `subdistricts` VALUES (4203, 4203, 297, 'Bayung Lencir');
INSERT INTO `subdistricts` VALUES (4204, 4204, 297, 'Keluang');
INSERT INTO `subdistricts` VALUES (4205, 4205, 297, 'Lais');
INSERT INTO `subdistricts` VALUES (4206, 4206, 297, 'Lalan (Sungai Lalan)');
INSERT INTO `subdistricts` VALUES (4207, 4207, 297, 'Lawang Wetan');
INSERT INTO `subdistricts` VALUES (4208, 4208, 297, 'Plakat Tinggi (Pelakat Tinggi)');
INSERT INTO `subdistricts` VALUES (4209, 4209, 297, 'Sanga Desa');
INSERT INTO `subdistricts` VALUES (4210, 4210, 297, 'Sekayu');
INSERT INTO `subdistricts` VALUES (4211, 4211, 297, 'Sungai Keruh');
INSERT INTO `subdistricts` VALUES (4212, 4212, 297, 'Sungai Lilin');
INSERT INTO `subdistricts` VALUES (4213, 4213, 297, 'Tungkal Jaya');
INSERT INTO `subdistricts` VALUES (4214, 4214, 298, 'Batukuning Lakitan Ulu (BTS)/Cecar');
INSERT INTO `subdistricts` VALUES (4215, 4215, 298, 'Jaya Loka');
INSERT INTO `subdistricts` VALUES (4216, 4216, 298, 'Karang Dapo');
INSERT INTO `subdistricts` VALUES (4217, 4217, 298, 'Karang Jaya');
INSERT INTO `subdistricts` VALUES (4218, 4218, 298, 'Megang Sakti');
INSERT INTO `subdistricts` VALUES (4219, 4219, 298, 'Muara Beliti');
INSERT INTO `subdistricts` VALUES (4220, 4220, 298, 'Muara Kelingi');
INSERT INTO `subdistricts` VALUES (4221, 4221, 298, 'Muara Lakitan');
INSERT INTO `subdistricts` VALUES (4222, 4222, 298, 'Nibung');
INSERT INTO `subdistricts` VALUES (4223, 4223, 298, 'Purwodadi');
INSERT INTO `subdistricts` VALUES (4224, 4224, 298, 'Rawas Ilir');
INSERT INTO `subdistricts` VALUES (4225, 4225, 298, 'Rawas Ulu');
INSERT INTO `subdistricts` VALUES (4226, 4226, 298, 'Rupit');
INSERT INTO `subdistricts` VALUES (4227, 4227, 298, 'Selangit');
INSERT INTO `subdistricts` VALUES (4228, 4228, 298, 'STL Ulu Terawas');
INSERT INTO `subdistricts` VALUES (4229, 4229, 298, 'Sukakarya');
INSERT INTO `subdistricts` VALUES (4230, 4230, 298, 'Sumber Harta');
INSERT INTO `subdistricts` VALUES (4231, 4231, 298, 'Tiang Pumpung Kepungut');
INSERT INTO `subdistricts` VALUES (4232, 4232, 298, 'Tuah Negeri');
INSERT INTO `subdistricts` VALUES (4233, 4233, 298, 'Tugumulyo');
INSERT INTO `subdistricts` VALUES (4234, 4234, 298, 'Ulu Rawas');
INSERT INTO `subdistricts` VALUES (4235, 4235, 299, 'Dipa');
INSERT INTO `subdistricts` VALUES (4236, 4236, 299, 'Makimi');
INSERT INTO `subdistricts` VALUES (4237, 4237, 299, 'Menou');
INSERT INTO `subdistricts` VALUES (4238, 4238, 299, 'Moora');
INSERT INTO `subdistricts` VALUES (4239, 4239, 299, 'Nabire');
INSERT INTO `subdistricts` VALUES (4240, 4240, 299, 'Nabire Barat');
INSERT INTO `subdistricts` VALUES (4241, 4241, 299, 'Napan');
INSERT INTO `subdistricts` VALUES (4242, 4242, 299, 'Siriwo');
INSERT INTO `subdistricts` VALUES (4243, 4243, 299, 'Teluk Kimi');
INSERT INTO `subdistricts` VALUES (4244, 4244, 299, 'Teluk Umar');
INSERT INTO `subdistricts` VALUES (4245, 4245, 299, 'Uwapa');
INSERT INTO `subdistricts` VALUES (4246, 4246, 299, 'Wanggar');
INSERT INTO `subdistricts` VALUES (4247, 4247, 299, 'Wapoga');
INSERT INTO `subdistricts` VALUES (4248, 4248, 299, 'Yaro (Yaro Kabisay)');
INSERT INTO `subdistricts` VALUES (4249, 4249, 299, 'Yaur');
INSERT INTO `subdistricts` VALUES (4250, 4250, 300, 'Beutong');
INSERT INTO `subdistricts` VALUES (4251, 4251, 300, 'Beutong Ateuh Banggalang');
INSERT INTO `subdistricts` VALUES (4252, 4252, 300, 'Darul Makmur');
INSERT INTO `subdistricts` VALUES (4253, 4253, 300, 'Kuala');
INSERT INTO `subdistricts` VALUES (4254, 4254, 300, 'Kuala Pesisir');
INSERT INTO `subdistricts` VALUES (4255, 4255, 300, 'Seunagan');
INSERT INTO `subdistricts` VALUES (4256, 4256, 300, 'Seunagan Timur');
INSERT INTO `subdistricts` VALUES (4257, 4257, 300, 'Suka Makmue');
INSERT INTO `subdistricts` VALUES (4258, 4258, 300, 'Tadu Raya');
INSERT INTO `subdistricts` VALUES (4259, 4259, 300, 'Tripa Makmur');
INSERT INTO `subdistricts` VALUES (4260, 4260, 301, 'Aesesa');
INSERT INTO `subdistricts` VALUES (4261, 4261, 301, 'Aesesa Selatan');
INSERT INTO `subdistricts` VALUES (4262, 4262, 301, 'Boawae');
INSERT INTO `subdistricts` VALUES (4263, 4263, 301, 'Keo Tengah');
INSERT INTO `subdistricts` VALUES (4264, 4264, 301, 'Mauponggo');
INSERT INTO `subdistricts` VALUES (4265, 4265, 301, 'Nangaroro');
INSERT INTO `subdistricts` VALUES (4266, 4266, 301, 'Wolowae');
INSERT INTO `subdistricts` VALUES (4267, 4267, 302, 'Bunguran Barat');
INSERT INTO `subdistricts` VALUES (4268, 4268, 302, 'Bunguran Selatan');
INSERT INTO `subdistricts` VALUES (4269, 4269, 302, 'Bunguran Tengah');
INSERT INTO `subdistricts` VALUES (4270, 4270, 302, 'Bunguran Timur');
INSERT INTO `subdistricts` VALUES (4271, 4271, 302, 'Bunguran Timur Laut');
INSERT INTO `subdistricts` VALUES (4272, 4272, 302, 'Bunguran Utara');
INSERT INTO `subdistricts` VALUES (4273, 4273, 302, 'Midai');
INSERT INTO `subdistricts` VALUES (4274, 4274, 302, 'Pulau Laut');
INSERT INTO `subdistricts` VALUES (4275, 4275, 302, 'Pulau Tiga');
INSERT INTO `subdistricts` VALUES (4276, 4276, 302, 'Serasan');
INSERT INTO `subdistricts` VALUES (4277, 4277, 302, 'Serasan Timur');
INSERT INTO `subdistricts` VALUES (4278, 4278, 302, 'Subi');
INSERT INTO `subdistricts` VALUES (4279, 4279, 303, 'Alama');
INSERT INTO `subdistricts` VALUES (4280, 4280, 303, 'Dal');
INSERT INTO `subdistricts` VALUES (4281, 4281, 303, 'Embetpen');
INSERT INTO `subdistricts` VALUES (4282, 4282, 303, 'Gearek');
INSERT INTO `subdistricts` VALUES (4283, 4283, 303, 'Geselma (Geselema)');
INSERT INTO `subdistricts` VALUES (4284, 4284, 303, 'Inikgal');
INSERT INTO `subdistricts` VALUES (4285, 4285, 303, 'Iniye');
INSERT INTO `subdistricts` VALUES (4286, 4286, 303, 'Kegayem');
INSERT INTO `subdistricts` VALUES (4287, 4287, 303, 'Kenyam');
INSERT INTO `subdistricts` VALUES (4288, 4288, 303, 'Kilmid');
INSERT INTO `subdistricts` VALUES (4289, 4289, 303, 'Kora');
INSERT INTO `subdistricts` VALUES (4290, 4290, 303, 'Koroptak');
INSERT INTO `subdistricts` VALUES (4291, 4291, 303, 'Krepkuri');
INSERT INTO `subdistricts` VALUES (4292, 4292, 303, 'Mam');
INSERT INTO `subdistricts` VALUES (4293, 4293, 303, 'Mapenduma');
INSERT INTO `subdistricts` VALUES (4294, 4294, 303, 'Mbua (Mbuga)');
INSERT INTO `subdistricts` VALUES (4295, 4295, 303, 'Mbua Tengah');
INSERT INTO `subdistricts` VALUES (4296, 4296, 303, 'Mbulmu Yalma');
INSERT INTO `subdistricts` VALUES (4297, 4297, 303, 'Mebarok');
INSERT INTO `subdistricts` VALUES (4298, 4298, 303, 'Moba');
INSERT INTO `subdistricts` VALUES (4299, 4299, 303, 'Mugi');
INSERT INTO `subdistricts` VALUES (4300, 4300, 303, 'Nenggeagin');
INSERT INTO `subdistricts` VALUES (4301, 4301, 303, 'Nirkuri');
INSERT INTO `subdistricts` VALUES (4302, 4302, 303, 'Paro');
INSERT INTO `subdistricts` VALUES (4303, 4303, 303, 'Pasir Putih');
INSERT INTO `subdistricts` VALUES (4304, 4304, 303, 'Pija');
INSERT INTO `subdistricts` VALUES (4305, 4305, 303, 'Wosak');
INSERT INTO `subdistricts` VALUES (4306, 4306, 303, 'Wusi');
INSERT INTO `subdistricts` VALUES (4307, 4307, 303, 'Wutpaga');
INSERT INTO `subdistricts` VALUES (4308, 4308, 303, 'Yal');
INSERT INTO `subdistricts` VALUES (4309, 4309, 303, 'Yenggelo');
INSERT INTO `subdistricts` VALUES (4310, 4310, 303, 'Yigi');
INSERT INTO `subdistricts` VALUES (4311, 4311, 304, 'Aimere');
INSERT INTO `subdistricts` VALUES (4312, 4312, 304, 'Bajawa');
INSERT INTO `subdistricts` VALUES (4313, 4313, 304, 'Bajawa Utara');
INSERT INTO `subdistricts` VALUES (4314, 4314, 304, 'Golewa');
INSERT INTO `subdistricts` VALUES (4315, 4315, 304, 'Golewa Barat');
INSERT INTO `subdistricts` VALUES (4316, 4316, 304, 'Golewa Selatan');
INSERT INTO `subdistricts` VALUES (4317, 4317, 304, 'Inerie');
INSERT INTO `subdistricts` VALUES (4318, 4318, 304, 'Jerebuu');
INSERT INTO `subdistricts` VALUES (4319, 4319, 304, 'Riung');
INSERT INTO `subdistricts` VALUES (4320, 4320, 304, 'Riung Barat');
INSERT INTO `subdistricts` VALUES (4321, 4321, 304, 'Soa');
INSERT INTO `subdistricts` VALUES (4322, 4322, 304, 'Wolomeze (Riung Selatan)');
INSERT INTO `subdistricts` VALUES (4323, 4323, 305, 'Bagor');
INSERT INTO `subdistricts` VALUES (4324, 4324, 305, 'Baron');
INSERT INTO `subdistricts` VALUES (4325, 4325, 305, 'Berbek');
INSERT INTO `subdistricts` VALUES (4326, 4326, 305, 'Gondang');
INSERT INTO `subdistricts` VALUES (4327, 4327, 305, 'Jatikalen');
INSERT INTO `subdistricts` VALUES (4328, 4328, 305, 'Kertosono');
INSERT INTO `subdistricts` VALUES (4329, 4329, 305, 'Lengkong');
INSERT INTO `subdistricts` VALUES (4330, 4330, 305, 'Loceret');
INSERT INTO `subdistricts` VALUES (4331, 4331, 305, 'Nganjuk');
INSERT INTO `subdistricts` VALUES (4332, 4332, 305, 'Ngetos');
INSERT INTO `subdistricts` VALUES (4333, 4333, 305, 'Ngluyu');
INSERT INTO `subdistricts` VALUES (4334, 4334, 305, 'Ngronggot');
INSERT INTO `subdistricts` VALUES (4335, 4335, 305, 'Pace');
INSERT INTO `subdistricts` VALUES (4336, 4336, 305, 'Patianrowo');
INSERT INTO `subdistricts` VALUES (4337, 4337, 305, 'Prambon');
INSERT INTO `subdistricts` VALUES (4338, 4338, 305, 'Rejoso');
INSERT INTO `subdistricts` VALUES (4339, 4339, 305, 'Sawahan');
INSERT INTO `subdistricts` VALUES (4340, 4340, 305, 'Sukomoro');
INSERT INTO `subdistricts` VALUES (4341, 4341, 305, 'Tanjunganom');
INSERT INTO `subdistricts` VALUES (4342, 4342, 305, 'Wilangan');
INSERT INTO `subdistricts` VALUES (4343, 4343, 306, 'Bringin');
INSERT INTO `subdistricts` VALUES (4344, 4344, 306, 'Geneng');
INSERT INTO `subdistricts` VALUES (4345, 4345, 306, 'Gerih');
INSERT INTO `subdistricts` VALUES (4346, 4346, 306, 'Jogorogo');
INSERT INTO `subdistricts` VALUES (4347, 4347, 306, 'Karanganyar');
INSERT INTO `subdistricts` VALUES (4348, 4348, 306, 'Karangjati');
INSERT INTO `subdistricts` VALUES (4349, 4349, 306, 'Kasreman');
INSERT INTO `subdistricts` VALUES (4350, 4350, 306, 'Kedunggalar');
INSERT INTO `subdistricts` VALUES (4351, 4351, 306, 'Kendal');
INSERT INTO `subdistricts` VALUES (4352, 4352, 306, 'Kwadungan');
INSERT INTO `subdistricts` VALUES (4353, 4353, 306, 'Mantingan');
INSERT INTO `subdistricts` VALUES (4354, 4354, 306, 'Ngawi');
INSERT INTO `subdistricts` VALUES (4355, 4355, 306, 'Ngrambe');
INSERT INTO `subdistricts` VALUES (4356, 4356, 306, 'Padas');
INSERT INTO `subdistricts` VALUES (4357, 4357, 306, 'Pangkur');
INSERT INTO `subdistricts` VALUES (4358, 4358, 306, 'Paron');
INSERT INTO `subdistricts` VALUES (4359, 4359, 306, 'Pitu');
INSERT INTO `subdistricts` VALUES (4360, 4360, 306, 'Sine');
INSERT INTO `subdistricts` VALUES (4361, 4361, 306, 'Widodaren');
INSERT INTO `subdistricts` VALUES (4362, 4362, 307, 'Bawolato');
INSERT INTO `subdistricts` VALUES (4363, 4363, 307, 'Botomuzoi');
INSERT INTO `subdistricts` VALUES (4364, 4364, 307, 'Gido');
INSERT INTO `subdistricts` VALUES (4365, 4365, 307, 'Hili Serangkai (Hilisaranggu)');
INSERT INTO `subdistricts` VALUES (4366, 4366, 307, 'Hiliduho');
INSERT INTO `subdistricts` VALUES (4367, 4367, 307, 'Idano Gawo');
INSERT INTO `subdistricts` VALUES (4368, 4368, 307, 'Ma\'u');
INSERT INTO `subdistricts` VALUES (4369, 4369, 307, 'Sogae Adu (Sogaeadu)');
INSERT INTO `subdistricts` VALUES (4370, 4370, 307, 'Somolo-Molo (Samolo)');
INSERT INTO `subdistricts` VALUES (4371, 4371, 307, 'Ulugawo');
INSERT INTO `subdistricts` VALUES (4372, 4372, 308, 'Lahomi (Gahori)');
INSERT INTO `subdistricts` VALUES (4373, 4373, 308, 'Lolofitu Moi');
INSERT INTO `subdistricts` VALUES (4374, 4374, 308, 'Mandrehe');
INSERT INTO `subdistricts` VALUES (4375, 4375, 308, 'Mandrehe Barat');
INSERT INTO `subdistricts` VALUES (4376, 4376, 308, 'Mandrehe Utara');
INSERT INTO `subdistricts` VALUES (4377, 4377, 308, 'Moro\'o');
INSERT INTO `subdistricts` VALUES (4378, 4378, 308, 'Sirombu');
INSERT INTO `subdistricts` VALUES (4379, 4379, 308, 'Ulu Moro\'o (Ulu Narwo)');
INSERT INTO `subdistricts` VALUES (4380, 4380, 309, 'Amandraya');
INSERT INTO `subdistricts` VALUES (4381, 4381, 309, 'Aramo');
INSERT INTO `subdistricts` VALUES (4382, 4382, 309, 'Boronadu');
INSERT INTO `subdistricts` VALUES (4383, 4383, 309, 'Fanayama');
INSERT INTO `subdistricts` VALUES (4384, 4384, 309, 'Gomo');
INSERT INTO `subdistricts` VALUES (4385, 4385, 309, 'Hibala');
INSERT INTO `subdistricts` VALUES (4386, 4386, 309, 'Hilimegai');
INSERT INTO `subdistricts` VALUES (4387, 4387, 309, 'Hilisalawa\'ahe (Hilisalawaahe)');
INSERT INTO `subdistricts` VALUES (4388, 4388, 309, 'Huruna');
INSERT INTO `subdistricts` VALUES (4389, 4389, 309, 'Lahusa');
INSERT INTO `subdistricts` VALUES (4390, 4390, 309, 'Lolomatua');
INSERT INTO `subdistricts` VALUES (4391, 4391, 309, 'Lolowau');
INSERT INTO `subdistricts` VALUES (4392, 4392, 309, 'Maniamolo');
INSERT INTO `subdistricts` VALUES (4393, 4393, 309, 'Mazino');
INSERT INTO `subdistricts` VALUES (4394, 4394, 309, 'Mazo');
INSERT INTO `subdistricts` VALUES (4395, 4395, 309, 'O\'o\'u (Oou)');
INSERT INTO `subdistricts` VALUES (4396, 4396, 309, 'Onohazumba');
INSERT INTO `subdistricts` VALUES (4397, 4397, 309, 'Pulau-Pulau Batu');
INSERT INTO `subdistricts` VALUES (4398, 4398, 309, 'Pulau-Pulau Batu Barat');
INSERT INTO `subdistricts` VALUES (4399, 4399, 309, 'Pulau-Pulau Batu Timur');
INSERT INTO `subdistricts` VALUES (4400, 4400, 309, 'Pulau-Pulau Batu Utara');
INSERT INTO `subdistricts` VALUES (4401, 4401, 309, 'Sidua\'ori');
INSERT INTO `subdistricts` VALUES (4402, 4402, 309, 'Simuk');
INSERT INTO `subdistricts` VALUES (4403, 4403, 309, 'Somambawa');
INSERT INTO `subdistricts` VALUES (4404, 4404, 309, 'Susua');
INSERT INTO `subdistricts` VALUES (4405, 4405, 309, 'Tanah Masa');
INSERT INTO `subdistricts` VALUES (4406, 4406, 309, 'Teluk Dalam');
INSERT INTO `subdistricts` VALUES (4407, 4407, 309, 'Toma');
INSERT INTO `subdistricts` VALUES (4408, 4408, 309, 'Ulunoyo');
INSERT INTO `subdistricts` VALUES (4409, 4409, 309, 'Ulususua');
INSERT INTO `subdistricts` VALUES (4410, 4410, 309, 'Umbunasi');
INSERT INTO `subdistricts` VALUES (4411, 4411, 310, 'Afulu');
INSERT INTO `subdistricts` VALUES (4412, 4412, 310, 'Alasa');
INSERT INTO `subdistricts` VALUES (4413, 4413, 310, 'Alasa Talumuzoi');
INSERT INTO `subdistricts` VALUES (4414, 4414, 310, 'Lahewa');
INSERT INTO `subdistricts` VALUES (4415, 4415, 310, 'Lahewa Timur');
INSERT INTO `subdistricts` VALUES (4416, 4416, 310, 'Lotu');
INSERT INTO `subdistricts` VALUES (4417, 4417, 310, 'Namohalu Esiwa');
INSERT INTO `subdistricts` VALUES (4418, 4418, 310, 'Sawo');
INSERT INTO `subdistricts` VALUES (4419, 4419, 310, 'Sitolu Ori');
INSERT INTO `subdistricts` VALUES (4420, 4420, 310, 'Tugala Oyo');
INSERT INTO `subdistricts` VALUES (4421, 4421, 310, 'Tuhemberua');
INSERT INTO `subdistricts` VALUES (4422, 4422, 311, 'Krayan');
INSERT INTO `subdistricts` VALUES (4423, 4423, 311, 'Krayan Selatan');
INSERT INTO `subdistricts` VALUES (4424, 4424, 311, 'Lumbis');
INSERT INTO `subdistricts` VALUES (4425, 4425, 311, 'Lumbis Ogong');
INSERT INTO `subdistricts` VALUES (4426, 4426, 311, 'Nunukan');
INSERT INTO `subdistricts` VALUES (4427, 4427, 311, 'Nunukan Selatan');
INSERT INTO `subdistricts` VALUES (4428, 4428, 311, 'Sebatik');
INSERT INTO `subdistricts` VALUES (4429, 4429, 311, 'Sebatik Barat');
INSERT INTO `subdistricts` VALUES (4430, 4430, 311, 'Sebatik Tengah');
INSERT INTO `subdistricts` VALUES (4431, 4431, 311, 'Sebatik Timur');
INSERT INTO `subdistricts` VALUES (4432, 4432, 311, 'Sebatik Utara');
INSERT INTO `subdistricts` VALUES (4433, 4433, 311, 'Sebuku');
INSERT INTO `subdistricts` VALUES (4434, 4434, 311, 'Sei Menggaris');
INSERT INTO `subdistricts` VALUES (4435, 4435, 311, 'Sembakung');
INSERT INTO `subdistricts` VALUES (4436, 4436, 311, 'Tulin Onsoi');
INSERT INTO `subdistricts` VALUES (4437, 4437, 312, 'Indralaya');
INSERT INTO `subdistricts` VALUES (4438, 4438, 312, 'Indralaya Selatan');
INSERT INTO `subdistricts` VALUES (4439, 4439, 312, 'Indralaya Utara');
INSERT INTO `subdistricts` VALUES (4440, 4440, 312, 'Kandis');
INSERT INTO `subdistricts` VALUES (4441, 4441, 312, 'Lubuk Keliat');
INSERT INTO `subdistricts` VALUES (4442, 4442, 312, 'Muara Kuang');
INSERT INTO `subdistricts` VALUES (4443, 4443, 312, 'Payaraman');
INSERT INTO `subdistricts` VALUES (4444, 4444, 312, 'Pemulutan');
INSERT INTO `subdistricts` VALUES (4445, 4445, 312, 'Pemulutan Barat');
INSERT INTO `subdistricts` VALUES (4446, 4446, 312, 'Pemulutan Selatan');
INSERT INTO `subdistricts` VALUES (4447, 4447, 312, 'Rambang Kuang');
INSERT INTO `subdistricts` VALUES (4448, 4448, 312, 'Rantau Alai');
INSERT INTO `subdistricts` VALUES (4449, 4449, 312, 'Rantau Panjang');
INSERT INTO `subdistricts` VALUES (4450, 4450, 312, 'Sungai Pinang');
INSERT INTO `subdistricts` VALUES (4451, 4451, 312, 'Tanjung Batu');
INSERT INTO `subdistricts` VALUES (4452, 4452, 312, 'Tanjung Raja');
INSERT INTO `subdistricts` VALUES (4453, 4453, 313, 'Air Sugihan');
INSERT INTO `subdistricts` VALUES (4454, 4454, 313, 'Cengal');
INSERT INTO `subdistricts` VALUES (4455, 4455, 313, 'Jejawi');
INSERT INTO `subdistricts` VALUES (4456, 4456, 313, 'Kayu Agung');
INSERT INTO `subdistricts` VALUES (4457, 4457, 313, 'Lempuing');
INSERT INTO `subdistricts` VALUES (4458, 4458, 313, 'Lempuing Jaya');
INSERT INTO `subdistricts` VALUES (4459, 4459, 313, 'Mesuji');
INSERT INTO `subdistricts` VALUES (4460, 4460, 313, 'Mesuji Makmur');
INSERT INTO `subdistricts` VALUES (4461, 4461, 313, 'Mesuji Raya');
INSERT INTO `subdistricts` VALUES (4462, 4462, 313, 'Pampangan');
INSERT INTO `subdistricts` VALUES (4463, 4463, 313, 'Pangkalan Lampam');
INSERT INTO `subdistricts` VALUES (4464, 4464, 313, 'Pedamaran');
INSERT INTO `subdistricts` VALUES (4465, 4465, 313, 'Pedamaran Timur');
INSERT INTO `subdistricts` VALUES (4466, 4466, 313, 'Sirah Pulau Padang');
INSERT INTO `subdistricts` VALUES (4467, 4467, 313, 'Sungai Menang');
INSERT INTO `subdistricts` VALUES (4468, 4468, 313, 'Tanjung Lubuk');
INSERT INTO `subdistricts` VALUES (4469, 4469, 313, 'Teluk Gelam');
INSERT INTO `subdistricts` VALUES (4470, 4470, 313, 'Tulung Selapan');
INSERT INTO `subdistricts` VALUES (4471, 4471, 314, 'Baturaja Barat');
INSERT INTO `subdistricts` VALUES (4472, 4472, 314, 'Baturaja Timur');
INSERT INTO `subdistricts` VALUES (4473, 4473, 314, 'Lengkiti');
INSERT INTO `subdistricts` VALUES (4474, 4474, 314, 'Lubuk Batang');
INSERT INTO `subdistricts` VALUES (4475, 4475, 314, 'Lubuk Raja');
INSERT INTO `subdistricts` VALUES (4476, 4476, 314, 'Muara Jaya');
INSERT INTO `subdistricts` VALUES (4477, 4477, 314, 'Pengandonan');
INSERT INTO `subdistricts` VALUES (4478, 4478, 314, 'Peninjauan');
INSERT INTO `subdistricts` VALUES (4479, 4479, 314, 'Semidang Aji');
INSERT INTO `subdistricts` VALUES (4480, 4480, 314, 'Sinar Peninjauan');
INSERT INTO `subdistricts` VALUES (4481, 4481, 314, 'Sosoh Buay Rayap');
INSERT INTO `subdistricts` VALUES (4482, 4482, 314, 'Ulu Ogan');
INSERT INTO `subdistricts` VALUES (4483, 4483, 315, 'Banding Agung');
INSERT INTO `subdistricts` VALUES (4484, 4484, 315, 'Buana Pemaca');
INSERT INTO `subdistricts` VALUES (4485, 4485, 315, 'Buay Pemaca');
INSERT INTO `subdistricts` VALUES (4486, 4486, 315, 'Buay Pematang Ribu Ranau Tengah');
INSERT INTO `subdistricts` VALUES (4487, 4487, 315, 'Buay Rawan');
INSERT INTO `subdistricts` VALUES (4488, 4488, 315, 'Buay Runjung');
INSERT INTO `subdistricts` VALUES (4489, 4489, 315, 'Buay Sandang Aji');
INSERT INTO `subdistricts` VALUES (4490, 4490, 315, 'Kisam Ilir');
INSERT INTO `subdistricts` VALUES (4491, 4491, 315, 'Kisam Tinggi');
INSERT INTO `subdistricts` VALUES (4492, 4492, 315, 'Mekakau Ilir');
INSERT INTO `subdistricts` VALUES (4493, 4493, 315, 'Muaradua');
INSERT INTO `subdistricts` VALUES (4494, 4494, 315, 'Muaradua Kisam');
INSERT INTO `subdistricts` VALUES (4495, 4495, 315, 'Pulau Beringin');
INSERT INTO `subdistricts` VALUES (4496, 4496, 315, 'Runjung Agung');
INSERT INTO `subdistricts` VALUES (4497, 4497, 315, 'Simpang');
INSERT INTO `subdistricts` VALUES (4498, 4498, 315, 'Sindang Danau');
INSERT INTO `subdistricts` VALUES (4499, 4499, 315, 'Sungai Are');
INSERT INTO `subdistricts` VALUES (4500, 4500, 315, 'Tiga Dihaji');
INSERT INTO `subdistricts` VALUES (4501, 4501, 315, 'Warkuk Ranau Selatan');
INSERT INTO `subdistricts` VALUES (4502, 4502, 316, 'Belitang');
INSERT INTO `subdistricts` VALUES (4503, 4503, 316, 'Belitang II');
INSERT INTO `subdistricts` VALUES (4504, 4504, 316, 'Belitang III');
INSERT INTO `subdistricts` VALUES (4505, 4505, 316, 'Belitang Jaya');
INSERT INTO `subdistricts` VALUES (4506, 4506, 316, 'Belitang Madang Raya');
INSERT INTO `subdistricts` VALUES (4507, 4507, 316, 'Belitang Mulya');
INSERT INTO `subdistricts` VALUES (4508, 4508, 316, 'Buay Madang');
INSERT INTO `subdistricts` VALUES (4509, 4509, 316, 'Buay Madang Timur');
INSERT INTO `subdistricts` VALUES (4510, 4510, 316, 'Buay Pemuka Bangsa Raja');
INSERT INTO `subdistricts` VALUES (4511, 4511, 316, 'Buay Pemuka Beliung / Peliung');
INSERT INTO `subdistricts` VALUES (4512, 4512, 316, 'Bunga Mayang');
INSERT INTO `subdistricts` VALUES (4513, 4513, 316, 'Cempaka');
INSERT INTO `subdistricts` VALUES (4514, 4514, 316, 'Jayapura');
INSERT INTO `subdistricts` VALUES (4515, 4515, 316, 'Madang Suku I');
INSERT INTO `subdistricts` VALUES (4516, 4516, 316, 'Madang Suku II');
INSERT INTO `subdistricts` VALUES (4517, 4517, 316, 'Madang Suku III');
INSERT INTO `subdistricts` VALUES (4518, 4518, 316, 'Martapura');
INSERT INTO `subdistricts` VALUES (4519, 4519, 316, 'Semendawai Barat');
INSERT INTO `subdistricts` VALUES (4520, 4520, 316, 'Semendawai Suku III');
INSERT INTO `subdistricts` VALUES (4521, 4521, 316, 'Semendawai Timur');
INSERT INTO `subdistricts` VALUES (4522, 4522, 317, 'Arjosari');
INSERT INTO `subdistricts` VALUES (4523, 4523, 317, 'Bandar');
INSERT INTO `subdistricts` VALUES (4524, 4524, 317, 'Donorojo');
INSERT INTO `subdistricts` VALUES (4525, 4525, 317, 'Kebon Agung');
INSERT INTO `subdistricts` VALUES (4526, 4526, 317, 'Nawangan');
INSERT INTO `subdistricts` VALUES (4527, 4527, 317, 'Ngadirojo');
INSERT INTO `subdistricts` VALUES (4528, 4528, 317, 'Pacitan');
INSERT INTO `subdistricts` VALUES (4529, 4529, 317, 'Pringkuku');
INSERT INTO `subdistricts` VALUES (4530, 4530, 317, 'Punung');
INSERT INTO `subdistricts` VALUES (4531, 4531, 317, 'Sudimoro');
INSERT INTO `subdistricts` VALUES (4532, 4532, 317, 'Tegalombo');
INSERT INTO `subdistricts` VALUES (4533, 4533, 317, 'Tulakan');
INSERT INTO `subdistricts` VALUES (4534, 4534, 318, 'Bungus Teluk Kabung');
INSERT INTO `subdistricts` VALUES (4535, 4535, 318, 'Koto Tangah');
INSERT INTO `subdistricts` VALUES (4536, 4536, 318, 'Kuranji');
INSERT INTO `subdistricts` VALUES (4537, 4537, 318, 'Lubuk Begalung');
INSERT INTO `subdistricts` VALUES (4538, 4538, 318, 'Lubuk Kilangan');
INSERT INTO `subdistricts` VALUES (4539, 4539, 318, 'Nanggalo');
INSERT INTO `subdistricts` VALUES (4540, 4540, 318, 'Padang Barat');
INSERT INTO `subdistricts` VALUES (4541, 4541, 318, 'Padang Selatan');
INSERT INTO `subdistricts` VALUES (4542, 4542, 318, 'Padang Timur');
INSERT INTO `subdistricts` VALUES (4543, 4543, 318, 'Padang Utara');
INSERT INTO `subdistricts` VALUES (4544, 4544, 318, 'Pauh');
INSERT INTO `subdistricts` VALUES (4545, 4545, 319, 'Aek Nabara Barumun');
INSERT INTO `subdistricts` VALUES (4546, 4546, 319, 'Barumun');
INSERT INTO `subdistricts` VALUES (4547, 4547, 319, 'Barumun Selatan');
INSERT INTO `subdistricts` VALUES (4548, 4548, 319, 'Barumun Tengah');
INSERT INTO `subdistricts` VALUES (4549, 4549, 319, 'Batang Lubu Sutam');
INSERT INTO `subdistricts` VALUES (4550, 4550, 319, 'Huristak');
INSERT INTO `subdistricts` VALUES (4551, 4551, 319, 'Huta Raja Tinggi');
INSERT INTO `subdistricts` VALUES (4552, 4552, 319, 'Lubuk Barumun');
INSERT INTO `subdistricts` VALUES (4553, 4553, 319, 'Sihapas Barumun');
INSERT INTO `subdistricts` VALUES (4554, 4554, 319, 'Sosa');
INSERT INTO `subdistricts` VALUES (4555, 4555, 319, 'Sosopan');
INSERT INTO `subdistricts` VALUES (4556, 4556, 319, 'Ulu Barumun');
INSERT INTO `subdistricts` VALUES (4557, 4557, 320, 'Batang Onang');
INSERT INTO `subdistricts` VALUES (4558, 4558, 320, 'Dolok');
INSERT INTO `subdistricts` VALUES (4559, 4559, 320, 'Dolok Sigompulon');
INSERT INTO `subdistricts` VALUES (4560, 4560, 320, 'Halongonan');
INSERT INTO `subdistricts` VALUES (4561, 4561, 320, 'Hulu Sihapas');
INSERT INTO `subdistricts` VALUES (4562, 4562, 320, 'Padang Bolak');
INSERT INTO `subdistricts` VALUES (4563, 4563, 320, 'Padang Bolak Julu');
INSERT INTO `subdistricts` VALUES (4564, 4564, 320, 'Portibi');
INSERT INTO `subdistricts` VALUES (4565, 4565, 320, 'Simangambat');
INSERT INTO `subdistricts` VALUES (4566, 4566, 321, 'Padang Panjang Barat');
INSERT INTO `subdistricts` VALUES (4567, 4567, 321, 'Padang Panjang Timur');
INSERT INTO `subdistricts` VALUES (4568, 4568, 322, '2 X 11 Enam Lingkung');
INSERT INTO `subdistricts` VALUES (4569, 4569, 322, '2 X 11 Kayu Tanam');
INSERT INTO `subdistricts` VALUES (4570, 4570, 322, 'Batang Anai');
INSERT INTO `subdistricts` VALUES (4571, 4571, 322, 'Batang Gasan');
INSERT INTO `subdistricts` VALUES (4572, 4572, 322, 'Enam Lingkung');
INSERT INTO `subdistricts` VALUES (4573, 4573, 322, 'IV Koto Aur Malintang');
INSERT INTO `subdistricts` VALUES (4574, 4574, 322, 'Lubuk Alung');
INSERT INTO `subdistricts` VALUES (4575, 4575, 322, 'Nan Sabaris');
INSERT INTO `subdistricts` VALUES (4576, 4576, 322, 'Padang Sago');
INSERT INTO `subdistricts` VALUES (4577, 4577, 322, 'Patamuan');
INSERT INTO `subdistricts` VALUES (4578, 4578, 322, 'Sintuk/Sintuak Toboh Gadang');
INSERT INTO `subdistricts` VALUES (4579, 4579, 322, 'Sungai Geringging/Garingging');
INSERT INTO `subdistricts` VALUES (4580, 4580, 322, 'Sungai Limau');
INSERT INTO `subdistricts` VALUES (4581, 4581, 322, 'Ulakan Tapakih/Tapakis');
INSERT INTO `subdistricts` VALUES (4582, 4582, 322, 'V Koto Kampung Dalam');
INSERT INTO `subdistricts` VALUES (4583, 4583, 322, 'V Koto Timur');
INSERT INTO `subdistricts` VALUES (4584, 4584, 322, 'VII Koto Sungai Sarik');
INSERT INTO `subdistricts` VALUES (4585, 4585, 323, 'Padang Sidempuan Angkola Julu');
INSERT INTO `subdistricts` VALUES (4586, 4586, 323, 'Padang Sidempuan Batunadua');
INSERT INTO `subdistricts` VALUES (4587, 4587, 323, 'Padang Sidempuan Hutaimbaru');
INSERT INTO `subdistricts` VALUES (4588, 4588, 323, 'Padang Sidempuan Selatan');
INSERT INTO `subdistricts` VALUES (4589, 4589, 323, 'Padang Sidempuan Tenggara');
INSERT INTO `subdistricts` VALUES (4590, 4590, 323, 'Padang Sidempuan Utara (Padangsidimpuan)');
INSERT INTO `subdistricts` VALUES (4591, 4591, 324, 'Dempo Selatan');
INSERT INTO `subdistricts` VALUES (4592, 4592, 324, 'Dempo Tengah');
INSERT INTO `subdistricts` VALUES (4593, 4593, 324, 'Dempo Utara');
INSERT INTO `subdistricts` VALUES (4594, 4594, 324, 'Pagar Alam Selatan');
INSERT INTO `subdistricts` VALUES (4595, 4595, 324, 'Pagar Alam Utara');
INSERT INTO `subdistricts` VALUES (4596, 4596, 325, 'Kerajaan');
INSERT INTO `subdistricts` VALUES (4597, 4597, 325, 'Pagindar');
INSERT INTO `subdistricts` VALUES (4598, 4598, 325, 'Pergetteng Getteng Sengkut');
INSERT INTO `subdistricts` VALUES (4599, 4599, 325, 'Salak');
INSERT INTO `subdistricts` VALUES (4600, 4600, 325, 'Siempat Rube');
INSERT INTO `subdistricts` VALUES (4601, 4601, 325, 'Sitellu Tali Urang Jehe');
INSERT INTO `subdistricts` VALUES (4602, 4602, 325, 'Sitellu Tali Urang Julu');
INSERT INTO `subdistricts` VALUES (4603, 4603, 325, 'Tinada');
INSERT INTO `subdistricts` VALUES (4604, 4604, 326, 'Bukit Batu');
INSERT INTO `subdistricts` VALUES (4605, 4605, 326, 'Jekan Raya');
INSERT INTO `subdistricts` VALUES (4606, 4606, 326, 'Pahandut');
INSERT INTO `subdistricts` VALUES (4607, 4607, 326, 'Rakumpit');
INSERT INTO `subdistricts` VALUES (4608, 4608, 326, 'Sebangau');
INSERT INTO `subdistricts` VALUES (4609, 4609, 327, 'Alang-Alang Lebar');
INSERT INTO `subdistricts` VALUES (4610, 4610, 327, 'Bukit Kecil');
INSERT INTO `subdistricts` VALUES (4611, 4611, 327, 'Gandus');
INSERT INTO `subdistricts` VALUES (4612, 4612, 327, 'Ilir Barat I');
INSERT INTO `subdistricts` VALUES (4613, 4613, 327, 'Ilir Barat II');
INSERT INTO `subdistricts` VALUES (4614, 4614, 327, 'Ilir Timur I');
INSERT INTO `subdistricts` VALUES (4615, 4615, 327, 'Ilir Timur II');
INSERT INTO `subdistricts` VALUES (4616, 4616, 327, 'Kalidoni');
INSERT INTO `subdistricts` VALUES (4617, 4617, 327, 'Kemuning');
INSERT INTO `subdistricts` VALUES (4618, 4618, 327, 'Kertapati');
INSERT INTO `subdistricts` VALUES (4619, 4619, 327, 'Plaju');
INSERT INTO `subdistricts` VALUES (4620, 4620, 327, 'Sako');
INSERT INTO `subdistricts` VALUES (4621, 4621, 327, 'Seberang Ulu I');
INSERT INTO `subdistricts` VALUES (4622, 4622, 327, 'Seberang Ulu II');
INSERT INTO `subdistricts` VALUES (4623, 4623, 327, 'Sematang Borang');
INSERT INTO `subdistricts` VALUES (4624, 4624, 327, 'Sukarami');
INSERT INTO `subdistricts` VALUES (4625, 4625, 328, 'Bara');
INSERT INTO `subdistricts` VALUES (4626, 4626, 328, 'Mungkajang');
INSERT INTO `subdistricts` VALUES (4627, 4627, 328, 'Sendana');
INSERT INTO `subdistricts` VALUES (4628, 4628, 328, 'Telluwanua');
INSERT INTO `subdistricts` VALUES (4629, 4629, 328, 'Wara');
INSERT INTO `subdistricts` VALUES (4630, 4630, 328, 'Wara Barat');
INSERT INTO `subdistricts` VALUES (4631, 4631, 328, 'Wara Selatan');
INSERT INTO `subdistricts` VALUES (4632, 4632, 328, 'Wara Timur');
INSERT INTO `subdistricts` VALUES (4633, 4633, 328, 'Wara Utara');
INSERT INTO `subdistricts` VALUES (4634, 4634, 329, 'Mantikulore');
INSERT INTO `subdistricts` VALUES (4635, 4635, 329, 'Palu Barat');
INSERT INTO `subdistricts` VALUES (4636, 4636, 329, 'Palu Selatan');
INSERT INTO `subdistricts` VALUES (4637, 4637, 329, 'Palu Timur');
INSERT INTO `subdistricts` VALUES (4638, 4638, 329, 'Palu Utara');
INSERT INTO `subdistricts` VALUES (4639, 4639, 329, 'Tatanga');
INSERT INTO `subdistricts` VALUES (4640, 4640, 329, 'Tawaeli');
INSERT INTO `subdistricts` VALUES (4641, 4641, 329, 'Ulujadi');
INSERT INTO `subdistricts` VALUES (4642, 4642, 330, 'Batumarmar');
INSERT INTO `subdistricts` VALUES (4643, 4643, 330, 'Galis');
INSERT INTO `subdistricts` VALUES (4644, 4644, 330, 'Kadur');
INSERT INTO `subdistricts` VALUES (4645, 4645, 330, 'Larangan');
INSERT INTO `subdistricts` VALUES (4646, 4646, 330, 'Pademawu');
INSERT INTO `subdistricts` VALUES (4647, 4647, 330, 'Pakong');
INSERT INTO `subdistricts` VALUES (4648, 4648, 330, 'Palenga\'an');
INSERT INTO `subdistricts` VALUES (4649, 4649, 330, 'Pamekasan');
INSERT INTO `subdistricts` VALUES (4650, 4650, 330, 'Pasean');
INSERT INTO `subdistricts` VALUES (4651, 4651, 330, 'Pegantenan');
INSERT INTO `subdistricts` VALUES (4652, 4652, 330, 'Proppo');
INSERT INTO `subdistricts` VALUES (4653, 4653, 330, 'Tlanakan');
INSERT INTO `subdistricts` VALUES (4654, 4654, 330, 'Waru');
INSERT INTO `subdistricts` VALUES (4655, 4655, 331, 'Angsana');
INSERT INTO `subdistricts` VALUES (4656, 4656, 331, 'Banjar');
INSERT INTO `subdistricts` VALUES (4657, 4657, 331, 'Bojong');
INSERT INTO `subdistricts` VALUES (4658, 4658, 331, 'Cadasari');
INSERT INTO `subdistricts` VALUES (4659, 4659, 331, 'Carita');
INSERT INTO `subdistricts` VALUES (4660, 4660, 331, 'Cibaliung');
INSERT INTO `subdistricts` VALUES (4661, 4661, 331, 'Cibitung');
INSERT INTO `subdistricts` VALUES (4662, 4662, 331, 'Cigeulis');
INSERT INTO `subdistricts` VALUES (4663, 4663, 331, 'Cikeudal (Cikedal)');
INSERT INTO `subdistricts` VALUES (4664, 4664, 331, 'Cikeusik');
INSERT INTO `subdistricts` VALUES (4665, 4665, 331, 'Cimanggu');
INSERT INTO `subdistricts` VALUES (4666, 4666, 331, 'Cimanuk');
INSERT INTO `subdistricts` VALUES (4667, 4667, 331, 'Cipeucang');
INSERT INTO `subdistricts` VALUES (4668, 4668, 331, 'Cisata');
INSERT INTO `subdistricts` VALUES (4669, 4669, 331, 'Jiput');
INSERT INTO `subdistricts` VALUES (4670, 4670, 331, 'Kaduhejo');
INSERT INTO `subdistricts` VALUES (4671, 4671, 331, 'Karang Tanjung');
INSERT INTO `subdistricts` VALUES (4672, 4672, 331, 'Koroncong');
INSERT INTO `subdistricts` VALUES (4673, 4673, 331, 'Labuan');
INSERT INTO `subdistricts` VALUES (4674, 4674, 331, 'Majasari');
INSERT INTO `subdistricts` VALUES (4675, 4675, 331, 'Mandalawangi');
INSERT INTO `subdistricts` VALUES (4676, 4676, 331, 'Mekarjaya');
INSERT INTO `subdistricts` VALUES (4677, 4677, 331, 'Menes');
INSERT INTO `subdistricts` VALUES (4678, 4678, 331, 'Munjul');
INSERT INTO `subdistricts` VALUES (4679, 4679, 331, 'Pagelaran');
INSERT INTO `subdistricts` VALUES (4680, 4680, 331, 'Pandeglang');
INSERT INTO `subdistricts` VALUES (4681, 4681, 331, 'Panimbang');
INSERT INTO `subdistricts` VALUES (4682, 4682, 331, 'Patia');
INSERT INTO `subdistricts` VALUES (4683, 4683, 331, 'Picung');
INSERT INTO `subdistricts` VALUES (4684, 4684, 331, 'Pulosari');
INSERT INTO `subdistricts` VALUES (4685, 4685, 331, 'Saketi');
INSERT INTO `subdistricts` VALUES (4686, 4686, 331, 'Sindangresmi');
INSERT INTO `subdistricts` VALUES (4687, 4687, 331, 'Sobang');
INSERT INTO `subdistricts` VALUES (4688, 4688, 331, 'Sukaresmi');
INSERT INTO `subdistricts` VALUES (4689, 4689, 331, 'Sumur');
INSERT INTO `subdistricts` VALUES (4690, 4690, 332, 'Cigugur');
INSERT INTO `subdistricts` VALUES (4691, 4691, 332, 'Cijulang');
INSERT INTO `subdistricts` VALUES (4692, 4692, 332, 'Cimerak');
INSERT INTO `subdistricts` VALUES (4693, 4693, 332, 'Kalipucang');
INSERT INTO `subdistricts` VALUES (4694, 4694, 332, 'Langkaplancar');
INSERT INTO `subdistricts` VALUES (4695, 4695, 332, 'Mangunjaya');
INSERT INTO `subdistricts` VALUES (4696, 4696, 332, 'Padaherang');
INSERT INTO `subdistricts` VALUES (4697, 4697, 332, 'Pangandaran');
INSERT INTO `subdistricts` VALUES (4698, 4698, 332, 'Parigi');
INSERT INTO `subdistricts` VALUES (4699, 4699, 332, 'Sidamulih');
INSERT INTO `subdistricts` VALUES (4700, 4700, 333, 'Balocci');
INSERT INTO `subdistricts` VALUES (4701, 4701, 333, 'Bungoro');
INSERT INTO `subdistricts` VALUES (4702, 4702, 333, 'Labakkang');
INSERT INTO `subdistricts` VALUES (4703, 4703, 333, 'Liukang Kalmas (Kalukuang Masalima)');
INSERT INTO `subdistricts` VALUES (4704, 4704, 333, 'Liukang Tangaya');
INSERT INTO `subdistricts` VALUES (4705, 4705, 333, 'Liukang Tupabbiring');
INSERT INTO `subdistricts` VALUES (4706, 4706, 333, 'Liukang Tupabbiring Utara');
INSERT INTO `subdistricts` VALUES (4707, 4707, 333, 'Mandalle');
INSERT INTO `subdistricts` VALUES (4708, 4708, 333, 'Marang (Ma Rang)');
INSERT INTO `subdistricts` VALUES (4709, 4709, 333, 'Minasa Tene');
INSERT INTO `subdistricts` VALUES (4710, 4710, 333, 'Pangkajene');
INSERT INTO `subdistricts` VALUES (4711, 4711, 333, 'Segeri');
INSERT INTO `subdistricts` VALUES (4712, 4712, 333, 'Tondong Tallasa');
INSERT INTO `subdistricts` VALUES (4713, 4713, 334, 'Bukit Intan');
INSERT INTO `subdistricts` VALUES (4714, 4714, 334, 'Gabek');
INSERT INTO `subdistricts` VALUES (4715, 4715, 334, 'Gerunggang');
INSERT INTO `subdistricts` VALUES (4716, 4716, 334, 'Girimaya');
INSERT INTO `subdistricts` VALUES (4717, 4717, 334, 'Pangkal Balam');
INSERT INTO `subdistricts` VALUES (4718, 4718, 334, 'Rangkui');
INSERT INTO `subdistricts` VALUES (4719, 4719, 334, 'Taman Sari');
INSERT INTO `subdistricts` VALUES (4720, 4720, 335, 'Aradide');
INSERT INTO `subdistricts` VALUES (4721, 4721, 335, 'Bibida');
INSERT INTO `subdistricts` VALUES (4722, 4722, 335, 'Bogobaida');
INSERT INTO `subdistricts` VALUES (4723, 4723, 335, 'Dumadama');
INSERT INTO `subdistricts` VALUES (4724, 4724, 335, 'Ekadide');
INSERT INTO `subdistricts` VALUES (4725, 4725, 335, 'Kebo');
INSERT INTO `subdistricts` VALUES (4726, 4726, 335, 'Paniai Barat');
INSERT INTO `subdistricts` VALUES (4727, 4727, 335, 'Paniai Timur');
INSERT INTO `subdistricts` VALUES (4728, 4728, 335, 'Siriwo');
INSERT INTO `subdistricts` VALUES (4729, 4729, 335, 'Yatamo');
INSERT INTO `subdistricts` VALUES (4730, 4730, 336, 'Bacukiki');
INSERT INTO `subdistricts` VALUES (4731, 4731, 336, 'Bacukiki Barat');
INSERT INTO `subdistricts` VALUES (4732, 4732, 336, 'Soreang');
INSERT INTO `subdistricts` VALUES (4733, 4733, 336, 'Ujung');
INSERT INTO `subdistricts` VALUES (4734, 4734, 337, 'Pariaman Selatan');
INSERT INTO `subdistricts` VALUES (4735, 4735, 337, 'Pariaman Tengah');
INSERT INTO `subdistricts` VALUES (4736, 4736, 337, 'Pariaman Timur');
INSERT INTO `subdistricts` VALUES (4737, 4737, 337, 'Pariaman Utara');
INSERT INTO `subdistricts` VALUES (4738, 4738, 338, 'Ampibabo');
INSERT INTO `subdistricts` VALUES (4739, 4739, 338, 'Balinggi');
INSERT INTO `subdistricts` VALUES (4740, 4740, 338, 'Bolano');
INSERT INTO `subdistricts` VALUES (4741, 4741, 338, 'Bolano Lambunu/Lambulu');
INSERT INTO `subdistricts` VALUES (4742, 4742, 338, 'Kasimbar');
INSERT INTO `subdistricts` VALUES (4743, 4743, 338, 'Mepanga');
INSERT INTO `subdistricts` VALUES (4744, 4744, 338, 'Moutong');
INSERT INTO `subdistricts` VALUES (4745, 4745, 338, 'Ongka Malino');
INSERT INTO `subdistricts` VALUES (4746, 4746, 338, 'Palasa');
INSERT INTO `subdistricts` VALUES (4747, 4747, 338, 'Parigi');
INSERT INTO `subdistricts` VALUES (4748, 4748, 338, 'Parigi Barat');
INSERT INTO `subdistricts` VALUES (4749, 4749, 338, 'Parigi Selatan');
INSERT INTO `subdistricts` VALUES (4750, 4750, 338, 'Parigi Tengah');
INSERT INTO `subdistricts` VALUES (4751, 4751, 338, 'Parigi Utara');
INSERT INTO `subdistricts` VALUES (4752, 4752, 338, 'Sausu');
INSERT INTO `subdistricts` VALUES (4753, 4753, 338, 'Siniu');
INSERT INTO `subdistricts` VALUES (4754, 4754, 338, 'Taopa');
INSERT INTO `subdistricts` VALUES (4755, 4755, 338, 'Tinombo');
INSERT INTO `subdistricts` VALUES (4756, 4756, 338, 'Tinombo Selatan');
INSERT INTO `subdistricts` VALUES (4757, 4757, 338, 'Tomini');
INSERT INTO `subdistricts` VALUES (4758, 4758, 338, 'Toribulu');
INSERT INTO `subdistricts` VALUES (4759, 4759, 338, 'Torue');
INSERT INTO `subdistricts` VALUES (4760, 4760, 339, 'Bonjol');
INSERT INTO `subdistricts` VALUES (4761, 4761, 339, 'Duo Koto (II Koto)');
INSERT INTO `subdistricts` VALUES (4762, 4762, 339, 'Lubuk Sikaping');
INSERT INTO `subdistricts` VALUES (4763, 4763, 339, 'Mapat Tunggul');
INSERT INTO `subdistricts` VALUES (4764, 4764, 339, 'Mapat Tunggul Selatan');
INSERT INTO `subdistricts` VALUES (4765, 4765, 339, 'Padang Gelugur');
INSERT INTO `subdistricts` VALUES (4766, 4766, 339, 'Panti');
INSERT INTO `subdistricts` VALUES (4767, 4767, 339, 'Rao');
INSERT INTO `subdistricts` VALUES (4768, 4768, 339, 'Rao Selatan');
INSERT INTO `subdistricts` VALUES (4769, 4769, 339, 'Rao Utara');
INSERT INTO `subdistricts` VALUES (4770, 4770, 339, 'Simpang Alahan Mati');
INSERT INTO `subdistricts` VALUES (4771, 4771, 339, 'Tigo Nagari (III Nagari)');
INSERT INTO `subdistricts` VALUES (4772, 4772, 340, 'Gunung Tuleh');
INSERT INTO `subdistricts` VALUES (4773, 4773, 340, 'Kinali');
INSERT INTO `subdistricts` VALUES (4774, 4774, 340, 'Koto Balingka');
INSERT INTO `subdistricts` VALUES (4775, 4775, 340, 'Lembah Melintang');
INSERT INTO `subdistricts` VALUES (4776, 4776, 340, 'Luhak Nan Duo');
INSERT INTO `subdistricts` VALUES (4777, 4777, 340, 'Pasaman');
INSERT INTO `subdistricts` VALUES (4778, 4778, 340, 'Ranah Batahan');
INSERT INTO `subdistricts` VALUES (4779, 4779, 340, 'Sasak Ranah Pasisir/Pesisie');
INSERT INTO `subdistricts` VALUES (4780, 4780, 340, 'Sei Beremas');
INSERT INTO `subdistricts` VALUES (4781, 4781, 340, 'Sungai Aur');
INSERT INTO `subdistricts` VALUES (4782, 4782, 340, 'Talamau');
INSERT INTO `subdistricts` VALUES (4783, 4783, 341, 'Batu Engau');
INSERT INTO `subdistricts` VALUES (4784, 4784, 341, 'Batu Sopang');
INSERT INTO `subdistricts` VALUES (4785, 4785, 341, 'Kuaro');
INSERT INTO `subdistricts` VALUES (4786, 4786, 341, 'Long Ikis');
INSERT INTO `subdistricts` VALUES (4787, 4787, 341, 'Long Kali');
INSERT INTO `subdistricts` VALUES (4788, 4788, 341, 'Muara Komam');
INSERT INTO `subdistricts` VALUES (4789, 4789, 341, 'Muara Samu');
INSERT INTO `subdistricts` VALUES (4790, 4790, 341, 'Pasir Belengkong');
INSERT INTO `subdistricts` VALUES (4791, 4791, 341, 'Tanah Grogot');
INSERT INTO `subdistricts` VALUES (4792, 4792, 341, 'Tanjung Harapan');
INSERT INTO `subdistricts` VALUES (4793, 4793, 342, 'Bangil');
INSERT INTO `subdistricts` VALUES (4794, 4794, 342, 'Beji');
INSERT INTO `subdistricts` VALUES (4795, 4795, 342, 'Gempol');
INSERT INTO `subdistricts` VALUES (4796, 4796, 342, 'Gondang Wetan');
INSERT INTO `subdistricts` VALUES (4797, 4797, 342, 'Grati');
INSERT INTO `subdistricts` VALUES (4798, 4798, 342, 'Kejayan');
INSERT INTO `subdistricts` VALUES (4799, 4799, 342, 'Kraton');
INSERT INTO `subdistricts` VALUES (4800, 4800, 342, 'Lekok');
INSERT INTO `subdistricts` VALUES (4801, 4801, 342, 'Lumbang');
INSERT INTO `subdistricts` VALUES (4802, 4802, 342, 'Nguling');
INSERT INTO `subdistricts` VALUES (4803, 4803, 342, 'Pandaan');
INSERT INTO `subdistricts` VALUES (4804, 4804, 342, 'Pasrepan');
INSERT INTO `subdistricts` VALUES (4805, 4805, 342, 'Pohjentrek');
INSERT INTO `subdistricts` VALUES (4806, 4806, 342, 'Prigen');
INSERT INTO `subdistricts` VALUES (4807, 4807, 342, 'Purwodadi');
INSERT INTO `subdistricts` VALUES (4808, 4808, 342, 'Purwosari');
INSERT INTO `subdistricts` VALUES (4809, 4809, 342, 'Puspo');
INSERT INTO `subdistricts` VALUES (4810, 4810, 342, 'Rejoso');
INSERT INTO `subdistricts` VALUES (4811, 4811, 342, 'Rembang');
INSERT INTO `subdistricts` VALUES (4812, 4812, 342, 'Sukorejo');
INSERT INTO `subdistricts` VALUES (4813, 4813, 342, 'Tosari');
INSERT INTO `subdistricts` VALUES (4814, 4814, 342, 'Tutur');
INSERT INTO `subdistricts` VALUES (4815, 4815, 342, 'Winongan');
INSERT INTO `subdistricts` VALUES (4816, 4816, 342, 'Wonorejo');
INSERT INTO `subdistricts` VALUES (4817, 4817, 343, 'Bugul Kidul');
INSERT INTO `subdistricts` VALUES (4818, 4818, 343, 'Gadingrejo');
INSERT INTO `subdistricts` VALUES (4819, 4819, 343, 'Panggungrejo');
INSERT INTO `subdistricts` VALUES (4820, 4820, 343, 'Purworejo');
INSERT INTO `subdistricts` VALUES (4821, 4821, 344, 'Batangan');
INSERT INTO `subdistricts` VALUES (4822, 4822, 344, 'Cluwak');
INSERT INTO `subdistricts` VALUES (4823, 4823, 344, 'Dukuhseti');
INSERT INTO `subdistricts` VALUES (4824, 4824, 344, 'Gabus');
INSERT INTO `subdistricts` VALUES (4825, 4825, 344, 'Gembong');
INSERT INTO `subdistricts` VALUES (4826, 4826, 344, 'Gunungwungkal');
INSERT INTO `subdistricts` VALUES (4827, 4827, 344, 'Jaken');
INSERT INTO `subdistricts` VALUES (4828, 4828, 344, 'Jakenan');
INSERT INTO `subdistricts` VALUES (4829, 4829, 344, 'Juwana');
INSERT INTO `subdistricts` VALUES (4830, 4830, 344, 'Kayen');
INSERT INTO `subdistricts` VALUES (4831, 4831, 344, 'Margorejo');
INSERT INTO `subdistricts` VALUES (4832, 4832, 344, 'Margoyoso');
INSERT INTO `subdistricts` VALUES (4833, 4833, 344, 'Pati');
INSERT INTO `subdistricts` VALUES (4834, 4834, 344, 'Pucakwangi');
INSERT INTO `subdistricts` VALUES (4835, 4835, 344, 'Sukolilo');
INSERT INTO `subdistricts` VALUES (4836, 4836, 344, 'Tambakromo');
INSERT INTO `subdistricts` VALUES (4837, 4837, 344, 'Tayu');
INSERT INTO `subdistricts` VALUES (4838, 4838, 344, 'Tlogowungu');
INSERT INTO `subdistricts` VALUES (4839, 4839, 344, 'Trangkil');
INSERT INTO `subdistricts` VALUES (4840, 4840, 344, 'Wedarijaksa');
INSERT INTO `subdistricts` VALUES (4841, 4841, 344, 'Winong');
INSERT INTO `subdistricts` VALUES (4842, 4842, 345, 'Lamposi Tigo Nagari');
INSERT INTO `subdistricts` VALUES (4843, 4843, 345, 'Payakumbuh Barat');
INSERT INTO `subdistricts` VALUES (4844, 4844, 345, 'Payakumbuh Selatan');
INSERT INTO `subdistricts` VALUES (4845, 4845, 345, 'Payakumbuh Timur');
INSERT INTO `subdistricts` VALUES (4846, 4846, 345, 'Payakumbuh Utara');
INSERT INTO `subdistricts` VALUES (4847, 4847, 346, 'Anggi');
INSERT INTO `subdistricts` VALUES (4848, 4848, 346, 'Anggi Gida');
INSERT INTO `subdistricts` VALUES (4849, 4849, 346, 'Catubouw');
INSERT INTO `subdistricts` VALUES (4850, 4850, 346, 'Didohu');
INSERT INTO `subdistricts` VALUES (4851, 4851, 346, 'Hingk');
INSERT INTO `subdistricts` VALUES (4852, 4852, 346, 'Membey');
INSERT INTO `subdistricts` VALUES (4853, 4853, 346, 'Menyambouw (Minyambouw)');
INSERT INTO `subdistricts` VALUES (4854, 4854, 346, 'Sururey');
INSERT INTO `subdistricts` VALUES (4855, 4855, 346, 'Taige');
INSERT INTO `subdistricts` VALUES (4856, 4856, 346, 'Testega');
INSERT INTO `subdistricts` VALUES (4857, 4857, 347, 'Aboy');
INSERT INTO `subdistricts` VALUES (4858, 4858, 347, 'Alemsom');
INSERT INTO `subdistricts` VALUES (4859, 4859, 347, 'Awinbon');
INSERT INTO `subdistricts` VALUES (4860, 4860, 347, 'Batani');
INSERT INTO `subdistricts` VALUES (4861, 4861, 347, 'Batom');
INSERT INTO `subdistricts` VALUES (4862, 4862, 347, 'Bime');
INSERT INTO `subdistricts` VALUES (4863, 4863, 347, 'Borme');
INSERT INTO `subdistricts` VALUES (4864, 4864, 347, 'Eipumek');
INSERT INTO `subdistricts` VALUES (4865, 4865, 347, 'Iwur (Okiwur)');
INSERT INTO `subdistricts` VALUES (4866, 4866, 347, 'Jetfa');
INSERT INTO `subdistricts` VALUES (4867, 4867, 347, 'Kalomdol');
INSERT INTO `subdistricts` VALUES (4868, 4868, 347, 'Kawor');
INSERT INTO `subdistricts` VALUES (4869, 4869, 347, 'Kiwirok');
INSERT INTO `subdistricts` VALUES (4870, 4870, 347, 'Kiwirok Timur');
INSERT INTO `subdistricts` VALUES (4871, 4871, 347, 'Mofinop');
INSERT INTO `subdistricts` VALUES (4872, 4872, 347, 'Murkim');
INSERT INTO `subdistricts` VALUES (4873, 4873, 347, 'Nongme');
INSERT INTO `subdistricts` VALUES (4874, 4874, 347, 'Ok Aom');
INSERT INTO `subdistricts` VALUES (4875, 4875, 347, 'Okbab');
INSERT INTO `subdistricts` VALUES (4876, 4876, 347, 'Okbape');
INSERT INTO `subdistricts` VALUES (4877, 4877, 347, 'Okbemtau');
INSERT INTO `subdistricts` VALUES (4878, 4878, 347, 'Okbibab');
INSERT INTO `subdistricts` VALUES (4879, 4879, 347, 'Okhika');
INSERT INTO `subdistricts` VALUES (4880, 4880, 347, 'Oklip');
INSERT INTO `subdistricts` VALUES (4881, 4881, 347, 'Oksamol');
INSERT INTO `subdistricts` VALUES (4882, 4882, 347, 'Oksebang');
INSERT INTO `subdistricts` VALUES (4883, 4883, 347, 'Oksibil');
INSERT INTO `subdistricts` VALUES (4884, 4884, 347, 'Oksop');
INSERT INTO `subdistricts` VALUES (4885, 4885, 347, 'Pamek');
INSERT INTO `subdistricts` VALUES (4886, 4886, 347, 'Pepera');
INSERT INTO `subdistricts` VALUES (4887, 4887, 347, 'Serambakon');
INSERT INTO `subdistricts` VALUES (4888, 4888, 347, 'Tarup');
INSERT INTO `subdistricts` VALUES (4889, 4889, 347, 'Teiraplu');
INSERT INTO `subdistricts` VALUES (4890, 4890, 347, 'Weime');
INSERT INTO `subdistricts` VALUES (4891, 4891, 348, 'Bojong');
INSERT INTO `subdistricts` VALUES (4892, 4892, 348, 'Buaran');
INSERT INTO `subdistricts` VALUES (4893, 4893, 348, 'Doro');
INSERT INTO `subdistricts` VALUES (4894, 4894, 348, 'Kajen');
INSERT INTO `subdistricts` VALUES (4895, 4895, 348, 'Kandangserang');
INSERT INTO `subdistricts` VALUES (4896, 4896, 348, 'Karanganyar');
INSERT INTO `subdistricts` VALUES (4897, 4897, 348, 'Karangdadap');
INSERT INTO `subdistricts` VALUES (4898, 4898, 348, 'Kedungwuni');
INSERT INTO `subdistricts` VALUES (4899, 4899, 348, 'Kesesi');
INSERT INTO `subdistricts` VALUES (4900, 4900, 348, 'Lebakbarang');
INSERT INTO `subdistricts` VALUES (4901, 4901, 348, 'Paninggaran');
INSERT INTO `subdistricts` VALUES (4902, 4902, 348, 'Petungkriono/Petungkriyono');
INSERT INTO `subdistricts` VALUES (4903, 4903, 348, 'Siwalan');
INSERT INTO `subdistricts` VALUES (4904, 4904, 348, 'Sragi');
INSERT INTO `subdistricts` VALUES (4905, 4905, 348, 'Talun');
INSERT INTO `subdistricts` VALUES (4906, 4906, 348, 'Tirto');
INSERT INTO `subdistricts` VALUES (4907, 4907, 348, 'Wiradesa');
INSERT INTO `subdistricts` VALUES (4908, 4908, 348, 'Wonokerto');
INSERT INTO `subdistricts` VALUES (4909, 4909, 348, 'Wonopringgo');
INSERT INTO `subdistricts` VALUES (4910, 4910, 349, 'Pekalongan Barat');
INSERT INTO `subdistricts` VALUES (4911, 4911, 349, 'Pekalongan Selatan');
INSERT INTO `subdistricts` VALUES (4912, 4912, 349, 'Pekalongan Timur');
INSERT INTO `subdistricts` VALUES (4913, 4913, 349, 'Pekalongan Utara');
INSERT INTO `subdistricts` VALUES (4914, 4914, 350, 'Bukit Raya');
INSERT INTO `subdistricts` VALUES (4915, 4915, 350, 'Lima Puluh');
INSERT INTO `subdistricts` VALUES (4916, 4916, 350, 'Marpoyan Damai');
INSERT INTO `subdistricts` VALUES (4917, 4917, 350, 'Payung Sekaki');
INSERT INTO `subdistricts` VALUES (4918, 4918, 350, 'Pekanbaru Kota');
INSERT INTO `subdistricts` VALUES (4919, 4919, 350, 'Rumbai');
INSERT INTO `subdistricts` VALUES (4920, 4920, 350, 'Rumbai Pesisir');
INSERT INTO `subdistricts` VALUES (4921, 4921, 350, 'Sail');
INSERT INTO `subdistricts` VALUES (4922, 4922, 350, 'Senapelan');
INSERT INTO `subdistricts` VALUES (4923, 4923, 350, 'Sukajadi');
INSERT INTO `subdistricts` VALUES (4924, 4924, 350, 'Tampan');
INSERT INTO `subdistricts` VALUES (4925, 4925, 350, 'Tenayan Raya');
INSERT INTO `subdistricts` VALUES (4926, 4926, 351, 'Bandar Petalangan');
INSERT INTO `subdistricts` VALUES (4927, 4927, 351, 'Bandar Sei Kijang');
INSERT INTO `subdistricts` VALUES (4928, 4928, 351, 'Bunut');
INSERT INTO `subdistricts` VALUES (4929, 4929, 351, 'Kerumutan');
INSERT INTO `subdistricts` VALUES (4930, 4930, 351, 'Kuala Kampar');
INSERT INTO `subdistricts` VALUES (4931, 4931, 351, 'Langgam');
INSERT INTO `subdistricts` VALUES (4932, 4932, 351, 'Pangkalan Kerinci');
INSERT INTO `subdistricts` VALUES (4933, 4933, 351, 'Pangkalan Kuras');
INSERT INTO `subdistricts` VALUES (4934, 4934, 351, 'Pangkalan Lesung');
INSERT INTO `subdistricts` VALUES (4935, 4935, 351, 'Pelalawan');
INSERT INTO `subdistricts` VALUES (4936, 4936, 351, 'Teluk Meranti');
INSERT INTO `subdistricts` VALUES (4937, 4937, 351, 'Ukui');
INSERT INTO `subdistricts` VALUES (4938, 4938, 352, 'Ampelgading');
INSERT INTO `subdistricts` VALUES (4939, 4939, 352, 'Bantarbolang');
INSERT INTO `subdistricts` VALUES (4940, 4940, 352, 'Belik');
INSERT INTO `subdistricts` VALUES (4941, 4941, 352, 'Bodeh');
INSERT INTO `subdistricts` VALUES (4942, 4942, 352, 'Comal');
INSERT INTO `subdistricts` VALUES (4943, 4943, 352, 'Moga');
INSERT INTO `subdistricts` VALUES (4944, 4944, 352, 'Pemalang');
INSERT INTO `subdistricts` VALUES (4945, 4945, 352, 'Petarukan');
INSERT INTO `subdistricts` VALUES (4946, 4946, 352, 'Pulosari');
INSERT INTO `subdistricts` VALUES (4947, 4947, 352, 'Randudongkal');
INSERT INTO `subdistricts` VALUES (4948, 4948, 352, 'Taman');
INSERT INTO `subdistricts` VALUES (4949, 4949, 352, 'Ulujami');
INSERT INTO `subdistricts` VALUES (4950, 4950, 352, 'Warungpring');
INSERT INTO `subdistricts` VALUES (4951, 4951, 352, 'Watukumpul');
INSERT INTO `subdistricts` VALUES (4952, 4952, 353, 'Siantar Barat');
INSERT INTO `subdistricts` VALUES (4953, 4953, 353, 'Siantar Marihat');
INSERT INTO `subdistricts` VALUES (4954, 4954, 353, 'Siantar Marimbun');
INSERT INTO `subdistricts` VALUES (4955, 4955, 353, 'Siantar Martoba');
INSERT INTO `subdistricts` VALUES (4956, 4956, 353, 'Siantar Selatan');
INSERT INTO `subdistricts` VALUES (4957, 4957, 353, 'Siantar Sitalasari');
INSERT INTO `subdistricts` VALUES (4958, 4958, 353, 'Siantar Timur');
INSERT INTO `subdistricts` VALUES (4959, 4959, 353, 'Siantar Utara');
INSERT INTO `subdistricts` VALUES (4960, 4960, 354, 'Babulu');
INSERT INTO `subdistricts` VALUES (4961, 4961, 354, 'Penajam');
INSERT INTO `subdistricts` VALUES (4962, 4962, 354, 'Sepaku');
INSERT INTO `subdistricts` VALUES (4963, 4963, 354, 'Waru');
INSERT INTO `subdistricts` VALUES (4964, 4964, 355, 'Gedong Tataan (Gedung Tataan)');
INSERT INTO `subdistricts` VALUES (4965, 4965, 355, 'Kedondong');
INSERT INTO `subdistricts` VALUES (4966, 4966, 355, 'Marga Punduh');
INSERT INTO `subdistricts` VALUES (4967, 4967, 355, 'Negeri Katon');
INSERT INTO `subdistricts` VALUES (4968, 4968, 355, 'Padang Cermin');
INSERT INTO `subdistricts` VALUES (4969, 4969, 355, 'Punduh Pidada (Pedada)');
INSERT INTO `subdistricts` VALUES (4970, 4970, 355, 'Tegineneng');
INSERT INTO `subdistricts` VALUES (4971, 4971, 355, 'Way Khilau');
INSERT INTO `subdistricts` VALUES (4972, 4972, 355, 'Way Lima');
INSERT INTO `subdistricts` VALUES (4973, 4973, 356, 'Bengkunat');
INSERT INTO `subdistricts` VALUES (4974, 4974, 356, 'Bengkunat Belimbing');
INSERT INTO `subdistricts` VALUES (4975, 4975, 356, 'Karya Penggawa');
INSERT INTO `subdistricts` VALUES (4976, 4976, 356, 'Krui Selatan');
INSERT INTO `subdistricts` VALUES (4977, 4977, 356, 'Lemong');
INSERT INTO `subdistricts` VALUES (4978, 4978, 356, 'Ngambur');
INSERT INTO `subdistricts` VALUES (4979, 4979, 356, 'Pesisir Selatan');
INSERT INTO `subdistricts` VALUES (4980, 4980, 356, 'Pesisir Tengah');
INSERT INTO `subdistricts` VALUES (4981, 4981, 356, 'Pesisir Utara');
INSERT INTO `subdistricts` VALUES (4982, 4982, 356, 'Pulau Pisang');
INSERT INTO `subdistricts` VALUES (4983, 4983, 356, 'Way Krui');
INSERT INTO `subdistricts` VALUES (4984, 4984, 357, 'Airpura');
INSERT INTO `subdistricts` VALUES (4985, 4985, 357, 'Basa Ampek Balai Tapan');
INSERT INTO `subdistricts` VALUES (4986, 4986, 357, 'Batang Kapas');
INSERT INTO `subdistricts` VALUES (4987, 4987, 357, 'Bayang');
INSERT INTO `subdistricts` VALUES (4988, 4988, 357, 'IV Jurai');
INSERT INTO `subdistricts` VALUES (4989, 4989, 357, 'IV Nagari Bayang Utara');
INSERT INTO `subdistricts` VALUES (4990, 4990, 357, 'Koto XI Tarusan');
INSERT INTO `subdistricts` VALUES (4991, 4991, 357, 'Lengayang');
INSERT INTO `subdistricts` VALUES (4992, 4992, 357, 'Linggo Sari Baganti');
INSERT INTO `subdistricts` VALUES (4993, 4993, 357, 'Lunang');
INSERT INTO `subdistricts` VALUES (4994, 4994, 357, 'Pancung Soal');
INSERT INTO `subdistricts` VALUES (4995, 4995, 357, 'Ranah Ampek Hulu Tapan');
INSERT INTO `subdistricts` VALUES (4996, 4996, 357, 'Ranah Pesisir');
INSERT INTO `subdistricts` VALUES (4997, 4997, 357, 'Silaut');
INSERT INTO `subdistricts` VALUES (4998, 4998, 357, 'Sutera');
INSERT INTO `subdistricts` VALUES (4999, 4999, 358, 'Batee');
INSERT INTO `subdistricts` VALUES (5000, 5000, 358, 'Delima');
INSERT INTO `subdistricts` VALUES (5001, 5001, 358, 'Geumpang');
INSERT INTO `subdistricts` VALUES (5002, 5002, 358, 'Glumpang Baro');
INSERT INTO `subdistricts` VALUES (5003, 5003, 358, 'Glumpang Tiga (Geulumpang Tiga)');
INSERT INTO `subdistricts` VALUES (5004, 5004, 358, 'Grong Grong');
INSERT INTO `subdistricts` VALUES (5005, 5005, 358, 'Indrajaya');
INSERT INTO `subdistricts` VALUES (5006, 5006, 358, 'Kembang Tanjong (Tanjung)');
INSERT INTO `subdistricts` VALUES (5007, 5007, 358, 'Keumala');
INSERT INTO `subdistricts` VALUES (5008, 5008, 358, 'Kota Sigli');
INSERT INTO `subdistricts` VALUES (5009, 5009, 358, 'Mane');
INSERT INTO `subdistricts` VALUES (5010, 5010, 358, 'Mila');
INSERT INTO `subdistricts` VALUES (5011, 5011, 358, 'Muara Tiga');
INSERT INTO `subdistricts` VALUES (5012, 5012, 358, 'Mutiara');
INSERT INTO `subdistricts` VALUES (5013, 5013, 358, 'Mutiara Timur');
INSERT INTO `subdistricts` VALUES (5014, 5014, 358, 'Padang Tiji');
INSERT INTO `subdistricts` VALUES (5015, 5015, 358, 'Peukan Baro');
INSERT INTO `subdistricts` VALUES (5016, 5016, 358, 'Pidie');
INSERT INTO `subdistricts` VALUES (5017, 5017, 358, 'Sakti');
INSERT INTO `subdistricts` VALUES (5018, 5018, 358, 'Simpang Tiga');
INSERT INTO `subdistricts` VALUES (5019, 5019, 358, 'Tangse');
INSERT INTO `subdistricts` VALUES (5020, 5020, 358, 'Tiro/Truseb');
INSERT INTO `subdistricts` VALUES (5021, 5021, 358, 'Titeue');
INSERT INTO `subdistricts` VALUES (5022, 5022, 359, 'Bandar Baru');
INSERT INTO `subdistricts` VALUES (5023, 5023, 359, 'Bandar Dua');
INSERT INTO `subdistricts` VALUES (5024, 5024, 359, 'Jangka Buya');
INSERT INTO `subdistricts` VALUES (5025, 5025, 359, 'Meurah Dua');
INSERT INTO `subdistricts` VALUES (5026, 5026, 359, 'Meureudu');
INSERT INTO `subdistricts` VALUES (5027, 5027, 359, 'Panteraja');
INSERT INTO `subdistricts` VALUES (5028, 5028, 359, 'Trienggadeng');
INSERT INTO `subdistricts` VALUES (5029, 5029, 359, 'Ulim');
INSERT INTO `subdistricts` VALUES (5030, 5030, 360, 'Batulappa');
INSERT INTO `subdistricts` VALUES (5031, 5031, 360, 'Cempa');
INSERT INTO `subdistricts` VALUES (5032, 5032, 360, 'Duampanua');
INSERT INTO `subdistricts` VALUES (5033, 5033, 360, 'Lanrisang');
INSERT INTO `subdistricts` VALUES (5034, 5034, 360, 'Lembang');
INSERT INTO `subdistricts` VALUES (5035, 5035, 360, 'Mattiro Bulu');
INSERT INTO `subdistricts` VALUES (5036, 5036, 360, 'Mattiro Sompe');
INSERT INTO `subdistricts` VALUES (5037, 5037, 360, 'Paleteang');
INSERT INTO `subdistricts` VALUES (5038, 5038, 360, 'Patampanua');
INSERT INTO `subdistricts` VALUES (5039, 5039, 360, 'Suppa');
INSERT INTO `subdistricts` VALUES (5040, 5040, 360, 'Tiroang');
INSERT INTO `subdistricts` VALUES (5041, 5041, 360, 'Watang Sawitto');
INSERT INTO `subdistricts` VALUES (5042, 5042, 361, 'Buntulia');
INSERT INTO `subdistricts` VALUES (5043, 5043, 361, 'Dengilo');
INSERT INTO `subdistricts` VALUES (5044, 5044, 361, 'Duhiadaa');
INSERT INTO `subdistricts` VALUES (5045, 5045, 361, 'Lemito');
INSERT INTO `subdistricts` VALUES (5046, 5046, 361, 'Marisa');
INSERT INTO `subdistricts` VALUES (5047, 5047, 361, 'Paguat');
INSERT INTO `subdistricts` VALUES (5048, 5048, 361, 'Patilanggio');
INSERT INTO `subdistricts` VALUES (5049, 5049, 361, 'Popayato');
INSERT INTO `subdistricts` VALUES (5050, 5050, 361, 'Popayato Barat');
INSERT INTO `subdistricts` VALUES (5051, 5051, 361, 'Popayato Timur');
INSERT INTO `subdistricts` VALUES (5052, 5052, 361, 'Randangan');
INSERT INTO `subdistricts` VALUES (5053, 5053, 361, 'Taluditi (Taluduti)');
INSERT INTO `subdistricts` VALUES (5054, 5054, 361, 'Wanggarasi');
INSERT INTO `subdistricts` VALUES (5055, 5055, 362, 'Alu (Allu)');
INSERT INTO `subdistricts` VALUES (5056, 5056, 362, 'Anreapi');
INSERT INTO `subdistricts` VALUES (5057, 5057, 362, 'Balanipa');
INSERT INTO `subdistricts` VALUES (5058, 5058, 362, 'Binuang');
INSERT INTO `subdistricts` VALUES (5059, 5059, 362, 'Bulo');
INSERT INTO `subdistricts` VALUES (5060, 5060, 362, 'Campalagian');
INSERT INTO `subdistricts` VALUES (5061, 5061, 362, 'Limboro');
INSERT INTO `subdistricts` VALUES (5062, 5062, 362, 'Luyo');
INSERT INTO `subdistricts` VALUES (5063, 5063, 362, 'Mapilli');
INSERT INTO `subdistricts` VALUES (5064, 5064, 362, 'Matakali');
INSERT INTO `subdistricts` VALUES (5065, 5065, 362, 'Matangnga');
INSERT INTO `subdistricts` VALUES (5066, 5066, 362, 'Polewali');
INSERT INTO `subdistricts` VALUES (5067, 5067, 362, 'Tapango');
INSERT INTO `subdistricts` VALUES (5068, 5068, 362, 'Tinambung');
INSERT INTO `subdistricts` VALUES (5069, 5069, 362, 'Tubbi Taramanu (Tutar/Tutallu)');
INSERT INTO `subdistricts` VALUES (5070, 5070, 362, 'Wonomulyo');
INSERT INTO `subdistricts` VALUES (5071, 5071, 363, 'Babadan');
INSERT INTO `subdistricts` VALUES (5072, 5072, 363, 'Badegan');
INSERT INTO `subdistricts` VALUES (5073, 5073, 363, 'Balong');
INSERT INTO `subdistricts` VALUES (5074, 5074, 363, 'Bungkal');
INSERT INTO `subdistricts` VALUES (5075, 5075, 363, 'Jambon');
INSERT INTO `subdistricts` VALUES (5076, 5076, 363, 'Jenangan');
INSERT INTO `subdistricts` VALUES (5077, 5077, 363, 'Jetis');
INSERT INTO `subdistricts` VALUES (5078, 5078, 363, 'Kauman');
INSERT INTO `subdistricts` VALUES (5079, 5079, 363, 'Mlarak');
INSERT INTO `subdistricts` VALUES (5080, 5080, 363, 'Ngebel');
INSERT INTO `subdistricts` VALUES (5081, 5081, 363, 'Ngrayun');
INSERT INTO `subdistricts` VALUES (5082, 5082, 363, 'Ponorogo');
INSERT INTO `subdistricts` VALUES (5083, 5083, 363, 'Pudak');
INSERT INTO `subdistricts` VALUES (5084, 5084, 363, 'Pulung');
INSERT INTO `subdistricts` VALUES (5085, 5085, 363, 'Sambit');
INSERT INTO `subdistricts` VALUES (5086, 5086, 363, 'Sampung');
INSERT INTO `subdistricts` VALUES (5087, 5087, 363, 'Sawoo');
INSERT INTO `subdistricts` VALUES (5088, 5088, 363, 'Siman');
INSERT INTO `subdistricts` VALUES (5089, 5089, 363, 'Slahung');
INSERT INTO `subdistricts` VALUES (5090, 5090, 363, 'Sooko');
INSERT INTO `subdistricts` VALUES (5091, 5091, 363, 'Sukorejo');
INSERT INTO `subdistricts` VALUES (5092, 5092, 364, 'Anjongan');
INSERT INTO `subdistricts` VALUES (5093, 5093, 364, 'Mempawah Hilir');
INSERT INTO `subdistricts` VALUES (5094, 5094, 364, 'Mempawah Timur');
INSERT INTO `subdistricts` VALUES (5095, 5095, 364, 'Sadaniang');
INSERT INTO `subdistricts` VALUES (5096, 5096, 364, 'Segedong');
INSERT INTO `subdistricts` VALUES (5097, 5097, 364, 'Sei/Sungai Kunyit');
INSERT INTO `subdistricts` VALUES (5098, 5098, 364, 'Sei/Sungai Pinyuh');
INSERT INTO `subdistricts` VALUES (5099, 5099, 364, 'Siantan');
INSERT INTO `subdistricts` VALUES (5100, 5100, 364, 'Toho');
INSERT INTO `subdistricts` VALUES (5101, 5101, 365, 'Pontianak Barat');
INSERT INTO `subdistricts` VALUES (5102, 5102, 365, 'Pontianak Kota');
INSERT INTO `subdistricts` VALUES (5103, 5103, 365, 'Pontianak Selatan');
INSERT INTO `subdistricts` VALUES (5104, 5104, 365, 'Pontianak Tenggara');
INSERT INTO `subdistricts` VALUES (5105, 5105, 365, 'Pontianak Timur');
INSERT INTO `subdistricts` VALUES (5106, 5106, 365, 'Pontianak Utara');
INSERT INTO `subdistricts` VALUES (5107, 5107, 366, 'Lage');
INSERT INTO `subdistricts` VALUES (5108, 5108, 366, 'Lore Barat');
INSERT INTO `subdistricts` VALUES (5109, 5109, 366, 'Lore Piore');
INSERT INTO `subdistricts` VALUES (5110, 5110, 366, 'Lore Selatan');
INSERT INTO `subdistricts` VALUES (5111, 5111, 366, 'Lore Tengah');
INSERT INTO `subdistricts` VALUES (5112, 5112, 366, 'Lore Timur');
INSERT INTO `subdistricts` VALUES (5113, 5113, 366, 'Lore Utara');
INSERT INTO `subdistricts` VALUES (5114, 5114, 366, 'Pamona Barat');
INSERT INTO `subdistricts` VALUES (5115, 5115, 366, 'Pamona Puselemba');
INSERT INTO `subdistricts` VALUES (5116, 5116, 366, 'Pamona Selatan');
INSERT INTO `subdistricts` VALUES (5117, 5117, 366, 'Pamona Tenggara');
INSERT INTO `subdistricts` VALUES (5118, 5118, 366, 'Pamona Timur');
INSERT INTO `subdistricts` VALUES (5119, 5119, 366, 'Pamona Utara');
INSERT INTO `subdistricts` VALUES (5120, 5120, 366, 'Poso Kota');
INSERT INTO `subdistricts` VALUES (5121, 5121, 366, 'Poso Kota Selatan');
INSERT INTO `subdistricts` VALUES (5122, 5122, 366, 'Poso Kota Utara');
INSERT INTO `subdistricts` VALUES (5123, 5123, 366, 'Poso Pesisir');
INSERT INTO `subdistricts` VALUES (5124, 5124, 366, 'Poso Pesisir Selatan');
INSERT INTO `subdistricts` VALUES (5125, 5125, 366, 'Poso Pesisir Utara');
INSERT INTO `subdistricts` VALUES (5126, 5126, 367, 'Cambai');
INSERT INTO `subdistricts` VALUES (5127, 5127, 367, 'Prabumulih Barat');
INSERT INTO `subdistricts` VALUES (5128, 5128, 367, 'Prabumulih Selatan');
INSERT INTO `subdistricts` VALUES (5129, 5129, 367, 'Prabumulih Timur');
INSERT INTO `subdistricts` VALUES (5130, 5130, 367, 'Prabumulih Utara');
INSERT INTO `subdistricts` VALUES (5131, 5131, 367, 'Rambang Kapak Tengah');
INSERT INTO `subdistricts` VALUES (5132, 5132, 368, 'Adi Luwih');
INSERT INTO `subdistricts` VALUES (5133, 5133, 368, 'Ambarawa');
INSERT INTO `subdistricts` VALUES (5134, 5134, 368, 'Banyumas');
INSERT INTO `subdistricts` VALUES (5135, 5135, 368, 'Gading Rejo');
INSERT INTO `subdistricts` VALUES (5136, 5136, 368, 'Pagelaran');
INSERT INTO `subdistricts` VALUES (5137, 5137, 368, 'Pagelaran Utara');
INSERT INTO `subdistricts` VALUES (5138, 5138, 368, 'Pardasuka');
INSERT INTO `subdistricts` VALUES (5139, 5139, 368, 'Pringsewu');
INSERT INTO `subdistricts` VALUES (5140, 5140, 368, 'Sukoharjo');
INSERT INTO `subdistricts` VALUES (5141, 5141, 369, 'Bantaran');
INSERT INTO `subdistricts` VALUES (5142, 5142, 369, 'Banyu Anyar');
INSERT INTO `subdistricts` VALUES (5143, 5143, 369, 'Besuk');
INSERT INTO `subdistricts` VALUES (5144, 5144, 369, 'Dringu');
INSERT INTO `subdistricts` VALUES (5145, 5145, 369, 'Gading');
INSERT INTO `subdistricts` VALUES (5146, 5146, 369, 'Gending');
INSERT INTO `subdistricts` VALUES (5147, 5147, 369, 'Kota Anyar');
INSERT INTO `subdistricts` VALUES (5148, 5148, 369, 'Kraksaan');
INSERT INTO `subdistricts` VALUES (5149, 5149, 369, 'Krejengan');
INSERT INTO `subdistricts` VALUES (5150, 5150, 369, 'Krucil');
INSERT INTO `subdistricts` VALUES (5151, 5151, 369, 'Kuripan');
INSERT INTO `subdistricts` VALUES (5152, 5152, 369, 'Leces');
INSERT INTO `subdistricts` VALUES (5153, 5153, 369, 'Lumbang');
INSERT INTO `subdistricts` VALUES (5154, 5154, 369, 'Maron');
INSERT INTO `subdistricts` VALUES (5155, 5155, 369, 'Paiton');
INSERT INTO `subdistricts` VALUES (5156, 5156, 369, 'Pajarakan');
INSERT INTO `subdistricts` VALUES (5157, 5157, 369, 'Pakuniran');
INSERT INTO `subdistricts` VALUES (5158, 5158, 369, 'Sukapura');
INSERT INTO `subdistricts` VALUES (5159, 5159, 369, 'Sumber');
INSERT INTO `subdistricts` VALUES (5160, 5160, 369, 'Sumberasih');
INSERT INTO `subdistricts` VALUES (5161, 5161, 369, 'Tegal Siwalan');
INSERT INTO `subdistricts` VALUES (5162, 5162, 369, 'Tiris');
INSERT INTO `subdistricts` VALUES (5163, 5163, 369, 'Tongas');
INSERT INTO `subdistricts` VALUES (5164, 5164, 369, 'Wonomerto');
INSERT INTO `subdistricts` VALUES (5165, 5165, 370, 'Kademangan');
INSERT INTO `subdistricts` VALUES (5166, 5166, 370, 'Kanigaran');
INSERT INTO `subdistricts` VALUES (5167, 5167, 370, 'Kedopok (Kedopak)');
INSERT INTO `subdistricts` VALUES (5168, 5168, 370, 'Mayangan');
INSERT INTO `subdistricts` VALUES (5169, 5169, 370, 'Wonoasih');
INSERT INTO `subdistricts` VALUES (5170, 5170, 371, 'Banama Tingang');
INSERT INTO `subdistricts` VALUES (5171, 5171, 371, 'Jabiren Raya');
INSERT INTO `subdistricts` VALUES (5172, 5172, 371, 'Kahayan Hilir');
INSERT INTO `subdistricts` VALUES (5173, 5173, 371, 'Kahayan Kuala');
INSERT INTO `subdistricts` VALUES (5174, 5174, 371, 'Kahayan Tengah');
INSERT INTO `subdistricts` VALUES (5175, 5175, 371, 'Maliku');
INSERT INTO `subdistricts` VALUES (5176, 5176, 371, 'Pandih Batu');
INSERT INTO `subdistricts` VALUES (5177, 5177, 371, 'Sebangau Kuala');
INSERT INTO `subdistricts` VALUES (5178, 5178, 372, 'Morotai Jaya');
INSERT INTO `subdistricts` VALUES (5179, 5179, 372, 'Morotai Selatan');
INSERT INTO `subdistricts` VALUES (5180, 5180, 372, 'Morotai Selatan Barat');
INSERT INTO `subdistricts` VALUES (5181, 5181, 372, 'Morotai Timur');
INSERT INTO `subdistricts` VALUES (5182, 5182, 372, 'Morotai Utara');
INSERT INTO `subdistricts` VALUES (5183, 5183, 373, 'Agadugume');
INSERT INTO `subdistricts` VALUES (5184, 5184, 373, 'Beoga');
INSERT INTO `subdistricts` VALUES (5185, 5185, 373, 'Doufu');
INSERT INTO `subdistricts` VALUES (5186, 5186, 373, 'Gome');
INSERT INTO `subdistricts` VALUES (5187, 5187, 373, 'Ilaga');
INSERT INTO `subdistricts` VALUES (5188, 5188, 373, 'Pogoma');
INSERT INTO `subdistricts` VALUES (5189, 5189, 373, 'Sinak');
INSERT INTO `subdistricts` VALUES (5190, 5190, 373, 'Wangbe');
INSERT INTO `subdistricts` VALUES (5191, 5191, 374, 'Fawi');
INSERT INTO `subdistricts` VALUES (5192, 5192, 374, 'Ilu');
INSERT INTO `subdistricts` VALUES (5193, 5193, 374, 'Jigonikme');
INSERT INTO `subdistricts` VALUES (5194, 5194, 374, 'Mewoluk (Mewulok)');
INSERT INTO `subdistricts` VALUES (5195, 5195, 374, 'Mulia');
INSERT INTO `subdistricts` VALUES (5196, 5196, 374, 'Tingginambut');
INSERT INTO `subdistricts` VALUES (5197, 5197, 374, 'Torere');
INSERT INTO `subdistricts` VALUES (5198, 5198, 374, 'Yamo');
INSERT INTO `subdistricts` VALUES (5199, 5199, 375, 'Bobotsari');
INSERT INTO `subdistricts` VALUES (5200, 5200, 375, 'Bojongsari');
INSERT INTO `subdistricts` VALUES (5201, 5201, 375, 'Bukateja');
INSERT INTO `subdistricts` VALUES (5202, 5202, 375, 'Kaligondang');
INSERT INTO `subdistricts` VALUES (5203, 5203, 375, 'Kalimanah');
INSERT INTO `subdistricts` VALUES (5204, 5204, 375, 'Karanganyar');
INSERT INTO `subdistricts` VALUES (5205, 5205, 375, 'Karangjambu');
INSERT INTO `subdistricts` VALUES (5206, 5206, 375, 'Karangmoncol');
INSERT INTO `subdistricts` VALUES (5207, 5207, 375, 'Karangreja');
INSERT INTO `subdistricts` VALUES (5208, 5208, 375, 'Kejobong');
INSERT INTO `subdistricts` VALUES (5209, 5209, 375, 'Kemangkon');
INSERT INTO `subdistricts` VALUES (5210, 5210, 375, 'Kertanegara');
INSERT INTO `subdistricts` VALUES (5211, 5211, 375, 'Kutasari');
INSERT INTO `subdistricts` VALUES (5212, 5212, 375, 'Mrebet');
INSERT INTO `subdistricts` VALUES (5213, 5213, 375, 'Padamara');
INSERT INTO `subdistricts` VALUES (5214, 5214, 375, 'Pengadegan');
INSERT INTO `subdistricts` VALUES (5215, 5215, 375, 'Purbalingga');
INSERT INTO `subdistricts` VALUES (5216, 5216, 375, 'Rembang');
INSERT INTO `subdistricts` VALUES (5217, 5217, 376, 'Babakancikao');
INSERT INTO `subdistricts` VALUES (5218, 5218, 376, 'Bojong');
INSERT INTO `subdistricts` VALUES (5219, 5219, 376, 'Bungursari');
INSERT INTO `subdistricts` VALUES (5220, 5220, 376, 'Campaka');
INSERT INTO `subdistricts` VALUES (5221, 5221, 376, 'Cibatu');
INSERT INTO `subdistricts` VALUES (5222, 5222, 376, 'Darangdan');
INSERT INTO `subdistricts` VALUES (5223, 5223, 376, 'Jatiluhur');
INSERT INTO `subdistricts` VALUES (5224, 5224, 376, 'Kiarapedes');
INSERT INTO `subdistricts` VALUES (5225, 5225, 376, 'Maniis');
INSERT INTO `subdistricts` VALUES (5226, 5226, 376, 'Pasawahan');
INSERT INTO `subdistricts` VALUES (5227, 5227, 376, 'Plered');
INSERT INTO `subdistricts` VALUES (5228, 5228, 376, 'Pondoksalam');
INSERT INTO `subdistricts` VALUES (5229, 5229, 376, 'Purwakarta');
INSERT INTO `subdistricts` VALUES (5230, 5230, 376, 'Sukasari');
INSERT INTO `subdistricts` VALUES (5231, 5231, 376, 'Sukatani');
INSERT INTO `subdistricts` VALUES (5232, 5232, 376, 'Tegal Waru');
INSERT INTO `subdistricts` VALUES (5233, 5233, 376, 'Wanayasa');
INSERT INTO `subdistricts` VALUES (5234, 5234, 377, 'Bagelen');
INSERT INTO `subdistricts` VALUES (5235, 5235, 377, 'Banyuurip');
INSERT INTO `subdistricts` VALUES (5236, 5236, 377, 'Bayan');
INSERT INTO `subdistricts` VALUES (5237, 5237, 377, 'Bener');
INSERT INTO `subdistricts` VALUES (5238, 5238, 377, 'Bruno');
INSERT INTO `subdistricts` VALUES (5239, 5239, 377, 'Butuh');
INSERT INTO `subdistricts` VALUES (5240, 5240, 377, 'Gebang');
INSERT INTO `subdistricts` VALUES (5241, 5241, 377, 'Grabag');
INSERT INTO `subdistricts` VALUES (5242, 5242, 377, 'Kaligesing');
INSERT INTO `subdistricts` VALUES (5243, 5243, 377, 'Kemiri');
INSERT INTO `subdistricts` VALUES (5244, 5244, 377, 'Kutoarjo');
INSERT INTO `subdistricts` VALUES (5245, 5245, 377, 'Loano');
INSERT INTO `subdistricts` VALUES (5246, 5246, 377, 'Ngombol');
INSERT INTO `subdistricts` VALUES (5247, 5247, 377, 'Pituruh');
INSERT INTO `subdistricts` VALUES (5248, 5248, 377, 'Purwodadi');
INSERT INTO `subdistricts` VALUES (5249, 5249, 377, 'Purworejo');
INSERT INTO `subdistricts` VALUES (5250, 5250, 378, 'Ayau');
INSERT INTO `subdistricts` VALUES (5251, 5251, 378, 'Batanta Selatan');
INSERT INTO `subdistricts` VALUES (5252, 5252, 378, 'Batanta Utara');
INSERT INTO `subdistricts` VALUES (5253, 5253, 378, 'Kepulauan Ayau');
INSERT INTO `subdistricts` VALUES (5254, 5254, 378, 'Kepulauan Sembilan');
INSERT INTO `subdistricts` VALUES (5255, 5255, 378, 'Kofiau');
INSERT INTO `subdistricts` VALUES (5256, 5256, 378, 'Kota Waisai');
INSERT INTO `subdistricts` VALUES (5257, 5257, 378, 'Meos Mansar');
INSERT INTO `subdistricts` VALUES (5258, 5258, 378, 'Misool (Misool Utara)');
INSERT INTO `subdistricts` VALUES (5259, 5259, 378, 'Misool Barat');
INSERT INTO `subdistricts` VALUES (5260, 5260, 378, 'Misool Selatan');
INSERT INTO `subdistricts` VALUES (5261, 5261, 378, 'Misool Timur');
INSERT INTO `subdistricts` VALUES (5262, 5262, 378, 'Salawati Barat');
INSERT INTO `subdistricts` VALUES (5263, 5263, 378, 'Salawati Tengah');
INSERT INTO `subdistricts` VALUES (5264, 5264, 378, 'Salawati Utara (Samate)');
INSERT INTO `subdistricts` VALUES (5265, 5265, 378, 'Supnin');
INSERT INTO `subdistricts` VALUES (5266, 5266, 378, 'Teluk Mayalibit');
INSERT INTO `subdistricts` VALUES (5267, 5267, 378, 'Tiplol Mayalibit');
INSERT INTO `subdistricts` VALUES (5268, 5268, 378, 'Waigeo Barat');
INSERT INTO `subdistricts` VALUES (5269, 5269, 378, 'Waigeo Barat Kepulauan');
INSERT INTO `subdistricts` VALUES (5270, 5270, 378, 'Waigeo Selatan');
INSERT INTO `subdistricts` VALUES (5271, 5271, 378, 'Waigeo Timur');
INSERT INTO `subdistricts` VALUES (5272, 5272, 378, 'Waigeo Utara');
INSERT INTO `subdistricts` VALUES (5273, 5273, 378, 'Warwabomi');
INSERT INTO `subdistricts` VALUES (5274, 5274, 379, 'Bermani Ulu');
INSERT INTO `subdistricts` VALUES (5275, 5275, 379, 'Bermani Ulu Raya');
INSERT INTO `subdistricts` VALUES (5276, 5276, 379, 'Binduriang');
INSERT INTO `subdistricts` VALUES (5277, 5277, 379, 'Curup');
INSERT INTO `subdistricts` VALUES (5278, 5278, 379, 'Curup Selatan');
INSERT INTO `subdistricts` VALUES (5279, 5279, 379, 'Curup Tengah');
INSERT INTO `subdistricts` VALUES (5280, 5280, 379, 'Curup Timur');
INSERT INTO `subdistricts` VALUES (5281, 5281, 379, 'Curup Utara');
INSERT INTO `subdistricts` VALUES (5282, 5282, 379, 'Kota Padang');
INSERT INTO `subdistricts` VALUES (5283, 5283, 379, 'Padang Ulak Tanding');
INSERT INTO `subdistricts` VALUES (5284, 5284, 379, 'Selupu Rejang');
INSERT INTO `subdistricts` VALUES (5285, 5285, 379, 'Sindang Beliti Ilir');
INSERT INTO `subdistricts` VALUES (5286, 5286, 379, 'Sindang Beliti Ulu');
INSERT INTO `subdistricts` VALUES (5287, 5287, 379, 'Sindang Daratan');
INSERT INTO `subdistricts` VALUES (5288, 5288, 379, 'Sindang Kelingi');
INSERT INTO `subdistricts` VALUES (5289, 5289, 380, 'Bulu');
INSERT INTO `subdistricts` VALUES (5290, 5290, 380, 'Gunem');
INSERT INTO `subdistricts` VALUES (5291, 5291, 380, 'Kaliori');
INSERT INTO `subdistricts` VALUES (5292, 5292, 380, 'Kragan');
INSERT INTO `subdistricts` VALUES (5293, 5293, 380, 'Lasem');
INSERT INTO `subdistricts` VALUES (5294, 5294, 380, 'Pamotan');
INSERT INTO `subdistricts` VALUES (5295, 5295, 380, 'Pancur');
INSERT INTO `subdistricts` VALUES (5296, 5296, 380, 'Rembang');
INSERT INTO `subdistricts` VALUES (5297, 5297, 380, 'Sale');
INSERT INTO `subdistricts` VALUES (5298, 5298, 380, 'Sarang');
INSERT INTO `subdistricts` VALUES (5299, 5299, 380, 'Sedan');
INSERT INTO `subdistricts` VALUES (5300, 5300, 380, 'Sluke');
INSERT INTO `subdistricts` VALUES (5301, 5301, 380, 'Sulang');
INSERT INTO `subdistricts` VALUES (5302, 5302, 380, 'Sumber');
INSERT INTO `subdistricts` VALUES (5303, 5303, 381, 'Bagan Sinembah');
INSERT INTO `subdistricts` VALUES (5304, 5304, 381, 'Bangko');
INSERT INTO `subdistricts` VALUES (5305, 5305, 381, 'Bangko Pusaka (Pusako)');
INSERT INTO `subdistricts` VALUES (5306, 5306, 381, 'Batu Hampar');
INSERT INTO `subdistricts` VALUES (5307, 5307, 381, 'Kubu');
INSERT INTO `subdistricts` VALUES (5308, 5308, 381, 'Kubu Babussalam');
INSERT INTO `subdistricts` VALUES (5309, 5309, 381, 'Pasir Limau Kapas');
INSERT INTO `subdistricts` VALUES (5310, 5310, 381, 'Pekaitan');
INSERT INTO `subdistricts` VALUES (5311, 5311, 381, 'Pujud');
INSERT INTO `subdistricts` VALUES (5312, 5312, 381, 'Rantau Kopar');
INSERT INTO `subdistricts` VALUES (5313, 5313, 381, 'Rimba Melintang');
INSERT INTO `subdistricts` VALUES (5314, 5314, 381, 'Simpang Kanan');
INSERT INTO `subdistricts` VALUES (5315, 5315, 381, 'Sinaboi (Senaboi)');
INSERT INTO `subdistricts` VALUES (5316, 5316, 381, 'Tanah Putih');
INSERT INTO `subdistricts` VALUES (5317, 5317, 381, 'Tanah Putih Tanjung Melawan');
INSERT INTO `subdistricts` VALUES (5318, 5318, 382, 'Bangun Purba');
INSERT INTO `subdistricts` VALUES (5319, 5319, 382, 'Bonai Darussalam');
INSERT INTO `subdistricts` VALUES (5320, 5320, 382, 'Kabun');
INSERT INTO `subdistricts` VALUES (5321, 5321, 382, 'Kepenuhan');
INSERT INTO `subdistricts` VALUES (5322, 5322, 382, 'Kepenuhan Hulu');
INSERT INTO `subdistricts` VALUES (5323, 5323, 382, 'Kunto Darussalam');
INSERT INTO `subdistricts` VALUES (5324, 5324, 382, 'Pagaran Tapah Darussalam');
INSERT INTO `subdistricts` VALUES (5325, 5325, 382, 'Pendalian IV Koto');
INSERT INTO `subdistricts` VALUES (5326, 5326, 382, 'Rambah');
INSERT INTO `subdistricts` VALUES (5327, 5327, 382, 'Rambah Hilir');
INSERT INTO `subdistricts` VALUES (5328, 5328, 382, 'Rambah Samo');
INSERT INTO `subdistricts` VALUES (5329, 5329, 382, 'Rokan IV Koto');
INSERT INTO `subdistricts` VALUES (5330, 5330, 382, 'Tambusai');
INSERT INTO `subdistricts` VALUES (5331, 5331, 382, 'Tambusai Utara');
INSERT INTO `subdistricts` VALUES (5332, 5332, 382, 'Tandun');
INSERT INTO `subdistricts` VALUES (5333, 5333, 382, 'Ujung Batu');
INSERT INTO `subdistricts` VALUES (5334, 5334, 383, 'Landu Leko');
INSERT INTO `subdistricts` VALUES (5335, 5335, 383, 'Lobalain');
INSERT INTO `subdistricts` VALUES (5336, 5336, 383, 'Ndao Nuse');
INSERT INTO `subdistricts` VALUES (5337, 5337, 383, 'Pantai Baru');
INSERT INTO `subdistricts` VALUES (5338, 5338, 383, 'Rote Barat');
INSERT INTO `subdistricts` VALUES (5339, 5339, 383, 'Rote Barat Daya');
INSERT INTO `subdistricts` VALUES (5340, 5340, 383, 'Rote Barat Laut');
INSERT INTO `subdistricts` VALUES (5341, 5341, 383, 'Rote Selatan');
INSERT INTO `subdistricts` VALUES (5342, 5342, 383, 'Rote Tengah');
INSERT INTO `subdistricts` VALUES (5343, 5343, 383, 'Rote Timur');
INSERT INTO `subdistricts` VALUES (5344, 5344, 384, 'Sukajaya');
INSERT INTO `subdistricts` VALUES (5345, 5345, 384, 'Sukakarya');
INSERT INTO `subdistricts` VALUES (5346, 5346, 385, 'Hawu Mehara');
INSERT INTO `subdistricts` VALUES (5347, 5347, 385, 'Raijua');
INSERT INTO `subdistricts` VALUES (5348, 5348, 385, 'Sabu Barat');
INSERT INTO `subdistricts` VALUES (5349, 5349, 385, 'Sabu Liae');
INSERT INTO `subdistricts` VALUES (5350, 5350, 385, 'Sabu Tengah');
INSERT INTO `subdistricts` VALUES (5351, 5351, 385, 'Sabu Timur');
INSERT INTO `subdistricts` VALUES (5352, 5352, 386, 'Argomulyo');
INSERT INTO `subdistricts` VALUES (5353, 5353, 386, 'Sidomukti');
INSERT INTO `subdistricts` VALUES (5354, 5354, 386, 'Sidorejo');
INSERT INTO `subdistricts` VALUES (5355, 5355, 386, 'Tingkir');
INSERT INTO `subdistricts` VALUES (5356, 5356, 387, 'Loa Janan Ilir');
INSERT INTO `subdistricts` VALUES (5357, 5357, 387, 'Palaran');
INSERT INTO `subdistricts` VALUES (5358, 5358, 387, 'Samarinda Ilir');
INSERT INTO `subdistricts` VALUES (5359, 5359, 387, 'Samarinda Kota');
INSERT INTO `subdistricts` VALUES (5360, 5360, 387, 'Samarinda Seberang');
INSERT INTO `subdistricts` VALUES (5361, 5361, 387, 'Samarinda Ulu');
INSERT INTO `subdistricts` VALUES (5362, 5362, 387, 'Samarinda Utara');
INSERT INTO `subdistricts` VALUES (5363, 5363, 387, 'Sambutan');
INSERT INTO `subdistricts` VALUES (5364, 5364, 387, 'Sungai Kunjang');
INSERT INTO `subdistricts` VALUES (5365, 5365, 387, 'Sungai Pinang');
INSERT INTO `subdistricts` VALUES (5366, 5366, 388, 'Galing');
INSERT INTO `subdistricts` VALUES (5367, 5367, 388, 'Jawai');
INSERT INTO `subdistricts` VALUES (5368, 5368, 388, 'Jawai Selatan');
INSERT INTO `subdistricts` VALUES (5369, 5369, 388, 'Paloh');
INSERT INTO `subdistricts` VALUES (5370, 5370, 388, 'Pemangkat');
INSERT INTO `subdistricts` VALUES (5371, 5371, 388, 'Sajad');
INSERT INTO `subdistricts` VALUES (5372, 5372, 388, 'Sajingan Besar');
INSERT INTO `subdistricts` VALUES (5373, 5373, 388, 'Salatiga');
INSERT INTO `subdistricts` VALUES (5374, 5374, 388, 'Sambas');
INSERT INTO `subdistricts` VALUES (5375, 5375, 388, 'Sebawi');
INSERT INTO `subdistricts` VALUES (5376, 5376, 388, 'Sejangkung');
INSERT INTO `subdistricts` VALUES (5377, 5377, 388, 'Selakau');
INSERT INTO `subdistricts` VALUES (5378, 5378, 388, 'Selakau Timur');
INSERT INTO `subdistricts` VALUES (5379, 5379, 388, 'Semparuk');
INSERT INTO `subdistricts` VALUES (5380, 5380, 388, 'Subah');
INSERT INTO `subdistricts` VALUES (5381, 5381, 388, 'Tangaran');
INSERT INTO `subdistricts` VALUES (5382, 5382, 388, 'Tebas');
INSERT INTO `subdistricts` VALUES (5383, 5383, 388, 'Tekarang');
INSERT INTO `subdistricts` VALUES (5384, 5384, 388, 'Teluk Keramat');
INSERT INTO `subdistricts` VALUES (5385, 5385, 389, 'Harian');
INSERT INTO `subdistricts` VALUES (5386, 5386, 389, 'Nainggolan');
INSERT INTO `subdistricts` VALUES (5387, 5387, 389, 'Onan Runggu');
INSERT INTO `subdistricts` VALUES (5388, 5388, 389, 'Palipi');
INSERT INTO `subdistricts` VALUES (5389, 5389, 389, 'Pangururan');
INSERT INTO `subdistricts` VALUES (5390, 5390, 389, 'Ronggur Nihuta');
INSERT INTO `subdistricts` VALUES (5391, 5391, 389, 'Sianjur Mula-Mula');
INSERT INTO `subdistricts` VALUES (5392, 5392, 389, 'Simanindo');
INSERT INTO `subdistricts` VALUES (5393, 5393, 389, 'Sitio-Tio');
INSERT INTO `subdistricts` VALUES (5394, 5394, 390, 'Banyuates');
INSERT INTO `subdistricts` VALUES (5395, 5395, 390, 'Camplong');
INSERT INTO `subdistricts` VALUES (5396, 5396, 390, 'Jrengik');
INSERT INTO `subdistricts` VALUES (5397, 5397, 390, 'Karang Penang');
INSERT INTO `subdistricts` VALUES (5398, 5398, 390, 'Kedungdung');
INSERT INTO `subdistricts` VALUES (5399, 5399, 390, 'Ketapang');
INSERT INTO `subdistricts` VALUES (5400, 5400, 390, 'Omben');
INSERT INTO `subdistricts` VALUES (5401, 5401, 390, 'Pangarengan');
INSERT INTO `subdistricts` VALUES (5402, 5402, 390, 'Robatal');
INSERT INTO `subdistricts` VALUES (5403, 5403, 390, 'Sampang');
INSERT INTO `subdistricts` VALUES (5404, 5404, 390, 'Sokobanah');
INSERT INTO `subdistricts` VALUES (5405, 5405, 390, 'Sreseh');
INSERT INTO `subdistricts` VALUES (5406, 5406, 390, 'Tambelangan');
INSERT INTO `subdistricts` VALUES (5407, 5407, 390, 'Torjun');
INSERT INTO `subdistricts` VALUES (5408, 5408, 391, 'Balai');
INSERT INTO `subdistricts` VALUES (5409, 5409, 391, 'Beduai (Beduwai)');
INSERT INTO `subdistricts` VALUES (5410, 5410, 391, 'Bonti');
INSERT INTO `subdistricts` VALUES (5411, 5411, 391, 'Entikong');
INSERT INTO `subdistricts` VALUES (5412, 5412, 391, 'Jangkang');
INSERT INTO `subdistricts` VALUES (5413, 5413, 391, 'Kapuas (Sanggau Kapuas)');
INSERT INTO `subdistricts` VALUES (5414, 5414, 391, 'Kembayan');
INSERT INTO `subdistricts` VALUES (5415, 5415, 391, 'Meliau');
INSERT INTO `subdistricts` VALUES (5416, 5416, 391, 'Mukok');
INSERT INTO `subdistricts` VALUES (5417, 5417, 391, 'Noyan');
INSERT INTO `subdistricts` VALUES (5418, 5418, 391, 'Parindu');
INSERT INTO `subdistricts` VALUES (5419, 5419, 391, 'Sekayam');
INSERT INTO `subdistricts` VALUES (5420, 5420, 391, 'Tayan Hilir');
INSERT INTO `subdistricts` VALUES (5421, 5421, 391, 'Tayan Hulu');
INSERT INTO `subdistricts` VALUES (5422, 5422, 391, 'Toba');
INSERT INTO `subdistricts` VALUES (5423, 5423, 392, 'Apawer Hulu');
INSERT INTO `subdistricts` VALUES (5424, 5424, 392, 'Bonggo');
INSERT INTO `subdistricts` VALUES (5425, 5425, 392, 'Bonggo Timur');
INSERT INTO `subdistricts` VALUES (5426, 5426, 392, 'Pantai Barat');
INSERT INTO `subdistricts` VALUES (5427, 5427, 392, 'Pantai Timur');
INSERT INTO `subdistricts` VALUES (5428, 5428, 392, 'Pantai Timur Barat');
INSERT INTO `subdistricts` VALUES (5429, 5429, 392, 'Sarmi');
INSERT INTO `subdistricts` VALUES (5430, 5430, 392, 'Sarmi Selatan');
INSERT INTO `subdistricts` VALUES (5431, 5431, 392, 'Sarmi Timur');
INSERT INTO `subdistricts` VALUES (5432, 5432, 392, 'Tor Atas');
INSERT INTO `subdistricts` VALUES (5433, 5433, 393, 'Air Hitam');
INSERT INTO `subdistricts` VALUES (5434, 5434, 393, 'Batang Asai');
INSERT INTO `subdistricts` VALUES (5435, 5435, 393, 'Bathin VIII (Batin VIII)');
INSERT INTO `subdistricts` VALUES (5436, 5436, 393, 'Cermin Nan Gadang');
INSERT INTO `subdistricts` VALUES (5437, 5437, 393, 'Limun');
INSERT INTO `subdistricts` VALUES (5438, 5438, 393, 'Mandiangin');
INSERT INTO `subdistricts` VALUES (5439, 5439, 393, 'Pauh');
INSERT INTO `subdistricts` VALUES (5440, 5440, 393, 'Pelawan');
INSERT INTO `subdistricts` VALUES (5441, 5441, 393, 'Sarolangun');
INSERT INTO `subdistricts` VALUES (5442, 5442, 393, 'Singkut');
INSERT INTO `subdistricts` VALUES (5443, 5443, 394, 'Barangin');
INSERT INTO `subdistricts` VALUES (5444, 5444, 394, 'Lembah Segar');
INSERT INTO `subdistricts` VALUES (5445, 5445, 394, 'Silungkang');
INSERT INTO `subdistricts` VALUES (5446, 5446, 394, 'Talawi');
INSERT INTO `subdistricts` VALUES (5447, 5447, 395, 'Belitang');
INSERT INTO `subdistricts` VALUES (5448, 5448, 395, 'Belitang Hilir');
INSERT INTO `subdistricts` VALUES (5449, 5449, 395, 'Belitang Hulu');
INSERT INTO `subdistricts` VALUES (5450, 5450, 395, 'Nanga Mahap');
INSERT INTO `subdistricts` VALUES (5451, 5451, 395, 'Nanga Taman');
INSERT INTO `subdistricts` VALUES (5452, 5452, 395, 'Sekadau Hilir');
INSERT INTO `subdistricts` VALUES (5453, 5453, 395, 'Sekadau Hulu');
INSERT INTO `subdistricts` VALUES (5454, 5454, 396, 'Benteng');
INSERT INTO `subdistricts` VALUES (5455, 5455, 396, 'Bontoharu');
INSERT INTO `subdistricts` VALUES (5456, 5456, 396, 'Bontomanai');
INSERT INTO `subdistricts` VALUES (5457, 5457, 396, 'Bontomatene');
INSERT INTO `subdistricts` VALUES (5458, 5458, 396, 'Bontosikuyu');
INSERT INTO `subdistricts` VALUES (5459, 5459, 396, 'Buki');
INSERT INTO `subdistricts` VALUES (5460, 5460, 396, 'Pasilambena');
INSERT INTO `subdistricts` VALUES (5461, 5461, 396, 'Pasimarannu');
INSERT INTO `subdistricts` VALUES (5462, 5462, 396, 'Pasimassunggu');
INSERT INTO `subdistricts` VALUES (5463, 5463, 396, 'Pasimasunggu Timur');
INSERT INTO `subdistricts` VALUES (5464, 5464, 396, 'Takabonerate');
INSERT INTO `subdistricts` VALUES (5465, 5465, 397, 'Air Periukan');
INSERT INTO `subdistricts` VALUES (5466, 5466, 397, 'Ilir Talo');
INSERT INTO `subdistricts` VALUES (5467, 5467, 397, 'Lubuk Sandi');
INSERT INTO `subdistricts` VALUES (5468, 5468, 397, 'Seluma');
INSERT INTO `subdistricts` VALUES (5469, 5469, 397, 'Seluma Barat');
INSERT INTO `subdistricts` VALUES (5470, 5470, 397, 'Seluma Selatan');
INSERT INTO `subdistricts` VALUES (5471, 5471, 397, 'Seluma Timur');
INSERT INTO `subdistricts` VALUES (5472, 5472, 397, 'Seluma Utara');
INSERT INTO `subdistricts` VALUES (5473, 5473, 397, 'Semidang Alas');
INSERT INTO `subdistricts` VALUES (5474, 5474, 397, 'Semidang Alas Maras');
INSERT INTO `subdistricts` VALUES (5475, 5475, 397, 'Sukaraja');
INSERT INTO `subdistricts` VALUES (5476, 5476, 397, 'Talo');
INSERT INTO `subdistricts` VALUES (5477, 5477, 397, 'Talo Kecil');
INSERT INTO `subdistricts` VALUES (5478, 5478, 397, 'Ulu Talo');
INSERT INTO `subdistricts` VALUES (5479, 5479, 398, 'Ambarawa');
INSERT INTO `subdistricts` VALUES (5480, 5480, 398, 'Bancak');
INSERT INTO `subdistricts` VALUES (5481, 5481, 398, 'Bandungan');
INSERT INTO `subdistricts` VALUES (5482, 5482, 398, 'Banyubiru');
INSERT INTO `subdistricts` VALUES (5483, 5483, 398, 'Bawen');
INSERT INTO `subdistricts` VALUES (5484, 5484, 398, 'Bergas');
INSERT INTO `subdistricts` VALUES (5485, 5485, 398, 'Bringin');
INSERT INTO `subdistricts` VALUES (5486, 5486, 398, 'Getasan');
INSERT INTO `subdistricts` VALUES (5487, 5487, 398, 'Jambu');
INSERT INTO `subdistricts` VALUES (5488, 5488, 398, 'Kaliwungu');
INSERT INTO `subdistricts` VALUES (5489, 5489, 398, 'Pabelan');
INSERT INTO `subdistricts` VALUES (5490, 5490, 398, 'Pringapus');
INSERT INTO `subdistricts` VALUES (5491, 5491, 398, 'Sumowono');
INSERT INTO `subdistricts` VALUES (5492, 5492, 398, 'Suruh');
INSERT INTO `subdistricts` VALUES (5493, 5493, 398, 'Susukan');
INSERT INTO `subdistricts` VALUES (5494, 5494, 398, 'Tengaran');
INSERT INTO `subdistricts` VALUES (5495, 5495, 398, 'Tuntang');
INSERT INTO `subdistricts` VALUES (5496, 5496, 398, 'Ungaran Barat');
INSERT INTO `subdistricts` VALUES (5497, 5497, 398, 'Ungaran Timur');
INSERT INTO `subdistricts` VALUES (5498, 5498, 399, 'Banyumanik');
INSERT INTO `subdistricts` VALUES (5499, 5499, 399, 'Candisari');
INSERT INTO `subdistricts` VALUES (5500, 5500, 399, 'Gajah Mungkur');
INSERT INTO `subdistricts` VALUES (5501, 5501, 399, 'Gayamsari');
INSERT INTO `subdistricts` VALUES (5502, 5502, 399, 'Genuk');
INSERT INTO `subdistricts` VALUES (5503, 5503, 399, 'Gunungpati');
INSERT INTO `subdistricts` VALUES (5504, 5504, 399, 'Mijen');
INSERT INTO `subdistricts` VALUES (5505, 5505, 399, 'Ngaliyan');
INSERT INTO `subdistricts` VALUES (5506, 5506, 399, 'Pedurungan');
INSERT INTO `subdistricts` VALUES (5507, 5507, 399, 'Semarang Barat');
INSERT INTO `subdistricts` VALUES (5508, 5508, 399, 'Semarang Selatan');
INSERT INTO `subdistricts` VALUES (5509, 5509, 399, 'Semarang Tengah');
INSERT INTO `subdistricts` VALUES (5510, 5510, 399, 'Semarang Timur');
INSERT INTO `subdistricts` VALUES (5511, 5511, 399, 'Semarang Utara');
INSERT INTO `subdistricts` VALUES (5512, 5512, 399, 'Tembalang');
INSERT INTO `subdistricts` VALUES (5513, 5513, 399, 'Tugu');
INSERT INTO `subdistricts` VALUES (5514, 5514, 400, 'Amalatu');
INSERT INTO `subdistricts` VALUES (5515, 5515, 400, 'Elpaputih');
INSERT INTO `subdistricts` VALUES (5516, 5516, 400, 'Huamual');
INSERT INTO `subdistricts` VALUES (5517, 5517, 400, 'Huamual Belakang (Waisala)');
INSERT INTO `subdistricts` VALUES (5518, 5518, 400, 'Inamosol');
INSERT INTO `subdistricts` VALUES (5519, 5519, 400, 'Kairatu');
INSERT INTO `subdistricts` VALUES (5520, 5520, 400, 'Kairatu Barat');
INSERT INTO `subdistricts` VALUES (5521, 5521, 400, 'Kepulauan Manipa');
INSERT INTO `subdistricts` VALUES (5522, 5522, 400, 'Seram Barat');
INSERT INTO `subdistricts` VALUES (5523, 5523, 400, 'Taniwel');
INSERT INTO `subdistricts` VALUES (5524, 5524, 400, 'Taniwel Timur');
INSERT INTO `subdistricts` VALUES (5525, 5525, 401, 'Bula');
INSERT INTO `subdistricts` VALUES (5526, 5526, 401, 'Bula Barat');
INSERT INTO `subdistricts` VALUES (5527, 5527, 401, 'Gorom Timur');
INSERT INTO `subdistricts` VALUES (5528, 5528, 401, 'Kian Darat');
INSERT INTO `subdistricts` VALUES (5529, 5529, 401, 'Kilmury');
INSERT INTO `subdistricts` VALUES (5530, 5530, 401, 'Pulau Gorong (Gorom)');
INSERT INTO `subdistricts` VALUES (5531, 5531, 401, 'Pulau Panjang');
INSERT INTO `subdistricts` VALUES (5532, 5532, 401, 'Seram Timur');
INSERT INTO `subdistricts` VALUES (5533, 5533, 401, 'Siritaun Wida Timur');
INSERT INTO `subdistricts` VALUES (5534, 5534, 401, 'Siwalalat');
INSERT INTO `subdistricts` VALUES (5535, 5535, 401, 'Teluk Waru');
INSERT INTO `subdistricts` VALUES (5536, 5536, 401, 'Teor');
INSERT INTO `subdistricts` VALUES (5537, 5537, 401, 'Tutuk Tolu');
INSERT INTO `subdistricts` VALUES (5538, 5538, 401, 'Wakate');
INSERT INTO `subdistricts` VALUES (5539, 5539, 401, 'Werinama');
INSERT INTO `subdistricts` VALUES (5540, 5540, 402, 'Anyar');
INSERT INTO `subdistricts` VALUES (5541, 5541, 402, 'Bandung');
INSERT INTO `subdistricts` VALUES (5542, 5542, 402, 'Baros');
INSERT INTO `subdistricts` VALUES (5543, 5543, 402, 'Binuang');
INSERT INTO `subdistricts` VALUES (5544, 5544, 402, 'Bojonegara');
INSERT INTO `subdistricts` VALUES (5545, 5545, 402, 'Carenang (Cerenang)');
INSERT INTO `subdistricts` VALUES (5546, 5546, 402, 'Cikande');
INSERT INTO `subdistricts` VALUES (5547, 5547, 402, 'Cikeusal');
INSERT INTO `subdistricts` VALUES (5548, 5548, 402, 'Cinangka');
INSERT INTO `subdistricts` VALUES (5549, 5549, 402, 'Ciomas');
INSERT INTO `subdistricts` VALUES (5550, 5550, 402, 'Ciruas');
INSERT INTO `subdistricts` VALUES (5551, 5551, 402, 'Gunungsari');
INSERT INTO `subdistricts` VALUES (5552, 5552, 402, 'Jawilan');
INSERT INTO `subdistricts` VALUES (5553, 5553, 402, 'Kibin');
INSERT INTO `subdistricts` VALUES (5554, 5554, 402, 'Kopo');
INSERT INTO `subdistricts` VALUES (5555, 5555, 402, 'Kragilan');
INSERT INTO `subdistricts` VALUES (5556, 5556, 402, 'Kramatwatu');
INSERT INTO `subdistricts` VALUES (5557, 5557, 402, 'Lebak Wangi');
INSERT INTO `subdistricts` VALUES (5558, 5558, 402, 'Mancak');
INSERT INTO `subdistricts` VALUES (5559, 5559, 402, 'Pabuaran');
INSERT INTO `subdistricts` VALUES (5560, 5560, 402, 'Padarincang');
INSERT INTO `subdistricts` VALUES (5561, 5561, 402, 'Pamarayan');
INSERT INTO `subdistricts` VALUES (5562, 5562, 402, 'Petir');
INSERT INTO `subdistricts` VALUES (5563, 5563, 402, 'Pontang');
INSERT INTO `subdistricts` VALUES (5564, 5564, 402, 'Pulo Ampel');
INSERT INTO `subdistricts` VALUES (5565, 5565, 402, 'Tanara');
INSERT INTO `subdistricts` VALUES (5566, 5566, 402, 'Tirtayasa');
INSERT INTO `subdistricts` VALUES (5567, 5567, 402, 'Tunjung Teja');
INSERT INTO `subdistricts` VALUES (5568, 5568, 402, 'Waringin Kurung');
INSERT INTO `subdistricts` VALUES (5569, 5569, 403, 'Cipocok Jaya');
INSERT INTO `subdistricts` VALUES (5570, 5570, 403, 'Curug');
INSERT INTO `subdistricts` VALUES (5571, 5571, 403, 'Kasemen');
INSERT INTO `subdistricts` VALUES (5572, 5572, 403, 'Serang');
INSERT INTO `subdistricts` VALUES (5573, 5573, 403, 'Taktakan');
INSERT INTO `subdistricts` VALUES (5574, 5574, 403, 'Walantaka');
INSERT INTO `subdistricts` VALUES (5575, 5575, 404, 'Bandar Khalifah');
INSERT INTO `subdistricts` VALUES (5576, 5576, 404, 'Bintang Bayu');
INSERT INTO `subdistricts` VALUES (5577, 5577, 404, 'Dolok Masihul');
INSERT INTO `subdistricts` VALUES (5578, 5578, 404, 'Dolok Merawan');
INSERT INTO `subdistricts` VALUES (5579, 5579, 404, 'Kotarih');
INSERT INTO `subdistricts` VALUES (5580, 5580, 404, 'Pantai Cermin');
INSERT INTO `subdistricts` VALUES (5581, 5581, 404, 'Pegajahan');
INSERT INTO `subdistricts` VALUES (5582, 5582, 404, 'Perbaungan');
INSERT INTO `subdistricts` VALUES (5583, 5583, 404, 'Sei Bamban');
INSERT INTO `subdistricts` VALUES (5584, 5584, 404, 'Sei Rampah');
INSERT INTO `subdistricts` VALUES (5585, 5585, 404, 'Serba Jadi');
INSERT INTO `subdistricts` VALUES (5586, 5586, 404, 'Silinda');
INSERT INTO `subdistricts` VALUES (5587, 5587, 404, 'Sipispis');
INSERT INTO `subdistricts` VALUES (5588, 5588, 404, 'Tanjung Beringin');
INSERT INTO `subdistricts` VALUES (5589, 5589, 404, 'Tebing Syahbandar');
INSERT INTO `subdistricts` VALUES (5590, 5590, 404, 'Tebing Tinggi');
INSERT INTO `subdistricts` VALUES (5591, 5591, 404, 'Teluk Mengkudu');
INSERT INTO `subdistricts` VALUES (5592, 5592, 405, 'Batu Ampar');
INSERT INTO `subdistricts` VALUES (5593, 5593, 405, 'Danau Seluluk');
INSERT INTO `subdistricts` VALUES (5594, 5594, 405, 'Danau Sembuluh');
INSERT INTO `subdistricts` VALUES (5595, 5595, 405, 'Hanau');
INSERT INTO `subdistricts` VALUES (5596, 5596, 405, 'Seruyan Hilir');
INSERT INTO `subdistricts` VALUES (5597, 5597, 405, 'Seruyan Hilir Timur');
INSERT INTO `subdistricts` VALUES (5598, 5598, 405, 'Seruyan Hulu');
INSERT INTO `subdistricts` VALUES (5599, 5599, 405, 'Seruyan Raya');
INSERT INTO `subdistricts` VALUES (5600, 5600, 405, 'Seruyan Tengah');
INSERT INTO `subdistricts` VALUES (5601, 5601, 405, 'Suling Tambun');
INSERT INTO `subdistricts` VALUES (5602, 5602, 406, 'Bunga Raya');
INSERT INTO `subdistricts` VALUES (5603, 5603, 406, 'Dayun');
INSERT INTO `subdistricts` VALUES (5604, 5604, 406, 'Kandis');
INSERT INTO `subdistricts` VALUES (5605, 5605, 406, 'Kerinci Kanan');
INSERT INTO `subdistricts` VALUES (5606, 5606, 406, 'Koto Gasib');
INSERT INTO `subdistricts` VALUES (5607, 5607, 406, 'Lubuk Dalam');
INSERT INTO `subdistricts` VALUES (5608, 5608, 406, 'Mempura');
INSERT INTO `subdistricts` VALUES (5609, 5609, 406, 'Minas');
INSERT INTO `subdistricts` VALUES (5610, 5610, 406, 'Pusako');
INSERT INTO `subdistricts` VALUES (5611, 5611, 406, 'Sabak Auh');
INSERT INTO `subdistricts` VALUES (5612, 5612, 406, 'Siak');
INSERT INTO `subdistricts` VALUES (5613, 5613, 406, 'Sungai Apit');
INSERT INTO `subdistricts` VALUES (5614, 5614, 406, 'Sungai Mandau');
INSERT INTO `subdistricts` VALUES (5615, 5615, 406, 'Tualang');
INSERT INTO `subdistricts` VALUES (5616, 5616, 407, 'Sibolga Kota');
INSERT INTO `subdistricts` VALUES (5617, 5617, 407, 'Sibolga Sambas');
INSERT INTO `subdistricts` VALUES (5618, 5618, 407, 'Sibolga Selatan');
INSERT INTO `subdistricts` VALUES (5619, 5619, 407, 'Sibolga Utara');
INSERT INTO `subdistricts` VALUES (5620, 5620, 408, 'Baranti');
INSERT INTO `subdistricts` VALUES (5621, 5621, 408, 'Dua Pitue');
INSERT INTO `subdistricts` VALUES (5622, 5622, 408, 'Kulo');
INSERT INTO `subdistricts` VALUES (5623, 5623, 408, 'Maritengngae');
INSERT INTO `subdistricts` VALUES (5624, 5624, 408, 'Panca Lautan (Lautang)');
INSERT INTO `subdistricts` VALUES (5625, 5625, 408, 'Panca Rijang');
INSERT INTO `subdistricts` VALUES (5626, 5626, 408, 'Pitu Raise/Riase');
INSERT INTO `subdistricts` VALUES (5627, 5627, 408, 'Pitu Riawa');
INSERT INTO `subdistricts` VALUES (5628, 5628, 408, 'Tellu Limpoe');
INSERT INTO `subdistricts` VALUES (5629, 5629, 408, 'Watang Pulu');
INSERT INTO `subdistricts` VALUES (5630, 5630, 408, 'Wattang Sidenreng (Watang Sidenreng)');
INSERT INTO `subdistricts` VALUES (5631, 5631, 409, 'Balongbendo');
INSERT INTO `subdistricts` VALUES (5632, 5632, 409, 'Buduran');
INSERT INTO `subdistricts` VALUES (5633, 5633, 409, 'Candi');
INSERT INTO `subdistricts` VALUES (5634, 5634, 409, 'Gedangan');
INSERT INTO `subdistricts` VALUES (5635, 5635, 409, 'Jabon');
INSERT INTO `subdistricts` VALUES (5636, 5636, 409, 'Krembung');
INSERT INTO `subdistricts` VALUES (5637, 5637, 409, 'Krian');
INSERT INTO `subdistricts` VALUES (5638, 5638, 409, 'Porong');
INSERT INTO `subdistricts` VALUES (5639, 5639, 409, 'Prambon');
INSERT INTO `subdistricts` VALUES (5640, 5640, 409, 'Sedati');
INSERT INTO `subdistricts` VALUES (5641, 5641, 409, 'Sidoarjo');
INSERT INTO `subdistricts` VALUES (5642, 5642, 409, 'Sukodono');
INSERT INTO `subdistricts` VALUES (5643, 5643, 409, 'Taman');
INSERT INTO `subdistricts` VALUES (5644, 5644, 409, 'Tanggulangin');
INSERT INTO `subdistricts` VALUES (5645, 5645, 409, 'Tarik');
INSERT INTO `subdistricts` VALUES (5646, 5646, 409, 'Tulangan');
INSERT INTO `subdistricts` VALUES (5647, 5647, 409, 'Waru');
INSERT INTO `subdistricts` VALUES (5648, 5648, 409, 'Wonoayu');
INSERT INTO `subdistricts` VALUES (5649, 5649, 410, 'Dolo');
INSERT INTO `subdistricts` VALUES (5650, 5650, 410, 'Dolo Barat');
INSERT INTO `subdistricts` VALUES (5651, 5651, 410, 'Dolo Selatan');
INSERT INTO `subdistricts` VALUES (5652, 5652, 410, 'Gumbasa');
INSERT INTO `subdistricts` VALUES (5653, 5653, 410, 'Kinovaru');
INSERT INTO `subdistricts` VALUES (5654, 5654, 410, 'Kulawi');
INSERT INTO `subdistricts` VALUES (5655, 5655, 410, 'Kulawi Selatan');
INSERT INTO `subdistricts` VALUES (5656, 5656, 410, 'Lindu');
INSERT INTO `subdistricts` VALUES (5657, 5657, 410, 'Marawola');
INSERT INTO `subdistricts` VALUES (5658, 5658, 410, 'Marawola Barat');
INSERT INTO `subdistricts` VALUES (5659, 5659, 410, 'Nokilalaki');
INSERT INTO `subdistricts` VALUES (5660, 5660, 410, 'Palolo');
INSERT INTO `subdistricts` VALUES (5661, 5661, 410, 'Pipikoro');
INSERT INTO `subdistricts` VALUES (5662, 5662, 410, 'Sigi Biromaru');
INSERT INTO `subdistricts` VALUES (5663, 5663, 410, 'Tanambulava');
INSERT INTO `subdistricts` VALUES (5664, 5664, 411, 'IV Nagari');
INSERT INTO `subdistricts` VALUES (5665, 5665, 411, 'Kamang Baru');
INSERT INTO `subdistricts` VALUES (5666, 5666, 411, 'Koto VII');
INSERT INTO `subdistricts` VALUES (5667, 5667, 411, 'Kupitan');
INSERT INTO `subdistricts` VALUES (5668, 5668, 411, 'Lubuak Tarok');
INSERT INTO `subdistricts` VALUES (5669, 5669, 411, 'Sijunjung');
INSERT INTO `subdistricts` VALUES (5670, 5670, 411, 'Sumpur Kudus');
INSERT INTO `subdistricts` VALUES (5671, 5671, 411, 'Tanjung Gadang');
INSERT INTO `subdistricts` VALUES (5672, 5672, 412, 'Alok');
INSERT INTO `subdistricts` VALUES (5673, 5673, 412, 'Alok Barat');
INSERT INTO `subdistricts` VALUES (5674, 5674, 412, 'Alok Timur');
INSERT INTO `subdistricts` VALUES (5675, 5675, 412, 'Bola');
INSERT INTO `subdistricts` VALUES (5676, 5676, 412, 'Doreng');
INSERT INTO `subdistricts` VALUES (5677, 5677, 412, 'Hewokloang');
INSERT INTO `subdistricts` VALUES (5678, 5678, 412, 'Kangae');
INSERT INTO `subdistricts` VALUES (5679, 5679, 412, 'Kewapante');
INSERT INTO `subdistricts` VALUES (5680, 5680, 412, 'Koting');
INSERT INTO `subdistricts` VALUES (5681, 5681, 412, 'Lela');
INSERT INTO `subdistricts` VALUES (5682, 5682, 412, 'Magepanda');
INSERT INTO `subdistricts` VALUES (5683, 5683, 412, 'Mapitara');
INSERT INTO `subdistricts` VALUES (5684, 5684, 412, 'Mego');
INSERT INTO `subdistricts` VALUES (5685, 5685, 412, 'Nelle (Maumerei)');
INSERT INTO `subdistricts` VALUES (5686, 5686, 412, 'Nita');
INSERT INTO `subdistricts` VALUES (5687, 5687, 412, 'Paga');
INSERT INTO `subdistricts` VALUES (5688, 5688, 412, 'Palue');
INSERT INTO `subdistricts` VALUES (5689, 5689, 412, 'Talibura');
INSERT INTO `subdistricts` VALUES (5690, 5690, 412, 'Tana Wawo');
INSERT INTO `subdistricts` VALUES (5691, 5691, 412, 'Waiblama');
INSERT INTO `subdistricts` VALUES (5692, 5692, 412, 'Waigete');
INSERT INTO `subdistricts` VALUES (5693, 5693, 413, 'Bandar');
INSERT INTO `subdistricts` VALUES (5694, 5694, 413, 'Bandar Huluan');
INSERT INTO `subdistricts` VALUES (5695, 5695, 413, 'Bandar Masilam');
INSERT INTO `subdistricts` VALUES (5696, 5696, 413, 'Bosar Maligas');
INSERT INTO `subdistricts` VALUES (5697, 5697, 413, 'Dolok Batu Nanggar');
INSERT INTO `subdistricts` VALUES (5698, 5698, 413, 'Dolok Panribuan');
INSERT INTO `subdistricts` VALUES (5699, 5699, 413, 'Dolok Pardamean');
INSERT INTO `subdistricts` VALUES (5700, 5700, 413, 'Dolok Silau');
INSERT INTO `subdistricts` VALUES (5701, 5701, 413, 'Girsang Sipangan Bolon');
INSERT INTO `subdistricts` VALUES (5702, 5702, 413, 'Gunung Malela');
INSERT INTO `subdistricts` VALUES (5703, 5703, 413, 'Gunung Maligas');
INSERT INTO `subdistricts` VALUES (5704, 5704, 413, 'Haranggaol Horison');
INSERT INTO `subdistricts` VALUES (5705, 5705, 413, 'Hatonduhan');
INSERT INTO `subdistricts` VALUES (5706, 5706, 413, 'Huta Bayu Raja');
INSERT INTO `subdistricts` VALUES (5707, 5707, 413, 'Jawa Maraja Bah Jambi');
INSERT INTO `subdistricts` VALUES (5708, 5708, 413, 'Jorlang Hataran');
INSERT INTO `subdistricts` VALUES (5709, 5709, 413, 'Panei');
INSERT INTO `subdistricts` VALUES (5710, 5710, 413, 'Panombeian Panei');
INSERT INTO `subdistricts` VALUES (5711, 5711, 413, 'Pematang Bandar');
INSERT INTO `subdistricts` VALUES (5712, 5712, 413, 'Pematang Sidamanik');
INSERT INTO `subdistricts` VALUES (5713, 5713, 413, 'Pematang Silima Huta');
INSERT INTO `subdistricts` VALUES (5714, 5714, 413, 'Purba');
INSERT INTO `subdistricts` VALUES (5715, 5715, 413, 'Raya');
INSERT INTO `subdistricts` VALUES (5716, 5716, 413, 'Raya Kahean');
INSERT INTO `subdistricts` VALUES (5717, 5717, 413, 'Siantar');
INSERT INTO `subdistricts` VALUES (5718, 5718, 413, 'Sidamanik');
INSERT INTO `subdistricts` VALUES (5719, 5719, 413, 'Silimakuta');
INSERT INTO `subdistricts` VALUES (5720, 5720, 413, 'Silou Kahean');
INSERT INTO `subdistricts` VALUES (5721, 5721, 413, 'Tanah Jawa');
INSERT INTO `subdistricts` VALUES (5722, 5722, 413, 'Tapian Dolok');
INSERT INTO `subdistricts` VALUES (5723, 5723, 413, 'Ujung Padang');
INSERT INTO `subdistricts` VALUES (5724, 5724, 414, 'Alapan (Alafan)');
INSERT INTO `subdistricts` VALUES (5725, 5725, 414, 'Salang');
INSERT INTO `subdistricts` VALUES (5726, 5726, 414, 'Simeuleu Barat');
INSERT INTO `subdistricts` VALUES (5727, 5727, 414, 'Simeuleu Tengah');
INSERT INTO `subdistricts` VALUES (5728, 5728, 414, 'Simeuleu Timur');
INSERT INTO `subdistricts` VALUES (5729, 5729, 414, 'Simeulue Cut');
INSERT INTO `subdistricts` VALUES (5730, 5730, 414, 'Teluk Dalam');
INSERT INTO `subdistricts` VALUES (5731, 5731, 414, 'Teupah Barat');
INSERT INTO `subdistricts` VALUES (5732, 5732, 414, 'Teupah Selatan');
INSERT INTO `subdistricts` VALUES (5733, 5733, 414, 'Teupah Tengah');
INSERT INTO `subdistricts` VALUES (5734, 5734, 415, 'Singkawang Barat');
INSERT INTO `subdistricts` VALUES (5735, 5735, 415, 'Singkawang Selatan');
INSERT INTO `subdistricts` VALUES (5736, 5736, 415, 'Singkawang Tengah');
INSERT INTO `subdistricts` VALUES (5737, 5737, 415, 'Singkawang Timur');
INSERT INTO `subdistricts` VALUES (5738, 5738, 415, 'Singkawang Utara');
INSERT INTO `subdistricts` VALUES (5739, 5739, 416, 'Bulupoddo');
INSERT INTO `subdistricts` VALUES (5740, 5740, 416, 'Pulau Sembilan');
INSERT INTO `subdistricts` VALUES (5741, 5741, 416, 'Sinjai Barat');
INSERT INTO `subdistricts` VALUES (5742, 5742, 416, 'Sinjai Borong');
INSERT INTO `subdistricts` VALUES (5743, 5743, 416, 'Sinjai Selatan');
INSERT INTO `subdistricts` VALUES (5744, 5744, 416, 'Sinjai Tengah');
INSERT INTO `subdistricts` VALUES (5745, 5745, 416, 'Sinjai Timur');
INSERT INTO `subdistricts` VALUES (5746, 5746, 416, 'Sinjai Utara');
INSERT INTO `subdistricts` VALUES (5747, 5747, 416, 'Tellu Limpoe');
INSERT INTO `subdistricts` VALUES (5748, 5748, 417, 'Ambalau');
INSERT INTO `subdistricts` VALUES (5749, 5749, 417, 'Binjai Hulu');
INSERT INTO `subdistricts` VALUES (5750, 5750, 417, 'Dedai');
INSERT INTO `subdistricts` VALUES (5751, 5751, 417, 'Kayan Hilir');
INSERT INTO `subdistricts` VALUES (5752, 5752, 417, 'Kayan Hulu');
INSERT INTO `subdistricts` VALUES (5753, 5753, 417, 'Kelam Permai');
INSERT INTO `subdistricts` VALUES (5754, 5754, 417, 'Ketungau Hilir');
INSERT INTO `subdistricts` VALUES (5755, 5755, 417, 'Ketungau Hulu');
INSERT INTO `subdistricts` VALUES (5756, 5756, 417, 'Ketungau Tengah');
INSERT INTO `subdistricts` VALUES (5757, 5757, 417, 'Sepauk');
INSERT INTO `subdistricts` VALUES (5758, 5758, 417, 'Serawai (Nanga Serawai)');
INSERT INTO `subdistricts` VALUES (5759, 5759, 417, 'Sintang');
INSERT INTO `subdistricts` VALUES (5760, 5760, 417, 'Sungai Tebelian');
INSERT INTO `subdistricts` VALUES (5761, 5761, 417, 'Tempunak');
INSERT INTO `subdistricts` VALUES (5762, 5762, 418, 'Arjasa');
INSERT INTO `subdistricts` VALUES (5763, 5763, 418, 'Asembagus');
INSERT INTO `subdistricts` VALUES (5764, 5764, 418, 'Banyuglugur');
INSERT INTO `subdistricts` VALUES (5765, 5765, 418, 'Banyuputih');
INSERT INTO `subdistricts` VALUES (5766, 5766, 418, 'Besuki');
INSERT INTO `subdistricts` VALUES (5767, 5767, 418, 'Bungatan');
INSERT INTO `subdistricts` VALUES (5768, 5768, 418, 'Jangkar');
INSERT INTO `subdistricts` VALUES (5769, 5769, 418, 'Jatibanteng');
INSERT INTO `subdistricts` VALUES (5770, 5770, 418, 'Kapongan');
INSERT INTO `subdistricts` VALUES (5771, 5771, 418, 'Kendit');
INSERT INTO `subdistricts` VALUES (5772, 5772, 418, 'Mangaran');
INSERT INTO `subdistricts` VALUES (5773, 5773, 418, 'Mlandingan');
INSERT INTO `subdistricts` VALUES (5774, 5774, 418, 'Panarukan');
INSERT INTO `subdistricts` VALUES (5775, 5775, 418, 'Panji');
INSERT INTO `subdistricts` VALUES (5776, 5776, 418, 'Situbondo');
INSERT INTO `subdistricts` VALUES (5777, 5777, 418, 'Suboh');
INSERT INTO `subdistricts` VALUES (5778, 5778, 418, 'Sumbermalang');
INSERT INTO `subdistricts` VALUES (5779, 5779, 419, 'Berbah');
INSERT INTO `subdistricts` VALUES (5780, 5780, 419, 'Cangkringan');
INSERT INTO `subdistricts` VALUES (5781, 5781, 419, 'Depok');
INSERT INTO `subdistricts` VALUES (5782, 5782, 419, 'Gamping');
INSERT INTO `subdistricts` VALUES (5783, 5783, 419, 'Godean');
INSERT INTO `subdistricts` VALUES (5784, 5784, 419, 'Kalasan');
INSERT INTO `subdistricts` VALUES (5785, 5785, 419, 'Minggir');
INSERT INTO `subdistricts` VALUES (5786, 5786, 419, 'Mlati');
INSERT INTO `subdistricts` VALUES (5787, 5787, 419, 'Moyudan');
INSERT INTO `subdistricts` VALUES (5788, 5788, 419, 'Ngaglik');
INSERT INTO `subdistricts` VALUES (5789, 5789, 419, 'Ngemplak');
INSERT INTO `subdistricts` VALUES (5790, 5790, 419, 'Pakem');
INSERT INTO `subdistricts` VALUES (5791, 5791, 419, 'Prambanan');
INSERT INTO `subdistricts` VALUES (5792, 5792, 419, 'Seyegan');
INSERT INTO `subdistricts` VALUES (5793, 5793, 419, 'Sleman');
INSERT INTO `subdistricts` VALUES (5794, 5794, 419, 'Tempel');
INSERT INTO `subdistricts` VALUES (5795, 5795, 419, 'Turi');
INSERT INTO `subdistricts` VALUES (5796, 5796, 420, 'Bukit Sundi');
INSERT INTO `subdistricts` VALUES (5797, 5797, 420, 'Danau Kembar');
INSERT INTO `subdistricts` VALUES (5798, 5798, 420, 'Gunung Talang');
INSERT INTO `subdistricts` VALUES (5799, 5799, 420, 'Hiliran Gumanti');
INSERT INTO `subdistricts` VALUES (5800, 5800, 420, 'IX Koto Sei Lasi');
INSERT INTO `subdistricts` VALUES (5801, 5801, 420, 'Junjung Sirih');
INSERT INTO `subdistricts` VALUES (5802, 5802, 420, 'Kubung');
INSERT INTO `subdistricts` VALUES (5803, 5803, 420, 'Lembah Gumanti');
INSERT INTO `subdistricts` VALUES (5804, 5804, 420, 'Lembang Jaya');
INSERT INTO `subdistricts` VALUES (5805, 5805, 420, 'Pantai Cermin');
INSERT INTO `subdistricts` VALUES (5806, 5806, 420, 'Payung Sekaki');
INSERT INTO `subdistricts` VALUES (5807, 5807, 420, 'Tigo Lurah');
INSERT INTO `subdistricts` VALUES (5808, 5808, 420, 'X Koto Diatas');
INSERT INTO `subdistricts` VALUES (5809, 5809, 420, 'X Koto Singkarak');
INSERT INTO `subdistricts` VALUES (5810, 5810, 421, 'Lubuk Sikarah');
INSERT INTO `subdistricts` VALUES (5811, 5811, 421, 'Tanjung Harapan');
INSERT INTO `subdistricts` VALUES (5812, 5812, 422, 'Koto Parik Gadang Diateh');
INSERT INTO `subdistricts` VALUES (5813, 5813, 422, 'Pauh Duo');
INSERT INTO `subdistricts` VALUES (5814, 5814, 422, 'Sangir');
INSERT INTO `subdistricts` VALUES (5815, 5815, 422, 'Sangir Balai Janggo');
INSERT INTO `subdistricts` VALUES (5816, 5816, 422, 'Sangir Batang Hari');
INSERT INTO `subdistricts` VALUES (5817, 5817, 422, 'Sangir Jujuan');
INSERT INTO `subdistricts` VALUES (5818, 5818, 422, 'Sungai Pagu');
INSERT INTO `subdistricts` VALUES (5819, 5819, 423, 'Citta');
INSERT INTO `subdistricts` VALUES (5820, 5820, 423, 'Donri-Donri');
INSERT INTO `subdistricts` VALUES (5821, 5821, 423, 'Ganra');
INSERT INTO `subdistricts` VALUES (5822, 5822, 423, 'Lalabata');
INSERT INTO `subdistricts` VALUES (5823, 5823, 423, 'Lili Rilau');
INSERT INTO `subdistricts` VALUES (5824, 5824, 423, 'Liliraja (Lili Riaja)');
INSERT INTO `subdistricts` VALUES (5825, 5825, 423, 'Mario Riawa');
INSERT INTO `subdistricts` VALUES (5826, 5826, 423, 'Mario Riwawo');
INSERT INTO `subdistricts` VALUES (5827, 5827, 424, 'Aimas');
INSERT INTO `subdistricts` VALUES (5828, 5828, 424, 'Beraur');
INSERT INTO `subdistricts` VALUES (5829, 5829, 424, 'Klabot');
INSERT INTO `subdistricts` VALUES (5830, 5830, 424, 'Klamono');
INSERT INTO `subdistricts` VALUES (5831, 5831, 424, 'Klaso');
INSERT INTO `subdistricts` VALUES (5832, 5832, 424, 'Klawak');
INSERT INTO `subdistricts` VALUES (5833, 5833, 424, 'Klayili');
INSERT INTO `subdistricts` VALUES (5834, 5834, 424, 'Makbon');
INSERT INTO `subdistricts` VALUES (5835, 5835, 424, 'Mariat');
INSERT INTO `subdistricts` VALUES (5836, 5836, 424, 'Maudus');
INSERT INTO `subdistricts` VALUES (5837, 5837, 424, 'Mayamuk');
INSERT INTO `subdistricts` VALUES (5838, 5838, 424, 'Moisegen');
INSERT INTO `subdistricts` VALUES (5839, 5839, 424, 'Salawati');
INSERT INTO `subdistricts` VALUES (5840, 5840, 424, 'Salawati Selatan');
INSERT INTO `subdistricts` VALUES (5841, 5841, 424, 'Sayosa');
INSERT INTO `subdistricts` VALUES (5842, 5842, 424, 'Seget');
INSERT INTO `subdistricts` VALUES (5843, 5843, 424, 'Segun');
INSERT INTO `subdistricts` VALUES (5844, 5844, 425, 'Sorong');
INSERT INTO `subdistricts` VALUES (5845, 5845, 425, 'Sorong Barat');
INSERT INTO `subdistricts` VALUES (5846, 5846, 425, 'Sorong Kepulauan');
INSERT INTO `subdistricts` VALUES (5847, 5847, 425, 'Sorong Manoi');
INSERT INTO `subdistricts` VALUES (5848, 5848, 425, 'Sorong Timur');
INSERT INTO `subdistricts` VALUES (5849, 5849, 425, 'Sorong Utara');
INSERT INTO `subdistricts` VALUES (5850, 5850, 426, 'Fokour');
INSERT INTO `subdistricts` VALUES (5851, 5851, 426, 'Inanwatan (Inawatan)');
INSERT INTO `subdistricts` VALUES (5852, 5852, 426, 'Kais (Matemani Kais)');
INSERT INTO `subdistricts` VALUES (5853, 5853, 426, 'Kokoda');
INSERT INTO `subdistricts` VALUES (5854, 5854, 426, 'Kokoda Utara');
INSERT INTO `subdistricts` VALUES (5855, 5855, 426, 'Konda');
INSERT INTO `subdistricts` VALUES (5856, 5856, 426, 'Matemani');
INSERT INTO `subdistricts` VALUES (5857, 5857, 426, 'Moswaren');
INSERT INTO `subdistricts` VALUES (5858, 5858, 426, 'Saifi');
INSERT INTO `subdistricts` VALUES (5859, 5859, 426, 'Sawiat');
INSERT INTO `subdistricts` VALUES (5860, 5860, 426, 'Seremuk');
INSERT INTO `subdistricts` VALUES (5861, 5861, 426, 'Teminabuan');
INSERT INTO `subdistricts` VALUES (5862, 5862, 426, 'Wayer');
INSERT INTO `subdistricts` VALUES (5863, 5863, 427, 'Gemolong');
INSERT INTO `subdistricts` VALUES (5864, 5864, 427, 'Gesi');
INSERT INTO `subdistricts` VALUES (5865, 5865, 427, 'Gondang');
INSERT INTO `subdistricts` VALUES (5866, 5866, 427, 'Jenar');
INSERT INTO `subdistricts` VALUES (5867, 5867, 427, 'Kalijambe');
INSERT INTO `subdistricts` VALUES (5868, 5868, 427, 'Karangmalang');
INSERT INTO `subdistricts` VALUES (5869, 5869, 427, 'Kedawung');
INSERT INTO `subdistricts` VALUES (5870, 5870, 427, 'Masaran');
INSERT INTO `subdistricts` VALUES (5871, 5871, 427, 'Miri');
INSERT INTO `subdistricts` VALUES (5872, 5872, 427, 'Mondokan');
INSERT INTO `subdistricts` VALUES (5873, 5873, 427, 'Ngrampal');
INSERT INTO `subdistricts` VALUES (5874, 5874, 427, 'Plupuh');
INSERT INTO `subdistricts` VALUES (5875, 5875, 427, 'Sambirejo');
INSERT INTO `subdistricts` VALUES (5876, 5876, 427, 'Sambung Macan');
INSERT INTO `subdistricts` VALUES (5877, 5877, 427, 'Sidoharjo');
INSERT INTO `subdistricts` VALUES (5878, 5878, 427, 'Sragen');
INSERT INTO `subdistricts` VALUES (5879, 5879, 427, 'Sukodono');
INSERT INTO `subdistricts` VALUES (5880, 5880, 427, 'Sumberlawang');
INSERT INTO `subdistricts` VALUES (5881, 5881, 427, 'Tangen');
INSERT INTO `subdistricts` VALUES (5882, 5882, 427, 'Tanon');
INSERT INTO `subdistricts` VALUES (5883, 5883, 428, 'Binong');
INSERT INTO `subdistricts` VALUES (5884, 5884, 428, 'Blanakan');
INSERT INTO `subdistricts` VALUES (5885, 5885, 428, 'Ciasem');
INSERT INTO `subdistricts` VALUES (5886, 5886, 428, 'Ciater');
INSERT INTO `subdistricts` VALUES (5887, 5887, 428, 'Cibogo');
INSERT INTO `subdistricts` VALUES (5888, 5888, 428, 'Cijambe');
INSERT INTO `subdistricts` VALUES (5889, 5889, 428, 'Cikaum');
INSERT INTO `subdistricts` VALUES (5890, 5890, 428, 'Cipeundeuy');
INSERT INTO `subdistricts` VALUES (5891, 5891, 428, 'Cipunagara');
INSERT INTO `subdistricts` VALUES (5892, 5892, 428, 'Cisalak');
INSERT INTO `subdistricts` VALUES (5893, 5893, 428, 'Compreng');
INSERT INTO `subdistricts` VALUES (5894, 5894, 428, 'Dawuan');
INSERT INTO `subdistricts` VALUES (5895, 5895, 428, 'Jalancagak');
INSERT INTO `subdistricts` VALUES (5896, 5896, 428, 'Kalijati');
INSERT INTO `subdistricts` VALUES (5897, 5897, 428, 'Kasomalang');
INSERT INTO `subdistricts` VALUES (5898, 5898, 428, 'Legonkulon');
INSERT INTO `subdistricts` VALUES (5899, 5899, 428, 'Pabuaran');
INSERT INTO `subdistricts` VALUES (5900, 5900, 428, 'Pagaden');
INSERT INTO `subdistricts` VALUES (5901, 5901, 428, 'Pagaden Barat');
INSERT INTO `subdistricts` VALUES (5902, 5902, 428, 'Pamanukan');
INSERT INTO `subdistricts` VALUES (5903, 5903, 428, 'Patokbeusi');
INSERT INTO `subdistricts` VALUES (5904, 5904, 428, 'Purwadadi');
INSERT INTO `subdistricts` VALUES (5905, 5905, 428, 'Pusakajaya');
INSERT INTO `subdistricts` VALUES (5906, 5906, 428, 'Pusakanagara');
INSERT INTO `subdistricts` VALUES (5907, 5907, 428, 'Sagalaherang');
INSERT INTO `subdistricts` VALUES (5908, 5908, 428, 'Serangpanjang');
INSERT INTO `subdistricts` VALUES (5909, 5909, 428, 'Subang');
INSERT INTO `subdistricts` VALUES (5910, 5910, 428, 'Sukasari');
INSERT INTO `subdistricts` VALUES (5911, 5911, 428, 'Tambakdahan');
INSERT INTO `subdistricts` VALUES (5912, 5912, 428, 'Tanjungsiang');
INSERT INTO `subdistricts` VALUES (5913, 5913, 429, 'Longkib');
INSERT INTO `subdistricts` VALUES (5914, 5914, 429, 'Penanggalan');
INSERT INTO `subdistricts` VALUES (5915, 5915, 429, 'Rundeng');
INSERT INTO `subdistricts` VALUES (5916, 5916, 429, 'Simpang Kiri');
INSERT INTO `subdistricts` VALUES (5917, 5917, 429, 'Sultan Daulat');
INSERT INTO `subdistricts` VALUES (5918, 5918, 430, 'Bantargadung');
INSERT INTO `subdistricts` VALUES (5919, 5919, 430, 'Bojong Genteng');
INSERT INTO `subdistricts` VALUES (5920, 5920, 430, 'Caringin');
INSERT INTO `subdistricts` VALUES (5921, 5921, 430, 'Ciambar');
INSERT INTO `subdistricts` VALUES (5922, 5922, 430, 'Cibadak');
INSERT INTO `subdistricts` VALUES (5923, 5923, 430, 'Cibitung');
INSERT INTO `subdistricts` VALUES (5924, 5924, 430, 'Cicantayan');
INSERT INTO `subdistricts` VALUES (5925, 5925, 430, 'Cicurug');
INSERT INTO `subdistricts` VALUES (5926, 5926, 430, 'Cidadap');
INSERT INTO `subdistricts` VALUES (5927, 5927, 430, 'Cidahu');
INSERT INTO `subdistricts` VALUES (5928, 5928, 430, 'Cidolog');
INSERT INTO `subdistricts` VALUES (5929, 5929, 430, 'Ciemas');
INSERT INTO `subdistricts` VALUES (5930, 5930, 430, 'Cikakak');
INSERT INTO `subdistricts` VALUES (5931, 5931, 430, 'Cikembar');
INSERT INTO `subdistricts` VALUES (5932, 5932, 430, 'Cikidang');
INSERT INTO `subdistricts` VALUES (5933, 5933, 430, 'Cimanggu');
INSERT INTO `subdistricts` VALUES (5934, 5934, 430, 'Ciracap');
INSERT INTO `subdistricts` VALUES (5935, 5935, 430, 'Cireunghas');
INSERT INTO `subdistricts` VALUES (5936, 5936, 430, 'Cisaat');
INSERT INTO `subdistricts` VALUES (5937, 5937, 430, 'Cisolok');
INSERT INTO `subdistricts` VALUES (5938, 5938, 430, 'Curugkembar');
INSERT INTO `subdistricts` VALUES (5939, 5939, 430, 'Geger Bitung');
INSERT INTO `subdistricts` VALUES (5940, 5940, 430, 'Gunung Guruh');
INSERT INTO `subdistricts` VALUES (5941, 5941, 430, 'Jampang Kulon');
INSERT INTO `subdistricts` VALUES (5942, 5942, 430, 'Jampang Tengah');
INSERT INTO `subdistricts` VALUES (5943, 5943, 430, 'Kabandungan');
INSERT INTO `subdistricts` VALUES (5944, 5944, 430, 'Kadudampit');
INSERT INTO `subdistricts` VALUES (5945, 5945, 430, 'Kalapa Nunggal');
INSERT INTO `subdistricts` VALUES (5946, 5946, 430, 'Kali Bunder');
INSERT INTO `subdistricts` VALUES (5947, 5947, 430, 'Kebonpedes');
INSERT INTO `subdistricts` VALUES (5948, 5948, 430, 'Lengkong');
INSERT INTO `subdistricts` VALUES (5949, 5949, 430, 'Nagrak');
INSERT INTO `subdistricts` VALUES (5950, 5950, 430, 'Nyalindung');
INSERT INTO `subdistricts` VALUES (5951, 5951, 430, 'Pabuaran');
INSERT INTO `subdistricts` VALUES (5952, 5952, 430, 'Parakan Salak');
INSERT INTO `subdistricts` VALUES (5953, 5953, 430, 'Parung Kuda');
INSERT INTO `subdistricts` VALUES (5954, 5954, 430, 'Pelabuhan/Palabuhan Ratu');
INSERT INTO `subdistricts` VALUES (5955, 5955, 430, 'Purabaya');
INSERT INTO `subdistricts` VALUES (5956, 5956, 430, 'Sagaranten');
INSERT INTO `subdistricts` VALUES (5957, 5957, 430, 'Simpenan');
INSERT INTO `subdistricts` VALUES (5958, 5958, 430, 'Sukabumi');
INSERT INTO `subdistricts` VALUES (5959, 5959, 430, 'Sukalarang');
INSERT INTO `subdistricts` VALUES (5960, 5960, 430, 'Sukaraja');
INSERT INTO `subdistricts` VALUES (5961, 5961, 430, 'Surade');
INSERT INTO `subdistricts` VALUES (5962, 5962, 430, 'Tegal Buleud');
INSERT INTO `subdistricts` VALUES (5963, 5963, 430, 'Waluran');
INSERT INTO `subdistricts` VALUES (5964, 5964, 430, 'Warung Kiara');
INSERT INTO `subdistricts` VALUES (5965, 5965, 431, 'Baros');
INSERT INTO `subdistricts` VALUES (5966, 5966, 431, 'Cibeureum');
INSERT INTO `subdistricts` VALUES (5967, 5967, 431, 'Cikole');
INSERT INTO `subdistricts` VALUES (5968, 5968, 431, 'Citamiang');
INSERT INTO `subdistricts` VALUES (5969, 5969, 431, 'Gunung Puyuh');
INSERT INTO `subdistricts` VALUES (5970, 5970, 431, 'Lembursitu');
INSERT INTO `subdistricts` VALUES (5971, 5971, 431, 'Warudoyong');
INSERT INTO `subdistricts` VALUES (5972, 5972, 432, 'Balai Riam');
INSERT INTO `subdistricts` VALUES (5973, 5973, 432, 'Jelai');
INSERT INTO `subdistricts` VALUES (5974, 5974, 432, 'Pantai Lunci');
INSERT INTO `subdistricts` VALUES (5975, 5975, 432, 'Permata Kecubung');
INSERT INTO `subdistricts` VALUES (5976, 5976, 432, 'Sukamara');
INSERT INTO `subdistricts` VALUES (5977, 5977, 433, 'Baki');
INSERT INTO `subdistricts` VALUES (5978, 5978, 433, 'Bendosari');
INSERT INTO `subdistricts` VALUES (5979, 5979, 433, 'Bulu');
INSERT INTO `subdistricts` VALUES (5980, 5980, 433, 'Gatak');
INSERT INTO `subdistricts` VALUES (5981, 5981, 433, 'Grogol');
INSERT INTO `subdistricts` VALUES (5982, 5982, 433, 'Kartasura');
INSERT INTO `subdistricts` VALUES (5983, 5983, 433, 'Mojolaban');
INSERT INTO `subdistricts` VALUES (5984, 5984, 433, 'Nguter');
INSERT INTO `subdistricts` VALUES (5985, 5985, 433, 'Polokarto');
INSERT INTO `subdistricts` VALUES (5986, 5986, 433, 'Sukoharjo');
INSERT INTO `subdistricts` VALUES (5987, 5987, 433, 'Tawangsari');
INSERT INTO `subdistricts` VALUES (5988, 5988, 433, 'Weru');
INSERT INTO `subdistricts` VALUES (5989, 5989, 434, 'Kota Waikabubak');
INSERT INTO `subdistricts` VALUES (5990, 5990, 434, 'Lamboya');
INSERT INTO `subdistricts` VALUES (5991, 5991, 434, 'Lamboya Barat');
INSERT INTO `subdistricts` VALUES (5992, 5992, 434, 'Loli');
INSERT INTO `subdistricts` VALUES (5993, 5993, 434, 'Tana Righu');
INSERT INTO `subdistricts` VALUES (5994, 5994, 434, 'Wanokaka');
INSERT INTO `subdistricts` VALUES (5995, 5995, 435, 'Kodi');
INSERT INTO `subdistricts` VALUES (5996, 5996, 435, 'Kodi Balaghar');
INSERT INTO `subdistricts` VALUES (5997, 5997, 435, 'Kodi Bangedo');
INSERT INTO `subdistricts` VALUES (5998, 5998, 435, 'Kodi Utara');
INSERT INTO `subdistricts` VALUES (5999, 5999, 435, 'Kota Tambolaka');
INSERT INTO `subdistricts` VALUES (6000, 6000, 435, 'Loura (Laura)');
INSERT INTO `subdistricts` VALUES (6001, 6001, 435, 'Wewewa Barat');
INSERT INTO `subdistricts` VALUES (6002, 6002, 435, 'Wewewa Selatan');
INSERT INTO `subdistricts` VALUES (6003, 6003, 435, 'Wewewa Tengah (Wewera Tengah)');
INSERT INTO `subdistricts` VALUES (6004, 6004, 435, 'Wewewa Timur');
INSERT INTO `subdistricts` VALUES (6005, 6005, 435, 'Wewewa Utara');
INSERT INTO `subdistricts` VALUES (6006, 6006, 436, 'Katikutana');
INSERT INTO `subdistricts` VALUES (6007, 6007, 436, 'Katikutana Selatan');
INSERT INTO `subdistricts` VALUES (6008, 6008, 436, 'Mamboro');
INSERT INTO `subdistricts` VALUES (6009, 6009, 436, 'Umbu Ratu Nggay');
INSERT INTO `subdistricts` VALUES (6010, 6010, 436, 'Umbu Ratu Nggay Barat');
INSERT INTO `subdistricts` VALUES (6011, 6011, 437, 'Haharu');
INSERT INTO `subdistricts` VALUES (6012, 6012, 437, 'Kahaunguweti (Kahaungu Eti)');
INSERT INTO `subdistricts` VALUES (6013, 6013, 437, 'Kambata Mapambuhang');
INSERT INTO `subdistricts` VALUES (6014, 6014, 437, 'Kambera');
INSERT INTO `subdistricts` VALUES (6015, 6015, 437, 'Kanatang');
INSERT INTO `subdistricts` VALUES (6016, 6016, 437, 'Karera');
INSERT INTO `subdistricts` VALUES (6017, 6017, 437, 'Katala Hamu Lingu');
INSERT INTO `subdistricts` VALUES (6018, 6018, 437, 'Kota Waingapu');
INSERT INTO `subdistricts` VALUES (6019, 6019, 437, 'Lewa');
INSERT INTO `subdistricts` VALUES (6020, 6020, 437, 'Lewa Tidahu');
INSERT INTO `subdistricts` VALUES (6021, 6021, 437, 'Mahu');
INSERT INTO `subdistricts` VALUES (6022, 6022, 437, 'Matawai Lappau (La Pawu)');
INSERT INTO `subdistricts` VALUES (6023, 6023, 437, 'Ngadu Ngala');
INSERT INTO `subdistricts` VALUES (6024, 6024, 437, 'Nggaha Oriangu');
INSERT INTO `subdistricts` VALUES (6025, 6025, 437, 'Paberiwai');
INSERT INTO `subdistricts` VALUES (6026, 6026, 437, 'Pahunga Lodu');
INSERT INTO `subdistricts` VALUES (6027, 6027, 437, 'Pandawai');
INSERT INTO `subdistricts` VALUES (6028, 6028, 437, 'Pinupahar (Pirapahar)');
INSERT INTO `subdistricts` VALUES (6029, 6029, 437, 'Rindi');
INSERT INTO `subdistricts` VALUES (6030, 6030, 437, 'Tabundung');
INSERT INTO `subdistricts` VALUES (6031, 6031, 437, 'Umalulu');
INSERT INTO `subdistricts` VALUES (6032, 6032, 437, 'Wula Waijelu');
INSERT INTO `subdistricts` VALUES (6033, 6033, 438, 'Alas');
INSERT INTO `subdistricts` VALUES (6034, 6034, 438, 'Alas Barat');
INSERT INTO `subdistricts` VALUES (6035, 6035, 438, 'Batulanteh');
INSERT INTO `subdistricts` VALUES (6036, 6036, 438, 'Buer');
INSERT INTO `subdistricts` VALUES (6037, 6037, 438, 'Empang');
INSERT INTO `subdistricts` VALUES (6038, 6038, 438, 'Labangka');
INSERT INTO `subdistricts` VALUES (6039, 6039, 438, 'Labuhan Badas');
INSERT INTO `subdistricts` VALUES (6040, 6040, 438, 'Lantung');
INSERT INTO `subdistricts` VALUES (6041, 6041, 438, 'Lape (Lape Lopok)');
INSERT INTO `subdistricts` VALUES (6042, 6042, 438, 'Lenangguar');
INSERT INTO `subdistricts` VALUES (6043, 6043, 438, 'Lopok');
INSERT INTO `subdistricts` VALUES (6044, 6044, 438, 'Lunyuk');
INSERT INTO `subdistricts` VALUES (6045, 6045, 438, 'Maronge');
INSERT INTO `subdistricts` VALUES (6046, 6046, 438, 'Moyo Hilir');
INSERT INTO `subdistricts` VALUES (6047, 6047, 438, 'Moyo Hulu');
INSERT INTO `subdistricts` VALUES (6048, 6048, 438, 'Moyo Utara');
INSERT INTO `subdistricts` VALUES (6049, 6049, 438, 'Orong Telu');
INSERT INTO `subdistricts` VALUES (6050, 6050, 438, 'Plampang');
INSERT INTO `subdistricts` VALUES (6051, 6051, 438, 'Rhee');
INSERT INTO `subdistricts` VALUES (6052, 6052, 438, 'Ropang');
INSERT INTO `subdistricts` VALUES (6053, 6053, 438, 'Sumbawa');
INSERT INTO `subdistricts` VALUES (6054, 6054, 438, 'Tarano');
INSERT INTO `subdistricts` VALUES (6055, 6055, 438, 'Unter Iwes (Unterwiris)');
INSERT INTO `subdistricts` VALUES (6056, 6056, 438, 'Utan');
INSERT INTO `subdistricts` VALUES (6057, 6057, 439, 'Brang Ene');
INSERT INTO `subdistricts` VALUES (6058, 6058, 439, 'Brang Rea');
INSERT INTO `subdistricts` VALUES (6059, 6059, 439, 'Jereweh');
INSERT INTO `subdistricts` VALUES (6060, 6060, 439, 'Maluk');
INSERT INTO `subdistricts` VALUES (6061, 6061, 439, 'Poto Tano');
INSERT INTO `subdistricts` VALUES (6062, 6062, 439, 'Sateluk (Seteluk)');
INSERT INTO `subdistricts` VALUES (6063, 6063, 439, 'Sekongkang');
INSERT INTO `subdistricts` VALUES (6064, 6064, 439, 'Taliwang');
INSERT INTO `subdistricts` VALUES (6065, 6065, 440, 'Buahdua');
INSERT INTO `subdistricts` VALUES (6066, 6066, 440, 'Cibugel');
INSERT INTO `subdistricts` VALUES (6067, 6067, 440, 'Cimalaka');
INSERT INTO `subdistricts` VALUES (6068, 6068, 440, 'Cimanggung');
INSERT INTO `subdistricts` VALUES (6069, 6069, 440, 'Cisarua');
INSERT INTO `subdistricts` VALUES (6070, 6070, 440, 'Cisitu');
INSERT INTO `subdistricts` VALUES (6071, 6071, 440, 'Conggeang');
INSERT INTO `subdistricts` VALUES (6072, 6072, 440, 'Darmaraja');
INSERT INTO `subdistricts` VALUES (6073, 6073, 440, 'Ganeas');
INSERT INTO `subdistricts` VALUES (6074, 6074, 440, 'Jatigede');
INSERT INTO `subdistricts` VALUES (6075, 6075, 440, 'Jatinangor');
INSERT INTO `subdistricts` VALUES (6076, 6076, 440, 'Jatinunggal');
INSERT INTO `subdistricts` VALUES (6077, 6077, 440, 'Pamulihan');
INSERT INTO `subdistricts` VALUES (6078, 6078, 440, 'Paseh');
INSERT INTO `subdistricts` VALUES (6079, 6079, 440, 'Rancakalong');
INSERT INTO `subdistricts` VALUES (6080, 6080, 440, 'Situraja');
INSERT INTO `subdistricts` VALUES (6081, 6081, 440, 'Sukasari');
INSERT INTO `subdistricts` VALUES (6082, 6082, 440, 'Sumedang Selatan');
INSERT INTO `subdistricts` VALUES (6083, 6083, 440, 'Sumedang Utara');
INSERT INTO `subdistricts` VALUES (6084, 6084, 440, 'Surian');
INSERT INTO `subdistricts` VALUES (6085, 6085, 440, 'Tanjungkerta');
INSERT INTO `subdistricts` VALUES (6086, 6086, 440, 'Tanjungmedar');
INSERT INTO `subdistricts` VALUES (6087, 6087, 440, 'Tanjungsari');
INSERT INTO `subdistricts` VALUES (6088, 6088, 440, 'Tomo');
INSERT INTO `subdistricts` VALUES (6089, 6089, 440, 'Ujungjaya');
INSERT INTO `subdistricts` VALUES (6090, 6090, 440, 'Wado');
INSERT INTO `subdistricts` VALUES (6091, 6091, 441, 'Ambunten');
INSERT INTO `subdistricts` VALUES (6092, 6092, 441, 'Arjasa');
INSERT INTO `subdistricts` VALUES (6093, 6093, 441, 'Batang Batang');
INSERT INTO `subdistricts` VALUES (6094, 6094, 441, 'Batuan');
INSERT INTO `subdistricts` VALUES (6095, 6095, 441, 'Batuputih');
INSERT INTO `subdistricts` VALUES (6096, 6096, 441, 'Bluto');
INSERT INTO `subdistricts` VALUES (6097, 6097, 441, 'Dasuk');
INSERT INTO `subdistricts` VALUES (6098, 6098, 441, 'Dungkek');
INSERT INTO `subdistricts` VALUES (6099, 6099, 441, 'Ganding');
INSERT INTO `subdistricts` VALUES (6100, 6100, 441, 'Gapura');
INSERT INTO `subdistricts` VALUES (6101, 6101, 441, 'Gayam');
INSERT INTO `subdistricts` VALUES (6102, 6102, 441, 'Gili Ginting (Giligenteng)');
INSERT INTO `subdistricts` VALUES (6103, 6103, 441, 'Guluk Guluk');
INSERT INTO `subdistricts` VALUES (6104, 6104, 441, 'Kalianget');
INSERT INTO `subdistricts` VALUES (6105, 6105, 441, 'Kangayan');
INSERT INTO `subdistricts` VALUES (6106, 6106, 441, 'Kota Sumenep');
INSERT INTO `subdistricts` VALUES (6107, 6107, 441, 'Lenteng');
INSERT INTO `subdistricts` VALUES (6108, 6108, 441, 'Manding');
INSERT INTO `subdistricts` VALUES (6109, 6109, 441, 'Masalembu');
INSERT INTO `subdistricts` VALUES (6110, 6110, 441, 'Nonggunong');
INSERT INTO `subdistricts` VALUES (6111, 6111, 441, 'Pasongsongan');
INSERT INTO `subdistricts` VALUES (6112, 6112, 441, 'Pragaan');
INSERT INTO `subdistricts` VALUES (6113, 6113, 441, 'Ra\'as (Raas)');
INSERT INTO `subdistricts` VALUES (6114, 6114, 441, 'Rubaru');
INSERT INTO `subdistricts` VALUES (6115, 6115, 441, 'Sapeken');
INSERT INTO `subdistricts` VALUES (6116, 6116, 441, 'Saronggi');
INSERT INTO `subdistricts` VALUES (6117, 6117, 441, 'Talango');
INSERT INTO `subdistricts` VALUES (6118, 6118, 442, 'Hamparan Rawang');
INSERT INTO `subdistricts` VALUES (6119, 6119, 442, 'Koto Baru');
INSERT INTO `subdistricts` VALUES (6120, 6120, 442, 'Kumun Debai');
INSERT INTO `subdistricts` VALUES (6121, 6121, 442, 'Pesisir Bukit');
INSERT INTO `subdistricts` VALUES (6122, 6122, 442, 'Pondok Tinggi');
INSERT INTO `subdistricts` VALUES (6123, 6123, 442, 'Sungai Bungkal');
INSERT INTO `subdistricts` VALUES (6124, 6124, 442, 'Sungai Penuh');
INSERT INTO `subdistricts` VALUES (6125, 6125, 442, 'Tanah Kampung');
INSERT INTO `subdistricts` VALUES (6126, 6126, 443, 'Kepulauan Aruri');
INSERT INTO `subdistricts` VALUES (6127, 6127, 443, 'Supiori Barat');
INSERT INTO `subdistricts` VALUES (6128, 6128, 443, 'Supiori Selatan');
INSERT INTO `subdistricts` VALUES (6129, 6129, 443, 'Supiori Timur');
INSERT INTO `subdistricts` VALUES (6130, 6130, 443, 'Supiori Utara');
INSERT INTO `subdistricts` VALUES (6131, 6131, 444, 'Asemrowo');
INSERT INTO `subdistricts` VALUES (6132, 6132, 444, 'Benowo');
INSERT INTO `subdistricts` VALUES (6133, 6133, 444, 'Bubutan');
INSERT INTO `subdistricts` VALUES (6134, 6134, 444, 'Bulak');
INSERT INTO `subdistricts` VALUES (6135, 6135, 444, 'Dukuh Pakis');
INSERT INTO `subdistricts` VALUES (6136, 6136, 444, 'Gayungan');
INSERT INTO `subdistricts` VALUES (6137, 6137, 444, 'Genteng');
INSERT INTO `subdistricts` VALUES (6138, 6138, 444, 'Gubeng');
INSERT INTO `subdistricts` VALUES (6139, 6139, 444, 'Gununganyar');
INSERT INTO `subdistricts` VALUES (6140, 6140, 444, 'Jambangan');
INSERT INTO `subdistricts` VALUES (6141, 6141, 444, 'Karangpilang');
INSERT INTO `subdistricts` VALUES (6142, 6142, 444, 'Kenjeran');
INSERT INTO `subdistricts` VALUES (6143, 6143, 444, 'Krembangan');
INSERT INTO `subdistricts` VALUES (6144, 6144, 444, 'Lakar Santri');
INSERT INTO `subdistricts` VALUES (6145, 6145, 444, 'Mulyorejo');
INSERT INTO `subdistricts` VALUES (6146, 6146, 444, 'Pabean Cantikan');
INSERT INTO `subdistricts` VALUES (6147, 6147, 444, 'Pakal');
INSERT INTO `subdistricts` VALUES (6148, 6148, 444, 'Rungkut');
INSERT INTO `subdistricts` VALUES (6149, 6149, 444, 'Sambikerep');
INSERT INTO `subdistricts` VALUES (6150, 6150, 444, 'Sawahan');
INSERT INTO `subdistricts` VALUES (6151, 6151, 444, 'Semampir');
INSERT INTO `subdistricts` VALUES (6152, 6152, 444, 'Simokerto');
INSERT INTO `subdistricts` VALUES (6153, 6153, 444, 'Sukolilo');
INSERT INTO `subdistricts` VALUES (6154, 6154, 444, 'Sukomanunggal');
INSERT INTO `subdistricts` VALUES (6155, 6155, 444, 'Tambaksari');
INSERT INTO `subdistricts` VALUES (6156, 6156, 444, 'Tandes');
INSERT INTO `subdistricts` VALUES (6157, 6157, 444, 'Tegalsari');
INSERT INTO `subdistricts` VALUES (6158, 6158, 444, 'Tenggilis Mejoyo');
INSERT INTO `subdistricts` VALUES (6159, 6159, 444, 'Wiyung');
INSERT INTO `subdistricts` VALUES (6160, 6160, 444, 'Wonocolo');
INSERT INTO `subdistricts` VALUES (6161, 6161, 444, 'Wonokromo');
INSERT INTO `subdistricts` VALUES (6162, 6162, 445, 'Banjarsari');
INSERT INTO `subdistricts` VALUES (6163, 6163, 445, 'Jebres');
INSERT INTO `subdistricts` VALUES (6164, 6164, 445, 'Laweyan');
INSERT INTO `subdistricts` VALUES (6165, 6165, 445, 'Pasar Kliwon');
INSERT INTO `subdistricts` VALUES (6166, 6166, 445, 'Serengan');
INSERT INTO `subdistricts` VALUES (6167, 6167, 446, 'Banua Lawas');
INSERT INTO `subdistricts` VALUES (6168, 6168, 446, 'Bintang Ara');
INSERT INTO `subdistricts` VALUES (6169, 6169, 446, 'Haruai');
INSERT INTO `subdistricts` VALUES (6170, 6170, 446, 'Jaro');
INSERT INTO `subdistricts` VALUES (6171, 6171, 446, 'Kelua (Klua)');
INSERT INTO `subdistricts` VALUES (6172, 6172, 446, 'Muara Harus');
INSERT INTO `subdistricts` VALUES (6173, 6173, 446, 'Muara Uya');
INSERT INTO `subdistricts` VALUES (6174, 6174, 446, 'Murung Pudak');
INSERT INTO `subdistricts` VALUES (6175, 6175, 446, 'Pugaan');
INSERT INTO `subdistricts` VALUES (6176, 6176, 446, 'Tanjung');
INSERT INTO `subdistricts` VALUES (6177, 6177, 446, 'Tanta');
INSERT INTO `subdistricts` VALUES (6178, 6178, 446, 'Upau');
INSERT INTO `subdistricts` VALUES (6179, 6179, 447, 'Baturiti');
INSERT INTO `subdistricts` VALUES (6180, 6180, 447, 'Kediri');
INSERT INTO `subdistricts` VALUES (6181, 6181, 447, 'Kerambitan');
INSERT INTO `subdistricts` VALUES (6182, 6182, 447, 'Marga');
INSERT INTO `subdistricts` VALUES (6183, 6183, 447, 'Penebel');
INSERT INTO `subdistricts` VALUES (6184, 6184, 447, 'Pupuan');
INSERT INTO `subdistricts` VALUES (6185, 6185, 447, 'Selemadeg');
INSERT INTO `subdistricts` VALUES (6186, 6186, 447, 'Selemadeg / Salamadeg Timur');
INSERT INTO `subdistricts` VALUES (6187, 6187, 447, 'Selemadeg / Salemadeg Barat');
INSERT INTO `subdistricts` VALUES (6188, 6188, 447, 'Tabanan');
INSERT INTO `subdistricts` VALUES (6189, 6189, 448, 'Galesong');
INSERT INTO `subdistricts` VALUES (6190, 6190, 448, 'Galesong Selatan');
INSERT INTO `subdistricts` VALUES (6191, 6191, 448, 'Galesong Utara');
INSERT INTO `subdistricts` VALUES (6192, 6192, 448, 'Mangara Bombang');
INSERT INTO `subdistricts` VALUES (6193, 6193, 448, 'Mappakasunggu');
INSERT INTO `subdistricts` VALUES (6194, 6194, 448, 'Patallassang');
INSERT INTO `subdistricts` VALUES (6195, 6195, 448, 'Polombangkeng Selatan (Polobangkeng)');
INSERT INTO `subdistricts` VALUES (6196, 6196, 448, 'Polombangkeng Utara (Polobangkeng)');
INSERT INTO `subdistricts` VALUES (6197, 6197, 448, 'Sanrobone');
INSERT INTO `subdistricts` VALUES (6198, 6198, 449, 'Abun');
INSERT INTO `subdistricts` VALUES (6199, 6199, 449, 'Amberbaken');
INSERT INTO `subdistricts` VALUES (6200, 6200, 449, 'Fef (Peef)');
INSERT INTO `subdistricts` VALUES (6201, 6201, 449, 'Kebar');
INSERT INTO `subdistricts` VALUES (6202, 6202, 449, 'Kwoor');
INSERT INTO `subdistricts` VALUES (6203, 6203, 449, 'Miyah (Meyah)');
INSERT INTO `subdistricts` VALUES (6204, 6204, 449, 'Moraid');
INSERT INTO `subdistricts` VALUES (6205, 6205, 449, 'Mubrani');
INSERT INTO `subdistricts` VALUES (6206, 6206, 449, 'Sausapor');
INSERT INTO `subdistricts` VALUES (6207, 6207, 449, 'Senopi');
INSERT INTO `subdistricts` VALUES (6208, 6208, 449, 'Syujak');
INSERT INTO `subdistricts` VALUES (6209, 6209, 449, 'Yembun');
INSERT INTO `subdistricts` VALUES (6210, 6210, 450, 'Betayau');
INSERT INTO `subdistricts` VALUES (6211, 6211, 450, 'Sesayap');
INSERT INTO `subdistricts` VALUES (6212, 6212, 450, 'Sesayap Hilir');
INSERT INTO `subdistricts` VALUES (6213, 6213, 450, 'Tana Lia (Tanah Lia)');
INSERT INTO `subdistricts` VALUES (6214, 6214, 451, 'Bittuang');
INSERT INTO `subdistricts` VALUES (6215, 6215, 451, 'Bonggakaradeng');
INSERT INTO `subdistricts` VALUES (6216, 6216, 451, 'Gandang Batu Sillanan');
INSERT INTO `subdistricts` VALUES (6217, 6217, 451, 'Kurra');
INSERT INTO `subdistricts` VALUES (6218, 6218, 451, 'Makale');
INSERT INTO `subdistricts` VALUES (6219, 6219, 451, 'Makale Selatan');
INSERT INTO `subdistricts` VALUES (6220, 6220, 451, 'Makale Utara');
INSERT INTO `subdistricts` VALUES (6221, 6221, 451, 'Malimbong Balepe');
INSERT INTO `subdistricts` VALUES (6222, 6222, 451, 'Mappak');
INSERT INTO `subdistricts` VALUES (6223, 6223, 451, 'Masanda');
INSERT INTO `subdistricts` VALUES (6224, 6224, 451, 'Mengkendek');
INSERT INTO `subdistricts` VALUES (6225, 6225, 451, 'Rano');
INSERT INTO `subdistricts` VALUES (6226, 6226, 451, 'Rantetayo');
INSERT INTO `subdistricts` VALUES (6227, 6227, 451, 'Rembon');
INSERT INTO `subdistricts` VALUES (6228, 6228, 451, 'Saluputti');
INSERT INTO `subdistricts` VALUES (6229, 6229, 451, 'Sangalla (Sanggala)');
INSERT INTO `subdistricts` VALUES (6230, 6230, 451, 'Sangalla Selatan');
INSERT INTO `subdistricts` VALUES (6231, 6231, 451, 'Sangalla Utara');
INSERT INTO `subdistricts` VALUES (6232, 6232, 451, 'Simbuang');
INSERT INTO `subdistricts` VALUES (6233, 6233, 452, 'Angsana');
INSERT INTO `subdistricts` VALUES (6234, 6234, 452, 'Batulicin');
INSERT INTO `subdistricts` VALUES (6235, 6235, 452, 'Karang Bintang');
INSERT INTO `subdistricts` VALUES (6236, 6236, 452, 'Kuranji');
INSERT INTO `subdistricts` VALUES (6237, 6237, 452, 'Kusan Hilir');
INSERT INTO `subdistricts` VALUES (6238, 6238, 452, 'Kusan Hulu');
INSERT INTO `subdistricts` VALUES (6239, 6239, 452, 'Mantewe');
INSERT INTO `subdistricts` VALUES (6240, 6240, 452, 'Satui');
INSERT INTO `subdistricts` VALUES (6241, 6241, 452, 'Simpang Empat');
INSERT INTO `subdistricts` VALUES (6242, 6242, 452, 'Sungai Loban');
INSERT INTO `subdistricts` VALUES (6243, 6243, 453, 'Batipuh');
INSERT INTO `subdistricts` VALUES (6244, 6244, 453, 'Batipuh Selatan');
INSERT INTO `subdistricts` VALUES (6245, 6245, 453, 'Lima Kaum');
INSERT INTO `subdistricts` VALUES (6246, 6246, 453, 'Lintau Buo');
INSERT INTO `subdistricts` VALUES (6247, 6247, 453, 'Lintau Buo Utara');
INSERT INTO `subdistricts` VALUES (6248, 6248, 453, 'Padang Ganting');
INSERT INTO `subdistricts` VALUES (6249, 6249, 453, 'Pariangan');
INSERT INTO `subdistricts` VALUES (6250, 6250, 453, 'Rambatan');
INSERT INTO `subdistricts` VALUES (6251, 6251, 453, 'Salimpaung');
INSERT INTO `subdistricts` VALUES (6252, 6252, 453, 'Sepuluh Koto (X Koto)');
INSERT INTO `subdistricts` VALUES (6253, 6253, 453, 'Sungai Tarab');
INSERT INTO `subdistricts` VALUES (6254, 6254, 453, 'Sungayang');
INSERT INTO `subdistricts` VALUES (6255, 6255, 453, 'Tanjung Baru');
INSERT INTO `subdistricts` VALUES (6256, 6256, 453, 'Tanjung Emas');
INSERT INTO `subdistricts` VALUES (6257, 6257, 454, 'Bajuin');
INSERT INTO `subdistricts` VALUES (6258, 6258, 454, 'Bati-Bati');
INSERT INTO `subdistricts` VALUES (6259, 6259, 454, 'Batu Ampar');
INSERT INTO `subdistricts` VALUES (6260, 6260, 454, 'Bumi Makmur');
INSERT INTO `subdistricts` VALUES (6261, 6261, 454, 'Jorong');
INSERT INTO `subdistricts` VALUES (6262, 6262, 454, 'Kintap');
INSERT INTO `subdistricts` VALUES (6263, 6263, 454, 'Kurau');
INSERT INTO `subdistricts` VALUES (6264, 6264, 454, 'Panyipatan');
INSERT INTO `subdistricts` VALUES (6265, 6265, 454, 'Pelaihari');
INSERT INTO `subdistricts` VALUES (6266, 6266, 454, 'Takisung');
INSERT INTO `subdistricts` VALUES (6267, 6267, 454, 'Tambang Ulang');
INSERT INTO `subdistricts` VALUES (6268, 6268, 455, 'Balaraja');
INSERT INTO `subdistricts` VALUES (6269, 6269, 455, 'Cikupa');
INSERT INTO `subdistricts` VALUES (6270, 6270, 455, 'Cisauk');
INSERT INTO `subdistricts` VALUES (6271, 6271, 455, 'Cisoka');
INSERT INTO `subdistricts` VALUES (6272, 6272, 455, 'Curug');
INSERT INTO `subdistricts` VALUES (6273, 6273, 455, 'Gunung Kaler');
INSERT INTO `subdistricts` VALUES (6274, 6274, 455, 'Jambe');
INSERT INTO `subdistricts` VALUES (6275, 6275, 455, 'Jayanti');
INSERT INTO `subdistricts` VALUES (6276, 6276, 455, 'Kelapa Dua');
INSERT INTO `subdistricts` VALUES (6277, 6277, 455, 'Kemiri');
INSERT INTO `subdistricts` VALUES (6278, 6278, 455, 'Kosambi');
INSERT INTO `subdistricts` VALUES (6279, 6279, 455, 'Kresek');
INSERT INTO `subdistricts` VALUES (6280, 6280, 455, 'Kronjo');
INSERT INTO `subdistricts` VALUES (6281, 6281, 455, 'Legok');
INSERT INTO `subdistricts` VALUES (6282, 6282, 455, 'Mauk');
INSERT INTO `subdistricts` VALUES (6283, 6283, 455, 'Mekar Baru');
INSERT INTO `subdistricts` VALUES (6284, 6284, 455, 'Pagedangan');
INSERT INTO `subdistricts` VALUES (6285, 6285, 455, 'Pakuhaji');
INSERT INTO `subdistricts` VALUES (6286, 6286, 455, 'Panongan');
INSERT INTO `subdistricts` VALUES (6287, 6287, 455, 'Pasar Kemis');
INSERT INTO `subdistricts` VALUES (6288, 6288, 455, 'Rajeg');
INSERT INTO `subdistricts` VALUES (6289, 6289, 455, 'Sepatan');
INSERT INTO `subdistricts` VALUES (6290, 6290, 455, 'Sepatan Timur');
INSERT INTO `subdistricts` VALUES (6291, 6291, 455, 'Sindang Jaya');
INSERT INTO `subdistricts` VALUES (6292, 6292, 455, 'Solear');
INSERT INTO `subdistricts` VALUES (6293, 6293, 455, 'Sukadiri');
INSERT INTO `subdistricts` VALUES (6294, 6294, 455, 'Sukamulya');
INSERT INTO `subdistricts` VALUES (6295, 6295, 455, 'Teluknaga');
INSERT INTO `subdistricts` VALUES (6296, 6296, 455, 'Tigaraksa');
INSERT INTO `subdistricts` VALUES (6297, 6297, 456, 'Batuceper');
INSERT INTO `subdistricts` VALUES (6298, 6298, 456, 'Benda');
INSERT INTO `subdistricts` VALUES (6299, 6299, 456, 'Cibodas');
INSERT INTO `subdistricts` VALUES (6300, 6300, 456, 'Ciledug');
INSERT INTO `subdistricts` VALUES (6301, 6301, 456, 'Cipondoh');
INSERT INTO `subdistricts` VALUES (6302, 6302, 456, 'Jatiuwung');
INSERT INTO `subdistricts` VALUES (6303, 6303, 456, 'Karang Tengah');
INSERT INTO `subdistricts` VALUES (6304, 6304, 456, 'Karawaci');
INSERT INTO `subdistricts` VALUES (6305, 6305, 456, 'Larangan');
INSERT INTO `subdistricts` VALUES (6306, 6306, 456, 'Neglasari');
INSERT INTO `subdistricts` VALUES (6307, 6307, 456, 'Periuk');
INSERT INTO `subdistricts` VALUES (6308, 6308, 456, 'Pinang (Penang)');
INSERT INTO `subdistricts` VALUES (6309, 6309, 456, 'Tangerang');
INSERT INTO `subdistricts` VALUES (6310, 6310, 457, 'Ciputat');
INSERT INTO `subdistricts` VALUES (6311, 6311, 457, 'Ciputat Timur');
INSERT INTO `subdistricts` VALUES (6312, 6312, 457, 'Pamulang');
INSERT INTO `subdistricts` VALUES (6313, 6313, 457, 'Pondok Aren');
INSERT INTO `subdistricts` VALUES (6314, 6314, 457, 'Serpong');
INSERT INTO `subdistricts` VALUES (6315, 6315, 457, 'Serpong Utara');
INSERT INTO `subdistricts` VALUES (6316, 6316, 457, 'Setu');
INSERT INTO `subdistricts` VALUES (6317, 6317, 458, 'Air Naningan');
INSERT INTO `subdistricts` VALUES (6318, 6318, 458, 'Bandar Negeri Semuong');
INSERT INTO `subdistricts` VALUES (6319, 6319, 458, 'Bulok');
INSERT INTO `subdistricts` VALUES (6320, 6320, 458, 'Cukuh Balak');
INSERT INTO `subdistricts` VALUES (6321, 6321, 458, 'Gisting');
INSERT INTO `subdistricts` VALUES (6322, 6322, 458, 'Gunung Alip');
INSERT INTO `subdistricts` VALUES (6323, 6323, 458, 'Kelumbayan');
INSERT INTO `subdistricts` VALUES (6324, 6324, 458, 'Kelumbayan Barat');
INSERT INTO `subdistricts` VALUES (6325, 6325, 458, 'Kota Agung (Kota Agung Pusat)');
INSERT INTO `subdistricts` VALUES (6326, 6326, 458, 'Kota Agung Barat');
INSERT INTO `subdistricts` VALUES (6327, 6327, 458, 'Kota Agung Timur');
INSERT INTO `subdistricts` VALUES (6328, 6328, 458, 'Limau');
INSERT INTO `subdistricts` VALUES (6329, 6329, 458, 'Pematang Sawa');
INSERT INTO `subdistricts` VALUES (6330, 6330, 458, 'Pugung');
INSERT INTO `subdistricts` VALUES (6331, 6331, 458, 'Pulau Panggung');
INSERT INTO `subdistricts` VALUES (6332, 6332, 458, 'Semaka');
INSERT INTO `subdistricts` VALUES (6333, 6333, 458, 'Sumberejo');
INSERT INTO `subdistricts` VALUES (6334, 6334, 458, 'Talang Padang');
INSERT INTO `subdistricts` VALUES (6335, 6335, 458, 'Ulubelu');
INSERT INTO `subdistricts` VALUES (6336, 6336, 458, 'Wonosobo');
INSERT INTO `subdistricts` VALUES (6337, 6337, 459, 'Datuk Bandar');
INSERT INTO `subdistricts` VALUES (6338, 6338, 459, 'Datuk Bandar Timur');
INSERT INTO `subdistricts` VALUES (6339, 6339, 459, 'Sei Tualang Raso');
INSERT INTO `subdistricts` VALUES (6340, 6340, 459, 'Tanjung Balai Selatan');
INSERT INTO `subdistricts` VALUES (6341, 6341, 459, 'Tanjung Balai Utara');
INSERT INTO `subdistricts` VALUES (6342, 6342, 459, 'Teluk Nibung');
INSERT INTO `subdistricts` VALUES (6343, 6343, 460, 'Batang Asam');
INSERT INTO `subdistricts` VALUES (6344, 6344, 460, 'Betara');
INSERT INTO `subdistricts` VALUES (6345, 6345, 460, 'Bram Itam');
INSERT INTO `subdistricts` VALUES (6346, 6346, 460, 'Kuala Betara');
INSERT INTO `subdistricts` VALUES (6347, 6347, 460, 'Merlung');
INSERT INTO `subdistricts` VALUES (6348, 6348, 460, 'Muara Papalik');
INSERT INTO `subdistricts` VALUES (6349, 6349, 460, 'Pengabuan');
INSERT INTO `subdistricts` VALUES (6350, 6350, 460, 'Renah Mendaluh');
INSERT INTO `subdistricts` VALUES (6351, 6351, 460, 'Seberang Kota');
INSERT INTO `subdistricts` VALUES (6352, 6352, 460, 'Senyerang');
INSERT INTO `subdistricts` VALUES (6353, 6353, 460, 'Tebing Tinggi');
INSERT INTO `subdistricts` VALUES (6354, 6354, 460, 'Tungkal Ilir');
INSERT INTO `subdistricts` VALUES (6355, 6355, 460, 'Tungkal Ulu');
INSERT INTO `subdistricts` VALUES (6356, 6356, 461, 'Berbak');
INSERT INTO `subdistricts` VALUES (6357, 6357, 461, 'Dendang');
INSERT INTO `subdistricts` VALUES (6358, 6358, 461, 'Geragai');
INSERT INTO `subdistricts` VALUES (6359, 6359, 461, 'Kuala Jambi');
INSERT INTO `subdistricts` VALUES (6360, 6360, 461, 'Mendahara');
INSERT INTO `subdistricts` VALUES (6361, 6361, 461, 'Mendahara Ulu');
INSERT INTO `subdistricts` VALUES (6362, 6362, 461, 'Muara Sabak Barat');
INSERT INTO `subdistricts` VALUES (6363, 6363, 461, 'Muara Sabak Timur');
INSERT INTO `subdistricts` VALUES (6364, 6364, 461, 'Nipah Panjang');
INSERT INTO `subdistricts` VALUES (6365, 6365, 461, 'Rantau Rasau');
INSERT INTO `subdistricts` VALUES (6366, 6366, 461, 'Sadu');
INSERT INTO `subdistricts` VALUES (6367, 6367, 462, 'Bukit Bestari');
INSERT INTO `subdistricts` VALUES (6368, 6368, 462, 'Tanjung Pinang Barat');
INSERT INTO `subdistricts` VALUES (6369, 6369, 462, 'Tanjung Pinang Kota');
INSERT INTO `subdistricts` VALUES (6370, 6370, 462, 'Tanjung Pinang Timur');
INSERT INTO `subdistricts` VALUES (6371, 6371, 463, 'Aek Bilah');
INSERT INTO `subdistricts` VALUES (6372, 6372, 463, 'Angkola Barat (Padang Sidempuan)');
INSERT INTO `subdistricts` VALUES (6373, 6373, 463, 'Angkola Sangkunur');
INSERT INTO `subdistricts` VALUES (6374, 6374, 463, 'Angkola Selatan (Siais)');
INSERT INTO `subdistricts` VALUES (6375, 6375, 463, 'Angkola Timur (Padang Sidempuan)');
INSERT INTO `subdistricts` VALUES (6376, 6376, 463, 'Arse');
INSERT INTO `subdistricts` VALUES (6377, 6377, 463, 'Batang Angkola');
INSERT INTO `subdistricts` VALUES (6378, 6378, 463, 'Batang Toru');
INSERT INTO `subdistricts` VALUES (6379, 6379, 463, 'Marancar');
INSERT INTO `subdistricts` VALUES (6380, 6380, 463, 'Muara Batang Toru');
INSERT INTO `subdistricts` VALUES (6381, 6381, 463, 'Saipar Dolok Hole');
INSERT INTO `subdistricts` VALUES (6382, 6382, 463, 'Sayur Matinggi');
INSERT INTO `subdistricts` VALUES (6383, 6383, 463, 'Sipirok');
INSERT INTO `subdistricts` VALUES (6384, 6384, 463, 'Tano Tombangan Angkola');
INSERT INTO `subdistricts` VALUES (6385, 6385, 464, 'Andam Dewi');
INSERT INTO `subdistricts` VALUES (6386, 6386, 464, 'Badiri');
INSERT INTO `subdistricts` VALUES (6387, 6387, 464, 'Barus');
INSERT INTO `subdistricts` VALUES (6388, 6388, 464, 'Barus Utara');
INSERT INTO `subdistricts` VALUES (6389, 6389, 464, 'Kolang');
INSERT INTO `subdistricts` VALUES (6390, 6390, 464, 'Lumut');
INSERT INTO `subdistricts` VALUES (6391, 6391, 464, 'Manduamas');
INSERT INTO `subdistricts` VALUES (6392, 6392, 464, 'Pandan');
INSERT INTO `subdistricts` VALUES (6393, 6393, 464, 'Pasaribu Tobing');
INSERT INTO `subdistricts` VALUES (6394, 6394, 464, 'Pinangsori');
INSERT INTO `subdistricts` VALUES (6395, 6395, 464, 'Sarudik');
INSERT INTO `subdistricts` VALUES (6396, 6396, 464, 'Sibabangun');
INSERT INTO `subdistricts` VALUES (6397, 6397, 464, 'Sirandorung');
INSERT INTO `subdistricts` VALUES (6398, 6398, 464, 'Sitahuis');
INSERT INTO `subdistricts` VALUES (6399, 6399, 464, 'Sorkam');
INSERT INTO `subdistricts` VALUES (6400, 6400, 464, 'Sorkam Barat');
INSERT INTO `subdistricts` VALUES (6401, 6401, 464, 'Sosor Gadong');
INSERT INTO `subdistricts` VALUES (6402, 6402, 464, 'Suka Bangun');
INSERT INTO `subdistricts` VALUES (6403, 6403, 464, 'Tapian Nauli');
INSERT INTO `subdistricts` VALUES (6404, 6404, 464, 'Tukka');
INSERT INTO `subdistricts` VALUES (6405, 6405, 465, 'Adian Koting');
INSERT INTO `subdistricts` VALUES (6406, 6406, 465, 'Garoga');
INSERT INTO `subdistricts` VALUES (6407, 6407, 465, 'Muara');
INSERT INTO `subdistricts` VALUES (6408, 6408, 465, 'Pagaran');
INSERT INTO `subdistricts` VALUES (6409, 6409, 465, 'Pahae Jae');
INSERT INTO `subdistricts` VALUES (6410, 6410, 465, 'Pahae Julu');
INSERT INTO `subdistricts` VALUES (6411, 6411, 465, 'Pangaribuan');
INSERT INTO `subdistricts` VALUES (6412, 6412, 465, 'Parmonangan');
INSERT INTO `subdistricts` VALUES (6413, 6413, 465, 'Purbatua');
INSERT INTO `subdistricts` VALUES (6414, 6414, 465, 'Siatas Barita');
INSERT INTO `subdistricts` VALUES (6415, 6415, 465, 'Siborong-Borong');
INSERT INTO `subdistricts` VALUES (6416, 6416, 465, 'Simangumban');
INSERT INTO `subdistricts` VALUES (6417, 6417, 465, 'Sipahutar');
INSERT INTO `subdistricts` VALUES (6418, 6418, 465, 'Sipoholon');
INSERT INTO `subdistricts` VALUES (6419, 6419, 465, 'Tarutung');
INSERT INTO `subdistricts` VALUES (6420, 6420, 466, 'Bakarangan');
INSERT INTO `subdistricts` VALUES (6421, 6421, 466, 'Binuang');
INSERT INTO `subdistricts` VALUES (6422, 6422, 466, 'Bungur');
INSERT INTO `subdistricts` VALUES (6423, 6423, 466, 'Candi Laras Selatan');
INSERT INTO `subdistricts` VALUES (6424, 6424, 466, 'Candi Laras Utara');
INSERT INTO `subdistricts` VALUES (6425, 6425, 466, 'Hatungun');
INSERT INTO `subdistricts` VALUES (6426, 6426, 466, 'Lokpaikat');
INSERT INTO `subdistricts` VALUES (6427, 6427, 466, 'Piani');
INSERT INTO `subdistricts` VALUES (6428, 6428, 466, 'Salam Babaris');
INSERT INTO `subdistricts` VALUES (6429, 6429, 466, 'Tapin Selatan');
INSERT INTO `subdistricts` VALUES (6430, 6430, 466, 'Tapin Tengah');
INSERT INTO `subdistricts` VALUES (6431, 6431, 466, 'Tapin Utara');
INSERT INTO `subdistricts` VALUES (6432, 6432, 467, 'Tarakan Barat');
INSERT INTO `subdistricts` VALUES (6433, 6433, 467, 'Tarakan Tengah');
INSERT INTO `subdistricts` VALUES (6434, 6434, 467, 'Tarakan Timur');
INSERT INTO `subdistricts` VALUES (6435, 6435, 467, 'Tarakan Utara');
INSERT INTO `subdistricts` VALUES (6436, 6436, 468, 'Bantarkalong');
INSERT INTO `subdistricts` VALUES (6437, 6437, 468, 'Bojongasih');
INSERT INTO `subdistricts` VALUES (6438, 6438, 468, 'Bojonggambir');
INSERT INTO `subdistricts` VALUES (6439, 6439, 468, 'Ciawi');
INSERT INTO `subdistricts` VALUES (6440, 6440, 468, 'Cibalong');
INSERT INTO `subdistricts` VALUES (6441, 6441, 468, 'Cigalontang');
INSERT INTO `subdistricts` VALUES (6442, 6442, 468, 'Cikalong');
INSERT INTO `subdistricts` VALUES (6443, 6443, 468, 'Cikatomas');
INSERT INTO `subdistricts` VALUES (6444, 6444, 468, 'Cineam');
INSERT INTO `subdistricts` VALUES (6445, 6445, 468, 'Cipatujah');
INSERT INTO `subdistricts` VALUES (6446, 6446, 468, 'Cisayong');
INSERT INTO `subdistricts` VALUES (6447, 6447, 468, 'Culamega');
INSERT INTO `subdistricts` VALUES (6448, 6448, 468, 'Gunung Tanjung');
INSERT INTO `subdistricts` VALUES (6449, 6449, 468, 'Jamanis');
INSERT INTO `subdistricts` VALUES (6450, 6450, 468, 'Jatiwaras');
INSERT INTO `subdistricts` VALUES (6451, 6451, 468, 'Kadipaten');
INSERT INTO `subdistricts` VALUES (6452, 6452, 468, 'Karang Jaya');
INSERT INTO `subdistricts` VALUES (6453, 6453, 468, 'Karangnunggal');
INSERT INTO `subdistricts` VALUES (6454, 6454, 468, 'Leuwisari');
INSERT INTO `subdistricts` VALUES (6455, 6455, 468, 'Mangunreja');
INSERT INTO `subdistricts` VALUES (6456, 6456, 468, 'Manonjaya');
INSERT INTO `subdistricts` VALUES (6457, 6457, 468, 'Padakembang');
INSERT INTO `subdistricts` VALUES (6458, 6458, 468, 'Pagerageung');
INSERT INTO `subdistricts` VALUES (6459, 6459, 468, 'Pancatengah');
INSERT INTO `subdistricts` VALUES (6460, 6460, 468, 'Parungponteng');
INSERT INTO `subdistricts` VALUES (6461, 6461, 468, 'Puspahiang');
INSERT INTO `subdistricts` VALUES (6462, 6462, 468, 'Rajapolah');
INSERT INTO `subdistricts` VALUES (6463, 6463, 468, 'Salawu');
INSERT INTO `subdistricts` VALUES (6464, 6464, 468, 'Salopa');
INSERT INTO `subdistricts` VALUES (6465, 6465, 468, 'Sariwangi');
INSERT INTO `subdistricts` VALUES (6466, 6466, 468, 'Singaparna');
INSERT INTO `subdistricts` VALUES (6467, 6467, 468, 'Sodonghilir');
INSERT INTO `subdistricts` VALUES (6468, 6468, 468, 'Sukahening');
INSERT INTO `subdistricts` VALUES (6469, 6469, 468, 'Sukaraja');
INSERT INTO `subdistricts` VALUES (6470, 6470, 468, 'Sukarame');
INSERT INTO `subdistricts` VALUES (6471, 6471, 468, 'Sukaratu');
INSERT INTO `subdistricts` VALUES (6472, 6472, 468, 'Sukaresik');
INSERT INTO `subdistricts` VALUES (6473, 6473, 468, 'Tanjungjaya');
INSERT INTO `subdistricts` VALUES (6474, 6474, 468, 'Taraju');
INSERT INTO `subdistricts` VALUES (6475, 6475, 469, 'Bungursari');
INSERT INTO `subdistricts` VALUES (6476, 6476, 469, 'Cibeureum');
INSERT INTO `subdistricts` VALUES (6477, 6477, 469, 'Cihideung');
INSERT INTO `subdistricts` VALUES (6478, 6478, 469, 'Cipedes');
INSERT INTO `subdistricts` VALUES (6479, 6479, 469, 'Indihiang');
INSERT INTO `subdistricts` VALUES (6480, 6480, 469, 'Kawalu');
INSERT INTO `subdistricts` VALUES (6481, 6481, 469, 'Mangkubumi');
INSERT INTO `subdistricts` VALUES (6482, 6482, 469, 'Purbaratu');
INSERT INTO `subdistricts` VALUES (6483, 6483, 469, 'Tamansari');
INSERT INTO `subdistricts` VALUES (6484, 6484, 469, 'Tawang');
INSERT INTO `subdistricts` VALUES (6485, 6485, 470, 'Bajenis');
INSERT INTO `subdistricts` VALUES (6486, 6486, 470, 'Padang Hilir');
INSERT INTO `subdistricts` VALUES (6487, 6487, 470, 'Padang Hulu');
INSERT INTO `subdistricts` VALUES (6488, 6488, 470, 'Rambutan');
INSERT INTO `subdistricts` VALUES (6489, 6489, 470, 'Tebing Tinggi Kota');
INSERT INTO `subdistricts` VALUES (6490, 6490, 471, 'Muara Tabir');
INSERT INTO `subdistricts` VALUES (6491, 6491, 471, 'Rimbo Bujang');
INSERT INTO `subdistricts` VALUES (6492, 6492, 471, 'Rimbo Ilir');
INSERT INTO `subdistricts` VALUES (6493, 6493, 471, 'Rimbo Ulu');
INSERT INTO `subdistricts` VALUES (6494, 6494, 471, 'Serai Serumpun');
INSERT INTO `subdistricts` VALUES (6495, 6495, 471, 'Sumay');
INSERT INTO `subdistricts` VALUES (6496, 6496, 471, 'Tebo Ilir');
INSERT INTO `subdistricts` VALUES (6497, 6497, 471, 'Tebo Tengah');
INSERT INTO `subdistricts` VALUES (6498, 6498, 471, 'Tebo Ulu');
INSERT INTO `subdistricts` VALUES (6499, 6499, 471, 'Tengah Ilir');
INSERT INTO `subdistricts` VALUES (6500, 6500, 471, 'VII Koto');
INSERT INTO `subdistricts` VALUES (6501, 6501, 471, 'VII Koto Ilir');
INSERT INTO `subdistricts` VALUES (6502, 6502, 472, 'Adiwerna');
INSERT INTO `subdistricts` VALUES (6503, 6503, 472, 'Balapulang');
INSERT INTO `subdistricts` VALUES (6504, 6504, 472, 'Bojong');
INSERT INTO `subdistricts` VALUES (6505, 6505, 472, 'Bumijawa');
INSERT INTO `subdistricts` VALUES (6506, 6506, 472, 'Dukuhturi');
INSERT INTO `subdistricts` VALUES (6507, 6507, 472, 'Dukuhwaru');
INSERT INTO `subdistricts` VALUES (6508, 6508, 472, 'Jatinegara');
INSERT INTO `subdistricts` VALUES (6509, 6509, 472, 'Kedung Banteng');
INSERT INTO `subdistricts` VALUES (6510, 6510, 472, 'Kramat');
INSERT INTO `subdistricts` VALUES (6511, 6511, 472, 'Lebaksiu');
INSERT INTO `subdistricts` VALUES (6512, 6512, 472, 'Margasari');
INSERT INTO `subdistricts` VALUES (6513, 6513, 472, 'Pagerbarang');
INSERT INTO `subdistricts` VALUES (6514, 6514, 472, 'Pangkah');
INSERT INTO `subdistricts` VALUES (6515, 6515, 472, 'Slawi');
INSERT INTO `subdistricts` VALUES (6516, 6516, 472, 'Surodadi');
INSERT INTO `subdistricts` VALUES (6517, 6517, 472, 'Talang');
INSERT INTO `subdistricts` VALUES (6518, 6518, 472, 'Tarub');
INSERT INTO `subdistricts` VALUES (6519, 6519, 472, 'Warurejo');
INSERT INTO `subdistricts` VALUES (6520, 6520, 473, 'Margadana');
INSERT INTO `subdistricts` VALUES (6521, 6521, 473, 'Tegal Barat');
INSERT INTO `subdistricts` VALUES (6522, 6522, 473, 'Tegal Selatan');
INSERT INTO `subdistricts` VALUES (6523, 6523, 473, 'Tegal Timur');
INSERT INTO `subdistricts` VALUES (6524, 6524, 474, 'Aranday');
INSERT INTO `subdistricts` VALUES (6525, 6525, 474, 'Aroba');
INSERT INTO `subdistricts` VALUES (6526, 6526, 474, 'Babo');
INSERT INTO `subdistricts` VALUES (6527, 6527, 474, 'Bintuni');
INSERT INTO `subdistricts` VALUES (6528, 6528, 474, 'Biscoop');
INSERT INTO `subdistricts` VALUES (6529, 6529, 474, 'Dataran Beimes');
INSERT INTO `subdistricts` VALUES (6530, 6530, 474, 'Fafurwar (Irorutu)');
INSERT INTO `subdistricts` VALUES (6531, 6531, 474, 'Kaitaro');
INSERT INTO `subdistricts` VALUES (6532, 6532, 474, 'Kamundan');
INSERT INTO `subdistricts` VALUES (6533, 6533, 474, 'Kuri');
INSERT INTO `subdistricts` VALUES (6534, 6534, 474, 'Manimeri');
INSERT INTO `subdistricts` VALUES (6535, 6535, 474, 'Masyeta');
INSERT INTO `subdistricts` VALUES (6536, 6536, 474, 'Mayado');
INSERT INTO `subdistricts` VALUES (6537, 6537, 474, 'Merdey');
INSERT INTO `subdistricts` VALUES (6538, 6538, 474, 'Moskona Barat');
INSERT INTO `subdistricts` VALUES (6539, 6539, 474, 'Moskona Selatan');
INSERT INTO `subdistricts` VALUES (6540, 6540, 474, 'Moskona Timur');
INSERT INTO `subdistricts` VALUES (6541, 6541, 474, 'Moskona Utara');
INSERT INTO `subdistricts` VALUES (6542, 6542, 474, 'Sumuri (Simuri)');
INSERT INTO `subdistricts` VALUES (6543, 6543, 474, 'Tembuni');
INSERT INTO `subdistricts` VALUES (6544, 6544, 474, 'Tomu');
INSERT INTO `subdistricts` VALUES (6545, 6545, 474, 'Tuhiba');
INSERT INTO `subdistricts` VALUES (6546, 6546, 474, 'Wamesa (Idoor)');
INSERT INTO `subdistricts` VALUES (6547, 6547, 474, 'Weriagar');
INSERT INTO `subdistricts` VALUES (6548, 6548, 475, 'Kuri Wamesa');
INSERT INTO `subdistricts` VALUES (6549, 6549, 475, 'Naikere (Wasior Barat)');
INSERT INTO `subdistricts` VALUES (6550, 6550, 475, 'Nikiwar');
INSERT INTO `subdistricts` VALUES (6551, 6551, 475, 'Rasiei');
INSERT INTO `subdistricts` VALUES (6552, 6552, 475, 'Roon');
INSERT INTO `subdistricts` VALUES (6553, 6553, 475, 'Roswar');
INSERT INTO `subdistricts` VALUES (6554, 6554, 475, 'Rumberpon');
INSERT INTO `subdistricts` VALUES (6555, 6555, 475, 'Soug Jaya');
INSERT INTO `subdistricts` VALUES (6556, 6556, 475, 'Teluk Duairi (Wasior Utara)');
INSERT INTO `subdistricts` VALUES (6557, 6557, 475, 'Wamesa');
INSERT INTO `subdistricts` VALUES (6558, 6558, 475, 'Wasior');
INSERT INTO `subdistricts` VALUES (6559, 6559, 475, 'Windesi');
INSERT INTO `subdistricts` VALUES (6560, 6560, 475, 'Wondiboy (Wasior Selatan)');
INSERT INTO `subdistricts` VALUES (6561, 6561, 476, 'Bansari');
INSERT INTO `subdistricts` VALUES (6562, 6562, 476, 'Bejen');
INSERT INTO `subdistricts` VALUES (6563, 6563, 476, 'Bulu');
INSERT INTO `subdistricts` VALUES (6564, 6564, 476, 'Candiroto');
INSERT INTO `subdistricts` VALUES (6565, 6565, 476, 'Gemawang');
INSERT INTO `subdistricts` VALUES (6566, 6566, 476, 'Jumo');
INSERT INTO `subdistricts` VALUES (6567, 6567, 476, 'Kaloran');
INSERT INTO `subdistricts` VALUES (6568, 6568, 476, 'Kandangan');
INSERT INTO `subdistricts` VALUES (6569, 6569, 476, 'Kedu');
INSERT INTO `subdistricts` VALUES (6570, 6570, 476, 'Kledung');
INSERT INTO `subdistricts` VALUES (6571, 6571, 476, 'Kranggan');
INSERT INTO `subdistricts` VALUES (6572, 6572, 476, 'Ngadirejo');
INSERT INTO `subdistricts` VALUES (6573, 6573, 476, 'Parakan');
INSERT INTO `subdistricts` VALUES (6574, 6574, 476, 'Pringsurat');
INSERT INTO `subdistricts` VALUES (6575, 6575, 476, 'Selopampang');
INSERT INTO `subdistricts` VALUES (6576, 6576, 476, 'Temanggung');
INSERT INTO `subdistricts` VALUES (6577, 6577, 476, 'Tembarak');
INSERT INTO `subdistricts` VALUES (6578, 6578, 476, 'Tlogomulyo');
INSERT INTO `subdistricts` VALUES (6579, 6579, 476, 'Tretep');
INSERT INTO `subdistricts` VALUES (6580, 6580, 476, 'Wonoboyo');
INSERT INTO `subdistricts` VALUES (6581, 6581, 477, 'Moti (Pulau Moti)');
INSERT INTO `subdistricts` VALUES (6582, 6582, 477, 'Pulau Batang Dua');
INSERT INTO `subdistricts` VALUES (6583, 6583, 477, 'Pulau Hiri');
INSERT INTO `subdistricts` VALUES (6584, 6584, 477, 'Pulau Ternate');
INSERT INTO `subdistricts` VALUES (6585, 6585, 477, 'Ternate Selatan (Kota)');
INSERT INTO `subdistricts` VALUES (6586, 6586, 477, 'Ternate Tengah (Kota)');
INSERT INTO `subdistricts` VALUES (6587, 6587, 477, 'Ternate Utara (Kota)');
INSERT INTO `subdistricts` VALUES (6588, 6588, 478, 'Oba');
INSERT INTO `subdistricts` VALUES (6589, 6589, 478, 'Oba Selatan');
INSERT INTO `subdistricts` VALUES (6590, 6590, 478, 'Oba Tengah');
INSERT INTO `subdistricts` VALUES (6591, 6591, 478, 'Oba Utara');
INSERT INTO `subdistricts` VALUES (6592, 6592, 478, 'Tidore (Pulau Tidore)');
INSERT INTO `subdistricts` VALUES (6593, 6593, 478, 'Tidore Selatan');
INSERT INTO `subdistricts` VALUES (6594, 6594, 478, 'Tidore Timur (Pulau Tidore)');
INSERT INTO `subdistricts` VALUES (6595, 6595, 478, 'Tidore Utara');
INSERT INTO `subdistricts` VALUES (6596, 6596, 479, 'Amanatun Selatan');
INSERT INTO `subdistricts` VALUES (6597, 6597, 479, 'Amanatun Utara');
INSERT INTO `subdistricts` VALUES (6598, 6598, 479, 'Amanuban Barat');
INSERT INTO `subdistricts` VALUES (6599, 6599, 479, 'Amanuban Selatan');
INSERT INTO `subdistricts` VALUES (6600, 6600, 479, 'Amanuban Tengah');
INSERT INTO `subdistricts` VALUES (6601, 6601, 479, 'Amanuban Timur');
INSERT INTO `subdistricts` VALUES (6602, 6602, 479, 'Batu Putih');
INSERT INTO `subdistricts` VALUES (6603, 6603, 479, 'Boking');
INSERT INTO `subdistricts` VALUES (6604, 6604, 479, 'Fatukopa');
INSERT INTO `subdistricts` VALUES (6605, 6605, 479, 'Fatumnasi');
INSERT INTO `subdistricts` VALUES (6606, 6606, 479, 'Fautmolo');
INSERT INTO `subdistricts` VALUES (6607, 6607, 479, 'Kie (Ki\'e)');
INSERT INTO `subdistricts` VALUES (6608, 6608, 479, 'Kok Baun');
INSERT INTO `subdistricts` VALUES (6609, 6609, 479, 'Kolbano');
INSERT INTO `subdistricts` VALUES (6610, 6610, 479, 'Kot Olin');
INSERT INTO `subdistricts` VALUES (6611, 6611, 479, 'Kota Soe');
INSERT INTO `subdistricts` VALUES (6612, 6612, 479, 'Kualin');
INSERT INTO `subdistricts` VALUES (6613, 6613, 479, 'Kuanfatu');
INSERT INTO `subdistricts` VALUES (6614, 6614, 479, 'Kuatnana');
INSERT INTO `subdistricts` VALUES (6615, 6615, 479, 'Mollo Barat');
INSERT INTO `subdistricts` VALUES (6616, 6616, 479, 'Mollo Selatan');
INSERT INTO `subdistricts` VALUES (6617, 6617, 479, 'Mollo Tengah');
INSERT INTO `subdistricts` VALUES (6618, 6618, 479, 'Mollo Utara');
INSERT INTO `subdistricts` VALUES (6619, 6619, 479, 'Noebana');
INSERT INTO `subdistricts` VALUES (6620, 6620, 479, 'Noebeba');
INSERT INTO `subdistricts` VALUES (6621, 6621, 479, 'Nunbena');
INSERT INTO `subdistricts` VALUES (6622, 6622, 479, 'Nunkolo');
INSERT INTO `subdistricts` VALUES (6623, 6623, 479, 'Oenino');
INSERT INTO `subdistricts` VALUES (6624, 6624, 479, 'Polen');
INSERT INTO `subdistricts` VALUES (6625, 6625, 479, 'Santian');
INSERT INTO `subdistricts` VALUES (6626, 6626, 479, 'Tobu');
INSERT INTO `subdistricts` VALUES (6627, 6627, 479, 'Toianas');
INSERT INTO `subdistricts` VALUES (6628, 6628, 480, 'Biboki Anleu');
INSERT INTO `subdistricts` VALUES (6629, 6629, 480, 'Biboki Feotleu');
INSERT INTO `subdistricts` VALUES (6630, 6630, 480, 'Biboki Moenleu');
INSERT INTO `subdistricts` VALUES (6631, 6631, 480, 'Biboki Selatan');
INSERT INTO `subdistricts` VALUES (6632, 6632, 480, 'Biboki Tan Pah');
INSERT INTO `subdistricts` VALUES (6633, 6633, 480, 'Biboki Utara');
INSERT INTO `subdistricts` VALUES (6634, 6634, 480, 'Bikomi Nilulat');
INSERT INTO `subdistricts` VALUES (6635, 6635, 480, 'Bikomi Selatan');
INSERT INTO `subdistricts` VALUES (6636, 6636, 480, 'Bikomi Tengah');
INSERT INTO `subdistricts` VALUES (6637, 6637, 480, 'Bikomi Utara');
INSERT INTO `subdistricts` VALUES (6638, 6638, 480, 'Insana');
INSERT INTO `subdistricts` VALUES (6639, 6639, 480, 'Insana Barat');
INSERT INTO `subdistricts` VALUES (6640, 6640, 480, 'Insana Fafinesu');
INSERT INTO `subdistricts` VALUES (6641, 6641, 480, 'Insana Tengah');
INSERT INTO `subdistricts` VALUES (6642, 6642, 480, 'Insana Utara');
INSERT INTO `subdistricts` VALUES (6643, 6643, 480, 'Kota Kefamenanu');
INSERT INTO `subdistricts` VALUES (6644, 6644, 480, 'Miomaffo Barat');
INSERT INTO `subdistricts` VALUES (6645, 6645, 480, 'Miomaffo Tengah');
INSERT INTO `subdistricts` VALUES (6646, 6646, 480, 'Miomaffo Timur');
INSERT INTO `subdistricts` VALUES (6647, 6647, 480, 'Musi');
INSERT INTO `subdistricts` VALUES (6648, 6648, 480, 'Mutis');
INSERT INTO `subdistricts` VALUES (6649, 6649, 480, 'Naibenu');
INSERT INTO `subdistricts` VALUES (6650, 6650, 480, 'Noemuti');
INSERT INTO `subdistricts` VALUES (6651, 6651, 480, 'Noemuti Timur');
INSERT INTO `subdistricts` VALUES (6652, 6652, 481, 'Ajibata');
INSERT INTO `subdistricts` VALUES (6653, 6653, 481, 'Balige');
INSERT INTO `subdistricts` VALUES (6654, 6654, 481, 'Bonatua Lunasi');
INSERT INTO `subdistricts` VALUES (6655, 6655, 481, 'Bor-Bor');
INSERT INTO `subdistricts` VALUES (6656, 6656, 481, 'Habinsaran');
INSERT INTO `subdistricts` VALUES (6657, 6657, 481, 'Laguboti');
INSERT INTO `subdistricts` VALUES (6658, 6658, 481, 'Lumban Julu');
INSERT INTO `subdistricts` VALUES (6659, 6659, 481, 'Nassau');
INSERT INTO `subdistricts` VALUES (6660, 6660, 481, 'Parmaksian');
INSERT INTO `subdistricts` VALUES (6661, 6661, 481, 'Pintu Pohan Meranti');
INSERT INTO `subdistricts` VALUES (6662, 6662, 481, 'Porsea');
INSERT INTO `subdistricts` VALUES (6663, 6663, 481, 'Siantar Narumonda');
INSERT INTO `subdistricts` VALUES (6664, 6664, 481, 'Sigumpar');
INSERT INTO `subdistricts` VALUES (6665, 6665, 481, 'Silaen');
INSERT INTO `subdistricts` VALUES (6666, 6666, 481, 'Tampahan');
INSERT INTO `subdistricts` VALUES (6667, 6667, 481, 'Uluan');
INSERT INTO `subdistricts` VALUES (6668, 6668, 482, 'Ampana Kota');
INSERT INTO `subdistricts` VALUES (6669, 6669, 482, 'Ampana Tete');
INSERT INTO `subdistricts` VALUES (6670, 6670, 482, 'Togean');
INSERT INTO `subdistricts` VALUES (6671, 6671, 482, 'Tojo');
INSERT INTO `subdistricts` VALUES (6672, 6672, 482, 'Tojo Barat');
INSERT INTO `subdistricts` VALUES (6673, 6673, 482, 'Ulu Bongka');
INSERT INTO `subdistricts` VALUES (6674, 6674, 482, 'Una - Una');
INSERT INTO `subdistricts` VALUES (6675, 6675, 482, 'Walea Besar');
INSERT INTO `subdistricts` VALUES (6676, 6676, 482, 'Walea Kepulauan');
INSERT INTO `subdistricts` VALUES (6677, 6677, 483, 'Baolan');
INSERT INTO `subdistricts` VALUES (6678, 6678, 483, 'Basidondo');
INSERT INTO `subdistricts` VALUES (6679, 6679, 483, 'Dako Pamean');
INSERT INTO `subdistricts` VALUES (6680, 6680, 483, 'Dampal Selatan');
INSERT INTO `subdistricts` VALUES (6681, 6681, 483, 'Dampal Utara');
INSERT INTO `subdistricts` VALUES (6682, 6682, 483, 'Dondo');
INSERT INTO `subdistricts` VALUES (6683, 6683, 483, 'Galang');
INSERT INTO `subdistricts` VALUES (6684, 6684, 483, 'Lampasio');
INSERT INTO `subdistricts` VALUES (6685, 6685, 483, 'Ogo Deide');
INSERT INTO `subdistricts` VALUES (6686, 6686, 483, 'Tolitoli Utara');
INSERT INTO `subdistricts` VALUES (6687, 6687, 484, 'Airgaram');
INSERT INTO `subdistricts` VALUES (6688, 6688, 484, 'Anawi');
INSERT INTO `subdistricts` VALUES (6689, 6689, 484, 'Aweku');
INSERT INTO `subdistricts` VALUES (6690, 6690, 484, 'Bewani');
INSERT INTO `subdistricts` VALUES (6691, 6691, 484, 'Biuk');
INSERT INTO `subdistricts` VALUES (6692, 6692, 484, 'Bogonuk');
INSERT INTO `subdistricts` VALUES (6693, 6693, 484, 'Bokondini');
INSERT INTO `subdistricts` VALUES (6694, 6694, 484, 'Bokoneri');
INSERT INTO `subdistricts` VALUES (6695, 6695, 484, 'Danime');
INSERT INTO `subdistricts` VALUES (6696, 6696, 484, 'Dow');
INSERT INTO `subdistricts` VALUES (6697, 6697, 484, 'Dundu (Ndundu)');
INSERT INTO `subdistricts` VALUES (6698, 6698, 484, 'Egiam');
INSERT INTO `subdistricts` VALUES (6699, 6699, 484, 'Geya');
INSERT INTO `subdistricts` VALUES (6700, 6700, 484, 'Gika');
INSERT INTO `subdistricts` VALUES (6701, 6701, 484, 'Gilubandu (Gilumbandu/Gilimbandu)');
INSERT INTO `subdistricts` VALUES (6702, 6702, 484, 'Goyage');
INSERT INTO `subdistricts` VALUES (6703, 6703, 484, 'Gundagi (Gudage)');
INSERT INTO `subdistricts` VALUES (6704, 6704, 484, 'Kai');
INSERT INTO `subdistricts` VALUES (6705, 6705, 484, 'Kamboneri');
INSERT INTO `subdistricts` VALUES (6706, 6706, 484, 'Kanggime (Kanggima )');
INSERT INTO `subdistricts` VALUES (6707, 6707, 484, 'Karubaga');
INSERT INTO `subdistricts` VALUES (6708, 6708, 484, 'Kembu');
INSERT INTO `subdistricts` VALUES (6709, 6709, 484, 'Kondaga (Konda)');
INSERT INTO `subdistricts` VALUES (6710, 6710, 484, 'Kuari');
INSERT INTO `subdistricts` VALUES (6711, 6711, 484, 'Kubu');
INSERT INTO `subdistricts` VALUES (6712, 6712, 484, 'Li Anogomma');
INSERT INTO `subdistricts` VALUES (6713, 6713, 484, 'Nabunage');
INSERT INTO `subdistricts` VALUES (6714, 6714, 484, 'Nelawi');
INSERT INTO `subdistricts` VALUES (6715, 6715, 484, 'Numba');
INSERT INTO `subdistricts` VALUES (6716, 6716, 484, 'Nunggawi (Munggawi)');
INSERT INTO `subdistricts` VALUES (6717, 6717, 484, 'Panaga');
INSERT INTO `subdistricts` VALUES (6718, 6718, 484, 'Poganeri');
INSERT INTO `subdistricts` VALUES (6719, 6719, 484, 'Tagime');
INSERT INTO `subdistricts` VALUES (6720, 6720, 484, 'Tagineri');
INSERT INTO `subdistricts` VALUES (6721, 6721, 484, 'Telenggeme');
INSERT INTO `subdistricts` VALUES (6722, 6722, 484, 'Timori');
INSERT INTO `subdistricts` VALUES (6723, 6723, 484, 'Umagi');
INSERT INTO `subdistricts` VALUES (6724, 6724, 484, 'Wakuwo');
INSERT INTO `subdistricts` VALUES (6725, 6725, 484, 'Wari (Taiyeve)');
INSERT INTO `subdistricts` VALUES (6726, 6726, 484, 'Wenam');
INSERT INTO `subdistricts` VALUES (6727, 6727, 484, 'Wina');
INSERT INTO `subdistricts` VALUES (6728, 6728, 484, 'Wonoki (Woniki)');
INSERT INTO `subdistricts` VALUES (6729, 6729, 484, 'Wugi');
INSERT INTO `subdistricts` VALUES (6730, 6730, 484, 'Wunin (Wumin)');
INSERT INTO `subdistricts` VALUES (6731, 6731, 484, 'Yuko');
INSERT INTO `subdistricts` VALUES (6732, 6732, 484, 'Yuneri');
INSERT INTO `subdistricts` VALUES (6733, 6733, 485, 'Tomohon Barat');
INSERT INTO `subdistricts` VALUES (6734, 6734, 485, 'Tomohon Selatan');
INSERT INTO `subdistricts` VALUES (6735, 6735, 485, 'Tomohon Tengah');
INSERT INTO `subdistricts` VALUES (6736, 6736, 485, 'Tomohon Timur');
INSERT INTO `subdistricts` VALUES (6737, 6737, 485, 'Tomohon Utara');
INSERT INTO `subdistricts` VALUES (6738, 6738, 486, 'Awan Rante Karua');
INSERT INTO `subdistricts` VALUES (6739, 6739, 486, 'Balusu');
INSERT INTO `subdistricts` VALUES (6740, 6740, 486, 'Bangkelekila');
INSERT INTO `subdistricts` VALUES (6741, 6741, 486, 'Baruppu');
INSERT INTO `subdistricts` VALUES (6742, 6742, 486, 'Buntao');
INSERT INTO `subdistricts` VALUES (6743, 6743, 486, 'Buntu Pepasan');
INSERT INTO `subdistricts` VALUES (6744, 6744, 486, 'Dende\' Piongan Napo');
INSERT INTO `subdistricts` VALUES (6745, 6745, 486, 'Kapalla Pitu (Kapala Pitu)');
INSERT INTO `subdistricts` VALUES (6746, 6746, 486, 'Kesu');
INSERT INTO `subdistricts` VALUES (6747, 6747, 486, 'Nanggala');
INSERT INTO `subdistricts` VALUES (6748, 6748, 486, 'Rantebua');
INSERT INTO `subdistricts` VALUES (6749, 6749, 486, 'Rantepao');
INSERT INTO `subdistricts` VALUES (6750, 6750, 486, 'Rindingallo');
INSERT INTO `subdistricts` VALUES (6751, 6751, 486, 'Sa\'dan');
INSERT INTO `subdistricts` VALUES (6752, 6752, 486, 'Sanggalangi');
INSERT INTO `subdistricts` VALUES (6753, 6753, 486, 'Sesean');
INSERT INTO `subdistricts` VALUES (6754, 6754, 486, 'Sesean Suloara');
INSERT INTO `subdistricts` VALUES (6755, 6755, 486, 'Sopai');
INSERT INTO `subdistricts` VALUES (6756, 6756, 486, 'Tallunglipu');
INSERT INTO `subdistricts` VALUES (6757, 6757, 486, 'Tikala');
INSERT INTO `subdistricts` VALUES (6758, 6758, 486, 'Tondon');
INSERT INTO `subdistricts` VALUES (6759, 6759, 487, 'Bendungan');
INSERT INTO `subdistricts` VALUES (6760, 6760, 487, 'Dongko');
INSERT INTO `subdistricts` VALUES (6761, 6761, 487, 'Durenan');
INSERT INTO `subdistricts` VALUES (6762, 6762, 487, 'Gandusari');
INSERT INTO `subdistricts` VALUES (6763, 6763, 487, 'Kampak');
INSERT INTO `subdistricts` VALUES (6764, 6764, 487, 'Karangan');
INSERT INTO `subdistricts` VALUES (6765, 6765, 487, 'Munjungan');
INSERT INTO `subdistricts` VALUES (6766, 6766, 487, 'Panggul');
INSERT INTO `subdistricts` VALUES (6767, 6767, 487, 'Pogalan');
INSERT INTO `subdistricts` VALUES (6768, 6768, 487, 'Pule');
INSERT INTO `subdistricts` VALUES (6769, 6769, 487, 'Suruh');
INSERT INTO `subdistricts` VALUES (6770, 6770, 487, 'Trenggalek');
INSERT INTO `subdistricts` VALUES (6771, 6771, 487, 'Tugu');
INSERT INTO `subdistricts` VALUES (6772, 6772, 487, 'Watulimo');
INSERT INTO `subdistricts` VALUES (6773, 6773, 488, 'Kur Selatan');
INSERT INTO `subdistricts` VALUES (6774, 6774, 488, 'Pulau Dullah Selatan');
INSERT INTO `subdistricts` VALUES (6775, 6775, 488, 'Pulau Dullah Utara');
INSERT INTO `subdistricts` VALUES (6776, 6776, 488, 'Pulau Tayando Tam');
INSERT INTO `subdistricts` VALUES (6777, 6777, 488, 'Pulau-Pulau Kur');
INSERT INTO `subdistricts` VALUES (6778, 6778, 489, 'Bancar');
INSERT INTO `subdistricts` VALUES (6779, 6779, 489, 'Bangilan');
INSERT INTO `subdistricts` VALUES (6780, 6780, 489, 'Grabagan');
INSERT INTO `subdistricts` VALUES (6781, 6781, 489, 'Jatirogo');
INSERT INTO `subdistricts` VALUES (6782, 6782, 489, 'Jenu');
INSERT INTO `subdistricts` VALUES (6783, 6783, 489, 'Kenduruan');
INSERT INTO `subdistricts` VALUES (6784, 6784, 489, 'Kerek');
INSERT INTO `subdistricts` VALUES (6785, 6785, 489, 'Merakurak');
INSERT INTO `subdistricts` VALUES (6786, 6786, 489, 'Montong');
INSERT INTO `subdistricts` VALUES (6787, 6787, 489, 'Palang');
INSERT INTO `subdistricts` VALUES (6788, 6788, 489, 'Parengan');
INSERT INTO `subdistricts` VALUES (6789, 6789, 489, 'Plumpang');
INSERT INTO `subdistricts` VALUES (6790, 6790, 489, 'Rengel');
INSERT INTO `subdistricts` VALUES (6791, 6791, 489, 'Semanding');
INSERT INTO `subdistricts` VALUES (6792, 6792, 489, 'Senori');
INSERT INTO `subdistricts` VALUES (6793, 6793, 489, 'Singgahan');
INSERT INTO `subdistricts` VALUES (6794, 6794, 489, 'Soko');
INSERT INTO `subdistricts` VALUES (6795, 6795, 489, 'Tambakboyo');
INSERT INTO `subdistricts` VALUES (6796, 6796, 489, 'Tuban');
INSERT INTO `subdistricts` VALUES (6797, 6797, 489, 'Widang');
INSERT INTO `subdistricts` VALUES (6798, 6798, 490, 'Banjar Agung');
INSERT INTO `subdistricts` VALUES (6799, 6799, 490, 'Banjar Baru');
INSERT INTO `subdistricts` VALUES (6800, 6800, 490, 'Banjar Margo');
INSERT INTO `subdistricts` VALUES (6801, 6801, 490, 'Dente Teladas');
INSERT INTO `subdistricts` VALUES (6802, 6802, 490, 'Gedung Aji');
INSERT INTO `subdistricts` VALUES (6803, 6803, 490, 'Gedung Aji Baru');
INSERT INTO `subdistricts` VALUES (6804, 6804, 490, 'Gedung Meneng');
INSERT INTO `subdistricts` VALUES (6805, 6805, 490, 'Menggala');
INSERT INTO `subdistricts` VALUES (6806, 6806, 490, 'Menggala Timur');
INSERT INTO `subdistricts` VALUES (6807, 6807, 490, 'Meraksa Aji');
INSERT INTO `subdistricts` VALUES (6808, 6808, 490, 'Penawar Aji');
INSERT INTO `subdistricts` VALUES (6809, 6809, 490, 'Penawar Tama');
INSERT INTO `subdistricts` VALUES (6810, 6810, 490, 'Rawa Pitu');
INSERT INTO `subdistricts` VALUES (6811, 6811, 490, 'Rawajitu Selatan');
INSERT INTO `subdistricts` VALUES (6812, 6812, 490, 'Rawajitu Timur');
INSERT INTO `subdistricts` VALUES (6813, 6813, 491, 'Gunung Agung');
INSERT INTO `subdistricts` VALUES (6814, 6814, 491, 'Gunung Terang');
INSERT INTO `subdistricts` VALUES (6815, 6815, 491, 'Lambu Kibang');
INSERT INTO `subdistricts` VALUES (6816, 6816, 491, 'Pagar Dewa');
INSERT INTO `subdistricts` VALUES (6817, 6817, 491, 'Tulang Bawang Tengah');
INSERT INTO `subdistricts` VALUES (6818, 6818, 491, 'Tulang Bawang Udik');
INSERT INTO `subdistricts` VALUES (6819, 6819, 491, 'Tumijajar');
INSERT INTO `subdistricts` VALUES (6820, 6820, 491, 'Way Kenanga');
INSERT INTO `subdistricts` VALUES (6821, 6821, 492, 'Bandung');
INSERT INTO `subdistricts` VALUES (6822, 6822, 492, 'Besuki');
INSERT INTO `subdistricts` VALUES (6823, 6823, 492, 'Boyolangu');
INSERT INTO `subdistricts` VALUES (6824, 6824, 492, 'Campur Darat');
INSERT INTO `subdistricts` VALUES (6825, 6825, 492, 'Gondang');
INSERT INTO `subdistricts` VALUES (6826, 6826, 492, 'Kalidawir');
INSERT INTO `subdistricts` VALUES (6827, 6827, 492, 'Karang Rejo');
INSERT INTO `subdistricts` VALUES (6828, 6828, 492, 'Kauman');
INSERT INTO `subdistricts` VALUES (6829, 6829, 492, 'Kedungwaru');
INSERT INTO `subdistricts` VALUES (6830, 6830, 492, 'Ngantru');
INSERT INTO `subdistricts` VALUES (6831, 6831, 492, 'Ngunut');
INSERT INTO `subdistricts` VALUES (6832, 6832, 492, 'Pagerwojo');
INSERT INTO `subdistricts` VALUES (6833, 6833, 492, 'Pakel');
INSERT INTO `subdistricts` VALUES (6834, 6834, 492, 'Pucanglaban');
INSERT INTO `subdistricts` VALUES (6835, 6835, 492, 'Rejotangan');
INSERT INTO `subdistricts` VALUES (6836, 6836, 492, 'Sendang');
INSERT INTO `subdistricts` VALUES (6837, 6837, 492, 'Sumbergempol');
INSERT INTO `subdistricts` VALUES (6838, 6838, 492, 'Tanggung Gunung');
INSERT INTO `subdistricts` VALUES (6839, 6839, 492, 'Tulungagung');
INSERT INTO `subdistricts` VALUES (6840, 6840, 493, 'Belawa');
INSERT INTO `subdistricts` VALUES (6841, 6841, 493, 'Bola');
INSERT INTO `subdistricts` VALUES (6842, 6842, 493, 'Gilireng');
INSERT INTO `subdistricts` VALUES (6843, 6843, 493, 'Keera');
INSERT INTO `subdistricts` VALUES (6844, 6844, 493, 'Majauleng');
INSERT INTO `subdistricts` VALUES (6845, 6845, 493, 'Maniang Pajo');
INSERT INTO `subdistricts` VALUES (6846, 6846, 493, 'Pammana');
INSERT INTO `subdistricts` VALUES (6847, 6847, 493, 'Penrang');
INSERT INTO `subdistricts` VALUES (6848, 6848, 493, 'Pitumpanua');
INSERT INTO `subdistricts` VALUES (6849, 6849, 493, 'Sabbang Paru');
INSERT INTO `subdistricts` VALUES (6850, 6850, 493, 'Sajoanging');
INSERT INTO `subdistricts` VALUES (6851, 6851, 493, 'Takkalalla');
INSERT INTO `subdistricts` VALUES (6852, 6852, 493, 'Tana Sitolo');
INSERT INTO `subdistricts` VALUES (6853, 6853, 493, 'Tempe');
INSERT INTO `subdistricts` VALUES (6854, 6854, 494, 'Binongko');
INSERT INTO `subdistricts` VALUES (6855, 6855, 494, 'Kaledupa');
INSERT INTO `subdistricts` VALUES (6856, 6856, 494, 'Kaledupa Selatan');
INSERT INTO `subdistricts` VALUES (6857, 6857, 494, 'Togo Binongko');
INSERT INTO `subdistricts` VALUES (6858, 6858, 494, 'Tomia');
INSERT INTO `subdistricts` VALUES (6859, 6859, 494, 'Tomia Timur');
INSERT INTO `subdistricts` VALUES (6860, 6860, 494, 'Wangi-Wangi');
INSERT INTO `subdistricts` VALUES (6861, 6861, 494, 'Wangi-Wangi Selatan');
INSERT INTO `subdistricts` VALUES (6862, 6862, 495, 'Demba Masirei');
INSERT INTO `subdistricts` VALUES (6863, 6863, 495, 'Inggerus');
INSERT INTO `subdistricts` VALUES (6864, 6864, 495, 'Kirihi');
INSERT INTO `subdistricts` VALUES (6865, 6865, 495, 'Masirei');
INSERT INTO `subdistricts` VALUES (6866, 6866, 495, 'Oudate Waropen');
INSERT INTO `subdistricts` VALUES (6867, 6867, 495, 'Risei Sayati');
INSERT INTO `subdistricts` VALUES (6868, 6868, 495, 'Ureifasei');
INSERT INTO `subdistricts` VALUES (6869, 6869, 495, 'Wapoga Inggerus');
INSERT INTO `subdistricts` VALUES (6870, 6870, 495, 'Waropen Bawah');
INSERT INTO `subdistricts` VALUES (6871, 6871, 496, 'Bahuga');
INSERT INTO `subdistricts` VALUES (6872, 6872, 496, 'Banjit');
INSERT INTO `subdistricts` VALUES (6873, 6873, 496, 'Baradatu');
INSERT INTO `subdistricts` VALUES (6874, 6874, 496, 'Blambangan Umpu');
INSERT INTO `subdistricts` VALUES (6875, 6875, 496, 'Buay Bahuga');
INSERT INTO `subdistricts` VALUES (6876, 6876, 496, 'Bumi Agung');
INSERT INTO `subdistricts` VALUES (6877, 6877, 496, 'Gunung Labuhan');
INSERT INTO `subdistricts` VALUES (6878, 6878, 496, 'Kasui');
INSERT INTO `subdistricts` VALUES (6879, 6879, 496, 'Negara Batin');
INSERT INTO `subdistricts` VALUES (6880, 6880, 496, 'Negeri Agung');
INSERT INTO `subdistricts` VALUES (6881, 6881, 496, 'Negeri Besar');
INSERT INTO `subdistricts` VALUES (6882, 6882, 496, 'Pakuan Ratu');
INSERT INTO `subdistricts` VALUES (6883, 6883, 496, 'Rebang Tangkas');
INSERT INTO `subdistricts` VALUES (6884, 6884, 496, 'Way Tuba');
INSERT INTO `subdistricts` VALUES (6885, 6885, 497, 'Baturetno');
INSERT INTO `subdistricts` VALUES (6886, 6886, 497, 'Batuwarno');
INSERT INTO `subdistricts` VALUES (6887, 6887, 497, 'Bulukerto');
INSERT INTO `subdistricts` VALUES (6888, 6888, 497, 'Eromoko');
INSERT INTO `subdistricts` VALUES (6889, 6889, 497, 'Girimarto');
INSERT INTO `subdistricts` VALUES (6890, 6890, 497, 'Giritontro');
INSERT INTO `subdistricts` VALUES (6891, 6891, 497, 'Giriwoyo');
INSERT INTO `subdistricts` VALUES (6892, 6892, 497, 'Jatipurno');
INSERT INTO `subdistricts` VALUES (6893, 6893, 497, 'Jatiroto');
INSERT INTO `subdistricts` VALUES (6894, 6894, 497, 'Jatisrono');
INSERT INTO `subdistricts` VALUES (6895, 6895, 497, 'Karangtengah');
INSERT INTO `subdistricts` VALUES (6896, 6896, 497, 'Kismantoro');
INSERT INTO `subdistricts` VALUES (6897, 6897, 497, 'Manyaran');
INSERT INTO `subdistricts` VALUES (6898, 6898, 497, 'Ngadirojo');
INSERT INTO `subdistricts` VALUES (6899, 6899, 497, 'Nguntoronadi');
INSERT INTO `subdistricts` VALUES (6900, 6900, 497, 'Paranggupito');
INSERT INTO `subdistricts` VALUES (6901, 6901, 497, 'Pracimantoro');
INSERT INTO `subdistricts` VALUES (6902, 6902, 497, 'Puhpelem');
INSERT INTO `subdistricts` VALUES (6903, 6903, 497, 'Purwantoro');
INSERT INTO `subdistricts` VALUES (6904, 6904, 497, 'Selogiri');
INSERT INTO `subdistricts` VALUES (6905, 6905, 497, 'Sidoharjo');
INSERT INTO `subdistricts` VALUES (6906, 6906, 497, 'Slogohimo');
INSERT INTO `subdistricts` VALUES (6907, 6907, 497, 'Tirtomoyo');
INSERT INTO `subdistricts` VALUES (6908, 6908, 497, 'Wonogiri');
INSERT INTO `subdistricts` VALUES (6909, 6909, 497, 'Wuryantoro');
INSERT INTO `subdistricts` VALUES (6910, 6910, 498, 'Garung');
INSERT INTO `subdistricts` VALUES (6911, 6911, 498, 'Kalibawang');
INSERT INTO `subdistricts` VALUES (6912, 6912, 498, 'Kalikajar');
INSERT INTO `subdistricts` VALUES (6913, 6913, 498, 'Kaliwiro');
INSERT INTO `subdistricts` VALUES (6914, 6914, 498, 'Kejajar');
INSERT INTO `subdistricts` VALUES (6915, 6915, 498, 'Kepil');
INSERT INTO `subdistricts` VALUES (6916, 6916, 498, 'Kertek');
INSERT INTO `subdistricts` VALUES (6917, 6917, 498, 'Leksono');
INSERT INTO `subdistricts` VALUES (6918, 6918, 498, 'Mojotengah');
INSERT INTO `subdistricts` VALUES (6919, 6919, 498, 'Sapuran');
INSERT INTO `subdistricts` VALUES (6920, 6920, 498, 'Selomerto');
INSERT INTO `subdistricts` VALUES (6921, 6921, 498, 'Sukoharjo');
INSERT INTO `subdistricts` VALUES (6922, 6922, 498, 'Wadaslintang');
INSERT INTO `subdistricts` VALUES (6923, 6923, 498, 'Watumalang');
INSERT INTO `subdistricts` VALUES (6924, 6924, 498, 'Wonosobo');
INSERT INTO `subdistricts` VALUES (6925, 6925, 499, 'Amuma');
INSERT INTO `subdistricts` VALUES (6926, 6926, 499, 'Anggruk');
INSERT INTO `subdistricts` VALUES (6927, 6927, 499, 'Bomela');
INSERT INTO `subdistricts` VALUES (6928, 6928, 499, 'Dekai');
INSERT INTO `subdistricts` VALUES (6929, 6929, 499, 'Dirwemna (Diruwena)');
INSERT INTO `subdistricts` VALUES (6930, 6930, 499, 'Duram');
INSERT INTO `subdistricts` VALUES (6931, 6931, 499, 'Endomen');
INSERT INTO `subdistricts` VALUES (6932, 6932, 499, 'Hereapini (Hereanini)');
INSERT INTO `subdistricts` VALUES (6933, 6933, 499, 'Hilipuk');
INSERT INTO `subdistricts` VALUES (6934, 6934, 499, 'Hogio (Hugio)');
INSERT INTO `subdistricts` VALUES (6935, 6935, 499, 'Holuon');
INSERT INTO `subdistricts` VALUES (6936, 6936, 499, 'Kabianggama (Kabianggema)');
INSERT INTO `subdistricts` VALUES (6937, 6937, 499, 'Kayo');
INSERT INTO `subdistricts` VALUES (6938, 6938, 499, 'Kona');
INSERT INTO `subdistricts` VALUES (6939, 6939, 499, 'Koropun (Korupun)');
INSERT INTO `subdistricts` VALUES (6940, 6940, 499, 'Kosarek');
INSERT INTO `subdistricts` VALUES (6941, 6941, 499, 'Kurima');
INSERT INTO `subdistricts` VALUES (6942, 6942, 499, 'Kwelemdua (Kwelamdua)');
INSERT INTO `subdistricts` VALUES (6943, 6943, 499, 'Kwikma');
INSERT INTO `subdistricts` VALUES (6944, 6944, 499, 'Langda');
INSERT INTO `subdistricts` VALUES (6945, 6945, 499, 'Lolat');
INSERT INTO `subdistricts` VALUES (6946, 6946, 499, 'Mugi');
INSERT INTO `subdistricts` VALUES (6947, 6947, 499, 'Musaik');
INSERT INTO `subdistricts` VALUES (6948, 6948, 499, 'Nalca');
INSERT INTO `subdistricts` VALUES (6949, 6949, 499, 'Ninia');
INSERT INTO `subdistricts` VALUES (6950, 6950, 499, 'Nipsan');
INSERT INTO `subdistricts` VALUES (6951, 6951, 499, 'Obio');
INSERT INTO `subdistricts` VALUES (6952, 6952, 499, 'Panggema');
INSERT INTO `subdistricts` VALUES (6953, 6953, 499, 'Pasema');
INSERT INTO `subdistricts` VALUES (6954, 6954, 499, 'Pronggoli (Proggoli)');
INSERT INTO `subdistricts` VALUES (6955, 6955, 499, 'Puldama');
INSERT INTO `subdistricts` VALUES (6956, 6956, 499, 'Samenage');
INSERT INTO `subdistricts` VALUES (6957, 6957, 499, 'Sela');
INSERT INTO `subdistricts` VALUES (6958, 6958, 499, 'Seredela (Seredala)');
INSERT INTO `subdistricts` VALUES (6959, 6959, 499, 'Silimo');
INSERT INTO `subdistricts` VALUES (6960, 6960, 499, 'Soba');
INSERT INTO `subdistricts` VALUES (6961, 6961, 499, 'Sobaham');
INSERT INTO `subdistricts` VALUES (6962, 6962, 499, 'Soloikma');
INSERT INTO `subdistricts` VALUES (6963, 6963, 499, 'Sumo');
INSERT INTO `subdistricts` VALUES (6964, 6964, 499, 'Suntamon');
INSERT INTO `subdistricts` VALUES (6965, 6965, 499, 'Suru Suru');
INSERT INTO `subdistricts` VALUES (6966, 6966, 499, 'Talambo');
INSERT INTO `subdistricts` VALUES (6967, 6967, 499, 'Tangma');
INSERT INTO `subdistricts` VALUES (6968, 6968, 499, 'Ubahak');
INSERT INTO `subdistricts` VALUES (6969, 6969, 499, 'Ubalihi');
INSERT INTO `subdistricts` VALUES (6970, 6970, 499, 'Ukha');
INSERT INTO `subdistricts` VALUES (6971, 6971, 499, 'Walma');
INSERT INTO `subdistricts` VALUES (6972, 6972, 499, 'Werima');
INSERT INTO `subdistricts` VALUES (6973, 6973, 499, 'Wusuma');
INSERT INTO `subdistricts` VALUES (6974, 6974, 499, 'Yahuliambut');
INSERT INTO `subdistricts` VALUES (6975, 6975, 499, 'Yogosem');
INSERT INTO `subdistricts` VALUES (6976, 6976, 500, 'Abenaho');
INSERT INTO `subdistricts` VALUES (6977, 6977, 500, 'Apalapsili');
INSERT INTO `subdistricts` VALUES (6978, 6978, 500, 'Benawa');
INSERT INTO `subdistricts` VALUES (6979, 6979, 500, 'Elelim');
INSERT INTO `subdistricts` VALUES (6980, 6980, 500, 'Welarek');
INSERT INTO `subdistricts` VALUES (6981, 6981, 501, 'Danurejan');
INSERT INTO `subdistricts` VALUES (6982, 6982, 501, 'Gedong Tengen');
INSERT INTO `subdistricts` VALUES (6983, 6983, 501, 'Gondokusuman');
INSERT INTO `subdistricts` VALUES (6984, 6984, 501, 'Gondomanan');
INSERT INTO `subdistricts` VALUES (6985, 6985, 501, 'Jetis');
INSERT INTO `subdistricts` VALUES (6986, 6986, 501, 'Kotagede');
INSERT INTO `subdistricts` VALUES (6987, 6987, 501, 'Kraton');
INSERT INTO `subdistricts` VALUES (6988, 6988, 501, 'Mantrijeron');
INSERT INTO `subdistricts` VALUES (6989, 6989, 501, 'Mergangsan');
INSERT INTO `subdistricts` VALUES (6990, 6990, 501, 'Ngampilan');
INSERT INTO `subdistricts` VALUES (6991, 6991, 501, 'Pakualaman');
INSERT INTO `subdistricts` VALUES (6992, 6992, 501, 'Tegalrejo');
INSERT INTO `subdistricts` VALUES (6993, 6993, 501, 'Umbulharjo');
INSERT INTO `subdistricts` VALUES (6994, 6994, 501, 'Wirobrajan');

-- ----------------------------
-- Table structure for suppliers
-- ----------------------------
DROP TABLE IF EXISTS `suppliers`;
CREATE TABLE `suppliers`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `supplier_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `vat_number` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `country` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `city` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `state` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `postal_code` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `company_id` bigint NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of suppliers
-- ----------------------------

-- ----------------------------
-- Table structure for task_statuses
-- ----------------------------
DROP TABLE IF EXISTS `task_statuses`;
CREATE TABLE `task_statuses`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `color` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int NULL DEFAULT NULL,
  `company_id` bigint NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of task_statuses
-- ----------------------------

-- ----------------------------
-- Table structure for tasks
-- ----------------------------
DROP TABLE IF EXISTS `tasks`;
CREATE TABLE `tasks`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `project_id` bigint NOT NULL,
  `milestone_id` bigint NULL DEFAULT NULL,
  `priority` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `task_status_id` bigint NOT NULL,
  `assigned_user_id` bigint NULL DEFAULT NULL,
  `start_date` date NOT NULL,
  `end_date` date NULL DEFAULT NULL,
  `custom_fields` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `user_id` bigint NOT NULL,
  `company_id` bigint NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tasks
-- ----------------------------

-- ----------------------------
-- Table structure for taxs
-- ----------------------------
DROP TABLE IF EXISTS `taxs`;
CREATE TABLE `taxs`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `tax_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `rate` decimal(10, 2) NOT NULL,
  `type` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_id` bigint NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of taxs
-- ----------------------------

-- ----------------------------
-- Table structure for timesheets
-- ----------------------------
DROP TABLE IF EXISTS `timesheets`;
CREATE TABLE `timesheets`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `total_hour` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint NOT NULL,
  `project_id` bigint NOT NULL,
  `task_id` bigint NOT NULL,
  `note` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `company_id` bigint NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of timesheets
-- ----------------------------

-- ----------------------------
-- Table structure for transactions
-- ----------------------------
DROP TABLE IF EXISTS `transactions`;
CREATE TABLE `transactions`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `trans_date` date NOT NULL,
  `account_id` bigint NOT NULL,
  `chart_id` bigint NOT NULL,
  `type` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `dr_cr` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` decimal(10, 2) NOT NULL,
  `base_amount` decimal(10, 2) NULL DEFAULT NULL,
  `payer_payee_id` bigint NULL DEFAULT NULL,
  `invoice_id` bigint NULL DEFAULT NULL,
  `purchase_id` bigint NULL DEFAULT NULL,
  `purchase_return_id` bigint NULL DEFAULT NULL,
  `project_id` bigint NULL DEFAULT NULL,
  `payment_method_id` bigint NOT NULL,
  `reference` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `attachment` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `note` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `company_id` bigint NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of transactions
-- ----------------------------

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_id` bigint NULL DEFAULT NULL,
  `profile_picture` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `status` int NOT NULL,
  `language` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `company_id` int NULL DEFAULT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `nama_lengkap` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `jk` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `tgl_lahir` date NULL DEFAULT NULL,
  `no_hp` varbinary(255) NULL DEFAULT NULL,
  `nik` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `alamat` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `kec` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `kota` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `prov` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `kode_pos` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `merchant_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `users_email_unique`(`email`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 219 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'Admin', 'admin@ngelapak.co.id', '2021-09-27 22:48:38', '$2y$10$j.e1t388401LJBrRpeJEpe8fg3HJRykUyWjtdum8a3q95ZAD0CUNe', 'admin', NULL, 'default.png', 1, NULL, NULL, 'DRWHA0SVXkZd40hVDISTVmVtC4P37IqnsUAn9WGqK23IDXmsYEZVi7n9OeKa', '2021-09-27 22:48:38', '2021-10-28 11:50:32', 'Admin Ngelapak', 'Laki-Laki', '2021-01-01', 0x31323334353637383930, '1234567890', 'Royal Residence', '', 'Surabaya', 'Jawa Timur', '1234567890', '');
INSERT INTO `users` VALUES (50, 'LPDPUI_Siti Jumhati', 'jumhati1981@gmail.com', '2021-10-15 19:34:20', '$2y$10$YuIiLGam7A3AjjlcxaWHRu0sKqs6kabElJtM2FEx6kMz4DdYp/5Mm', 'user', NULL, 'profile_1634302970.jpg', 1, 'English', 49, 'g8uuPpRA0HieoAnapXBrMs4lxRvFZJ2MekQ8uSmkSSpD6nXnWPKlIBazqtwt', '2021-10-15 19:29:29', '2021-10-15 20:02:50', 'Siti Jumhati', 'Perempuan', '1981-05-10', 0x303831333131343536303335, '3276025005810015', 'Jalan Kelengkeng 4 Blok E0 RT002 RW018 Kelurahan Sukatanai Kecamatan Tapos', '', 'Depok', 'Jawa Barat', '16454', '');
INSERT INTO `users` VALUES (51, 'Susilawati', 'sw.susilwati12@gmail.com', NULL, '$2y$10$.NTf8xhJo135zWoasFjMouhteaU9BPqKfrRKbOuQnRz3vxpWYu/oq', 'user', NULL, 'default.png', 1, 'English', 50, NULL, '2021-10-15 19:29:30', '2021-10-15 19:29:30', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '');
INSERT INTO `users` VALUES (52, 'LPDPUI_Dian Annisa', 'dianannisa1031@gmail.com', '2021-10-15 19:32:52', '$2y$10$CJWJ9ZQW2/G85nKIOEM7F.SIF.4PbJj7BVHzNYKTNISekfuVfZulC', 'user', NULL, 'profile_1634302394.png', 1, 'English', 51, 'DF7oD5a0fqXtght4ugQzFdd9vdsbplbPPdkxSmzK5lInrGIZy4LjhIlGmRxX', '2021-10-15 19:29:32', '2021-10-15 20:02:55', 'Dian Annisa', 'Perempuan', '1995-02-10', 0x303833383131353339373635, '3201246104950002', 'Kp  tipar rt 04 rw 04 no 73 ciawi bogor 16720', '', 'Kabupaten Bogor', 'Jawa Barat', '16720', '');
INSERT INTO `users` VALUES (53, 'Budi', 'budisulis804@gmail.com', '2021-10-15 19:43:19', '$2y$10$v/WA8wJ0U6lpt8nDA/OqQOrmTFkvj9IlboqcsVAhcxDl3kyabF6MG', 'user', NULL, 'profile_1634302408.jpg', 1, 'English', 52, 'JEsA4achlkfr1FEmH0FvlRRwzyFSeBr3yAIOdWzwGzI5QbxS1ORA11jI1t1o', '2021-10-15 19:30:13', '2021-10-15 19:53:28', 'sulistiyanto', 'Laki-Laki', '1972-06-19', 0x303831333836313038363234, '3216221906720001', 'Perum Mutiara Bekasi Jaya Blok F16/11 Sindangmulya.Cibarusah', '', 'Bekasi', 'Jawa Barat', '17341', '');
INSERT INTO `users` VALUES (55, 'LPDPUI_Yessie Natasia Mareti', 'yorrieatery@gmail.com', '2021-10-15 19:32:34', '$2y$10$hct3SiIXxU2rnecVEtXhVu0hGRHZd5uPBxgKlVQ7JTxTjjOcuALyu', 'user', NULL, 'default.png', 1, 'English', 54, 'VW3khieKtj4EeEOurKvrvs8g4mn7AtxO0aGbRWzGQHntgacLrl3SJEwTMKgC', '2021-10-15 19:30:21', '2021-10-15 19:37:24', 'Yessie Natasia Mareti', 'Perempuan', '1986-03-18', 0x30383137333034393838, '3578244306860001', 'Pandugo Baru XIII P1A', '', 'Surabaya', 'Jawa Timur', '60297', '');
INSERT INTO `users` VALUES (56, 'LPDPUI_ratnapuspita', 'ratnapuspitarp071@gmail.com', '2021-10-15 19:31:59', '$2y$10$BJ53A5QyxA7FaiKS1YIB8O5N3cFPwxefb82XagMJRYGD1xVD/CBKy', 'user', NULL, 'default.png', 1, 'English', 55, 'VF0o8iVhvpZCi5KO7n285xr8fYvHE0ujmCX9L3lHuAqGZZNtgvQY0rSFwWx9', '2021-10-15 19:30:22', '2021-10-15 19:33:29', 'Ratna Puspita', 'Perempuan', '1995-07-26', 0x30383935343232383832303932, '3319016607950001', 'Blimbing Kidul Rt 04/03', '', 'KAB. KUDUS', 'Jawa Tengah', '59332', '');
INSERT INTO `users` VALUES (57, 'LPDPUI_Miftahudin Nur Ihsan', 'smartbatikid@gmail.com', '2021-10-15 19:32:24', '$2y$10$TJOdxFK55S/JahyICKRMTuE4byKEYN.yDw9Is6c5.dh0AuAoo9mei', 'user', NULL, 'default.png', 1, 'English', 56, NULL, '2021-10-15 19:30:31', '2021-10-15 19:39:34', 'Miftahudin Nur Ihsan', 'Laki-Laki', '1993-08-11', 0x303835373437303730333836, '3471071108930001', 'Ketanggungan, WB 2/429, RT 057, RW 012', '', 'Yogyakarta', 'Daerah Istimewa Yogyakarta', '55252', '');
INSERT INTO `users` VALUES (58, 'LPDPUI_Eman Sulaeman', 'madu.ti.kulon@gmail.com', '2021-10-15 19:45:14', '$2y$10$NJdU8TZxkNK0hCC5EqQXyuCFipHhMUWQeZS8SEphZgTj1PtM2wEIm', 'user', NULL, 'default.png', 1, 'English', 57, '5gvaFCiQeu2hkGdLj3RIIV1jBII193GYtvUYrAUHDCxvtfcG7Souts8asoKx', '2021-10-15 19:32:02', '2021-10-15 19:47:31', 'eman sulaeman', 'Laki-Laki', '1983-03-30', 0x303831323839333539303932, '3601343003830003', 'Komplek saruni permai RT 03/09 Kel. Saruni Kec. Majasari', '', 'pandeglang', 'Banten', '42216', '');
INSERT INTO `users` VALUES (59, 'lpdpui_sanga', 'sca_sanga@yahoo.com', '2021-10-15 19:33:44', '$2y$10$c4nYuQEsx3TnffqkipqZneC0T.FtTSoEsYhAj2apBKRqWdMTllV6i', 'user', NULL, 'default.png', 1, 'English', 58, NULL, '2021-10-15 19:32:03', '2021-10-15 19:35:15', 'Endah WN', 'Perempuan', '1980-07-28', 0x303835323134313433343236, '3276026807800013', 'Jl. Tole Iskandar No. 17', '', 'Depok', 'Jawa Barat', '16451', '');
INSERT INTO `users` VALUES (60, 'LPDPUI_WARUNGSASTI', 'yudhi280883@gmail.com', NULL, '$2y$10$MAfrcHG3JCffXO9KxiLjA.lPgU6qbicmgyclC9In4Xe27K9OiwdWS', 'user', NULL, 'default.png', 1, 'English', 59, 'ja04fu9rZkKMQejOYW17wO6Ntt0M08pkmrv58lYHBP2Ls69xBZ5g5dRLe08s', '2021-10-15 19:33:15', '2021-10-15 19:33:15', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '');
INSERT INTO `users` VALUES (61, 'Siti Nur Seha', 'sitinursehasuman@gmail.com', '2021-10-15 20:03:09', '$2y$10$9YhBxmQSCiUZU.Ss8LAZgODlIkYnoclC4Xr4EK45AVhYyPvh4nzTK', 'user', NULL, 'profile_1634304580.jpg', 1, 'English', 60, 'LtPYGgwf5ZzPA239uARM4EKWYVwqwtNzIDzGxhaNo93rtSjh4SkjR95d1594', '2021-10-15 19:35:12', '2021-10-15 20:29:40', 'SITI NUR SEHA', 'Perempuan', '1996-07-10', 0x303835373731383831343936, '3513105007960003', 'Dusun Pasar II RT 006/ RW 003', '', 'Kabupaten Probolinggo', 'JAWA TIMUR', '67292', '');
INSERT INTO `users` VALUES (62, 'Yunita Indrawati', 'yunita260390@gmail.com', '2021-10-15 19:42:03', '$2y$10$GchTmXUR2zOorr6SgnvTqeeNPlbOfgewKexp7aisJLQqywOi7QTu6', 'user', NULL, 'default.png', 1, 'English', 61, 'XNqHCNJywhh41i6J4kcUTOINg6YsVDlD50mNKfepY6RMJb0JBavLp6B8Dafk', '2021-10-15 19:38:47', '2021-10-15 19:42:03', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '');
INSERT INTO `users` VALUES (63, 'Samsidar', 'ssemsmart@gmail.com', '2021-10-15 19:43:04', '$2y$10$BROkRr0Ll6rISwBTi3Fa8Olg5Jczbb57wE1UUI3fKG6JJSPFtT0fy', 'user', NULL, 'default.png', 1, 'English', 62, 'E1wQvWhUQoZUNlw59WZCqKM1by3ZmibMVizR9uM8FoMnj8S14kJIqQKiDEnY', '2021-10-15 19:39:52', '2021-10-15 19:43:04', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '');
INSERT INTO `users` VALUES (65, 'Bosteak', 'bosteakcafe@gmail.com', NULL, '$2y$10$DNq6GO.tHKA1DqgyyxSWqelYtz/05D6GC66Pl/a0PnOBupmEBtqsq', 'user', NULL, 'default.png', 1, 'English', 64, 'aTVtHU9W6N6ilEuCkBbWD9h3qzqc9qkhWXVBcBWp8bI7f636NCQnhDmbrnXL', '2021-10-15 19:54:29', '2021-10-15 19:54:29', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '');
INSERT INTO `users` VALUES (66, 'LPDPUI_Yuria Ekalitani', 'ayuriaekalitani@gmail.com', NULL, '$2y$10$89T4pKN9rLiyHPBv4SQwM.hyyUmnGpKFDlUn8oL9UZJwJ9Hv5mh4a', 'user', NULL, 'default.png', 1, 'English', 65, NULL, '2021-10-15 20:05:12', '2021-10-15 20:05:12', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '');
INSERT INTO `users` VALUES (68, 'Ricka', 'rickakrisnawati42@gmail.com', '2021-10-19 22:28:45', '$2y$10$Ckzj63woEsQD2tjDC8fSOOxC9q4aOn9iDGTAd3SY7sc8lmQvnW8bG', 'user', NULL, 'default.png', 1, 'English', 67, '2L4DynsUXH9sAOHirllcVxclSl3wC27BH3VSK07ehhAbajB6Y7av3quo95RP', '2021-10-19 22:22:13', '2021-12-06 13:03:55', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '');
INSERT INTO `users` VALUES (117, 'Irma', 'irmadamasurya@yahoo.com', '2021-10-27 00:25:40', '$2y$10$i6kbbhsDB8IgXWOD4PIPuOq2bdf3CNrb6uluOhwCvemR7qx20ltYa', 'user', NULL, 'default.png', 1, 'English', 169, 'WagzSJeG3NCUMYjYFxtIJVxrX8mwV6LWbjplMi8Z9quE30TBbHaXFq0hmI0N', '2021-10-27 00:23:32', '2021-10-27 00:25:40', NULL, NULL, NULL, 0x303832323231373232323338, NULL, NULL, '', NULL, NULL, NULL, '96f417a9-d9cc-461f-a1d1-1bc265d9454a');
INSERT INTO `users` VALUES (121, 'PT Lautan Harum Mewangi', 'ptlautanmewangi@gmail.com', '2021-10-27 05:31:33', '$2y$10$M11b6btaLLcjRqZGLcUwluwROZtgvV4xgS7bi/xXZ4lGUmylwwLm6', 'user', NULL, 'default.png', 1, 'English', 173, NULL, '2021-10-27 05:28:49', '2021-10-27 05:41:38', 'SATRYA WIRA WICAKSANA', 'Laki-Laki', '1993-06-22', 0x303831323230323035323432, '3217062206930017', 'Permata Cimahi M2 No 18 Rt 9 Rw14', '', 'Cimahi', 'Jawa Barat', '40552', 'f69fee91-f511-41ac-ac8a-70e5eec591e3');
INSERT INTO `users` VALUES (126, 'Satrya Wira Wicaksana', 'satryawirawicaksana@gmail.com', '2021-10-27 09:16:50', '$2y$10$IL4lyA2NHMAQMh01J9LyB.RTXcDcsvfua6dZOgMsCubMfZeewnlva', 'user', NULL, 'default.png', 1, 'English', 178, '0E44HZ4tUF330KAZRxjRIJmucFSJt2Mk8xQQi2Mcu6s90PlVE3WIMmSXcYWk', '2021-10-27 09:16:09', '2021-11-03 01:09:49', 'SATRYA WIRA WICAKSANA', 'Laki-Laki', '1993-06-22', 0x303831323230323035323432, '32270612324122', 'Permata Cimahi M2 No 18 Rt 9 Rw14', '', 'Cimahi', 'Jawa Barat', '40552', '62c0a87c-5ce8-4949-8481-52c5e901bbc7');
INSERT INTO `users` VALUES (130, 'Lautan', 'lautanharummewangi@gmail.com', '2021-10-27 10:14:57', '$2y$10$MraSylbpsBW79fX4OujcKujRKDhtW.OGhWh2wb3Uka3o/T41gkzLq', 'user', NULL, 'default.png', 1, 'English', 182, NULL, '2021-10-27 10:14:04', '2021-10-27 10:15:18', 'SATRYA WIRA WICAKSANA', 'Laki-Laki', '1993-06-22', 0x303831323230323035323432, '3217062206930017', 'Permata Cimahi M2 No 18 Rt 9 Rw14', '', 'Cimahi', 'Jawa Barat', '40552', '9e3c03f3-a8e4-46ac-8f32-4f72fdd4ab37');
INSERT INTO `users` VALUES (133, 'Yuansyah Arief', 'yuansyaharief@gmail.com', '2021-10-28 06:06:46', '$2y$10$RzLGgJ0bz.cfJlsRXFbvtOIB89yNB/Gn9XxxtCSVHZExhy1lJDb3K', 'user', NULL, 'default.png', 1, 'English', 185, NULL, '2021-10-28 06:04:13', '2021-10-28 06:10:33', 'Yuansyah Arief', 'Laki-Laki', '1992-07-25', 0x303831323934393032393831, '167302250792', 'Jaln Cinta, Kelurahan Merindu, No. 007', '', 'Lubuklinggau', 'Sumatera Selatan', '31614', '53339819-53bd-43a2-856d-117d7638b781');
INSERT INTO `users` VALUES (136, 'demo', 'adeulala5@gmail.com', '2021-10-29 02:39:55', '$2y$10$d3fcPyYSXtw.f71I4xcQEOcVO75m8nd6qI8wZVK/Gni7ToEkiid92', 'user', NULL, 'default.png', 1, 'Indonesia', 191, NULL, '2021-10-29 02:38:30', '2021-10-29 02:41:47', 'demo tes', 'Perempuan', '2000-01-01', 0x303832313435303334383631, '3509205670005', 'jl. kebon kacang 3 no.20', '', 'Kota Jakarta Pusat', 'dki jakarta', '10210', '5f8d2b35-3392-4b1a-a85c-53bef7484f2f');
INSERT INTO `users` VALUES (137, 'Yudha purnama', 'yudha.purnama168@gmail.com', '2021-10-29 04:19:48', '$2y$10$XI9r.KH3rg5DwXSoQMwNqeyAllOmrJtkDah1EdUr1Vd17GaITgR7W', 'user', NULL, 'default.png', 1, 'Indonesia', 192, 'zQCOro2nj0WfL5tqhlzjtPLdERWGx7DXpwIINcDNwwhnDNDElA0uu93SQzgv', '2021-10-29 04:18:48', '2021-10-29 04:20:55', 'Yudha purnama', 'Laki-Laki', '1990-03-29', 0x30383132323232323338313638, '123456789', 'Royal resident', '', 'Surabaya', 'Jawa timur', '60000', '9514010d-0797-4548-a8db-d912770a39e2');
INSERT INTO `users` VALUES (139, 'Ngelapak', 'ngelapakbersamakita@gmail.com', '2021-10-31 13:48:36', '$2y$10$nh3jsATuiOW72oiEHKCWv.oftr.DY5ut/Qs.zd2/Ny42jb4WoJm9y', 'user', NULL, 'default.png', 1, 'Indonesia', 194, '5oUVyiUBvzQ9MZ0D9tDwTuuzqNFeDi0aPJaZpQwGcUJXPMVku7sfUSr0RmKm', '2021-10-31 13:47:06', '2021-10-31 13:49:02', 'SATRYA WIRA WICAKSANA', 'Laki-Laki', '1993-06-22', 0x303831323230323035323432, '3217062206930017', 'Permata Cimahi M2 No 18 Rt 9 Rw14', '', 'Cimahi', 'Jawa Barat', '40552', '6e373165-29b0-433b-93e6-5c042ef86aec');
INSERT INTO `users` VALUES (140, 'ali', 'ali.ramdani_legal@yahoo.com', '2021-11-03 05:52:50', '$2y$10$G2kyxQIVD1uBFRY3FC1xj.L8OB//tnKHCZ0.nNoqADc6Qfv8mehrO', 'user', NULL, 'default.png', 1, 'Indonesia', 195, NULL, '2021-11-03 05:50:38', '2021-11-03 05:56:02', 'ali ramdani', 'Laki-Laki', '1992-03-13', 0x303831333333383835333231, '3578281303920001', 'greges', '', 'surabaya', 'jawa timur', '60193', '6ee04f18-1000-4a21-87c1-9c5374a5ab76');
INSERT INTO `users` VALUES (142, 'Andrean Aw', 'andreanarya2000@gmail.com', '2021-11-10 11:00:37', '$2y$10$ODR1A4XDkjafVY3XP3C1hue/903vuJQqTnKZA6lXZrgf59C7TSDlG', 'user', NULL, 'default.png', 1, 'Indonesia', 197, NULL, '2021-11-04 03:38:37', '2021-11-10 11:02:36', 'Andrean Arya Wicaksana', 'Laki-Laki', '2000-06-16', 0x30383935383036373034313635, '3516111606000007', 'Sumbergirang', '', 'Mojokerto', 'Jawa timur', '61363', 'a7d1895a-ea03-40d7-8a01-54c3c84bd728');
INSERT INTO `users` VALUES (148, 'Testing', 'ytgratisan99@gmail.com', '2021-11-04 09:41:23', '$2y$10$Ka7QzSYzDaC.CUx.p.NDfehKDmwgza4TGosL1PoJ8W4nffuMhURvu', 'user', NULL, 'default.png', 1, 'Indonesia', 203, NULL, '2021-11-04 09:38:32', '2021-11-12 15:01:56', 'tes2', 'Laki-Laki', '2021-11-22', 0x303831313233313233313233, '3223', 'we', '4605', '326', '14', '1212', '52a03926-65d5-465a-94a7-af8b6aa62d00');
INSERT INTO `users` VALUES (151, 'Ian Lumban Raja', 'ianmariabless@gmail.com', '2021-11-08 12:07:53', '$2y$10$qkgPnqtyq3gNtQZGLbue/eKJ8HDpm7mW1f8e8VZnrr44M/bkUrAWK', 'user', NULL, 'default.png', 1, 'Indonesia', 206, '6qNRjzg20H8fo8aiCeJRopRxGYvIFckSiucqgGuLnjTfLr8wU5vSpT6m1lNL', '2021-11-08 12:04:58', '2021-11-08 12:09:46', 'Ian Lumban Raja', 'Laki-Laki', '2021-11-08', 0x36323831333438313933323839, '1271112204840004', 'Jl. Bunga Rampai Raya no. 106 Lingk II', '', 'Medan', 'Sumatera Utara', '20135', 'aa730cd8-f9bc-419a-86ea-df3927400a49');
INSERT INTO `users` VALUES (152, 'LPDPUI_Maria Juliastuti', 'uswatunnazwa.juli@gmail.com', NULL, '$2y$10$Q9/NldlpOpRRQdOum8HUY.SHhsuB3HTvWMbor0bm.a84gppC9igFW', 'user', NULL, 'default.png', 1, 'Indonesia', 207, NULL, '2021-11-08 13:21:54', '2021-11-08 13:21:54', NULL, NULL, NULL, 0x303832313435323833393039, NULL, NULL, '', NULL, NULL, NULL, '9fb97187-1a62-4cea-89de-b5fb14dce804');
INSERT INTO `users` VALUES (154, 'IMANDA EDWIN PUTRA', 'edwinkph9@gmail.com', '2021-11-09 10:44:29', '$2y$10$m0YZUPJ8R.L5ayhjic8NXevyb7LMXnu3sSFMLU72tX4HA1Zs3wgEq', 'user', NULL, 'default.png', 1, 'Indonesia', 211, NULL, '2021-11-09 10:43:13', '2021-11-09 10:47:48', 'IMANDA EDWIN PUTRA', 'Laki-Laki', '1998-06-03', 0x3038393832363932323238, '1708040306980002', 'Desa karang endah kecamatan kepahiang', '', 'Kabupaten kepahiang', 'Bengkulu', '39172', '5cadce04-7e42-49fb-8ceb-ea38504d88ab');
INSERT INTO `users` VALUES (155, 'Mauldy', 'babasulthankitchen.sby@gmail.com', '2021-11-09 12:54:52', '$2y$10$/0yc4M3VNtSmdBxMSHfW6OG5Me627w1y3foW9ZzqoM9z3Gu9aAmYa', 'user', NULL, 'default.png', 1, 'Indonesia', 212, 'SHfYzgnfg8YnjyfpVXEmleBvgtceQVN2grKmWqzptrl20UML7JwTHJjm8e5c', '2021-11-09 12:52:25', '2022-02-10 14:30:09', 'Mauldy Rizqia Ilhaq', 'Laki-Laki', '1996-12-01', 0x303837373031343938373539, '357820012960003', 'Pondok Maritim Indah AL-3', '', 'Surabaya', 'Jawa Timur', '60222', '5b171d72-c60d-421d-8f7e-24fb5cfb3c7d');
INSERT INTO `users` VALUES (156, 'LPDPUI_MARIANA', 'marianamelia55@gmail.com', '2021-11-09 13:37:25', '$2y$10$2rLEQBwL2EAXIzP4.BgtPOBSLrwKYyzwRprWiiiUSJ4tybzr.DhsW', 'user', NULL, 'default.png', 1, 'Indonesia', 213, 'GcsXzvwcmlpX74vTzmozulYi03tGg6d8tJIbPuG4j47ffWvnNRLSqGHxbNY2', '2021-11-09 13:36:11', '2021-11-09 13:39:49', 'Mariana melia', 'Perempuan', '1986-10-18', 0x303835333632373039313030, '1174025810860001', 'Jalan mesjid', '', 'Langsa', 'Aceh', '24415', 'e2a36a99-94b1-4c1b-a3ba-cc607fae088c');
INSERT INTO `users` VALUES (157, 'Rifiana Kartika Sari', 'iburifi@gmail.com', '2021-11-09 18:27:18', '$2y$10$V7T7tWmBCArjKF6rR4OFouSZ47FKJfwnnvdUn/3L3SxlaiLo.WYvG', 'user', NULL, 'default.png', 1, 'Indonesia', 214, 'DAbShIqxZZZBFZHJwvQPCZTaRWQyiMLQX2JDWNDqm1qbo7zBv4KKiwCekuJL', '2021-11-09 18:25:31', '2021-11-09 18:30:24', 'Rifiana Kartika Sari', 'Perempuan', '1990-06-20', 0x303832323232323030323035, '112233', 'Jl.Rejomulyo II Wates Ngaliyan', '', 'SEMARANG', 'JAWA TENGAH', 'NGALIYAN', 'd6afd963-1603-422a-aaa7-824b8d258d2c');
INSERT INTO `users` VALUES (158, 'MUJIYATI', 'muji38105@gmail.com.id', NULL, '$2y$10$ucnFv0cgDcGvyQhqlEeyJuoio5AZ5r8rco7IEaWZNT3G96YPWhc5C', 'user', NULL, 'default.png', 1, 'Indonesia', 215, NULL, '2021-11-12 07:10:10', '2021-11-12 07:10:10', NULL, NULL, NULL, 0x303831333131343834303438, NULL, NULL, '', NULL, NULL, NULL, 'f8fa65fa-f6f9-45dd-a7b5-c7e87527ca31');
INSERT INTO `users` VALUES (159, 'Mujiyati', 'muji38105@gmail.com', NULL, '$2y$10$ln1cvord44AmOVq4Kd.r2eZv8XwAPhOwII94laIvJU43qQlSlWWPy', 'user', NULL, 'default.png', 1, 'Indonesia', 216, '7kxf0IzBFg3GqGFv172q9zSikFy3CbR28pPtUHc4FuEoSVAvWVxlRSY5WMqV', '2021-11-12 09:58:25', '2021-11-12 09:58:25', NULL, NULL, NULL, 0x303831333131343834303438, NULL, NULL, '', NULL, NULL, NULL, 'cc660dc9-cd1c-4b7d-b1c5-de3ab2c8a6ff');
INSERT INTO `users` VALUES (160, 'risma', 'risma@ngelapak.co.id', '2021-11-12 10:41:52', '$2y$10$Lgc0jRMSHfjfJuSujTURLOY4jI8SSIN/XHP7N5Ilt6.ghycnxU0ee', 'user', NULL, 'default.png', 1, 'Indonesia', 217, NULL, '2021-11-12 10:39:46', '2021-11-12 10:50:20', 'Risma', 'Perempuan', '1997-11-12', 0x303833383436363030393838, '13141225', 'sukodono blok h no 4b', '', 'sidoarjo', 'jatim', '61258', '8447fae7-c681-4695-ba84-4e80ce6722f1');
INSERT INTO `users` VALUES (161, 'Ida Pudjiastuti', 'idamahagita71@gmail.com', '2021-11-17 11:21:24', '$2y$10$2TgMT/JRxd5qiwYW2lBU/./GOfFHdjrTCTJsOmKJbsna6qt/Z55qq', 'user', NULL, 'default.png', 1, 'Indonesia', 218, NULL, '2021-11-17 11:20:40', '2021-11-17 11:29:04', 'Ida Pudjiastuti', 'Perempuan', '0001-01-01', 0x303835363439303639383439, '0', 'Jln Raya kediri blitar ds Susuhbango Rt01 Rw01', '2519', '178', '11', '64176', '376f11f9-7355-4115-8c66-15f275e2a322');
INSERT INTO `users` VALUES (162, 'Doltons Edward', 'edwarddoltons@gmail.com', '2021-11-17 11:52:08', '$2y$10$Df25ZeJ9NJsTo5lid7pvredANG0oECK7m/GeGpm4unn4oQEi3LPI2', 'user', NULL, 'default.png', 1, 'Indonesia', 219, NULL, '2021-11-17 11:51:18', '2021-11-17 12:06:24', 'Doltons Edward', 'Laki-Laki', '0001-01-01', 0x303839363139383030343539, '0', 'Jln. Musyawarah', '3868', '275', '28', '90554', '376f11f9-7355-4115-8c66-15f275e2a322');
INSERT INTO `users` VALUES (166, 'M Okta Prayudi', 'octaprayudi@gmail.com', '2021-11-17 14:41:14', '$2y$10$0HZ3d7rtugcWJINGcjzpVuRWheTmHEQO5sbA4AK.1ugq4acBwnw5q', 'user', NULL, 'default.png', 1, 'Indonesia', 227, NULL, '2021-11-17 14:25:06', '2021-11-24 15:19:36', 'M Okta Prayudi', 'Laki-Laki', '0001-01-01', 0x3038313137313233303130, '0', 'Konp garuda Putra D 105', '4614', '327', '33', '30114', '376f11f9-7355-4115-8c66-15f275e2a322');
INSERT INTO `users` VALUES (168, 'Friska Margareta Tobing', 'friskamt28@gmail.com', '2021-11-17 15:00:09', '$2y$10$/iUcfiYzeGHs0mpLWO5Ww.ZH1ewhiBA/h4wvYhh8gCDSzA5gVkYzG', 'user', NULL, 'default.png', 1, 'Indonesia', 230, NULL, '2021-11-17 14:59:37', '2021-11-17 15:02:14', 'Friska Margareta Tobing', 'Perempuan', '0001-01-01', 0x303831333538323030323832, '0', 'Pondok Benowo Indah', '6132', '444', '11', '60197', '376f11f9-7355-4115-8c66-15f275e2a322');
INSERT INTO `users` VALUES (169, 'Edvano Rahmana', 'edvano.rahmana@gmail.com', '2021-11-17 15:04:43', '$2y$10$pn9SwQ0fvouE1mZM3.U7Gu31Us4IsUnBLSntyrTmfy.czmvFGm3Z2', 'user', NULL, 'default.png', 1, 'Indonesia', 231, NULL, '2021-11-17 15:04:15', '2021-11-17 15:06:59', 'Edvano Rahmana', 'Laki-Laki', '0001-01-01', 0x303831383033303839333539, '0', 'Jl Kedondong Kidul IV no.6', '6157', '444', '11', '60262', '376f11f9-7355-4115-8c66-15f275e2a322');
INSERT INTO `users` VALUES (170, 'Guntur Harianto', 'gunturharianto05@gmail.com', '2021-11-17 16:07:36', '$2y$10$PHi91kjBBdr4UbytAKWX1uD/9oUcs/AweJUw08qtgFmRHeTMfbcMu', 'user', NULL, 'default.png', 1, 'Indonesia', 232, NULL, '2021-11-17 16:06:59', '2021-11-29 10:27:59', 'Guntur Harianto', 'Laki-Laki', '0001-01-01', 0x303835373931303133323835, '0', 'Jl. Trunojoyo', '3636', '256', '11', '65111', '376f11f9-7355-4115-8c66-15f275e2a322');
INSERT INTO `users` VALUES (171, 'Matoyib', 'matoyib@gmail.com', '2021-11-17 16:12:55', '$2y$10$Atn6X.xWnKV2nKiWavbure4a2WNS.et1hdLDrJ2plCHQYNLOGnVBu', 'user', NULL, 'default.png', 1, 'Indonesia', 233, NULL, '2021-11-17 16:12:09', '2021-11-17 16:14:38', 'Matoyib', 'Laki-Laki', '0001-01-01', 0x303831323938343338333834, '0', 'Jl. Peta Selatan', '2089', '151', '6', '11840', '376f11f9-7355-4115-8c66-15f275e2a322');
INSERT INTO `users` VALUES (172, 'Sobat Ngelapak', 'sobat@ngelapak.co.id', '2021-11-18 09:49:28', '$2y$10$1KSjeALTMfMoQhAE6NVKxuvwf5IOSci1oQ343Z.6iFtbx3VKQCFAy', 'user', NULL, 'default.png', 1, 'Indonesia', 234, 'Gh9WOG13NfH4qnEc63NQ6UkVash9B09Wb2AX7gYYTD1ORgjxoliWimOLcysp', '2021-11-18 09:48:56', '2021-11-18 09:55:16', 'Sobat Ngelapak', 'Laki-Laki', '2000-08-17', 0x303831323232333333343434, '123', 'Jalan ke rumah sobat Ngelapak', '2102', '152', '6', '123', '7f897249-7035-445b-9444-a4f3c0390e31');
INSERT INTO `users` VALUES (173, 'Arif Senoaji', '09superseno@gmail.com', '2021-11-18 12:24:23', '$2y$10$8ZpBc40GGRIvY5wSNaF6huhwbffi.9SKxd91v3oF1GudQJfFBYELi', 'user', NULL, 'default.png', 1, 'Indonesia', 235, NULL, '2021-11-18 12:22:09', '2021-11-18 12:29:24', 'Arif Senoaji', 'Laki-Laki', '0001-01-01', 0x3038353632393331343630, '0', 'jalan menteng atas selatan 3', '2111', '153', '6', '12960', '376f11f9-7355-4115-8c66-15f275e2a322');
INSERT INTO `users` VALUES (174, 'Sakti Al Fattaah', 'sakti.alfattaah@gmail.com', '2021-11-18 12:37:33', '$2y$10$MiBXPnglj1YoCNXqDEc43eN8UUM6FCFhBKnTu/HDPKs3fRNGbgLaW', 'user', NULL, 'default.png', 1, 'Indonesia', 236, NULL, '2021-11-18 12:31:29', '2021-11-18 12:53:51', 'Sakti Al Fattaah', 'Laki-Laki', '0001-01-01', 0x303832313130383036323838, '0', 'Sumber Lor Rt 001/028, Kalitirto', '5779', '419', '5', '55573', '376f11f9-7355-4115-8c66-15f275e2a322');
INSERT INTO `users` VALUES (175, 'Siti Nur Latifah', 'snurlatifah784@gmail.com', '2021-11-19 13:09:55', '$2y$10$6XytGZXcA53RvKK4XfpOdO./ixiICqn/HrF5v2Qu0krRdnby1Ghv.', 'user', NULL, 'default.png', 1, 'Indonesia', 237, NULL, '2021-11-19 12:58:21', '2021-11-19 13:18:00', 'Siti Nur Latifah', 'Perempuan', '0001-01-01', 0x303832333734393831333734, '0', 'Tulang bawang', '6806', '490', '18', '34555', '376f11f9-7355-4115-8c66-15f275e2a322');
INSERT INTO `users` VALUES (176, 'Arie Arma Dhani', 'arie_armadhani@gmail.com', '2021-11-19 13:21:13', '$2y$10$uJR8xvDZhVHfsI6XByOIruVATAxd52wArbFa4USXJf9epqt1XKI1u', 'user', NULL, 'default.png', 1, 'Indonesia', 238, NULL, '2021-11-19 13:20:31', '2021-11-19 13:50:53', 'Arie Arma Dhani', 'Laki-Laki', '0001-01-01', 0x303832323937393737373837, '0', 'Perumahan awani residence cluster vimala blok L-09', '378', '24', '9', '40552', '376f11f9-7355-4115-8c66-15f275e2a322');
INSERT INTO `users` VALUES (177, 'Santi Heramurtiani', 'santiheramurtiani@ymail.com', '2021-11-19 15:10:48', '$2y$10$m0oXA9ymrnnZwGvbuHivZOt5rEBREj7egQ5GPkqHajF4GCZBjOTmm', 'user', NULL, 'default.png', 1, 'Indonesia', 239, NULL, '2021-11-19 15:10:13', '2021-11-19 15:16:54', 'Santi Heramurtiani', 'Perempuan', '0001-01-01', 0x303831333136303337303134, '0', 'Apartment Green Pramuka, Tower Chrysant, Jl. Jend. A. Yani Kav. 49', '2095', '152', '6', '10570', '376f11f9-7355-4115-8c66-15f275e2a322');
INSERT INTO `users` VALUES (178, 'Nando Dwiyandi', 'nando.dwi@gmail.com', '2021-11-19 15:21:26', '$2y$10$M8o7rxOg7F5vmJGdohrBbem.N/mltSATMBeM3FD26iYeOwdr5Creu', 'user', NULL, 'default.png', 1, 'Indonesia', 240, NULL, '2021-11-19 15:19:34', '2021-11-19 15:23:22', 'Nando Dwiyandi', 'Laki-Laki', '0001-01-01', 0x3038393939313634363433, '0', 'Jalan Pemancingan I No. 22, Srengseng', '2091', '151', '6', '11630', '376f11f9-7355-4115-8c66-15f275e2a322');
INSERT INTO `users` VALUES (179, 'Dikron', 'dikron0503@yahoo.com', '2021-11-19 15:30:24', '$2y$10$Ebtw0brWPLIGHiPN7NnLw.3PeoVCVQo4qsjQ/5QKzkoWucnJ9LVr2', 'user', NULL, 'default.png', 1, 'Indonesia', 241, NULL, '2021-11-19 15:29:58', '2021-11-19 15:33:39', 'Dikron', 'Laki-Laki', '0001-01-01', 0x303832313535303030303533, '0', 'JL RAWA GEDE WETAN NO 48', '759', '55', '9', '16350', '376f11f9-7355-4115-8c66-15f275e2a322');
INSERT INTO `users` VALUES (180, 'Savira Azura', 'virazura.bumiarasy@gmail.com', '2021-11-19 15:54:11', '$2y$10$TEGKlERJCaDZrkDR6fGteOvIB149UPuPgQTVWMKnyN.2rYMxOzrR2', 'user', NULL, 'default.png', 1, 'Indonesia', 242, NULL, '2021-11-19 15:53:41', '2021-11-19 15:55:34', 'Savira Azura', 'Perempuan', '0001-01-01', 0x3038313131393439353133, '0', 'Jl. M. Kahfi 1 gang Aselih Grand Matoa Blok AA No. 9', '2104', '153', '6', '12530', '376f11f9-7355-4115-8c66-15f275e2a322');
INSERT INTO `users` VALUES (181, 'Frida Ganda Magdalena', 'fridasiallagans@gmail.com', '2021-11-22 10:43:02', '$2y$10$vsp/K.GCkqF0GB5j71b.DOFE1CGZMTPNwrZxXGpKrgco4S4WIdwVm', 'user', NULL, 'default.png', 1, 'Indonesia', 243, NULL, '2021-11-22 10:42:23', '2021-11-22 10:47:47', 'Frida Ganda Magdalena', 'Perempuan', '0001-01-01', 0x303839353333323131393038, '0', 'Jalan haji tarin no 56 rt.02 rw.09 kp.cikumpa', '1586', '115', '9', '16412', '376f11f9-7355-4115-8c66-15f275e2a322');
INSERT INTO `users` VALUES (182, 'Daniel Tomi Hutasoit', 'hutasoitdaniel274@yahoo.com', '2021-11-22 10:49:57', '$2y$10$WxMs2iqkVBLdH4VdUAtaY.oAni9SPEUbZDP/OVj7yLAYkyRGtqoYC', 'user', NULL, 'default.png', 1, 'Indonesia', 244, NULL, '2021-11-22 10:49:28', '2021-11-22 10:53:22', 'Daniel Tomi Hutasoit', 'Laki-Laki', '0001-01-01', 0x303832323934313838383733, '0', 'Jalan Sisingamangaraja no.169', '6415', '465', '34', '22474', '376f11f9-7355-4115-8c66-15f275e2a322');
INSERT INTO `users` VALUES (183, 'Laras Puspasari', 'manifestudio@gmail.com', '2021-11-22 11:06:19', '$2y$10$/bVpk5Ok1OWQYi9dPu7VLuG1WxMp5wLCzpEN74Exc7br6yfyTksMi', 'user', NULL, 'default.png', 1, 'Indonesia', 245, NULL, '2021-11-22 11:05:46', '2021-11-22 11:08:33', 'Laras Puspasari', 'Perempuan', '0001-01-01', 0x303831323130353036333330, '0', 'Jl Delman Elok VII no 17 Tanah Kusir', '2106', '153', '6', '12240', '376f11f9-7355-4115-8c66-15f275e2a322');
INSERT INTO `users` VALUES (184, 'Fikri Bastian', 'fikribastian01@gmail.com', '2021-11-22 11:21:09', '$2y$10$yQMLcP4Zjwq8oscH6CQ0POuKRs56N9mHC/ZO.Jqol84RCzIUbta0m', 'user', NULL, 'default.png', 1, 'Indonesia', 246, NULL, '2021-11-22 11:20:39', '2021-11-22 11:24:09', 'Fikri Bastian', 'Laki-Laki', '0001-01-01', 0x303833313232393133393836, '0', 'Dukuh pakis 5 no 34B, rt4 rw3', '6135', '444', '11', '60224', '376f11f9-7355-4115-8c66-15f275e2a322');
INSERT INTO `users` VALUES (185, 'Nevyta Hardyah', 'neeturelife@gmail.com', '2021-11-22 11:44:13', '$2y$10$lSAIXdcw53KRORoASN97NORxhr4U6uj.9j4sQmQkc9M7cnHyXG3H.', 'user', NULL, 'default.png', 1, 'Indonesia', 247, NULL, '2021-11-22 11:38:57', '2021-11-22 11:47:00', 'Nevyta Hardyah Setyaningtyas', 'Perempuan', '0001-01-01', 0x30383131333235383131, '0', 'Perum. Istana Mentari blok E1 no.31', '5641', '409', '11', '61234', '376f11f9-7355-4115-8c66-15f275e2a322');
INSERT INTO `users` VALUES (186, 'Amnu Ubaidillah', 'amnu.ubeq@gmail.com', '2021-11-22 11:48:53', '$2y$10$U9/p83ABCaMNvrFQ6sAI8.snNZVhgFD2wr4BTQmEGxQWsyalyDNXe', 'user', NULL, 'default.png', 1, 'Indonesia', 248, 'UL0b4BQkrLqfp3SkuP7I38BuOHzjCg1uxjjOnDGkslOTVTP1tCIU6ZRkzRNH', '2021-11-22 11:48:17', '2021-11-22 11:50:36', 'Amnu Ubaidillah', 'Laki-Laki', '0001-01-01', 0x303831323137393834373036, '0', 'Sono Indah, Buduran Gg Tpq, Sidokerto', '5632', '409', '11', '61252', '376f11f9-7355-4115-8c66-15f275e2a322');
INSERT INTO `users` VALUES (187, 'Sri Hernowo Wiworo', 'ardhinawa08@yahoo.com', '2021-11-22 12:04:01', '$2y$10$YCdboC/DoIJQ6k3lUj1BL.jSirhUNSEQfi74JSX0Gap7H77GRuu4C', 'user', NULL, 'default.png', 1, 'Indonesia', 249, NULL, '2021-11-22 12:03:32', '2021-11-22 12:09:15', 'Sri Hernowo Wiworo Hernowo', 'Laki-Laki', '0001-01-01', 0x303831333335353737333537, '0', 'Jemursari Timur V blok ji no 13', '6160', '444', '11', '60237', '376f11f9-7355-4115-8c66-15f275e2a322');
INSERT INTO `users` VALUES (188, 'Reffi Nugraha', 'revinugraha535@gmail.com', '2021-11-22 12:24:29', '$2y$10$VfyA8mz.dAo0KhQd2pkZN.DVgr4d8NaxUZ5PJREUBJ7O7IPguQP5u', 'user', NULL, 'default.png', 1, 'Indonesia', 250, NULL, '2021-11-22 12:17:40', '2021-11-22 12:26:09', 'Reffi Nugraha', 'Laki-Laki', '0001-01-01', 0x303831323930393938313530, '0', 'Jl. Baros no 69', '5965', '431', '9', '43161', '376f11f9-7355-4115-8c66-15f275e2a322');
INSERT INTO `users` VALUES (189, 'Muhammad Al Faris', 'farizie1999@gmail.com', '2021-11-22 13:13:58', '$2y$10$A3g/Y/jHpFhcGHb7g5ubbuZMriyL/eAlyVS.WTDkmGtDAucnQTl7m', 'user', NULL, 'default.png', 1, 'Indonesia', 251, NULL, '2021-11-22 13:13:21', '2021-11-22 13:15:24', 'Muhammad Al Faris', 'Laki-Laki', '0001-01-01', 0x303835383437353837303734, '0', 'Jl. Mentani I', '1830', '133', '11', '61152', '376f11f9-7355-4115-8c66-15f275e2a322');
INSERT INTO `users` VALUES (190, 'Sihotang', 'sobbinsihotang@yahoo.com', '2021-11-22 13:20:24', '$2y$10$qlyNm2PzwMhthmvFZCm8r.HeVJYlvXeeVPLuBarZCLBtXDjuK3px2', 'user', NULL, 'default.png', 1, 'Indonesia', 252, NULL, '2021-11-22 13:19:51', '2021-11-22 13:22:28', 'Sihotang', 'Laki-Laki', '0001-01-01', 0x303832313135373033343439, '0', 'Komplek Nata Endah 2 Sadang, Jalan Teratai Blok K No 48', '327', '22', '9', '40225', '376f11f9-7355-4115-8c66-15f275e2a322');
INSERT INTO `users` VALUES (191, 'Erika Fahrur', 'erikafahrur@gmail.com', '2021-11-22 13:26:57', '$2y$10$.U5UZ9N5pQZqF4g/P55REusL974nJBsEvEH5yMHeuK2JG92PjHqTu', 'user', NULL, 'default.png', 1, 'Indonesia', 253, NULL, '2021-11-22 13:25:00', '2021-11-22 13:28:51', 'Erika Fahrur', 'Laki-Laki', '0001-01-01', 0x303835323235333731373036, '0', 'Kembul sari, Suwawal Timur', '2251', '163', '10', '59456', '376f11f9-7355-4115-8c66-15f275e2a322');
INSERT INTO `users` VALUES (192, 'Johan Fakhrurrizal', 'johanfakhrizal@gmail.com', '2021-11-23 11:07:04', '$2y$10$.F8wpyEbIHyX.vPj/hZ0pe0J/hBl72C0uzAEugiwIJ17pGPWKrx1u', 'user', NULL, 'default.png', 1, 'Indonesia', 254, NULL, '2021-11-23 11:06:21', '2021-11-23 11:12:10', 'Johan Fakhrurrizal', 'Laki-Laki', '0001-01-01', 0x303831323232323133303535, '0', 'Jalan bogen 49a', '6155', '444', '11', '60133', '376f11f9-7355-4115-8c66-15f275e2a322');
INSERT INTO `users` VALUES (193, 'Harry Datum', 'harrydatumm@gmail.com', '2021-11-23 11:13:58', '$2y$10$LEJAHLT/bV6lOgRe8KsCRuDiMb2PdXaP0tx9tvkZ8dG9EuWBrgmlW', 'user', NULL, 'default.png', 1, 'Indonesia', 255, NULL, '2021-11-23 11:13:24', '2021-11-23 11:28:58', 'Harry Datum', 'Laki-Laki', '0001-01-01', 0x303835333230303936343439, '0', 'Jl.Sapta marga Blok M No 8', '2943', '211', '9', '46264', '376f11f9-7355-4115-8c66-15f275e2a322');
INSERT INTO `users` VALUES (194, 'Yunita', 'nhardiyanti2@gmail.com', '2021-11-23 11:33:24', '$2y$10$FLwbckZR6d1LfUq5r9CBpeqWLJkNXg.cb9.ytpQAgqGGihwtYbReC', 'user', NULL, 'default.png', 1, 'Indonesia', 256, NULL, '2021-11-23 11:32:56', '2021-11-23 11:35:35', 'Yunita Trri Hardiyanti', 'Perempuan', '1990-06-10', 0x303831323935353536303430, '3275095006900009', 'Jl Arwana 1 Blok B8 No 4 Perumahan Pondok Gede Permai', '755', '55', '9', '17424', '376f11f9-7355-4115-8c66-15f275e2a322');
INSERT INTO `users` VALUES (195, 'Achmad Arief', 'vacanaindonesia@gmail.com', '2021-11-23 11:37:06', '$2y$10$D..56zcGWtEoQ/t.HB9bG.b3YxaYvcYw2vKJTXrdQLVVjRn6N46xa', 'user', NULL, 'default.png', 1, 'Indonesia', 257, NULL, '2021-11-23 11:36:39', '2021-11-23 11:39:49', 'Achmad Arief', 'Laki-Laki', '1984-11-03', 0x30383138333534383030, '0', 'Jl. Taman Buana Permai Blk. C', '1573', '114', '1', '80118', '376f11f9-7355-4115-8c66-15f275e2a322');
INSERT INTO `users` VALUES (196, 'Putiari', 'putiarialainarizki@gmail.com', '2021-11-23 13:01:24', '$2y$10$Yb5Elz9HnL5p/yQAN4op9u.7p2ctI9FTm71ee0OvMFrX7I6eLUMhe', 'user', NULL, 'default.png', 1, 'Indonesia', 258, NULL, '2021-11-23 13:00:09', '2021-11-23 13:03:30', 'Putiari Alaina Rizki', 'Perempuan', '2000-08-18', 0x303837373032353436373136, '3515185801000003', 'Jl. Jeruk VI/37, Pondok Chandra, Tambak Rejo', '5647', '409', '11', '61256', '376f11f9-7355-4115-8c66-15f275e2a322');
INSERT INTO `users` VALUES (197, 'Sayyidah', 'unolaundry12@gmail.com', '2021-11-23 13:06:22', '$2y$10$KMZFx/Bjzl4q6u7mpoJEwOqKgx235/R467r85NDECUSlEB8fSkCH2', 'user', NULL, 'default.png', 1, 'Indonesia', 259, NULL, '2021-11-23 13:05:57', '2021-11-23 13:08:57', 'Sayyidah Rachmatul F', 'Perempuan', '1997-06-18', 0x303831333538343434353336, '35151848069700004', 'Jl. K. Zainal abidin no.38a, Tambak sumur', '5647', '409', '11', '61256', '376f11f9-7355-4115-8c66-15f275e2a322');
INSERT INTO `users` VALUES (198, 'Tharisa', 'thrsaulia@gmail.com', '2021-11-23 14:00:43', '$2y$10$n1JtFZe70CUF1JyyYO.mmuXqOzsv2KJ5mZf4LhRqXqmzNyFhcv9Fa', 'user', NULL, 'default.png', 1, 'Indonesia', 260, NULL, '2021-11-23 14:00:08', '2021-11-23 14:02:50', 'Tharisa Aulia', 'Perempuan', '2004-09-20', 0x303837383833393238373732, '3674015710040002', 'Jl. Palem no 9', '1585', '115', '9', '16516', '376f11f9-7355-4115-8c66-15f275e2a322');
INSERT INTO `users` VALUES (199, 'Nicky', 'balirent.net@gmail.com', '2021-11-23 14:04:24', '$2y$10$jXUIXhLzU8XWILe1XAz.p.ooLZ58PmQnSYbPmfkN1zyfRndT78VTy', 'user', NULL, 'default.png', 1, 'Indonesia', 261, NULL, '2021-11-23 14:03:57', '2021-11-23 14:11:58', 'Nicky Nakhlah', 'Laki-Laki', '2009-02-21', 0x30383138333534383030, '5171032103090004', 'Jl. Bukit Permai Lot B4 No. 8', '260', '17', '1', '80361', '376f11f9-7355-4115-8c66-15f275e2a322');
INSERT INTO `users` VALUES (200, 'Nazaruddin', 'ahmadinnajar@gmail.com', '2021-11-23 14:13:57', '$2y$10$X7dFULsttKzymwcNh5v1B.lMX7is6gmCDwS/cf3QKPugkrZZFvEMO', 'user', NULL, 'default.png', 1, 'Indonesia', 262, '9uvOITZjVBZrEqCRYMQh73MGqnpZXoy1VygkSYCKVeIXPnKunqCMPt8JxiS3', '2021-11-23 14:13:15', '2021-11-23 14:16:29', 'Muhammad Nazaruddin', 'Laki-Laki', '1982-04-04', 0x3038313136383530383938, '1114040402820003', 'Jl. Banda Aceh - Calang Km. 117', '47', '4', '21', '23656', '376f11f9-7355-4115-8c66-15f275e2a322');
INSERT INTO `users` VALUES (202, 'ruben saputra hasiholan', 'rubensiaputra21@gmail.com', NULL, '$2y$10$Khk/x05rFFETjzzhTR3nou01HzAF6oX/I0q48MOpXkkxovA0I..EO', 'user', NULL, 'default.png', 1, 'Indonesia', 264, 'ZiaQOZoyY3W6HCLY0aZPCQCtXUX0p44TP7x9f5W1KMWdbot38giB2oAGhgFb', '2021-11-25 16:03:53', '2021-11-25 16:03:53', NULL, NULL, NULL, 0x303831353130323739343036, NULL, NULL, '', NULL, NULL, NULL, 'b2029ba9-262f-4f3f-8975-9971aeca46e6');
INSERT INTO `users` VALUES (209, 'lusi', 'lusiistanti66@gmail.com', '2022-01-07 10:29:38', '$2y$10$0UV/tv3mzxt2Eu.NNVNYxus45BOsMGVSA9d9MUzi6iUETFdztdZZu', 'user', NULL, 'default.png', 1, 'Indonesia', 305, 'XSy0fUVmK8rfBXZUbF3GQ8L3duDqnyu5aC1ZEpnw5WrFbCH8PwErA91cSZm3', '2022-01-07 10:27:21', '2022-01-22 16:35:35', 'lusiana istanti', 'Perempuan', '2000-02-02', 0x303832323331323834353935, '3578266402000002', 'mansab 170b', '6145', '444', '11', '60116', '3b15a0c5-17e7-420c-a01a-b4b6f25d255a');
INSERT INTO `users` VALUES (210, 'Mauldy', 'mauldyri689@gmail.com', '2022-01-07 13:15:48', '$2y$10$oGOkcvb/Fp3Au20nc1ZT7u8f08gXzBS15Dt2ui8LNtVbC.v4uXKMO', 'user', NULL, 'default.png', 1, 'Indonesia', 306, NULL, '2022-01-07 13:11:48', '2022-01-07 13:17:10', 'Mauldy', 'Laki-Laki', '1996-12-01', 0x303835313535343036303235, '3578200112960003', 'Pondok Maritim Indah AL-3', '6159', '444', '11', '60222', '9717345a-a43b-428e-8f3c-ecdd8f6b5fb1');
INSERT INTO `users` VALUES (211, 'Demo', 'demo@ngelapak.co.id', '2022-01-14 10:23:41', '$2y$10$N5iyoxaEKGeSjA1Sshlv4eVWu50aMKrRKjFa4rXqyNyBpAXxv6oxy', 'user', NULL, 'default.png', 1, 'Indonesia', 307, NULL, '2022-01-14 10:22:15', '2022-01-14 10:23:41', NULL, NULL, NULL, 0x313233, NULL, NULL, '', NULL, NULL, NULL, '6e5f4d9b-3075-4b9e-82e0-60f0ba819933');
INSERT INTO `users` VALUES (212, 'Mauldy', 'mauldy.rizqia689@gmail.com', '2022-01-17 11:14:14', '$2y$10$W.86mOQLqM9v5UUhu9/bx.74WQBDxj1usfvOuluvT7IAPjOFZRQ8q', 'user', NULL, 'default.png', 1, 'Indonesia', 308, NULL, '2022-01-17 11:11:42', '2022-01-17 11:15:01', 'Mauldy Rizqia Ilhaq', 'Laki-Laki', '1996-12-01', 0x303835313535343036303235, '0', 'Pondok Maritim Indah AL-3 RT/RW 11/06 Balas Klumprik, Wiyung', '6159', '444', '11', '60222', '78de592d-332f-4041-a5ed-908b65d26903');
INSERT INTO `users` VALUES (213, 'Ngelapak', 'ngelapak@ngelapak.co.id', NULL, '$2y$10$g35q73X2Wt7VmsS20MrHlO0.b6BaTZDePq4FueIa5gQSDYOVjBV/u', 'user', NULL, 'default.png', 1, 'Indonesia', 309, NULL, '2022-01-26 15:25:19', '2022-01-26 15:25:19', NULL, NULL, NULL, 0x303831313233343536373839, NULL, NULL, '', NULL, NULL, NULL, '');
INSERT INTO `users` VALUES (214, 'Satrya', 'satrya@ifpro.co.id', '2022-02-08 16:58:27', '$2y$10$RXvfKKuMP4i6HwJbQqWwSu3ZUTmgGv2UXsYLHT3Cg35ysbGwQ4upy', 'user', NULL, 'default.png', 1, 'Indonesia', 310, NULL, '2022-02-08 16:57:29', '2022-02-08 16:59:08', 'Satrya Wira Wicaksana', 'Laki-Laki', '1997-02-08', 0x303831323230323035323432, '12345', 'Jalan Terus', '2118', '154', '6', '12345', '0eff9e6e-241c-4c04-b834-47634df6e19b');
INSERT INTO `users` VALUES (215, 'Lucy', 'lendrawanti@yahoo.com', '2022-02-14 21:34:38', '$2y$10$4iA7bExHTAi3Qcq.1QYmq.XesIlpYhlDJkEnr80ekC.uJdm8VguLK', 'user', NULL, 'default.png', 1, 'Indonesia', 311, NULL, '2022-02-14 21:33:04', '2022-02-14 21:40:04', 'Lucy Endrawanti', 'Perempuan', '1974-09-04', 0x303831323832343632303734, '3327094409740002', 'Jl PGT III no 19 dwikora', '2119', '154', '6', '13610', 'c46c60e7-93d7-4034-ac91-0259ad4b59a7');
INSERT INTO `users` VALUES (216, 'babi', 'babi@gmail.com', NULL, '$2y$10$vo7jjS49v5lhjVJghrlSU.Wo2v.iG.BqTLEiPludhjm/vaena0Jku', 'user', NULL, 'default.png', 1, 'Indonesia', 312, NULL, '2022-02-15 08:30:02', '2022-02-15 08:30:02', NULL, NULL, NULL, 0x303831333232323933343438, NULL, NULL, '', NULL, NULL, NULL, 'fc06461a-e998-42b4-8ffe-18e5f0cc56c8');
INSERT INTO `users` VALUES (217, 'arif bw', 'arifbw31@gmail.com', '2022-02-15 08:44:05', '$2y$10$opmefv9Vkc/fd48hx0.V/uQ6wGOpfxOE/6/DEEcfcqU4SA8itqpXS', 'user', NULL, 'default.png', 1, 'Indonesia', 313, NULL, '2022-02-15 08:36:51', '2022-02-15 08:44:05', NULL, NULL, NULL, 0x303835363032313331353333, NULL, NULL, '', NULL, NULL, NULL, '6b43805e-164c-448b-989d-e3a740c2ffd6');
INSERT INTO `users` VALUES (218, 'Wypate', 'wypatelancar@gmail.com', '2022-02-20 20:43:18', '$2y$10$6tYQwQQkJb31mv4J8z4yweMmos7KkUVTih2D965Xv6pp5ZJs1OYXS', 'user', NULL, 'default.png', 1, 'Indonesia', 314, NULL, '2022-02-20 20:40:26', '2022-02-20 20:43:18', NULL, NULL, NULL, 0x303831323833383537373735, NULL, NULL, '', NULL, NULL, NULL, '829bf509-87b6-4768-ad7e-64166294e35e');

SET FOREIGN_KEY_CHECKS = 1;
