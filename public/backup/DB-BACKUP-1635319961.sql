DROP TABLE IF EXISTS accounts;

CREATE TABLE `accounts` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `account_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `opening_date` date NOT NULL,
  `account_number` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `account_currency` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL,
  `opening_balance` decimal(10,2) NOT NULL,
  `note` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_id` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS activity_logs;

CREATE TABLE `activity_logs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `related_to` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `related_id` bigint(20) DEFAULT NULL,
  `activity` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `company_id` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO activity_logs VALUES('1','projects','1','Uploaded File','6','5','2021-10-08 22:26:35','2021-10-08 22:26:35');
INSERT INTO activity_logs VALUES('2','projects','2','Uploaded File','47','46','2021-10-14 20:42:54','2021-10-14 20:42:54');
INSERT INTO activity_logs VALUES('3','projects','2','File Removed','47','46','2021-10-15 11:22:58','2021-10-15 11:22:58');
INSERT INTO activity_logs VALUES('4','projects','3','Uploaded File','69','68','2021-10-21 15:32:51','2021-10-21 15:32:51');
INSERT INTO activity_logs VALUES('5','projects','3','Updated Project','69','68','2021-10-21 16:23:09','2021-10-21 16:23:09');
INSERT INTO activity_logs VALUES('6','projects','3','File Removed','69','68','2021-10-21 17:07:13','2021-10-21 17:07:13');
INSERT INTO activity_logs VALUES('7','projects','4','Uploaded File','69','68','2021-10-21 17:56:05','2021-10-21 17:56:05');
INSERT INTO activity_logs VALUES('8','projects','4','File Removed','69','68','2021-10-22 09:41:38','2021-10-22 09:41:38');
INSERT INTO activity_logs VALUES('9','projects','5','Uploaded File','69','68','2021-10-22 11:24:51','2021-10-22 11:24:51');
INSERT INTO activity_logs VALUES('10','projects','5','Updated Project','69','68','2021-10-22 11:25:05','2021-10-22 11:25:05');
INSERT INTO activity_logs VALUES('11','projects','5','File Removed','69','68','2021-10-26 13:11:09','2021-10-26 13:11:09');
INSERT INTO activity_logs VALUES('12','projects','6','Uploaded File','69','68','2021-10-26 13:29:07','2021-10-26 13:29:07');
INSERT INTO activity_logs VALUES('13','projects','7','Uploaded File','121','173','2021-10-27 12:52:58','2021-10-27 12:52:58');
INSERT INTO activity_logs VALUES('14','projects','8','Uploaded File','121','173','2021-10-27 13:12:59','2021-10-27 13:12:59');
INSERT INTO activity_logs VALUES('15','projects','9','Uploaded File','121','173','2021-10-27 13:18:42','2021-10-27 13:18:42');
INSERT INTO activity_logs VALUES('16','projects','8','File Removed','121','173','2021-10-27 13:19:48','2021-10-27 13:19:48');
INSERT INTO activity_logs VALUES('17','projects','7','File Removed','121','173','2021-10-27 13:19:53','2021-10-27 13:19:53');
INSERT INTO activity_logs VALUES('18','projects','6','Updated Project','69','68','2021-10-27 13:58:32','2021-10-27 13:58:32');
INSERT INTO activity_logs VALUES('19','projects','6','Updated Project','69','68','2021-10-27 13:58:44','2021-10-27 13:58:44');
INSERT INTO activity_logs VALUES('20','projects','6','Updated Project','69','68','2021-10-27 14:00:54','2021-10-27 14:00:54');
INSERT INTO activity_logs VALUES('21','projects','6','Updated Project','69','68','2021-10-27 14:01:12','2021-10-27 14:01:12');
INSERT INTO activity_logs VALUES('22','projects','6','Updated Project','69','68','2021-10-27 14:01:24','2021-10-27 14:01:24');
INSERT INTO activity_logs VALUES('23','projects','6','Updated Project','69','68','2021-10-27 14:05:18','2021-10-27 14:05:18');
INSERT INTO activity_logs VALUES('24','projects','9','Updated Project','121','173','2021-10-27 14:10:11','2021-10-27 14:10:11');



DROP TABLE IF EXISTS alamat;

CREATE TABLE `alamat` (
  `id` int(11) NOT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `kota` varchar(255) DEFAULT NULL,
  `prov` varchar(255) DEFAULT NULL,
  `kode_pos` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




DROP TABLE IF EXISTS chart_of_accounts;

CREATE TABLE `chart_of_accounts` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_id` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS chat_group_users;

CREATE TABLE `chat_group_users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `group_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS chat_groups;

CREATE TABLE `chat_groups` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_id` bigint(20) NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS chat_messages;

CREATE TABLE `chat_messages` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `from` bigint(20) NOT NULL,
  `to` bigint(20) NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `attachment` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 0,
  `company_id` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS cm_email_subscribers;

CREATE TABLE `cm_email_subscribers` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip_address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS cm_faqs;

CREATE TABLE `cm_faqs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `question` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `answer` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS cm_features;

CREATE TABLE `cm_features` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `icon` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO cm_features VALUES('1','icon icon-Life-Safer','Website Builder','Manage website directly from your browser','','');
INSERT INTO cm_features VALUES('2','icon icon-Duplicate-Window','Emails reminder','Get remiders before your subscription ends','','');
INSERT INTO cm_features VALUES('3','icon icon-Fingerprint','Support','Real time Chat with staffs, customers and private groups','','');
INSERT INTO cm_features VALUES('4','icon icon-Pantone','Online Payments','Accept Online Payments from different providers','','');
INSERT INTO cm_features VALUES('5','icon icon-Life-Safer','Website Builder','Manage website directly from your browser','','');
INSERT INTO cm_features VALUES('6','icon icon-Duplicate-Window','Emails reminder','Get remiders before your subscription ends','','');
INSERT INTO cm_features VALUES('7','icon icon-Fingerprint','Support','Real time Chat with staffs, customers and private groups','','');
INSERT INTO cm_features VALUES('8','icon icon-Pantone','Online Payments','Accept Online Payments from different providers','','');



DROP TABLE IF EXISTS companies;

CREATE TABLE `companies` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `business_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(10) unsigned NOT NULL,
  `package_id` int(11) DEFAULT NULL,
  `package_type` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `membership_type` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `valid_to` date NOT NULL,
  `last_email` date DEFAULT NULL,
  `websites_limit` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `recurring_transaction` varchar(3) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `online_payment` varchar(3) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `alamat` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kota` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `prov` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kode_pos` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cabang` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deskripsi` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slogan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ig` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fb` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `web` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ft_prd1` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ft_prd2` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ft_prd3` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ft_prd4` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ft_prd5` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `splash` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bg_apk` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `judul_camp` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sub_judul` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `key_brand` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi_camp` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cover` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `thumbnail` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo_camp` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gambar_camp` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=176 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO companies VALUES('1','tes1','1','3','yearly','member','2022-10-14','','1','Yes','Yes','2021-09-30 05:30:51','2021-10-14 20:56:55','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('2','tes2','1','6','yearly','trial','2021-10-08','','Unlimited','Yes','Yes','2021-10-01 03:37:49','2021-10-01 03:37:49','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('3','Tes sign up','1','1','monthly','trial','2021-10-11','','3','No','No','2021-10-04 12:33:24','2021-10-04 12:33:24','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('4','tesssss','1','2','monthly','trial','2021-10-11','','10','Yes','No','2021-10-04 13:08:00','2021-10-04 13:08:00','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('5','Polinema','1','6','yearly','member','2021-10-17','','Unlimited','Yes','Yes','2021-10-04 13:12:15','2021-10-14 16:04:31','tes','Kediri','Jawa timur','64211','ya','','','','','','logo_1634199669.jpg','ftprd1_1634200377.png','ftprd2_1634200377.png','ftprd3_1634200377.png','ftprd4_1634200377.jpg','','splash_1634201301.jpg','bg_apk_1634201223.jpg','jdul camp','sub','key','des','cover_1634202271.jpg','tmnail_1634202271.jpg','logocamp_1634202271.jpg','gbrcamp_1634202271.jpg');
INSERT INTO companies VALUES('6','','1','','','trial','2021-10-12','','','','','2021-10-05 10:38:47','2021-10-05 10:38:47','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('7','','1','','','trial','2021-10-12','','','','','2021-10-05 10:42:26','2021-10-05 10:42:26','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('8','','1','','','trial','2021-10-12','','','','','2021-10-05 10:46:27','2021-10-05 10:46:27','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('9','','1','','','trial','2021-10-12','','','','','2021-10-05 10:48:23','2021-10-05 10:48:23','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('10','','1','','','trial','2021-10-12','','','','','2021-10-05 10:50:57','2021-10-05 10:50:57','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('11','','1','','','trial','2021-10-12','','','','','2021-10-05 10:57:39','2021-10-05 10:57:39','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('12','','1','','','trial','2021-10-12','','','','','2021-10-05 11:02:50','2021-10-05 11:02:50','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('13','','1','','','trial','2021-10-12','','','','','2021-10-05 11:10:19','2021-10-05 11:10:19','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('14','','1','','','trial','2021-10-12','','','','','2021-10-05 11:38:48','2021-10-05 11:38:48','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('15','','1','','','trial','2021-10-12','','','','','2021-10-05 11:43:22','2021-10-05 11:43:22','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('16','','1','','','trial','2021-10-12','','','','','2021-10-05 11:45:57','2021-10-05 11:45:57','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('17','','1','','','trial','2021-10-13','','','','','2021-10-06 10:12:05','2021-10-06 10:12:05','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('18','','1','','','trial','2021-10-13','','','','','2021-10-06 10:50:05','2021-10-06 10:50:05','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('19','Tescoba3','1','6','yearly','member','2021-10-13','','Unlimited','Yes','Yes','2021-10-06 11:08:44','2021-10-06 11:08:44','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('20','','1','','','trial','2021-10-13','','','','','2021-10-06 13:39:54','2021-10-06 13:39:54','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('21','','1','','','trial','2021-10-13','','','','','2021-10-06 15:00:02','2021-10-06 15:00:02','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('22','','1','','','trial','2021-10-13','','','','','2021-10-06 15:08:47','2021-10-06 15:08:47','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('23','','1','','','trial','2021-10-14','','','','','2021-10-07 09:22:13','2021-10-07 09:22:13','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('24','','1','','','trial','2021-10-14','','','','','2021-10-07 09:56:56','2021-10-07 09:56:56','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('25','','1','','','trial','2021-10-14','','','','','2021-10-07 10:12:25','2021-10-07 10:12:25','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('26','tes edit perusahaan','1','3','','member','2022-10-21','','1','Yes','Yes','2021-10-07 11:46:27','2021-10-21 11:00:33','jalan2','kota','prov','12121','ya','r','cobaa','ig','fb','web','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('27','','1','','','trial','2021-10-15','','','','','2021-10-08 22:47:43','2021-10-08 22:47:43','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('28','','1','','','trial','2021-10-18','','','','','2021-10-11 11:11:15','2021-10-11 11:11:15','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('29','coba','1','','','trial','2021-10-18','','','','','2021-10-11 15:49:01','2021-10-19 11:22:32','Desa Pelem, Dusun Tempuran','Kediri','Jawa Timur','64211','ya','tes','','','','','logo_1634617351.png','ftprd1_1634617351.png','','','','','splash_1634617351.jpg','bg_apk_1634617352.jpg','rse','se','sese','sese','cover_1634617352.jpg','tmnail_1634617352.jpg','logocamp_1634617352.png','gbrcamp_1634617352.jpg');
INSERT INTO companies VALUES('30','','1','','','trial','2021-10-18','','','','','2021-10-11 15:52:18','2021-10-11 15:52:18','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('31','coba','1','4','monthly','trial','2021-10-18','','3','No','No','2021-10-11 16:03:23','2021-10-11 16:22:55','coba','sby','jawa timur','643221','ya','coba','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('32','','1','4','monthly','trial','2021-10-14','','1','No','No','2021-10-11 16:48:50','2021-10-11 16:48:50','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('33','','1','4','monthly','trial','2021-10-15','','1','No','No','2021-10-12 09:40:19','2021-10-12 09:40:19','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('34','','1','4','monthly','member','2021-10-15','','1','No','No','2021-10-12 10:25:01','2021-10-12 10:25:01','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('35','','1','4','monthly','trial','2021-10-15','','1','No','No','2021-10-12 13:59:36','2021-10-12 13:59:36','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('37','','1','1','yearly','trial','2021-10-17','','1','Yes','Yes','2021-10-14 10:01:57','2021-10-14 10:01:57','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('38','','1','1','monthly','trial','2021-10-17','','1','Yes','Yes','2021-10-14 11:16:32','2021-10-14 11:16:32','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('39','ngelapak','1','1','monthly','trial','2021-10-17','','1','Yes','Yes','2021-10-14 11:39:09','2021-10-14 12:08:15','royal','surabaya','jawa timur','60225','tidak','platform','aplikasi','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('40','ngelapak','1','1','monthly','trial','2021-10-17','','1','Yes','Yes','2021-10-14 14:59:29','2021-10-14 16:24:16','Ruko Soho Royal Residence BS-10 No. 07','Surabaya','jawa timur','60227','tidak','platform pembuatan aplikasi','milyaran aplikasi','','','','logo_1634203456.png','ftprd1_1634203456.png','','','','','splash_1634203456.png','bg_apk_1634203456.png','lalla','lallla','lallla','lallala','cover_1634203456.png','tmnail_1634203456.png','logocamp_1634203456.png','gbrcamp_1634203456.png');
INSERT INTO companies VALUES('41','','1','1','monthly','trial','2021-11-13','','1','Yes','Yes','2021-10-14 18:37:44','2021-10-14 18:37:44','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('42','','1','1','monthly','trial','2021-11-13','','1','Yes','Yes','2021-10-14 18:53:50','2021-10-14 18:53:50','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('45','','1','2','monthly','member','2021-12-13','','1','Yes','Yes','2021-10-14 19:21:47','2021-10-14 19:33:27','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('46','','1','3','yearly','member','2021-12-13','','1','Yes','Yes','2021-10-14 19:50:35','2021-10-14 19:52:55','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('48','','1','1','monthly','member','2021-12-14','','1','Yes','Yes','2021-10-15 12:26:56','2021-10-15 12:30:12','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('49','','1','1','monthly','member','2021-12-14','','1','Yes','Yes','2021-10-15 19:29:29','2021-10-15 19:29:29','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('50','','1','1','monthly','member','2021-12-14','','1','Yes','Yes','2021-10-15 19:29:30','2021-10-15 19:29:30','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('51','','1','3','monthly','member','2021-12-14','','1','Yes','Yes','2021-10-15 19:29:32','2021-10-15 19:29:32','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('52','','1','1','monthly','member','2021-12-14','','1','Yes','Yes','2021-10-15 19:30:13','2021-10-15 19:30:13','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('53','','1','1','monthly','member','2021-12-14','','1','Yes','Yes','2021-10-15 19:30:16','2021-10-15 19:39:25','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('54','YORRI Eatery','1','3','monthly','member','2021-12-14','','1','Yes','Yes','2021-10-15 19:30:20','2021-10-19 12:34:48','Pandugo Baru XIII P1A','Surabaya','Jawa Timur','60297','ya','YORRI adalah penyedia makanan dan minuman Vegan dan Gluten Free yang sehat, bergizi dan lezat, serta berkontribusi baik terhadap sesama, lingkungan dan hewan.','Love Yourself, Eat Healthy Food','yorrieatery','YORRI Eatery','','logo_1634621685.png','ftprd1_1634621685.jpg','ftprd2_1634621686.jpg','ftprd3_1634621686.jpg','ftprd4_1634621686.jpg','','splash_1634621686.png','bg_apk_1634621687.png','Healthy Foods and Drinks','Vegan & Gluten Free','Vegan & Gluten Free','YORRI Eatery menyediakan makanan dan minuman Vegan dan Gluten Free tanpa 4P (Pemanis, Penguat rasa / Perisa, Pewarna, dan Pengawet) dengan menggunakan bahan baku lokal yang bermutu dari petani lokal Indonesia.','cover_1634621687.png','tmnail_1634621687.png','logocamp_1634621687.png','gbrcamp_1634621687.png');
INSERT INTO companies VALUES('55','','1','1','monthly','member','2021-12-14','','1','Yes','Yes','2021-10-15 19:30:22','2021-10-15 19:33:29','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('56','CV. Smart Batik Indonesia','1','3','monthly','member','2021-12-14','','1','Yes','Yes','2021-10-15 19:30:31','2021-10-15 20:05:49','Ketanggungan, WB 2/429, RT 057, RW 012, Kelurahan Wirobrajan, Kecamatan Wirobrajan','Yogyakarta','Daerah Istimewa Yogyakarta','55252','tidak','CV. Smart Batik Indonesia memproduksi dan memasarkan produk batik buatan tangan dengan motif unik, seperti motif sains, teknologi, olahraga, musik, lingkungan, perikanan, pertanian, pariwisata, dan budaya. Perusahaan juga menyediakan layanan desain kustom untuk pembuatan seragam instansi, organisasi, atau komunitas.','Inovatif, Edukatif, dan Kontributif','@smart_batik','Smart Batik Indonesia','www.smartbatikindonesia.com','logo_1634303135.png','ftprd1_1634303137.jpg','ftprd2_1634303137.jpg','ftprd3_1634303137.jpg','ftprd4_1634303137.jpg','','splash_1634303137.png','bg_apk_1634303138.jpg','Produsen Batik Tematik Kustom untuk Instansi Pemerintah, Organisasi, dan Asosiasi','Batik Tematik','Batik Unik Handmade','CV. Smart Batik Indonesia merupakan perusahaan batik yang telah dipercaya puluhan instansi pemerintah dan asosiasi untuk mendesain batik kustom sesuai karakter masing-masing organisasi. Perusahaan juga menjual produk batik dengan motif-motif unik dan anti','cover_1634303140.png','tmnail_1634303142.png','logocamp_1634303148.png','gbrcamp_1634303149.jpg');
INSERT INTO companies VALUES('57','','1','2','monthly','member','2021-12-14','','1','Yes','Yes','2021-10-15 19:32:02','2021-10-15 19:47:31','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('58','','1','1','monthly','member','2021-12-14','','1','Yes','Yes','2021-10-15 19:32:03','2021-10-15 19:35:15','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('59','','1','3','monthly','member','2021-12-14','','1','Yes','Yes','2021-10-15 19:33:14','2021-10-15 19:33:14','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('60','','1','3','monthly','member','2021-12-14','','1','Yes','Yes','2021-10-15 19:35:12','2021-10-15 19:35:12','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('61','','1','1','monthly','member','2021-12-14','','1','Yes','Yes','2021-10-15 19:38:47','2021-10-15 19:38:47','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('62','','1','1','monthly','member','2021-12-14','','1','Yes','Yes','2021-10-15 19:39:52','2021-10-15 19:39:52','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('63','','1','2','monthly','member','2021-12-14','','1','Yes','Yes','2021-10-15 19:51:22','2021-10-15 19:56:45','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('64','','1','3','monthly','member','2021-12-14','','1','Yes','Yes','2021-10-15 19:54:29','2021-10-15 19:54:29','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('65','','1','3','monthly','member','2021-12-14','','1','Yes','Yes','2021-10-15 20:05:12','2021-10-15 20:05:12','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('66','Polinema','1','3','monthly','member','2022-10-21','','1','Yes','Yes','2021-10-19 11:25:36','2021-10-21 11:33:22','Desa Pelem, Dusun Tempuran','Kediri','Pare','64211','','2','','','','','logo_1634620001.png','ftprd1_1634620001.jpg','ftprd2_1634620001.jpg','ftprd3_1634620004.jpg','ftprd4_1634620005.jpg','','splash_1634620005.jpg','bg_apk_1634620005.jpg','dsd','sd','asd','asd','cover_1634620005.jpg','tmnail_1634620006.jpg','logocamp_1634620006.png','gbrcamp_1634620006.jpg');
INSERT INTO companies VALUES('67','','1','3','monthly','trial','2021-11-18','','1','Yes','Yes','2021-10-19 22:22:13','2021-10-19 22:22:13','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('68','','1','1','yearly','member','2021-11-21','','1','Yes','Yes','2021-10-21 11:40:44','2021-10-21 11:59:57','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('70','','1','1','monthly','trial','2021-11-08','','1','Yes','Yes','2021-10-25 10:21:20','2021-10-25 10:21:20','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('71','','1','3','monthly','trial','2021-11-08','','1','Yes','Yes','2021-10-25 10:29:31','2021-10-25 10:29:31','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('74','','1','1','monthly','trial','2021-11-08','','1','Yes','Yes','2021-10-25 10:37:48','2021-10-25 10:37:48','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('77','','1','2','monthly','trial','2021-11-08','','1','Yes','Yes','2021-10-25 10:41:39','2021-10-25 10:41:39','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('80','','1','1','yearly','trial','2021-11-08','','1','Yes','Yes','2021-10-25 10:43:33','2021-10-25 10:43:33','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('81','','1','1','monthly','trial','2021-11-08','','1','Yes','Yes','2021-10-25 10:44:17','2021-10-25 10:44:17','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('87','','1','1','monthly','trial','2021-11-08','','1','Yes','Yes','2021-10-25 10:51:08','2021-10-25 10:51:08','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('90','','1','1','monthly','trial','2021-11-08','','1','Yes','Yes','2021-10-25 10:53:39','2021-10-25 10:53:39','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('91','','1','1','monthly','trial','2021-11-08','','1','Yes','Yes','2021-10-25 10:54:45','2021-10-25 10:54:45','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('92','','1','1','monthly','trial','2021-11-08','','1','Yes','Yes','2021-10-25 10:55:28','2021-10-25 10:55:28','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('95','','1','1','monthly','trial','2021-11-08','','1','Yes','Yes','2021-10-25 10:56:40','2021-10-25 10:56:40','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('98','','1','1','monthly','trial','2021-11-08','','1','Yes','Yes','2021-10-25 10:57:27','2021-10-25 10:57:27','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('103','','1','1','monthly','trial','2021-11-08','','1','Yes','Yes','2021-10-25 10:59:47','2021-10-25 10:59:47','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('106','','1','1','monthly','trial','2021-11-08','','1','Yes','Yes','2021-10-25 11:02:18','2021-10-25 11:02:18','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('107','','1','1','monthly','trial','2021-11-08','','1','Yes','Yes','2021-10-25 11:05:27','2021-10-25 11:05:27','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('109','','1','1','monthly','trial','2021-11-08','','1','Yes','Yes','2021-10-25 11:06:07','2021-10-25 11:06:07','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('112','','1','1','monthly','trial','2021-11-08','','1','Yes','Yes','2021-10-25 11:07:41','2021-10-25 11:07:41','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('113','','1','1','monthly','trial','2021-11-08','','1','Yes','Yes','2021-10-25 11:07:52','2021-10-25 11:07:52','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('119','','1','1','monthly','trial','2021-11-08','','1','Yes','Yes','2021-10-25 11:12:28','2021-10-25 11:12:28','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('120','','1','1','monthly','trial','2021-11-08','','1','Yes','Yes','2021-10-25 11:15:12','2021-10-25 11:15:12','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('121','','1','1','monthly','trial','2021-11-08','','1','Yes','Yes','2021-10-25 11:15:25','2021-10-25 11:15:25','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('125','','1','1','monthly','trial','2021-11-08','','1','Yes','Yes','2021-10-25 11:18:06','2021-10-25 11:18:06','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('130','','1','1','monthly','trial','2021-11-08','','1','Yes','Yes','2021-10-25 11:20:39','2021-10-25 11:20:39','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('131','','1','1','monthly','trial','2021-11-08','','1','Yes','Yes','2021-10-25 11:22:53','2021-10-25 11:22:53','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('139','','1','1','monthly','trial','2021-11-08','','1','Yes','Yes','2021-10-25 11:28:14','2021-10-25 11:28:14','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('142','','1','1','monthly','trial','2021-11-08','','1','Yes','Yes','2021-10-25 12:31:21','2021-10-25 12:31:21','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('143','','1','3','yearly','trial','2021-11-08','','1','Yes','Yes','2021-10-25 12:33:25','2021-10-25 12:33:25','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('144','','1','1','yearly','trial','2021-11-08','','1','Yes','Yes','2021-10-25 12:41:21','2021-10-25 12:41:21','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('145','','1','1','yearly','trial','2021-11-09','','1','Yes','Yes','2021-10-26 09:49:59','2021-10-26 09:49:59','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('146','','1','1','monthly','trial','2021-11-09','','1','Yes','Yes','2021-10-26 04:47:07','2021-10-26 04:47:07','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('147','tes','1','3','yearly','trial','2021-11-09','','1','Yes','Yes','2021-10-26 04:48:59','2021-10-26 04:55:09','Desa Pelem','Kediri','Jawa Timur','64211','','tes','','','','','logo_1635224108.png','ftprd1_1635224108.png','','','','','splash_1635224108.jpg','bg_apk_1635224108.jpg','tes','tes','tes','tes','cover_1635224108.jpg','tmnail_1635224109.jpg','logocamp_1635224109.png','gbrcamp_1635224109.png');
INSERT INTO companies VALUES('149','','1','3','monthly','trial','2021-11-09','','1','Yes','Yes','2021-10-26 05:08:03','2021-10-26 05:08:03','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('150','','1','3','yearly','trial','2021-11-09','','1','Yes','Yes','2021-10-26 05:21:06','2021-10-26 05:21:06','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('151','','1','3','yearly','trial','2021-11-09','','1','Yes','Yes','2021-10-26 05:23:09','2021-10-26 05:23:09','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('153','','1','1','monthly','trial','2021-11-09','','1','Yes','Yes','2021-10-26 07:04:20','2021-10-26 07:04:20','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('154','','1','2','monthly','trial','2021-11-09','','1','Yes','Yes','2021-10-26 07:21:05','2021-10-26 07:21:05','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('159','','1','3','yearly','trial','2021-11-25','','1','Yes','Yes','2021-10-26 09:16:17','2021-10-26 09:16:17','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('169','','1','3','yearly','trial','2021-11-26','','1','Yes','Yes','2021-10-27 00:23:28','2021-10-27 00:23:28','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('171','','1','1','monthly','trial','2021-11-10','','1','Yes','Yes','2021-10-27 04:25:40','2021-10-27 04:25:40','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('172','','1','1','monthly','trial','2021-11-10','','1','Yes','Yes','2021-10-27 04:35:56','2021-10-27 04:35:56','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('173','','1','3','yearly','trial','2021-11-10','','1','Yes','Yes','2021-10-27 05:28:47','2021-10-27 05:28:47','','','','','','','','','','','','','','','','','','','','','','','','','','');
INSERT INTO companies VALUES('174','','1','1','yearly','trial','2021-11-10','','1','Yes','Yes','2021-10-27 05:29:35','2021-10-27 05:29:35','','','','','','','','','','','','','','','','','','','','','','','','','','');



DROP TABLE IF EXISTS company_email_template;

CREATE TABLE `company_email_template` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `related_to` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_id` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS company_settings;

CREATE TABLE `company_settings` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_id` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS contact_groups;

CREATE TABLE `contact_groups` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `note` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_id` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS contacts;

CREATE TABLE `contacts` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `profile_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vat_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reg_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_phone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `currency` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `linkedin` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remarks` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `group_id` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) NOT NULL,
  `custom_fields` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS currency_rates;

CREATE TABLE `currency_rates` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `currency` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rate` decimal(10,6) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=169 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO currency_rates VALUES('1','AED','4.101083','','');
INSERT INTO currency_rates VALUES('2','AFN','85.378309','','');
INSERT INTO currency_rates VALUES('3','ALL','123.510844','','');
INSERT INTO currency_rates VALUES('4','AMD','548.849773','','');
INSERT INTO currency_rates VALUES('5','ANG','2.008050','','');
INSERT INTO currency_rates VALUES('6','AOA','556.155120','','');
INSERT INTO currency_rates VALUES('7','ARS','70.205746','','');
INSERT INTO currency_rates VALUES('8','AUD','1.809050','','');
INSERT INTO currency_rates VALUES('9','AWG','2.009782','','');
INSERT INTO currency_rates VALUES('10','AZN','1.833159','','');
INSERT INTO currency_rates VALUES('11','BAM','1.966840','','');
INSERT INTO currency_rates VALUES('12','BBD','2.245460','','');
INSERT INTO currency_rates VALUES('13','BDT','95.162306','','');
INSERT INTO currency_rates VALUES('14','BGN','1.952383','','');
INSERT INTO currency_rates VALUES('15','BHD','0.421787','','');
INSERT INTO currency_rates VALUES('16','BIF','2117.865003','','');
INSERT INTO currency_rates VALUES('17','BMD','1.116545','','');
INSERT INTO currency_rates VALUES('18','BND','1.583270','','');
INSERT INTO currency_rates VALUES('19','BOB','7.718004','','');
INSERT INTO currency_rates VALUES('20','BRL','5.425949','','');
INSERT INTO currency_rates VALUES('21','BSD','1.121775','','');
INSERT INTO currency_rates VALUES('22','BTC','0.000244','','');
INSERT INTO currency_rates VALUES('23','BTN','82.818317','','');
INSERT INTO currency_rates VALUES('24','BWP','12.683055','','');
INSERT INTO currency_rates VALUES('25','BYN','2.621037','','');
INSERT INTO currency_rates VALUES('26','BYR','9999.999999','','');
INSERT INTO currency_rates VALUES('27','BZD','2.261248','','');
INSERT INTO currency_rates VALUES('28','CAD','1.552879','','');
INSERT INTO currency_rates VALUES('29','CDF','1898.127343','','');
INSERT INTO currency_rates VALUES('30','CHF','1.056023','','');
INSERT INTO currency_rates VALUES('31','CLF','0.033950','','');
INSERT INTO currency_rates VALUES('32','CLP','936.781769','','');
INSERT INTO currency_rates VALUES('33','CNY','7.827878','','');
INSERT INTO currency_rates VALUES('34','COP','4491.872864','','');
INSERT INTO currency_rates VALUES('35','CRC','635.520417','','');
INSERT INTO currency_rates VALUES('36','CUC','1.116545','','');
INSERT INTO currency_rates VALUES('37','CUP','29.588450','','');
INSERT INTO currency_rates VALUES('38','CVE','110.887227','','');
INSERT INTO currency_rates VALUES('39','CZK','26.906059','','');
INSERT INTO currency_rates VALUES('40','DJF','198.432393','','');
INSERT INTO currency_rates VALUES('41','DKK','7.472892','','');
INSERT INTO currency_rates VALUES('42','DOP','60.196240','','');
INSERT INTO currency_rates VALUES('43','DZD','134.499489','','');
INSERT INTO currency_rates VALUES('44','EGP','17.585483','','');
INSERT INTO currency_rates VALUES('45','ERN','16.748349','','');
INSERT INTO currency_rates VALUES('46','ETB','36.696587','','');
INSERT INTO currency_rates VALUES('47','EUR','1.000000','','');
INSERT INTO currency_rates VALUES('48','FJD','2.549240','','');
INSERT INTO currency_rates VALUES('49','FKP','0.908257','','');
INSERT INTO currency_rates VALUES('50','GBP','0.907964','','');
INSERT INTO currency_rates VALUES('51','GEL','3.115301','','');
INSERT INTO currency_rates VALUES('52','GGP','0.908257','','');
INSERT INTO currency_rates VALUES('53','GHS','6.220337','','');
INSERT INTO currency_rates VALUES('54','GIP','0.908257','','');
INSERT INTO currency_rates VALUES('55','GMD','56.605069','','');
INSERT INTO currency_rates VALUES('56','GNF','9999.999999','','');
INSERT INTO currency_rates VALUES('57','GTQ','8.576324','','');
INSERT INTO currency_rates VALUES('58','GYD','234.489495','','');
INSERT INTO currency_rates VALUES('59','HKD','8.674753','','');
INSERT INTO currency_rates VALUES('60','HNL','27.678062','','');
INSERT INTO currency_rates VALUES('61','HRK','7.590196','','');
INSERT INTO currency_rates VALUES('62','HTG','106.356510','','');
INSERT INTO currency_rates VALUES('63','HUF','341.150311','','');
INSERT INTO currency_rates VALUES('64','IDR','9999.999999','','');
INSERT INTO currency_rates VALUES('65','ILS','4.159226','','');
INSERT INTO currency_rates VALUES('66','IMP','0.908257','','');
INSERT INTO currency_rates VALUES('67','INR','82.763894','','');
INSERT INTO currency_rates VALUES('68','IQD','1339.198712','','');
INSERT INTO currency_rates VALUES('69','IRR','9999.999999','','');
INSERT INTO currency_rates VALUES('70','ISK','151.202539','','');
INSERT INTO currency_rates VALUES('71','JEP','0.908257','','');
INSERT INTO currency_rates VALUES('72','JMD','151.606351','','');
INSERT INTO currency_rates VALUES('73','JOD','0.791685','','');
INSERT INTO currency_rates VALUES('74','JPY','118.278988','','');
INSERT INTO currency_rates VALUES('75','KES','115.283224','','');
INSERT INTO currency_rates VALUES('76','KGS','81.395812','','');
INSERT INTO currency_rates VALUES('77','KHR','4603.144194','','');
INSERT INTO currency_rates VALUES('78','KMF','495.355724','','');
INSERT INTO currency_rates VALUES('79','KPW','1004.922902','','');
INSERT INTO currency_rates VALUES('80','KRW','1372.190164','','');
INSERT INTO currency_rates VALUES('81','KWD','0.344879','','');
INSERT INTO currency_rates VALUES('82','KYD','0.934921','','');
INSERT INTO currency_rates VALUES('83','KZT','456.318281','','');
INSERT INTO currency_rates VALUES('84','LAK','9978.233671','','');
INSERT INTO currency_rates VALUES('85','LBP','1696.373291','','');
INSERT INTO currency_rates VALUES('86','LKR','206.967335','','');
INSERT INTO currency_rates VALUES('87','LRD','221.076044','','');
INSERT INTO currency_rates VALUES('88','LSL','18.121543','','');
INSERT INTO currency_rates VALUES('89','LTL','3.296868','','');
INSERT INTO currency_rates VALUES('90','LVL','0.675387','','');
INSERT INTO currency_rates VALUES('91','LYD','1.557311','','');
INSERT INTO currency_rates VALUES('92','MAD','10.730569','','');
INSERT INTO currency_rates VALUES('93','MDL','19.734707','','');
INSERT INTO currency_rates VALUES('94','MGA','4165.265277','','');
INSERT INTO currency_rates VALUES('95','MKD','61.516342','','');
INSERT INTO currency_rates VALUES('96','MMK','1566.586511','','');
INSERT INTO currency_rates VALUES('97','MNT','3088.650418','','');
INSERT INTO currency_rates VALUES('98','MOP','8.975925','','');
INSERT INTO currency_rates VALUES('99','MRO','398.607011','','');
INSERT INTO currency_rates VALUES('100','MUR','43.205754','','');
INSERT INTO currency_rates VALUES('101','MVR','17.250725','','');
INSERT INTO currency_rates VALUES('102','MWK','825.239292','','');
INSERT INTO currency_rates VALUES('103','MXN','24.963329','','');
INSERT INTO currency_rates VALUES('104','MYR','4.810633','','');
INSERT INTO currency_rates VALUES('105','MZN','73.591410','','');
INSERT INTO currency_rates VALUES('106','NAD','18.121621','','');
INSERT INTO currency_rates VALUES('107','NGN','408.099790','','');
INSERT INTO currency_rates VALUES('108','NIO','37.844015','','');
INSERT INTO currency_rates VALUES('109','NOK','11.405599','','');
INSERT INTO currency_rates VALUES('110','NPR','132.508354','','');
INSERT INTO currency_rates VALUES('111','NZD','1.847363','','');
INSERT INTO currency_rates VALUES('112','OMR','0.429801','','');
INSERT INTO currency_rates VALUES('113','PAB','1.121880','','');
INSERT INTO currency_rates VALUES('114','PEN','3.958258','','');
INSERT INTO currency_rates VALUES('115','PGK','3.838505','','');
INSERT INTO currency_rates VALUES('116','PHP','57.698037','','');
INSERT INTO currency_rates VALUES('117','PKR','176.121721','','');
INSERT INTO currency_rates VALUES('118','PLN','4.386058','','');
INSERT INTO currency_rates VALUES('119','PYG','7386.917924','','');
INSERT INTO currency_rates VALUES('120','QAR','4.065302','','');
INSERT INTO currency_rates VALUES('121','RON','4.826717','','');
INSERT INTO currency_rates VALUES('122','RSD','117.627735','','');
INSERT INTO currency_rates VALUES('123','RUB','83.568390','','');
INSERT INTO currency_rates VALUES('124','RWF','1067.822267','','');
INSERT INTO currency_rates VALUES('125','SAR','4.190432','','');
INSERT INTO currency_rates VALUES('126','SBD','9.235251','','');
INSERT INTO currency_rates VALUES('127','SCR','14.529548','','');
INSERT INTO currency_rates VALUES('128','SDG','61.772847','','');
INSERT INTO currency_rates VALUES('129','SEK','10.785247','','');
INSERT INTO currency_rates VALUES('130','SGD','1.587844','','');
INSERT INTO currency_rates VALUES('131','SHP','0.908257','','');
INSERT INTO currency_rates VALUES('132','SLL','9999.999999','','');
INSERT INTO currency_rates VALUES('133','SOS','653.732410','','');
INSERT INTO currency_rates VALUES('134','SRD','8.327212','','');
INSERT INTO currency_rates VALUES('135','STD','9999.999999','','');
INSERT INTO currency_rates VALUES('136','SVC','9.816821','','');
INSERT INTO currency_rates VALUES('137','SYP','575.019506','','');
INSERT INTO currency_rates VALUES('138','SZL','18.038821','','');
INSERT INTO currency_rates VALUES('139','THB','35.884679','','');
INSERT INTO currency_rates VALUES('140','TJS','10.875343','','');
INSERT INTO currency_rates VALUES('141','TMT','3.907909','','');
INSERT INTO currency_rates VALUES('142','TND','3.186636','','');
INSERT INTO currency_rates VALUES('143','TOP','2.635661','','');
INSERT INTO currency_rates VALUES('144','TRY','7.131927','','');
INSERT INTO currency_rates VALUES('145','TTD','7.585158','','');
INSERT INTO currency_rates VALUES('146','TWD','33.739208','','');
INSERT INTO currency_rates VALUES('147','TZS','2582.397529','','');
INSERT INTO currency_rates VALUES('148','UAH','29.335146','','');
INSERT INTO currency_rates VALUES('149','UGX','4169.685347','','');
INSERT INTO currency_rates VALUES('150','USD','1.116545','','');
INSERT INTO currency_rates VALUES('151','UYU','48.718630','','');
INSERT INTO currency_rates VALUES('152','UZS','9999.999999','','');
INSERT INTO currency_rates VALUES('153','VEF','11.151499','','');
INSERT INTO currency_rates VALUES('154','VND','9999.999999','','');
INSERT INTO currency_rates VALUES('155','VUV','133.944917','','');
INSERT INTO currency_rates VALUES('156','WST','3.074259','','');
INSERT INTO currency_rates VALUES('157','XAF','659.652615','','');
INSERT INTO currency_rates VALUES('158','XAG','0.088073','','');
INSERT INTO currency_rates VALUES('159','XAU','0.000756','','');
INSERT INTO currency_rates VALUES('160','XCD','3.017519','','');
INSERT INTO currency_rates VALUES('161','XDR','0.809234','','');
INSERT INTO currency_rates VALUES('162','XOF','659.646672','','');
INSERT INTO currency_rates VALUES('163','XPF','119.931356','','');
INSERT INTO currency_rates VALUES('164','YER','279.475009','','');
INSERT INTO currency_rates VALUES('165','ZAR','18.603040','','');
INSERT INTO currency_rates VALUES('166','ZMK','9999.999999','','');
INSERT INTO currency_rates VALUES('167','ZMW','17.892580','','');
INSERT INTO currency_rates VALUES('168','ZWL','359.527584','','');



DROP TABLE IF EXISTS current_stocks;

CREATE TABLE `current_stocks` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` bigint(20) NOT NULL,
  `quantity` decimal(8,2) NOT NULL,
  `company_id` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS email_templates;

CREATE TABLE `email_templates` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO email_templates VALUES('1','registration','Registrasi Berhasil','<div style=\"padding: 15px 30px;\">
<h2 style=\"color: #555555;\">Pendaftaran Akun Berhasil</h2>
<p style=\"color: #555555;\">Hai, {name}!,<br /><span style=\"color: #555555;\">Selamat! Akun anda berhasil diperbarui. Nikmati kemudahan layanan akses mandiri pembuatan aplikasi (self service access builder apps) di Ngelapak. Mulai sekarang kamu bisa buat toko online impian berbasis website dan aplikasi mobile tanpa ribet coding.<br />Jika ada pertanyaan, hubungi kami di contact@ngelapak.co.id. <br /><br /></span>Salam,</p>
<p style=\"color: #555555;\">Tim Ngelapak</p>
<p>&nbsp;</p>
<p style=\"color: #555555;\"><span style=\"color: #555555;\">&nbsp;</span></p>
</div>','','2021-10-27 05:20:42');
INSERT INTO email_templates VALUES('2','premium_membership','Langganan Premium','<div style=\"padding: 15px 30px;\">
<h2 style=\"color: #555555;\">Langganan Premium Ngelapak</h2>
<p style=\"color: #555555;\">Hai {name},<br /><span style=\"color: #555555;\">Selamat pembayaran Anda berhasil dilakukan. Langganan Anda saat ini berlaku sampai <strong>{valid_to}</strong></span><span style=\"color: #555555;\"><strong>.</strong>&nbsp;</span></p>
<p><br style=\"color: #555555;\" /><span style=\"color: #555555;\">Terima Kasih</span><br style=\"color: #555555;\" /><span style=\"color: #555555;\">Tim Ngelapak</span></p>
</div>','','2021-10-27 05:22:48');
INSERT INTO email_templates VALUES('3','alert_notification','Pembaharuan Ngelapak','<div style=\"padding: 15px 30px;\">
<h2 style=\"color: #555555;\">Pemberitahuan Perpanjangan Akun</h2>
<p style=\"color: #555555;\">Hai {name},<br />Paket Anda akan kedaluwarsa pada {valid_to} sehingga Anda harus memperbaruinya saat itu agar akun Anda tetap aktif.</p>
<p><br style=\"color: #555555;\" /><span style=\"color: #555555;\">Salam,</span></p>
<p><br style=\"color: #555555;\" /><span style=\"color: #555555;\">Tim Ngelapak</span></p>
</div>','','2021-10-27 05:24:44');



DROP TABLE IF EXISTS file_manager;

CREATE TABLE `file_manager` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mime_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_dir` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'no',
  `file` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS files;

CREATE TABLE `files` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `related_to` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `related_id` bigint(20) DEFAULT NULL,
  `file` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `company_id` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO files VALUES('1','projects','1','/home/tesngela/public_html/public/uploads/project_files/1_project.supra','6','5','2021-10-08 22:26:35','2021-10-08 22:26:35');
INSERT INTO files VALUES('2','projects','2','/home/ngelapak/dev2.ngelapak.co.id/public/uploads/project_files/2_project.supra','47','46','2021-10-14 20:42:54','2021-10-14 20:42:54');
INSERT INTO files VALUES('3','projects','3','/home/ngelapak/public_html/public/uploads/project_files/3_project.supra','69','68','2021-10-21 15:32:51','2021-10-21 15:32:51');
INSERT INTO files VALUES('4','projects','4','/home/ngelapak/public_html/public/uploads/project_files/4_project.supra','69','68','2021-10-21 17:56:05','2021-10-21 17:56:05');
INSERT INTO files VALUES('5','projects','5','/home/ngelapak/public_html/public/uploads/project_files/5_project.supra','69','68','2021-10-22 11:24:51','2021-10-22 11:24:51');
INSERT INTO files VALUES('6','projects','6','/home/ngelapak/public_html/public/uploads/project_files/6_project.supra','69','68','2021-10-26 13:29:07','2021-10-26 13:29:07');
INSERT INTO files VALUES('7','projects','7','/home/ngelapak/public_html/public/uploads/project_files/7_project.supra','121','173','2021-10-27 12:52:58','2021-10-27 12:52:58');
INSERT INTO files VALUES('8','projects','8','/home/ngelapak/public_html/public/uploads/project_files/8_project.supra','121','173','2021-10-27 13:12:59','2021-10-27 13:12:59');
INSERT INTO files VALUES('9','projects','9','/home/ngelapak/public_html/public/uploads/project_files/9_project.supra','121','173','2021-10-27 13:18:42','2021-10-27 13:18:42');



DROP TABLE IF EXISTS group_chat_message_status;

CREATE TABLE `group_chat_message_status` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `message_id` bigint(20) NOT NULL,
  `group_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `company_id` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS group_chat_messages;

CREATE TABLE `group_chat_messages` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `group_id` bigint(20) NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `attachment` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sender_id` bigint(20) NOT NULL,
  `company_id` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS invoice_items;

CREATE TABLE `invoice_items` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `invoice_id` bigint(20) NOT NULL,
  `item_id` bigint(20) NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quantity` decimal(10,2) NOT NULL,
  `unit_cost` decimal(10,2) NOT NULL,
  `discount` decimal(10,2) NOT NULL,
  `tax_method` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tax_id` bigint(20) DEFAULT NULL,
  `tax_amount` decimal(10,2) DEFAULT NULL,
  `sub_total` decimal(10,2) NOT NULL,
  `company_id` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS invoice_templates;

CREATE TABLE `invoice_templates` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `editor` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `custom_css` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_id` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS invoices;

CREATE TABLE `invoices` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `invoice_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `invoice_date` date NOT NULL,
  `due_date` date NOT NULL,
  `grand_total` decimal(10,2) NOT NULL,
  `tax_total` decimal(10,2) NOT NULL,
  `paid` decimal(10,2) DEFAULT NULL,
  `converted_total` decimal(10,2) DEFAULT NULL,
  `status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `template` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `note` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `related_to` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `related_id` bigint(20) DEFAULT NULL,
  `client_id` bigint(20) NOT NULL,
  `company_id` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS items;

CREATE TABLE `items` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `item_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `item_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_id` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS lead_sources;

CREATE TABLE `lead_sources` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) DEFAULT NULL,
  `company_id` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS lead_statuses;

CREATE TABLE `lead_statuses` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `color` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) DEFAULT NULL,
  `company_id` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS leads;

CREATE TABLE `leads` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `converted_lead` int(11) DEFAULT NULL,
  `lead_status_id` bigint(20) NOT NULL,
  `lead_source_id` bigint(20) NOT NULL,
  `assigned_user_id` bigint(20) NOT NULL,
  `created_user_id` bigint(20) NOT NULL,
  `contact_date` date NOT NULL,
  `phone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `website` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `currency` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vat_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reg_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `custom_fields` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_id` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS migrations;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO migrations VALUES('1','2014_10_12_000000_create_users_table','1');
INSERT INTO migrations VALUES('2','2014_10_12_100000_create_password_resets_table','1');
INSERT INTO migrations VALUES('3','2018_06_01_080940_create_settings_table','1');
INSERT INTO migrations VALUES('4','2018_08_29_084110_create_permissions_table','1');
INSERT INTO migrations VALUES('5','2018_10_28_101819_create_contact_groups_table','1');
INSERT INTO migrations VALUES('6','2018_10_28_104344_create_contacts_table','1');
INSERT INTO migrations VALUES('7','2018_10_28_151911_create_taxs_table','1');
INSERT INTO migrations VALUES('8','2018_10_29_095644_create_items_table','1');
INSERT INTO migrations VALUES('9','2018_10_29_100449_create_products_table','1');
INSERT INTO migrations VALUES('10','2018_10_29_101301_create_services_table','1');
INSERT INTO migrations VALUES('11','2018_10_29_101756_create_suppliers_table','1');
INSERT INTO migrations VALUES('12','2018_11_12_152015_create_email_templates_table','1');
INSERT INTO migrations VALUES('13','2018_11_13_063551_create_accounts_table','1');
INSERT INTO migrations VALUES('14','2018_11_13_082226_create_chart_of_accounts_table','1');
INSERT INTO migrations VALUES('15','2018_11_13_082512_create_payment_methods_table','1');
INSERT INTO migrations VALUES('16','2018_11_13_141249_create_transactions_table','1');
INSERT INTO migrations VALUES('17','2018_11_14_134254_create_repeating_transactions_table','1');
INSERT INTO migrations VALUES('18','2018_11_17_142037_create_payment_histories_table','1');
INSERT INTO migrations VALUES('19','2019_03_07_084028_create_purchase_orders_table','1');
INSERT INTO migrations VALUES('20','2019_03_07_085537_create_purchase_order_items_table','1');
INSERT INTO migrations VALUES('21','2019_03_19_070903_create_current_stocks_table','1');
INSERT INTO migrations VALUES('22','2019_03_19_123527_create_company_settings_table','1');
INSERT INTO migrations VALUES('23','2019_03_19_133922_create_product_units_table','1');
INSERT INTO migrations VALUES('24','2019_03_20_113605_create_invoices_table','1');
INSERT INTO migrations VALUES('25','2019_03_20_113618_create_invoice_items_table','1');
INSERT INTO migrations VALUES('26','2019_05_11_080519_create_purchase_return_table','1');
INSERT INTO migrations VALUES('27','2019_05_11_080546_create_purchase_return_items_table','1');
INSERT INTO migrations VALUES('28','2019_05_27_153656_create_quotations_table','1');
INSERT INTO migrations VALUES('29','2019_05_27_153712_create_quotation_items_table','1');
INSERT INTO migrations VALUES('30','2019_06_22_062221_create_sales_return_table','1');
INSERT INTO migrations VALUES('31','2019_06_22_062233_create_sales_return_items_table','1');
INSERT INTO migrations VALUES('32','2019_06_23_055645_create_company_email_template_table','1');
INSERT INTO migrations VALUES('33','2019_10_31_172912_create_social_google_accounts_table','1');
INSERT INTO migrations VALUES('34','2019_11_04_133151_create_chat_messages_table','1');
INSERT INTO migrations VALUES('35','2019_11_07_105822_create_chat_groups_table','1');
INSERT INTO migrations VALUES('36','2019_11_08_063856_create_chat_group_users','1');
INSERT INTO migrations VALUES('37','2019_11_08_143329_create_group_chat_messages_table','1');
INSERT INTO migrations VALUES('38','2019_11_08_143607_create_group_chat_message_status_table','1');
INSERT INTO migrations VALUES('39','2019_11_11_170656_create_file_manager_table','1');
INSERT INTO migrations VALUES('40','2020_03_15_154649_create_currency_rates_table','1');
INSERT INTO migrations VALUES('41','2020_03_21_052934_create_companies_table','1');
INSERT INTO migrations VALUES('42','2020_03_21_070022_create_packages_table','1');
INSERT INTO migrations VALUES('43','2020_04_02_155956_create_cm_features_table','1');
INSERT INTO migrations VALUES('44','2020_04_02_160209_create_cm_faqs_table','1');
INSERT INTO migrations VALUES('45','2020_04_02_160249_create_cm_email_subscribers_table','1');
INSERT INTO migrations VALUES('46','2020_05_18_104400_create_invoice_templates_table','1');
INSERT INTO migrations VALUES('47','2020_05_24_152947_create_lead_statuses_table','1');
INSERT INTO migrations VALUES('48','2020_05_24_153000_create_lead_sources_table','1');
INSERT INTO migrations VALUES('49','2020_05_24_153224_create_leads_table','1');
INSERT INTO migrations VALUES('50','2020_06_03_112519_create_files_table','1');
INSERT INTO migrations VALUES('51','2020_06_03_112538_create_notes_table','1');
INSERT INTO migrations VALUES('52','2020_06_03_112553_create_activity_logs_table','1');
INSERT INTO migrations VALUES('53','2020_06_22_083001_create_projects_table','1');
INSERT INTO migrations VALUES('54','2020_06_22_095143_create_project_members_table','1');
INSERT INTO migrations VALUES('55','2020_06_23_083455_create_project_milestones_table','1');
INSERT INTO migrations VALUES('56','2020_06_23_112159_create_task_statuses_table','1');
INSERT INTO migrations VALUES('57','2020_06_23_144512_create_tasks_table','1');
INSERT INTO migrations VALUES('58','2020_06_25_065937_create_timesheets_table','1');
INSERT INTO migrations VALUES('59','2020_06_27_152210_create_notifications_table','1');
INSERT INTO migrations VALUES('60','2020_08_21_063443_add_related_to_company_email_template','1');
INSERT INTO migrations VALUES('61','2020_10_19_082621_create_staff_roles_table','1');
INSERT INTO migrations VALUES('62','2020_10_20_080849_add_description_to_invoice_items','1');



DROP TABLE IF EXISTS notes;

CREATE TABLE `notes` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `related_to` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `related_id` bigint(20) DEFAULT NULL,
  `note` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `company_id` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS notifications;

CREATE TABLE `notifications` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_id` bigint(20) unsigned NOT NULL,
  `data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `read_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `notifications_notifiable_type_notifiable_id_index` (`notifiable_type`,`notifiable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS packages;

CREATE TABLE `packages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `package_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cost_per_month` decimal(10,2) NOT NULL,
  `cost_per_year` decimal(10,2) NOT NULL,
  `websites_limit` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `recurring_transaction` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `online_payment` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_featured` tinyint(4) NOT NULL DEFAULT 0,
  `others` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO packages VALUES('1','Ngelapak Basic','99000.00','1100000.00','a:2:{s:7:\"monthly\";s:1:\"1\";s:6:\"yearly\";s:1:\"1\";}','a:2:{s:7:\"monthly\";s:3:\"Yes\";s:6:\"yearly\";s:3:\"Yes\";}','a:2:{s:7:\"monthly\";s:3:\"Yes\";s:6:\"yearly\";s:3:\"Yes\";}','1','','','2021-10-11 16:34:35');
INSERT INTO packages VALUES('2','Ngelapak Pro','449000.00','5300000.00','a:2:{s:7:\"monthly\";s:1:\"1\";s:6:\"yearly\";s:1:\"1\";}','a:2:{s:7:\"monthly\";s:3:\"Yes\";s:6:\"yearly\";s:3:\"Yes\";}','a:2:{s:7:\"monthly\";s:3:\"Yes\";s:6:\"yearly\";s:3:\"Yes\";}','1','','','2021-10-11 16:33:03');
INSERT INTO packages VALUES('3','Ngelapak Advanced','499000.00','5900000.00','a:2:{s:7:\"monthly\";s:1:\"1\";s:6:\"yearly\";s:1:\"1\";}','a:2:{s:7:\"monthly\";s:3:\"Yes\";s:6:\"yearly\";s:3:\"Yes\";}','a:2:{s:7:\"monthly\";s:3:\"Yes\";s:6:\"yearly\";s:3:\"Yes\";}','1','','','2021-10-11 16:34:19');
INSERT INTO packages VALUES('4','Trial','5000.00','5000.00','a:2:{s:7:\"monthly\";s:1:\"1\";s:6:\"yearly\";s:1:\"1\";}','a:2:{s:7:\"monthly\";s:3:\"Yes\";s:6:\"yearly\";s:3:\"Yes\";}','a:2:{s:7:\"monthly\";s:3:\"Yes\";s:6:\"yearly\";s:3:\"Yes\";}','1','','2021-10-11 16:29:11','2021-10-27 11:14:32');



DROP TABLE IF EXISTS password_resets;

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO password_resets VALUES('testing@gmail.com','$2y$10$ndX1yCdceIMLfFF1JHVYSOmxVaSj/b9rvOvGlw.Ssu.akfecTjPvO','2021-10-26 04:32:55');
INSERT INTO password_resets VALUES('pygmy199@gmail.com','$2y$10$r.GDMoiIrRIyVw6nocYgiuSmoxusTh9foxvafjdjPN9i25Vo0cU2G','2021-10-26 04:33:19');



DROP TABLE IF EXISTS payment_histories;

CREATE TABLE `payment_histories` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `company_id` bigint(20) NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `method` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `currency` varchar(3) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` decimal(10,2) NOT NULL,
  `package_id` int(11) NOT NULL,
  `package_type` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `sessionID` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `trx_id` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=120 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO payment_histories VALUES('77','68','Buy Ngelapak Basic Package','Ipaymu','IDR','99000.00','1','monthly','paid','2021-10-21 11:56:16','2021-10-21 11:59:57','580DD342-C1F4-4EDD-A0BF-02B7B43EA2EB','https://my.ipaymu.com/payment/580DD342-C1F4-4EDD-A0BF-02B7B43EA2EB','5529006');
INSERT INTO payment_histories VALUES('97','164','Buy Ngelapak Basic Package','','IDR','99000.00','1','monthly','pending','2021-10-26 17:28:50','2021-10-26 17:28:50','','','');
INSERT INTO payment_histories VALUES('98','164','Buy Ngelapak Basic Package','','IDR','99000.00','1','monthly','pending','2021-10-26 17:29:15','2021-10-26 17:29:16','ED529A90-F9EB-4952-9578-D9F28E88EC37','https://my.ipaymu.com/payment/ED529A90-F9EB-4952-9578-D9F28E88EC37','');
INSERT INTO payment_histories VALUES('99','159','Buy Trial Package','','IDR','0.00','4','yearly','pending','2021-10-27 11:09:36','2021-10-27 11:09:36','','','');
INSERT INTO payment_histories VALUES('100','159','Buy Ngelapak Advanced Package','','IDR','5900000.00','3','yearly','pending','2021-10-27 11:09:51','2021-10-27 11:09:52','BA272BCD-C7CA-4E75-B0E9-8528096F3BA1','https://my.ipaymu.com/payment/BA272BCD-C7CA-4E75-B0E9-8528096F3BA1','');
INSERT INTO payment_histories VALUES('101','159','Buy Trial Package','','IDR','0.00','4','monthly','pending','2021-10-27 11:10:06','2021-10-27 11:10:06','','','');
INSERT INTO payment_histories VALUES('102','159','Buy Trial Package','','IDR','0.00','4','yearly','pending','2021-10-27 11:10:30','2021-10-27 11:10:30','','','');
INSERT INTO payment_histories VALUES('103','159','Buy Ngelapak Basic Package','','IDR','99000.00','1','monthly','pending','2021-10-27 11:11:12','2021-10-27 11:11:12','8048FB4F-91C2-4DB9-A313-58E95BF25B45','https://my.ipaymu.com/payment/8048FB4F-91C2-4DB9-A313-58E95BF25B45','');
INSERT INTO payment_histories VALUES('104','159','Buy Trial Package','','IDR','0.00','4','yearly','pending','2021-10-27 11:11:43','2021-10-27 11:11:43','','','');
INSERT INTO payment_histories VALUES('105','159','Buy Trial Package','','IDR','0.00','4','yearly','pending','2021-10-27 11:11:56','2021-10-27 11:11:56','','','');
INSERT INTO payment_histories VALUES('106','159','Buy Trial Package','','IDR','0.00','4','yearly','pending','2021-10-27 11:12:08','2021-10-27 11:12:08','','','');
INSERT INTO payment_histories VALUES('107','159','Buy Ngelapak Advanced Package','','IDR','5900000.00','3','yearly','pending','2021-10-27 11:12:18','2021-10-27 11:12:18','937B8E46-F90D-4968-966E-6329CAA11033','https://my.ipaymu.com/payment/937B8E46-F90D-4968-966E-6329CAA11033','');
INSERT INTO payment_histories VALUES('108','159','Buy Trial Package','','IDR','0.00','4','yearly','pending','2021-10-27 11:12:28','2021-10-27 11:12:28','','','');
INSERT INTO payment_histories VALUES('109','159','Buy Trial Package','','IDR','0.00','4','yearly','pending','2021-10-27 11:12:58','2021-10-27 11:12:58','','','');
INSERT INTO payment_histories VALUES('110','159','Buy Trial Package','','IDR','1.00','4','yearly','pending','2021-10-27 11:14:58','2021-10-27 11:14:58','','','');
INSERT INTO payment_histories VALUES('111','159','Buy Ngelapak Basic Package','','IDR','99000.00','1','monthly','pending','2021-10-27 11:16:01','2021-10-27 11:16:02','0341BF86-8D00-498E-B237-82D287518117','https://my.ipaymu.com/payment/0341BF86-8D00-498E-B237-82D287518117','');
INSERT INTO payment_histories VALUES('112','159','Buy Trial Package','','IDR','99000.00','4','monthly','pending','2021-10-27 11:18:06','2021-10-27 11:18:07','7232E96A-EEAF-4E31-9764-DD8691DD623E','https://my.ipaymu.com/payment/7232E96A-EEAF-4E31-9764-DD8691DD623E','');
INSERT INTO payment_histories VALUES('113','159','Buy Trial Package','','IDR','5000.00','4','monthly','pending','2021-10-27 11:19:07','2021-10-27 11:19:07','2FAAFA1D-8FAC-4F3F-B983-2B9975D19FFD','https://my.ipaymu.com/payment/2FAAFA1D-8FAC-4F3F-B983-2B9975D19FFD','');
INSERT INTO payment_histories VALUES('114','174','Buy Ngelapak Advanced Package','','IDR','5900000.00','3','yearly','pending','2021-10-27 12:49:58','2021-10-27 12:50:00','0A6EA79E-BF73-4649-B2B7-43A83569EA22','https://my.ipaymu.com/payment/0A6EA79E-BF73-4649-B2B7-43A83569EA22','');
INSERT INTO payment_histories VALUES('115','174','Buy Trial Package','','IDR','5000.00','4','monthly','pending','2021-10-27 12:51:37','2021-10-27 12:51:38','85B075CF-B917-4AD9-8F1C-753DDA3BF3E8','https://my.ipaymu.com/payment/85B075CF-B917-4AD9-8F1C-753DDA3BF3E8','');
INSERT INTO payment_histories VALUES('116','174','Buy Trial Package','','IDR','5000.00','4','monthly','pending','2021-10-27 12:53:54','2021-10-27 12:53:55','6C34E928-E6D8-4508-B0A3-6B5E543A6D96','https://my.ipaymu.com/payment/6C34E928-E6D8-4508-B0A3-6B5E543A6D96','');
INSERT INTO payment_histories VALUES('117','174','Buy Trial Package','','IDR','5000.00','4','monthly','pending','2021-10-27 13:13:46','2021-10-27 13:13:46','','','');
INSERT INTO payment_histories VALUES('118','174','Buy Ngelapak Advanced Package','','IDR','5900000.00','3','yearly','pending','2021-10-27 13:14:08','2021-10-27 13:14:08','','','');
INSERT INTO payment_histories VALUES('119','173','Buy Ngelapak Advanced Package','','IDR','5900000.00','3','yearly','pending','2021-10-27 13:36:10','2021-10-27 13:36:11','166AB110-A8DE-49AF-8873-F63CF74C160C','https://my.ipaymu.com/payment/166AB110-A8DE-49AF-8873-F63CF74C160C','');



DROP TABLE IF EXISTS payment_methods;

CREATE TABLE `payment_methods` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_id` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS permissions;

CREATE TABLE `permissions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) NOT NULL,
  `permission` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS product_units;

CREATE TABLE `product_units` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `unit_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_id` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS products;

CREATE TABLE `products` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `item_id` bigint(20) NOT NULL,
  `supplier_id` bigint(20) DEFAULT NULL,
  `product_cost` decimal(10,2) NOT NULL,
  `product_price` decimal(10,2) NOT NULL,
  `product_unit` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tax_method` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tax_id` bigint(20) DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS project_members;

CREATE TABLE `project_members` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS project_milestones;

CREATE TABLE `project_milestones` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `due_date` date NOT NULL,
  `status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cost` decimal(10,2) DEFAULT NULL,
  `project_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `company_id` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS projects;

CREATE TABLE `projects` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `client_id` bigint(20) NOT NULL,
  `progress` int(11) DEFAULT NULL,
  `billing_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fixed_rate` decimal(10,2) DEFAULT NULL,
  `hourly_rate` decimal(10,2) DEFAULT NULL,
  `start_date` date NOT NULL,
  `end_date` date DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `custom_fields` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` bigint(20) NOT NULL,
  `company_id` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO projects VALUES('1','Project_2021-10-08_22:26:35','0','','','lara','','','0000-00-00','','','','6','5','2021-10-08 22:26:35','2021-10-08 22:26:35');
INSERT INTO projects VALUES('6','Project_2021-10-26_13:29:07','0','','','lara','','','0000-00-00','','<p>sdf</p>','testing','69','68','2021-10-26 13:29:07','2021-10-27 14:05:18');
INSERT INTO projects VALUES('9','Lautan.io Landing Page','0','','','lara','','','0000-00-00','','<p>Lautan.io Landing Page</p>','lautan','121','173','2021-10-27 13:18:42','2021-10-27 14:10:11');



DROP TABLE IF EXISTS purchase_order_items;

CREATE TABLE `purchase_order_items` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `purchase_order_id` bigint(20) NOT NULL,
  `product_id` bigint(20) NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quantity` decimal(8,2) NOT NULL,
  `unit_cost` decimal(10,2) NOT NULL,
  `discount` decimal(10,2) NOT NULL,
  `tax_method` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tax_id` bigint(20) DEFAULT NULL,
  `tax_amount` decimal(10,2) DEFAULT NULL,
  `sub_total` decimal(10,2) NOT NULL,
  `company_id` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS purchase_orders;

CREATE TABLE `purchase_orders` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `order_date` date NOT NULL,
  `supplier_id` bigint(20) NOT NULL,
  `order_status` tinyint(4) NOT NULL,
  `order_tax_id` bigint(20) DEFAULT NULL,
  `order_tax` decimal(10,2) DEFAULT NULL,
  `order_discount` decimal(10,2) NOT NULL,
  `shipping_cost` decimal(10,2) NOT NULL,
  `product_total` decimal(10,2) NOT NULL,
  `grand_total` decimal(10,2) NOT NULL,
  `paid` decimal(10,2) NOT NULL,
  `payment_status` tinyint(4) NOT NULL,
  `attachemnt` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `note` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_id` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS purchase_return;

CREATE TABLE `purchase_return` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `return_date` date NOT NULL,
  `supplier_id` bigint(20) DEFAULT NULL,
  `account_id` bigint(20) NOT NULL,
  `chart_id` bigint(20) NOT NULL,
  `payment_method_id` bigint(20) NOT NULL,
  `tax_id` bigint(20) DEFAULT NULL,
  `tax_amount` decimal(10,2) DEFAULT NULL,
  `product_total` decimal(10,2) NOT NULL,
  `grand_total` decimal(10,2) NOT NULL,
  `attachemnt` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `note` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_id` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS purchase_return_items;

CREATE TABLE `purchase_return_items` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `purchase_return_id` int(11) NOT NULL,
  `product_id` bigint(20) NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quantity` decimal(10,2) NOT NULL,
  `unit_cost` decimal(10,2) NOT NULL,
  `discount` decimal(10,2) NOT NULL,
  `tax_method` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tax_id` bigint(20) DEFAULT NULL,
  `tax_amount` decimal(10,2) DEFAULT NULL,
  `sub_total` decimal(10,2) NOT NULL,
  `company_id` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS quotation_items;

CREATE TABLE `quotation_items` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `quotation_id` bigint(20) NOT NULL,
  `item_id` bigint(20) NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quantity` decimal(10,2) NOT NULL,
  `unit_cost` decimal(10,2) NOT NULL,
  `discount` decimal(10,2) NOT NULL,
  `tax_method` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tax_id` bigint(20) DEFAULT NULL,
  `tax_amount` decimal(10,2) DEFAULT NULL,
  `sub_total` decimal(10,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS quotations;

CREATE TABLE `quotations` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `quotation_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quotation_date` date NOT NULL,
  `template` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `grand_total` decimal(10,2) NOT NULL,
  `converted_total` decimal(10,2) DEFAULT NULL,
  `tax_total` decimal(10,2) NOT NULL,
  `note` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `related_to` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `related_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS repeating_transactions;

CREATE TABLE `repeating_transactions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `trans_date` date NOT NULL,
  `account_id` bigint(20) NOT NULL,
  `chart_id` bigint(20) NOT NULL,
  `type` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dr_cr` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `base_amount` decimal(10,2) DEFAULT NULL,
  `payer_payee_id` bigint(20) DEFAULT NULL,
  `payment_method_id` bigint(20) NOT NULL,
  `reference` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `note` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_id` bigint(20) NOT NULL,
  `status` tinyint(4) DEFAULT 0,
  `trans_id` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS sales_return;

CREATE TABLE `sales_return` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `return_date` date NOT NULL,
  `customer_id` bigint(20) NOT NULL,
  `tax_id` bigint(20) DEFAULT NULL,
  `tax_amount` decimal(10,2) DEFAULT NULL,
  `product_total` decimal(10,2) NOT NULL,
  `grand_total` decimal(10,2) NOT NULL,
  `converted_total` decimal(10,2) DEFAULT NULL,
  `attachemnt` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `note` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_id` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS sales_return_items;

CREATE TABLE `sales_return_items` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `sales_return_id` bigint(20) NOT NULL,
  `product_id` bigint(20) NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quantity` decimal(8,2) NOT NULL,
  `unit_cost` decimal(10,2) NOT NULL,
  `discount` decimal(10,2) NOT NULL,
  `tax_method` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tax_id` bigint(20) DEFAULT NULL,
  `tax_amount` decimal(10,2) DEFAULT NULL,
  `sub_total` decimal(10,2) NOT NULL,
  `company_id` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS services;

CREATE TABLE `services` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `item_id` bigint(20) NOT NULL,
  `cost` decimal(10,2) NOT NULL,
  `tax_method` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tax_id` bigint(20) DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS settings;

CREATE TABLE `settings` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=79 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO settings VALUES('1','mail_type','smtp','','2021-10-26 17:21:05');
INSERT INTO settings VALUES('2','backend_direction','ltr','','2021-10-27 12:59:31');
INSERT INTO settings VALUES('3','membership_system','enabled','','2021-10-27 10:22:12');
INSERT INTO settings VALUES('4','trial_period','14','','2021-10-27 10:22:12');
INSERT INTO settings VALUES('5','allow_singup','yes','','2021-10-27 10:22:12');
INSERT INTO settings VALUES('6','email_verification','enabled','','2021-10-27 10:22:12');
INSERT INTO settings VALUES('7','hero_title','Start Your Business With LaraBuilder','','');
INSERT INTO settings VALUES('8','hero_sub_title','A +400 professionally designed blocks allowing you to start building sites and pages instantly with Easy to use drag & drop builder!','','');
INSERT INTO settings VALUES('9','meta_keywords','drag and drop, laravel drag and drop, laravel sitebuilder, site builder, sitebuilder, website builder','','');
INSERT INTO settings VALUES('10','meta_description','A +400 professionally designed blocks allowing you to start building sites and pages instantly with Easy to use drag & drop builder!','','');
INSERT INTO settings VALUES('11','language','English','','2021-10-26 18:55:41');
INSERT INTO settings VALUES('12','company_name','Ngelapak Bersama Kita','2021-09-27 22:49:52','2021-10-26 18:55:41');
INSERT INTO settings VALUES('13','site_title','Ngelapak - Milyaran Aplikasi Untuk UMKM Indonesia','2021-09-27 22:49:52','2021-10-26 18:55:41');
INSERT INTO settings VALUES('14','phone','081333222111','2021-09-27 22:49:52','2021-10-26 18:55:41');
INSERT INTO settings VALUES('15','email','admin@ngelapak.co.id','2021-09-27 22:49:52','2021-10-26 18:55:41');
INSERT INTO settings VALUES('16','timezone','Asia/Jakarta','2021-09-27 22:49:52','2021-10-26 18:55:41');
INSERT INTO settings VALUES('17','mail_type','smtp','','2021-10-26 17:21:05');
INSERT INTO settings VALUES('18','backend_direction','ltr','','2021-10-27 12:59:31');
INSERT INTO settings VALUES('19','membership_system','enabled','','2021-10-27 10:22:12');
INSERT INTO settings VALUES('20','trial_period','14','','2021-10-27 10:22:12');
INSERT INTO settings VALUES('21','allow_singup','yes','','2021-10-27 10:22:12');
INSERT INTO settings VALUES('22','email_verification','enabled','','2021-10-27 10:22:12');
INSERT INTO settings VALUES('23','hero_title','Start Your Business With LaraBuilder','','');
INSERT INTO settings VALUES('24','hero_sub_title','A +400 professionally designed blocks allowing you to start building sites and pages instantly with Easy to use drag & drop builder!','','');
INSERT INTO settings VALUES('25','meta_keywords','drag and drop, laravel drag and drop, laravel sitebuilder, site builder, sitebuilder, website builder','','');
INSERT INTO settings VALUES('26','meta_description','A +400 professionally designed blocks allowing you to start building sites and pages instantly with Easy to use drag & drop builder!','','');
INSERT INTO settings VALUES('27','language','English','','2021-10-26 18:55:41');
INSERT INTO settings VALUES('28','currency','IDR','2021-10-05 11:37:42','2021-10-27 10:22:12');
INSERT INTO settings VALUES('29','currency_position','left','2021-10-05 11:37:42','2021-10-27 10:22:12');
INSERT INTO settings VALUES('30','from_email','customer@ngelapak.co.id','2021-10-05 11:41:25','2021-10-26 17:21:05');
INSERT INTO settings VALUES('31','from_name','Ngelapak','2021-10-05 11:41:25','2021-10-26 17:21:05');
INSERT INTO settings VALUES('32','smtp_host','ngelapak.co.id','2021-10-05 11:41:25','2021-10-26 17:21:05');
INSERT INTO settings VALUES('33','smtp_port','587','2021-10-05 11:41:25','2021-10-26 17:21:05');
INSERT INTO settings VALUES('34','smtp_username','customer@ngelapak.co.id','2021-10-05 11:41:25','2021-10-26 17:21:05');
INSERT INTO settings VALUES('35','smtp_password','Ngelapak2021!','2021-10-05 11:41:25','2021-10-26 17:21:05');
INSERT INTO settings VALUES('36','smtp_encryption','tls','2021-10-05 11:41:25','2021-10-26 17:21:05');
INSERT INTO settings VALUES('37','address','','2021-10-05 12:43:29','2021-10-26 18:55:41');
INSERT INTO settings VALUES('38','paypal_active','No','2021-10-05 13:01:40','2021-10-27 13:38:30');
INSERT INTO settings VALUES('39','paypal_email','ngelapak2021@gmail.com','2021-10-05 13:01:40','2021-10-27 13:38:30');
INSERT INTO settings VALUES('40','paypal_currency','USD','2021-10-05 13:01:40','2021-10-27 13:38:30');
INSERT INTO settings VALUES('41','stripe_active','No','2021-10-05 13:01:40','2021-10-27 13:38:30');
INSERT INTO settings VALUES('42','stripe_secret_key','23424','2021-10-05 13:01:40','2021-10-27 13:38:30');
INSERT INTO settings VALUES('43','stripe_publishable_key','234','2021-10-05 13:01:40','2021-10-27 13:38:30');
INSERT INTO settings VALUES('44','stripe_currency','USD','2021-10-05 13:01:40','2021-10-27 13:38:30');
INSERT INTO settings VALUES('45','razorpay_active','No','2021-10-05 13:01:40','2021-10-27 13:38:30');
INSERT INTO settings VALUES('46','razorpay_key_id','2324','2021-10-05 13:01:40','2021-10-27 13:38:30');
INSERT INTO settings VALUES('47','razorpay_secret_key','23424','2021-10-05 13:01:40','2021-10-27 13:38:30');
INSERT INTO settings VALUES('48','razorpay_currency','INR','2021-10-05 13:01:40','2021-10-27 13:38:30');
INSERT INTO settings VALUES('49','paystack_active','No','2021-10-05 13:01:40','2021-10-27 13:38:30');
INSERT INTO settings VALUES('50','paystack_public_key','123','2021-10-05 13:01:40','2021-10-27 13:38:30');
INSERT INTO settings VALUES('51','paystack_secret_key','123','2021-10-05 13:01:40','2021-10-27 13:38:30');
INSERT INTO settings VALUES('52','paystack_currency','NGN','2021-10-05 13:01:40','2021-10-27 13:38:30');
INSERT INTO settings VALUES('53','mail_type','smtp','','2021-10-26 17:21:05');
INSERT INTO settings VALUES('54','backend_direction','ltr','','2021-10-27 12:59:31');
INSERT INTO settings VALUES('55','membership_system','enabled','','2021-10-27 10:22:12');
INSERT INTO settings VALUES('56','trial_period','14','','2021-10-27 10:22:12');
INSERT INTO settings VALUES('57','allow_singup','yes','','2021-10-27 10:22:12');
INSERT INTO settings VALUES('58','email_verification','enabled','','2021-10-27 10:22:12');
INSERT INTO settings VALUES('59','hero_title','Start Your Business With LaraBuilder','','');
INSERT INTO settings VALUES('60','hero_sub_title','A +400 professionally designed blocks allowing you to start building sites and pages instantly with Easy to use drag & drop builder!','','');
INSERT INTO settings VALUES('61','meta_keywords','drag and drop, laravel drag and drop, laravel sitebuilder, site builder, sitebuilder, website builder','','');
INSERT INTO settings VALUES('62','meta_description','A +400 professionally designed blocks allowing you to start building sites and pages instantly with Easy to use drag & drop builder!','','');
INSERT INTO settings VALUES('63','language','English','','2021-10-26 18:55:41');
INSERT INTO settings VALUES('64','website_enable','yes','2021-10-26 13:21:05','2021-10-27 12:59:31');
INSERT INTO settings VALUES('65','default_builder','both','2021-10-26 13:21:05','2021-10-27 12:59:31');
INSERT INTO settings VALUES('66','website_language_dropdown','yes','2021-10-26 13:21:05','2021-10-27 12:59:31');
INSERT INTO settings VALUES('67','currency_converter','manual','2021-10-26 13:21:05','2021-10-27 12:59:31');
INSERT INTO settings VALUES('68','fixer_api_key','','2021-10-26 13:21:05','2021-10-27 12:59:31');
INSERT INTO settings VALUES('69','google_map_key','AIzaSyAcp1Ne9Mh0PEReSU155zIqxke7R-nOgSg','2021-10-26 13:21:05','2021-10-27 12:59:31');
INSERT INTO settings VALUES('70','date_format','Y-m-d','2021-10-26 13:21:05','2021-10-27 12:59:31');
INSERT INTO settings VALUES('71','time_format','24','2021-10-26 13:21:05','2021-10-27 12:59:31');
INSERT INTO settings VALUES('72','file_manager_file_type_supported','png,jpg,jpeg','2021-10-26 13:21:05','2021-10-27 12:59:31');
INSERT INTO settings VALUES('73','file_manager_max_upload_size','2','2021-10-26 13:21:05','2021-10-27 12:59:31');
INSERT INTO settings VALUES('74','favicon','file_1635229632.png','2021-10-26 13:27:12','2021-10-26 13:27:12');
INSERT INTO settings VALUES('75','logo','logo.png','2021-10-26 13:27:42','2021-10-26 13:27:42');
INSERT INTO settings VALUES('76','google_login','disabled','2021-10-26 16:16:02','2021-10-26 16:16:43');
INSERT INTO settings VALUES('77','GOOGLE_CLIENT_ID','','2021-10-26 16:16:02','2021-10-26 16:16:43');
INSERT INTO settings VALUES('78','GOOGLE_CLIENT_SECRET','','2021-10-26 16:16:02','2021-10-26 16:16:43');



DROP TABLE IF EXISTS social_google_accounts;

CREATE TABLE `social_google_accounts` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `provider_user_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `provider` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS staff_roles;

CREATE TABLE `staff_roles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_id` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS suppliers;

CREATE TABLE `suppliers` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `supplier_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vat_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `postal_code` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_id` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS task_statuses;

CREATE TABLE `task_statuses` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `color` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) DEFAULT NULL,
  `company_id` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS tasks;

CREATE TABLE `tasks` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `project_id` bigint(20) NOT NULL,
  `milestone_id` bigint(20) DEFAULT NULL,
  `priority` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `task_status_id` bigint(20) NOT NULL,
  `assigned_user_id` bigint(20) DEFAULT NULL,
  `start_date` date NOT NULL,
  `end_date` date DEFAULT NULL,
  `custom_fields` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` bigint(20) NOT NULL,
  `company_id` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS taxs;

CREATE TABLE `taxs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tax_name` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rate` decimal(10,2) NOT NULL,
  `type` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_id` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS timesheets;

CREATE TABLE `timesheets` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `total_hour` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `project_id` bigint(20) NOT NULL,
  `task_id` bigint(20) NOT NULL,
  `note` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_id` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS transactions;

CREATE TABLE `transactions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `trans_date` date NOT NULL,
  `account_id` bigint(20) NOT NULL,
  `chart_id` bigint(20) NOT NULL,
  `type` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dr_cr` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `base_amount` decimal(10,2) DEFAULT NULL,
  `payer_payee_id` bigint(20) DEFAULT NULL,
  `invoice_id` bigint(20) DEFAULT NULL,
  `purchase_id` bigint(20) DEFAULT NULL,
  `purchase_return_id` bigint(20) DEFAULT NULL,
  `project_id` bigint(20) DEFAULT NULL,
  `payment_method_id` bigint(20) NOT NULL,
  `reference` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attachment` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `note` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_id` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS users;

CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_id` bigint(20) DEFAULT NULL,
  `profile_picture` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL,
  `language` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `nama_lengkap` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jk` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `no_hp` varbinary(255) DEFAULT NULL,
  `nik` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alamat` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kota` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `prov` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kode_pos` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `merchant_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=124 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO users VALUES('1','Admin','admin@ngelapak.co.id','2021-09-27 22:48:38','$2y$10$j.e1t388401LJBrRpeJEpe8fg3HJRykUyWjtdum8a3q95ZAD0CUNe','admin','','default.png','1','','','VckRi6YM1wWsy7de9kabGdsJVCBveDOEtUuGk5XLidyeZN9U41x3zWvQrrAL','2021-09-27 22:48:38','2021-10-18 10:14:44','','','','','','','','','','');
INSERT INTO users VALUES('50','LPDPUI_Siti Jumhati','jumhati1981@gmail.com','2021-10-15 19:34:20','$2y$10$YuIiLGam7A3AjjlcxaWHRu0sKqs6kabElJtM2FEx6kMz4DdYp/5Mm','user','','profile_1634302970.jpg','1','English','49','g8uuPpRA0HieoAnapXBrMs4lxRvFZJ2MekQ8uSmkSSpD6nXnWPKlIBazqtwt','2021-10-15 19:29:29','2021-10-15 20:02:50','Siti Jumhati','Perempuan','1981-05-10','081311456035','3276025005810015','Jalan Kelengkeng 4 Blok E0 RT002 RW018 Kelurahan Sukatanai Kecamatan Tapos','Depok','Jawa Barat','16454','');
INSERT INTO users VALUES('51','Susilawati','sw.susilwati12@gmail.com','','$2y$10$.NTf8xhJo135zWoasFjMouhteaU9BPqKfrRKbOuQnRz3vxpWYu/oq','user','','default.png','1','English','50','','2021-10-15 19:29:30','2021-10-15 19:29:30','','','','','','','','','','');
INSERT INTO users VALUES('52','LPDPUI_Dian Annisa','dianannisa1031@gmail.com','2021-10-15 19:32:52','$2y$10$CJWJ9ZQW2/G85nKIOEM7F.SIF.4PbJj7BVHzNYKTNISekfuVfZulC','user','','profile_1634302394.png','1','English','51','DF7oD5a0fqXtght4ugQzFdd9vdsbplbPPdkxSmzK5lInrGIZy4LjhIlGmRxX','2021-10-15 19:29:32','2021-10-15 20:02:55','Dian Annisa','Perempuan','1995-02-10','083811539765','3201246104950002','Kp  tipar rt 04 rw 04 no 73 ciawi bogor 16720','Kabupaten Bogor','Jawa Barat','16720','');
INSERT INTO users VALUES('53','Budi','budisulis804@gmail.com','2021-10-15 19:43:19','$2y$10$v/WA8wJ0U6lpt8nDA/OqQOrmTFkvj9IlboqcsVAhcxDl3kyabF6MG','user','','profile_1634302408.jpg','1','English','52','JEsA4achlkfr1FEmH0FvlRRwzyFSeBr3yAIOdWzwGzI5QbxS1ORA11jI1t1o','2021-10-15 19:30:13','2021-10-15 19:53:28','sulistiyanto','Laki-Laki','1972-06-19','081386108624','3216221906720001','Perum Mutiara Bekasi Jaya Blok F16/11 Sindangmulya.Cibarusah','Bekasi','Jawa Barat','17341','');
INSERT INTO users VALUES('54','LPDPUI_lusiana','lusiistanti66@gmail.com','2021-10-15 19:37:42','$2y$10$XyIE./G9F0W0ozZ8c78AeOcaXj2qBDELRuMRfm7yYVcn/SgzNBTDe','user','','default.png','1','English','53','7KJiEwImH9635VnrjANGq45U9uaPMx8vPYGUDzVJ9IYcPYUjd4TPtiEbRrjm','2021-10-15 19:30:16','2021-10-15 19:39:25','lusi','Perempuan','1990-01-01','08123456789','35780000000002','Ruko Soho Royal Residence BS 10-07','Surabaya','Jawa Timur','60227','');
INSERT INTO users VALUES('55','LPDPUI_Yessie Natasia Mareti','yorrieatery@gmail.com','2021-10-15 19:32:34','$2y$10$hct3SiIXxU2rnecVEtXhVu0hGRHZd5uPBxgKlVQ7JTxTjjOcuALyu','user','','default.png','1','English','54','VW3khieKtj4EeEOurKvrvs8g4mn7AtxO0aGbRWzGQHntgacLrl3SJEwTMKgC','2021-10-15 19:30:21','2021-10-15 19:37:24','Yessie Natasia Mareti','Perempuan','1986-03-18','0817304988','3578244306860001','Pandugo Baru XIII P1A','Surabaya','Jawa Timur','60297','');
INSERT INTO users VALUES('56','LPDPUI_ratnapuspita','ratnapuspitarp071@gmail.com','2021-10-15 19:31:59','$2y$10$BJ53A5QyxA7FaiKS1YIB8O5N3cFPwxefb82XagMJRYGD1xVD/CBKy','user','','default.png','1','English','55','VF0o8iVhvpZCi5KO7n285xr8fYvHE0ujmCX9L3lHuAqGZZNtgvQY0rSFwWx9','2021-10-15 19:30:22','2021-10-15 19:33:29','Ratna Puspita','Perempuan','1995-07-26','0895422882092','3319016607950001','Blimbing Kidul Rt 04/03','KAB. KUDUS','Jawa Tengah','59332','');
INSERT INTO users VALUES('57','LPDPUI_Miftahudin Nur Ihsan','smartbatikid@gmail.com','2021-10-15 19:32:24','$2y$10$TJOdxFK55S/JahyICKRMTuE4byKEYN.yDw9Is6c5.dh0AuAoo9mei','user','','default.png','1','English','56','','2021-10-15 19:30:31','2021-10-15 19:39:34','Miftahudin Nur Ihsan','Laki-Laki','1993-08-11','085747070386','3471071108930001','Ketanggungan, WB 2/429, RT 057, RW 012','Yogyakarta','Daerah Istimewa Yogyakarta','55252','');
INSERT INTO users VALUES('58','LPDPUI_Eman Sulaeman','madu.ti.kulon@gmail.com','2021-10-15 19:45:14','$2y$10$NJdU8TZxkNK0hCC5EqQXyuCFipHhMUWQeZS8SEphZgTj1PtM2wEIm','user','','default.png','1','English','57','5gvaFCiQeu2hkGdLj3RIIV1jBII193GYtvUYrAUHDCxvtfcG7Souts8asoKx','2021-10-15 19:32:02','2021-10-15 19:47:31','eman sulaeman','Laki-Laki','1983-03-30','081289359092','3601343003830003','Komplek saruni permai RT 03/09 Kel. Saruni Kec. Majasari','pandeglang','Banten','42216','');
INSERT INTO users VALUES('59','lpdpui_sanga','sca_sanga@yahoo.com','2021-10-15 19:33:44','$2y$10$c4nYuQEsx3TnffqkipqZneC0T.FtTSoEsYhAj2apBKRqWdMTllV6i','user','','default.png','1','English','58','','2021-10-15 19:32:03','2021-10-15 19:35:15','Endah WN','Perempuan','1980-07-28','085214143426','3276026807800013','Jl. Tole Iskandar No. 17','Depok','Jawa Barat','16451','');
INSERT INTO users VALUES('60','LPDPUI_WARUNGSASTI','yudhi280883@gmail.com','','$2y$10$MAfrcHG3JCffXO9KxiLjA.lPgU6qbicmgyclC9In4Xe27K9OiwdWS','user','','default.png','1','English','59','ja04fu9rZkKMQejOYW17wO6Ntt0M08pkmrv58lYHBP2Ls69xBZ5g5dRLe08s','2021-10-15 19:33:15','2021-10-15 19:33:15','','','','','','','','','','');
INSERT INTO users VALUES('61','Siti Nur Seha','sitinursehasuman@gmail.com','2021-10-15 20:03:09','$2y$10$9YhBxmQSCiUZU.Ss8LAZgODlIkYnoclC4Xr4EK45AVhYyPvh4nzTK','user','','profile_1634304580.jpg','1','English','60','LtPYGgwf5ZzPA239uARM4EKWYVwqwtNzIDzGxhaNo93rtSjh4SkjR95d1594','2021-10-15 19:35:12','2021-10-15 20:29:40','SITI NUR SEHA','Perempuan','1996-07-10','085771881496','3513105007960003','Dusun Pasar II RT 006/ RW 003','Kabupaten Probolinggo','JAWA TIMUR','67292','');
INSERT INTO users VALUES('62','Yunita Indrawati','yunita260390@gmail.com','2021-10-15 19:42:03','$2y$10$GchTmXUR2zOorr6SgnvTqeeNPlbOfgewKexp7aisJLQqywOi7QTu6','user','','default.png','1','English','61','XNqHCNJywhh41i6J4kcUTOINg6YsVDlD50mNKfepY6RMJb0JBavLp6B8Dafk','2021-10-15 19:38:47','2021-10-15 19:42:03','','','','','','','','','','');
INSERT INTO users VALUES('63','Samsidar','ssemsmart@gmail.com','2021-10-15 19:43:04','$2y$10$BROkRr0Ll6rISwBTi3Fa8Olg5Jczbb57wE1UUI3fKG6JJSPFtT0fy','user','','default.png','1','English','62','E1wQvWhUQoZUNlw59WZCqKM1by3ZmibMVizR9uM8FoMnj8S14kJIqQKiDEnY','2021-10-15 19:39:52','2021-10-15 19:43:04','','','','','','','','','','');
INSERT INTO users VALUES('65','Bosteak','bosteakcafe@gmail.com','','$2y$10$DNq6GO.tHKA1DqgyyxSWqelYtz/05D6GC66Pl/a0PnOBupmEBtqsq','user','','default.png','1','English','64','aTVtHU9W6N6ilEuCkBbWD9h3qzqc9qkhWXVBcBWp8bI7f636NCQnhDmbrnXL','2021-10-15 19:54:29','2021-10-15 19:54:29','','','','','','','','','','');
INSERT INTO users VALUES('66','LPDPUI_Yuria Ekalitani','ayuriaekalitani@gmail.com','','$2y$10$89T4pKN9rLiyHPBv4SQwM.hyyUmnGpKFDlUn8oL9UZJwJ9Hv5mh4a','user','','default.png','1','English','65','','2021-10-15 20:05:12','2021-10-15 20:05:12','','','','','','','','','','');
INSERT INTO users VALUES('68','Learninqur\'an International','rickakrisnawati42@gmail.com','2021-10-19 22:28:45','$2y$10$Ckzj63woEsQD2tjDC8fSOOxC9q4aOn9iDGTAd3SY7sc8lmQvnW8bG','user','','default.png','1','English','67','2L4DynsUXH9sAOHirllcVxclSl3wC27BH3VSK07ehhAbajB6Y7av3quo95RP','2021-10-19 22:22:13','2021-10-19 22:28:45','','','','','','','','','','');
INSERT INTO users VALUES('69','tes','testing@gmail.com','2021-10-21 11:41:52','$2y$10$y1KLJuW0O.VJE6GGZcScB.6WvNhV5cXXqsyb9AWus.8549GBIjZTG','user','','default.png','1','English','68','IfPlDnY1K64S7e6njhySabLvx8OuBcuLGVtln1mKAaUUr3cJig7SPKjmwNWS','2021-10-21 11:40:44','2021-10-26 05:32:28','testing','Laki-Laki','2021-10-30','081234567890','1234567890','Surabaya','Surabaya','Jawa Timur','61234','7b825583-1ec1-4955-b45f-6c0f58958629');
INSERT INTO users VALUES('117','Irma','irmadamasurya@yahoo.com','2021-10-27 00:25:40','$2y$10$i6kbbhsDB8IgXWOD4PIPuOq2bdf3CNrb6uluOhwCvemR7qx20ltYa','user','','default.png','1','English','169','WagzSJeG3NCUMYjYFxtIJVxrX8mwV6LWbjplMi8Z9quE30TBbHaXFq0hmI0N','2021-10-27 00:23:32','2021-10-27 00:25:40','','','','082221722238','','','','','','96f417a9-d9cc-461f-a1d1-1bc265d9454a');
INSERT INTO users VALUES('121','PT Lautan Harum Mewangi','ptlautanmewangi@gmail.com','2021-10-27 05:31:33','$2y$10$M11b6btaLLcjRqZGLcUwluwROZtgvV4xgS7bi/xXZ4lGUmylwwLm6','user','','default.png','1','English','173','','2021-10-27 05:28:49','2021-10-27 05:41:38','SATRYA WIRA WICAKSANA','Laki-Laki','1993-06-22','081220205242','3217062206930017','Permata Cimahi M2 No 18 Rt 9 Rw14','Cimahi','Jawa Barat','40552','f69fee91-f511-41ac-ac8a-70e5eec591e3');



