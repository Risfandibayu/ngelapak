@extends('layouts.app')
@section('content')
<div class="row">
	<div class="col-12">
		<div class="card">
		    <h5 class="card-header bg-primary text-white mt-0 panel-title">{{ _lang('Pembayaran Langganan') }}</h5>
			<div class="card-body">
				<div class="row">
					<div class="col-lg-12">
						@if (\Session::has('message'))
							<div class="alert alert-danger text-center">
								<b>{{ \Session::get('message') }}</b>
							</div>
							@endif		

							<!--begin::Form-->
							<form method="POST" class="kt-form" action="{{ url('membership/pay') }}">
								@csrf

								<div class="form-group">
									<label class="control-label">{{ _lang('Paket') }}</label>	
									<select id="package" class="form-control" name="package" required>
										<option value="" hidden>{{ _lang('Pilih Paket') }}</option>
										<!--{{ create_option('packages', 'id', 'package_name', $user->company->package_id) }}-->
										<option value="1" {{ $user->company->package_id == 1 ? 'selected' : '' }}>Ngelapak Basic</option>
										<option value="2" {{ $user->company->package_id == 2 ? 'selected' : '' }}>Ngelapak Pro</option>
										<option value="3" {{ $user->company->package_id == 3 ? 'selected' : '' }}>Ngelapak Advanced</option>
										<!--<option value="4" {{ $user->company->package_id == 4 ? 'selected' : '' }}>Trial member</option>-->
									</select>  
								</div>		
								<br>		
							
								<div class="form-group">
									<label class="control-label">{{ _lang('Jenis Paket') }}</label>	
									<select class="form-control" name="package_type" required>
										<option value="" hidden>{{ _lang('Pilih Jenis Paket') }}</option>
										<option value="monthly" {{ $user->company->package_type == 'monthly' ? 'selected' : '' }}>{{ _lang('Paket Bulanan') }}</option>
										<option value="yearly" {{ $user->company->package_type == 'yearly' ? 'selected' : '' }}>{{ _lang('Paket Tahunan') }}</option> 
									</select>  
								</div>	
								<br>		
						<div class="form-group">
									<label class="control-label">{{ _lang('Permbayaran') }}</label>						
									<select class="form-control" name="gateway" id="gateway" required>
									    <option value="Ipaymu">{{ _lang('IPaymu') }}</option>
										@if (get_option('paypal_active') == 'Yes')
											<option value="PayPal">{{ _lang('PayPal') }}</option>
										@endif
										@if (get_option('stripe_active') == 'Yes')
											<option value="Stripe">{{ _lang('Stripe') }}</option>
										@endif
										@if (get_option('razorpay_active') == 'Yes')
											<option value="Razorpay">{{ _lang('Razorpay') }}</option>
										@endif
										@if (get_option('paystack_active') == 'Yes')
											<option value="Paystack">{{ _lang('Paystack') }}</option>
										@endif
										    
									</select>
								</div>
								<div class="kt-login__actions">
									<button type="submit" class="btn btn-primary btn-elevate kt-login__btn-primary">{{ _lang('Proses') }}</button>
								</div>
							</form>
							
					</div>	


				</div>	
			</div>
		</div>
	</div>
</div>
@endsection

