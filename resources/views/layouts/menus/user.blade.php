@php $permissions = permission_list(); @endphp

<li class="kt-menu__item kt-menu__item--submenu kt-menu__item--rel @if(Request::is('dashboard')) kt-menu__item--open kt-menu__item--here @endif"  >
	<a href="{{ url('dashboard') }}" class="kt-menu__link">
		<span class="kt-menu__link-icon">
			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
					<rect id="bound" x="0" y="0" width="24" height="24"></rect>
					<path d="M2.56066017,10.6819805 L4.68198052,8.56066017 C5.26776695,7.97487373 6.21751442,7.97487373 6.80330086,8.56066017 L8.9246212,10.6819805 C9.51040764,11.267767 9.51040764,12.2175144 8.9246212,12.8033009 L6.80330086,14.9246212 C6.21751442,15.5104076 5.26776695,15.5104076 4.68198052,14.9246212 L2.56066017,12.8033009 C1.97487373,12.2175144 1.97487373,11.267767 2.56066017,10.6819805 Z M14.5606602,10.6819805 L16.6819805,8.56066017 C17.267767,7.97487373 18.2175144,7.97487373 18.8033009,8.56066017 L20.9246212,10.6819805 C21.5104076,11.267767 21.5104076,12.2175144 20.9246212,12.8033009 L18.8033009,14.9246212 C18.2175144,15.5104076 17.267767,15.5104076 16.6819805,14.9246212 L14.5606602,12.8033009 C13.9748737,12.2175144 13.9748737,11.267767 14.5606602,10.6819805 Z" id="Combined-Shape" fill="#000000" opacity="0.3"></path>
					<path d="M8.56066017,16.6819805 L10.6819805,14.5606602 C11.267767,13.9748737 12.2175144,13.9748737 12.8033009,14.5606602 L14.9246212,16.6819805 C15.5104076,17.267767 15.5104076,18.2175144 14.9246212,18.8033009 L12.8033009,20.9246212 C12.2175144,21.5104076 11.267767,21.5104076 10.6819805,20.9246212 L8.56066017,18.8033009 C7.97487373,18.2175144 7.97487373,17.267767 8.56066017,16.6819805 Z M8.56066017,4.68198052 L10.6819805,2.56066017 C11.267767,1.97487373 12.2175144,1.97487373 12.8033009,2.56066017 L14.9246212,4.68198052 C15.5104076,5.26776695 15.5104076,6.21751442 14.9246212,6.80330086 L12.8033009,8.9246212 C12.2175144,9.51040764 11.267767,9.51040764 10.6819805,8.9246212 L8.56066017,6.80330086 C7.97487373,6.21751442 7.97487373,5.26776695 8.56066017,4.68198052 Z" id="Combined-Shape" fill="#000000"></path>
				</g>
			</svg>
		</span>
		<span class="kt-menu__link-text">{{ _lang('Dashboard') }}</span>
	</a>
</li>


@if( has_feature('websites_limit') )
	<li class="kt-menu__item kt-menu__item--submenu kt-menu__item--rel @if(Request::is('projects') || Request::is('projects/create')) kt-menu__item--open @endif" data-ktmenu-submenu-toggle="hover" aria-haspopup="true">
		<a href="{{ url('projects') }}" class="kt-menu__link kt-menu__toggle">
			<span class="kt-menu__link-icon">
				<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
					<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
						<rect id="bound" x="0" y="0" width="24" height="24"></rect>
						<path d="M20.4061385,6.73606154 C20.7672665,6.89656288 21,7.25468437 21,7.64987309 L21,16.4115967 C21,16.7747638 20.8031081,17.1093844 20.4856429,17.2857539 L12.4856429,21.7301984 C12.1836204,21.8979887 11.8163796,21.8979887 11.5143571,21.7301984 L3.51435707,17.2857539 C3.19689188,17.1093844 3,16.7747638 3,16.4115967 L3,7.64987309 C3,7.25468437 3.23273352,6.89656288 3.59386153,6.73606154 L11.5938615,3.18050598 C11.8524269,3.06558805 12.1475731,3.06558805 12.4061385,3.18050598 L20.4061385,6.73606154 Z" id="Combined-Shape" fill="#000000" opacity="0.3"></path>
						<polygon id="Combined-Shape-Copy" fill="#000000" points="14.9671522 4.22441676 7.5999999 8.31727912 7.5999999 12.9056825 9.5999999 13.9056825 9.5999999 9.49408582 17.25507 5.24126912"></polygon>
					</g>
				</svg>
			</span>
			<span class="kt-menu__link-text">{{ _lang('Aplikasi Saya') }}</span>
			<i class="kt-menu__ver-arrow la la-angle-right"></i>
		</a>
		<div class="kt-menu__submenu ">
			<span class="kt-menu__arrow"></span>
			<ul class="kt-menu__subnav">
			    <?php
				$user_id = \Auth::user()->id;
            
                $projek = \DB::table('projects')
                  ->select('*')
                  ->where('user_id', $user_id)
                  ->get();
				?>
			    @if(count($projek) >= 1)
			    
			    @else
					<li class="kt-menu__item @if(Request::is('projects/create')) kt-menu__item--open @endif"  aria-haspopup="true">
					<a  href="{{ url('projects/create') }}" class="kt-menu__link ">
						<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
						<span class="kt-menu__link-text">{{ _lang('Buat Aplikasi Saya') }}</span>
					</a>
				</li>
				@endif
				<!--<li class="kt-menu__item @if(Request::is('projects')) kt-menu__item--open @endif"  aria-haspopup="true">-->
				<!--	<a  href="{{ url('projects') }}" class="kt-menu__link ">-->
				<!--		<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>-->
				<!--		<span class="kt-menu__link-text">{{ _lang('Atur template saya') }}</span>-->
				<!--	</a>-->
				<!--</li>-->
				
				@if(Auth::user()->company->package_id != 1 && Auth::user()->company->membership_type == 'member')
				<li class="kt-menu__item @if(Request::is('login_refeed')) kt-menu__item--open @endif"  aria-haspopup="true">
					<a  href="{{ url('login_refeed') }}" class="kt-menu__link " target="_blank">
						<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
						<span class="kt-menu__link-text">{{ _lang('Atur Toko Saya') }}</span>
					</a>
				</li>
				@else
					<li class="kt-menu__item @if(Request::is('login_refeed')) kt-menu__item--open @endif"  aria-haspopup="true">
					<a  class="kt-menu__link" data-toggle="modal" data-target="#exampleModalCenter">
						<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
						<span class="kt-menu__link-text">{{ _lang('Atur toko saya') }}</span>
					</a>
				</li>
				@endif
			</ul>
		</div>
	</li>
@endif

@if(Auth::user()->company->package_id != 1 && Auth::user()->company->membership_type == 'member')

        <!--                        <form action="https://staging.refeed.id/xapi/v2/partner/login?" method="post" target="_blank">-->
								<!--	<input type="hidden" name="cmd" value="_xclick">-->
									
								<!--	<input type="hidden" name="key" value="f5415ee1-83ad-4858-b5af-f2d9c0d1bed6"/>-->
								<!--	<input type="hidden" name="merchant_key" value="{{ Auth::user()->merchant_id}}" />-->
								<!--	<input type="hidden" name="password" value="{{ Auth::user()->password }}" />-->
									
								<!--	<input type="submit" name="submit" class="btn btn-primary btn-block" value="Kelola Produk" alt="PayPal - The safer, easier way to pay online.">-->
								<!--</form>-->
								<!--	<a target="_blank"  href="{{ url('login_refeed') }}" class="btn btn-primary btn-block mt-2">-->
        <!--        						<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>-->
        <!--        						<span class="kt-menu__link-text">{{ _lang('Kelola Toko') }}</span>-->
        <!--        					</a>-->
                					
                					
                					
                					
<li class="kt-menu__item kt-menu__item--submenu kt-menu__item--rel "  >
    <form action="https://staging.refeed.id/xapi/v2/partner/login?" id="my_form" method="post" target="_blank">
									<input type="hidden" name="cmd" value="_xclick">
									
									<input type="hidden" name="key" value="f5415ee1-83ad-4858-b5af-f2d9c0d1bed6"/>
									<input type="hidden" name="merchant_key" value="{{ Auth::user()->merchant_id}}" />
									<input type="hidden" name="password" value="{{ Auth::user()->password }}" />
	</form>
        	<a href="javascript:{}" onclick="document.getElementById('my_form').submit(); return false;" class="kt-menu__link">
        		<span class="kt-menu__link-icon">
        			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
        				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                <rect x="0" y="0" width="24" height="24"/>
                <path d="M3.95709826,8.41510662 L11.47855,3.81866389 C11.7986624,3.62303967 12.2013376,3.62303967 12.52145,3.81866389 L20.0429,8.41510557 C20.6374094,8.77841684 21,9.42493654 21,10.1216692 L21,19.0000642 C21,20.1046337 20.1045695,21.0000642 19,21.0000642 L4.99998155,21.0000673 C3.89541205,21.0000673 2.99998155,20.1046368 2.99998155,19.0000673 L2.99999828,10.1216672 C2.99999935,9.42493561 3.36258984,8.77841732 3.95709826,8.41510662 Z M10,13 C9.44771525,13 9,13.4477153 9,14 L9,17 C9,17.5522847 9.44771525,18 10,18 L14,18 C14.5522847,18 15,17.5522847 15,17 L15,14 C15,13.4477153 14.5522847,13 14,13 L10,13 Z" fill="#000000"/>
            </g>
			</svg>
		</span>
		<span style="line-height:20px;" class="kt-menu__link-text">{{ _lang('Toko saya') }}</span>
	</a>
	
</li>

<!--<li class="kt-menu__item kt-menu__item--submenu kt-menu__item--rel "  >-->
<!--        	<a href="{{ url('login_refeed') }}" class="kt-menu__link" target="_blank">-->
<!--        		<span class="kt-menu__link-icon">-->
<!--        			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">-->
<!--        				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">-->
<!--        <rect x="0" y="0" width="24" height="24"/>-->
<!--        <path d="M14,9 L14,8 C14,6.8954305 13.1045695,6 12,6 C10.8954305,6 10,6.8954305 10,8 L10,9 L8,9 L8,8 C8,5.790861 9.790861,4 12,4 C14.209139,4 16,5.790861 16,8 L16,9 L14,9 Z M14,9 L14,8 C14,6.8954305 13.1045695,6 12,6 C10.8954305,6 10,6.8954305 10,8 L10,9 L8,9 L8,8 C8,5.790861 9.790861,4 12,4 C14.209139,4 16,5.790861 16,8 L16,9 L14,9 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>-->
<!--        <path d="M6.84712709,9 L17.1528729,9 C17.6417121,9 18.0589022,9.35341304 18.1392668,9.83560101 L19.611867,18.671202 C19.7934571,19.7607427 19.0574178,20.7911977 17.9678771,20.9727878 C17.8592143,20.9908983 17.7492409,21 17.6390792,21 L6.36092084,21 C5.25635134,21 4.36092084,20.1045695 4.36092084,19 C4.36092084,18.8898383 4.37002252,18.7798649 4.388133,18.671202 L5.86073316,9.83560101 C5.94109783,9.35341304 6.35828794,9 6.84712709,9 Z" fill="#000000"/>-->
<!--    </g>-->
<!--			</svg>-->
<!--		</span>-->
<!--		<span style="line-height:20px;" class="kt-menu__link-text">{{ _lang('Toko Online') }}</span>-->
<!--	</a>-->
	
<!--</li>-->
@else
<!--<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">-->
<!--  Launch demo modal-->
<!--</button>-->
<li class="kt-menu__item kt-menu__item--submenu kt-menu__item--rel "  >
        	<a class="kt-menu__link" data-toggle="modal" data-target="#exampleModalCenter">
        		<span class="kt-menu__link-icon">
        			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
        				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                <rect x="0" y="0" width="24" height="24"/>
                <path d="M3.95709826,8.41510662 L11.47855,3.81866389 C11.7986624,3.62303967 12.2013376,3.62303967 12.52145,3.81866389 L20.0429,8.41510557 C20.6374094,8.77841684 21,9.42493654 21,10.1216692 L21,19.0000642 C21,20.1046337 20.1045695,21.0000642 19,21.0000642 L4.99998155,21.0000673 C3.89541205,21.0000673 2.99998155,20.1046368 2.99998155,19.0000673 L2.99999828,10.1216672 C2.99999935,9.42493561 3.36258984,8.77841732 3.95709826,8.41510662 Z M10,13 C9.44771525,13 9,13.4477153 9,14 L9,17 C9,17.5522847 9.44771525,18 10,18 L14,18 C14.5522847,18 15,17.5522847 15,17 L15,14 C15,13.4477153 14.5522847,13 14,13 L10,13 Z" fill="#000000"/>
            </g>
			</svg>
		</span>
		<span style="line-height:20px;" class="kt-menu__link-text">{{ _lang('Toko saya') }}</span>
	</a>
	
</li>
<!--<li class="kt-menu__item kt-menu__item--submenu kt-menu__item--rel "  >-->
<!--        	<a class="kt-menu__link" data-toggle="modal" data-target="#exampleModalCenter">-->
<!--        		<span class="kt-menu__link-icon">-->
<!--        			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">-->
<!--        				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">-->
<!--        <rect x="0" y="0" width="24" height="24"/>-->
<!--        <path d="M14,9 L14,8 C14,6.8954305 13.1045695,6 12,6 C10.8954305,6 10,6.8954305 10,8 L10,9 L8,9 L8,8 C8,5.790861 9.790861,4 12,4 C14.209139,4 16,5.790861 16,8 L16,9 L14,9 Z M14,9 L14,8 C14,6.8954305 13.1045695,6 12,6 C10.8954305,6 10,6.8954305 10,8 L10,9 L8,9 L8,8 C8,5.790861 9.790861,4 12,4 C14.209139,4 16,5.790861 16,8 L16,9 L14,9 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>-->
<!--        <path d="M6.84712709,9 L17.1528729,9 C17.6417121,9 18.0589022,9.35341304 18.1392668,9.83560101 L19.611867,18.671202 C19.7934571,19.7607427 19.0574178,20.7911977 17.9678771,20.9727878 C17.8592143,20.9908983 17.7492409,21 17.6390792,21 L6.36092084,21 C5.25635134,21 4.36092084,20.1045695 4.36092084,19 C4.36092084,18.8898383 4.37002252,18.7798649 4.388133,18.671202 L5.86073316,9.83560101 C5.94109783,9.35341304 6.35828794,9 6.84712709,9 Z" fill="#000000"/>-->
<!--    </g>-->
<!--			</svg>-->
<!--		</span>-->
<!--		<span style="line-height:20px;" class="kt-menu__link-text">{{ _lang('Toko Online') }}</span>-->
<!--	</a>-->
	
<!--</li>-->


@endif