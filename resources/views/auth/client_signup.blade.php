@extends('theme.default.source')
@section('css')
<style>

body {
    color: #000;
    overflow-x: hidden;
    height: 100%;
    background-color: #B0BEC5;
    background-repeat: no-repeat
}

.card0 {
    box-shadow: 0px 4px 8px 0px #757575;
    border-radius: 0px
}

.card2 {
    margin: 0px 40px
}

.logo {
    width: 200px;
    height: 100px;
    margin-top: 20px;
    margin-left: 35px
}

.image {
    width: 360px;
    height: 280px
}

.border-line {
    border-right: 1px solid #EEEEEE
}

.facebook {
    background-color: #3b5998;
    color: #fff;
    font-size: 18px;
    padding-top: 5px;
    border-radius: 50%;
    width: 35px;
    height: 35px;
    cursor: pointer
}

.twitter {
    background-color: #1DA1F2;
    color: #fff;
    font-size: 18px;
    padding-top: 5px;
    border-radius: 50%;
    width: 35px;
    height: 35px;
    cursor: pointer
}

.linkedin {
    background-color: #2867B2;
    color: #fff;
    font-size: 18px;
    padding-top: 5px;
    border-radius: 50%;
    width: 35px;
    height: 35px;
    cursor: pointer
}

.line {
    height: 2px;
    width: 37%;
    background-color: #E0E0E0;
    margin-top: 10px
}

.or {
    width: 26%;
    font-weight: bold
}

.text-sm {
    font-size: 14px !important
}

::placeholder {
    color: #BDBDBD;
    opacity: 1;
    font-weight: 300
}

:-ms-input-placeholder {
    color: #BDBDBD;
    font-weight: 300
}

::-ms-input-placeholder {
    color: #BDBDBD;
    font-weight: 300
}

input,
textarea {
    padding: 10px 12px 10px 12px;
    border: 1px solid lightgrey;
    border-radius: 2px;
    margin-bottom: 5px;
    margin-top: 2px;
    width: 100%;
    box-sizing: border-box;
    color: #2C3E50;
    font-size: 14px;
    letter-spacing: 1px
}

input:focus,
textarea:focus {
    -moz-box-shadow: none !important;
    -webkit-box-shadow: none !important;
    box-shadow: none !important;
    border: 1px solid #304FFE;
    outline-width: 0
}

button:focus {
    -moz-box-shadow: none !important;
    -webkit-box-shadow: none !important;
    box-shadow: none !important;
    outline-width: 0
}

a {
    color: inherit;
    cursor: pointer
}

.btn-blue {
    background-color: #1A237E;
    width: 150px;
    color: #fff;
    border-radius: 2px
}

.btn-blue:hover {
    background-color: #000;
    cursor: pointer
}

.bg-blue {
    color: #fff;
    background-color: #1A237E
}

@media screen and (max-width: 991px) {
    .logo {
        margin-left: 0px
    }

    .image {
        width: 300px;
        height: 220px
    }

    .border-line {
        border-right: none
    }

    .card2 {
        border-top: 1px solid #EEEEEE !important;
        margin: 0px 15px
    }
}
</style>

@endsection
@section('js')
<script>
    $(document).ready(function () {
      var selectData = unescape(window.location.search.substring(1).split('=')[1]);
      var $select = $('#package');
      $select.val( selectData );
});
</script>
<script>
            $(document).ready(function() {
            $("#show_hide_password a").on('click', function(event) {
                event.preventDefault();
                if($('#show_hide_password input').attr("type") == "text"){
                    $('#show_hide_password input').attr('type', 'password');
                    $('#show_hide_password i').addClass( "bi bi-eye-slash" );
                    $('#show_hide_password i').removeClass( "bi bi-eye" );
                }else if($('#show_hide_password input').attr("type") == "password"){
                    $('#show_hide_password input').attr('type', 'text');
                    $('#show_hide_password i').removeClass( "bi bi-eye-slash" );
                    $('#show_hide_password i').addClass( "bi bi-eye" );
                }
            });
            });
        </script>
        <script>
            $(document).ready(function() {
            $("#show_hide_password2 a").on('click', function(event) {
                event.preventDefault();
                if($('#show_hide_password2 input').attr("type") == "text"){
                    $('#show_hide_password2 input').attr('type', 'password');
                    $('#show_hide_password2 i').addClass( "bi bi-eye-slash" );
                    $('#show_hide_password2 i').removeClass( "bi bi-eye" );
                }else if($('#show_hide_password2 input').attr("type") == "password"){
                    $('#show_hide_password2 input').attr('type', 'text');
                    $('#show_hide_password2 i').removeClass( "bi bi-eye-slash" );
                    $('#show_hide_password2 i').addClass( "bi bi-eye" );
                }
            });
            });
        </script>
        <script>
            function clsAlphaNoOnly (e) {  // Accept only alpha numerics, no special characters 
    var regex = new RegExp("^[a-zA-Z0-9 ]+$");
    var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
    if (regex.test(str)) {
        return true;
    }

    e.preventDefault();
    return false;
}
function clsAlphaNoOnly2 () {  // Accept only alpha numerics, no special characters 
    return clsAlphaNoOnly (this.event); // window.event
}
        </script>
@endsection

@section('content')
<div class="container-fluid px-1 px-md-5 px-lg-1 px-xl-5 py-5 mx-auto" style="font-family:'cera pro light';">
    <div class="card card0 border-0">
        <div class="row d-flex">
            <div class="col-lg-6">
                <div class="card1 pb-5 text-center">
                    <div class="row p-5"><a href="/"><img src="{{asset('public/assets')}}/img/full biru.png" alt="" style="width: 300px"></a>  </div>
                    <div class="row px-3 justify-content-center border-line"> <img src="{{asset('public/assets')}}/img/App.gif" class="image" style="width:auto;height:380px;"> </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="card2 card border-0 px-4 py-5">
                    @if(Session::has('error'))
                                <div class="alert alert-danger text-center">
                                    <strong>{{ session('error') }}</strong>
                                </div>
                            @endif
					
                            @if(Session::has('registration_success'))
                                <div class="alert alert-success text-center">
                                    <strong>{{ session('registration_success') }}</strong>
                                </div>
                            @endif
                    <div class="row px-3 mb-4">
                        <div class="line"></div> <small class="or text-center">Daftar</small>
                        <div class="line"></div>
                    </div>
                    <form method="POST" class="kt-form" autocomplete="off" action="{{ url('register/client_signup') }}">
                        @csrf
                        
                        <div class="form-group row">
                          <label for="name" class="col-sm-4 col-form-label">Nama</label>
                          <div class="col-sm-8">
                            <input id="name" type="text" placeholder="{{ _lang('Nama') }}" onkeypress="clsAlphaNoOnly(event)" onpaste="return false;" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                                        @if ($errors->has('name'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="no_hp" class="col-sm-4 col-form-label">Nomor Telepon</label>
                          <div class="col-sm-8">
                            <input id="no_hp" type="number" placeholder="{{ _lang('Nomor Telepon') }}" class="form-control{{ $errors->has('no_hp') ? ' is-invalid' : '' }}" name="no_hp" value="{{ old('no_hp') }}" required autofocus>

                                        @if ($errors->has('no_hp'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('no_hp') }}</strong>
                                            </span>
                                        @endif
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="email" class="col-sm-4 col-form-label">Alamat E-mail</label>
                          <div class="col-sm-8">
                            <input id="email" type="email" placeholder="{{ _lang('Alamat E-mail') }}" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                        @if ($errors->has('email'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                          </div>
                        </div>
                        
                        <div class="form-group row">
                          
                            <label for="password" class="col-sm-4 col-form-label">Kata Sandi</label>
                            <div class="input-group col-sm-8" id="show_hide_password">
                                
                                    <input id="password" type="password" placeholder="{{ _lang('Kata Sandi') }}" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                                    <div class="input-group-addon" style="margin-top:2px;">
                                                                        <a href="" class="btn btn-outline-secondary"><i class="bi bi-eye-slash" aria-hidden="true"></i></a>
                                                                    </div>
                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                
                            </div>
                         
                        </div>
                        <div class="form-group row">
                          
                            <label for="password-confirm" class="col-sm-4 col-form-label">Kata Sandi Konfirmasi</label>
                            <div class="input-group col-sm-8" id="show_hide_password2">
                                <input id="password-confirm" type="password" class="form-control" placeholder="{{ _lang('Kata Sandi Konfirmasi') }}" name="password_confirmation" required>
                                <div class="input-group-addon" style="margin-top:2px;">
                                    <a href="" class="btn btn-outline-secondary"><i class="bi bi-eye-slash" aria-hidden="true"></i></a>
                                </div>
                            </div>
                         
                        </div>
                        <div class="form-group row">
                            <label for="package" class="col-sm-4 col-form-label">Paket</label>
                            <div class="col-sm-8">
                                <select id="package" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" name="package" required>
                                    <option value="0" hidden selected>Pilih Paket</option>
                                    <!--<option value="4" selected>Trial</option>-->
                                    <!--<option value="0">Pilih Paket</option>-->
                                    <option value="1">Ngelapak Basic</option>
                                    <option value="2">Ngelapak Pro</option>
                                    <option value="3">Ngelapak Advanced</option>
                                    {{-- {{ create_option('packages','id','package_name') }} --}}
                                </select>    

                                @if ($errors->has('package'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('package') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="package_type" class="col-sm-4 col-form-label">Jenis Paket</label>
                            <div class="col-sm-8">
                                <select id="package_type" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" name="package_type" required>
                                    <option value="" hidden selected>{{ _lang('Pilih Jenis Paket') }}</option>
                                    <option value="monthly">{{ _lang('Paket Bulanan') }}</option>
                                    <option value="yearly">{{ _lang('Paket Tahunan') }}</option> 
                                </select>    

                                @if ($errors->has('package_type'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('package_type') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <hr>
                        <div class="form-group row">
                          <div class="col-6">
                            {{-- <button type="submit" class="btn btn-primary">Daftar</button> --}}
                            <button type="submit" class="btn btn-primary btn-elevate kt-login__btn-primary">{{ _lang('Daftar') }}</button>
                          </div>
                          <div class="col-6" style="text-align: right;">
                              Sudah punya akun? <a href="{{ url('login') }}" class="text-decoration-underline" >Masuk</a>

                          </div>
                        </div>
                        
                      </form>
                </div>
            </div>
        </div>
        <div class="bg-blue py-4">
            <div class="row px-3 col-12"> <small class="ml-4 ml-sm-5 mb-2">Copyright &copy; 2021. <strong>PT Ngelapak Bersama Kita. </strong> All rights reserved.</small>
                <div class="social-contact ml-4 ml-sm-auto"> <span class="fa fa-facebook mr-4 text-sm"></span> <span class="fa fa-google-plus mr-4 text-sm"></span> <span class="fa fa-linkedin mr-4 text-sm"></span> <span class="fa fa-twitter mr-4 mr-sm-5 text-sm"></span> </div>
            </div>
        </div>
    </div>
</div>
@endsection
