
@extends('theme.default.source')
@section('css')
<style>

body {
    color: #000;
    overflow-x: hidden;
    height: 100%;
    background-color: #B0BEC5;
    background-repeat: no-repeat
}

.card0 {
    box-shadow: 0px 4px 8px 0px #757575;
    border-radius: 0px
}

.card2 {
    margin: 0px 40px
}

.logo {
    width: 200px;
    height: 100px;
    margin-top: 20px;
    margin-left: 35px
}

.image {
    width: 360px;
    height: 280px
}

.border-line {
    border-right: 1px solid #EEEEEE
}

.facebook {
    background-color: #3b5998;
    color: #fff;
    font-size: 18px;
    padding-top: 5px;
    border-radius: 50%;
    width: 35px;
    height: 35px;
    cursor: pointer
}

.twitter {
    background-color: #1DA1F2;
    color: #fff;
    font-size: 18px;
    padding-top: 5px;
    border-radius: 50%;
    width: 35px;
    height: 35px;
    cursor: pointer
}

.linkedin {
    background-color: #2867B2;
    color: #fff;
    font-size: 18px;
    padding-top: 5px;
    border-radius: 50%;
    width: 35px;
    height: 35px;
    cursor: pointer
}

.line {
    height: 1px;
    width: 35%;
    background-color: #E0E0E0;
    margin-top: 10px
}

.or {
    width: 30%;
    font-weight: bold
}

.text-sm {
    font-size: 14px !important
}

::placeholder {
    color: #BDBDBD;
    opacity: 1;
    font-weight: 300
}

:-ms-input-placeholder {
    color: #BDBDBD;
    font-weight: 300
}

::-ms-input-placeholder {
    color: #BDBDBD;
    font-weight: 300
}

input,
textarea {
    padding: 10px 12px 10px 12px;
    border: 1px solid lightgrey;
    border-radius: 2px;
    margin-bottom: 5px;
    margin-top: 2px;
    width: 100%;
    box-sizing: border-box;
    color: #2C3E50;
    font-size: 14px;
    letter-spacing: 1px
}

input:focus,
textarea:focus {
    -moz-box-shadow: none !important;
    -webkit-box-shadow: none !important;
    box-shadow: none !important;
    border: 1px solid #304FFE;
    outline-width: 0
}

button:focus {
    -moz-box-shadow: none !important;
    -webkit-box-shadow: none !important;
    box-shadow: none !important;
    outline-width: 0
}

a {
    color: inherit;
    cursor: pointer
}

.btn-blue {
    background-color: #1A237E;
    width: 150px;
    color: #fff;
    border-radius: 2px
}

.btn-blue:hover {
    background-color: #000;
    cursor: pointer
}

.bg-blue {
    color: #fff;
    background-color: #1A237E
}

@media screen and (max-width: 991px) {
    .logo {
        margin-left: 0px
    }

    .image {
        width: 300px;
        height: 220px
    }

    .border-line {
        border-right: none
    }

    .card2 {
        border-top: 1px solid #EEEEEE !important;
        margin: 0px 15px
    }
}
</style>

@endsection
@section('js')

<script type="text/javascript">
    var toHtml = (tag, value) => {
        $(tag).html(value);
    }
    $(document).ready(function() {
        //  $('#province_id').select2();
        //  $('#cities_id').select2();
        $('#province_id').on('change', function() {
            var id = $('#province_id').val();
            var url = window.location.href;
            var urlNya = url.substring(0, url.lastIndexOf('/signup_step/'));
            $.ajax({
                type: 'GET',
                url: urlNya + '/getcity/' + id,
                dataType: 'json',
                success: function(data) {
                    var op = '<option hidden value="">Pilih Kota</option>';
                    if (data.length > 0) {
                        var i = 0;
                        for (i = 0; i < data.length; i++) {
                            op += `<option value="${data[i].city_id}">${data[i].title}</option>`
                        }
                    }
                    toHtml('[name="kota"]', op);
                }
            })
        })
    });
</script>
<script type="text/javascript">
    var toHtml = (tag, value) => {
        $(tag).html(value);
    }
    $(document).ready(function() {
        //  $('#province_id').select2();
        //  $('#cities_id').select2();
        $('#cities_id').on('change', function() {
            var id = $('#cities_id').val();
            var url = window.location.href;
            var urlNya = url.substring(0, url.lastIndexOf('/alamat/'));
            $.ajax({
                type: 'GET',
                url: urlNya + '/getsubdistrict/' + id,
                dataType: 'json',
                success: function(data) {
                    var op = '<option hidden value="">Pilih Kecamatan</option>';
                    if (data.length > 0) {
                        var i = 0;
                        for (i = 0; i < data.length; i++) {
                            op += `<option value="${data[i].subdistrict_id}">${data[i].subdistrict_name}</option>`
                        }
                    }
                    toHtml('[name="kec"]', op);
                }
            })
        })
    });
</script>
@endsection


@section('content')
<div class="container-fluid px-1 px-md-5 px-lg-1 px-xl-5 py-5 mx-auto" style="font-family:'cera pro light';">
    <div class="card card0 border-0">
        <div class="row d-flex">
            <div class="col-lg-6">
                <div class="card1 pb-5">
                    <div class="row p-5"><img src="{{asset('public/assets')}}/img/full biru.png" alt="" style="width: 300px"> </div>
                    <div class="row px-3 justify-content-center border-line"> <img src="{{asset('public/assets')}}/img/App.gif" class="image" style="width:auto;height:380px;"> </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="card2 card border-0 px-4 py-5">
                    <div class="row px-3 mb-4">
                        <div class="line"></div> <small class="or text-center">Data Diri</small>
                        <div class="line"></div>
                    </div>
                    <form action="{{ url('update_step')}}" autocomplete="off" class="form-horizontal form-groups-bordered validate" enctype="multipart/form-data" method="post">
                        @csrf
                        
                        <div class="form-group row">
                            <label class="control-label col-lg-4 col-form-label">{{ _lang('Nama Lengkap') }}</label>
                            <div class="col-lg-8">

                                <input type="text" class="form-control" placeholder="Masukan nama lengkap anda" name="nama_lengkap" value="{{$profile->nama_lengkap}}" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-lg-4 col-form-label">{{ _lang('Jenis Kelamin') }}</label>
                            <div class="col-lg-8">
                                <select class="form-select" name="jk" aria-label="Default select example" required>
                                    <option hidden disabled selected>Pilih Jenis Kelamin</option>
                                    <option hidden 
                                    <?php 
                                    if($profile->jk == 'Laki-laki' || 'Perempuan')
                                    { 
                                        echo 'selected';
                                    }elseif ($profile->jk == Null || ''){
                                        ' ';
                                    } 
                                    
                                    ?> value="{{$profile->jk}}">
                                    
                                    
                                    @if ($profile->jk == Null || '')
                                        Pilih Jenis Kelamin
                                    @else
                                    {{$profile->jk}}
                                    @endif
                                    </option>
                                    <option value="Laki-Laki">Laki-laki</option>
                                    <option value="Perempuan">Perempuan</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-lg-4 col-form-label">{{ _lang('Tanggal Lahir') }}</label>
                            <div class="col-lg-8">
                                <input type="date" class="form-control" name="tgl_lahir" placeholder="Masukan tanggal lahir" value="{{$profile->tgl_lahir}}" required>
                            </div>
                        </div>
                        <div class="form-group row" hidden>
                            <label class="control-label col-lg-4">{{ _lang('Nomor HP/WhatsApp') }}</label>
                            <div class="col-lg-8">
                                <input type="number" class="form-control" name="no_hp" placeholder="Masukan nomor HP/WhatsApp" value="{{$profile->no_hp}}" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-lg-4 col-form-label">{{ _lang('NIK') }}</label>
                            <div class="col-lg-8">
                                <input type="number" class="form-control" name="nik" placeholder="Masukan NIK" value="{{$profile->nik}}" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-lg-4 col-form-label">{{ _lang('Nama Usaha') }}</label>
                            <div class="col-lg-8">
                                <input type="text" class="form-control" name="business_name" placeholder="Masukan nama usaha Anda" value="{{$profile->company->business_name}}" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">{{ _lang('Alamat') }}</label>
                            {{-- <input type="textarea" class="form-control textarea-group" name="alamat" placeholder="Masukan alamat" value="{{$profile->nik}}" required> --}}
                            <div class="row">
                                <div class="col-lg-6">
                                    <select name="prov" id="province_id" class="form-control" required>
                                        <option value="" hidden>-- Pilih Provinsi --</option>
                                        @foreach($province as $provinsi)
                                        <option value="{{ $provinsi->province_id }}" <?php if ($profile->prov == $provinsi->province_id) {
                                                        echo 'selected';
                                                      } ?>>{{ $provinsi->title }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-lg-6">
                                    <select name="kota" id="cities_id" class="form-control" required>
                                            <option value="" hidden selected>Pilih Kota</option>
                                            @foreach($city as $cit)
                                            <option value="{{ $cit->city_id }}" <?php if ($profile->kota == $cit->city_id) {
                                                            echo 'selected';
                                                          } ?>>{{ $cit->title }}</option>
                                            @endforeach
                                    </select>
                                </div>
                                

                            </div>
                            
                            
                                <select name="kec" id="subdistrict_id" class="form-control" required>
                                     @foreach($subdistrict as $sub)
                                     <option value="" hidden selected>Pilih Kecamatan</option>
                                            <option value="{{ $sub->subdistrict_id }}" <?php if ($profile->kec == $sub->subdistrict_id) {
                                                            echo 'selected';
                                                          } ?>>{{ $sub->subdistrict_name}}</option>
                                            @endforeach
                                </select>
                            
                                
                            <input type="text" class="form-control" name="kode_pos" placeholder="Kode Pos" value="{{$profile->kode_pos}}" required>
                            <input type="text" class="form-control" name="alamat" placeholder="Jalan, RT RW, No Rumah, Kelurahan" value="{{$profile->alamat}}" required>
                        </div>


                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">{{ _lang('Simpan') }}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="bg-blue py-4">
            <div class="row px-3"> <small class="ml-4 ml-sm-5 mb-2">Copyright &copy; 2021. <strong>PT Ngelapak Bersama Kita. </strong> All rights reserved.</small>
                <div class="social-contact ml-4 ml-sm-auto"> <span class="fa fa-facebook mr-4 text-sm"></span> <span class="fa fa-google-plus mr-4 text-sm"></span> <span class="fa fa-linkedin mr-4 text-sm"></span> <span class="fa fa-twitter mr-4 mr-sm-5 text-sm"></span> </div>
            </div>
        </div>
    </div>
</div>
@endsection

