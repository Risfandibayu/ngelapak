@extends('layouts.app')
@section('style')
<style>
.reveal-if-active {
	 opacity: 0;
	 max-height: 0;
	 overflow: hidden;
	 font-size: 16px;
	 transform: scale(0.8);
	 transition: 0.5s;
}
 .reveal-if-active label {
	 display: block;
	 margin: 0 0 3px 0;
}
 .reveal-if-active input[type=text] {
	 width: 100%;
}
 input[type="radio"]:checked ~ .reveal-if-active, input[type="checkbox"]:checked ~ .reveal-if-active {
	 opacity: 1;
	 max-height: 2000px;
	 padding: 10px 20px;
	 transform: scale(1);
	 overflow: visible;
}

    </style>
@endsection
@section('js-script')
<script>
          var FormStuff = {
  
  init: function() {
    this.applyConditionalRequired();
    this.bindUIActions();
  },
  
  bindUIActions: function() {
    $("input[type='radio'], input[type='checkbox']").on("change", this.applyConditionalRequired);
  },
  
  applyConditionalRequired: function() {
    
    $(".require-if-active").each(function() {
      var el = $(this);
      if ($(el.data("require-pair")).is(":checked")) {
        el.prop("required", true);
      } else {
        el.prop("required", false);
      }
    });
    
  }
  
};

FormStuff.init();
</script>

<script type="text/javascript">
    var toHtml = (tag, value) => {
        $(tag).html(value);
    }
    $(document).ready(function() {
        //  $('#province_id').select2();
        //  $('#cities_id').select2();
        $('#province_id').on('change', function() {
            var id = $('#province_id').val();
            var url = window.location.href;
            var urlNya = url.substring(0, url.lastIndexOf('/profile/edit/'));
            $.ajax({
                type: 'GET',
                url: urlNya + '/getcity/' + id,
                dataType: 'json',
                success: function(data) {
                    var op = '<option hidden value="">Pilih Kota</option>';
                    if (data.length > 0) {
                        var i = 0;
                        for (i = 0; i < data.length; i++) {
                            op += `<option value="${data[i].city_id}">${data[i].title}</option>`
                        }
                    }
                    toHtml('[name="kota"]', op);
                }
            })
        })
    });
</script>
<script type="text/javascript">
    var toHtml = (tag, value) => {
        $(tag).html(value);
    }
    $(document).ready(function() {
        //  $('#province_id').select2();
        //  $('#cities_id').select2();
        $('#cities_id').on('change', function() {
            var id = $('#cities_id').val();
            var url = window.location.href;
            var urlNya = url.substring(0, url.lastIndexOf('/profile/edit/'));
            $.ajax({
                type: 'GET',
                url: urlNya + '/getsubdistrict/' + id,
                dataType: 'json',
                success: function(data) {
                    var op = '<option hidden value="">Pilih Kecamatan</option>';
                    if (data.length > 0) {
                        var i = 0;
                        for (i = 0; i < data.length; i++) {
                            op += `<option value="${data[i].subdistrict_id}">${data[i].subdistrict_name}</option>`
                        }
                    }
                    toHtml('[name="kec"]', op);
                }
            })
        })
    });
</script>
@endsection
@section('content')
<div class="row">
	<div class="col-12">
		<div class="card">
		    <h5 class="card-header bg-primary text-white mt-0 panel-title">{{ _lang('Data Usaha Saya') }}</h5>
			<div class="card-body">
				<div class="row">
				    <div class="col-lg-1"></div>
					<div class="col-lg-10 justify-content-center">
						<form action="{{ url('perusahaan/update')}}" autocomplete="off" class="form-horizontal form-groups-bordered validate" enctype="multipart/form-data" method="post">
							@csrf
							<div class="form-group justify-content-center" style="text-align: center;">
							    <label style="font-size:25px;">{{ _lang('Data Perusahaan') }}</label>
							</div>
							
							
							<div class="form-group">
								<label class="control-label">{{ _lang('Nama Usaha') }}</label>
								<input type="text" class="form-control" placeholder="Nama Usaha" name="business_name" value="{{$company->business_name}}" required>
							</div>
							<div class="form-group">
								<label class="control-label">{{ _lang('Alamat Lengkap') }}</label>
								<input type="text" class="form-control" name="alamat" placeholder="Jalan, RT RW, No Rumah, Kelurahan"  value="{{$company->alamat}}" required>
							</div>
							<div class="form-group row">
								<div class="col-lg-6">
									<label class="control-label">{{ _lang('Provinsi') }}</label>
									<select name="prov" id="province_id" class="form-control" required>
                                        <option value="" hidden>-- Pilih Provinsi --</option>
                                        @foreach($province as $provinsi)
                                        <option value="{{ $provinsi->province_id }}" <?php if ($company->prov == $provinsi->province_id) {
                                                        echo 'selected';
                                                      } ?>>{{ $provinsi->title }}</option>
                                        @endforeach
                                    </select>
								</div>
								<div class="col-lg-6">
								    <label class="control-label">{{ _lang('Kota/Kab') }}</label>
									<select name="kota" id="cities_id" class="form-control" required>
									    <option value="" hidden selected>Pilih Kota</option>
                                            @foreach($city as $cit)
                                            <option value="{{ $cit->city_id }}" <?php if ($company->kota == $cit->city_id) {
                                                            echo 'selected';
                                                          } ?>>{{ $cit->title }}</option>
                                            @endforeach
                                    </select>
								</div>
							</div>
							<div class="form-group">
							    <label class="control-label">{{ _lang('Kecamatan') }}</label>
							    <select name="kec" id="subdistrict_id" class="form-control" required>
							        <option value="" hidden selected>Pilih Kecamatan</option>
                                     @foreach($subdistrict as $sub)
                                            <option value="{{ $sub->subdistrict_id }}" <?php if ($company->kec == $sub->subdistrict_id) {
                                                            echo 'selected';
                                                          } ?>>{{ $sub->subdistrict_name}}</option>
                                            @endforeach
                                </select>
							</div>
							<div class="form-group">
								<label class="control-label">{{ _lang('Kode Pos') }}</label>
								<input type="number" class="form-control" placeholder="Kode Pos" name="kode_pos" value="{{$company->kode_pos}}" required>
							</div>
							
							
							<div class="form-group row">
							    <div class="col-lg-2">
								<label class="control-label">{{ _lang('Memiliki Cabang?') }}</label>
								<div class="form-check">
									<input class="form-check-input" type="radio" name="cabang" id="flexRadioDefault1" value="ya"
									@if ($company->cabang == 'ya')
										checked
									@else
										
									@endif
									>
									<label class="form-check-label" for="flexRadioDefault1">
									  Ya
									</label>
								  </div>
								  <div class="form-check">
									<input class="form-check-input" type="radio" name="cabang" id="flexRadioDefault2" value="tidak"
									@if ($company->cabang == 'tidak')
										checked
									@else
										
									@endif
									>
									<label class="form-check-label" for="flexRadioDefault2">
									  Tidak
									</label>
								  </div>
								</div>
							    <div class="col-lg-10">
    								<label class="control-label">{{ _lang('Deskripsi Singkat') }}</label>
    								<input type="text" class="form-control" name="deskripsi" placeholder="Deskripsi Singkat"  value="{{$company->deskripsi}}" required>
    								{{-- <textarea name="deskripsi" class="form-control" id="" rows="2">
    									@if ($company->deskripsi != null)
    									{{$company->deskripsi}}
    									@else
    									@endif
    								</textarea> --}}
								</div>
							</div>
							<div class="form-group">
								<label class="control-label">{{ _lang('Tagline/Slogan') }}</label>
								<input type="text" class="form-control" placeholder="Tagline/Slogan" name="slogan" value="{{$company->slogan}}">
							</div><div class="form-group">
								<div class="row">
									<div class="col-lg-4">
										<label class="control-label">{{ _lang('Instagram') }}</label>
										<input type="text" class="form-control" placeholder="Instagram" name="ig" value="{{$company->ig}}" >
									</div>
									<div class="col-lg-4">
										<label class="control-label">{{ _lang('Facebook') }}</label>
										<input type="text" class="form-control" placeholder="Facebook" name="fb" value="{{$company->fb}}" >
									</div>
									<div class="col-lg-4">
										<label class="control-label">{{ _lang('Website') }}</label>
										<input type="text" class="form-control" placeholder="Website" name="web" value="{{$company->web}}" >
									</div>
								</div>
							</div>
							<!--<hr>-->
							
          
          <hr>
                            <div class="form-group">
                                <!--<label class="control-label">{{ _lang('Mau buat aplikasi sendiri?') }}</label><span style="color:red;"> *</span><br>-->
                                <div class="form-group justify-content-center" style="text-align: center;">
        							    <label style="font-size:25px;">{{ _lang('Mau buat aplikasi sendiri?') }}</label>
        							</div>
								<div class="form-check">
									<input class="form-check-input " type="radio" name="self_serv" id="selfya" value="ya" required
									@if ($company->self_serv == 'ya')
										checked
									@else
										
									@endif
									>
									<label class="btn btn-secondary " for="selfya">
									  Ya, Saya mau buat berkreasi sendiri
									</label>
								  </div>
								  <div class="form-check">
									<input class="form-check-input" type="radio" name="self_serv" id="selftidak" value="tidak"
									@if ($company->self_serv == 'tidak')
										checked
									@else
										
									@endif
									>
									<label class="btn btn-secondary " for="selftidak">
									  Buatkan Aplikasi Saya
									</label>
									<div class="reveal-if-active">
                                     <hr>
                                    <div class="form-group justify-content-center" style="text-align: center;">
        							    <label style="font-size:25px;">{{ _lang('Data Konten') }}</label>
        							</div>
            							<div class="form-group">
            								<label class="control-label">{{ _lang('Logo') }} <span style="color:red;"> *</span> <small>(512 X 512) format png | max 1 mb</small></label>
            								<input data-require-pair="#selftidak" type="file" class="require-if-active form-control dropify" data-default-file="{{ $company->logo != "" ? asset('public/uploads/logo_usaha/'.$company->logo) : '' }}" name="logo" accept=".png"  data-allowed-file-extensions="png PNG" >
            							</div>
            							<div class="form-group row">
            							    <div class="col-lg-3">
                								<label class="control-label">{{ _lang('Foto Produk 1') }}<span style="color:red;"> *</span><br><small> format png/jpg | max 1 mb</small></label>
                								<input data-require-pair="#selftidak" type="file" class="require-if-active form-control dropify" data-default-file="{{ $company->ft_prd1 != "" ? asset('public/uploads/foto_produk/'.$company->ft_prd1) : '' }}" name="ft_prd1" accept=".jpg,.png"  data-allowed-file-extensions="png jpg jpeg PNG JPG JPEG" >
            							    </div>
            							    <div class="col-lg-3">
                								<label class="control-label">{{ _lang('Foto Produk 2') }} <br><small> format png/jpg | max 1 mb</small></label>
                								<input type="file" class="form-control dropify" data-default-file="{{ $company->ft_prd2 != "" ? asset('public/uploads/foto_produk/'.$company->ft_prd2) : '' }}" name="ft_prd2" accept=".jpg,.png"  data-allowed-file-extensions="png jpg jpeg PNG JPG JPEG">
            							    </div>
            							    <div class="col-lg-3">
                								<label class="control-label">{{ _lang('Foto Produk 3') }} <br><small> format png/jpg | max 1 mb</small></label>
                								<input type="file" class="form-control dropify" data-default-file="{{ $company->ft_prd3 != "" ? asset('public/uploads/foto_produk/'.$company->ft_prd3) : '' }}" name="ft_prd3" accept=".jpg,.png"  data-allowed-file-extensions="png jpg jpeg PNG JPG JPEG">
            							    </div>
            							    <div class="col-lg-3">
                								<label class="control-label">{{ _lang('Foto Produk 4') }} <br><small> format png/jpg | max 1 mb</small></label>
                								<input type="file" class="form-control dropify" data-default-file="{{ $company->ft_prd4 != "" ? asset('public/uploads/foto_produk/'.$company->ft_prd4) : '' }}" name="ft_prd4" accept=".jpg,.png"  data-allowed-file-extensions="png jpg jpeg PNG JPG JPEG">
            							    </div>
            							</div>
            							<div class="form-group">
            								<label class="control-label">{{ _lang('Splash Screen') }}<span style="color:red;"> *</span> <small>(2732 X 2732) format png/jpg | max 1 mb</small></label>
            								<input data-require-pair="#selftidak" type="file" class="require-if-active form-control dropify" data-default-file="{{ $company->splash != "" ? asset('public/uploads/splash/'.$company->splash) : '' }}" name="splash" accept=".jpg,.png"  data-allowed-file-extensions="png jpg jpeg PNG JPG JPEG">
            							</div>
            							<div class="form-group">
            								<label class="control-label">{{ _lang('Background Aplikasi') }}<span style="color:red;"> *</span> <small>Potrait : 1920 X 1080 , Landscape : 1080 X 1920 format png/jpg | max 1 mb</small></label>
            								<input data-require-pair="#selftidak" type="file" class="require-if-active form-control dropify" data-default-file="{{ $company->bg_apk != "" ? asset('public/uploads/bg_apk/'.$company->bg_apk) : '' }}" name="bg_apk" accept=".jpg,.png"  data-allowed-file-extensions="png jpg jpeg PNG JPG JPEG" >
            							</div>
							            <hr>
                                    </div>
								  </div>
								  
								  
							
							
                            </div>
							
							
							
							
							
							
							
							<hr>
							<div class="form-group justify-content-center" style="text-align: center;">
							    <label style="font-size:25px;">{{ _lang('Data Campaign') }}</label>
							</div>
							<div class="form-group">
								<label class="control-label">{{ _lang('Judul Campaign') }}</label>
								<input type="text" class="form-control" placeholder="Judul Campaign" name="judul_camp" value="{{$company->judul_camp}}" >
							</div>
							<div class="form-group">
								<label class="control-label">{{ _lang('Sub Judul') }}</label>
								<input type="text" class="form-control" placeholder="Sub Judul" name="sub_judul" value="{{$company->sub_judul}}" >
							</div>
							<div class="form-group">
								<label class="control-label">{{ _lang('Keyword Brand') }}</label>
								<input type="text" class="form-control" placeholder="Keyword Brand" name="key_brand" value="{{$company->key_brand}}" >
							</div>
							<div class="form-group">
								<label class="control-label">{{ _lang('Deskripsi Campaign') }}</label>
								<input type="text" class="form-control" placeholder="Deskripsi Campaign" name="deskripsi_camp" value="{{$company->deskripsi_camp}}" >
							</div>
							<div class="form-group">
								<label class="control-label">{{ _lang('Cover') }}<span style="color:red;"> *</span><small>(400 X 400) format png/jpg | max 1 mb</small></label>
								<input type="file" class="form-control dropify" data-default-file="{{ $company->cover != "" ? asset('public/uploads/cover/'.$company->cover) : '' }}" name="cover" accept=".jpg,.png"  data-allowed-file-extensions="png jpg jpeg PNG JPG JPEG" >
							</div>
							<div class="form-group">
								<label class="control-label">{{ _lang('Thumbnail') }}<span style="color:red;"> *</span><small>(400 X 400) format png/jpg | max 1 mb</small></label>
								<input type="file" class="form-control dropify" data-default-file="{{ $company->thumbnail != "" ? asset('public/uploads/thumbnail/'.$company->thumbnail) : '' }}" name="thumbnail" accept=".jpg,.png"  data-allowed-file-extensions="png jpg jpeg PNG JPG JPEG" >
							</div><div class="form-group">
								<label class="control-label">{{ _lang('Logo Campaign') }}<span style="color:red;"> *</span><small>(512 X 512) format png | max 1 mb</small></label>
								<input type="file" class="form-control dropify" data-default-file="{{ $company->logo_camp != "" ? asset('public/uploads/logo_camp/'.$company->logo_camp) : '' }}" name="logo_camp" accept=".png"  data-allowed-file-extensions="png PNG">
							</div><div class="form-group">
								<label class="control-label">{{ _lang('Gambar Campaign') }}<span style="color:red;"> *</span><small> format png/jpg | max 1 mb</small></label>
								<input type="file" class="form-control dropify" data-default-file="{{ $company->gambar_camp != "" ? asset('public/uploads/gambar_camp/'.$company->gambar_camp) : '' }}" name="gambar_camp" accept=".jpg,.png"  data-allowed-file-extensions="png jpg jpeg PNG JPG JPEG" >
							</div>
							
							<div class="form-group">
								<button type="submit" class="btn btn-primary">{{ _lang('Save') }}</button>
							</div>
						</form>
					</div>	


				</div>	
			</div>
		</div>
	</div>
</div>
@endsection

