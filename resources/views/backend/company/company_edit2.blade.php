@extends('layouts.app')
@section('content')
<div class="row">
	<div class="col-12">
		<div class="card">
		    <h5 class="card-header bg-primary text-white mt-0 panel-title">{{ _lang('Pengaturan Data Usaha') }}</h5>
			<div class="card-body">
				<div class="row">
					<div class="col-lg-12">
						<form action="{{ url('perusahaan/update')}}" autocomplete="off" class="form-horizontal form-groups-bordered validate" enctype="multipart/form-data" method="post">
							@csrf
							<div class="form-group justify-content-center" style="text-align: center;">
							    <label style="font-size:25px;">{{ _lang('Data Perusahaan') }}</label>
							</div>
							<div class="form-group">
								<label class="control-label">{{ _lang('Nama Usaha') }}</label>
								<input type="text" class="form-control" placeholder="Nama Usaha" name="business_name" value="{{$company->business_name}}" required>
							</div>
							<div class="form-group">
								<label class="control-label">{{ _lang('Alamat Lengkap') }}</label>
								<input type="text" class="form-control" name="alamat" placeholder="Jalan, Kelurahan, Kecamatan "  value="{{$company->alamat}}" required>
							</div>
							<div class="form-group row">
								<div class="col-lg-6">
									<label class="control-label">{{ _lang('Kota') }}</label>
									<input type="text" class="form-control" name="kota" placeholder="Kota" value="{{$company->kota}}" required>
								</div>
								<div class="col-lg-6">
									<label class="control-label">{{ _lang('Provinsi') }}</label>
									<input type="text" class="form-control" name="prov" placeholder="Provinsi" value="{{$company->prov}}" required>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label">{{ _lang('Kode Pos') }}</label>
								<input type="number" class="form-control" placeholder="Kode Pos" name="kode_pos" value="{{$company->kode_pos}}" required>
							</div>
							<div class="form-group row">
							    <div class="col-lg-2">
								<label class="control-label">{{ _lang('Memiliki Cabang?') }}</label>
								<div class="form-check">
									<input class="form-check-input" type="radio" name="cabang" id="flexRadioDefault1" value="ya"
									@if ($company->cabang == 'ya')
										checked
									@else
										
									@endif
									>
									<label class="form-check-label" for="flexRadioDefault1">
									  Ya
									</label>
								  </div>
								  <div class="form-check">
									<input class="form-check-input" type="radio" name="cabang" id="flexRadioDefault2" value="tidak"
									@if ($company->cabang == 'tidak')
										checked
									@else
										
									@endif
									>
									<label class="form-check-label" for="flexRadioDefault2">
									  Tidak
									</label>
								  </div>
								</div>
							    <div class="col-lg-10">
    								<label class="control-label">{{ _lang('Deskripsi Singkat') }}</label>
    								<input type="text" class="form-control" name="deskripsi" placeholder="Deskripsi Singkat"  value="{{$company->deskripsi}}" required>
    								{{-- <textarea name="deskripsi" class="form-control" id="" rows="2">
    									@if ($company->deskripsi != null)
    									{{$company->deskripsi}}
    									@else
    									@endif
    								</textarea> --}}
								</div>
								
							</div>
							<div class="form-group">
								<label class="control-label">{{ _lang('Tagline/Slogan') }}</label>
								<input type="text" class="form-control" placeholder="Tagline/Slogan" name="slogan" value="{{$company->slogan}}">
							</div><div class="form-group">
								<div class="row">
									<div class="col-lg-4">
										<label class="control-label">{{ _lang('Instagram') }}</label>
										<input type="text" class="form-control" placeholder="Instagram" name="ig" value="{{$company->ig}}" >
									</div>
									<div class="col-lg-4">
										<label class="control-label">{{ _lang('Facebook') }}</label>
										<input type="text" class="form-control" placeholder="Facebook" name="fb" value="{{$company->fb}}" >
									</div>
									<div class="col-lg-4">
										<label class="control-label">{{ _lang('Website') }}</label>
										<input type="text" class="form-control" placeholder="Website" name="web" value="{{$company->web}}" >
									</div>
								</div>
							</div>
							<hr>
							<div class="form-group justify-content-center" style="text-align: center;">
							    <label style="font-size:25px;">{{ _lang('Data Konten') }}</label>
							</div>
							<div class="form-group">
								<label class="control-label">{{ _lang('Logo') }} <span style="color:red;"> *</span> <small>(512 X 512) format .png</small></label>
								<input type="file" class="form-control dropify" data-default-file="{{ $company->logo != "" ? asset('public/uploads/logo_usaha/'.$company->logo) : '' }}" name="logo" accept=".png"  data-allowed-file-extensions="png PNG" required>
							</div>
							<div class="form-group row">
							    <div class="col-lg-3">
    								<label class="control-label">{{ _lang('Foto Produk 1') }}<span style="color:red;"> *</span><small> format .png, .jpg</small></label>
    								<input type="file" class="form-control dropify" data-default-file="{{ $company->ft_prd1 != "" ? asset('public/uploads/foto_produk/'.$company->ft_prd1) : '' }}" name="ft_prd1" accept=".jpg,.png"  data-allowed-file-extensions="png jpg jpeg PNG JPG JPEG" required>
							    </div>
							    <div class="col-lg-3">
    								<label class="control-label">{{ _lang('Foto Produk 2') }} <small> format .png, .jpg</small></label>
    								<input type="file" class="form-control dropify" data-default-file="{{ $company->ft_prd2 != "" ? asset('public/uploads/foto_produk/'.$company->ft_prd2) : '' }}" name="ft_prd2" accept=".jpg,.png"  data-allowed-file-extensions="png jpg jpeg PNG JPG JPEG">
							    </div>
							    <div class="col-lg-3">
    								<label class="control-label">{{ _lang('Foto Produk 3') }} <small> format .png, .jpg</small></label>
    								<input type="file" class="form-control dropify" data-default-file="{{ $company->ft_prd3 != "" ? asset('public/uploads/foto_produk/'.$company->ft_prd3) : '' }}" name="ft_prd3" accept=".jpg,.png"  data-allowed-file-extensions="png jpg jpeg PNG JPG JPEG">
							    </div>
							    <div class="col-lg-3">
    								<label class="control-label">{{ _lang('Foto Produk 4') }} <small> format .png, .jpg</small></label>
    								<input type="file" class="form-control dropify" data-default-file="{{ $company->ft_prd4 != "" ? asset('public/uploads/foto_produk/'.$company->ft_prd4) : '' }}" name="ft_prd4" accept=".jpg,.png"  data-allowed-file-extensions="png jpg jpeg PNG JPG JPEG">
							    </div>
							</div>
							<div class="form-group">
								<label class="control-label">{{ _lang('Splash Screen') }}<span style="color:red;"> *</span> <small>(2732 X 2732) format .png, .jpg</small></label>
								<input type="file" class="form-control dropify" data-default-file="{{ $company->splash != "" ? asset('public/uploads/splash/'.$company->splash) : '' }}" name="splash" accept=".jpg,.png"  data-allowed-file-extensions="png jpg jpeg PNG JPG JPEG" required>
							</div>
							<div class="form-group">
								<label class="control-label">{{ _lang('Background Aplikasi') }}<span style="color:red;"> *</span> <small>Potrait : 1920 X 1080 , Landscape : 1080 X 1920 format .png, .jpg</small></label>
								<input type="file" class="form-control dropify" data-default-file="{{ $company->bg_apk != "" ? asset('public/uploads/bg_apk/'.$company->bg_apk) : '' }}" name="bg_apk" accept=".jpg,.png"  data-allowed-file-extensions="png jpg jpeg PNG JPG JPEG" required>
							</div>
							<hr>
							<div class="form-group justify-content-center" style="text-align: center;">
							    <label style="font-size:25px;">{{ _lang('Data Campaign') }}</label>
							</div>
							<div class="form-group">
								<label class="control-label">{{ _lang('Judul Campaign') }}</label>
								<input type="text" class="form-control" placeholder="Judul Campaign" name="judul_camp" value="{{$company->judul_camp}}" required>
							</div>
							<div class="form-group">
								<label class="control-label">{{ _lang('Sub Judul') }}</label>
								<input type="text" class="form-control" placeholder="Sub Judul" name="sub_judul" value="{{$company->sub_judul}}" required>
							</div>
							<div class="form-group">
								<label class="control-label">{{ _lang('Keyword Brand') }}</label>
								<input type="text" class="form-control" placeholder="Keyword Brand" name="key_brand" value="{{$company->key_brand}}" required>
							</div>
							<div class="form-group">
								<label class="control-label">{{ _lang('Deskripsi Campaign') }}</label>
								<input type="text" class="form-control" placeholder="Deskripsi Campaign" name="deskripsi_camp" value="{{$company->deskripsi_camp}}" required>
							</div>
							<div class="form-group">
								<label class="control-label">{{ _lang('Cover') }}<span style="color:red;"> *</span><small>(400 X 400) format .png, .jpg</small></label>
								<input type="file" class="form-control dropify" data-default-file="{{ $company->cover != "" ? asset('public/uploads/cover/'.$company->cover) : '' }}" name="cover" accept=".jpg,.png"  data-allowed-file-extensions="png jpg jpeg PNG JPG JPEG" required>
							</div>
							<div class="form-group">
								<label class="control-label">{{ _lang('Thumbnail') }}<span style="color:red;"> *</span><small>(400 X 400) format .png, .jpg</small></label>
								<input type="file" class="form-control dropify" data-default-file="{{ $company->thumbnail != "" ? asset('public/uploads/thumbnail/'.$company->thumbnail) : '' }}" name="thumbnail" accept=".jpg,.png"  data-allowed-file-extensions="png jpg jpeg PNG JPG JPEG" required>
							</div><div class="form-group">
								<label class="control-label">{{ _lang('Logo Campaign') }}<span style="color:red;"> *</span><small>(512 X 512) format .png</small></label>
								<input type="file" class="form-control dropify" data-default-file="{{ $company->logo_camp != "" ? asset('public/uploads/logo_camp/'.$company->logo_camp) : '' }}" name="logo_camp" accept=".png"  data-allowed-file-extensions="png PNG" required>
							</div><div class="form-group">
								<label class="control-label">{{ _lang('Gambar Campaign') }}<span style="color:red;"> *</span><small> format .png, .jpg</small></label>
								<input type="file" class="form-control dropify" data-default-file="{{ $company->gambar_camp != "" ? asset('public/uploads/gambar_camp/'.$company->gambar_camp) : '' }}" name="gambar_camp" accept=".jpg,.png"  data-allowed-file-extensions="png jpg jpeg PNG JPG JPEG" required>
							</div>
							
							<div class="form-group">
								<button type="submit" class="btn btn-primary">{{ _lang('Save') }}</button>
							</div>
						</form>
					</div>	


				</div>	
			</div>
		</div>
	</div>
</div>
@endsection

