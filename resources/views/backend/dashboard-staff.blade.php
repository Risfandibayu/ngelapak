@extends('layouts.app')

@section('content')



<!--Start Card-->
<div class="row">
	<div class="col-md-3 mb-3">
		<div class="card">
			<div class="seo-fact sbg1">
				<div class="p-4">
					<div class="seofct-icon">
						<span>{{ _lang('Total Project') }}</span>
					</div>
					
				</div>
			</div>
		</div>
	</div>
	
	<div class="col-md-3 mb-3">
		<div class="card">
			<div class="seo-fact sbg2">
				<div class="p-4">
					<div class="seofct-icon">
						<span>{{ _lang('Not Started Project') }}</span>
					</div>
					
				</div>
			</div>
		</div>
	</div>

	<div class="col-md-3 mb-3">
		<div class="card">
			<div class="seo-fact sbg4">
				<div class="p-4">
					<div class="seofct-icon">
						<span>{{ _lang('Project In Progress') }}</span>
					</div>
					
				</div>
			</div>
		</div>
	</div>
	
	<div class="col-md-3 mb-3">
		<div class="card">
			<div class="seo-fact sbg2">
				<div class="p-4">
					<div class="seofct-icon">
						<span>{{ _lang('Completed Project') }}</span>
					</div>
					
				</div>
			</div>
		</div>
	</div>

</div><!--end row-->
<!--End Card-->




@endsection
