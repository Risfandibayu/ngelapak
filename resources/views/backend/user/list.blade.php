@extends('layouts.app')

@section('js-script')
<script>
    
    table = $('#tabel').DataTable( {
        responsive: true,
        destroy: true,
        "sdom": '<"top"i>rt<"bottom"flp><"clear">'
    } );

</script>
@endsection
@section('content')

<div class="row">
	<div class="col-12">
	    <a class="btn btn-primary btn-xs ajax-modal" data-title="{{ _lang('Add User') }}" href="{{ route('users.create') }}"><i class="ti-plus"></i> {{ _lang('Add New') }}</a>
		<div class="card mt-2"> 
			<div class="card-body">
				<h4 class="mt-0 header-title d-none panel-title">{{ $title }}</h4>
				
			<form action="{{ action('UserController@index') }}" method="get">
            <div class="row">
                
            
                <div class="form-group col-lg-3">
                    <label><strong>Membership Type  </strong></label>
                    <select id='membership_type' name='membership_type' class="form-control">
                        <option value="" hidden>Select Membership Type</option>
                        <option value="member">Member</option>
                        <option value="trial">Trial</option>
                        <option value="All">All</option>
                    </select>
                </div>
                <div class="form-group col-lg-3">
                    <label><strong>Package Type </strong></label>
                    <select id='package_type' name='package_type' class="form-control">
                        <option value="" hidden>Select Package Type</option>
                        <option value="basic">Basic</option>
                        <option value="pro">Pro</option>
                        <option value="advanced">Advanced</option>
                    </select>
                </div>
                
                <div class="form-group  col-lg-4">
                    <label><strong>Action </strong></label><br>
                    	
                    <button class="btn btn-success" type="submit" style="font-size:14px;">{{ _lang('Filter') }}</button>
                    <a href="{{url('users/type/user')}}" class="btn btn-primary" style="font-size:14px;">{{ _lang('Reset') }}</a>
                </div>
            </div>
            </form>
        
				<table class="table table-bordered data-table" id='tabel'>
					<thead style="background-color:#1c2c70;">
					  <tr>
						<th class="text-center" style="font-weight:bold;text-transform: uppercase;color:white;">{{ _lang('ID') }}</th>
						<!--<th class="text-center">{{ _lang('Avatar') }}</th>-->
						<th class="text-center" style="font-weight:bold;text-transform: uppercase;color:white">{{ _lang('Nama Usaha') }}</th>
						<th class="text-center" style="font-weight:bold;text-transform: uppercase;color:white">{{ _lang('Nama') }}</th>
						<!--<th>{{ _lang('Email') }}</th>-->
						<th class="text-center" style="font-weight:bold;text-transform: uppercase;color:white" width="12%">{{ _lang('Tanggal Daftar') }}</th>
						<!--<th>{{ _lang('Proyek ID') }}</th>-->
						
						<!--<th>{{ _lang('Package') }}</th>-->
						<th class="text-center" style="font-weight:bold;text-transform: uppercase;color:white">{{ _lang('Membership') }}</th>
						<th class="text-center" style="font-weight:bold;text-transform: uppercase;color:white">{{ _lang('Data Usaha') }}</th>
						<th class="text-center" style="font-weight:bold;text-transform: uppercase;color:white">{{ _lang('Progress') }}</th>
						<th class="text-center" style="font-weight:bold;text-transform: uppercase;color:white" width="25%">{{ _lang('Action') }}</th>
					  </tr>
					</thead>
					<tbody>
					  
					  @foreach($users as $user)
						<tr id="row_{{ $user->id }}">
							<td class='id'>{{ $user->id }}</td>
							<!--<td class="text-center">-->
							<!--	<img src="{{ asset('public/uploads/profile/'.$user->profile_picture) }}" class="thumb-sm rounded-circle mr-2">-->
							<!--</td>-->
							<td class='name'>{{ $user->company->business_name }}</td>
							<td class='email'>{{ $user->name }}</td>		
							<!--<td class='email'>{{ $user->email}}</td>		-->
							<td class='created_at'>{{ $user->created_at}}</td>		
							<!--<td class='email'>{{ $user->proid}}</td>		-->
							
							<!--<td class='package_id'>{{ $user->company->package->package_name }}({{ ucwords($user->company->package_type) }})</td>-->
							<td class='membership_type text-center'>
							 
							         {!! $user->company->membership_type == 'trial' ? clean(status(ucwords($user->company->membership_type), 'danger')) : clean(status(ucwords($user->company->membership_type), 'success')) !!}
							
							     
							         	<!--{{$user->company->package_id}}-->
							         	<br>
							@if($user->company->package_id == 1)
							<span class="badge" style="background-color:#8ac4d0;color:#1c2c70;">Basic</span>
							@elseif($user->company->package_id == 2)
							<span class="badge" style="background-color:#3e83a8;color:white;">Pro</span>
							@elseif($user->company->package_id == 3)
							<span class="badge" style="background-color:#1c2c70;color:white;">Advanced</span>
							@endif
							     
							 
						
							
							
							</td>					
							<!--<td class='status'>{!! $user->company->status == 1 ? clean(status(_lang('Active'), 'success')) : clean(status(_lang('In-Active'), 'danger')) !!}</td>					-->
							<td class='status'>{!! $user->company->alamat != null ? clean(status(_lang('Sudah Lengkap'), 'success')) : clean(status(_lang('Belum Lengkap'), 'danger')) !!}</td>
							<td class='email'>
							   <!--{{ $user->company->progress }}-->
							 @if($user->company->progress == null || $user->company->progress == 'To Do')
							    @if($user->company->alamat != null)
							    <!--<span class="badge badge-warning">To Do</span>-->
							    <a href="{{ action('UserController@edit_progress', $user['id']) }}" data-title="{{ _lang('Update User') }}" class="ajax-modal"><span class="badge badge-warning" style="color:white;">To Do</span></a>
							    @else
							    <!--{!! clean(status(_lang('Waiting'), 'danger')) !!}-->
							    <a href="{{ action('UserController@edit_progress', $user['id']) }}" data-title="{{ _lang('Update User') }}" class="ajax-modal">{!! clean(status(_lang('Waiting'), 'danger')) !!}</a>
							    @endif
							 @elseif($user->company->progress == 'In Progress')
							 <!--<span class="badge badge-warning">In Progress</span>-->
							 <a href="{{ action('UserController@edit_progress', $user['id']) }}" data-title="{{ _lang('Update User') }}" class="ajax-modal"><span class="badge badge-warning">In Progress</span></a>
							 @elseif($user->company->progress == 'Waiting')
							 
							 <a href="{{ action('UserController@edit_progress', $user['id']) }}" data-title="{{ _lang('Update User') }}" class="ajax-modal">{!! clean(status(_lang('Waiting'), 'danger')) !!}</a>
							 @elseif($user->company->progress == 'Done')
							 <!--{!! clean(status(_lang('Done'), 'success')) !!}-->
							  <a href="{{ action('UserController@edit_progress', $user['id']) }}" data-title="{{ _lang('Update User') }}" class="ajax-modal">{!! clean(status(_lang('Done'), 'success')) !!}</a>
							 @endif
							 </td>
							
							<td class="text-center">
							  <form action="{{ action('UserController@destroy', $user['id']) }}" method="post">
								<a href="{{ action('UserController@edit', $user['id']) }}" data-title="{{ _lang('Update User') }}" class="btn btn-outline-warning btn-xs ajax-modal" style="font-size:10px;">{{ _lang('Edit') }}</a>
								
								<a href="{{ action('UserController@show', $user['id']) }}" data-title="{{ _lang('View User') }}" class="btn btn-outline-primary btn-xs ajax-modal" style="font-size:10px;">{{ _lang('View') }}</a>
								{{ csrf_field() }}
								<input name="_method" type="hidden" value="DELETE">
								<button class="btn btn-outline-danger btn-xs btn-remove" type="submit" style="font-size:10px;">{{ _lang('Delete') }}</button>
							  </form>
							</td>
						</tr>
					  @endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

@endsection


