<div class="card panel-default">
<div class="card-body">
    @php $date_format = get_option('date_format','Y-m-d'); @endphp
	
    <table class="table table-bordered">
		<tr><td colspan="2" class="text-center"><img class="thumb-xl rounded" src="{{ $user->profile_picture != "" ? asset('public/uploads/profile/'.$user->profile_picture) : asset('public/images/avatar.png') }}"></td></tr>
		<!--<tr><td>{{ _lang('Business Name') }}</td><td>{{ $user->company->business_name }}</td></tr>-->
		<tr><td>{{ _lang('Admin Name') }}</td><td>{{ $user->name }}</td></tr>
		<tr><td>{{ _lang('Admin Email') }}</td><td>{{ $user->email }}</td></tr>
		<tr><td>{{ _lang('Nomor Telepon') }}</td><td>{{ $user->no_hp }}</td></tr>
		
		
		<?php 
		$project = db::table('projects')
		->where('user_id',$user->id)
		->first();
		
		?>
		@if(empty($project))
		<tr><td>{{ _lang('Proyek') }}</td><td> <i><small style="color: #afacac;">!User belum membuat proyek</small></i> </td></tr>
		@else
		<tr><td>{{ _lang('Proyek') }}</td><td>{{ $project->file }}
		<!--<a href="'ProjectController@edit', $project->id).'" data-title="'. _lang('Update Project') .'" class="btn btn-warning btn-xs"><i class="ti-pencil"></i> Edit</a>-->
		<!--	<a href="{{ action('ProjectController@edit', $project->id) }}" data-title="{{ _lang('Update Project') }}" class="btn btn-outline-warning btn-xs " style="font-size:10px;">{{ _lang('Edit') }}</a>-->
		<!--</td></tr>-->
		<a href="{{asset('public/uploads/file_project/'.$project->file)}}" class="btn btn-outline-success btn-xs" style="font-size:10px;float: right;padding: 3px 9px 3px 9px;"  download>Download Proyek</a>
		@endif
		
		<tr><td>{{ _lang('Status') }}</td><td>{!! $user->company->status == 1 ? clean(status(_lang('Active'), 'success')) : clean(status(_lang('In-Active'), 'danger')) !!}</td></tr>
		@if($user->user_type == 'user')
			<tr><td>{{ _lang('Package') }}</td><td>{{ $user->company->package->package_name }}({{ ucwords($user->company->package_type) }})</td></tr>	
			<tr><td>{{ _lang('Package Valid To') }}</td><td>{{ date($date_format, strtotime($user->company->valid_to)) }}</td></tr>	
	        <tr>
	        	<td>{{ _lang('Membersip Type') }}</td><td>{!! $user->company->membership_type == 'trial' ? clean(status(ucwords($user->company->membership_type), 'danger')) : clean(status(ucwords($user->company->membership_type), 'success')) !!}</td>
	        </tr>
		@endif
    </table>

    <!--@if($user->user_type == 'user')-->
	   <!-- <table class="table table-striped">-->
	   <!-- 	<tr>-->
	   <!-- 		<td colspan="2" class="text-center"><b>{{ _lang('Package Details') }}</b></td>-->
	   <!-- 	</tr>-->
	   <!-- 	<tr>-->
	   <!-- 		<td><b>{{ _lang('Feature') }}</b></td>-->
	   <!-- 		<td class="text-center"><b>{{ _lang('Avaialble Limit') }}</b></td>-->
	   <!-- 	</tr>-->
	   <!-- 	<tr>-->
	   <!-- 		<td>{{ _lang('Websites Limit') }}</td>-->
	   <!-- 		<td class="text-center">{{ $user->company->websites_limit }}</td>-->
	   <!-- 	</tr>-->
	   <!-- 	<tr>-->
	   <!-- 		<td>{{ _lang('Recurring Transaction') }}</td>-->
	   <!-- 		<td class="text-center">{{ ucwords($user->company->recurring_transaction) }}</td>-->
	   <!-- 	</tr>-->
	   <!-- 	<tr>-->
	   <!-- 		<td>{{ _lang('Online Payment') }}</td>-->
	   <!-- 		<td class="text-center">{{ ucwords($user->company->online_payment) }}</td>-->
	   <!-- 	</tr>	    	-->
	   <!-- 	<tr>-->
	   <!-- 		<td><a href="{{asset('public/uploads/logo_usaha/'.$user->company->logo)}}" download><img class="img-fluid rounded" src="{{ $user->company->logo != "" ? asset('public/uploads/logo_usaha/'.$user->company->logo) : asset('public/images/avatar.png') }}"></a></td>-->
	    		<!--<td><a><img class="thumb-xl rounded" src="{{ $user->profile_picture != "" ? asset('public/uploads/profile/'.$user->profile_picture) : asset('public/images/avatar.png') }}"></a></td>-->
	   <!-- 	</tr>-->
	   <!-- </table>-->
    <!--@endif-->
    
     <table class="table table-striped">
          	<tr>
	    		<td colspan="2" class="text-center"><b>{{ _lang('Data Usaha') }}</b></td>
	    	</tr>
	    	<tr>
	    		<td><b>{{ _lang('Nama Usaha') }}</b></td>
	    		<td class="text-center">{{ $user->company->business_name }}</td>
	    	</tr>
	    	<!--<tr>-->
	    	<!--	<td><b>{{ _lang('Nomor Telepon') }}</b></td>-->
	    	<!--	<td class="text-center">{{ $user->no_hp }}</td>-->
	    	<!--</tr>-->
	    	<tr>
	    		<td><b>{{ _lang('Alamat') }}</b></td>
	    		<td class="text-center">
	    		    @if($user->company->alamat != null)
	    		    {{ $user->company->alamat}}, Kota {{$user->company->kota}}, Prov. {{$user->company->prov}}, {{$user->company->kode_pos }}
	    		    @else
	    		    
	    		    @endif
	    		    
	    		    </td>
	    	</tr>
	    	<tr>
	    		<td><b>{{ _lang('Deskripsi') }}</b></td>
	    		<td class="text-center">{{ $user->company->deskripsi }}</td>
	    	</tr>
	    	<tr>
	    		<td><b>{{ _lang('Slogan/Tagline') }}</b></td>
	    		<td class="text-center">{{ $user->company->slogan }}</td>
	    	</tr>
	    	<tr>
	    		<td><b>{{ _lang('Instagram') }}</b></td>
	    		<td class="text-center">{{ $user->company->ig }}</td>
	    	</tr>
	    	<tr>
	    		<td><b>{{ _lang('Facebook') }}</b></td>
	    		<td class="text-center">{{ $user->company->fb }}</td>
	    	</tr>
	    	<tr>
	    		<td><b>{{ _lang('Website') }}</b></td>
	    		<td class="text-center">{{ $user->company->web }}</td>
	    	</tr>
	    	
	    </table>
	    
	    <table class="table table-striped">
          	<tr>
	    		<td colspan="2" class="text-center"><b>{{ _lang('Data Konten') }} <br><small>( klik gambar untuk download )</small</b></td>
	    	</tr>
	    	<!--<tr>-->
	    	<!--	<td colspan="2" class="text-center"><small>klik gambar untuk download</small></td>-->
	    	<!--</tr>-->
	        <tr>
	    	    <td>Logo Usaha</td>
	    		<td><a href="{{asset('public/uploads/logo_usaha/'.$user->company->logo)}}" download><img class="img-fluid rounded" src="{{ $user->company->logo != "" ? asset('public/uploads/logo_usaha/'.$user->company->logo) : asset('public/images/avatar.png') }}"></a></td>
	    		<!--<td><a><img class="thumb-xl rounded" src="{{ $user->profile_picture != "" ? asset('public/uploads/profile/'.$user->profile_picture) : asset('public/images/avatar.png') }}"></a></td>-->
	    	</tr>
	    	<tr>
	    	    <td>Foto Produk 1</td>
	    		<td><a href="{{asset('public/uploads/foto_produk/'.$user->company->ft_prd1)}}" download><img class="img-fluid rounded" src="{{ $user->company->ft_prd1 != "" ? asset('public/uploads/foto_produk/'.$user->company->ft_prd1) : asset('public/images/avatar.png') }}"></a></td>
	    		<!--<td><a><img class="thumb-xl rounded" src="{{ $user->profile_picture != "" ? asset('public/uploads/profile/'.$user->profile_picture) : asset('public/images/avatar.png') }}"></a></td>-->
	    	</tr>
	    	<tr>
	    	    <td>Foto Produk 2</td>
	    		<td><a href="{{asset('public/uploads/foto_produk/'.$user->company->ft_prd2)}}" download><img class="img-fluid rounded" src="{{ $user->company->ft_prd2 != "" ? asset('public/uploads/foto_produk/'.$user->company->ft_prd2) : asset('public/images/avatar.png') }}"></a></td>
	    		<!--<td><a><img class="thumb-xl rounded" src="{{ $user->profile_picture != "" ? asset('public/uploads/profile/'.$user->profile_picture) : asset('public/images/avatar.png') }}"></a></td>-->
	    	</tr>
	    	<tr>
	    	    <td>Foto Produk 3</td>
	    		<td><a href="{{asset('public/uploads/foto_produk/'.$user->company->ft_prd3)}}" download><img class="img-fluid rounded" src="{{ $user->company->ft_prd3 != "" ? asset('public/uploads/foto_produk/'.$user->company->ft_prd3) : asset('public/images/avatar.png') }}"></a></td>
	    		<!--<td><a><img class="thumb-xl rounded" src="{{ $user->profile_picture != "" ? asset('public/uploads/profile/'.$user->profile_picture) : asset('public/images/avatar.png') }}"></a></td>-->
	    	</tr>
	    	<tr>
	    	    <td>Foto Produk 4</td>
	    		<td><a href="{{asset('public/uploads/foto_produk/'.$user->company->ft_prd4)}}" download><img class="img-fluid rounded" src="{{ $user->company->ft_prd4 != "" ? asset('public/uploads/foto_produk/'.$user->company->ft_prd4) : asset('public/images/avatar.png') }}"></a></td>
	    		<!--<td><a><img class="thumb-xl rounded" src="{{ $user->profile_picture != "" ? asset('public/uploads/profile/'.$user->profile_picture) : asset('public/images/avatar.png') }}"></a></td>-->
	    	</tr>
	        <tr>
	    	    <td>Splash Screen</td>
	    		<td><a href="{{asset('public/uploads/splash/'.$user->company->splash)}}" download><img class="img-fluid rounded" src="{{ $user->company->splash != "" ? asset('public/uploads/splash/'.$user->company->splash) : asset('public/images/avatar.png') }}"></a></td>
	    		<!--<td><a><img class="thumb-xl rounded" src="{{ $user->profile_picture != "" ? asset('public/uploads/profile/'.$user->profile_picture) : asset('public/images/avatar.png') }}"></a></td>-->
	    	</tr>
	    	<tr>
	    	    <td>Background Aplikasi</td>
	    		<td><a href="{{asset('public/uploads/bg_apk/'.$user->company->bg_apk)}}" download><img class="img-fluid rounded" src="{{ $user->company->bg_apk != "" ? asset('public/uploads/bg_apk/'.$user->company->bg_apk) : asset('public/images/avatar.png') }}"></a></td>
	    		<!--<td><a><img class="thumb-xl rounded" src="{{ $user->profile_picture != "" ? asset('public/uploads/profile/'.$user->profile_picture) : asset('public/images/avatar.png') }}"></a></td>-->
	    	</tr>
	    </table>
	    
	    <table class="table table-striped">
	        <tr>
	    		<td colspan="2" class="text-center"><b>{{ _lang('Data Campaign') }} <br><small>( klik gambar untuk download )</small</b></td>
	    	</tr>
	    	<tr>
	    		<td>Judul Campaign</td>
	    		<td class="text-center">{{ $user->company->judul_camp }}</td>
	    	</tr>
	    	<tr>
	    		<td>Sub Judul</td>
	    		<td class="text-center">{{ $user->company->sub_judul }}</td>
	    	</tr>
	    	<tr>
	    		<td>Keyword Brand</td>
	    		<td class="text-center">{{ $user->company->key_brand }}</td>
	    	</tr>
	    	<tr>
	    		<td>Keyword Brand</td>
	    		<td class="text-center">{{ $user->company->deskripsi_camp }}</td>
	    	</tr>
	    	<tr>
	    	    <td>Cover</td>
	    		<td><a href="{{asset('public/uploads/cover/'.$user->company->cover)}}" download><img class="img-fluid rounded" src="{{ $user->company->cover != "" ? asset('public/uploads/cover/'.$user->company->cover) : asset('public/images/avatar.png') }}"></a></td>
	    		<!--<td><a><img class="thumb-xl rounded" src="{{ $user->profile_picture != "" ? asset('public/uploads/profile/'.$user->profile_picture) : asset('public/images/avatar.png') }}"></a></td>-->
	    	</tr>
	    	<tr>
	    	    <td>Thumbnail</td>
	    		<td><a href="{{asset('public/uploads/thumbnail/'.$user->company->thumbnail)}}" download><img class="img-fluid rounded" src="{{ $user->company->thumbnail != "" ? asset('public/uploads/thumbnail/'.$user->company->thumbnail) : asset('public/images/avatar.png') }}"></a></td>
	    		<!--<td><a><img class="thumb-xl rounded" src="{{ $user->profile_picture != "" ? asset('public/uploads/profile/'.$user->profile_picture) : asset('public/images/avatar.png') }}"></a></td>-->
	    	</tr>
	    	<tr>
	    	    <td>Logo Campaign</td>
	    		<td><a href="{{asset('public/uploads/logo_camp/'.$user->company->logo_camp)}}" download><img class="img-fluid rounded" src="{{ $user->company->logo_camp != "" ? asset('public/uploads/logo_camp/'.$user->company->logo_camp) : asset('public/images/avatar.png') }}"></a></td>
	    		<!--<td><a><img class="thumb-xl rounded" src="{{ $user->profile_picture != "" ? asset('public/uploads/profile/'.$user->profile_picture) : asset('public/images/avatar.png') }}"></a></td>-->
	    	</tr>
	    	<tr>
	    	    <td>Gambar Campaign</td>
	    		<td><a href="{{asset('public/uploads/gambar_camp/'.$user->company->gambar_camp)}}" download><img class="img-fluid rounded" src="{{ $user->company->gambar_camp != "" ? asset('public/uploads/gambar_camp/'.$user->company->gambar_camp) : asset('public/images/avatar.png') }}"></a></td>
	    		<!--<td><a><img class="thumb-xl rounded" src="{{ $user->profile_picture != "" ? asset('public/uploads/profile/'.$user->profile_picture) : asset('public/images/avatar.png') }}"></a></td>-->
	    	</tr>
	    	
	    </table>
</div>
</div>
