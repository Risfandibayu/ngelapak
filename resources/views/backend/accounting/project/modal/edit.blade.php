<style>
#main_modal .modal-lg {
    max-width: 800px;
}

#main_modal .modal-body {
    overflow: visible !important;
}
.reveal-if-active {
	 opacity: 0;
	 max-height: 0;
	 overflow: hidden;
	 font-size: 16px;
	 transform: scale(0.8);
	 transition: 0.5s;
}
 .reveal-if-active label {
	 display: block;
	 margin: 0 0 3px 0;
}
 .reveal-if-active input[type=text] {
	 width: 100%;
}
 input[type="radio"]:checked ~ .reveal-if-active, input[type="checkbox"]:checked ~ .reveal-if-active {
	 opacity: 1;
	 max-height: 200px;
	 padding: 10px 20px;
	 transform: scale(1);
	 overflow: visible;
}

</style>
@section('js-script')


<script>
  var FormStuff = {
  
  init: function() {
    this.applyConditionalRequired();
    this.bindUIActions();
  },
  
  bindUIActions: function() {
    $("input[type='radio'], input[type='checkbox']").on("change", this.applyConditionalRequired);
  },
  
  applyConditionalRequired: function() {
    
    $(".require-if-active").each(function() {
      var el = $(this);
      if ($(el.data("require-pair")).is(":checked")) {
        el.prop("required", true);
      } else {
        el.prop("required", false);
      }
    });
    
  }
  
};

FormStuff.init();
</script>
@endsection
<form method="post" class="ajax-submit" autocomplete="off" action="{{ action('ProjectController@update', $id) }}" enctype="multipart/form-data">
	{{ csrf_field()}}
	<input name="_method" type="hidden" value="PATCH">				
	
	<div class="row">
		<div class="col-md-12">
			<div class="form-group">
			   <label class="control-label">{{ _lang('Nama Proyek') }}</label>						
			   <input type="text" class="form-control" name="name" value="{{ $project->name }}" required>
			</div>
		</div>

		<div class="col-md-12">
			<div class="form-group">
				<label class="control-label">{{ _lang('Subdomain') }}</label>
				<div class="input-group mb-3">
					<input type="text" class="form-control" name="custom_domain" value="{{ $project->custom_fields }}" @if(env('DEMO_MODE') == true) disabled @endif>
					<div class="input-group-append">
						<!--<span class="input-group-text" id="basic-addon2">.{{ config('app.short_url') }}</span>-->
						<span class="input-group-text" id="basic-addon2">.ngelapak.co.id</span>
					</div>
				</div>
        		@if(env('DEMO_MODE') == true)
					<span class="required">{{ _lang("UNFORTUNATELY IT'S NOT ALLOWED AT DEMO MODE!")}}</span>
				@endif
			</div>
		</div>
		<div class="col-md-12">
		  <div class="form-group">
			<label class="control-label">{{ _lang('Progress Proyek') }}</label>						
			<select class="form-control " id="ket" name="ket" >
			    
			  	
			  	<option value="Belum Selesai" {{ $project->ket == 'Belum Selesai' || $project->ket == null ? 'selected' : '' }}>{{ _lang('Belum Selesai') }}</option>
			  	<option value="Selesai" {{ $project->ket == 'Selesai' ? 'selected' : '' }}>{{ _lang('Selesai') }}</option>
			</select>
		  </div>
        </div>
        <div class="col-md-12">
		  <div class="form-group">
			<label class="control-label" for="exampleFormControlFile1">{{ _lang('File Proyek') }}</label>						
			<input type="file" name="file" data-default-file="{{ $project->file != "" ? asset('public/uploads/file_project/'.$project->file) : '' }}"  class="form-control dropify" id="exampleFormControlFile1" value="{{$project->file}}" accept=".zip,.rar" data-allowed-file-extensions="zip rar ZIP RAR ">
			<!--<label class="custom-file-label" for="exampleFormControlFile1">Choose file</label>-->
			
		  </div>
		 
        </div>
        <!--<div class="col-md-12">-->
        <!--                      <div class="form-group">-->
        <!--                      <label class="control-label">{{ _lang('Progress Proyek') }}</label>	-->
        <!--                      <div class="form-check">-->
								<!--	<input class="form-check-input " type="radio" name="ket" id="belum" value="Belum Selesai" required-->
								<!--	{{ $project->ket == 'Belum Selesai' || $project->ket == null ? 'checked' : '' }}-->
								<!--	>-->
								<!--	<label class="form-check-label " for="belum">-->
								<!--	  Belum Selesai-->
								<!--	</label>-->
								<!--  </div>-->
								<!--  <div class="form-check">-->
								<!--	<input class="form-check-input" type="radio" name="ket" id="selesai" value="Selesai"-->
								<!--	{{ $project->ket == 'Selesai' ? 'checked' : '' }}-->
								<!--	>-->
								<!--	<label class="form-check-label " for="selesai">-->
								<!--	  Selesai-->
								<!--	</label>-->
								<!--	<div class="reveal-if-active">-->
                                     
                                    
        <!--    							<div class="form-group">-->
        <!--    								<label class="control-label">{{ _lang('File Proyek') }} </label>-->
        <!--    								<input data-require-pair="#selesai" type="file" class="require-if-active form-control " data-default-file="" name="logo" accept=".zip .rar"  data-allowed-file-extensions="zip ZIP rar RAR" >-->
        <!--    							</div>-->
            							
							            
        <!--                            </div>-->
								<!--  </div>-->
								<!--</div>-->
        <!--                    </div>                  -->

		<div class="col-md-12">
			<div class="form-group">
			   <label class="control-label">{{ _lang('Deskripsi Proyek') }}</label>						
			   <textarea class="form-control summernote" name="description">{{ $project->description }}</textarea>
			</div>
		</div>
		
		<div class="form-group">
		    <div class="col-md-12">
			    <button type="submit" class="btn btn-primary">{{ _lang('Simpan') }}</button>
		    </div>
		</div>
	</div>
</form>
