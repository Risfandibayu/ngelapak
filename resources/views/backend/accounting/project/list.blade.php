@extends('layouts.app')

@section('content')

<div class="row">
	<div class="col-lg-12">

		<div class="card mt-2">
			<span class="panel-title d-none">{{ _lang('Daftar Proyek') }}</span>		
			<div class="card-body">


				<table id="projects_table" class="table table-bordered">
					<thead>
					    <tr>
							<th>{{ _lang('Nama') }}</th>
							<th class="text-center">{{ _lang('Aksi') }}</th>
					    </tr>
					</thead>
					<tbody>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

@endsection

@section('js-script')
<script src="{{ asset('public/backend/assets/js/ajax-datatable/projects.js') }}"></script>
<script>
          var FormStuff = {
  
  init: function() {
    this.applyConditionalRequired();
    this.bindUIActions();
  },
  
  bindUIActions: function() {
    $("input[type='radio'], input[type='checkbox']").on("change", this.applyConditionalRequired);
  },
  
  applyConditionalRequired: function() {
    
    $(".require-if-active").each(function() {
      var el = $(this);
      if ($(el.data("require-pair")).is(":checked")) {
        el.prop("required", true);
      } else {
        el.prop("required", false);
      }
    });
    
  }
  
};

FormStuff.init();
</script>
@endsection

@section('style')
<style>
.reveal-if-active {
	 opacity: 0;
	 max-height: 0;
	 overflow: hidden;
	 font-size: 16px;
	 transform: scale(0.8);
	 transition: 0.5s;
}
 .reveal-if-active label {
	 display: block;
	 margin: 0 0 3px 0;
}
 .reveal-if-active input[type=text] {
	 width: 100%;
}
 input[type="radio"]:checked ~ .reveal-if-active, input[type="checkbox"]:checked ~ .reveal-if-active {
	 opacity: 1;
	 max-height: 2000px;
	 padding: 10px 20px;
	 transform: scale(1);
	 overflow: visible;
}

    </style>
@endsection