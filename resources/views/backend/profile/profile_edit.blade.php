@extends('layouts.app')
@section('js-script')


<script type="text/javascript">
    var toHtml = (tag, value) => {
        $(tag).html(value);
    }
    $(document).ready(function() {
        //  $('#province_id').select2();
        //  $('#cities_id').select2();
        $('#province_id').on('change', function() {
            var id = $('#province_id').val();
            var url = window.location.href;
            var urlNya = url.substring(0, url.lastIndexOf('/profile/edit/'));
            $.ajax({
                type: 'GET',
                url: urlNya + '/getcity/' + id,
                dataType: 'json',
                success: function(data) {
                    var op = '<option hidden value="">Pilih Kota</option>';
                    if (data.length > 0) {
                        var i = 0;
                        for (i = 0; i < data.length; i++) {
                            op += `<option value="${data[i].city_id}">${data[i].title}</option>`
                        }
                    }
                    toHtml('[name="kota"]', op);
                }
            })
        })
    });
</script>
<script type="text/javascript">
    var toHtml = (tag, value) => {
        $(tag).html(value);
    }
    $(document).ready(function() {
        //  $('#province_id').select2();
        //  $('#cities_id').select2();
        $('#cities_id').on('change', function() {
            var id = $('#cities_id').val();
            var url = window.location.href;
            var urlNya = url.substring(0, url.lastIndexOf('/profile/edit/'));
            $.ajax({
                type: 'GET',
                url: urlNya + '/getsubdistrict/' + id,
                dataType: 'json',
                success: function(data) {
                    var op = '<option hidden value="">Pilih Kecamatan</option>';
                    if (data.length > 0) {
                        var i = 0;
                        for (i = 0; i < data.length; i++) {
                            op += `<option value="${data[i].subdistrict_id}">${data[i].subdistrict_name}</option>`
                        }
                    }
                    toHtml('[name="kec"]', op);
                }
            })
        })
    });
</script>
@endsection
@section('content')
<div class="row">
	<div class="col-12">
		<div class="card">
		    <h5 class="card-header bg-primary text-white mt-0 panel-title">{{ _lang('Pengaturan Profil') }}</h5>
			<div class="card-body">
				<div class="row">
					<div class="col-md-6">
						<form action="{{ url('profile/update')}}" autocomplete="off" class="form-horizontal form-groups-bordered validate" enctype="multipart/form-data" method="post">
							@csrf
							<div class="form-group">
								<label class="control-label">{{ _lang('Nama') }}</label>
								<input type="text" class="form-control" name="name" value="{{$profile->name}}" required>
							</div>
							<div class="form-group">
								<label class="control-label">{{ _lang('Nama Lengkap') }}</label>
								<input type="text" class="form-control" name="nama_lengkap" value="{{$profile->nama_lengkap}}" required>
							</div>
							<div class="form-group">
								<label class="control-label">{{ _lang('NIK') }}</label>
								<input type="text" class="form-control" name="nik" value="{{$profile->nik}}" required>
							</div>

							<div class="form-group">
								<label class="control-label">{{ _lang('Email') }}</label>
								<input type="text" class="form-control" name="email" value="{{ $profile->email }}" required>
							</div>
							<div class="form-group">
								<label class="control-label">{{ _lang('Jenis Kelamin') }}</label>
									<select class="form-control" name="jk" aria-label="Default select example" required>
										<option hidden disabled selected>Pilih Jenis Kelamin</option>
										<option hidden 
                                    <?php 
                                    if($profile->jk == 'Laki-laki' || 'Perempuan')
                                    { 
                                        echo 'selected';
                                    }elseif ($profile->jk == Null || ''){
                                        ' ';
                                    } 
                                    
                                    ?> value="{{$profile->jk}}">
                                    
                                    
                                    @if ($profile->jk == Null || '')
                                        Pilih Jenis Kelamin
                                    @else
                                    {{$profile->jk}}
                                    @endif
                                    </option>
										<option value="Laki-Laki">Laki-laki</option>
										<option value="Perempuan">Perempuan</option>
									</select>
							</div>
							<div class="form-group">
								<label class="control-label">{{ _lang('Tanggal Lahir') }}</label>
								<input type="date" class="form-control" name="tgl_lahir" placeholder="Masukan tanggal lahir" value="{{$profile->tgl_lahir}}" required>
							</div>
							<div class="form-group">
								<label class="control-label">{{ _lang('Nomor HP/WhatsApp') }}</label>
									<input type="number" class="form-control" name="no_hp" placeholder="Masukan nomor HP/WhatsApp" value="{{$profile->no_hp}}" required>
							</div>
							<div class="form-group" hidden>
								<label class="control-label">{{ _lang('Language') }}</label>
								<select class="form-control" name="language">
									<option value="">{{ _lang('-- Select One --') }}</option>
									{{ load_language( $profile->language ) }}
								</select>
							</div>
							<div class="form-group row">
								<div class="col-lg-12">
									<label class="control-label">{{ _lang('Alamat') }}</label>
								</div>
								{{-- <input type="textarea" class="form-control textarea-group" name="alamat" placeholder="Masukan alamat" value="{{$profile->alamat}}" required> --}}
								
								<div class="col-lg-6">
									<!--<input type="text" class="form-control" name="prov" placeholder="Provinsi" value="{{$profile->prov}}" required>-->
									<select name="prov" id="province_id" class="form-control" required>
                                        <option value="" hidden>-- Pilih Provinsi --</option>
                                        @foreach($province as $provinsi)
                                        <option value="{{ $provinsi->province_id }}" <?php if ($profile->prov == $provinsi->province_id) {
                                                        echo 'selected';
                                                      } ?>>{{ $provinsi->title }}</option>
                                        @endforeach
                                    </select>
								</div>
								<div class="col-lg-6">
									<select name="kota" id="cities_id" class="form-control" required>
                                            @foreach($city as $cit)
                                            <option value="{{ $cit->city_id }}" <?php if ($profile->kota == $cit->city_id) {
                                                            echo 'selected';
                                                          } ?>>{{ $cit->title }}</option>
                                            @endforeach
                                    </select>
								</div>
								<div class="col-lg-12">
									<select name="kec" id="subdistrict_id" class="form-control" required>
                                     @foreach($subdistrict as $sub)
                                            <option value="{{ $sub->subdistrict_id }}" <?php if ($profile->kec == $sub->subdistrict_id) {
                                                            echo 'selected';
                                                          } ?>>{{ $sub->subdistrict_name}}</option>
                                            @endforeach
                                </select>
								</div>
								
								<div class="col-lg-12">
									<input type="text" class="form-control" name="kode_pos" placeholder="Kode Pos" value="{{$profile->kode_pos}}" required>
								</div>
								<div class="col-lg-12">
									<input type="text" class="form-control" name="alamat" placeholder="Jalan, RT RW, No Rumah, Kelurahan" value="{{$profile->alamat}}" required>
								</div>
							</div>

							<div class="form-group">
								<label class="control-label">{{ _lang('Image') }} (300 X 300)</label>
								<input type="file" class="form-control dropify" data-default-file="{{ $profile->profile_picture != "" ? asset('public/uploads/profile/'.$profile->profile_picture) : '' }}" name="profile_picture" data-allowed-file-extensions="png jpg jpeg PNG JPG JPEG">
							</div>

							<div class="form-group">
								<button type="submit" class="btn btn-primary">{{ _lang('Update Profil') }}</button>
							</div>
						</form>
					</div>	

                    @if(Auth::user()->user_type == 'user')
	                    <div class="col-md-6">
	                    	 <table class="table table-striped table-bordered">
						    	<tr>
						    		<td colspan="2" class="text-center"><b>{{ _lang('Detail Paket ').$profile->company->package->package_name.' ' }}</b></td>
						    	</tr>
						    	<tr>
						    		<td><b>{{ _lang('Fitur') }}</b></td>
						    		<td class="text-center"><b>{{ _lang('Terdapat Batasan') }}</b></td>
						    	</tr>
						    	<tr>
						    		<td>{{ _lang('Batas Proyek') }}</td>
						    		<td class="text-center">{{ $profile->company->websites_limit }}</td>
						    	</tr>
						    	<tr>
						    		<td>{{ _lang('Transaksi Berulang') }}</td>
						    		<!--<td class="text-center">{{ $profile->company->recurring_transaction }}</td>-->
						    		@if( $profile->company->recurring_transaction == 'Yes')
                		    		<td class="text-center">Ya</td>
                		    		@else
                		    		<td class="text-center">Tidak</td>
                		    		@endif
						    	</tr>
						    	<tr>
						    		<td>{{ _lang('Pembayaran Online') }}</td>
						    		<!--<td class="text-center">{{ $profile->company->online_payment }}</td>-->
						    		@if( $profile->company->online_payment == 'Yes')
                		    		<td class="text-center">Ya</td>
                		    		@else
                		    		<td class="text-center">Tidak</td>
                		    		@endif
						    	</tr>
						    </table>
	                    </div>
                    @endif


				</div>	
			</div>
		</div>
	</div>
</div>
@endsection

