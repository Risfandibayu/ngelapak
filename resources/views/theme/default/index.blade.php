<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Ngelapak - Milyaran Aplikasi Untuk Indonesia</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="{{asset('public/assets2')}}/img/icon.png" rel="icon">
  <link href="{{asset('public/assets2')}}/img/icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <!--<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">-->
<link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
  <!-- Vendor CSS Files -->
  <link href="{{asset('public/assets2')}}/vendor/aos/aos.css" rel="stylesheet">
  <link href="{{asset('public/assets2')}}/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="{{asset('public/assets2')}}/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="{{asset('public/assets2')}}/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="{{asset('public/assets2')}}/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="{{asset('public/assets2')}}/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="{{asset('public/assets2')}}/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">
  
  <!-- Template Main CSS File -->
  <link href="{{asset('public/assets2')}}/css/style.css" rel="stylesheet">
  <!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css" integrity="sha512-+EoPw+Fiwh6eSeRK7zwIKG2MA8i3rV/DGa3tdttQGgWyatG/SkncT53KHQaS5Jh9MNOT3dmFL0FjTY08And/Cw==" crossorigin="anonymous" referrerpolicy="no-referrer" />-->

  <link href="{{asset('public/assets2')}}/css/magnific.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: Gp - v4.6.0
  * Template URL: https://bootstrapmade.com/gp-free-multipurpose-html-bootstrap-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
  <style>
      .float{
	position:fixed;
	width:60px;
	height:60px;
	bottom:85px;
	right:15px;
	background-color:#25D366;
	color:#FFF;
	border-radius:50px;
	text-align:center;
	box-shadow: 2px 2px 3px #999;
	
	/*position: fixed;*/
 /* visibility: hidden;*/
 /* opacity: 0;*/
 /* right: 15px;*/
 /* bottom: 15px;*/
 /* z-index: 996;*/
 /* background: #01b0f8;*/
 /* width: 40px;*/
 /* height: 40px;*/
 /* border-radius: 4px;*/
 /* transition: all 0.4s;*/
}

.float i {
  font-size: 28px;
  color: #fff;
  line-height: 0;
}

.my-float{
	
}

#headerPopup{
  width:75%;
  margin:0 auto;
}

#headerPopup iframe{
  width:100%;
  margin:0 auto;
}
  </style>
</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top ">
    <div class="container d-flex align-items-center justify-content-sm-between">

      <!-- <h1 class="logo me-auto me-lg-0" style="float: left;"><a href="index.html">Gp<span>.</span></a></h1> -->
      <!-- Uncomment below if you prefer to use an image logo -->
      <a href="/" class="logo me-auto me-lg-0"><img src="{{asset('public/assets2')}}/img/LOGO 1 WHITE PNJANG.png" alt="" class="img-fluid"></a>

      <nav id="navbar" class="navbar" style="font-family: 'Montserrat', sans-serif;">
        <ul>
          <li><a class="nav-link scrollto" href="#hero" style="font-weight: 400;">Daftar Sekarang</a></li>
          <li><a class="nav-link scrollto" href="#fitur" style="font-weight: 400;">Fitur</a></li>
          <li><a class="nav-link scrollto" href="#paket" style="font-weight: 400;">Pilihan Paket</a></li>
          <li><a class="nav-link scrollto " href="#klien" style="font-weight: 400;">Klien Kami</a></li>
          <!--<li><a class="nav-link scrollto" href="#footer">Kontak Kami</a></li>-->
          <li><a class="nav-link scrollto" href="/faq" style="font-weight: 400;">FAQ</a></li>
          <li class="dropdown"><a href="#" style="font-weight: 400;"><span>Akun</span> <i class="bi bi-chevron-down"></i></a>
            <ul>
              <!-- <li><a href="#">Masuk</a></li>
              <li><a href="#">Daftar</a></li> -->
              @if(! Auth::check())
              <li><a href="{{ url('login') }}">Login</a></li>
                @if(get_option('allow_singup','yes') == 'yes')
                <li><a href="{{ url('register/client_signup') }}" data-select="0">Sign Up</a></li>
                @endif
              @else
              <li><a href="{{ url('dashboard') }}">Dashboard</a></li>
              <li><a href="{{ url('logout') }}">Logout</a></li>
            </ul>
        @endif
            </ul>
          </li>
          
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav><!-- .navbar -->

      

    </div>
  </header><!-- End Header -->

  <!-- ======= Hero Section ======= -->
  <section id="hero" class="d-flex align-items-center justify-content-center">
    <div class="container" data-aos="fade-up">

      <div class="row justify-content-center" data-aos="fade-up" data-aos-delay="150">
        <div class="col-xl-6 col-lg-6" style="align-self: center;">
          <h1 style=" font-family: 'Montserrat', sans-serif;font-size: 25px; line-height: 30px;">Ngelapak - Milyaran Aplikasi Untuk Indonesia</h1>
          <h2 style=" font-family: 'Montserrat', sans-serif;font-size: 20px;    font-weight: 200;">Platform pengembang aplikasi pertama di Indonesia yang memberikanmu akses untuk atur template sendiri.</h2>
          @if(! Auth::check())
          <a data-select="0" href="{{ url('register/client_signup') }}" class="btn " style="background-color: rgba(173, 173, 173, 0.5);    color: white;border-radius: 50px;margin-top: 15px;padding-left: 50px;padding-right: 50px;font-size: 15px;">Daftar Sekarang!</a>
          @else

          @endif
          <a href="https://youtu.be/d-ldghN33v0" target="_blank"  class="btn " style="background-color: rgba(173, 173, 173, 0.5);    color: white;border-radius: 50px;margin-top: 15px;padding-left: 50px;padding-right: 50px; font-size: 15px;">Tutorial</a>
        </div>
        <div class="col-xl-6 col-lg-6">
          <img src="{{asset('public/assets2')}}/img/Slider/landing page hp 3 2.png" class="img-fluid" style="width: 100%;max-width: 700px; padding-top: 125px;" alt="">
        </div>
      </div>

      

    </div>
  </section><!-- End Hero -->
       
  <main id="main">

    <!-- ======= Services Section ======= -->
    <section id="fitur" class="services">
      <div class="container" data-aos="fade-up">

        <div class="section-title text-center">
          <!-- <h2>Fitur</h2> -->
          <p style="font-family: 'Montserrat', sans-serif;font-weight:700;color: #1c2c70; text-transform: none;">Benefit Yang Tersedia</p>
        </div>
        <div class="row justify-content-sm-center" >
          <!-- <div class="col-sm-2"></div> -->
          <div class="col-lg-4 col-sm-8 col-md-8 d-flex align-items-stretch" data-aos="zoom-in" data-aos-delay="100">
            <!-- <div class="icon-box">
              <div><img src="{{asset('public/assets2')}}/img/Mobile Marketing-cuate.png" class="img-fluid" style="max-width: 200px;" alt=""> </div>
              <h4><a href="">Auto Selling Platform</a></h4>
              <p>Tiap produk akan dipromosikan oleh mikro nano influencer (90.000++ akun)</p>
            </div> -->
            <div>
              <img src="{{asset('public/assets2')}}/img/fitur/auto sell 1.png" class="img-fluid fitur" alt="" style="padding-right:52px;">
            </div>
          </div>
          <div class="col-lg-4 col-sm-8 col-md-8 d-flex align-items-stretch" data-aos="zoom-in" data-aos-delay="100">
            <!-- <div class="icon-box">
              <div><img src="{{asset('public/assets2')}}/img/Mobile Marketing-cuate.png" class="img-fluid" style="max-width: 200px;" alt=""> </div>
              <h4><a href="">Auto Selling Platform</a></h4>
              <p>Tiap produk akan dipromosikan oleh mikro nano influencer (90.000++ akun)</p>
            </div> -->
            <div>
              <img src="{{asset('public/assets2')}}/img/fitur/ecommerce full 1.png" class="img-fluid fitur" alt="" style="padding-right:52px;">
            </div>
          </div>
          <div class="col-lg-4 col-sm-8 col-md-8 d-flex align-items-stretch" data-aos="zoom-in" data-aos-delay="100">
            <!-- <div class="icon-box">
              <div><img src="{{asset('public/assets2')}}/img/Mobile Marketing-cuate.png" class="img-fluid" style="max-width: 200px;" alt=""> </div>
              <h4><a href="">Auto Selling Platform</a></h4>
              <p>Tiap produk akan dipromosikan oleh mikro nano influencer (90.000++ akun)</p>
            </div> -->
            <div>
              <img src="{{asset('public/assets2')}}/img/fitur/exclusive full 1.png" class="img-fluid fitur" alt="" style="padding-right:52px;">
            </div>
          </div>
          <!-- <div class="col-sm-2"></div> -->

          

        </div>

      </div>
    </section><!-- End Services Section -->

    <section id="paket" class="services">
      <div class="container" data-aos="fade-up">

        <div class="row justify-content-sm-center justify-content-md-center justify-content-lg-center" >
          <div class="col-lg-3 col-sm-6 col-md-6">
            <h4 class="title align-content-center" style="font-family: 'Montserrat', sans-serif;font-weight:700;text-align: left;font-size: 38px;color: #1c2c70;">Pilihan Paket</h4>
                <p class="description" style="padding: 0 0 0 0;font-family: 'Montserrat', sans-serif;font-weight:200;font-size: 20px;">Tentukan Pilihan Terbaik untuk Bisnismu!</p>
          </div>
          <div class="col-lg-3 col-sm-6 col-md-6 d-flex align-items-stretch " data-aos="zoom-in" data-aos-delay="100">
            <!-- <div class="icon-box">
              <div><img src="{{asset('public/assets2')}}/img/Mobile Marketing-cuate.png" class="img-fluid" style="max-width: 200px;" alt=""> </div>
              <h4><a href="">Auto Selling Platform</a></h4>
              <p>Tiap produk akan dipromosikan oleh mikro nano influencer (90.000++ akun)</p>
            </div> -->
            <div>
                <a href="{{ url('register/client_signup') }}" data-select="1">
              <img src="{{asset('public/assets2')}}/img/paket/basic full 1.png" class="img-fluid fitur" alt="" style="padding-right:52px;">
              </a>
            </div>
          </div>
          <div class="col-lg-3 col-sm-6 col-md-6 d-flex align-items-stretch" data-aos="zoom-in" data-aos-delay="100">
            <!-- <div class="icon-box">
              <div><img src="{{asset('public/assets2')}}/img/Mobile Marketing-cuate.png" class="img-fluid" style="max-width: 200px;" alt=""> </div>
              <h4><a href="">Auto Selling Platform</a></h4>
              <p>Tiap produk akan dipromosikan oleh mikro nano influencer (90.000++ akun)</p>
            </div> -->
            <div>
                <a href="{{ url('register/client_signup') }}" data-select="2">
              <img src="{{asset('public/assets2')}}/img/paket/pro full 2.png" class="img-fluid fitur" alt="" style="padding-right:52px;">
              </a>
            </div>
          </div>
          <div class="col-lg-3 col-sm-6 col-md-6 d-flex align-items-stretch" data-aos="zoom-in" data-aos-delay="100">
            <!-- <div class="icon-box">
              <div><img src="{{asset('public/assets2')}}/img/Mobile Marketing-cuate.png" class="img-fluid" style="max-width: 200px;" alt=""> </div>
              <h4><a href="">Auto Selling Platform</a></h4>
              <p>Tiap produk akan dipromosikan oleh mikro nano influencer (90.000++ akun)</p>
            </div> -->
            <div >
                <a href="{{ url('register/client_signup') }}" data-select="3">
              <img src="{{asset('public/assets2')}}/img/paket/advanced full 2.png" class="img-fluid fitur" alt="" style="padding-right:52px;">
              </a>
            </div>
          </div>
          <!-- <div class="col-sm-2"></div> -->

          

        </div>

      </div>
    </section><!-- End Services Section -->

   

    <!-- ======= Testimonials Section ======= -->
    <section id="klien" class="testimonials" style="background-color: white;">
      <div class="container" data-aos="zoom-in">
        <div class="section-title text-center">
          <!-- <h2>Fitur</h2> -->
          <p style="font-family: 'Montserrat', sans-serif;font-weight:700; color: #1c2c70; text-transform: none;">Klien Kami</p>
        </div>

        <div class="testimonials-slider swiper" data-aos="fade-up" data-aos-delay="100">
          <div class="swiper-wrapper">

            <div class="swiper-slide">
              <div class="testimonial-item">
                <img src="{{asset('public/assets2')}}/img/folder/1.1.png" class="testimonial-img img-fluid" alt="">
                <h3>Hello Baby and Kids Shop</h3>
                <h4>Menyediakan perlengkapan bayi dan anak – anak yang berkualitas dengan harga terjangkau</h4>
                <!-- <p>
                  <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                  Proin iaculis purus consequat sem cure digni ssim donec porttitora entum suscipit rhoncus. Accusantium quam, ultricies eget id, aliquam eget nibh et. Maecen aliquam, risus at semper.
                  <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                </p> -->
              </div>
            </div><!-- End testimonial item -->
            <div class="swiper-slide">
              <div class="testimonial-item">
                <img src="{{asset('public/assets2')}}/img/folder/2.1.png" class="testimonial-img img-fluid" alt="">
                <h3>Pempek Baba Sultan</h3>
                <h4>Lemak nian... taraso ikannyo</h4>
                <!-- <p>
                  <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                  Proin iaculis purus consequat sem cure digni ssim donec porttitora entum suscipit rhoncus. Accusantium quam, ultricies eget id, aliquam eget nibh et. Maecen aliquam, risus at semper.
                  <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                </p> -->
              </div>
            </div><!-- End testimonial item -->
            <div class="swiper-slide">
              <div class="testimonial-item">
                <img src="{{asset('public/assets2')}}/img/folder/3.1.png" class="testimonial-img img-fluid" alt="">
                <h3>Syena Cheesecake</h3>
                <h4>Cheesecake in jar to fulfill your craving</h4>
                <!-- <p>
                  <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                  Proin iaculis purus consequat sem cure digni ssim donec porttitora entum suscipit rhoncus. Accusantium quam, ultricies eget id, aliquam eget nibh et. Maecen aliquam, risus at semper.
                  <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                </p> -->
              </div>
            </div><!-- End testimonial item -->
            
            <div class="swiper-slide">
              <div class="testimonial-item">
                <img src="{{asset('public/assets2')}}/img/folder/4.1.png" class="testimonial-img img-fluid" alt="">
                <h3>Semangat Tani Sweet (STS)</h3>
                <h4>Semangat Tani Sweet. Usaha kelompok Tani dari Tapanuli, yang bergerak di bidang komoditi hasil pertanian.</h4>
                <!-- <p>
                  <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                  Proin iaculis purus consequat sem cure digni ssim donec porttitora entum suscipit rhoncus. Accusantium quam, ultricies eget id, aliquam eget nibh et. Maecen aliquam, risus at semper.
                  <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                </p> -->
              </div>
            </div><!-- End testimonial item -->

             
            <div class="swiper-slide">
              <div class="testimonial-item">
                <img src="{{asset('public/assets2')}}/img/folder/5.1.png" class="testimonial-img img-fluid" alt="">
                <h3>Mamam Sedapp</h3>
                <h4>Providing a variety of indonesian specific culinary that are sedapp and quality</h4>
                <!-- <p>
                  <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                  Proin iaculis purus consequat sem cure digni ssim donec porttitora entum suscipit rhoncus. Accusantium quam, ultricies eget id, aliquam eget nibh et. Maecen aliquam, risus at semper.
                  <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                </p> -->
              </div>
            </div><!-- End testimonial item -->

             
            <div class="swiper-slide">
              <div class="testimonial-item">
                <img src="{{asset('public/assets2')}}/img/folder/6.1.png" class="testimonial-img img-fluid" alt="">
                <h3>Fight Art</h3>
                <h4>Heathly, Positivity, Perfect Body</h4>
                <!-- <p>
                  <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                  Proin iaculis purus consequat sem cure digni ssim donec porttitora entum suscipit rhoncus. Accusantium quam, ultricies eget id, aliquam eget nibh et. Maecen aliquam, risus at semper.
                  <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                </p> -->
              </div>
            </div><!-- End testimonial item -->

             
            <div class="swiper-slide">
              <div class="testimonial-item">
                <img src="{{asset('public/assets2')}}/img/folder/7.1.png" class="testimonial-img img-fluid" alt="">
                <h3>Bumi Arasy</h3>
                <h4>The solution for happiness and success</h4>
                <!-- <p>
                  <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                  Proin iaculis purus consequat sem cure digni ssim donec porttitora entum suscipit rhoncus. Accusantium quam, ultricies eget id, aliquam eget nibh et. Maecen aliquam, risus at semper.
                  <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                </p> -->
              </div>
            </div><!-- End testimonial item -->

             
            <div class="swiper-slide">
              <div class="testimonial-item">
                <img src="{{asset('public/assets2')}}/img/folder/8.1.png" class="testimonial-img img-fluid" alt="">
                <h3>Datum Carport Gallery</h3>
                <h4>Not just what you’re looking for</h4>
                <!-- <p>
                  <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                  Proin iaculis purus consequat sem cure digni ssim donec porttitora entum suscipit rhoncus. Accusantium quam, ultricies eget id, aliquam eget nibh et. Maecen aliquam, risus at semper.
                  <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                </p> -->
              </div>
            </div><!-- End testimonial item -->

             
            <div class="swiper-slide">
              <div class="testimonial-item">
                <img src="{{asset('public/assets2')}}/img/folder/9.1.png" class="testimonial-img img-fluid" alt="">
                <h3>Manifestudio</h3>
                <h4>Capture memories around the world through souvenirs, gifts, Home decor, and illustration</h4>
                <!-- <p>
                  <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                  Proin iaculis purus consequat sem cure digni ssim donec porttitora entum suscipit rhoncus. Accusantium quam, ultricies eget id, aliquam eget nibh et. Maecen aliquam, risus at semper.
                  <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                </p> -->
              </div>
            </div><!-- End testimonial item -->

             
            <div class="swiper-slide">
              <div class="testimonial-item">
                <img src="{{asset('public/assets2')}}/img/folder/10.1.png" class="testimonial-img img-fluid" alt="">
                <h3>Barosfood </h3>
                <h4>Yuk beli makan ga pake ribet!</h4>
                <!-- <p>
                  <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                  Proin iaculis purus consequat sem cure digni ssim donec porttitora entum suscipit rhoncus. Accusantium quam, ultricies eget id, aliquam eget nibh et. Maecen aliquam, risus at semper.
                  <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                </p> -->
              </div>
            </div><!-- End testimonial item -->

              
            <div class="swiper-slide">
              <div class="testimonial-item">
                <img src="{{asset('public/assets2')}}/img/folder/11.1.png" class="testimonial-img img-fluid" alt="">
                <h3>Vacanatour</h3>
                <h4>Unforgettable Dream Vacation</h4>
                <!-- <p>
                  <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                  Proin iaculis purus consequat sem cure digni ssim donec porttitora entum suscipit rhoncus. Accusantium quam, ultricies eget id, aliquam eget nibh et. Maecen aliquam, risus at semper.
                  <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                </p> -->
              </div>
            </div><!-- End testimonial item -->

              
            <div class="swiper-slide">
              <div class="testimonial-item">
                <img src="{{asset('public/assets2')}}/img/folder/12.1.png" class="testimonial-img img-fluid" alt="">
                <h3>Cumi Hitam Racun</h3>
                <h4>Keracunan pengen nambah terus</h4>
                <!-- <p>
                  <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                  Proin iaculis purus consequat sem cure digni ssim donec porttitora entum suscipit rhoncus. Accusantium quam, ultricies eget id, aliquam eget nibh et. Maecen aliquam, risus at semper.
                  <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                </p> -->
              </div>
            </div><!-- End testimonial item -->

             
           
            
          </div>
          <div class="swiper-pagination"></div>
        </div>

      </div>
    </section><!-- End Testimonials Section -->

    

    

  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
  <footer id="footer">
    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-4 col-md-6 col-sm-6">
            <div class="footer-info">
              <h3 style="text-transform: none;font-size: 40px; padding-right: 30px;"><img src="{{asset('public/assets2')}}/img/LOGO 1 WHITE PNJANG.png" class="img-fluid" alt=""></h3>
              <p style="font-family: 'Montserrat', sans-serif;font-weight:400;text-transform: none;font-size: 15px;">Platform pengembang aplikasi digital berbasis Android dan iOS. Kamu bisa buat aplikasi sendiri semudah masak mie instan.</p>
              
            </div>
          </div>
          <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 footer-links">
            
              <!-- <h3 style="text-transform: none;font-size: 40px;">Ngelapak</h3> -->
              <!-- <h4>Alamat Kami</h4> -->
              <h3 style="font-size: 24px;font-family: 'Montserrat', sans-serif;font-weight:600">Alamat Kami</h3>
              <p style="font-family: 'Montserrat', sans-serif;">
                  Ruko Soho, 
                  Royal Residence BS-10 No.7 <br>
                  Surabaya, Indonesia <br>
<br>
                  <strong>Telepon: </strong>0811 – 3443 – 154 <br>
                  <strong>Email: </strong>contact@ngelapak.co.id <br>
              </p>
              <div class="social-links mt-3">
                <a href="https://instagram.com/ngelapak.co.id?utm_medium=copy_link" class="instagram" target="_blank" ><i class="bx bxl-instagram"></i></a>
                  <a href="https://business.facebook.com/latest/home?nav_ref=bm_home_redirect&business_id=1073657656465527&asset_id=101427295405162" class="facebook" target="_blank" ><i class="bx bxl-facebook"></i></a>
                  <a href="https://www.linkedin.com/company/ngelapak-co-id" class="instagram" target="_blank" ><i class="bx bxl-linkedin"></i></a>
                  <a href="https://twitter.com/Ngelapak2021?s=08" class="twitter" target="_blank" ><i class="bx bxl-twitter"></i></a>
              </div>
            
          </div>

          

          

          <div class="col-lg-5 col-md-12 footer-newsletter">
            <!-- <div class="col-lg-4 col-md-6 footer-info"> -->
              <h3 style="font-size: 24px;font-family: 'Montserrat', sans-serif;font-weight:600">Temukan Kami</h3>
              <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d494.65806635891215!2d112.6736403318811!3d-7.324155900000001!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd7fc58381ff9b7%3A0x194196ebbc065378!2sA%26A%20ROYAL%20RESIDENCE!5e0!3m2!1sid!2sid!4v1632986719031!5m2!1sid!2sid" width="100%" height="210" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
            <!-- </div> -->

          </div>

        </div>
      </div>
    </div>

    <div class="container">
      <div class="copyright">
        Copyright &copy; 2021.<strong><span> PT Ngelapak Bersama Kita</span></strong>. All Rights Reserved
      </div>
      <div class="credits">
        <!-- All the links in the footer should remain intact. -->
        <!-- You can delete the links only if you purchased the pro version. -->
        <!-- Licensing information: https://bootstrapmade.com/license/ -->
        <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/gp-free-multipurpose-html-bootstrap-template/ -->
        
      </div>
    </div>
  </footer><!-- End Footer -->

  <div id="preloader"></div>
  <a href="https://wa.me/+628113443154?text=Halo%20Ngelapak" target="_blank" class="float d-flex align-items-center justify-content-center"><i style="width:30px;" class="bi bi-whatsapp"></i></a>
  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <!-- Vendor JS Files -->
  <script src="{{asset('public/assets2')}}/vendor/aos/aos.js"></script>
  <script src="{{asset('public/assets2')}}/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="{{asset('public/assets2')}}/vendor/glightbox/js/glightbox.min.js"></script>
  <script src="{{asset('public/assets2')}}/vendor/isotope-layout/isotope.pkgd.min.js"></script/>
  <script src="{{asset('public/assets2')}}/vendor/php-email-form/validate.js"></script>
  <script src="{{asset('public/assets2')}}/vendor/purecounter/purecounter.js"></script>
  <script src="{{asset('public/assets2')}}/vendor/swiper/swiper-bundle.min.js"></script>
   <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>-->

  <!-- Template Main JS File -->
  <script src="{{asset('public/assets')}}/js/jquery.js"></script>
  <script src="{{asset('public/assets2')}}/js/main.js"></script>
  <script src="{{asset('public/assets2')}}/js/magnific.js"></script>
  <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js" integrity="sha512-IsNh5E3eYy3tr/JiX2Yx4vsCujtkhwl7SLqgnwLNgf04Hrt9BT9SXlLlZlWx+OK4ndzAoALhsMNcCmkggjZB1w==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>-->
  <script>
    $(document).ready(function() {
      $('a[href="{{ url('register/client_signup') }}"]').click(function (e) {
        e.preventDefault();
        var data =  $(this).data('select');
        window.location = $(this).attr('href') + '?selectParam=' + escape(data);
      });
    });
  </script>
  <script>
      $(document).ready(function() {
  $('#headerVideoLink').magnificPopup({
    type:'inline',
    midClick: true // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.
  });         
});
  </script>

</body>

</html>