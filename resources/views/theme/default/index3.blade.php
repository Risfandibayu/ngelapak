@extends('theme.default.source')

@section('content')

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top header-inner-pages">
    <div class="container d-flex align-items-center justify-content-between">

      <!-- <h1 class="logo"><a href="index.html">Logo</a></h1> -->
      <!--<img src="{{asset('public/assets')}}/img/logorev2.png" alt="" style="max-width:200px;">-->
      <!-- Uncomment below if you prefer to use an image logo -->
       <a href="index.html" class="logo"><img src="{{asset('public/assets')}}/img/LOGO 1 WHITE PNJANG.png" alt="" class="img-fluid"></a>

      <nav id="navbar" class="navbar">
        <ul>
          <li><a class="nav-link scrollto active" href="#hero">Daftar Sekarang</a></li>
          <li><a class="nav-link scrollto" href="#fitur">Fitur</a></li>
          <li><a class="nav-link scrollto" href="#package">Pilihan Paket</a></li>
          <li><a class="nav-link  scrollto" href="#klien">Klien Kami</a></li>
          <li><a class="nav-link scrollto" href="#footer">Hubungi Kami</a></li>
          <li><a class="nav-link scrollto" href="#team">FAQ</a></li>
          {{-- <li>
            <div class="btn-group" style="margin-left: 20px" role="group" aria-label="Basic outlined example">


              @if(! Auth::check())
                                    <a class="btn btn-primary" style="padding-right: 20px;    background-color: #1d2e724f;" href="{{ url('login') }}">
                                        
                                            {{ _lang('Login') }}
                                        
                                    </a>
                                    @if(get_option('allow_singup','yes') == 'yes')
                                        <a class="btn btn-primary" style="padding-right: 20px;    background-color: #1d2e724f;" href="{{ url('register/client_signup') }}">
                                            
                                                {{ _lang('Sign Up') }}
                                            
                                        </a>
                                    @endif
                                @else
                                    <a class="btn btn-primary" style="padding-right: 20px;    background-color: #1d2e724f;" href="{{ url('dashboard') }}">
                                        
                                            {{ _lang('Dashboard') }}
                                        
                                    </a>
                                    <a class="btn btn-primary" style="padding-right: 20px;    background-color: #1d2e724f;" href="{{ url('logout') }}">
                                        
                                            {{ _lang('Logout') }}
                                        
                                    </a>
                                @endif
            </div>
          </li> --}}
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
              Akun
            </a>
            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
              @if(! Auth::check())
              <li><a href="{{ url('login') }}">Login</a></li>
                @if(get_option('allow_singup','yes') == 'yes')
                <li><a href="{{ url('register/client_signup') }}">Sign Up</a></li>
                @endif
              @else
              <li><a href="{{ url('dashboard') }}">Dashboard</a></li>
              <li><a href="{{ url('logout') }}">Logout</a></li>
            </ul>
        @endif
          </li>
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav><!-- .navbar -->

    </div>
  </header><!-- End Header -->

  <!-- ======= Hero Section ======= -->

  <section id="hero">
    <div id="her" data-bs-interval="5000" class="carousel slide carousel-fade" data-bs-ride="carousel">

      <div class="carousel-inner">

        <!-- Slide 1 -->
        <div class="carousel-item active" style="background-image: url({{asset('public/assets')}}/img/Slider/landing\ page\ BG.jpg)">
          <div class="carousel-container">
            <div class="row " style="margin-top: 15%;">
              <div class="col-sm-6">
                <div class="container">
                  <h2 class="animated fadeInDown" style="font-family: cera pro medium; padding-top: 20%;">Ngelapak - Milyaran Aplikasi Untuk Indonesia
                  </h2>
                  <p class="animated fadeInUp" style="font-family: cera pro light;">Platform pengembang aplikasi digital berbasis Android dan iOS. Kamu bisa buat aplikasi sendiri semudah masak mie instan.</p>
                  @if(! Auth::check())
                  <a href="{{ url('register/client_signup') }}" style="margin-right: 10px" class="btn-get-started animated fadeInUp scrollto mb-2">Daftar Sekarang!</a>
                  @else

                  @endif
                  
                 
                  
                  <a href="#about" class="btn-get-started animated fadeInUp scrollto">Tutorial</a>
                </div>
              </div>
              <div class="col-sm-6" style="text-align: center;">
                <img src="{{asset('public/assets')}}/img/Slider/landing page hp 3 2.png" width="100%" alt="">
              </div>
            </div>
          </div>
        </div>

        <!-- Slide 2 -->

      </div>

    </div>
  </section><!-- End Hero -->

  <main id="main">
    <!-- ======= Services Section ======= -->
    <section id="fitur" class="services">
      <div class="container-fluid">

        <div class="section-title">
          <h3 style="color: #1c2c70; font-family: Cera Pro;font-size: 38px;">Fitur yang Tersedia</h3>
        </div>

        <div class="row justify-content-center">
          <div class="col-xl-10">

            <div class="row">
              <!-- <div class="col-lg-1"></div> -->
              <div class="col-lg-3" style="text-align: center;border-radius: 5%;
              box-shadow: 0px 0px 20px rgb(0 0 0 / 25%);margin-right: auto; margin-top: 20px;padding-top: 20px;">
                <img src="{{asset('public/assets')}}/img/Mobile Marketing-cuate.png" style="width: 75%;" alt="">
                <h4 class="title align-content-center" style="text-align: center; font-family: Cera pro;color: #1c2c70;">Auto Selling Platform</h4>
                <p class="description" style="font-family: Cera pro light;font-size: 16px;">Tiap produk akan dipromosikan oleh mikro nano influencer (90.000++ akun)
                </p>
              </div>
              <div class="col-lg-1"></div>

              <div class="col-lg-3" style="text-align: center;border-radius: 5%;
              box-shadow: 0px 0px 20px rgb(0 0 0 / 25%);margin-right: auto; margin-top: 20px;padding-top: 20px;">
                <img src="{{asset('public/assets')}}/img/In no time-cuate (1).png" style="width: 75%;" alt="">

                <h4 class="title" style="font-family: Cera pro;color: #1c2c70;">E-Commerce</h4>
                <p class="description" style="font-family: Cera pro light;font-size: 16px;">Dalam aplikasi bisa digunakan untuk pemesanan, transaksi, logistik, dan pembayaran digital
                </p>
              </div>
              <div class="col-lg-1"></div>

              <div class="col-lg-3" style="text-align: center;border-radius: 5%;
              box-shadow: 0px 0px 20px rgb(0 0 0 / 25%); margin-top: 20px;padding-top: 20px;">
                <img src="{{asset('public/assets')}}/img/Collaboration-cuate (2).png" style="width: 75%;" alt="">

                <h4 class="title" style="font-family: Cera pro;color: #1c2c70;">Exclusive Members Community</h4>
                <p class="description" style="font-family: Cera pro light;font-size: 16px;">Mengikuti seluruh program yang ada di Ngelapak (gratis diskusi, )
                </p>
              </div>
            </div>
          </div>

        </div>
    </section><!-- End Services Section -->




    <section id="package" class="services" style="margin-top: 50px;">
      <div class="container-fluid">

        <div class="row justify-content-center">
          <div class="col-xl-10">
            <div class="row">
              <div class="col-lg-2" style="text-align: left;margin-right: auto; margin-top: 20px;">
                <h4 class="title align-content-center" style="text-align: left; font-family: Cera pro;font-size: 38px;color: #1c2c70;">Package Offer</h4>
                <p class="description" style="padding: 0 0 0 0;font-family: Cera pro light;font-size: 20px;">Tentukan Pilihan Terbaik untuk Bisnismu!</p>

              </div>
              <!-- <div class="col-lg-1"></div> -->

              <div class="col-lg-3" style="text-align: center; border-radius: 5%;
              box-shadow: 0px 0px 20px rgb(0 0 0 / 25%);margin-right: auto; padding-top: 20px; margin-top: 20px;">

                <h4 class="title" style="font-family: Cera pro;font-size: 20px;color: #1c2c70;">
                  Ngelapak Basic
                  <br>
                  (<span class="text-decoration-line-through">299k</span>) 
                  (99k/bulan)
                </h4>
                <hr>


                <p class="description" style="font-family: Cera pro light;font-size: 20px;">Template dasar aplikasi </p>
                <hr>
                <p class="description" style="font-family: Cera pro light;font-size: 20px;">Profil usaha</p>
                <hr>
                <p class="description" style="font-family: Cera pro light;font-size: 20px;">Katalog produk</p>
                <hr>
                <p class="description" style="font-family: Cera pro light;font-size: 20px;">Laporan promosi dan penjualan</p>
                <hr>
                <p class="description" style="font-family: Cera pro light;font-size: 20px;">Eksklusif member </p>
                <hr>
                <p class="description" style="font-family: Cera pro light;font-size: 20px;">Dukungan tim Ngelapak</p>
                <!-- <hr> -->
              </div>

              <div class="col-lg-3" style="text-align: center; border-radius: 5%;
              box-shadow: 0px 0px 20px rgb(0 0 0 / 25%);margin-right: auto; padding-top: 20px; margin-top: 20px;">

                <h4 class="title" style="font-family: Cera pro;font-size: 20px;color: #1c2c70;">
                  Ngelapak Pro <br>
                  (<span class="text-decoration-line-through">649k</span>) 
                  (449k /bulan)
                </h4>
                <hr>


                <p class="description" style="font-family: Cera pro light;font-size: 20px;">Paket Ngelapak Basic</p>
                <hr>
                <p class="description" style="font-family: Cera pro light;font-size: 20px;">Fitur pemesanan </p>
                <hr>
                <p class="description" style="font-family: Cera pro light;font-size: 20px;">Fitur pembayaran digital </p>
                <hr>
                <p class="description" style="font-family: Cera pro light;font-size: 20px;">Fitur logistik</p>
              </div>
              <!-- <div class="col-lg-1"></div> -->










              <div class="col-lg-3" style="text-align: center; border-radius: 5%;
              box-shadow: 0px 0px 20px rgb(0 0 0 / 25%);margin-right: auto; padding-top: 20px; margin-top: 20px;">

                <h4 class="title" style="font-family: Cera pro;font-size: 20px;color: #1c2c70;">
                  Ngelapak Advanced <br>
                  (<span class="text-decoration-line-through">799k</span>) 
                  (499k/ bulan)
                </h4>
                <hr>
                <p class="description" style="font-family: Cera pro light;font-size: 20px;">Paket Ngelapak Pro</p>
                <hr>
                <p class="description" style="font-family: Cera pro light;font-size: 20px;">Akses mandiri pembuatan aplikasi (self services acces build apps)</p>
                <hr>
                <p class="description" style="font-family: Cera pro light;font-size: 20px;">Publikasi di iOS</p>
                <hr>
                

              </div>

            </div>
          </div>
        </div>

      </div>
    </section><!-- End Services Section -->

    <!-- ======= Portfolio Section ======= -->
    <section id="klien">
      <div class="container">
        <div class="section-title" style="margin-bottom: -50px; margin-top: -60px;">
          <h3 style="color: #1c2c70; font-family: Cera Pro;font-size: 38px;">Klien Kami</h3>
        </div>
        <div class="row">
          <div class="col-sm-12">
          </div>
          <div id="customers-testimonials" class="owl-carousel">

            <!--TESTIMONIAL 1 -->
            <div class="item">
              <img src="{{asset('public/assets')}}/img/folder/BUMI ARASY 1.png" alt="">
            </div>
            <!--END OF TESTIMONIAL 1 -->

            <!--TESTIMONIAL 1 -->
            <div class="item">
              <img src="{{asset('public/assets')}}/img/folder/BUMI ARASY 2.png" alt="">
            </div>
            <!--END OF TESTIMONIAL 1 -->

            <!--TESTIMONIAL 1 -->
            <div class="item">
              <img src="{{asset('public/assets')}}/img/folder/COMELY 1.png" alt="">
            </div>
            <!--END OF TESTIMONIAL 1 -->

            <!--TESTIMONIAL 1 -->
            <div class="item">
              <img src="{{asset('public/assets')}}/img/folder/COMELY 2.png" alt="">
            </div>
            <!--END OF TESTIMONIAL 1 -->
            <!--TESTIMONIAL 1 -->
            <div class="item">
              <img src="{{asset('public/assets')}}/img/folder/CUMI HITAM 1.png" alt="">
            </div>
            <!--END OF TESTIMONIAL 1 -->
            <!--TESTIMONIAL 1 -->
            <div class="item">
              <img src="{{asset('public/assets')}}/img/folder/CUMI HITAM 2.png" alt="">
            </div>
            <!--END OF TESTIMONIAL 1 -->
            <!--TESTIMONIAL 1 -->
            <div class="item">
              <img src="{{asset('public/assets')}}/img/folder/FIGHT ART 1.png" alt="">
            </div>
            <!--END OF TESTIMONIAL 1 -->
            <!--TESTIMONIAL 1 -->
            <div class="item">
              <img src="{{asset('public/assets')}}/img/folder/FIGHT ART 2.png" alt="">
            </div>
            <!--END OF TESTIMONIAL 1 -->


          </div>
        </div>
      </div>
      </div>

    </section>

  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
  <footer id="footer">

    <div class="footer-top">
      <div class="container-fluid">
        <div class="row justify-content-center">
          <div class="col-xl-10">
            <div class="row">
              <div class="col-lg-6 col-md-6 footer-info">
                <h3 style="font-family: Cera pro;font-size: 24px;">Find Us</h3>
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d494.65806635891215!2d112.6736403318811!3d-7.324155900000001!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd7fc58381ff9b7%3A0x194196ebbc065378!2sA%26A%20ROYAL%20RESIDENCE!5e0!3m2!1sid!2sid!4v1632986719031!5m2!1sid!2sid" width="200" height="150" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
              </div>
              <div class="col-lg-3 col-md-6 footer-links">
                <h4 style="font-family: Cera pro;font-size: 24px;">Visit Us</h4>
                
                  
                  
                  Ruko Soho <br>
                  Royal Residence BS-10 No.7 <br>
                  Surabaya, Indonesia <br>
                  <br>
                  <strong>Phone:</strong> 0811 – 3443 – 154 <br>
                  <strong>Email:</strong> contact@ngelapak.co.id <br>
              </div>

              <div class="col-lg-3 col-md-6 footer-contact">
                <h3 style="font-family: Cera pro;font-size: 24px;">Follow Us</h3>
                <div class="social-links mt-3">
                  <a href="https://instagram.com/ngelapak.co.id?utm_medium=copy_link" class="instagram" target="_blank" ><i class="bx bxl-instagram" ></i></a>
                  <a href="https://business.facebook.com/latest/home?nav_ref=bm_home_redirect&business_id=1073657656465527&asset_id=101427295405162" class="facebook" target="_blank" ><i class="bx bxl-facebook"></i></a>
                  <a href="https://www.linkedin.com/company/ngelapak-co-id" class="instagram" target="_blank" ><i class="bx bxl-linkedin"></i></a>
                  <a href="https://twitter.com/Ngelapak2021?s=08" class="twitter" target="_blank" ><i class="bx bxl-twitter"></i></a>
                </div>
                
              </div>

             

            </div>
          </div>
        </div>
      </div>
    </div>

  </footer><!-- End Footer -->

  <div id="preloader"></div>
  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>
@endsection
