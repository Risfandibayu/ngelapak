<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Ngelapak - Milyaran Aplikasi Untuk Indonesia</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="{{asset('public/assets2')}}/img/icon.png" rel="icon">
  <link href="{{asset('public/assets2')}}/img/icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="{{asset('public/assets2')}}/vendor/aos/aos.css" rel="stylesheet">
  <link href="{{asset('public/assets2')}}/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="{{asset('public/assets2')}}/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="{{asset('public/assets2')}}/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="{{asset('public/assets2')}}/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="{{asset('public/assets2')}}/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="{{asset('public/assets2')}}/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="{{asset('public/assets2')}}/css/style.css" rel="stylesheet">
  <link href="{{asset('public/assets2')}}/css/magnific.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: Gp - v4.6.0
  * Template URL: https://bootstrapmade.com/gp-free-multipurpose-html-bootstrap-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
  <style>
      #headerPopup{
  width:75%;
  margin:0 auto;
}

#headerPopup iframe{
  width:100%;
  margin:0 auto;
}
  </style>
</head>

<body>
    
<header id="header" class="fixed-top header-inner-pages">
    <div class="container d-flex align-items-center justify-content-sm-between">

      <!-- <h1 class="logo me-auto me-lg-0" style="float: left;"><a href="index.html">Gp<span>.</span></a></h1> -->
      <!-- Uncomment below if you prefer to use an image logo -->
      <a href="/" class="logo me-auto me-lg-0"><img src="{{asset('public/assets2')}}/img/LOGO 1 WHITE PNJANG.png" alt="" class="img-fluid"></a>

      <nav id="navbar" class="navbar" style="font-family: 'Cera Pro light';">
        <ul>
          <li><a class="nav-link scrollto" href="/">Kembali Ke Beranda</a></li>
          
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav><!-- .navbar -->

      

    </div>
  </header><!-- End Header -->
    <main id="main">
    
        <!-- ======= Breadcrumbs ======= -->
        <section class="breadcrumbs">
          <div class="container">
    
            <div class="d-flex justify-content-between align-items-center">
              <h2>FAQ</h2>
              <ol>
                <li><a href="/">Beranda</a></li>
                <li>FAQ</li>
              </ol>
            </div>
    
          </div>
        </section><!-- End Breadcrumbs -->
    <div id="headerPopup" class="mfp-hide embed-responsive embed-responsive-21by9">
        <iframe class="embed-responsive-item" width="854" height="480" src="https://www.youtube.com/embed/d-ldghN33v0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
      </div>
        <section class="inner-page">
          <div class="container col-md-8">
              <div class="justify-content-between align-items-center text-center mb-4">
                  <h2>Punya Pertanyaan?</h2>
              </div>
            <div class="accordion" id="accordionExample">
                
  <div class="accordion-item">
    <h2 class="accordion-header" id="headingOne">
      <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          <strong>Apa itu Ngelapak?</strong>
        
      </button>
    </h2>
    <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
      <div class="accordion-body">
        Perusahaan pengembang aplikasi seluler berbasis web maupun mobile. Spesialisasi
dalam pembuatan dan perancangan WebbApp dengan menekankan pada
pemasaran digital (digital marketing), dan profil perusahaan (company profile).
Tujuan Ngelapak ingin membantu membangun skalabilitas produk UMKM untuk
berkembang tanpa terbebani oleh biaya.
      </div>
    </div>
  </div>
  <div class="accordion-item">
    <h2 class="accordion-header" id="headingTwo">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
          <strong>Apa kelebihan membuat aplikasi di Ngelapak?</strong>
        
      </button>
    </h2>
    <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
      <div class="accordion-body">
          
        Fitur unggulan dalam aplikasi, adanya fitur promosi & penjualan otomatis (auto
selling platform), dan toko online autopilot (independent e-commerce). Selain itu,
pembuatan aplikasi Ngelapak bisa dipublikasi di Google Play Store (Android) dan
App Store (iOS).
      </div>
    </div>
  </div>
  
  <div class="accordion-item">
    <h2 class="accordion-header" id="headingThree">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
          <strong>Bagaimana cara membuat aplikasi di Ngelapak?</strong>
        
      </button>
    </h2>
    <div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
      <div class="accordion-body">
          Bisa mengikuti sesuai panduan dalam video <a href="#headerPopup" id="headerVideoLink" target="_blank">Tutorial</a>
        
      </div>
    </div>
  </div>
  
    <div class="accordion-item">
    <h2 class="accordion-header" id="headingFour">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
          <strong>Apa itu akses mandiri pembuatan aplikasi (self service access builder apps)?</strong>
        
      </button>
    </h2>
    <div id="collapseFour" class="accordion-collapse collapse" aria-labelledby="headingFour" data-bs-parent="#accordionExample">
      <div class="accordion-body">
          Akses untuk Anda, pelaku usaha yang ingin berkreatifitas melalui desain toko online
sesuai keinginan.
        
      </div>
    </div>
  </div>
    <div class="accordion-item">
    <h2 class="accordion-header" id="headingFive">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
          <strong>
              Apa saja paket produk di Ngelapak?
          </strong>
        
      </button>
    </h2>
    <div id="collapseFive" class="accordion-collapse collapse" aria-labelledby="headingFive" data-bs-parent="#accordionExample">
      <div class="accordion-body">
        Paket Ngelapak Basic, Pro, dan Advanced. Semua pilihan paket tersebut
mendapatkan gratis pemakaian 14 hari pertama.
      </div>
    </div>
  </div>
</div>
          </div>
        </section>
    
      </main><!-- End #main -->

  <!-- ======= Footer ======= -->
  <footer id="footer">
    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-4 col-md-6 col-sm-6">
            <div class="footer-info">
              <h3 style="text-transform: none;font-family: 'Cera Pro';font-size: 40px; padding-right: 30px;"><img src="{{asset('public/assets2')}}/img/LOGO 1 WHITE PNJANG.png" class="img-fluid" alt=""></h3>
              <p style="text-transform: none;font-family: 'Cera Pro light';font-size: 15px;">Platform pengembang aplikasi digital berbasis Android dan iOS. Kamu bisa buat aplikasi sendiri semudah masak mie instan.</p>
              
            </div>
          </div>
          <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 footer-links">
            
              <!-- <h3 style="text-transform: none;font-family: 'Cera Pro';font-size: 40px;">Ngelapak</h3> -->
              <!-- <h4>Alamat Kami</h4> -->
              <h3 style="font-family: Cera pro;font-size: 24px;">Alamat Kami</h3>
              <p>
                  Ruko Soho, 
                  Royal Residence BS-10 No.7 <br>
                  Surabaya, Indonesia <br>
<br>
                  <strong>Telepon: </strong>0811 – 3443 – 154 <br>
                  <strong>Email: </strong>contact@ngelapak.co.id <br>
              </p>
              <div class="social-links mt-3">
                <a href="https://instagram.com/ngelapak.co.id?utm_medium=copy_link" class="instagram" target="_blank" ><i class="bx bxl-instagram"></i></a>
                  <a href="https://business.facebook.com/latest/home?nav_ref=bm_home_redirect&business_id=1073657656465527&asset_id=101427295405162" class="facebook" target="_blank" ><i class="bx bxl-facebook"></i></a>
                  <a href="https://www.linkedin.com/company/ngelapak-co-id" class="instagram" target="_blank" ><i class="bx bxl-linkedin"></i></a>
                  <a href="https://twitter.com/Ngelapak2021?s=08" class="twitter" target="_blank" ><i class="bx bxl-twitter"></i></a>
              </div>
            
          </div>

          

          

          <div class="col-lg-5 col-md-12 footer-newsletter">
            <!-- <div class="col-lg-4 col-md-6 footer-info"> -->
              <h3 style="font-family: Cera pro;font-size: 24px;">Temukan Kami</h3>
              <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d494.65806635891215!2d112.6736403318811!3d-7.324155900000001!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd7fc58381ff9b7%3A0x194196ebbc065378!2sA%26A%20ROYAL%20RESIDENCE!5e0!3m2!1sid!2sid!4v1632986719031!5m2!1sid!2sid" width="100%" height="210" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
            <!-- </div> -->

          </div>

        </div>
      </div>
    </div>

    <div class="container">
      <div class="copyright">
        Copyright &copy; 2021.<strong><span> PT Ngelapak Bersama Kita</span></strong>. All Rights Reserved
      </div>
      <div class="credits">
        <!-- All the links in the footer should remain intact. -->
        <!-- You can delete the links only if you purchased the pro version. -->
        <!-- Licensing information: https://bootstrapmade.com/license/ -->
        <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/gp-free-multipurpose-html-bootstrap-template/ -->
        
      </div>
    </div>
  </footer><!-- End Footer -->

  <div id="preloader"></div>
  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <!-- Vendor JS Files -->
  <script src="{{asset('public/assets2')}}/vendor/aos/aos.js"></script>
  <script src="{{asset('public/assets2')}}/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="{{asset('public/assets2')}}/vendor/glightbox/js/glightbox.min.js"></script>
  <script src="{{asset('public/assets2')}}/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="{{asset('public/assets2')}}/vendor/php-email-form/validate.js"></script>
  <script src="{{asset('public/assets2')}}/vendor/purecounter/purecounter.js"></script>
  <script src="{{asset('public/assets2')}}/vendor/swiper/swiper-bundle.min.js"></script>
   <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>-->

  <!-- Template Main JS File -->
  <script src="{{asset('public/assets')}}/js/jquery.js"></script>
  <script src="{{asset('public/assets2')}}/js/main.js"></script>
    <script src="{{asset('public/assets2')}}/js/magnific.js"></script>
  
  <script>
    $(document).ready(function() {
      $('a[href="{{ url('register/client_signup') }}"]').click(function (e) {
        e.preventDefault();
        var data =  $(this).data('select');
        window.location = $(this).attr('href') + '?selectParam=' + escape(data);
      });
    });
  </script>
<script>
      $(document).ready(function() {
  $('#headerVideoLink').magnificPopup({
    type:'inline',
    midClick: true // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.
  });         
});
  </script>
</body>

</html>