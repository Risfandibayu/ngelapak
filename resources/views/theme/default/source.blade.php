<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Ngelapak - Milyaran Aplikasi Untuk Indonesia</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="{{asset('public/assets2')}}/img/icon.png" rel="icon">
  <link href="{{asset('public/assets2')}}/img/icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <!-- <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet"> -->
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@200&display=swap" rel="stylesheet">
  <!-- Vendor CSS Files -->
  <link href="{{asset('public/assets')}}/vendor/animate.css/animate.min.css" rel="stylesheet">
  <link href="{{asset('public/assets')}}/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="{{asset('public/assets')}}/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="{{asset('public/assets')}}/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="{{asset('public/assets')}}/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="{{asset('public/assets')}}/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="{{asset('public/assets')}}/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">
  {{-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"> --}}
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css" />
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">

  <!-- Template Main CSS File -->
  <link href="{{asset('public/assets')}}/css/style.css" rel="stylesheet">
  <!-- <link href="{{asset('assets')}}/css/testimonial.css" rel="stylesheet"> -->
  <!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/{{asset('assets')}}/owl.carousel.min.css" rel="stylesheet" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/{{asset('assets')}}/owl.theme.default.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script> -->

  <!-- =======================================================
  * Template Name: Hidayah - v4.5.0
  * Template URL: https://bootstrapmade.com/hidayah-free-simple-html-template-for-corporate/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
  @yield('css')
</head>

<body>

 
 @yield('content')
 <!-- Vendor JS Files -->

 <script src="{{asset('public/assets')}}/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
 <script src="{{asset('public/assets')}}/vendor/glightbox/js/glightbox.min.js"></script>
 <script src="{{asset('public/assets')}}/vendor/isotope-layout/isotope.pkgd.min.js"></script>
 <script src="{{asset('public/assets')}}/vendor/php-email-form/validate.js"></script>
 <script src="{{asset('public/assets')}}/vendor/purecounter/purecounter.js"></script>
 <script src="{{asset('public/assets')}}/vendor/swiper/swiper-bundle.min.js"></script>
 <script src="{{asset('public/assets')}}/vendor/waypoints/noframework.waypoints.js"></script>
 {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script> --}}
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
 <!-- Template Main JS File -->
 <!--<script src="{{asset('public/assets')}}/js/jquery.js"></script>-->
 <script src="{{asset('public/assets')}}/js/main.js"></script>
 
  
   <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
  @yield('js')
 <script>
   jQuery(document).ready(function($) {
     "use strict";
     //  TESTIMONIALS CAROUSEL HOOK
     $('#customers-testimonials').owlCarousel({
       loop: true,
       center: true,
       margin: -38,
       dots: true,
       autoplayTimeout: 5000,
       smartSpeed: 450,
       responsive: {
         0: {
           items: 1
         },
         768: {
           items: 2
         },
         1170: {
           items: 3
         }
       }
     });
   });
 </script>
</body>

</html>