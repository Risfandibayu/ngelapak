<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'companies';
    
    protected $fillable = [
        'progress','business_name','alamat','valid_to','kec','kota','prov','kode_pos','cabang','deskripsi','slogan','ig','fb','web','logo','ft_prd1','ft_prd2','ft_prd3','ft_prd4','ft_prd5','splash','bg_apk','judul_camp','sub_judul','key_brand','deskripsi_camp','cover','thumbnail','logo_camp','gambar_camp'
    ];
	
	public function package()
    {
        return $this->belongsTo('App\Package',"package_id")->withDefault();
    }
}