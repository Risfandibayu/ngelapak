<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subdistrict extends Model
{
    protected $table = 'subdistricts';
    protected $fillable = [
        'subdistrict_id', 'city_id','subdistrict_name'
    ];

}
