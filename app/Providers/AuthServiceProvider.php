<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Auth\Notifications\VerifyEmail;
use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Auth;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
         VerifyEmail::toMailUsing(function ($notifiable, $url) {
            return (new MailMessage)
                ->greeting(sprintf('Hai, '.Auth::user()->name.'!'))
                ->subject('Verifikasi Alamat Email')
                ->line('Terima kasih telah mengunjungi platform ngelapak.co.id dan melakukan konfirmasi
pendaftaran. Tekan tombol di bawah untuk verifikasi alamat email.
                ')
                ->action('Verifikasi email saya', $url)
                ->line('Selanjutnya, kamu akan di arahkan untuk mengisi data diri sesuai identitas.')
                ->line('Jika ada pertanyaan, hubungi kami di contact@ngelapak.co.id.')
                ->salutation(' ')
                ->line('Salam,')
                
                // ->line('Salam kami,'.htmlspecialchars_decode('<br>').'Tim Ngelapak');
                ->line('Tim Ngelapak');
                
                
        });
        
        Passport::routes();
        
        
    }
}
