<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Company;
use DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        // $validator = Validator::make($request->all(), [
        //     'name' => 'required|max:191',
        //     'no_hp' => 'required',
        //     'email' => 'required|email|unique:users|max:191',
        //     'password' => 'required|max:20|min:6|confirmed',
            
        // ]);

        
        $trial_period = get_option('trial_period', 14);
        
        if($trial_period < 1){
            $valid_to = date('Y-m-d', strtotime(date('Y-m-d'). " -1 day"));
        }else{
            $valid_to = date('Y-m-d', strtotime(date('Y-m-d'). " + $trial_period days"));
        }
        if(!empty(User::where('email', '=', $request->email)->first())){
            return response(['message' => 'Email atau password salah!']);
        }
        if($request->password != $request->password_confirmation){
            return response(['message' => 'Password konfirmasi tidak sama!']);
        }
        
        DB::beginTransaction();

        //Create Company
        $company = new Company();
        $company->business_name = $request->business_name;
        // $company->business_name = $request->business_name;
        $company->package_id = $request->package;
        $company->package_type = $request->package_type;
        $company->membership_type = 'trial';
        $company->status = 1;
        $company->valid_to = $valid_to;

        //Package Details
        $package = $company->package;
        $company->websites_limit = 1;
        $company->recurring_transaction = 'Yes';
        $company->online_payment = 'Yes';

        $company->save();



        //Create User      
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->no_hp = $request->no_hp;
        if( get_option('email_verification') == 'disabled' ){
            $user->email_verified_at = now();
        }
        $user->password = Hash::make($request->password);
        $user->user_type = 'user';
        $user->status = 1;
        
        $user_language = session('user_language'); 
        if($user_language == ''){
            $user_language = get_option('language');
        }
        $user->language = $user_language;
        $user->profile_picture = 'default.png';
        $user->company_id = $company->id;
        $user->save();
            // var_dump($res['key']);
            // foreach($res as $r){
            //     echo $r['status'];
            // }
        DB::commit();
        $accessToken = $user->createToken('authToken')->accessToken;
        return response([ 'user' => $user, 'access_token' => $accessToken]);
    }

    public function login(Request $request)
    {
        $loginData = $request->validate([
            'email' => 'email|required',
            'password' => 'required'
        ]);

        if (!auth::attempt($loginData)) {
            return response(['message' => 'Email atau password salah!']);
        }

        if (Auth::user()->email_verified_at == null){
            return response(['message' => 'Verifikasi email anda!']);
        }
        $accessToken = Auth::user()->createToken('authToken')->accessToken;

        return response(['user' => auth::user(), 'access_token' => $accessToken]);

    }
}
