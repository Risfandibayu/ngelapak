<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\User;
use App\Contact;
use Hash;
use Auth;
use App\Company;
use App\Province;
use App\City;
use App\Subdistrict;
use Image;
use DB;

class ProfileController extends Controller
{
	public function __construct()
    {	
		$this->middleware(function ($request, $next) {
			if(has_membership_system() == 'enabled' && Auth::user()->user_type == "user"){
				if( membership_validity() < date('Y-m-d')){
					return redirect('membership/extend')->with('message',_lang('Your membership has expired. Please renew your membership !'));
				}
			}

			return $next($request);
		});
    }

    public function edit()
    {
        $profile = User::find(Auth::User()->id);
        $data['province'] = Province::all();
        $data['city'] = City::all();
        $data['subdistrict'] = Subdistrict::all();
        return view('backend.profile.profile_edit',compact('profile'),$data);
    }


    public function update(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => [
                'required',
                Rule::unique('users')->ignore(Auth::User()->id),
            ],
            'profile_picture' => 'nullable|image|max:5120',
        ]);
       
        DB::beginTransaction();

        $profile = Auth::user();
        $profile->name = $request->name;
        $profile->email = $request->email;
        $profile->language = $request->language;
        $profile->nama_lengkap = $request->nama_lengkap;
        $profile->jk = $request->jk;
        $profile->tgl_lahir = $request->tgl_lahir;
        $profile->no_hp = $request->no_hp;
        $profile->nik = $request->nik;
        $profile->alamat = $request->alamat;
        $profile->kota = $request->kota;
        $profile->kec = $request->kec;
        $profile->prov = $request->prov;
        $profile->kode_pos = $request->kode_pos;
        
		if ($request->hasFile('profile_picture')){
            $image = $request->file('profile_picture');
            $file_name = "profile_".time().'.'.$image->getClientOriginalExtension();
            Image::make($image)->crop(300, 300)->save(base_path('public/uploads/profile/') .$file_name);
            $profile->profile_picture = $file_name;
        }
		
        $profile->save();
        
        

        $request->session()->put('user_language', $profile->language);

        //Update Contact
        if($profile->user_type == 'client'){
            $contact = Contact::where('user_id',$profile->id)
                              ->update(['contact_email' => $profile->email]);
        }

        DB::commit();

        return redirect('profile/edit')->with('success', _lang('Data diri Berhasil di simpan'));
    }
    
    public function getCity($id)
    {
        //mengambil data kota/kab
        // $city = City::where('province_id',$id)->get();
        $city = City::where('province_id',$id)->get();
        return response()->json($city); 
    }
    public function getSubdistrict($id)
    {
        //mengambil data kota/kab
        $subdistrict = Subdistrict::where('city_id',$id)->get();
        return response()->json($subdistrict); 
    }

    public function signup_step()
    {
        $profile = User::find(Auth::User()->id);
        $data['province'] = Province::all();
        $data['city'] = City::all();
        $data['subdistrict'] = Subdistrict::all();
        // $data['id'] = $id;
        return view('auth.signup_step',compact('profile'),$data);
    }


    public function update_step(Request $request)
    {
        $this->validate($request, [
            'nama_lengkap' => 'required',
            'jk' => 'required',
            'tgl_lahir' => 'required',
            'no_hp' => 'required',
            'nik' => 'required',
            'alamat' => 'required',
            'kota' => 'required',
            'kec' => 'required',
            'prov' => 'required',
            'kode_pos' => 'required',
            
        ]);
       
        DB::beginTransaction();

        $profile = Auth::user();
        // $profile->name = $profile->name;
        // $profile->email = $profile->email;
        // $profile->language = $profile->language;
        $profile->nama_lengkap = $request->nama_lengkap;
        $profile->jk = $request->jk;
        $profile->tgl_lahir = $request->tgl_lahir;
        $profile->no_hp = $request->no_hp;
        $profile->nik = $request->nik;
        $profile->alamat = $request->alamat;
        $profile->kota = $request->kota;
        $profile->kec = $request->kec;
        $profile->prov = $request->prov;
        $profile->kode_pos = $request->kode_pos;
        
        // if ($request->hasFile('profile_picture')){
        //     $image = $request->file('profile_picture');
        //     $file_name = "profile_".time().'.'.$image->getClientOriginalExtension();
        //     Image::make($image)->crop(300, 300)->save(base_path('public/uploads/profile/') .$file_name);
        //     $profile->profile_picture = $file_name;
        // }
		
        $profile->save();
        
        $company = Company::where('id', $profile->company_id)->first();
        $company->business_name = $request->business_name;
        $company->save();
        
        // $lpdpui = Auth::user()->id->where('name','like',  'LPDPUI'.'%');
        // $lpdpui = DB::table('users')->where("name", "LIKE", "LPDPUI%")->where("id",Auth::user()->id)->first();
        //     // $profile = Auth::user()->name;
        // // dd($lpdpui->company_id);
        // $comp = \App\Company::find($lpdpui->company_id);
        // $comp->membership_type = 'member';
        // $comp->valid_to = date('Y-m-d', strtotime(date('Y-m-d'). " + 60 days"));
        // $comp->save();

        // $request->session()->put('user_language', $profile->language);

        //Update Contact
        // if($profile->user_type == 'client'){
        //     $contact = Contact::where('user_id',$profile->id)
        //                       ->update(['contact_email' => $profile->email]);
        // }


        DB::commit();
        return redirect('/dashboard')->with('success', _lang('Data diri Berhasil di simpan'));
    }

    /**
     * Show the form for change_password the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function change_password()
    {
        return view('backend.profile.change_password');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_password(Request $request)
    {
        $this->validate($request, [
            'oldpassword' => 'required',
            'password' => 'required|string|min:6|confirmed',
        ]);

        $user = User::find(Auth::User()->id);
        if(Hash::check($request->oldpassword, $user->password)){
            $user->password = Hash::make($request->password);
            $user->save();
        }else{
            return back()->with('error', _lang('Old Password did not match !'));
        }
        return back()->with('success', _lang('Password has been changed'));
    }

}
