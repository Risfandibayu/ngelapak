<?php

namespace App\Http\Controllers;

use App\Company;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\CompanySetting;
use Carbon\Carbon;
use DB;
// use App\Company;
use App\Province;
use App\City;
use App\Subdistrict;
use Illuminate\Support\Facades\Auth;
use ParagonIE\Sodium\Compat;
use Image;

class CompanySettingsController extends Controller
{
	public function __construct(){
		header('Cache-Control: no-cache');
		header('Pragma: no-cache');
	} 
	 
    public function settings($store="",Request $request)
    {
		if($store == ""){
           return view('backend.accounting.general_settings.settings');
        }else{
            $company_id = company_id();			
		    foreach($_POST as $key => $value){
				 if($key == "_token"){
					 continue;
				 }
				 
				 $data = array();
				 $data['value'] = $value; 
				 $data['company_id'] = $company_id; 
				 $data['updated_at'] = Carbon::now();
				 
				 if(CompanySetting::where('name', $key)->where("company_id",$company_id)->exists()){				
					CompanySetting::where('name','=',$key)
					              ->where("company_id",$company_id)
								  ->update($data);			
				 }else{
					$data['name'] = $key; 
					$data['created_at'] = Carbon::now();
					CompanySetting::insert($data); 
				 }
		    } //End Loop
			
			\Cache::forget('base_currency'.session('company_id'));
			\Cache::forget('currency_position'.session('company_id'));
			
			if(! $request->ajax()){
			   return redirect('company/general_settings')->with('success', _lang('Saved Sucessfully'));
			}else{
			   return response()->json(['result'=>'success','action'=>'update','message'=>_lang('Saved Sucessfully')]);
			}

		}
	}

	public function crm_settings(Request $request)
    {
    	$company_id = company_id();	
    	$data = array();
    	$data['leadstatuss'] = \App\LeadStatus::where('company_id',$company_id)
    	                                      ->orderBy("order","asc")
    	                                      ->get();
											  
		$data['leadsources'] = \App\LeadSource::where('company_id',$company_id)
    	                                      ->orderBy("id","desc")
    	                                      ->get();
											  
		$data['task_statuss'] = \App\TaskStatus::where('company_id',$company_id)
    	                                       ->orderBy("order","asc")
    	                                       ->get();

        return view('backend.accounting.general_settings.crm_settings',$data);
        
	}
	
	
	public function upload_logo(Request $request){

		$this->validate($request, [
			'logo' => 'required|image|mimes:jpeg,png,jpg|max:8192',
		]);

		$company_id = company_id();

		if ($request->hasFile('logo')) {
			$image = $request->file('logo');
			$name = 'company_logo'.time().'.'.$image->getClientOriginalExtension();
			$destinationPath = public_path('/uploads/company');
			$image->move($destinationPath, $name);

			$data = array();
			$data['value'] = $name; 
			$data['company_id'] = $company_id; 
			$data['updated_at'] = Carbon::now();
			
			if(CompanySetting::where('name', "company_logo")->where("company_id",$company_id)->exists()){				
				CompanySetting::where('name','=',"company_logo")
							  ->where("company_id",$company_id)
							  ->update($data);			
			}else{
				$data['name'] = "company_logo"; 
				$data['created_at'] = Carbon::now();
				CompanySetting::insert($data); 
			}
			
			if(! $request->ajax()){
			   return redirect('company/general_settings')->with('success', _lang('Saved Sucessfully'));
			}else{
			   return response()->json(['result'=>'success','action'=>'update','message'=>_lang('Logo Upload successfully')]);
			}

		}
	}
	
	public function upload_file($file_name,Request $request){

		if ($request->hasFile($file_name)) {
			$file = $request->file($file_name);
			$name = 'file_'.time().".".$file->getClientOriginalExtension();
			$destinationPath = public_path('/uploads/media');
			$file->move($destinationPath, $name);

			$data = array();
			$data['value'] = $name; 
			$data['company_id'] = company_id(); 
			$data['updated_at'] = Carbon::now();
			
			if(Setting::where('name', $file_name)->exists()){				
				Setting::where('name','=',$file_name)->update($data);			
			}else{
				$data['name'] = $file_name; 
				$data['created_at'] = Carbon::now();
				Setting::insert($data); 
			}	
		}
	}

	public function edit(Company $company){
		// $company = \App\Company::where('company_id', Auth::user()->company_id)->first();
		$data['province'] = Province::all();
        $data['city'] = City::all();
        $data['subdistrict'] = Subdistrict::all();
		$company = \App\Company::find(Auth::User()->company_id);
        return view('backend.company.company_edit', compact('company'),$data);
	}
	public function update(Request $request)
    {
        $this->validate($request, [
            'business_name' => 'required',
            'alamat' => 'required',
            'kota' => 'required',
            'kec' => 'required',
            'prov' => 'required',
            'kode_pos' => 'required',
            'deskripsi' => 'required',
            
            
        ]);
       
        DB::beginTransaction();

        $company = \App\Company::find(Auth::User()->company_id);
		$company->business_name = $request->business_name;
		$company->alamat = $request->alamat;
		$company->kota = $request->kota;
		$company->kec = $request->kec;
		$company->prov = $request->prov;
		$company->kode_pos = $request->kode_pos;
		$company->cabang = $request->cabang;
		$company->deskripsi = $request->deskripsi;
		$company->slogan = $request->slogan;
		$company->ig = $request->ig;
		$company->fb = $request->fb;
		$company->web = $request->web;
		$company->self_serv = $request->self_serv;
		
		if ($request->hasFile('logo')){
            $image = $request->file('logo');
            $file_name = "logo_".time().'.'.$image->getClientOriginalExtension();
            Image::make($image)->crop(512, 512)->save(base_path('public/uploads/logo_usaha/') .$file_name);
            $company->logo = $file_name;
        }
        
        if ($request->hasFile('ft_prd1')){
                    $image = $request->file('ft_prd1');
                    $file_name = "ftprd1_".time().'.'.$image->getClientOriginalExtension();
                    Image::make($image)->save(base_path('public/uploads/foto_produk/') .$file_name);
                    $company->ft_prd1 = $file_name;
        }
        
        if ($request->hasFile('ft_prd2')){
                    $image = $request->file('ft_prd2');
                    $file_name = "ftprd2_".time().'.'.$image->getClientOriginalExtension();
                    Image::make($image)->save(base_path('public/uploads/foto_produk/') .$file_name);
                    $company->ft_prd2 = $file_name;
        }
        if ($request->hasFile('ft_prd3')){
                    $image = $request->file('ft_prd3');
                    $file_name = "ftprd3_".time().'.'.$image->getClientOriginalExtension();
                    Image::make($image)->save(base_path('public/uploads/foto_produk/') .$file_name);
                    $company->ft_prd3 = $file_name;
        }
        if ($request->hasFile('ft_prd4')){
                    $image = $request->file('ft_prd4');
                    $file_name = "ftprd4_".time().'.'.$image->getClientOriginalExtension();
                    Image::make($image)->save(base_path('public/uploads/foto_produk/') .$file_name);
                    $company->ft_prd4 = $file_name;
        }
        
        if ($request->hasFile('splash')){
            $image = $request->file('splash');
            $file_name = "splash_".time().'.'.$image->getClientOriginalExtension();
            Image::make($image)->crop(2732, 2732)->save(base_path('public/uploads/splash/') .$file_name);
            $company->splash = $file_name;
        }
        if ($request->hasFile('bg_apk')){
            $image = $request->file('bg_apk');
            $file_name = "bg_apk_".time().'.'.$image->getClientOriginalExtension();
            Image::make($image)->save(base_path('public/uploads/bg_apk/') .$file_name);
            $company->bg_apk = $file_name;
        }
        $company->judul_camp = $request->judul_camp;
        $company->sub_judul = $request->sub_judul;
        $company->key_brand = $request->key_brand;
        $company->deskripsi_camp = $request->deskripsi_camp;
        if ($request->hasFile('cover')){
            $image = $request->file('cover');
            $file_name = "cover_".time().'.'.$image->getClientOriginalExtension();
            Image::make($image)->crop(400, 400)->save(base_path('public/uploads/cover/') .$file_name);
            $company->cover = $file_name;
        }
        if ($request->hasFile('thumbnail')){
            $image = $request->file('thumbnail');
            $file_name = "tmnail_".time().'.'.$image->getClientOriginalExtension();
            Image::make($image)->crop(400, 400)->save(base_path('public/uploads/thumbnail/') .$file_name);
            $company->thumbnail = $file_name;
        }
        if ($request->hasFile('logo_camp')){
            $image = $request->file('logo_camp');
            $file_name = "logocamp_".time().'.'.$image->getClientOriginalExtension();
            Image::make($image)->crop(512, 512)->save(base_path('public/uploads/logo_camp/') .$file_name);
            $company->logo_camp = $file_name;
        }
        if ($request->hasFile('gambar_camp')){
            $image = $request->file('gambar_camp');
            $file_name = "gbrcamp_".time().'.'.$image->getClientOriginalExtension();
            Image::make($image)->save(base_path('public/uploads/gambar_camp/') .$file_name);
            $company->gambar_camp = $file_name;
        }

        $company->save();

        DB::commit();

        return redirect('membership/extend')->with('success', _lang('Pengisian data perusahaan berhasil. Silahkan pilih paket membership'));
        // return redirect('/dashboard')->with('success', _lang('Pengisian data perusahaan berhasil. Silahkan pilih paket membership'));
    }

	
}